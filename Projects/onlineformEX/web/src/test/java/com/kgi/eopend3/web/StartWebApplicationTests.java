package com.kgi.eopend3.web;

import java.util.Date;

import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jwt.EncryptedJWT;
import com.nimbusds.jwt.JWTClaimsSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StartWebApplicationTests {

	/**
     * 有效時間 5天
     */
    static final long EXPIRATIONTIME = 432_000_000;
    /**
     * JWT密碼
     */
    static final String SECRET = "P@ssw02d";

    static final byte[] key = "kgieopfuco201907".getBytes();
    /**
     * Token前缀
     */
    static final String TOKEN_PREFIX = "Bearer";
    /**
     * 存放Token的Header Key
     */
    static final String HEADER_STRING = "Authorization";

	@Test
	public void main() {		
		try {

            // Create the header
            JWEHeader header = new JWEHeader(JWEAlgorithm.DIR, EncryptionMethod.A128GCM);

            JWTClaimsSet.Builder claimsSet = new JWTClaimsSet.Builder();
            claimsSet.subject("{\"uniqid\":\"ED320190101000001\",\"uniqType\":\"13\",\"idno\":\"F111111111\",\"ipAddress\":\"127.0.0.1\"}");
            claimsSet.expirationTime(new Date(System.currentTimeMillis() + EXPIRATIONTIME));
            // claimsSet.notBeforeTime(new Date());

            // create the EncryptedJWT object
            EncryptedJWT jweObject = new EncryptedJWT(header, claimsSet.build());

            jweObject.encrypt(new DirectEncrypter(key));

            // Serialise to compact JOSE form...
            String JWT = jweObject.serialize();
            System.out.println(JWT);

            
        } catch (Exception e) {
            
        }
    }
    
}

