
// NOTE: FORGOT LOGIN

var funcForgotCreateCaptcha = _ => {
    let captcha1 = new CaptchaMini();
    captcha1.draw(document.querySelector('#captcha1'), r => {
        captchaForgotChk = r;
    });
}

var funcForgotCheckId = _ => {
    $( '.idRequired' ).addClass( 'hidden' );
    var forgot_cust_id = $( '#forgotLoginEntry1' ).val();
    if (forgot_cust_id === '') {
        return $( '.idRequired' ).removeClass( 'hidden' ) && 
               $( '#forgotLoginEntry1' ).addClass( 'inputError' );
    }
    IdCardNumberCheck(forgot_cust_id)
        ? $( '.idError' ).addClass( 'hidden' ) && $( '#forgotLoginEntry1' ).removeClass( 'inputError' )
        : $( '.idError' ).removeClass( 'hidden' ) && $( '#forgotLoginEntry1' ).addClass( 'inputError' );
}

var funcForgotCheckCaptcha = _ => {
    $( '.captchaError' ).addClass( 'hidden' );
    $( '#forgotLoginEntry3' ).removeClass( 'inputError' );
    $( '#forgotLoginBtnSubmit1' ).removeClass( 'disabled' );
    $( '#forgotLoginBtnSubmit2' ).removeClass( 'disabled' );
    var captchaInput = $( '#forgotLoginEntry3' ).val();
    if (captchaForgotChk !== captchaInput) {
        return $( '.captchaError' ).removeClass( 'hidden' ) && 
               $( '#forgotLoginEntry3' ).addClass( 'inputError' ) &&
               $( '#forgotLoginBtnSubmit1' ).addClass( 'disabled' ) &&
               $( '#forgotLoginBtnSubmit2' ).addClass( 'disabled' );
    }
}


$( '#forgotLoginEntry1' ).on( 'focusout', funcForgotCheckId );
$( '#forgotLoginEntry3' ).on( 'focusout', funcForgotCheckCaptcha );
