// SECTOR: Common
var funcShowLog = (name, ...data) => {
    var message = '====> ';
    data.forEach(
        e => message + '%s, '
    )
    console.log( message, name, ...data);
}

var IdCardNumberCheck = id => {
    var city = new Array(1, 10, 19, 28, 37, 46, 55, 64, 39, 73, 82, 2, 11, 20, 48, 29, 38, 47, 56, 65, 74, 83, 21, 3, 12, 30);
    id = id.toUpperCase();

    // 使用「正規表達式」檢驗格式
    if (!id.match(/^[A-Z]\d{9}$/) && !id.match(/^[A-Z][A-D]\d{8}$/)) return false;

    else {
        var total = 0;
        if (id.match(/^[A-Z]\d{9}$/)) { //身分證字號號
            //將字串分割為陣列(IE必需這麼做才不會出錯)
            id = id.split('');
            //計算總分
            total = city[id[0].charCodeAt(0) - 65];
            for (var i = 1; i <= 8; i++) {
                total += eval(id[i]) * (9 - i);
            }
        } else { // 外來人口統一證號
            //將字串分割為陣列(IE必需這麼做才不會出錯)
            id = id.split('');
            //計算總分
            total = city[id[0].charCodeAt(0) - 65];
            // 外來人口的第2碼為英文A-D(10~13)，這裡把他轉為區碼並取個位數，之後就可以像一般身份證的計算方式一樣了。
            id[1] = id[1].charCodeAt(0) - 65;
            for (var i = 1; i <= 8; i++) {
                total += eval(id[i]) * (9 - i);
            }
        }
        //補上檢查碼(最後一碼)
        total += eval(id[9]);

        //檢查比對碼(餘數應為0);
        return total % 10 == 0 ? true : false;
    }
};

var showMessage = (data) => {
    $('#messageModalContent').empty();
    $('#messageModalContent').append(data.message);
    $('#exampleModal').modal('show');
};

var afterCloseModel = (_) => {
    if (flagStatus === 5) {
        return window.location.reload(false);
    }
    if (flagStatus === 7) {
        return window.location.reload(false);
    }
};

var connectSetting = (m, u, d) => {
    return {
        method: m,
        url: u,
        contentType: 'application/json',
        data: d,
        dataType: 'json',
        beforeSend: function () {
            $('#loader').show();
        },
        complete: function (data) {
            if( u != serveAPHost + '/mockRoute/mgm/checkMemberDetail'
                && data.status != 0){
                $('#loader').hide();
            }else if(u != serveAPHost + '/mockRoute/mgm/resetPassword'
            && data.status != 0){
                $('#loader').hide();
            }
        },
    };
};

// SECTOR: Parameter
var flagStatus = 0;
var dataReceive;
var userIdentify;
var captchaLoginChk;
var captchaForgotChk;
var cust_id;
var season;
var applyType;
var bonusType;
var giftId;
var arrAct$ = []; // It's array
var arrShortUrl$ = []; // It's array

// SECTOR: SUBMIT
// Sector_Alpha LOGIN
var funcLoginBtn = (_) => {
    console.log('====> Access the funcLoginBtn....');
    let channel_id = $('#sectAselect').val();
    cust_id = $('#loginUserid').val();
    let pw = $('#loginPassword').val();
    if (cust_id && pw && channel_id !== '0') {
        $.ajax(

            connectSetting(
                'POST',
                serveAPHost + '/mockRoute/mgm/checkMemberDetail',
                JSON.stringify({CHANNEL_ID: channel_id, CUST_ID: cust_id, PASSWORD: pw })
            )
            
        ).done((data) => {
            switch (data.status) {
                case 0:
                    dataReceive = JSON.parse(data.result);
                    getSummaryCase(7, 1, 1);
                    getSummaryBonus(7, 1, 1);
                    break;
                case 1:
                    dataReceive = JSON.parse(data.result);
                    funcSectorChange(1);
                    break;
                case 9:
                    showMessage(data);
                    break;
                default:
                    showMessage(data);
                    break;
            }
        });
    }
};

var funcLogoutBtn = (_) => {
    
    $('#loginUserid').val("");
    $('#loginPassword').val("");
    $('#loginCaptchaEntry1').val("");
    
    funcSectorChange(0);
}

var checkDepartNum1 = (_) => {
    console.log('====> Access the checkDepartNum1');
    let departnum1 = $('#departNum1').val();
    if (departnum1) {
        document.getElementById('error_departNum1').setAttribute('style', 'display: none;');
        document.getElementById('departNum1').removeAttribute('style');
        return true;
    } else {
        document.getElementById('error_departNum1').removeAttribute('style');
        document.getElementById('departNum1').setAttribute('style', 'border: 1px solid red;');
    }
}

var checkPassWord = (_) => {
    console.log('====> Access the checkPassWord');
    let reg = /^(?=.*[a-zA-Z])(?=.*\d).{6,16}$/;
    let pass1 = $('#passRe1').val();
    let pass2 = $('#passRe2').val();
    let flag = reg.test(pass1);
    if (!pass1) {
        document.getElementById('passwordError').setAttribute('style', 'display: none;');
        document.getElementById('re1Required').removeAttribute('style');
        document.getElementById('passRe1').setAttribute('style', 'border: 1px solid red;');
    }
    if (pass1 && !flag) {
        document.getElementById('passwordError').removeAttribute('style');
        document.getElementById('re1Required').setAttribute('style', 'display: none;');
        document.getElementById('passRe1').setAttribute('style', 'border: 1px solid red;');
    }
    if (pass1 && flag) {
        document.getElementById('re1Required').setAttribute('style', 'display: none;');
        document.getElementById('passwordError').setAttribute('style', 'display: none;');
        document.getElementById('passRe1').setAttribute('style', 'border: 1px solid #ced4da;');
    }
    if (!pass2) {
        document.getElementById('passwordError2').setAttribute('style', 'display: none;');
        document.getElementById('re2Required').removeAttribute('style');
        document.getElementById('passRe2').setAttribute('style', 'border: 1px solid red;');
    }
    if (pass2 && pass2 !== pass1) {
        document.getElementById('passwordError2').removeAttribute('style');
        document.getElementById('re2Required').setAttribute('style', 'display: none;');
        document.getElementById('passRe2').setAttribute('style', 'border: 1px solid red;');
    }
    if (pass2 && pass2 === pass1) {
        document.getElementById('re2Required').setAttribute('style', 'display: none;');
        document.getElementById('passwordError2').setAttribute('style', 'display: none;');
        document.getElementById('passRe2').setAttribute('style', 'border: 1px solid #ced4da;');
    }
    if(pass1 && pass2 && flag && pass2 === pass1){
        return true;
    }
};


var checkPassWord2 = (_) => {
    console.log('====> Access the checkPassWord');
    let reg = /^(?=.*[a-zA-Z])(?=.*\d).{6,16}$/;
    let pass1 = $('#setPass1').val();
    let pass2 = $('#setPass2').val();
    let flag = reg.test(pass1);

    if (!pass1) {
        document.getElementById('passwordError3').setAttribute('style', 'display: none;');
        document.getElementById('re1Required1').removeAttribute('style');
        document.getElementById('setPass1').setAttribute('style', 'border: 1px solid red;');
    }
    if (pass1 && !flag) {
        document.getElementById('passwordError3').removeAttribute('style');
        document.getElementById('re1Required1').setAttribute('style', 'display: none;');
        document.getElementById('setPass1').setAttribute('style', 'border: 1px solid red;');
    }
    if (pass1 && flag) {
        document.getElementById('re1Required1').setAttribute('style', 'display: none;');
        document.getElementById('passwordError3').setAttribute('style', 'display: none;');
        document.getElementById('setPass1').setAttribute('style', 'border: 1px solid #ced4da;');
    }
    if (!pass2) {
        document.getElementById('passwordError4').setAttribute('style', 'display: none;');
        document.getElementById('re2Required1').removeAttribute('style');
        document.getElementById('setPass2').setAttribute('style', 'border: 1px solid red;');
    }
    if (pass2 && pass2 !== pass1) {
        document.getElementById('passwordError4').removeAttribute('style');
        document.getElementById('re2Required1').setAttribute('style', 'display: none;');
        document.getElementById('setPass2').setAttribute('style', 'border: 1px solid red;');
    }
    if (pass2 && pass2 === pass1) {
        document.getElementById('re2Required1').setAttribute('style', 'display: none;');
        document.getElementById('passwordError4').setAttribute('style', 'display: none;');
        document.getElementById('setPass2').setAttribute('style', 'border: 1px solid #ced4da;');
    }
    if(pass1 && pass2 && flag && pass2 === pass1){
        return true;
    }
};

// Sector_Beta RESET
var funcResetBtnSubmit = (_) => {
    console.log('====> Access the funcResetBtn');
    let channel_id = $('#sectAselect').val();
    let departnum1 = $('#departNum1').val();
    let emp1 = $('#employNum1').val();
    let pass1 = $('#passRe1').val();
    if (checkDepartNum1() & checkPassWord()){
        // console.log("====> subment");
        $.ajax(
          connectSetting(
            'POST',
            serveAPHost + '/mockRoute/mgm/resetPassword',
            JSON.stringify({CHANNEL_ID:channel_id, CUST_ID: dataReceive.CUST_ID, PASSWORD: pass1, EMP_ID: emp1, DEPART_ID: departnum1 })
          )
        ).done((data) => {
            // TODO: send SMS mail
            switch (data.status) {
                case 0:
                    funcSectorChange(2);
                    funcResetSendMail(data);
                    break;
                default:
                    showMessage(data);
                    break;
            }
        });
    }
};

// Sector_Gamma SUCC
var funcSuccBtn = (_) => {
    // TODO:
    return window.location.reload(false);
};
// Sector_Delta
// Sector_Epilson
var funcForgotLoginBtnSubmit = (_) => {
    var a = $('#forgotLoginEntry1').val();
    var b = $('#forgotLoginEntry2').val();
    var c = $('#forgotLoginEntry3').val();
    if (a && b && c) {
        $.ajax(
          connectSetting(
            'POST',
            serveAPHost + '/mockRoute/mgm/forgotPassword',
            JSON.stringify({ CUST_ID: a, BIRTH_4: b })
          )
        ).done((data) => {
            switch (data.status) {
                case 0:
                    console.log(data);
                    dataReceive = data.result;
                    funcSectorChange(5);
                    break;
                case 1:
                    console.log('Password Error');
                    showMessage(data);
                    break;
                case 2:
                    console.log('Account Not Access');
                    showMessage(data);
                    break;
                case 4:
                    console.log('Too Many time error input');
                    showMessage(data);
                    break;
                case 9:
                    console.log('Not Found');
                    showMessage(data);
                    break;
                default:
                    break;
            }
        });
    }
};
// Sector_Zeta

// Sector_Eta
var funcForgotPassBtnSubmit = (_) => {
    $('.passRequired').addClass('hidden');
    $('#setPass1').removeClass('inputError');
    $('#setPass2').removeClass('inputError');

    var a = $('#setPass1').val();
    var b = $('#setPass2').val();
    cust_id = dataReceive.CUST_ID;

    if (a === b && a !== '') {
        $.ajax(
          connectSetting(
            'POST',
            serveAPHost + '/mockRoute/mgm/forgotPasswordReset',
            JSON.stringify({ CUST_ID: cust_id, CUST_PW: b })
          )
        ).done((data) => {
            switch (data.status) {
                case 0:
                    flagStatus = 7;
                    showMessage(data);
                    break;
                default:
                    showMessage(data);
                    break;
            }
        });
    } else {
        return (
          $('.passRequired').removeClass('hidden') &&
          $('#setPass1').addClass('inputError') &&
          $('#setPass2').addClass('inputError')
        );
    }
};

// SECTOR: INIT:
// Sector_Init
var funcSectorChange = (status) => {
    funcShowLog('flagStatus', status);
    flagStatus = status;
    switch (status) {
        case 0:
            funcSectorSelector('loginSector');
            funMGMSingleLoginInit();
            break;
        case 1:
            funcSectorSelector('resetSector');
            funcMGMSingleResetInit(userIdentify);
            break;
        case 2:
            funcSectorSelector('finishSector');
            break;
        case 3:
            funcSectorSelector('activitySector');
            funcMGMSingleDeltaInit(dataReceive.CUST_ID);
            break;
        case 4:
            funcSectorSelector('forgotLoginSector');
            funcForgotCreateCaptcha();
            break;
        case 5:
            funcSectorSelector('forgotOtpSector');
            funcMGMSingleOtpInit(dataReceive.CUST_PHONE);
            break;
        case 6:
            funcSectorSelector('forgotPassSector');
            break;

        // -----------------------mgm =====================
        case 7:
            funcSectorSelector('dashBoardSector');
            break;
        case 8:
            funcSectorSelector('progressEnquirySector');
            break;
        case 9:
            funcSectorSelector('myRecommendationSector');
            break;
        case 10:
            funcSectorSelector('myPointSector');
            break;
        case 11:
            funcSectorSelector('pointExchangeSector');
            break;
        case 12:
            funcSectorSelector('exchangeApplySector');
            break;
        case 13:
            funcSectorSelector('exchangeDoneSector');
            break;
    }
};

var funcSectorSelector = (sectorName) => {
    [
        'loginSector',
        'resetSector',
        'finishSector',
        'activitySector',
        'forgotLoginSector',
        'forgotOtpSector',
        'forgotPassSector',
        'dashBoardSector', 
        'progressEnquirySector', 
        'myRecommendationSector', 
        'myPointSector', 
        'pointExchangeSector', 
        'exchangeApplySector', 
        'exchangeDoneSector', 
    ].forEach((e) => {
        e === sectorName ? $('#' + sectorName).removeClass('hidden') : $('#' + e).addClass('hidden');
    });

    if(sectorName === 'loginSector'
        || sectorName === 'resetSector'
        || sectorName === 'finishSector'
        || sectorName === 'forgotLoginSector'
        || sectorName === 'forgotOtpSector'
        || sectorName === 'forgotPassSector'
        || sectorName === 'forgotLoginSector'){
        document.getElementById("logoutBtn1").hidden=true;
        document.getElementById("logoutBtn2").hidden=true;
    }else{
        document.getElementById("logoutBtn1").hidden=false;
        document.getElementById("logoutBtn2").hidden=false;
    }
};

$(document).ready((_) => {
    $('.page-title').append('MGM');
    funcSectorChange(0);
});

$('#showConsume').on('click', (_) => showMessage({ message: '客服專線：(02)8023-9088' }));
$('#exampleModal').on('hidden.bs.modal', afterCloseModel);

$("button[name*='loginBtnSubmit']").on('click', funcLoginBtn);
$("button[name*='resetBtnSubmit']").on('click', funcResetBtnSubmit);
$("button[name*='successBtnSubmit']").on('click', funcSuccBtn);
// $( "button[name*='successBtnSubmit']" ).on( 'click', funcLoginBtn );
$("button[name*='forgotLoginBtnSubmit']").on('click', funcForgotLoginBtnSubmit);
$("button[name*='forgotOtpBtnSubmit']").on('click', funcCheckOtp);
$("button[name*='forgotPassBtnSubmit']").on('click', funcForgotPassBtnSubmit);
$('#departNum1').on('keyup', checkDepartNum1);
$('#passRe1').on('keyup', checkPassWord);
$('#passRe2').on('keyup', checkPassWord);
$('#setPass1').on('keyup', checkPassWord2);
$('#setPass2').on('keyup', checkPassWord2);
$('#poolAlpha').on('hidden.bs.collapse show.bs.collapse', '.collapse', function () {
    let id = $(this).attr('id');
    let btn = $(`button[data-target="#${id}"]`);
    let img = btn.find('img');
    if (img.attr('src').includes('03-chevronup.png')) {
        console.log('imgid.src1');
        img.attr('src', 'assets/image/icon/04-chevrondown.png');
    } else {
        console.log('imgid.src2');
        img.attr('src', 'assets/image/icon/03-chevronup.png');
    }
});

var checkMemberId = (id) => {
    $.ajax(
      connectSetting(
        'POST',
        serveAPHost + '/mockRoute/mgm/checkMemberId',
        JSON.stringify({ CUST_ID: id, PASSWORD: '' })
      )
    ).done((data) => {
        switch (data.status) {
            case 0:
                $('#loginFirst').addClass('hidden');
                break;
            case 9:
                $('#loginFirst').removeClass('hidden');
                break;
            default:
                $('#loader').hide();
                break;
        }
    });
};

// dashboardSector
// chevrondown change color
const ChangeChevrondownColor = (target) => {
    $('.row-slider .enquiryCard')
        .removeClass('active')
        .find('img:eq(1)')
        .attr('src', 'assets/image/icon/04-chevrondown-grey.svg');
    $(target).addClass('active').find('img:eq(1)').attr('src', 'assets/image/icon/04-chevrondown.svg');
};
$('.row-slider .enquiryCard').on('click', function(){
    ChangeChevrondownColor(this);
});
const gotoApplyDetail = (type) => {
    let id;
    if(type === 1){
        id = "search-appling";
    } else if(type === 12){
        id = "search-applied";
    } else if(type === 22){
        id = "search-audited";
    } else if(type === 4){
        id = "search-card-bonus";
        getSummaryBouns(9, 1, 1);
    } else if(type === 5){
        id = "search-point";
        getSummaryCase(11, 1, 1);
    }
    getSummaryCase(8, type, 1);
    ChangeChevrondownColor(document.getElementById(id));
};
const gotoBonusDetail = (type) => {
    let id;
    if(type === 1){
        id = "search-card-bonus";
    } else if(type === 2){
        id = "search-bonus";
    }
    getSummaryBonus(9, type, 1);
    ChangeChevrondownColor(document.getElementById(id));
};
const gotoPointList = () => {
    giftId = "";
    funcSectorChange(11);
    getGiftList();
};
const gotoPointDetail = (gfId) => {
    funcSectorChange(12);
    giftId = gfId;
    getGiftList();
};

var getSummaryCase = (goto, type, seasonCondition) => {

    if (type === undefined | type === ''){
        type = applyType;
    } else {
        applyType = type;
    }
    let seasonCurrentObj = document.getElementById("season_current");
    let seasonLastObj = document.getElementById("season_last");
    if (seasonCondition === 1){
        seasonCurrentObj.style.color = "blue";
        seasonLastObj.style.color = "black";
        season = seasonCurrentObj.textContent;
    } else if (seasonCondition === 2){
        seasonCurrentObj.style.color = "black";
        seasonLastObj.style.color = "blue";
        season = seasonLastObj.textContent;
    }

    let typeStr;
    if (type === 12){
        typeStr = "送出完成"
    } else if(type === 22){
        typeStr = "審核完成"
    } else{
        typeStr = "申請"
    }

    $.ajax(
        connectSetting(
          'POST',
          serveAPHost + '/mockRoute/mgm/getSummaryCase',
          JSON.stringify({ ER_ID: cust_id, TYPE: type, SEASON: season })
        )
    ).done((data) => {
        switch (data.status) {
            case 0:
                // 申請中件數
                document.getElementById("dashboard-appling-cnt").textContent = data.dashboardApplingCnt;
                document.getElementById("detail-appling-cnt").textContent = data.detailApplingCnt;
                // 申請完成件數
                document.getElementById("dashboard-applied-cnt").textContent = data.dashboardAppliedCnt;
                document.getElementById("detail-applied-cnt").textContent = data.detailAppliedCnt;
                // 審核完成件數
                document.getElementById("dashboard-audited-cnt").textContent = data.dashboardAuditedCnt;
                document.getElementById("detail-audited-cnt").textContent = data.detailAuditedCnt;
                // 失效件數
                document.getElementById("detail-invalid-cnt").textContent = data.detailInvalidCnt;
        
                // 是否顯示已失效件數
                document.getElementById("search-invalid").hidden = data.detailInvalidCnt === "0";
        
                seasonCurrentObj.textContent = data.seasonCurrent;
                seasonLastObj.textContent = data.seasonLast;
        
                if(season === undefined || season === ""){
                    season = seasonCurrentObj.textContent;
                }
        
                document.getElementById("applyDetail").textContent="";
        
                let cnt = 1;
                data.dataList.forEach(a => {
                    $( '#applyDetail' ).append(
                        '<div class="card">' +
                            '<div class="card-header mb-0" id="heading' + cnt + '">' +
                                '<button ' +
                                    'class="btn btn-link w-100"' +
                                    'style="color:#000000"' +
                                    'type="button"' +
                                    'data-toggle="collapse"' +
                                    'data-target="#collapse' + cnt + '"' +
                                    'aria-expanded="true"' +
                                    'aria-controls="collapse' + cnt + '"' +
                                '>' +
                                    '<div class="d-flex w-100">' +
                                        '<div class="card-text">' +
                                            '<div class="row w-100">' +
                                                '<span class="col-5 mb-1">' + cnt + '.' + a.EE_NAME + '</span>' +
                                                '<span class="col-1"></span>' +
                                                '<span>' + a.PRO_NAME + '/' + a.CASE_STATUS_DESC + '</span>' +
                                            '</div>' +
                                            '<div class="row w-100">' +
                                                '<span class="col-5">' + typeStr + '日期</span>' +
                                                '<span class="col-1"></span>' +
                                                '<span>' + a.APPLY_DT + '</span>' +
                                            '</div>' +
                                        '</div>' +
                                        '<img ' +
                                            'src="assets/image/icon/04-chevrondown.png"' +
                                            'alt=""' +
                                            'width="36px"' +
                                            'height="36px"' +
                                        '/>' +
                                    '</div>' +
                                '</button>' +
                            '</div>' +
        
                            '<div ' +
                                'id="collapse' + cnt + '"' +
                                'class="collapse"' +
                                'aria-labelledby="heading' + cnt + '"' +
                                'data-parent="#applyDetail"' +
                            '>' +
                                '<div class="card-body">活動 ' + a.ER_ACC_TYPE + '</div>' +
                            '</div>' +
                        '</div>'
                    );
                    cnt++;
                });
        
                funcSectorChange(goto);
                break;
            default:
                showMessage(data);
                break;
        }
    })
};

var getSummaryBonus = (goto, type, seasonCondition) => {

    if (type === undefined || type === ''){
        type = bonusType;
    } else {
        bonusType = type;
    }
    let seasonCurrentObj = document.getElementById("season_current_bonus");
    let seasonLastObj = document.getElementById("season_last_bonus");
    if (seasonCondition === 1){
        seasonCurrentObj.style.color = "blue";
        seasonLastObj.style.color = "black";
        season = seasonCurrentObj.textContent;
    } else if (seasonCondition === 2){
        seasonCurrentObj.style.color = "black";
        seasonLastObj.style.color = "blue";
        season = seasonLastObj.textContent;
    }

    $.ajax(
        connectSetting(
          'POST',
          serveAPHost + '/mockRoute/mgm/getSummaryBonus',
          JSON.stringify({ ER_ID: cust_id, TYPE: type, SEASON: season })
        )
    ).done((data) => {

        switch (data.status) {
            case 0:
                // 我的推薦禮
                document.getElementById("dashboard-present").textContent = "$" + data.dashboardBonusTotal;
        
                document.getElementById("detail-card-bonus-total").textContent = "$" + data.detailCardBonusTotal;
                document.getElementById("detail-bonus-total").textContent = "$" + data.detailBonusTotal;
        
                document.getElementById("season_current_bonus").textContent = data.seasonCurrent;
                document.getElementById("season_last_bonus").textContent = data.seasonLast;
        
        
                seasonCurrentObj.textContent = data.seasonCurrent;
                seasonLastObj.textContent = data.seasonLast;
        
                if(season === undefined || season === ""){
                    season = seasonCurrentObj.textContent;
                }
        
                document.getElementById("bonusDetail").textContent="";
        
                let cnt = 1;
                data.dataList.forEach(a => {
                    
                    $( '#bonusDetail' ).append(
                        '<div class="card">' +
                            '<div class="card-header mb-0" id="heading' + cnt + '">' +
                                '<button ' +
                                    'class="btn btn-link w-100"' +
                                    'style="color:#000000"' +
                                    'type="button"' +
                                    'data-toggle="collapse"' +
                                    'data-target="#collapse' + cnt + '"' +
                                    'aria-expanded="true"' +
                                    'aria-controls="collapse' + cnt + '"' +
                                '>' +
                                    '<div class="d-flex w-100">' +
                                        '<div class="card-text">' +
                                            '<div class="row w-100">' +
                                                '<span class="col-5 mb-1">' + cnt + '.' + a.EE_NAME + '</span>' +
                                                '<span class="col-1"></span>' +
                                                '<span>' + a.PRO_NAME + '</span>' +
                                            '</div>' +
                                            '<div class="row w-100">' +
                                                '<span class="col-5">' + a.ER_ACC_TYPE + '</span>' +
                                                '<span class="col-1"></span>' +
                                                '<span>$' + a.ER_BONUS_AMT + '</span>' +
                                            '</div>' +
                                        '</div>' +
                                        '<img ' +
                                            'src="assets/image/icon/04-chevrondown.png"' +
                                            'alt=""' +
                                            'width="36px"' +
                                            'height="36px"' +
                                        '/>' +
                                    '</div>' +
                                '</button>' +
                            '</div>' +
        
                            '<div ' +
                                'id="collapse' + cnt + '"' +
                                'class="collapse"' +
                                'aria-labelledby="heading' + cnt + '"' +
                                'data-parent="#applyDetail"' +
                            '>' +
                                '<div class="card-body"> ' + a.ER_BONUS_NAME + ' - ' + a.PRO_NAME + '</div>' +
                            '</div>' +
                        '</div>'
                    );
                    cnt++;
                });
        
                funcSectorChange(goto);
                break;
            default:
                showMessage(data);
                break;
        }
    })
};

var getGiftList = (_) => {

    $.ajax(
        connectSetting(
          'POST',
          serveAPHost + '/mockRoute/mgm/getGiftList',
          JSON.stringify({ GIFT_ID: giftId })
        )
    ).done((data) => {
        switch (data.status) {
            case 0:
                if (giftId === undefined || giftId === ""){
        
                    document.getElementById("giftList").textContent="";
        
                    data.dataList.forEach(a => {
                        $( '#giftList' ).append(
                            '<div class="presentCard" onClick="gotoPointDetail(' + a.GIFT_ID + ');">' +
                                '<div class="presentCard-img">' +
                                    '<img src="' + a.GIFT_IMG_LINK_PATH + '" alt="" />' +
                                '</div>' +
                                '<div class="p-2 presentCard-text">' +
                                    '<h6 class="mb-2">' + a.GIFT_NAME + '</h6>' +
                                    '<span class="mb-2 mr-2 point">' + a.GIFT_SPEND_POINT + '</span><span>點</span>' +
                                '</div>' +
                            '</div>'
                        );
                    });
                } else {
        
                    document.getElementById("giftDetail").textContent="";
        
                    data.dataList.forEach(a => {
                        $( '#giftDetail' ).append(
                            '<div class="container mb-1">' +
                                '<div class="row no-margin kgi-box-001 exchangeApply">' +
                                    '<div class="col-12">' +
                                        '<div class="row">' +
                                            '<div class="col">' +
                                                '<img class="w-100" src="' + a.GIFT_IMG_LINK_PATH + '" alt="" />' +
                                            '</div>' +
                                            '<div class="col d-flex align-items-center">' +
                                                '<div>' +
                                                    '<h5>' + a.GIFT_NAME + '</h5>' +
                                                    '<h5><span class="mr-2" style="color: orange" id="pointSpend">' + a.GIFT_SPEND_POINT + '</span><span>點</span></h5>' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="container">' +
                                '<div class="row no-margin kgi-box-001 exchangeApply">' +
                                    '<div class="col-12">' +
                                        '<div>' +
                                            '<h5 class="kgi-color">兌換說明</h5>' +
                                            '<p>' +
                                                a.GIFT_DESC +
                                            '</p>' +
                                        '</div>' +
                                        '<div class="mb-4">' +
                                            '<h5 class="kgi-color">注意事項</h5>' +
                                            '<p>' +
                                                a.GIFT_NOTES +
                                            '</p>' +
                                        '</div>' +
                                        '<div class="d-flex justify-content-end countInput mb-3">' +
                                            '<i class="fas fa-minus" ' +
                                            'style="font-size: 30px;margin-right: 15px;margin-top: 5px;' + 
                                                'color: #3287BC;-webkit-text-stroke: 2px white; cursor: pointer;" ' +
                                                'onClick="reduceGift()"></i>' +
                                            '<input type="number" name="applyCount" id="applyCount" value="0" min="1" />' +
                                            '<i class="fas fa-plus" ' +
                                            'style="font-size: 30px;margin-left: 15px;margin-top: 5px;' + 
                                                'color: #3287BC;-webkit-text-stroke: 2px white; cursor: pointer;" ' +
                                                'onClick="addGift()"></i>' +
                                        '</div>' +
                                        '<h5 class="d-flex justify-content-end">' +
                                            '共計 <span class="mx-2" style="color: orange" id="applyCountPoint">0</span>點' +
                                        '</h5>' +
                                    '</div>' +
                                '</div>' +
                            '</div>'
                        );
                    });
                }
                break;
            default:
                showMessage(data);
                break;
        }
    })
};

var reduceGift = (_) => {
    let applyCount = document.getElementById("applyCount");
    let result = parseInt(applyCount.value) - 1;
    if(result < 0){
        return;
    }

    applyCount.value = result;

    let pointSpend = parseInt(document.getElementById("pointSpend").textContent);
    let applyCountPoint = document.getElementById("applyCountPoint");
    applyCountPoint.textContent = result * pointSpend;
};

var addGift = (_) => {
    let applyCount = document.getElementById("applyCount");
    let result = parseInt(applyCount.value) + 1;
    if(result > 10){
        return;
    }

    applyCount.value = result;

    let pointSpend = parseInt(document.getElementById("pointSpend").textContent);
    let applyCountPoint = document.getElementById("applyCountPoint");
    applyCountPoint.textContent = result * pointSpend;
};



var applyGift = (_) => {

    let applyAmt = document.getElementById("applyCountPoint").textContent;

    if(applyAmt === "0"){
        let data = { message: '兌換數量需大於0' };
        showMessage(data)
        return;
    }

    $.ajax(
        connectSetting(
          'POST',
          serveAPHost + '/mockRoute/mgm/applyGift',
          JSON.stringify({ ER_ID: cust_id, GIFT_ID: giftId, APPLY_AMT: applyAmt })
        )
    ).done((data) => {
        switch (data.status) {
            case 0:
                funcSectorChange(13);
                break;
            default:
                showMessage(data);
                break;
        }
    })
};