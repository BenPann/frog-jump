flagStatus = 5

var showMessage = data => {
    $( '#messageModalContent' ).empty();
    $( '#messageModalContent' ).append(data.message);
    $( '#exampleModal' ).modal( 'show' );
}
var afterCloseModel = _ => {
    if (flagStatus === 5) {
        return window.location.reload(false);
    }
}
var funcForgotPassBtnSubmit = _ => {
    $( '.passRequired' ).addClass( 'hidden' );
    $( '#setPass1' ).removeClass( 'inputError' );
    $( '#setPass2' ).removeClass( 'inputError' );

    var a = $( '#setPass1' ).val();
    var b = $( '#setPass2' ).val();
    var cust_id = 'A123456789';

    if ( a === b ) {
        if ( a && b ) {
            $.ajax({
                method: "POST",
                url: "http://localhost:8080/onlineformEX/mockRoute/mgm/forgotPasswordReset",
                contentType: 'application/json',
                data: JSON.stringify({ CUST_ID: cust_id, CUST_PW: b }),
                dataType: 'json',
            }).done( data => {
                switch (data.status) {
                    case 0:
                        showMessage(data);
                        break;
                    default:
                        showMessage(data);
                        break;
                }
            })
        }
    } else {
        return $( '.passRequired' ).removeClass( 'hidden' ) 
            && $( '#setPass1' ).addClass( 'inputError' )
            && $( '#setPass2' ).addClass( 'inputError' );
    }
}

// SECTOR: METHOD
$( "#forgotPassBtnSubmit1" ).on( 'click', funcForgotPassBtnSubmit );
$( "#forgotPassBtnSubmit2" ).on( 'click', funcForgotPassBtnSubmit );
$( '#exampleModal' ).on( 'hidden.bs.modal', afterCloseModel );
