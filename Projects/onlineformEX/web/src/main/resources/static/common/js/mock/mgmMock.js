// SECTOR: PARAMETER
var connectUrl = 'http://localhost:8080/onlineformEX/mockRoute/';


// SECTOR: METHOD
var funcAlpha = _ => {
    $.ajax({
        url: 'http://localhost:8080/onlineformEX/mockRoute/mgm/mockMgmApi'
    }).done(
        data => console.log(data)
    )
}
var funcBeta = _ => {
    console.log('====> Access the funcBeta');
    var id = 'A123456789';
    $.ajax({
        method: 'POST',
        url: 'http://localhost:8080/onlineformEX/mockRoute/mgm/getActivity',
        contentType: 'application/json',
        data: JSON.stringify({CUST_ID: id, ACT_UNIQ_ID: ''}),
        dataType: 'json',
    }).done(
        data => console.log(data)
    )
}
var funcGamma = _ => {
    var id = 'A123456789';
    $.ajax({
        method: 'POST',
        url: 'http://localhost:8080/onlineformEX/mockRoute/mgm/checkMemberDetail',
        contentType: 'application/json',
        data: JSON.stringify({CUST_ID: id, ACT_UNIQ_ID: ''}),
        dataType: 'json',
    }).done(
        data => console.log(data)
    )
}
var funcDelta = _ => {
    var id = 'A123456789';
    $.ajax({
        method: 'POST',
        url: 'http://localhost:8080/onlineformEX/mockRoute/mgm/getConfig',
        contentType: 'application/json',
        data: JSON.stringify({CUST_ID: id, ACT_UNIQ_ID: ''}),
        dataType: 'json',
    }).done(
        data => console.log(data)
    )
}
var funcEpilson = _ => {
    var id = 'A123456789';
    $.ajax({
        method: 'POST',
        url: 'http://localhost:8080/onlineformEX/mockRoute/mgm/getContent',
        contentType: 'application/json',
        data: JSON.stringify({CUST_ID: id, ACT_UNIQ_ID: ''}),
        dataType: 'json',
    }).done(
        data => console.log(data)
    )
}
var funcZeta = _ => {
    var id = 'A123456789';
    $.ajax({
        method: 'POST',
        url: 'http://localhost:8080/onlineformEX/mockRoute/mgm/delectIdPass',
        contentType: 'application/json',
        data: JSON.stringify({CUST_ID: id, PASSWORD: '', EMP_ID: ''}),
        dataType: 'json',
    }).done(
        data => console.log(data)
    )
}



// SECTOR: METHOD
$( '#btnAlpha' ).on( 'click' , funcAlpha );
$( '#btnBeta' ).on( 'click' , funcBeta );
$( '#btnZeta' ).on( 'click', funcZeta );
