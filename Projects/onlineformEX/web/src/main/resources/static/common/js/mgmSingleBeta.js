
// NOTE: RESET

var funcResetInputA = _ => {
    $( '.re1Required' ).addClass( 'hidden' );
    $( '#passRe1' ).removeClass( 'inputError' );
    $( '#resetBtnSubmit1' ).attr( 'disabled', true );
    $( '#resetBtnSubmit2' ).attr( 'disabled', true );
    let Re1 = $( '#passRe1' ).val();
    let Re2 = $( '#passRe2' ).val();
    if (Re1 === '') {
        return $( '.re1Required' ).removeClass( 'hidden' ) && $( '#passRe1' ).addClass( 'inputError' )
    }
    if ( Re2 && funcResetInputCheck() ) {
        $( '#resetBtnSubmit1' ).removeAttr( 'disabled' );
        $( '#resetBtnSubmit2' ).removeAttr( 'disabled' );
    }
}

var funcResetInputB = _ => {
    $( '.re2Required' ).addClass( 'hidden' );
    $( '#passRe2' ).removeClass( 'inputError' );
    $( '#resetBtnSubmit1' ).attr( 'disabled', true );
    $( '#resetBtnSubmit2' ).attr( 'disabled', true );
    let Re1 = $( '#passRe1' ).val();
    let Re2 = $( '#passRe2' ).val();
    if (Re2 === '') {
        return $( '.re2Required' ).removeClass( 'hidden' ) && $( '#passRe2' ).addClass( 'inputError' )
    }
    if ( Re1 && funcResetInputCheck() ) {
        $( '#resetBtnSubmit1' ).removeAttr( 'disabled' );
        $( '#resetBtnSubmit2' ).removeAttr( 'disabled' );
    }
}

var funcMGMSingleResetInit = userIdentify => {
    $( '#passRe1' ).on( 'focusout', funcResetInputA );
    $( '#passRe2' ).on( 'focusout', funcResetInputB );
    $( '#agreement' ).on( 'click', funcResetCheckBox );
    employeeSector(userIdentify);
}

var funcResetSendMail = data => {
    $.ajax(
        // connectSetting('POST', 'http://172.31.1.116:3000/sendSMS/', JSON.stringify({billDepart: 9600, phoneNumber: data.CUST_PHONE, message: '文案'})))
        {
            method: 'POST',
            url: 'http://172.31.1.116:3000/sendSMS/',
            contentType: 'application/json',
            crossDomain: true,
            headers: {"Access-Control-Allow-Headers": "*"},
            data: JSON.stringify({billDepart: 9600, phoneNumber: data.CUST_PHONE, message: '密碼變更成功！歡迎加入中壽貴賓推薦活動。'}),
            dataType: 'json',
            beforeSend: function() {
                $("#loader").show();
            },
            complete: function() {
                $("#loader").hide();
            },
            error: function() {
                funcShowLog('SEND MAIL FAILED', data)
                funcSectorChange(2);
            }
        })
    .done( data => {
            funcShowLog('funcResetSendMail', data);
            funcSectorChange(2);
        })
}

var funcResetInputCheck = _ => {
    return $( '#agreement:checked' ).length > 0 ? true : false;
}

var funcResetCheckBox = _ => {
    $( '#resetBtnSubmit1' ).attr( 'disabled', true );
    $( '#resetBtnSubmit2' ).attr( 'disabled', true );
    let Re1 = $( '#passRe1' ).val();
    let Re2 = $( '#passRe2' ).val();
    if ( Re1 && Re2 && funcResetInputCheck() ) {
        $( '#resetBtnSubmit1' ).removeAttr( 'disabled' );
        $( '#resetBtnSubmit2' ).removeAttr( 'disabled' );
    }
}

var employeeSector = userIdentify => {
    $( '#resetBtnSubmit1' ).attr( 'disabled', true );
    $( '#resetBtnSubmit2' ).attr( 'disabled', true );
    $( "#agreement" ).prop( "checked", false );
    // $( '#departNum1' ).val('CL');
    if (userIdentify === 1) {
        $( '#employee1' ).addClass( 'hidden' );
        $( '#employee2' ).addClass( 'hidden' );
    } else {
        // $( '#departNum1' ).val('CL');
        $( '#employee1' ).removeClass( 'hidden' );
        $( '#employee2' ).removeClass( 'hidden' );
    }
}
