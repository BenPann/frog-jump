// SECTOR: COMMON
var funcShowLog = (name, ...data) => {
    var message = '====> ';
    data.forEach(
        e => message + '%s, '
    )
    console.log( message, name, ...data);
}


var eventFire = (el, etype) => {
    if (el.fireEvent) {
        el.fireEvent('on' + etype);
    } else {
        var evObj = document.createEvent('Events');
        evObj.initEvent(etype, true, false);
        el.dispatchEvent(evObj);
    }
}


// SECTOR: METHOD
var howToFilter = params => {
    return params.ACT_UNIQ_ID == belongAct && params.PROD_TYPE == prodType;
}


var savePicLocalAs = (uri, filename) => {
    var link = document.createElement('a');
    if (typeof link.download === 'string') {
        link.href = uri;
        link.download = filename;
        // Firefox requires the link to be in the body
        document.body.appendChild(link);
        // simulate click
        link.click();
        //remove the link when done
        document.body.removeChild(link);
    } else {
        window.open(uri);
    }
}

var onChevron = _ => {
    var imgid = document.getElementById("imgId");
    if (imgid.src !== 'assets/image/icon/03-chevronup.png'){
        imgid.src = 'assets/image/icon/03-chevronup.png';
    }else {
        imgid.src = 'assets/image/icon/04-chevrondown.png';
    }
}

var downloadPic = _ => {
    // $("#qrcodediv").show();
    // $("#newqrdiv").hide();
    // $('#qrcodediv').css('width',$('#qrcodediv').innerWidth()).removeClass('col-12 col-md-5');

    console.log('====> Access the downloadPic().... ');

    // html2canvas($("#newqrdiv")[0]).then(function(canvas) {
    // html2canvas($("#qrcodediv")).then(function(canvas) {
    html2canvas(document.querySelector("#qrcodediv")).then( canvas => {

        console.log('====> Access the downloadPic() PART 2.... ');

        // TEST:
        document.body.appendChild(canvas);
        var imgname = '<%=job.getString("shortURL").substring(job.getString("shortURL").indexOf("entry")+6)%>';
        // $('#qrcodediv').addClass('col-12 col-md-5').css('width','');

        console.log(canvas.msToBlob);
        if (canvas.msToBlob) { 
            var blob = canvas.msToBlob();
            window.navigator.msSaveBlob( blob, imgname + '.png');
        } else {
            // $("#qrcodediv").hide();
            // $("#newqrdiv").show();
            // var $div = $("#newqrdiv");
            // $div.empty();
            // $("<img />", { src: canvas.toDataURL("image/png") }).appendTo($div).css("width", "105%");
            if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
                Modal.showModal("訊息","若無法下載QR code 請長按圖片後儲存或另外截圖使用");
            } else {
                savePicLocalAs(canvas.toDataURL(), imgname + '.png');
            }
        }
    });
}


var copyURL = url => {
    var $temp = $("<input>");
    $("body").append($temp);
    // $temp.val($('#shortURL').text()).select();
    $temp.val(url).select();
    var el = $temp.get(0);
    var editable = el.contentEditable;
    var readOnly = el.readOnly;
    el.contentEditable = true;
    el.readOnly = false;
    var range = document.createRange();
    range.selectNodeContents(el);
    var sel = window.getSelection();
    sel.removeAllRanges();
    el.setSelectionRange(0, 999999);
    el.contentEditable = editable;
    el.readOnly = readOnly;

    document.execCommand('copy');
    $temp.remove();    
}


var genQRCodeForUrlMk2 = (inUrl, index) => {
    console.log('====>> Access the genQRCodeForUrlMk2');
    funcShowLog('param', inUrl, index);

    var G_QR_CODE_SIZE = 160;
    var targetId = '#qrcodeTable_' + index;
    $( targetId ).empty();

    jQuery( targetId ).qrcode({
        width	: G_QR_CODE_SIZE,
        height	: G_QR_CODE_SIZE,
        text	: inUrl
    });

    var canvas = $( targetId + ' canvas');
    console.log(canvas);
    var img = canvas.get(0).toDataURL("image/png");
}


var switchProdName = prodId => {
    switch (prodId) {
        case 'RPL':         return '循環信貸';
        case 'HL':          return '房貸';
        case 'DGT3':        return '數位存款帳戶';
        case 'PL':          return '個人信貸';
        case 'CREDITCARD':  return '信用卡';
    }
}


var connectSetting = (m, u, d) => {
    return {
        method: m,
        url: connectUrl + u,
        contentType: 'application/json',
        data: d,
        dataType: 'json',
        beforeSend: function() {
            $("#loader").show();
        },
        complete: function() {
            $("#loader").hide();
        },
    }
}


var showMessage = data => {
    $( '#messageModalContent' ).empty();
    $( '#messageModalContent' ).append(data.message);
    $( '#exampleModal' ).modal( 'show' );
}


// SECTOR: Parameter
var accessPage = true;  // CONNECT KNOT for single page
var arrAct$ = []; // It's array
var arrShortUrl$ = [];  // It's array
var connectUrl = 'http://localhost:8080/onlineformEX/mockRoute/mgm';


// SECTOR: HTML CREATE METHOD
// STEP: 1. get Activity
var getActivityById = (id) => {
    $.ajax(
        {
            method: 'POST',
            url: connectUrl + '/getActivity',
            contentType: 'application/json',
            data: JSON.stringify({ CUST_ID: id, ACT_UNIQ_ID: '' }),
            dataType: 'json',
            beforeSend: function() {
                $("#loader").show();
            },
            error: function() {
                $("#loader").hide();
                showMessage({ message: 'ERROR' });
            }
        }
    )
    .done(data => {
        switch(data.status) {
            case 0: getActivityCotentByUniqId(data.result); break;
            case 101: showMessage({ message: data.message }); $("#loader").hide(); break;
            default: $("#loader").hide(); break;
        }
    })
}


// STEP: 2. GET what Activity can engage by user id
var getActivityCotentByUniqId = (query) => {
    query.forEach( e => {
        $.ajax({
            method: 'POST',
            url: connectUrl + '/getContent',
            contentType: 'application/json',
            data: JSON.stringify({ CUST_ID: e.CUST_ID, ACT_UNIQ_ID: e.ACT_UNIQ_ID }),
            dataType: 'json',
        })
        .then( data => { 
            arrAct$.push(data);
        })
        .done( _ => {
            if (arrAct$.length == query.length) {
                createActList(arrAct$);
            }}
        )
    })
}


// STEP: 3. GET Detail of Activity by Acitivity id
var createActList = (arrActList$, count = 0) => {
    arrActList$.forEach( (e) => {
        $.ajax({
            method: 'POST',
            url: connectUrl + '/getShortUrl',
            contentType: 'application/json',
            data: JSON.stringify({CUST_ID: '', ACT_UNIQ_ID: e.ACT_UNIQ_ID}),
            dataType: 'json',
        })
        .then( data => {
            count++;
            arrShortUrl$ = arrShortUrl$.concat(data);
        })
        .done( _ => {
            if ( count == arrActList$.length) {
                $("#loader").hide();
                arrShortUrl$.forEach( (e2, i2) => {
                    createProdContent(e2, i2, arrActList$);
                })
            }
        })
    })
}


// STEP: 5. create content
var createProdContent = (e, i, arrActList$) => {
    funcShowLog('createProductContentMk2', [e, i, arrActList$]);

    let act = arrActList$.filter(params => {
        return params.ACT_UNIQ_ID === e.ACT_UNIQ_ID; 
    })[0];

    var fullUrl = 'https://www.kgibank.com/airloan/q/' + e.SHORT_URL;

    $( '#poolAlpha' ).append(
        '<div class="form-group row" id="accordionExampleAlpha_' + i + '" style="background-color: aliceblue; padding: 15px; border-radius: 10px;">' +
            '<div class="col-12">' +
                '<p class="form-title text-left color-primary">' + act.ACT_NAME + '</p>' +
            '</div>' +
            '<div class="form-group row" style="margin-bottom: 10px;">' +
                '<div class="col-12">' +
                    '<button onclick="onChevron()" class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseBeta_' + i + '" aria-expanded="true" aria-controls="collapseBeta_' + i + '">' +
                        '<img id="imgId" src="assets/image/icon/03-chevronup.png" alt="" style=" width:25px;">' +
                        '<label class="col-form-label text-left">了解更多</label>' +
                    '</button>' +
                '</div>' +
            '</div>' +
            '<div class="col-12">' +
                '<div class="form-group row" style="margin-bottom: 10px;">' +
                    '<div class="col-12 kgi-panel-group" id="accordionExampleBeta_' + i + '">' +
                        '<div id="collapseBeta_' + i + '" class="collapseBeta_' + i + ' collapse moveContent" aria-labelledby="Beta" data-parent="#accordionExampleBeta_' + i + '">' +
                            '<div class="panel panel-default">' +
                                '<p>' + act.ACT_DESC + '</p>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="col-12">' +
                '<div class="row">' +
                    '<div class="col-12 h-25 backGround" id="qrcodediv" style="padding:0 15px;">' +
                    '<div class="triangle"></div>' +
                    '<img id="smallicon" src="assets/image/icon/46-QRcode.png"/>' +
                    '<div class="row">' +
                        '<div class="col-6 col-md-4 pl-0">' +
                        '<div class="qrcode row">' +
                            '<div class="col-12" style="text-align:center;">' +
                            '<div id="qrcodeTable_' + i + '" class="qrTable" style="position:relative; width:160px; height:160px; border:0px solid #444; background-color:#e1e1e1;"></div>' +
                            '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-6 col-md-8 p-0">' +
                        '<p style="font-size:12px;padding-top:15px;">推薦申辦</p>' +
                        '<p style="font-size:14px;" class="mb-1">' + switchProdName(e.PROD_TYPE.substr(2)) + '</p>' +
                        '<div style="font-size:12px;">活動: ' + act.ACT_NAME + '</div>' +
                        '<div style="font-size:12px;">推薦人姓名: ' + act.CUST_NAME + '</div>' +
                        '</div>' +
                    '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-12 text-center">' +
                    '<button type="button" onclick="downloadPic()" class="btn btn-kgi-primary">下載QRcode</button>' +
                    '<button type="button" class="btn btn-kgi-primary" onclick="copyURL(\'' + fullUrl + '\')">複製分享網址</button>' +
                    '<a href="https://social-plugins.line.me/lineit/share?url=\'shortUrl\' ">' +
                        '<img src="assets/image/line208.png" class="line-button m-2" />' +
                    '</a>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="col-12 col-sm-12">' +
                '<img src="assets/image/icon/33-period.png" alt="" style=" width:25px;">' +
                '<span>2020/6/1-2020/7/31</span>' +
            '</div>' +
        '</div>'
    )

    // NOTE: Create shortUrl
    genQRCodeForUrlMk2(fullUrl, i);
}


// SECTOR: INIT:
var funcSectorChange = _ => {
    if (accessPage) {
        getActivityById('A123456789');
    }
}


$( document ).ready( funcSectorChange );

