var dataReceive;
var userIdentify;
var loginCaptchaChk;

var pageOnInit = _ => {
    $( '.SectADropContent' ).removeClass( 'hidden' );
    $( '.SectADropLabel' ).removeClass( 'hidden' );
    $( '#inlineRadio1' ).prop( 'checked', true );
    $( '#sectAselect' ).val('0');
    $( '#inlineRadio1' ).on( 'click', funcLoginDropHideOut );
    $( '#inlineRadio2' ).on( 'click', funcLoginDropHideIn );
    $( '#userid' ).on( 'focusout', funcLoginId );
    $( '#loginPassword' ).on( 'keyup', funcLoginPass );
    $( '#hrefForgot' ).on( 'click', funcLoginHrefForgot );
    $( "button[name*='loginBtnSubmit']" ).on( 'click', funcLoginBtn );
    $( '#loginCaptchaEntry1' ).on( 'focusout', funcLoginCheckCaptcha );
}

var funcLoginDropHideOut = _ => {
    $( '.SectADropLabel' ).removeClass( 'hidden' );
    $( '.SectADropContent' ).removeClass( 'hidden' );
    userIdentify = 0;
}

var funcLoginDropHideIn = _ => {
    $( '.SectADropLabel' ).addClass( 'hidden' );
    $( '.SectADropContent' ).addClass( 'hidden' );
    $( '#sectAselect' ).val('0');
    userIdentify = 1;
}

var funcLoginId = _ => {
    $( '.idRequired' ).addClass( 'hidden' );
    $( '#loginBtnSubmit1' ).attr( 'disabled', true );
    $( '#loginBtnSubmit2' ).attr( 'disabled', true );
    var userid = $( '#userid' ).val();
    let pass = $( '#password' ).val();
    if (userid === '') {
        return $( '.idRequired' ).removeClass( 'hidden' ) && $( '#userid' ).addClass( 'inputError' );
    }
    IdCardNumberCheck(userid)
        ? $( '.idError' ).addClass( 'hidden' ) && $( '#userid' ).removeClass( 'inputError' )
        : $( '.idError' ).removeClass( 'hidden' ) && $( '#userid' ).addClass( 'inputError' );
    if ( pass ) {
        $( '#loginBtnSubmit1' ).removeAttr( 'disabled' );
        $( '#loginBtnSubmit2' ).removeAttr( 'disabled' );
    }
}

var funcLoginPass = _ => {
    $( '.passRequired' ).addClass( 'hidden' );
    $( '#loginPassword' ).removeClass( 'inputError' );
    $( '#loginBtnSubmit1' ).attr( 'disabled', true );
    $( '#loginBtnSubmit2' ).attr( 'disabled', true );
    let userId = $( '#userid' ).val();
    let pass = $( '#loginPassword' ).val();
    if (pass === '') {
        return $( '.passRequired' ).removeClass( 'hidden' )
            && $( '#loginPassword' ).addClass( 'inputError' );
    }
    if ( userId ) {
        $( '#loginBtnSubmit1' ).removeAttr( 'disabled' );
        $( '#loginBtnSubmit2' ).removeAttr( 'disabled' );
    }
}

var funcLoginHrefForgot = _ => {
    funcSectorChange(4);
}

var funcLoginBtn = _ => {
    console.log('====> Activate the button....');
    var a = $( '#userid' ).val();
    var b = $( '#loginPassword' ).val();
    if ( a && b ) {
        $.ajax({
            method: 'POST',
            url: 'http://localhost:8080/onlineformEX/mockRoute/mgm/checkMemberDetail',
            contentType: 'application/json',
            data: JSON.stringify({CUST_ID: a, PASSWORD: b}),
            dataType: 'json',
        }).done( data => {
            switch (data.status) {
                case 0:
                    dataReceive = JSON.parse(data.result);
                    funcSectorChange(3);
                    break;
                case 1:
                    dataReceive = JSON.parse(data.result);
                    funcSectorChange(1);
                    break;
                case 9:
                    showMessage(data);
                    break;
                default:
                    showMessage(data);
                    break;
            }
        })
    }
}

var showMessage = data => {
    $( '#messageModalContent' ).empty();
    $( '#messageModalContent' ).append(data.message);
    $( '#exampleModal' ).modal( 'show' );
}

var IdCardNumberCheck = id => {
    var city = new Array(1, 10, 19, 28, 37, 46, 55, 64, 39, 73, 82, 2, 11, 20, 48, 29, 38, 47, 56, 65, 74, 83, 21, 3, 12, 30);
    id = id.toUpperCase();

    // 使用「正規表達式」檢驗格式
    if (!id.match(/^[A-Z]\d{9}$/) && !id.match(/^[A-Z][A-D]\d{8}$/)) return false;

    else {
        var total = 0;
        if (id.match(/^[A-Z]\d{9}$/)) { //身分證字號號
            //將字串分割為陣列(IE必需這麼做才不會出錯)
            id = id.split('');
            //計算總分
            total = city[id[0].charCodeAt(0) - 65];
            for (var i = 1; i <= 8; i++) {
                total += eval(id[i]) * (9 - i);
            }
        } else { // 外來人口統一證號
            //將字串分割為陣列(IE必需這麼做才不會出錯)
            id = id.split('');
            //計算總分
            total = city[id[0].charCodeAt(0) - 65];
            // 外來人口的第2碼為英文A-D(10~13)，這裡把他轉為區碼並取個位數，之後就可以像一般身份證的計算方式一樣了。
            id[1] = id[1].charCodeAt(0) - 65;
            for (var i = 1; i <= 8; i++) {
                total += eval(id[i]) * (9 - i);
            }
        }
        //補上檢查碼(最後一碼)
        total += eval(id[9]);

        //檢查比對碼(餘數應為0);
        return total % 10 == 0 ? true : false;
    }
};

var funcLoginCreateCaptcha = _ => {
    let captcha1 = new CaptchaMini();
    captcha1.draw(document.querySelector('#loginCaptcha1'), r => {
        loginCaptchaChk = r;
    });
}

var funcLoginCheckCaptcha = _ => {
    $( '.captchaError' ).addClass( 'hidden' );
    $( '#loginCaptcha1' ).removeClass( 'inputError' );
    var captchaInput = $( '#loginCaptcha1' ).val();
    console.log(captchaLoginChk);
    console.log(captchaInput);
    if (captchaLoginChk !== captchaInput) {
        return $( '.captchaError' ).removeClass( 'hidden' ) && 
               $( '#loginCaptcha1' ).addClass( 'inputError' ) &&
               $( '#loginBtnSubmit1' ).attr( 'disabled', true ) &&
               $( '#loginBtnSubmit2' ).attr( 'disabled', true );
    } else {
        $( '#loginBtnSubmit1' ).removeAttr( 'disabled' );
        $( '#loginBtnSubmit2' ).removeAttr( 'disabled' );
    }
}

$( document ).ready( pageOnInit );
