
var secText = 20;
var phoneNum = '0901020304'

var pageOnInit = _ => {
    console.log('===> Access mgmForgotOtp Init');
    sendOtp();
    $( '#phoneNum' ).append( phoneNum.substr(0, 4) + "-" + phoneNum.substr(4, 3) + "-" + phoneNum.substr(7, 3));
}

var sendOtp = _ => {
    $.ajax(
        {
            method: 'POST',
            url: serveAPHost + "/publicApi/validate/sendOTPNoLogin",
            contentType: 'application/json',
            data: JSON.stringify({ phone: '0901020304' }),
            dataType: 'json',
            beforeSend: function() {
                $("#loader").show();
            },
            complete: function() {
                $("#loader").hide();
            },
            error: function() {}
        }
    ).then( data => {
        console.log('then');
        console.log(data)
    })
    .done( _ => {
        console.log('done');
        setResendCountTime(60);
    });
}

var checkOtp = _ => {
    const req = {
        phone: phone,
        txnId: otpData.txnID,
        txnDate: otpData.txnDate,
        sk: otpData.sk,
        otp: otp
    };
    $.ajax(
        {
            method: 'POST',
            url: serveAPHost + "/publicApi/validate/checkOtpNoLogin",
            contentType: 'application/json',
            data: req,
            dataType: 'json',
            beforeSend: function() {
                $("#loader").show();
            },
            complete: function() {
                $("#loader").hide();
            },
            error: function() {}
        }
    ).then(
        data => {
            console.log('then');
            console.log(data)
        }
    ).done(
        data => {
            console.log('done');
            console.log(data);
        }
    );
}

var sendSMS = _ => {
    secText === 0 ? sendOtp() : '';
}

var setResendCountTime = (tempT) => {
    var clock = setInterval( () => {
        tempT !== 0 ? tempT-- : clearInterval(clock);
        secText = tempT;
        $( '#countdown' ).empty();
        $( '#countdown' ).append( secText );
    }, 1000);
}
 

$( '#forgotOTPInput' ).on( 'keyup', e => {} )
$( '#reSendOtp' ).on( 'click', sendSMS );
$( '#forgotOtpBtnSubmit1' ).on( 'click', checkOtp )

$( document ).ready( pageOnInit );

