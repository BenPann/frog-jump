var dataReceive = {
    CHANNEL_ID: "",
    CUST_BIRTH_4: "0827",
    CUST_EMAIL: "theMockMan@nonexist.com",
    CUST_EMP_ID: "",
    CUST_HAS_SET_PW: "N",
    CUST_ID: "A123456789",
    CUST_NAME: "The mock man",
    CUST_PHONE: "0901002003",
    CUST_PW: "123456",
    CUST_RIGSTER_FLAG: "N",
    CUST_UNIT_ID: "",
    IMPORT_TIME: "2020-08-01 00:00:00.0",
    PW_SETTING_TIME: "2020-09-10 17:47:01.78",
    REGISTER_TIME: "1900-01-01 00:00:00.0",
    STATUS: "00",
};
var userIdentify = 1;

var funcResetInputA = _ => {
    $( '.re1Required' ).addClass( 'hidden' );
    $( '#passRe1' ).removeClass( 'inputError' );
    $( '#resetBtnSubmit1' ).attr( 'disabled', true );
    $( '#resetBtnSubmit2' ).attr( 'disabled', true );
    let Re1 = $( '#passRe1' ).val();
    let Re2 = $( '#passRe2' ).val();
    if (Re1 === '') {
        return $( '.re1Required' ).removeClass( 'hidden' ) && $( '#passRe1' ).addClass( 'inputError' )
    }
    if ( Re2 && inputCheck() ) {
        $( '#resetBtnSubmit1' ).removeAttr( 'disabled' );
        $( '#resetBtnSubmit2' ).removeAttr( 'disabled' );
    }
}
var funcResetInputB = _ => {
    $( '.re2Required' ).addClass( 'hidden' );
    $( '#passRe2' ).removeClass( 'inputError' );
    $( '#resetBtnSubmit1' ).attr( 'disabled', true );
    $( '#resetBtnSubmit2' ).attr( 'disabled', true );
    let Re1 = $( '#passRe1' ).val();
    let Re2 = $( '#passRe2' ).val();
    if (Re2 === '') {
        return $( '.re2Required' ).removeClass( 'hidden' ) && $( '#passRe2' ).addClass( 'inputError' );
    }
    if ( Re1 && inputCheck() ) {
        $( '#resetBtnSubmit1' ).removeAttr( 'disabled' );
        $( '#resetBtnSubmit2' ).removeAttr( 'disabled' );
    }
}
var funcResetBtn = _ => {
    console.log("====> Access the funcResetBtn");
    var pass1 = $( "#passRe1" ).val();
    var pass2 = $( "#passRe2" ).val();
    var emp1 = $( "#employNum1" ).val();

    if (pass1 && pass2) {
        if (pass1 !== pass2) {
            document.getElementById("passwordError").removeAttribute("style");
            document.getElementById("passRe1").setAttribute("style", "border: 1px solid red;");
            document.getElementById("passRe2").setAttribute("style", "border: 1px solid red;");
            return;
        }
        $.ajax({
            method: 'POST',
            url: "http://localhost:8080/onlineformEX/mockRoute/mgm/resetPassword",
            contentType: 'application/json',
            data: JSON.stringify({CUST_ID: dataReceive.CUST_ID, PASSWORD: pass1, EMP_ID: emp1}),
            dataType: 'json',
        }).done( data => {
            // TODO: send SMS mail
            switch (data.status) {
                case 0:
                    funcSectorChange(2);                    
                    // funcSendMail(data);
                    break;
                default:
                    showMessage(data);
                    break;
            }
        })
    }
}
var funcSendMail = data => {
    $.ajax({
        method: 'POST',
        url: "http://172.31.1.116:3000/sendSMS/",
        contentType: 'application/json',
        data: JSON.stringify({billDepart: 9600, phoneNumber: data.CUST_PHONE, message: '密碼變更成功！歡迎加入中壽貴賓推薦活動。'}),
        dataType: 'json',
    }).done( data => {
        console.log(data);
        funcSectorChange(2);
    })
}
var pageOnInit = _ => {
    console.log( "====> ready!" );
    console.log(dataReceive);
    $( '#resetBtnSubmit1' ).attr( 'disabled', true );
    $( '#resetBtnSubmit2' ).attr( 'disabled', true );
    $( "#agreement" ).prop( "checked", false );
    // move to funcSectorChange when mgmSingle
    if (userIdentify === 1) {
        $( '#employee1' ).addClass( 'hidden' );
        $( '#employee2' ).addClass( 'hidden' );
    } else {
        // $( '#departNum1' ).val('CL');
        $( '#employee1' ).removeClass( 'hidden' );
        $( '#employee2' ).removeClass( 'hidden' );
    }
}
var inputCheck = _ => {
    return $( '#agreement:checked' ).length > 0
        ? true
        : false;
}
var funcCheckBox = _ => {
    $( '#resetBtnSubmit1' ).attr( 'disabled', true );
    $( '#resetBtnSubmit2' ).attr( 'disabled', true );
    let Re1 = $( '#passRe1' ).val();
    let Re2 = $( '#passRe2' ).val();
    if ( Re1 && Re2 && inputCheck() ) {
        $( '#resetBtnSubmit1' ).removeAttr( 'disabled' );
        $( '#resetBtnSubmit2' ).removeAttr( 'disabled' );
    }
}

// INIT:
$( document ).ready( pageOnInit );
// $( '#passRe1' ).on( 'focusout', funcResetInputA );
// $( '#passRe2' ).on( 'focusout', funcResetInputB );
$( '#agreement' ).on( 'click', funcCheckBox );
$( "button[name*='resetBtnSubmit']" ).on( 'click', funcResetBtn );

