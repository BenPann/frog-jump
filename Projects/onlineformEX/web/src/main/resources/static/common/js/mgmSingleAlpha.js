
// NOTE: LOGIN

var userIdentify;
var captchaLoginChk;


var funMGMSingleLoginInit = _ => {
    $( '.SectADropContent' ).removeClass( 'hidden' );
    $( '.SectADropLabel' ).removeClass( 'hidden' );
    $( '#inlineRadio1' ).prop( 'checked', true );
    $( '#sectAselect' ).val('0');
    // CALL FUNCTION
    $( '#inlineRadio1' ).on( 'click', funcLoginDropHideOut );
    $( '#inlineRadio2' ).on( 'click', funcLoginDropHideIn );
    $( '#sectAselect' ).on( 'change', funcLoginSelect );
    $( '#loginUserid' ).on( 'keyup', funcLoginUserId );
    $( '#loginUserid' ).on( 'focusout', funcLoginUserIdMk2 );
    $( '#loginPassword' ).on( 'keyup', funcLoginPass );
    $( '#loginCaptchaEntry1' ).on( 'keyup', funcLoginCheckCaptcha );
    $( '#loginCaptchaEntry1' ).on( 'focusout', funcLoginCheckCaptcha );

    $( '#loginUserid' ).on( 'focusin', funcSwitchButtonSubmit(false) );
    $( '#loginPassword' ).on( 'focusin', funcSwitchButtonSubmit(false) );
    $( '#loginCaptchaEntry1' ).on( 'focusin', funcSwitchButtonSubmit(false) );

    $( '#hrefForgot' ).on( 'click', funcLoginHrefForgot );
    funcLoginCreateCaptcha();
}

var funcLoginDropHideOut = _ => {
    $( '.SectADropLabel' ).removeClass( 'hidden' );
    $( '.SectADropContent' ).removeClass( 'hidden' );
    userIdentify = 0;
}

var funcLoginDropHideIn = _ => {
    $( '.SectADropLabel' ).addClass( 'hidden' );
    $( '.SectADropContent' ).addClass( 'hidden' );
    $( '#sectAselect' ).val('0');
    userIdentify = 1;
}

var funcLoginSelect = _ =>{
    return $( '#sectAselect' ).val() === '0'
      ? funcSwitchButtonSubmit(true, 0)
      : funcSwitchButtonSubmit(false) ;
}

var funcLoginUserId = _ => {
    let userid = $( '#loginUserid' ).val();
    if (userid === '') {
        return funcSwitchButtonSubmit(true, 11);
    }
    if (userid.length === 10) {
        console.log('test   ', IdCardNumberCheck(userid));
        if(IdCardNumberCheck(userid)){
            checkMemberId(userid);
            return funcSwitchButtonSubmit(false);
        } else {
            return funcSwitchButtonSubmit(true, 1);
        }
    }
}

var funcLoginUserIdMk2 = _ => {
    let userid = $( '#loginUserid' ).val();
    if (userid === '') {
        return funcSwitchButtonSubmit(true, 11);
    }
    return IdCardNumberCheck(userid)
        ? funcSwitchButtonSubmit(false)
        : funcSwitchButtonSubmit(true, 1);
}

var funcLoginPass = _ => {
    return $( '#loginPassword' ).val() === '' 
                ? funcSwitchButtonSubmit(true, 2)
                : funcSwitchButtonSubmit(false) ;
}

var funcLoginHrefForgot = _ => {
    funcSectorChange(4);
}

var funcLoginCreateCaptcha = _ => {
    let captcha1 = new CaptchaMini();
    captcha1.draw(document.querySelector('#loginCaptcha1'), r => {
        captchaLoginChk = r;
    });
}

var funcLoginCheckCaptcha = _ => {
    var captchaInput = $( '#loginCaptchaEntry1' ).val();
    if (captchaInput.length === 6) {
        return captchaLoginChk === captchaInput
                    ? funcSwitchButtonSubmit(false)
                    : funcSwitchButtonSubmit(true, 3);
    }
}

var funcSwitchButtonSubmit = (trigger, target) => {
    if (trigger) {
        $( '#loginBtnSubmit1' ).attr( 'disabled', true );
        $( '#loginBtnSubmit2' ).attr( 'disabled', true );
        switch (target) {
            case 0:
                $( '.selectRequired' ).removeClass( 'hidden' );
                $( '#sectAselect' ).addClass( 'inputError' );
                break;
            case 1:
                $( '.idError' ).removeClass( 'hidden' );
                $( '#loginUserid' ).addClass( 'inputError' );
                break;
            case 11:
                $( '.idRequired' ).removeClass( 'hidden' );
                $( '#loginUserid' ).addClass( 'inputError' );
                break;
            case 2:
                $( '.passRequired' ).removeClass( 'hidden' );
                $( '#loginPassword' ).addClass( 'inputError' );
                break;
            case 3:
                $( '.captchaError' ).removeClass( 'hidden' );
                $( '#loginCaptchaEntry1' ).addClass( 'inputError' );
                break;
        }
    }
    if (!trigger) {
        $( '.selectRequired' ).addClass( 'hidden' );
        $( '#sectAselect' ).removeClass( 'inputError' );
        $( '.idRequired' ).addClass( 'hidden' ); 
        $( '.idError' ).addClass( 'hidden' );
        $( '#loginUserid' ).removeClass( 'inputError' );
        $( '.passRequired' ).addClass( 'hidden' );
        $( '#loginPassword' ).removeClass( 'inputError' );
        $( '.captchaError' ).addClass( 'hidden' );
        $( '#loginCaptcha1' ).removeClass( 'inputError' );

        let sectAselect = $( '#sectAselect' ).val();
        let a = $( '#loginCaptchaEntry1' ).val();
        let b = $( '#loginUserid' ).val();
        let c = $( '#loginPassword' ).val();
        if (a !== '' && b !== '' && c !== '' && sectAselect !== '0') {
            $( '#loginBtnSubmit1' ).removeAttr( 'disabled', true );
            $( '#loginBtnSubmit2' ).removeAttr( 'disabled', true );
        }
    }
}
