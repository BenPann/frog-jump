// SECTOR: COMMON
var IdCardNumberCheck = id => {
    var city = new Array(1, 10, 19, 28, 37, 46, 55, 64, 39, 73, 82, 2, 11, 20, 48, 29, 38, 47, 56, 65, 74, 83, 21, 3, 12, 30);
    id = id.toUpperCase();

    // 使用「正規表達式」檢驗格式
    if (!id.match(/^[A-Z]\d{9}$/) && !id.match(/^[A-Z][A-D]\d{8}$/)) return false;

    else {
        var total = 0;
        if (id.match(/^[A-Z]\d{9}$/)) { //身分證字號號
            //將字串分割為陣列(IE必需這麼做才不會出錯)
            id = id.split('');
            //計算總分
            total = city[id[0].charCodeAt(0) - 65];
            for (var i = 1; i <= 8; i++) {
                total += eval(id[i]) * (9 - i);
            }
        } else { // 外來人口統一證號
            //將字串分割為陣列(IE必需這麼做才不會出錯)
            id = id.split('');
            //計算總分
            total = city[id[0].charCodeAt(0) - 65];
            // 外來人口的第2碼為英文A-D(10~13)，這裡把他轉為區碼並取個位數，之後就可以像一般身份證的計算方式一樣了。
            id[1] = id[1].charCodeAt(0) - 65;
            for (var i = 1; i <= 8; i++) {
                total += eval(id[i]) * (9 - i);
            }
        }
        //補上檢查碼(最後一碼)
        total += eval(id[9]);

        //檢查比對碼(餘數應為0);
        return total % 10 == 0 ? true : false;
    }
}

var showMessage = data => {
    $( '#messageModalContent' ).empty();
    $( '#messageModalContent' ).append(data.message);
    $( '#exampleModal' ).modal( 'show' );
}

// SECTOR: PARAMETER
var dataReceive;
var captchaChk;

// SECTOR: METHOD
var funcCreateCaptcha = _ => {
    let captcha1 = new CaptchaMini();
    captcha1.draw(document.querySelector('#captcha1'), r => {
        captchaChk = r;
    });
}

var funcForgotLogin1 = _ => {
    $( '.idRequired' ).addClass( 'hidden' );
    var forgot_cust_id = $( '#forgotLoginEntry1' ).val();
    if (forgot_cust_id === '') {
        return $( '.idRequired' ).removeClass( 'hidden' ) && $( '#forgotLoginEntry1' ).addClass( 'inputError' );
    }
    IdCardNumberCheck(forgot_cust_id)
        ? $( '.idError' ).addClass( 'hidden' ) && $( '#forgotLoginEntry1' ).removeClass( 'inputError' )
        : $( '.idError' ).removeClass( 'hidden' ) && $( '#forgotLoginEntry1' ).addClass( 'inputError' );
}

var funcForgotLogin3 = _ => {
    $( '.captchaError' ).addClass( 'hidden' );
    $( '#forgotLoginEntry3' ).removeClass( 'inputError' );
    var captchaInput = $( '#forgotLoginEntry3' ).val();
    if (captchaChk !== captchaInput) {
        return $( '.captchaError' ).removeClass( 'hidden' ) && $( '#forgotLoginEntry3' ).addClass( 'inputError' );
    }
}

var funcForgotLoginBtnSubmit = _ => {
    var a = $( '#forgotLoginEntry1' ).val();
    var b = $( '#forgotLoginEntry2' ).val();
    var c = captchaChk;
    if (a && b) {
        $.ajax({
            method: "POST",
            url: "http://localhost:8080/onlineformEX/mockRoute/mgm/forgotPassword",
            contentType: 'application/json',
            data: JSON.stringify({ CUST_ID: a, BIRTH_4: b }),
            dataType: 'json',
        }).done( data => {
            switch (data.status) {
                case 0:
                    console.log(data);
                    dataReceive = data.result;
                    // funcSectorChange(5);
                    break;
                case 1:
                    console.log('Password Error');
                    showMessage(data);
                    break;
                case 2:
                    console.log('Account Not Access');
                    showMessage(data);
                    break;
                case 4:
                    console.log('Too Many time error input');
                    showMessage(data);
                    break;
                case 9:
                    console.log('Not Found');
                    showMessage(data);
                    break;
                default:
                    break;
            }}
        )
    }
}

var pageOnInit = _ => {
    funcCreateCaptcha();
}

$( '#forgotLoginEntry1' ).on( 'focusout', funcForgotLogin1 );
$( '#forgotLoginEntry3' ).on( 'focusout', funcForgotLogin3 );
$( '#forgotLoginBtnSubmit1' ).on( 'click' , funcForgotLoginBtnSubmit );
$( '#forgotLoginBtnSubmit2' ).on( 'click' , funcForgotLoginBtnSubmit );
$( document ).ready( pageOnInit );

