
// NOTE: ACTIVITY

// SECTOR: PARAMETER
var accessPage = true;
var arrAct$ = [];
var arrShortUrl$ = [];



// SECTOR: METHOD
var eventFire = (el, etype) => {
    if (el.fireEvent) {
        el.fireEvent('on' + etype);
    } else {
        var evObj = document.createEvent('Events');
        evObj.initEvent(etype, true, false);
        el.dispatchEvent(evObj);
    }
}

var funcMGMSingleDeltaInit = (cust_id) => {
    // DEPR:
    // getActivityById(cust_id);
    // MODE: New version with fixed activity
    getActivityCotentByUniqIdMk2(cust_id);
}

var howToFilter = params => {
    return params.ACT_UNIQ_ID == belongAct && params.PROD_TYPE == prodType;
}

var savePicLocalAs = (uri, filename) => {
    var link = document.createElement('a');
    if (typeof link.download === 'string') {
        link.href = uri;
        link.download = filename;
        // Firefox requires the link to be in the body
        document.body.appendChild(link);
        // simulate click
        link.click();
        //remove the link when done
        document.body.removeChild(link);
    } else {
        window.open(uri);
    }
}

var downloadPic = _ => {
    // $("#qrcodediv").show();
    // $("#newqrdiv").hide();
    // $('#qrcodediv').css('width',$('#qrcodediv').innerWidth()).removeClass('col-12 col-md-5');

    console.log('====> Access the downloadPic().... ');

    // html2canvas($("#newqrdiv")[0]).then(function(canvas) {
    // html2canvas($("#qrcodediv")).then(function(canvas) {
    html2canvas(document.querySelector("#qrcodediv")).then( canvas => {

        console.log('====> Access the downloadPic() PART 2.... ');

        document.body.appendChild(canvas);
        var imgname = 'imageNameShortUrl';
        // $('#qrcodediv').addClass('col-12 col-md-5').css('width','');

        console.log(canvas.msToBlob);
        if (canvas.msToBlob) { 
            console.log('====> Access the downloadPic() PART 3.... ');
            var blob = canvas.msToBlob();
            window.navigator.msSaveBlob( blob, imgname + '.png');
        } else {
            console.log('====> Access the downloadPic() PART 4.... ');
            // $("#qrcodediv").hide();
            // $("#newqrdiv").show();
            // var $div = $("#newqrdiv");
            // $div.empty();
            // $("<img />", { src: canvas.toDataURL("image/png") }).appendTo($div).css("width", "105%");
            if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
                Modal.showModal("訊息","若無法下載QR code 請長按圖片後儲存或另外截圖使用");
            } else {
                savePicLocalAs(canvas.toDataURL(), imgname + '.png');
            }
        }
        document.body.removeChild(canvas);
    });
}

var copyURL = url => {
    var $temp = $("<input>");
    $("body").append($temp);
    // $temp.val($('#shortURL').text()).select();
    $temp.val(url).select();
    var el = $temp.get(0);
    var editable = el.contentEditable;
    var readOnly = el.readOnly;
    el.contentEditable = true;
    el.readOnly = false;
    var range = document.createRange();
    range.selectNodeContents(el);
    var sel = window.getSelection();
    sel.removeAllRanges();
    el.setSelectionRange(0, 999999);
    el.contentEditable = editable;
    el.readOnly = readOnly;

    document.execCommand('copy');
    $temp.remove();    
}

var genQRCodeForUrlMk2 = (inUrl, index) => {
    console.log('====>> Access the genQRCodeForUrlMk2');
    funcShowLog('param', inUrl, index);

    var G_QR_CODE_SIZE = 160;
    var targetId = '#qrcodeTable_' + index;
    var targetId2 = '#qrcodeTable_' + index + "_2";
    $( targetId ).empty();
    $( targetId2 ).empty();

    jQuery( targetId ).qrcode({
        width	: G_QR_CODE_SIZE,
        height	: G_QR_CODE_SIZE,
        text	: inUrl
    });

    jQuery( targetId2 ).qrcode({
        width   : 100,
        height  : 100,
        text    : inUrl
    })

    var canvas = $( targetId + ' canvas');
    var canvas_2 = $( targetId2 + ' canvas');
    console.log(canvas);
    console.log(canvas_2);
    var img = canvas.get(0).toDataURL("image/png");
    var img = canvas_2.get(0).toDataURL("image/png");
}

var switchProdName = prodId => {
    switch (prodId) {
        case 'RPL':         return '循環信貸';
        case 'HL':          return '房貸';
        case 'DGT3':        return '數位存款帳戶';
        case 'PL':          return '個人信貸';
        case 'CREDITCARD':  return '信用卡';
    }
}


// SECTOR: HTML CREATE METHOD
// STEP: 1. get Activity
var getActivityById = (id) => {
    let req = JSON.stringify({
        CUST_ID: id, 
        ACT_UNIQ_ID: '',
    });
    $.ajax(
        {
            method: 'POST',
            url: serveAPHost + '/mockRoute/mgm/getActivity',
            contentType: 'application/json',
            data: req,
            dataType: 'json',
            beforeSend: function() {
                $("#loader").show();
            },
        }
    )
    .done( data => {
        switch(data.status) {
            case 0: getActivityCotentByUniqId(data.result); break;
            case 101: showMessage({ message: data.message }); $("#loader").hide(); break;
            default: $("#loader").hide(); break;
        }
    })
}


// STEP: DEPR: 2. GET what Activity can engage by user id
var getActivityCotentByUniqId = (query) => {
    query.forEach( e => {
        let req = JSON.stringify({
            CUST_ID: e.CUST_ID, 
            ACT_UNIQ_ID: e.ACT_UNIQ_ID
        });
        $.ajax(
            {
                method: 'POST',
                url: serveAPHost + '/mockRoute/mgm/getContent',
                contentType: 'application/json',
                data: req,
                dataType: 'json',
            } 
        ).then( data => { 
            arrAct$.push(data);
        }).done( _ => {
            if (arrAct$.length == query.length) {
                createActList(arrAct$);
            }}
        )
    })
}


// STEP: 2.1 New version
var getActivityCotentByUniqIdMk2 = (custId) => {
    let req = JSON.stringify({
        CUST_ID: custId, 
        ACT_UNIQ_ID: ''
    });
    $.ajax(
        {
            method: 'POST',
            url: serveAPHost + '/mockRoute/mgm/getContent',
            contentType: 'application/json',
            data: req,
            dataType: 'json',
        } 
    ).then( data => { 
        arrAct$ = data;
    }).done( _ => {
        createActListMk2(arrAct$, custId);
    })
}


// STEP: DEPR: 3. GET Detail of Activity by Acitivity id
var createActList = (arrActList$, custId, count = 0) => {
    arrActList$.forEach( (e) => {
        $.ajax({
            method: 'POST',
            url: serveAPHost + '/mockRoute/mgm/getShortUrl',
            contentType: 'application/json',
            data: JSON.stringify({CUST_ID: custId, ACT_UNIQ_ID: e.ACT_UNIQ_ID}),
            dataType: 'json',
        })
        .then( data => {
            count++;
            arrShortUrl$ = arrShortUrl$.concat(data);
            arrShortUrl$ = data;
        })
        .done( _ => {
            if ( count == arrActList$.length) {
                $("#loader").hide();
                arrShortUrl$.forEach( (e2, i2) => {
                    createProdContent(e2, i2, arrActList$);
                })
            }
        })
    })
}


// STEP: 3. GET Detail of Activity by Acitivity id
var createActListMk2 = (arrActList$, custId) => {
    $.ajax({
        method: 'POST',
        url: serveAPHost + '/mockRoute/mgm/getShortUrl',
        contentType: 'application/json',
        data: JSON.stringify({CUST_ID: custId, ACT_UNIQ_ID: ''}),
        dataType: 'json',
    })
    .then( data => {
        arrShortUrl$ = data;
    })
    .done( _ => {
        
        $("#loader").hide();

        let sortArr = ['CREDITCARD', 'DGT3', 'RPL', 'PL', 'HL'];
        let newArr = [];
        
        sortArr.forEach( (e) => {
            arrShortUrl$.forEach( e2 => {
                if (e2.PROD_TYPE === e) {
                    newArr.push(e2);
                }
            })
        })

        newArr.forEach( (e2, i2) => {
            createProdContent(e2, i2, arrActList$);
        })
    })
}


// STEP: 4. CREATE content
var createProdContent = (e, i, arrActList$) => {
    funcShowLog('createProductContentMk2', [e, i, arrActList$]);

    // let act = arrActList$;
    let act = arrActList$.filter(params => {
        return params.ACT_UNIQ_ID === e.ACT_UNIQ_ID; 
    })[0];

    let fullUrl = 'https://www.kgibank.com/airloan/q/' + e.SHORT_URL;
    let lastPart = act.CUST_NAME.length > 2 ? act.CUST_NAME.substr(2, act.CUST_NAME.length-1) : '';
    let anonNam = act.CUST_NAME.substr(0, 1) + 'O' + lastPart;

    $( '#poolAlpha' ).append(
        '<div class="form-group row" id="accordionExampleAlpha_' + i + '" style="background-color: aliceblue; padding: 15px; border-radius: 10px;">' +
            '<div class="col-12">' +
                '<p class="form-title-act text-left color-primary">' +
                '<img src="./assets/image/icon/ICON-' + act.ACT_UNIQ_ID + '.png" style="width: 38px; margin: 0px 10px 10px 0px;"/>' +
                    act.ACT_NAME + 
                '</p>' +
            '</div>' +
            '<div class="form-group row" style="margin-bottom: 10px;">' +
                '<div class="col-12">' +
                    '<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseBeta_' + i + '" aria-expanded="true" aria-controls="collapseBeta_' + i + '" style="box-shadow: none;">' +
                        '<img id="imgId" src="assets/image/icon/04-chevrondown.png" alt="" style=" width:25px;">' +
                        '<label class="col-form-label text-left">了解更多</label>' +
                    '</button>' +
                '</div>' +
            '</div>' +
            '<div class="col-12">' +
                '<div class="form-group row" style="margin-bottom: 10px;">' +
                    '<div class="col-12 kgi-panel-group" id="accordionExampleBeta_' + i + '">' +
                        '<div id="collapseBeta_' + i + '" class="collapseBeta_' + i + ' collapse moveContent" aria-labelledby="Beta" data-parent="#accordionExampleBeta_' + i + '">' +
                            '<div class="panel panel-default">' +
                                '<p>' + act.ACT_DESC + '</p>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="col-12">' +
                '<div class="row qrTable-row">' +
                    '<div class="col-11 h-25 backGround" id="qrcodediv" style="padding:0 15px; margin-left: 22px">' +
                    '<div class="triangle"></div>' +
                    '<img id="smallicon" src="assets/image/icon/46-QRcode.png"/>' +
                    '<div class="row">' +
                        '<div class="col-6 col-md-4 pl-0">' +
                        '<div class="qrcode row">' +
                            '<div class="col-12" style="text-align:center;">' +
                            '<div id="qrcodeTable_' + i + '" class="qrTable" style="position:relative; width:160px; height:160px; border:0px solid #444; background-color:#e1e1e1;"></div>' +
                            '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-6 col-md-8 p-0">' +
                        '<p style="font-size:12px;padding-top:15px;">推薦申辦</p>' +
                        '<p style="font-size:14px;" class="mb-1">' + switchProdName(e.PROD_TYPE) + '</p>' +
                        '<div style="font-size:12px;">活動: ' + act.ACT_NAME + '</div>' +
                        '<div style="font-size:12px;">推薦人姓名: ' + anonNam + '</div>' +
                        '</div>' +
                    '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="row qrTable-mobile-row">' +
                    '<div class="col-11 h-25 backGround" id="qrcodediv" style="padding:0 15px; margin-left: 22px">' +
                    '<div class="triangle"></div>' +
                    '<img id="smallicon" src="assets/image/icon/46-QRcode.png"/>' +
                    '<div class="row">' +
                        '<div class="col-6 col-md-4 pl-0">' +
                        '<div class="qrcode row">' +
                            '<div class="col-12" style="text-align:center;">' +
                            '<div id="qrcodeTable_' + i + '_2" class="qrTable qrTable-mobile"></div>' +
                            '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-6 col-md-8 p-0">' +
                        '<p style="font-size:12px;">推薦申辦</p>' +
                        '<p style="font-size:14px;" class="mb-1">' + switchProdName(e.PROD_TYPE) + '</p>' +
                        '<div style="font-size:12px;">活動: ' + act.ACT_NAME + '</div>' +
                        '<div style="font-size:12px;">推薦人姓名: ' + anonNam + '</div>' +
                        '</div>' +
                    '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-12 text-left">' +
                        '<div class="d-none d-md-inline">' +
                            '<button type="button" onclick="downloadPic()" class="btn btn-kgi-primary">下載QRcode</button>' +
                            '<button type="button" class="btn btn-kgi-primary" onclick="copyURL(\'Hi~推薦凱基銀行信用卡給您，請點選下方連結立即申辦！' + fullUrl + '\')">複製分享網址</button>' +
                        '</div>' +
                        '<button type="button" onclick="downloadPic()" alt="下載QRcode" class="btn d-md-none" style="padding: 0px;"><img src="assets/image/icon/15-package.svg" style="width: 60px" /></button>' +
                        '<button type="button" class="btn d-md-none" alt="複製分享網址" onclick="copyURL(\'Hi~推薦凱基銀行信用卡給您，請點選下方連結立即申辦！' + fullUrl + '\')" style="padding: 0px; margin-right: 6px;"><img src="assets/image/icon/23-documents.svg" style="width: 60px" /></button>' +
                        '<a href="https://social-plugins.line.me/lineit/share?url=Hi~推薦凱基銀行信用卡給您，請點選下方連結立即申辦！' + fullUrl + ' ">' +
                            '<img src="assets/image/line_icon2.png" style="width: 45px" />' +
                        '</a>' +
                        '<span class="fb-share-button" data-href="' + fullUrl + '" data-layout="button_count" data-size="small" style="margin: 10px;">' +
                            '<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">' +
                                '<img src="assets/image/fb_icon.svg" style="width: 45px" />' + 
                            '</a>' +
                        '</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>'
    )

    // NOTE: Create shortUrl
    genQRCodeForUrlMk2(fullUrl, i);
}


