
var secText;
var phoneNum;
var otpData;

var funcMGMSingleOtpInit = phone => {
    funcShowLog('mgmForgotOtp');
    phoneNum = phone.substr(0, 10);
    sendOtp();
    // $( '#phoneNum' ).append( phoneNum.substr(0, 4) + "-" + phoneNum.substr(4, 3) + "-" + phoneNum.substr(7, 3));
    $( '#phoneNum' ).append( phoneNum.substr(0, 4) + "-XXX-" + phoneNum.substr(7, 3));
}

var sendOtp = _ => {
    $.ajax(
        connectSetting('POST', serveAPHost + "/publicApi/validate/sendOTPNoLogin", JSON.stringify({ phone: phoneNum }))
    ).then( data => {
        console.log('then');
        console.log(data);
        otpData = data.result;
    })
    .done( _ => {
        console.log('done');
        setResendCountTime(60);
    });
}

var funcInputOtp = _ => {
    $( '.otpRequired' ).addClass( 'hidden' );
    $( '#forgotOTPInput' ).removeClass( 'inputError' );
}

var funcCheckOtp = _ => {
    let otpVal = $( '#forgotOTPInput' ).val();
    if (otpVal == null || otpVal == undefined || otpVal == '') {
        $( '.otpRequired' ).removeClass( 'hidden' );
        $( '#forgotOTPInput' ).addClass( 'inputError' );
        return 'error';
    }
    const req = {
        phone: phoneNum,
        txnId: otpData.txnID,
        txnDate: otpData.txnDate,
        sk: otpData.sk,
        otp: otpVal
    };
    $.ajax(
        connectSetting('POST', serveAPHost + "/publicApi/validate/checkOTPNoLogin", JSON.stringify({
                phone: phoneNum,
                txnId: otpData.txnID,
                txnDate: otpData.txnDate,
                sk: otpData.sk,
                otp: otpVal
            })
        )
    ).then(
        data => {
            console.log('then');
            console.log(data)
            switch (data.status) {
                case 0:
                    // showMessage(data);
                    funcSectorChange(6);
                    break;
            }
        }
    ).done(
        data => {
            console.log('done');
            console.log(data);
        }
    );
}

var sendSMS = _ => {
    secText === 0 ? sendOtp() : '';
}

var setResendCountTime = (tempT) => {
    var clock = setInterval( () => {
        tempT !== 0 ? tempT-- : clearInterval(clock);
        secText = tempT;
        $( '#countdown' ).empty();
        $( '#countdown' ).append( secText );
    }, 1000);
}
 
$( '#forgotOTPInput' ).on( 'focusout', funcInputOtp )
$( '#reSendOtp' ).on( 'click', sendSMS );
