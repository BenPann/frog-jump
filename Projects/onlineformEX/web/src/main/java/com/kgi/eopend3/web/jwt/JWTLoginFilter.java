package com.kgi.eopend3.web.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.view.LoginView;
import com.kgi.onlineformex.common.dto.view.OFLoginView;

import org.owasp.esapi.ESAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import eu.bitwalker.useragentutils.UserAgent;

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {


    private Logger logger = LoggerFactory.getLogger(this.getClass());


    public JWTLoginFilter(String url, AuthenticationManager authManager) {
        super(new AntPathRequestMatcher(url));

        logger.info("======>> #FILTER Access the JWTLoginFilter()....");

        setAuthenticationManager(authManager);
    }


    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
            throws AuthenticationException, IOException, ServletException {

        logger.info("======>> #FILTER Access the attemptAuthentication()....");

        // 登入資訊解密 有需要的話在此做
        // JSON反序列化成 AccountCredentials
        OFLoginView loginAuth = setLoginViewByRequestMk2(req);

        UsernamePasswordAuthenticationToken uToken = new UsernamePasswordAuthenticationToken(loginAuth, null);
        Authentication auth = getAuthenticationManager().authenticate(uToken);
        // 返回驗證TOKEN
        return auth;
    }


    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
            Authentication auth) throws IOException, ServletException {

        logger.info("======>> #FILTER Access the successfulAuthentication()....");

        TokenAuthenticationService.addAuthentication(res, auth.getName());
    }


    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException failed) throws IOException, ServletException {

        logger.info("======>> #FILTER Access the unsuccessfulAuthentication()....");

        response.setContentType("application/json;charset=UTF-8");
        response.setHeader("Strict-Transport-Security", "max-age=31536000");
        response.setStatus(HttpServletResponse.SC_OK);
        String msg = failed.getMessage();
        if (ESAPI.validator().isValidInput("unsuccessfulAuthentication", msg, "Space", Integer.MAX_VALUE, false)) {
            response.getWriter().println(new WebResult(500, msg, "").toString());
        } else {
            response.getWriter().println(new WebResult(500, "", "").toString());
        }
    }


    // SECTOR: METHOD
    protected String getIpAddr(HttpServletRequest request) {

        logger.info("======>> #FILTER Access the getIpAddr()....");

        String ip = request.getHeader("X-FORWARDED-FOR");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        logger.info("Orinin IP" + ip);

        // IPV4的情況 把PORT號濾掉
        if (ip.split(":").length == 2) {
            ip = ip.split(":")[0];
        } else if (ip.split(":").length > 6) {
            // 如果是IP V6 先用此IP
            ip = "172.31.1.116";
        }
        logger.info("======>> #FILTER Last IP" + ip);

        return ip;
    }


    /**
     * Native create login entity as set user-agent seperate
     * 
     * @param req
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    protected LoginView setLoginViewByRequest(HttpServletRequest req) throws JsonParseException, JsonMappingException, IOException {
        LoginView loginAuth = new ObjectMapper().readValue(req.getInputStream(), LoginView.class);
        loginAuth.setIpAddress(getIpAddr(req));
        UserAgent userAgent = UserAgent.parseUserAgentString(req.getHeader("User-Agent"));
		String browser = userAgent.getBrowser().getName();
		String platform = userAgent.getOperatingSystem().getDeviceType().name();
		String os = userAgent.getOperatingSystem().getName();
        loginAuth.setBrowser(browser);
        loginAuth.setPlatform(platform);
        loginAuth.setOs(os);
        return loginAuth;
    }


    /**
     * 
     * @param req
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    protected OFLoginView setLoginViewByRequestMk2(HttpServletRequest req) throws JsonParseException, JsonMappingException, IOException {
        OFLoginView loginAuth = new ObjectMapper().readValue(req.getInputStream(), OFLoginView.class);
        String userAgent = req.getHeader("User-Agent");
        loginAuth.setIpAddress(getIpAddr(req));
        loginAuth.setUserAgent(userAgent);
        return loginAuth;
    }


}