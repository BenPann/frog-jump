package com.kgi.eopend3.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = { "file:${OFEX_CONFIG_PATH}/WEB.properties" }, encoding = "UTF-8")
public class GlobalConfig {

    @Value("${Server.AP.Host}")
    public String APServerHost;
    
}