package com.kgi.eopend3.web.config;

import com.kgi.eopend3.web.jwt.CustomAuthenticationProvider;
import com.kgi.eopend3.web.jwt.JWTAuthenticationFilter;
import com.kgi.eopend3.web.jwt.JWTLoginFilter;
import com.kgi.eopend3.web.jwt.JwtAuthenticationEntryPoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
// @EnableGlobalMethodSecurity(securedEnabled = true)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @Autowired
    private ApplicationContext appContext;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 開啟cors
        http.cors()
            .and().csrf().disable() // 關閉csrf
            .authorizeRequests() // 驗證需求
            .antMatchers("/mockRoute/**").permitAll()
            .antMatchers("/common/**").permitAll()
            .anyRequest().authenticated() // 所有需求都需要驗證
            .and().exceptionHandling().authenticationEntryPoint(unauthorizedHandler) // exception處理
            .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and() // session處理
            // 加入驗證登入的filter
            .addFilterBefore(new JWTLoginFilter("/api/onlineform/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
            .addFilterBefore(new JWTLoginFilter("/api/onlineform/freeLogin", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
            // 加入驗證token的filter
            .addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 使用自訂的身分驗證類別
        auth.authenticationProvider(new CustomAuthenticationProvider(appContext));
    }


    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                // Auth service
                .antMatchers("/publicApi/**")
                .antMatchers("/mockRoute/**")
                .antMatchers("/front/**")
                .antMatchers("/common/**");
    }


    @Bean
    public BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }


}
