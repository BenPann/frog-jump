package com.kgi.eopend3.web.controller;

import javax.servlet.http.HttpServletResponse;

import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.web.dao.HttpDao;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin
@RestController
public class CommonController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    HttpDao httpDao;

    /**
     * 此方法為"主要"跟AP溝通的 POST 管道 (無驗證) 當前端傳遞為JsonArray時使用
     */
    @PostMapping("/api/{controller}/{action}")
    public String postCommonEntry(@PathVariable("controller") String controller, @PathVariable("action") String action, @RequestBody String reqBody, HttpServletResponse response, UsernamePasswordAuthenticationToken userAuth) {
        logger.info("======> #Proxy Access the postCommonEntry() Dispatcher request to target prosition...");
        logger.info("======> #Proxy data of method(): ctrl: {}, act: {}, req: {}, res: {}, auth: {}", controller, action, reqBody, response, userAuth);

        String requestString = handleRequest(reqBody);
        String result = this.httpDao.getAPResponse(controller, action, requestString, userAuth);
        
        if (result.equals("")) { // 表示有錯誤
            result = new WebResult(101, "系統維護中，請稍後再試", "").toString();
        }

        if (result.equals("404")) { // 特殊處理 AP找不到 這邊也回找不到
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return "";
        }
        return result;
    }


    /** 
     * 此方法為"次要"跟AP溝通的 POST 管道(無參數以及驗證)
     */
    @PostMapping("/publicApi/{controller}/{action}")
    public String postCommonEntryNoAuth(@PathVariable("controller") String controller, @PathVariable("action") String action, @RequestBody String reqBody, HttpServletResponse response) {

        logger.info("======> #Proxy postCommonEntryNoAuth() Dispatcher request to target prosition...");

        String requestString = handleRequest(reqBody);
        String result = this.httpDao.getAPResponseNoAuth(controller, action, requestString);
        
        if (result.equals("")) { // 表示有錯誤
            result = new WebResult(101, "系統維護中，請稍後再試", "").toString();
        } 
        
        if (result.equals("404")) { // 特殊處理 AP找不到 這邊也回找不到
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return "";
        }
        return result;
    }
    

    /** 
     * 此方法為主要跟AP溝通的 GET 管道(無參數) 
     */
    @GetMapping("/api/{controller}/{action}")
    public String getCommonEntry(@PathVariable("controller") String controller, @PathVariable("action") String action, HttpServletResponse response, UsernamePasswordAuthenticationToken userAuth) {
        
        logger.info("======> #Proxy getCommonEntry() Dispatcher request to target prosition...");
        
        String result = this.httpDao.getAPResponseByGetMethod(controller, action, userAuth);
        // 表示有錯誤
        if (result.equals("")) {
            result = new WebResult(101, "系統維護中，請稍後再試", "").toString();
        } 
        // 特殊處理 AP找不到 這邊也回找不到
        if (result.equals("404")) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return "";
        }
        return result;
    }


    /** 
     * 此方法為次要跟AP溝通的 GET 管道(無參數以及驗證)
     */
    @GetMapping("/publicApi/{controller}/{action}")
    public String getCommonEntryNoAuth(@PathVariable("controller") String controller, @PathVariable("action") String action, HttpServletResponse response) {
        
        logger.info("======> #Proxy getCommonEntryNoAuth() Dispatcher request to target prosition...");
        
        String result = this.httpDao.getAPResponseNoAuthByGetMethod(controller, action);
        // 表示有錯誤
        if (result.equals("")) {
            result = new WebResult(101, "系統維護中，請稍後再試", "").toString();
        } 
        // 特殊處理 AP找不到 這邊也回找不到
        if (result.equals("404")) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return "";
        }
        return result;
    }



    // SECTOR: Route-2
    @GetMapping("/mockRoute/{controller}/{action}")
    public String getCommonHttpMk1(@PathVariable("controller") String controller, @PathVariable("action") String action, HttpServletResponse response) {
        logger.info("======> #Proxy Access the getCommonHttpMk1()....");
        String result = this.httpDao.getAPResponseNoAuthByGetMethod(controller, action);
        return result;
    }


    @PostMapping("/mockRoute/{controller}/{action}")
    public String postCommonHttpMk1(@PathVariable("controller") String controller, @PathVariable("action") String action, @RequestBody String reqBody, HttpServletResponse response) {
        logger.info("======> Access te postCommonHttpMk1().... {}, {}, {}", controller, action, response);
        String requestString = handleRequest(reqBody);
        String result = this.httpDao.getAPResponseNoAuth(controller, action, requestString);
        if (result.equals("")) { // 表示有錯誤
            result = new WebResult(101, "系統維護中，請稍後再試", "").toString();
        }
        if (result.equals("404")) { // 特殊處理 AP找不到 這邊也回找不到
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return "";
        }
        return result;
    }


    // SECTOR: METHOD
    /** 處理Request字串(有需要再做解密) */
    public String handleRequest(String requestString) {
        if (requestString == null) {
            return "";
        }
        if (requestString.trim().equals("")) {
            return "";
        }
        try{
            ESAPI.validator().isValidInput(this.getClass().getName(), requestString, "Space", Integer.MAX_VALUE,false);
        } catch (IntrusionException e){
            logger.error(this.getClass().getName() + " Invalid Input");
            return "";
        }
        // 如果需要解密 在這邊做
        return requestString;
    }


    /** 處理Response字串(有需要再做加密) */
    public String handleResponse(String responseStrinig) {
        // 有需要在此做加密
        return responseStrinig;
    }

}
