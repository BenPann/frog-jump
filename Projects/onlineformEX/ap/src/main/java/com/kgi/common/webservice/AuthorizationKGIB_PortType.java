/**
 * AuthorizationKGIB_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.kgi.common.webservice;

public interface AuthorizationKGIB_PortType extends java.rmi.Remote {
    public com.kgi.common.webservice.AUTINFO_KGIB FUNGETAUTINFOBYID_KGIB(java.lang.String STRID) throws java.rmi.RemoteException;
    public com.kgi.common.webservice.AUTINFO_KGIB_NEW FUNGETAUTINFOBYID_KGIB_NEW(java.lang.String STRIDG) throws java.rmi.RemoteException;
    public com.kgi.common.webservice.UPDATEAUTINFO_KGIB FUNUPDATEAUTINFO_KGIB(java.lang.String STRID, java.lang.String STRCUSTOMERNAME, java.lang.String STRA_ITEM01, java.lang.String STRAUTHORIZETOALLCORP, java.lang.String STRTOCORP01, java.lang.String STRTOCORP13, java.lang.String STRTOCORP22, java.lang.String STRTOCORP23, java.lang.String STRTOCORP24, java.lang.String STRTOCORP25, java.lang.String STRTOCORP26, java.lang.String STRTOCORP27, java.lang.String STRTOCORP29, java.lang.String STRFROMIP, java.lang.String STRVERNO) throws java.rmi.RemoteException;
    public com.kgi.common.webservice.UPDATEAUTINFO_KGIB FUNUPDATEAUTINFO_KGIB_NEW(java.lang.String STRID_NEW, java.lang.String STRCUSTOMERNAME, java.lang.String STRA_ITEM01, java.lang.String STRFROMIP, java.lang.String STRVERNO) throws java.rmi.RemoteException;
    public com.kgi.common.webservice.UPDATEAUTINFO_KGIB FUNUPDATEAUTINFO_KGIB_ACC(java.lang.String STRID, java.lang.String STRCUSTOMERNAME, java.lang.String STRA_ITEM01, java.lang.String STRA_DEPTNO, java.lang.String STRFROMIP, java.lang.String STRVERNO) throws java.rmi.RemoteException;
}
