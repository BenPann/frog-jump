package com.kgi.onlineformex.ap.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Base64;
import java.util.Base64.Decoder;

import com.kgi.onlineformex.ap.config.OFEXConfig;
import com.kgi.onlineformex.ap.dao.FTPDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class FTPService {


	@Autowired
	FTPDao ftpDao;
	@Autowired
	OFEXConfig globalConfig;

	InputStream tempInputStream = null;


	public void Base64ToInputStream(String fileStr) throws Exception{
		if (fileStr != null) {
			Decoder decoderMk2 = Base64.getDecoder();
			byte[] bMk2 = decoderMk2.decode(fileStr);
	 		for (int i = 0; i < bMk2.length; ++i ) {
	 			if ( bMk2[i] < 0) { // 調整異常數據
	 				bMk2[i] += 256;
	 			}
	 		}
	 		tempInputStream = new ByteArrayInputStream(bMk2);
		}	
	}


	public void Base64ToInputStream(byte[] fileByteArr) throws Exception{
		if (fileByteArr != null) {
	 		for (int i = 0; i < fileByteArr.length; ++i ) {
	 			if ( fileByteArr[i] < 0) { // 調整異常數據
					fileByteArr[i] += 256;
	 			}
			}
			// 將處裡完的PDF值儲存在容器裡 以後在上傳直接使用
	 		tempInputStream = new ByteArrayInputStream(fileByteArr);
		}
	}


	public InputStream Base64ToInputStreamMK2(byte[] fileByteArr) throws Exception {
		if (fileByteArr != null) {
	 		for (int i = 0; i < fileByteArr.length; ++i ) {
	 			if ( fileByteArr[i] < 0) { // 調整異常數據
					fileByteArr[i] += 256;
	 			}
			}
	 		return new ByteArrayInputStream(fileByteArr);
		}
		return null;
	}


	public boolean uploadImgBase64File(String filename) {
		String serverAdd = globalConfig.Img_FTPServer_Address();
		String ftpUserId = globalConfig.Img_FTPServer_Userid();
		String ftppazz = globalConfig.Img_FTPServer_PWD();
		String path = globalConfig.Img_FTPServer_Path();
		return ftpDao.upload(serverAdd, 21, ftpUserId, ftppazz, path, filename, tempInputStream);
	}


	public boolean uploadImgBase64File(String filename, byte[] bs) {
		String serverAdd = globalConfig.Img_FTPServer_Address();
		String ftpUserId = globalConfig.Img_FTPServer_Userid();
		String ftppazz = globalConfig.Img_FTPServer_PWD();
		String path = globalConfig.Img_FTPServer_Path();
		tempInputStream = new ByteArrayInputStream(bs);
		return ftpDao.upload(serverAdd, 21, ftpUserId, ftppazz, path, filename, tempInputStream);
	}


	public boolean uploadImgXml(String filename, InputStream is) {
		String serverAdd = globalConfig.Img_FTPServer_Address();
		String ftpUserId = globalConfig.Img_FTPServer_Userid();
		String ftppazz = globalConfig.Img_FTPServer_PWD();
		String path = globalConfig.Img_FTPServer_Path();
		return ftpDao.upload(serverAdd, 21, ftpUserId, ftppazz, path, filename, is);
	}


	public boolean uploadMailHunter(String ftppath, String filename, InputStream is) {
		String serverAdd = globalConfig.MailHunter_FTPServer_Address();
		String ftpUserId = globalConfig.MailHunter_FTPServer_Userid();
		String ftppazz = globalConfig.MailHunter_FTPServer_PAZZD();
		
		return ftpDao.upload(serverAdd, 21, ftpUserId, ftppazz, ftppath, filename, is);
	}


	/**
	 * @param filename
	 * 上傳已產好整批pdf檔
	 */
	public boolean uploadMailHunterPdfBase64File(String filename) {
		String serverAdd = globalConfig.MailHunter_FTPServer_Address();
		String ftpUserId = globalConfig.MailHunter_FTPServer_Userid();
		String ftppazz = globalConfig.MailHunter_FTPServer_PAZZD();

		return ftpDao.upload(serverAdd, 21, ftpUserId, ftppazz, "File", filename, tempInputStream);
	}


}
