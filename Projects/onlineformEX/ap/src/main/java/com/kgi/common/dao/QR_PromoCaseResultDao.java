package com.kgi.common.dao;

import java.util.List;
import com.kgi.eopend3.common.dto.db.QR_PromoCaseResult;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class QR_PromoCaseResultDao extends CRUDQDao<QR_PromoCaseResult> {

    @Override
    public int Create(QR_PromoCaseResult fullItem) {
        String sql = " Insert into QR_PromoCaseResult (UniqId,UniqType,PromoDepart1,PromoMember1,PromoDepart2,PromoMember2,AssignDepart,CreateTime) " +
                     " values (:UniqId,:UniqType,:PromoDepart1,:PromoMember1,:PromoDepart2,:PromoMember2,:AssignDepart,getdate())";
                     return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public QR_PromoCaseResult Read(QR_PromoCaseResult keyItem) {
        String sql = "SELECT * FROM QR_PromoCaseResult WHERE UniqId = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_PromoCaseResult.class),
                    new Object[] { keyItem.getUniqId() });
        } catch (DataAccessException ex) {
			logger.error("QR_PromoCaseResult查無資料");
            return null;
        }
    }

    @Override
    public int Update(QR_PromoCaseResult fullItem) {
        String sql = " UPDATE QR_PromoCaseResult set PromoDepart1=:PromoDepart1, PromoMember1=:PromoMember1, PromoDepart2=:PromoDepart2, PromoMember2=:PromoMember2, AssignDepart=:AssignDepart "
        		+ " WHERE UniqId=:UniqId";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(QR_PromoCaseResult keyItem) {
        return 0;
    }

    @Override
    public List<QR_PromoCaseResult> Query(QR_PromoCaseResult keyItem) {       
        return null;        
    }

}