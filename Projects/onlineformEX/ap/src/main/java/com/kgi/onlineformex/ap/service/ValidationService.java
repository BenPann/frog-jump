package com.kgi.onlineformex.ap.service;

import com.google.gson.Gson;
import com.kgi.common.config.GlobalConfig;
import com.kgi.common.dao.HttpDao;
import com.kgi.common.dao.SNDao;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.db.ED3_RejectLog;
import com.kgi.eopend3.common.dto.db.MailHistory;
import com.kgi.eopend3.common.dto.response.CheckOTPResp;
import com.kgi.eopend3.common.dto.response.HttpResp;
import com.kgi.eopend3.common.dto.response.LogFromValidation;
import com.kgi.eopend3.common.dto.response.SendOTPResp;
import com.kgi.eopend3.common.dto.view.CheckOTPView;
import com.kgi.eopend3.common.dto.view.EmailCheckView;
import com.kgi.eopend3.common.dto.view.SendOTPView;
import com.kgi.eopend3.common.util.CheckUtil;
import com.kgi.eopend3.common.util.DateUtil;
import com.kgi.onlineformex.ap.dao.FM_ApiLogDao;
import com.kgi.onlineformex.ap.dao.FM_CaseDataDao;
import com.kgi.onlineformex.ap.exception.ErrorResultException;
import com.kgi.onlineformex.common.dto.db.FM_CaseData;
import com.kgi.onlineformex.common.dto.response.VerifyCountResp;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidationService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private GlobalConfig globalConfig;
	@Autowired
	private SNDao sndao;
	@Autowired
	private FM_ApiLogDao fmalDao;
	@Autowired
	private FM_CaseDataDao fmcdDao;
	@Autowired
	private HttpDao httpDao;

	/**
	 * 取得 OTP
	 */
	public SendOTPResp getSessionKey(SendOTPView view, String uniqId, String idno) {

		logger.info("========>> #SERVICE Access the getSessionKey().... {}, {}, {}", view, uniqId, idno);
		String startTime = "", endTime = "", logReq = "", logRes = "";

		try {
			String date = DateUtil.GetDateFormatString("yyyyMMdd"); // 交易代號 格式(AIRLOAN + 年月日(8) + 流水號(4))
			Integer sni = sndao.getSerialNumber("OTP", date); // 從DN取流水號 並將其串成四位字串
			String sn = StringUtils.leftPad(sni.toString(), 4, "0");
			String txnId = "FM" + date + sn;
			view.setTxnId(txnId);
			view.setIdno(idno);
			view.setBillDep(globalConfig.OTPBillDep);
			view.setOpId(globalConfig.OTPOpID);
			String jsonStr = new Gson().toJson(view);

			// SEND OTP
			logReq = jsonStr;
			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			HttpResp resp = httpDao.postJson(globalConfig.validationServerUrl + SystemConst.API_SEND_OTP, jsonStr);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			logRes = resp.getResponse();

			logger.info("========>>> #SERVICE resp.getResponse {}", logRes);

			LogFromValidation log = new Gson().fromJson(resp.getResponse(), LogFromValidation.class);
			if (log != null) {
				if (!CheckUtil.isEmpty(log.getReq())) {
					logReq = log.getReq();
				}
				if (!CheckUtil.isEmpty(log.getRes())) {
					logRes = log.getRes();
				}
			}
			SendOTPResp sendOpt = new Gson().fromJson(resp.getResponse(), SendOTPResp.class);

			// record api log
			logger.info("========>> #SERVICE FM_ApiLog METHOD: /sendOTP, UniqId: {}", uniqId);
			fmalDao.Create(uniqId, "-", "/sendOTP", logReq, logRes, startTime, endTime);

			return sendOpt;

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("<<========= #SERVICE ERROR 未處理的錯誤: {}", e);
			SendOTPResp sendOpt = new SendOTPResp();
			sendOpt.setCode("F999");
			sendOpt.setMessage("系統錯誤");
			return sendOpt;

		} finally {
			logger.info("<<======== #SERVICE terminate getSessionKey()....");

		}
	} // end getSessionKey

	/**
	 * 檢核 OTP
	 */
	public CheckOTPResp checkSessionKeyOtp(CheckOTPView view, String uniqId, String idno, String ipAddr) {
		logger.info("========>> #SERVICE Access the checkSessionKeyOtp()....");
		String startTime = "", endTime = "", logReq = "", logRes = "";

		try {
			view.setIdno(idno);
			view.setBillDep(globalConfig.OTPBillDep);
			view.setOpId(globalConfig.OTPOpID);

			logReq = new Gson().toJson(view);
			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			HttpResp response = httpDao.postJson(globalConfig.validationServerUrl + SystemConst.API_CHECK_OTP, logReq);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			logRes = response.getResponse();

			LogFromValidation log = new Gson().fromJson(response.getResponse(), LogFromValidation.class);
			if (log != null) {
				if (!CheckUtil.isEmpty(log.getReq())) {
					logReq = log.getReq();
				}
				if (!CheckUtil.isEmpty(log.getRes())) {
					logRes = log.getRes();
				}
			}

			fmalDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "/checkOTP", logReq, logRes, startTime, endTime);

			CheckOTPResp checkOpt = new Gson().fromJson(response.getResponse(), CheckOTPResp.class);
			boolean isCheckOtpSuccess = "0000".equals(checkOpt.getCode());

			FM_CaseData fmcd = fmcdDao.Read(new FM_CaseData(uniqId));
			fmcd.setPhone(view.getPhone());
			// fmcd.setIpAddress(ipAddr);
			fmcd.setAuthStatus(isCheckOtpSuccess ? "1" : "0");

			int authErrCount = fmcd.getAuthErrCount() == null ? 0 : Integer.valueOf(fmcd.getAuthErrCount());
			if (!isCheckOtpSuccess) {
				authErrCount = Integer.valueOf(fmcd.getAuthErrCount()) + 1;
			}
			fmcd.setAuthErrCount(String.valueOf(authErrCount));
			fmcd.setAuthTime(DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS"));
			fmcd.setStatus(SystemConst.RPL_STATUS_SEND_OTP);
			fmcdDao.UpdateAfterOTPCheckResp(fmcd);

			return checkOpt;

		} catch (ErrorResultException e) {
			CheckOTPResp checkOpt = new CheckOTPResp();
			checkOpt.setCode(String.valueOf(e.getStatus()));
			checkOpt.setMessage("查無資料");
			return checkOpt;

		} catch (Exception e) {
			CheckOTPResp checkOpt = new CheckOTPResp();
			checkOpt.setCode("F099");
			checkOpt.setMessage("系統錯誤");
			logger.error("<<======== 系統錯誤", e);
			return checkOpt;

		} finally {
			logger.info("<<======== checkSessionKeyOtp()");
		}
	} // end checkSessionKeyOtp

	/**
	 * Check mail sending
	 * 
	 * @return
	 */
	public String afterAllFinish(String uniqId, String reqBody) {

		logger.info("========>> #Service Access the afterAllFinish()....");
		String startTime = "", endTime = "", logReq = "", logRes = "";

		try {
			MailHistory mh = new MailHistory();
			// 寫入apilog記錄
			fmalDao.Create(
					mh.getStatus().equals("00") ? SystemConst.RPL_MAIL_HISTORY_STATUS_SUCCESSFUL
							: SystemConst.RPL_MAIL_HISTORY_STATUS_FAILED,
					uniqId, "/checkOTP", logReq, logRes, startTime, endTime);
		} catch (Exception e) {
			logger.error("<<======== #Service ERROR 系統錯誤", e);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			fmcdDao.UpdateStatusByUniqId(uniqId, SystemConst.CASE_STATUS_ADDTIONAL_FAIL);
		} finally {
			fmalDao.Create(SystemConst.RPL_MAIL_HISTORY_STATUS_SUCCESSFUL, uniqId, "/checkOTP", logReq, logRes,
					startTime, endTime);
		}
		return null;
	} // end afterAllFinish

	/**
	 * 讀取案件的驗證失敗次數 如果案件不存在 ContractMainActInfo ，建立之
	 */
	public String getVerifyCount(String uniqId) {
		try {
			FM_CaseData verify = fmcdDao.touch(new FM_CaseData(uniqId));
			VerifyCountResp resp = new VerifyCountResp();
			resp.setAuthErrCount(verify.getAuthErrCount());

			return WebResult.GetResultString(0, "成功", resp);
		} catch (Exception e) {
			logger.error("<<======== #ERROR 未處理的錯誤", e);
			return WebResult.GetResultString(99, "系統錯誤", null);

		}
	} // end getVerifyCount

	/**
	 * 
	 * @param reqJson
	 * @param uniqId
	 * @param idno
	 * @param ipAddress
	 * @return
	 */
	public String isEmailSuccess(String reqJson, String uniqId, String idno, String ipAddress) {
		try {
			Gson gson = new Gson();
			EmailCheckView view = gson.fromJson(reqJson, EmailCheckView.class);

			if (!CheckUtil.check(view)) {
				throw new ErrorResultException(1, "參數錯誤", null);
			}

			// 先判斷凱基內部DB
			// 再判斷AIRLOANDB
			FM_CaseData fmcd = fmcdDao.Read(new FM_CaseData(uniqId));

			ED3_RejectLog ed3_RejectLog = new ED3_RejectLog(idno, ipAddress);
			ed3_RejectLog.setUniqId(uniqId);
			ed3_RejectLog.setPhone(fmcd.getPhone());
			ed3_RejectLog.setEmailAddress(view.getEmail());

			// int count = ed3CaseDataDao.isEmailSuccess();
			int count = fmcdDao.inDWEmailSuccess(view.getEmail());
			if (count > 0) {
				// ed3_RejectLog.setRejectCode(SystemConst.REJECTLOG_COED_EMAILAPPLYED);
				// ed3RejectLogDao.Create(ed3_RejectLog);
				return WebResult.GetResultString(0, "成功", "N");
			} else {
				count = fmcdDao.inAirloanEmailSuccess(view.getEmail(), idno);
				// ed3_CaseData.setEmailAddress(view.getEmail());
				fmcdDao.Update(fmcd);
				return WebResult.GetResultString(0, "成功", "Y");
			}

		} catch (ErrorResultException e) {
			return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
		} catch (Exception e) {
			logger.error("========<< 未處理的錯誤", e);
			return WebResult.GetResultString(99, "系統錯誤", null);
		}
	}

}
