package com.kgi.common.dao;

import java.util.List;
import java.util.Map;

import com.kgi.eopend3.common.dto.db.Dropdown;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class DropdownDao extends CRUDQDao<Dropdown> {

    @Override
    public int Create(Dropdown fullItem) {
        return 0;
    }

    @Override
    public Dropdown Read(Dropdown keyItem) {
    	String sql = "SELECT * FROM DropdownData where enable=1 AND Name = ? AND DataKey = ? ";
	    try {
	        return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(Dropdown.class),
	                keyItem.getName(),keyItem.getDataKey());
	    } catch (DataAccessException ex) {
	        logger.error("DropdownData查無資料");
	        return null;
	    }
    }

    @Override
    public int Update(Dropdown fullItem) {
        return 0;
    }

    @Override
    public int Delete(Dropdown keyItem) {
        return 0;
    }

    @Override
    public List<Dropdown> Query(Dropdown keyItem) {
        String sql = "SELECT * from DropdownData where enable=1 AND Name = ? order by Sort ";
        try {
            return this.getJdbcTemplate().query(sql, new BeanPropertyRowMapper<>(Dropdown.class), new Object[]{keyItem.getName()});
        } catch (DataAccessException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public Dropdown readByNameAndDataName(String name ,String dataName) {
    	String sql = "SELECT * FROM DropdownData where enable=1 AND Name = ? AND DataName = ? ";
	    try {
	        return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(Dropdown.class), name,dataName);
	    } catch (DataAccessException ex) {
	        logger.error("DropdownData查無資料");
	        return null;
	    }
    }


    public String getDataNameByNameAndDataKey(String name, String dataKey) {
		name = name.trim();
		dataKey = dataKey.trim();
		List<Map<String, Object>> ls = this.getJdbcTemplate().queryForList(
				"SELECT DataName FROM DropdownData WHERE Name= ? AND DataKey = ?", new Object[] { name, dataKey });
		if (ls.size() > 0) {
			return ls.get(0).get("DataName").toString();
		} else {
//			System.out.println("Dropdown Name = " + name + " DataKey = " + dataKey + " Not Found." );
			return "";
		}
    }
    

    /** 用ID取得DropdownData */
    public List<Map<String, Object>> getListByName(String name) {
        // 建立db連線及查詢字串
        return this.getJdbcTemplate().queryForList(
                "SELECT DataKey,DataName FROM DropdownData WHERE Enable = 1 and Name= ? ORDER BY Sort",
                new Object[] { name });
    }

}
