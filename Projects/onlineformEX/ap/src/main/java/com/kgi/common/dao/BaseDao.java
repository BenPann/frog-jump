package com.kgi.common.dao;

import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class BaseDao {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    DataSource dataSource;

    protected JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(this.dataSource);
    }

    protected JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    protected NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(this.dataSource);
    }

    protected NamedParameterJdbcTemplate getNamedParameterJdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }


    private DataSourceTransactionManager transactionManager;
    private DefaultTransactionDefinition def;

    protected TransactionStatus BeginTransaction() {
        return BeginTransaction(dataSource);
    }

    protected TransactionStatus BeginTransaction(DataSource dataSource) {
        transactionManager = new DataSourceTransactionManager(dataSource);
        def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        return transactionManager.getTransaction(def);
    }

    /**提交交易 */
    protected void CommitTransaction(TransactionStatus status) {
        this.transactionManager.commit(status);
    }

    /**退回交易 */
    protected void RollbakcTransaction(TransactionStatus status) {
        this.transactionManager.rollback(status);
    }

}