package com.kgi.common.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.kgi.common.config.GlobalConfig;
import com.kgi.common.dao.ConfigDao;
import com.kgi.common.dao.MailHistoryDao;
import com.kgi.common.dao.QR_ChannelDepartListDao;
import com.kgi.common.dao.QR_ChannelListDao;
import com.kgi.common.dao.QR_PromoCaseResultDao;
import com.kgi.common.dao.SMTPMailDao;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.dto.db.Config;
import com.kgi.eopend3.common.dto.db.ED3_CaseData;
import com.kgi.eopend3.common.dto.db.MailHistory;
import com.kgi.eopend3.common.dto.db.QR_ChannelDepartList;
import com.kgi.eopend3.common.dto.db.QR_ChannelList;
import com.kgi.eopend3.common.dto.db.QR_PromoCaseResult;
import com.kgi.eopend3.common.util.CommonUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SendSmtpService {
   

//    @Autowired
//    private ED3_CaseDataDao ed3CaseDataDao;
    @Autowired
    private ConfigDao configDao;
	@Autowired
	GlobalConfig globalConfig;
    @Autowired
    private MailHistoryDao mailHistoryDao;
    @Autowired
    private QR_PromoCaseResultDao qrPromoCaseResultDao;
    @Autowired
    private QR_ChannelDepartListDao qrChannelDepartListDao;
    @Autowired
    private QR_ChannelListDao qrChannelListDao;
    @Autowired
    private SMTPMailDao smtpMailDao;


    /**
     * 前一日未完成提醒
     */
    public void smtpWithCreditDataUnfinishCaseYesterday(List<ED3_CaseData> caseList) {
        try {
            // 信用卡前一日未完成提醒
            //List<ED3_CaseData> caseList = ed3CaseDataDao.getWrittingDataByCreateTime();
            String content = "";
            String tmpChannelID = "";
            String tmpDepartID = "";
            for(int i = 0; i < caseList.size() ;i++){
                
                ED3_CaseData ed3_CaseData = caseList.get(i);
                String channelId = ed3_CaseData.getPromoChannelID();
                String departId = ed3_CaseData.getPromoDepart();
                if(!tmpChannelID.equals(channelId)||!tmpDepartID.equals(departId)){
                    if(i!=0){
                        createMailHistory(content,"【凱基銀行台幣數位存款帳戶申請】-未完成批次通知", channelId, departId);
                        /*content += getSMTPContentFooter();
                        // 主旨需將中間名隱藏
                        QR_ChannelDepartList qr_ChannelDepartList = new QR_ChannelDepartList();
                        qr_ChannelDepartList.setChannelId(channelId);
                        qr_ChannelDepartList.setDepartId(departId);
                        qr_ChannelDepartList= qrChannelDepartListDao.Read(qr_ChannelDepartList);
                        
                        MailHistory mailHistory = new MailHistory();
                        mailHistory.setStatus("0");
                        mailHistory.setMailType(SystemConst.MAILHISTORY_TYPE_SMTP);
                        mailHistory.setTitle("【凱基銀行台幣數位存款帳戶申請】-未完成批次通知");
                        mailHistory.setContent(content);
                        mailHistory.setEMailAddress(qr_ChannelDepartList.getEMail());*/
                    }
                    content = getSMTPContentHeader();
                    tmpChannelID = channelId;
                    tmpDepartID = departId;
                }
                String uniqId = ed3_CaseData.getUniqId();               
                String name = ed3_CaseData.getChtName();                
                String applyTime = ed3_CaseData.getUpdateTime();
                String idno = ed3_CaseData.getIdno(); 
                //String gender = ed3_CaseData.getIdno();
                String phone = ed3_CaseData.getPhone();
                String email = ed3_CaseData.getEmailAddress();
                String contacTime = "無";
                String product = "數位存款";
                String status = "未完成";
                String productId = ed3_CaseData.getProductId(); 
                String prjCode = "";
                if(productId.equals(SystemConst.CASEDATA_PRODUCTID_ACCOUNT)){
                    prjCode = configDao.ReadConfigValue("MailHunter.ED3PrdouctAMsg");
                }else{
                    prjCode = configDao.ReadConfigValue("MailHunter.ED3PrdouctCCAMsg");
                }
                String promo1 = "";
                String member1  = "";
                QR_PromoCaseResult qr_promoCaseResult = qrPromoCaseResultDao.Read(new QR_PromoCaseResult(uniqId, ed3_CaseData.getUniqType()));
                if (qr_promoCaseResult != null) {
                    promo1 = qr_promoCaseResult.getPromoDepart1();
                    member1 = qr_promoCaseResult.getPromoMember1();
                }
                content += getSMTPContent(uniqId, applyTime, name, idno, phone, email, contacTime, product, status, prjCode, 
                                        channelId, promo1, member1, "",  "");     
                             // 寄送給相關處理人員
                if(i==caseList.size()-1){
                    createMailHistory(content,"【凱基銀行台幣數位存款帳戶申請】-未完成批次通知", channelId, departId);
                    //content += getSMTPContentFooter();
                }
            }
            
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

	//[2019.11.22 GaryLiu] ==== START : 組完成通知信內容(For TM) ====
    //[2019.11.22 GaryLiu] TIP : 邏輯跟smtpWithCreditDataUnfinishCaseYesterday幾乎一模一樣，但不確定以後是否會有個別間的差異，故先分開來寫
    public void smtpWithCreditDataFinishCaseYesterday(List<ED3_CaseData> caseList) {
        try {
            //數位帳戶申辦完成提醒
            //List<ED3_CaseData> caseList = ed3CaseDataDao.getWrittingDataByCreateTime();
            String content = "";
            String tmpChannelID = "";
            String tmpDepartID = "";
            for(int i = 0; i < caseList.size() ;i++){
                
                ED3_CaseData ed3_CaseData = caseList.get(i);
                String channelId = ed3_CaseData.getPromoChannelID();
                String departId = ed3_CaseData.getPromoDepart();
                if(!tmpChannelID.equals(channelId)||!tmpDepartID.equals(departId)){
                    if(i!=0){
                        createMailHistory(content,"【凱基銀行台幣數位存款帳戶申請】-已完成批次通知", channelId, departId);
                        /*content += getSMTPContentFooter();
                        // 主旨需將中間名隱藏
                        QR_ChannelDepartList qr_ChannelDepartList = new QR_ChannelDepartList();
                        qr_ChannelDepartList.setChannelId(channelId);
                        qr_ChannelDepartList.setDepartId(departId);
                        qr_ChannelDepartList= qrChannelDepartListDao.Read(qr_ChannelDepartList);
                        
                        MailHistory mailHistory = new MailHistory();
                        mailHistory.setStatus("0");
                        mailHistory.setMailType(SystemConst.MAILHISTORY_TYPE_SMTP);
                        mailHistory.setTitle("【凱基銀行台幣數位存款帳戶申請】-未完成批次通知");
                        mailHistory.setContent(content);
                        mailHistory.setEMailAddress(qr_ChannelDepartList.getEMail());*/
                    }
                    content = getSMTPContentHeader();
                    tmpChannelID = channelId;
                    tmpDepartID = departId;
                }
                String uniqId = ed3_CaseData.getUniqId();               
                String name = ed3_CaseData.getChtName();                
                String applyTime = ed3_CaseData.getUpdateTime();
                String idno = ed3_CaseData.getIdno(); 
                //String gender = ed3_CaseData.getIdno();
                String phone = ed3_CaseData.getPhone();
                String email = ed3_CaseData.getEmailAddress();
                String contacTime = "無";
                String product = "數位存款";
                String status = "已完成";
                String productId = ed3_CaseData.getProductId(); 
                String prjCode = "";
                if(productId.equals(SystemConst.CASEDATA_PRODUCTID_ACCOUNT)){
                    prjCode = configDao.ReadConfigValue("MailHunter.ED3PrdouctAMsg");
                }else{
                    prjCode = configDao.ReadConfigValue("MailHunter.ED3PrdouctCCAMsg");
                }
                String promo1 = "";
                String member1  = "";
                QR_PromoCaseResult qr_promoCaseResult = qrPromoCaseResultDao.Read(new QR_PromoCaseResult(uniqId, ed3_CaseData.getUniqType()));
                if (qr_promoCaseResult != null) {
                    promo1 = qr_promoCaseResult.getPromoDepart1();
                    member1 = qr_promoCaseResult.getPromoMember1();
                }
                content += getSMTPContent(uniqId, applyTime, name, idno, phone, email, contacTime, product, status, prjCode, 
                                        channelId, promo1, member1, "",  "");     
                             // 寄送給相關處理人員
                if(i==caseList.size()-1){
                    createMailHistory(content,"【凱基銀行台幣數位存款帳戶申請】-已完成批次通知", channelId, departId);
                    //content += getSMTPContentFooter();
                }
            }
            
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
    //[2019.11.22 GaryLiu] ====  END  : 組完成通知信內容(For TM) ====
    																		 
    public void createMailHistory(String content,String title,String channelId,String departId){
        content += getSMTPContentFooter();
        // 主旨需將中間名隱藏
        QR_ChannelDepartList qr_ChannelDepartList = new QR_ChannelDepartList();
        if(departId.equals("")){
            channelId= "-";
            departId = "-";
        }
        qr_ChannelDepartList.setChannelId(channelId);
        qr_ChannelDepartList.setDepartId(departId);
        qr_ChannelDepartList= qrChannelDepartListDao.Read(qr_ChannelDepartList);
        
        MailHistory mailHistory = new MailHistory();
        mailHistory.setStatus("0");
        mailHistory.setMailType(SystemConst.MAILHISTORY_TYPE_SMTP_ALL);
        mailHistory.setTitle(title);
        mailHistory.setContent(content);
        mailHistory.setEMailAddress(qr_ChannelDepartList.getEMail()); 
        mailHistoryDao.Create(mailHistory);
    }

    /**
     * 通知中壽 GetCif 無回應
     * */
    public void notifyCLGetCif(String uniqId, String time, String error) {
    	// TODO 存成設定檔
    	String mailList = "Ben.Pann@kgi.com,vicky.tsai@kgi.com" ;
    	
        MailHistory mailHistory = new MailHistory();
        mailHistory.setStatus("0");
        mailHistory.setMailType(SystemConst.MAILHISTORY_TYPE_SMTP_ALL);
        mailHistory.setTitle("中壽跨售商銀產品  取得客戶基本資訊失敗通知信");
        mailHistory.setContent("案號 [" + uniqId + "] 在" + time + " 時取得中壽個資失敗.\n訊息如下：\n" + error);
        mailHistory.setEMailAddress(mailList); 
        mailHistoryDao.Create(mailHistory);
    }

    /**
     * 通知代收付失敗7天未處理
     * */
    public void notifyPayerOver7Days(String content) {
    	// TODO 存成設定檔
    	String mailList = "Ben.Pann@kgi.com,vicky.tsai@kgi.com" ;
    	
        MailHistory mailHistory = new MailHistory();
        mailHistory.setStatus("0");
        mailHistory.setMailType(SystemConst.MAILHISTORY_TYPE_SMTP_ALL);
        mailHistory.setTitle("中壽跨售商銀產品  代收付失敗超過7日未處理通知信");
        mailHistory.setContent("以下案號續保扣繳失敗已逾7日未處理\n" + content);
        mailHistory.setEMailAddress(mailList); 
        mailHistoryDao.Create(mailHistory);
    }

    private String getTimeFormatSequence() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
        Date date = new Date();
        String seq = dateFormat.format(date);
        return seq;
    }

    private String getSMTPContentHeader() {        
        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        sb.append("<body>");
        sb.append("<table border==\"1\">");
        sb.append("<tr>");
        sb.append("<td>拋轉序號</td><td>案件編號</td>");
        sb.append("<td>時間</td><td>姓名</td>");
        sb.append("<td>身分證字號</td><td>性別</td>");
        sb.append("<td>連絡電話</td><td>email</td>");
        sb.append("<td>方便聯絡時間</td><td>申請產品別</td>");
        sb.append("<td>狀態別</td><td>卡片名稱/專案別</td>");
        sb.append("<td>通路來源</td><td>推薦單位</td>");
        sb.append("<td>推薦人員</td><td>轉介單位</td>");
        sb.append("<td>轉介人員</td>");
        sb.append("</tr>");
        return sb.toString();
    }

    public String nullChangeEmpty(String str){
        if(str == null){
            str = "";
        }
        return str;
    }

    public String getChannelName(String channelId){
        // 主旨需將中間名隱藏
        QR_ChannelList qr_ChannelList = qrChannelListDao.Read(new QR_ChannelList(channelId));
        if(channelId.equals("")){
            return "凱基商銀";
        }else{
            return qr_ChannelList.getChannelName();
        }
    }

    public String getSMTPContent(String uniqId, String applyTime, String name,String idno, String phone, String email, String contacTime,
                                String product, String status,String prjCode, String channelId, String promo1, String member1, String promo2, String member2 ) {
        
        StringBuilder sb = new StringBuilder();
        String seq = getTimeFormatSequence();      
        sb.append("<tr>");
        sb.append("<td>"+seq+"</td><td>"+uniqId+"</td>");
        sb.append("<td>"+applyTime+"</td><td>"+ CommonUtil.hiddenUserDataWithStar(nullChangeEmpty(name) , 1, 1)+"</td>");
        sb.append("<td>"+CommonUtil.hiddenUserDataWithStar(nullChangeEmpty(idno) , 4, 3)+"</td><td>"+ CommonUtil.getGenderFromId(nullChangeEmpty(idno))+"</td>");
        sb.append("<td>"+nullChangeEmpty(phone)+"</td><td>"+nullChangeEmpty(email)+"</td>");
        sb.append("<td>"+nullChangeEmpty(contacTime)+"</td><td>"+nullChangeEmpty(product)+"</td>");
        sb.append("<td>"+status+"</td><td>"+nullChangeEmpty(prjCode)+"</td>");
        sb.append("<td>"+getChannelName(channelId)+"</td><td>"+promo1+"</td>");
        sb.append("<td>"+member1+"</td><td>"+promo2+"</td>");
        sb.append("<td>"+member2+"</td>");
        sb.append("</tr>");
        return sb.toString();
    
    }
      
    public String getSMTPContentFooter() {
        StringBuilder sb = new StringBuilder();
        sb.append("</table>");
        sb.append("</body></html>");
        return sb.toString();
    
    }

	public void sendSMTP(MailHistory mailHistory) throws Exception {
        Config config = new Config();

        config.setKeyName("SMTP.MAIL.HOST");
        config = configDao.Read(config);
        String host = config.getKeyValue();

        config.setKeyName("SMTP.ContactMe.Mail.to");
        config = configDao.Read(config);
        String to = mailHistory.getEMailAddress().equals("") ? config.getKeyValue() : mailHistory.getEMailAddress();
        if (SystemConst.isTEST) {
        	to = globalConfig.FAKE_EMAIL ;
        }

        config.setKeyName("SMTP.ContactMe.Mail.From");
        config = configDao.Read(config);
        String from = config.getKeyValue();
        config.setKeyName("SMTP.Mail.Port");
        config = configDao.Read(config);
        String portstr = config.getKeyValue();

        smtpMailDao.sendEmail(host, portstr, to, from, mailHistory.getTitle(), mailHistory.getContent(), null);
	}
}
