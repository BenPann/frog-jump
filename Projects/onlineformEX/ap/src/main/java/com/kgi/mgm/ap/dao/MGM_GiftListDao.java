package com.kgi.mgm.ap.dao;

import java.util.List;

import com.kgi.common.config.DataSourceConfig;
import com.kgi.common.dao.CRUDQDao;
import com.kgi.mgm.common.dto.db.MGM_GiftList;
import com.kgi.onlineformex.ap.exception.ErrorResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class MGM_GiftListDao extends CRUDQDao<MGM_GiftList> {

    @Autowired
    private DataSourceConfig dsc;

    @Override
    public int Create(MGM_GiftList fullItem) {
        return 0;
    }

    @Override
    public int Delete(MGM_GiftList keyItem) {
        return 0;
    }

    @Override
    public List<MGM_GiftList> Query(MGM_GiftList keyItem) {
        return null;
    }

    /** SECTOR: READ */
    @Override
    public MGM_GiftList Read(MGM_GiftList keyItem) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).queryForObject(
                    "SELECT * FROM MGM_GiftList WHERE GIFT_ID = ? AND ENABLE = 1", new BeanPropertyRowMapper<>(MGM_GiftList.class),
                    new Object[] { keyItem.getGIFT_ID() });
        } catch (DataAccessException ex) {
            logger.error("========<<< #MGM_GiftList 查無資料");
            throw new ErrorResultException(9, "MGM_GiftList 查無資料", ex);
        }
    }

    @Override
    public int Update(MGM_GiftList fullItem) {
        return 0;
    }

    public List<MGM_GiftList> QueryAll() {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).query(
                            "SELECT * FROM MGM_GiftList WHERE ENABLE = 1",
                            new BeanPropertyRowMapper<>(MGM_GiftList.class));
        } catch (DataAccessException e) {
            logger.error("========<<< #MGM_GiftList 查無資料");
            throw new ErrorResultException(9, "MGM_GiftList 查無資料", e);
        } catch (Exception e) {
            throw new ErrorResultException(99, "MGM_GiftList 尚未處理之錯誤", e);
        }
    }
    
}
