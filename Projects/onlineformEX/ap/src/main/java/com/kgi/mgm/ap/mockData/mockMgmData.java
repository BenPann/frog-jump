package com.kgi.mgm.ap.mockData;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.mgm.ap.dao.MGM_MemberDao;
import com.kgi.mgm.common.dto.view.MgmCustomerLogin;
import com.kgi.onlineformex.ap.exception.ErrorResultException;
import com.kgi.onlineformex.common.dto.view.MGMMockView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class mockMgmData {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MGM_MemberDao mgmmDao;


    public String mockApiTestMethod(String reqBody) {
        logger.info("========>> #SERVICE Access the mockApiTestMethod().... ");
        try {
            return "";
        } catch(ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("<<======== #SERVICE ERROR 未處理的錯誤.... ", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
    }

    
    // MOCK:
    public String arrayFormForDemo() {
        Gson gson = new Gson();
        List<MGMMockView> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            MGMMockView mgmmv = new MGMMockView();
            mgmmv.setAlpha("A" + i);
            mgmmv.setBeta("B" + i);
            mgmmv.setGamma("C" + i);
            mgmmv.setDelta("D" + i);
            mgmmv.setEpsilon("E" + i);
            list.add(mgmmv);
            gson.toJson(mgmmv);
        }
        String res = WebResult.GetResultString(0, "成功", list);
        return res;
    }

    
    public String delectCustomerPasswordForTest(String reqBody) {
        logger.info("========>> #SERVICE Access the getShortUrlByActUniqId().... ");
        try {
            Gson gson = new Gson();
            MgmCustomerLogin cl = gson.fromJson(reqBody, MgmCustomerLogin.class);
            mgmmDao.UpdatePassword("", cl.getCUST_ID());
            return "";
        } catch(ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("<<======== #SERVICE ERROR 未處理的錯誤.... ", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
    }

}
