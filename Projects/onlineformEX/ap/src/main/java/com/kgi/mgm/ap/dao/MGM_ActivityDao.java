package com.kgi.mgm.ap.dao;

import java.util.List;

import javax.sql.DataSource;

import com.kgi.common.config.DataSourceConfig;
import com.kgi.common.config.StaticDB2Config;
import com.kgi.common.dao.CRUDQDao;
import com.kgi.mgm.common.dto.db.MGM_Activity;
import com.kgi.onlineformex.ap.exception.ErrorResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class MGM_ActivityDao extends CRUDQDao<MGM_Activity> {


    @Autowired
    private DataSourceConfig dsc;


    @Override
    public int Create(MGM_Activity fullItem) {
        return 0;
    }

    @Override
    public MGM_Activity Read(MGM_Activity keyItem) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).queryForObject(
                            "SELECT * FROM MGM_Activity WHERE ACT_UNIQ_ID = ? ",
                            new BeanPropertyRowMapper<>(MGM_Activity.class),
                            new Object[] { keyItem.getACT_UNIQ_ID() });
        } catch (DataAccessException ex) {
            logger.error("========<<< #MGM_Activity 查無資料 {}", ex);
            throw new ErrorResultException(9, "MGM_Activity 查無資料", ex);
        }
    }


    public List<MGM_Activity> ReadAll() {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).query(
                            "SELECT * FROM MGM_Activity", 
                            new BeanPropertyRowMapper<>(MGM_Activity.class));
        } catch (DataAccessException e) {
            logger.error("========<<< #MGM_Activity 查無資料");
            throw new ErrorResultException(9, "MGM_Activity 查無資料", e);
        } catch (Exception e) {
            throw new ErrorResultException(99, "MGM_Activity 尚未處理之錯誤", e);
        }
    }


    public MGM_Activity ReadByActUniqId(String actUniqId) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).queryForObject(
                            "SELECT * FROM MGM_Activity WHERE ACT_UNIQ_ID = ? ",
                            new BeanPropertyRowMapper<>(MGM_Activity.class),
                            new Object[] { actUniqId });
        } catch (DataAccessException ex) {
            logger.error("========<<< #MGM_Activity 查無資料 {}", ex);
            throw new ErrorResultException(9, "MGM_Activity 查無資料", ex);
        }
    }

    @Override
    public int Update(MGM_Activity fullItem) {
        return 0;
    }

    @Override
    public int Delete(MGM_Activity keyItem) {
        return 0;
    }

    @Override
    public List<MGM_Activity> Query(MGM_Activity keyItem) {
        return null;
    }

}
