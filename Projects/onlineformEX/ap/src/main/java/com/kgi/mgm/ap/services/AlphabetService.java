package com.kgi.mgm.ap.services;

import java.util.Random;

import org.springframework.stereotype.Service;

@Service
public class AlphabetService {

    /**
     * Copy from https://www.baeldung.com/java-random-string
     * @return
     */
	public String generatingRandomAlphabeticString() {

	    int leftLimit = 48; // letter '0'
	    int rightLimit = 122; // letter 'z'
        int targetStringLength = 8;

	    Random random = new Random();
        String generatedString = random.ints(leftLimit, rightLimit + 1)
            .filter( i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
	        .limit(targetStringLength)
	        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	        .toString();

        return generatedString;
    }
    

    static public void main(String[] args) {
        AlphabetService ma = new AlphabetService();
        ma.generatingRandomAlphabeticString();
    }
}
