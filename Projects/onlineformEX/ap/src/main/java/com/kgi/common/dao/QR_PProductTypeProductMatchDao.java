package com.kgi.common.dao;

import java.util.List;

import com.kgi.eopend3.common.dto.db.QR_PProductTypeProductMatch;
import com.kgi.onlineformex.ap.exception.ErrorResultException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class QR_PProductTypeProductMatchDao extends CRUDQDao<QR_PProductTypeProductMatch> {

    public static final String NAME = "QR_PProductTypeProductMatch" ;

    @Override
    public int Create(QR_PProductTypeProductMatch fullItem) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public QR_PProductTypeProductMatch Read(QR_PProductTypeProductMatch keyItem) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int Update(QR_PProductTypeProductMatch fullItem) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int Delete(QR_PProductTypeProductMatch keyItem) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public List<QR_PProductTypeProductMatch> Query(QR_PProductTypeProductMatch keyItem) {
        String sql = "SELECT * FROM " + NAME;
        try {
            return this.getJdbcTemplate().query(sql, new BeanPropertyRowMapper<>(QR_PProductTypeProductMatch.class));
        } catch (DataAccessException e) {
            logger.error("========<<< #{} 查無資料", NAME);
            throw new ErrorResultException(9, NAME + " 查無資料", e);
        } catch (Exception e) {
            throw new ErrorResultException(99, NAME + " 尚未處理之錯誤", e);
        }
    }


    public List<QR_PProductTypeProductMatch> QueryAll() {
        String sql = "SELECT * FROM " + NAME;
        try {
            return this.getJdbcTemplate().query(sql, new BeanPropertyRowMapper<>(QR_PProductTypeProductMatch.class));
        } catch (DataAccessException e) {
            logger.error("========<<< #{} 查無資料", NAME);
            throw new ErrorResultException(9, NAME + " 查無資料", e);
        } catch (Exception e) {
            throw new ErrorResultException(99, NAME + " 尚未處理之錯誤", e);
        }
    }
    
}
