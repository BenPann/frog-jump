package com.kgi.mgm.ap.dao;

import java.util.List;

import com.kgi.common.config.DataSourceConfig;
import com.kgi.common.dao.CRUDQDao;
import com.kgi.mgm.common.dto.db.MGM_Member;
import com.kgi.onlineformex.ap.exception.ErrorResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class MGM_MemberDao extends CRUDQDao<MGM_Member> {

    @Autowired
    private DataSourceConfig dsc;

    @Override
    public int Create(MGM_Member fullItem) {
        return this.getNamedParameterJdbcTemplate(dsc.mainDataSource2())
                .update("INSERT INTO MGM_Member (" + "CHANNEL_ID, " + "CUST_ID, " + "CUST_NAME, " + "CUST_PHONE, "
                        + "CUST_EMAIL, " + "CUST_BIRTH_4, " + "CUST_REGISTER_FLAG, " + "CUST_HAS_SET_PW, "
                        + "CUST_UNIT_ID, " + "CUST_EMP_ID, " + "CUST_PW, " + "IMPORT_TIME, " + "REGISTER_TIME, "
                        + "PW_SETTING_TIME, " + "STATUS)" + "VALUES (" + ":CHANNEL_ID, " + ":CUST_ID, " + ":CUST_NAME, "
                        + ":CUST_PHONE, " + ":CUST_EMAIL, " + ":CUST_BIRTH_4, " + "'N', " + ":CUST_HAS_SET_PW, "
                        + ":CUST_UNIT_ID, " + ":CUST_EMP_ID, " + ":CUST_PW, " + "getdate(), " + "NULL, " + "NULL, "
                        + "'00' )", new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(MGM_Member keyItem) {
        return 0;
    }

    @Override
    public List<MGM_Member> Query(MGM_Member keyItem) {
        return null;
    }

    /** SECTOR: READ */
    @Override
    public MGM_Member Read(MGM_Member keyItem) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).queryForObject(
                    "SELECT * FROM MGM_Member WHERE CUST_ID = ? ", new BeanPropertyRowMapper<>(MGM_Member.class),
                    new Object[] { keyItem.getCUST_ID() });
        } catch (DataAccessException ex) {
            logger.error("========<<< #MGM_Member 查無資料");
            throw new ErrorResultException(9, "MGM_Member 查無資料", ex);
        }
    }

    public MGM_Member ReadByCustId(String cust_id) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).queryForObject(
                    "SELECT * FROM MGM_Member WHERE CUST_ID = ? ", new BeanPropertyRowMapper<>(MGM_Member.class),
                    new Object[] { cust_id });
        } catch (DataAccessException ex) {
            logger.error("========<<< #MGM_Member 查無資料");
            throw new ErrorResultException(9, "您填寫的資料有誤，請重新輸入", ex);
        }
    }

    public MGM_Member ReadByCustIdNoError(String cust_id) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).queryForObject(
                    "SELECT * FROM MGM_Member WHERE CUST_ID = ? ", new BeanPropertyRowMapper<>(MGM_Member.class),
                    new Object[] { cust_id });
        } catch (DataAccessException ex) {
            logger.error("========<<< #MGM_Member 查無資料");
            return null;
        }
    }

    /** SECTOR: UPDATE */
    @Override
    public int Update(MGM_Member fullItem) {
        return 0;
    }

    public int Register(String cust_pw, String cust_id, String depart_id) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).update(
                    "UPDATE MGM_Member SET CUST_PW = ?, CUST_UNIT_ID = ?, CUST_REGISTER_FLAG = 'Y', PW_SETTING_TIME = getdate(), CUST_HAS_SET_PW = 0, REGISTER_TIME = getdate() WHERE CUST_ID = ? ",
                    cust_pw, depart_id, cust_id);
        } catch (DataAccessException e) {
            logger.error("========<<< #MGM_Member 查無資料");
            throw new ErrorResultException(9, "MGM_Member 查無資料", e);
        } catch (Exception e) {
            throw new ErrorResultException(99, "MGM_Member 尚未處理之錯誤", e);
        }
    }

    public int UpdatePassword(String cust_pw, String cust_id) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).update(
                    "UPDATE MGM_Member SET CUST_PW = ?, CUST_REGISTER_FLAG = 'Y', PW_SETTING_TIME = getdate(), CUST_HAS_SET_PW = 0 WHERE CUST_ID = ? ",
                    cust_pw, cust_id);
        } catch (DataAccessException e) {
            logger.error("========<<< #MGM_Member 查無資料");
            throw new ErrorResultException(9, "MGM_Member 查無資料", e);
        } catch (Exception e) {
            throw new ErrorResultException(99, "MGM_Member 尚未處理之錯誤", e);
        }
    }

    public int UpdateErrorCount(String cust_id, String count) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2())
                    .update("UPDATE MGM_Member set CUST_HAS_SET_PW = ? where CUST_ID = ? ", count, cust_id);
        } catch (Exception e) {
            throw new ErrorResultException(99, "MGM_Member 尚未處理之錯誤", e);
        }
    }

}
