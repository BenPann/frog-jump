package com.kgi.common.dao;

import java.util.List;

import com.kgi.eopend3.common.dto.db.EntryData;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class EntryDataDao extends CRUDQDao<EntryData> {

	@Override
	public int Create(EntryData fullItem) {
		String sql = "INSERT INTO EntryData (UniqId, UniqType, Entry, Member, Process, Browser, Platform, OS, Other, UTime) VALUES (?,?,?,?,?,?,?,?,?,GETDATE())";
		return this.getJdbcTemplate().update(sql, new Object[]{
				fullItem.getUniqId(), fullItem.getUniqType(),fullItem.getEntry(),fullItem.getMember(),fullItem.getProcess(),fullItem.getBrowser(),
				fullItem.getPlatform(),fullItem.getOs(),fullItem.getOther()
		});
	}

	@Override
	public EntryData Read(EntryData keyItem) {
		String sql = "Select * from EntryData where UniqId = ? ";
		return  this.getJdbcTemplate().queryForObject(sql, new Object[]{keyItem.getUniqId()}, new BeanPropertyRowMapper<>(EntryData.class));
	}

	@Override
	public int Update(EntryData fullItem) {
		return 0;
	}

	@Override
	public int Delete(EntryData keyItem) {
		String sql = "Delete From EntryData Where UniqId = ? ";
		return this.getJdbcTemplate().update(sql, new Object[]{ keyItem.getUniqId() });
	}

	@Override
	public List<EntryData> Query(EntryData keyItem) {
		return null;
	}	  
}
