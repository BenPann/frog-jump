package com.kgi.mgm.ap.dao;

import java.util.List;

import com.kgi.common.config.DataSourceConfig;
import com.kgi.common.dao.CRUDQDao;
import com.kgi.mgm.common.dto.db.MGM_Member;
import com.kgi.mgm.common.dto.db.QR_AgreeLog;
import com.kgi.onlineformex.ap.exception.ErrorResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class QR_AgreeLogDao extends CRUDQDao<QR_AgreeLog> {

    @Autowired
    private DataSourceConfig dsc;

    @Override
    public int Create(QR_AgreeLog fullItem) {
        return this.getNamedParameterJdbcTemplate(dsc.mainDataSource2())
                .update("INSERT INTO QR_AgreeLog (" + "ChannelId, " + "DepartId, " + "IdNo, " + "MemberId, "
                        + "AgreeTime, "  + "VALUES (" + ":ChannelId, " + ":DepartId, " + ":IdNo, "
                        + ":MemberId, "  + "getdate()) " , new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(QR_AgreeLog keyItem) {
        return 0;
    }

    @Override
    public List<QR_AgreeLog> Query(QR_AgreeLog keyItem) {
        return null;
    }

    /** SECTOR: READ */
    @Override
    public QR_AgreeLog Read(QR_AgreeLog keyItem) {
    	return keyItem;
    }

   

    /** SECTOR: UPDATE */
    @Override
    public int Update(QR_AgreeLog fullItem) {
        return 0;
    }

   

}
