package com.kgi.onlineformex.ap.pdf;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;

import com.itextpdf.text.pdf.PdfPageEvent;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

public abstract class BasePDFDocument {


	// SECTOR: VARIABLE
	/** 是否要加密PDF */
	private Boolean useEncrepyt = false;
	/** 是否要用浮水印 */
	private Boolean useWaterMark = false;
	/** 浮水印文字 */
	private String waterMarkText = "";
	/** 浮水印字型 */
	private String waterMarkFont = "";
	/** 是否要轉圖檔 */
	private Boolean pdfToImage = false;
	/** 是否要儲存至DB */
	private Boolean useSaveToDB = true;
	/** 是否發送郵件 */
	private Boolean useSendEMail = true;
	/** 報表的頁面事件(一般來說做footer用) */
	private PdfPageEvent pageEvent = null;
	/** 加密的密碼 */
	private String encryptPassword = "";
	/** 取得轉成PDF的Html */
	private String rawHtml = "";


	// SECTOR: ABSTRACT
	/** PDF流程前事件 */
	public abstract void beforePdfProcess() throws Exception;

	/** PDF流程後事件 */
	public abstract void afterPdfProcess() throws Exception;
	
	/**
	 * 將PDF轉成圖片List(base64格式)
	 * 
	 * @param base64List 圖片轉成byte[]之後的List
	 */
	public abstract void pdfToImage(List<byte[]> byteList);

	/**
	 * Html替換欄位
	 * 
	 * @param 原始html資料
	 * @return 替換資料後的Html
	 */
	public abstract String replaceField(String raw) throws Exception;

	/**
	 * 將PDF存到DB
	 * 
	 * @param pdfByte 最終產生報表的byte陣列
	 */
	public abstract void saveToDB(byte[] pdfByte);

	/**
	 * 傳送EMAIL
	 * 
	 * @param pdfByte 最終產生報表的byte陣列
	 */
	public abstract void sendEmail(byte[] pdfByte);


	// SECTOR: METHOD
    /** 使用appContext來建構autowired */
	public BasePDFDocument(ApplicationContext appContext) {
		AutowireCapableBeanFactory factory = appContext.getAutowireCapableBeanFactory();
		factory.autowireBean(this);
	}

	/** 取得外部filepath的內容 */
	public String getContentFromFilePath(InputStream filepath) throws IOException {
		return IOUtils.toString(filepath, Charset.forName("UTF-8"));
	}


	// SECTOR: GETTER SETTER
	public Boolean getUseEncrepyt() {
		return useEncrepyt;
	}

	public void setUseEncrepyt(Boolean useEncrepyt) {
		this.useEncrepyt = useEncrepyt;
	}

	public Boolean getUseWaterMark() {
		return useWaterMark;
	}

	public void setUseWaterMark(Boolean useWaterMark) {
		this.useWaterMark = useWaterMark;
	}

	public String getWaterMarkText() {
		return waterMarkText;
	}

	public void setWaterMarkText(String waterMarkText) {
		this.waterMarkText = waterMarkText;
	}

	public String getWaterMarkFont() {
		return waterMarkFont;
	}

	public void setWaterMarkFont(String waterMarkFont) {
		this.waterMarkFont = waterMarkFont;
	}

	public Boolean getPdfToImage() {
		return pdfToImage;
	}

	public void setPdfToImage(Boolean pdfToImage) {
		this.pdfToImage = pdfToImage;
	}

	public Boolean getUseSaveToDB() {
		return useSaveToDB;
	}

	public void setUseSaveToDB(Boolean useSaveToDB) {
		this.useSaveToDB = useSaveToDB;
	}

	public Boolean getUseSendEMail() {
		return useSendEMail;
	}

	public void setUseSendEMail(Boolean useSendEMail) {
		this.useSendEMail = useSendEMail;
	}

	public PdfPageEvent getPageEvent() {
		return pageEvent;
	}

	public void setPageEvent(PdfPageEvent pageEvent) {
		this.pageEvent = pageEvent;
	}

	public String getEncryptPassword() {
		return encryptPassword;
	}

	public void setEncryptPassword(String encryptPassword) {
		this.encryptPassword = encryptPassword;
	}

	public String getRawHtml() {
		return rawHtml;
	}

	public void setRawHtml(String rawHtml) {
		this.rawHtml = rawHtml;
	}

}
