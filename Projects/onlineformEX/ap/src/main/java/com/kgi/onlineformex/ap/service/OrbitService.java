package com.kgi.onlineformex.ap.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import com.kgi.common.config.GlobalConfig;
import com.kgi.common.util.ExportUtil;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.util.DateUtil;
import com.kgi.onlineformex.ap.config.OFEXConfig;
import com.kgi.onlineformex.ap.dao.FM_ApiLogDao;
import com.kgi.onlineformex.ap.dao.FM_CaseDataDao;
import com.kgi.onlineformex.ap.dao.FM_CaseDocumentDao;
import com.kgi.onlineformex.ap.helper.PDFHelper;
import com.kgi.onlineformex.common.OFEXContractConst;
import com.kgi.onlineformex.common.dto.db.FM_CaseData;
import com.kgi.onlineformex.common.dto.db.FM_CaseDocument;
import com.kgi.onlineformex.common.dto.orbit.OrbitRoot;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;


@Service
@PropertySource(value = {"file:${OFEX_CONFIG_PATH}/RPL.properties"}, encoding = "UTF-8")
public class OrbitService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FM_CaseDataDao fmcdDao;
    @Autowired
    private FM_CaseDocumentDao fmcDocDao;
    @Autowired
    private FM_ApiLogDao fmalDao;
    @Autowired
    private FTPService ftpService;
    @Autowired
    private GlobalConfig gc;
    @Autowired
    private OFEXConfig ofexConfig;

    @Value("${isMock}")
    private boolean isMock;
    @Value("${isDebug}")
    private boolean isDebug;


    // SECTOR: MEDIA UPLOAD
    public void uploadContractDataToOrbit() throws Exception {

        // CHAP: QUERY WHERE STATUS IS "03"
        List<FM_CaseData> fmcdList = 
            fmcdDao.QueryListByStatus(OFEXContractConst.CASE_STATUS_CREATE_SUCCESS, 
                                      OFEXContractConst.CASE_STATUS_SUBMIT_FAIL);

        if (fmcdList.size() == 0) {
            return;
        }

        logger.info("======**>> #JOB READY OF UPLOAD BY QUANTITY: {}", fmcdList.size());

        for (FM_CaseData fmcd : fmcdList) {

            logger.info("======**> #JOB content: {}", fmcd);

            int flagStep = 0;
            String
                idno         = fmcd.getIdno(),
                uniqId       = fmcd.getUniqId(),
                namePDF      = uniqId + ".pdf",
                productId    = "",
                branchId     = "9743";

            try {
                // STEP: 1. STATUS "11", prepared submit
                flagStep = 1;
                fmcdDao.UpdateStatusByUniqId(uniqId, OFEXContractConst.CASE_STATUS_SUBMITTING);


                // STEP: 2. CONTRACT UPLOAD TO FTP, 申請書檔案放在第一個
                FM_CaseDocument doc = fmcDocDao.queryDocumentByUniqIdMk3(uniqId);
                if (doc == null) {  // ERR. 沒產好pdf需查問題狀態 STATUS = "19"
                    logger.info("======**<< #JOB CONTRACT NOT FOUND.... {}", uniqId);
                    fmcdDao.UpdateStatusByUniqId(uniqId, OFEXContractConst.CASE_STATUS_SUBMIT_ERROR);
                    continue;
                }


                // STEP: 3. UPDLOAD PDF transfer from FM_CaseData
                flagStep = 2;
                doUploadPDFToMediaSystem(doc.getContent(), namePDF);


                // STEP: 4. UPLOAD IMAGE TO FTP
                flagStep = 6;
                PDFHelper pdfHelper = new PDFHelper();
                List<byte[]> imageList = pdfHelper.getImageList(doc.getContent());
                OrbitRoot orbitRoot = doCreateImageAndSomeStep(imageList, idno, productId, uniqId);


                // STEP: 5. UPLOAD XML TO MEDIA SYSTEM
                flagStep = 3;
                // LOG
                doUploadXmlFileToMediaSystem(fmcd, uniqId, branchId, orbitRoot);


                // STEP: 6. 契約書送件--呼叫訊息平台
                setMailHistoryByContract();


                // STEP: 7. 將案件狀態標註為12送件完成
                flagStep = 4;
                fmcdDao.UpdateStatusByUniqId(uniqId, OFEXContractConst.CASE_STATUS_SUBMIT_SUCCESS);


                // STEP: 8. 案件契約書後送完成，進行送件圖檔清檔
                flagStep = 5;
                String delete = ofexConfig.AdditionalPhotoDelete();
                if (delete.equals("1")) {
                    // contractDocDao.deleteContractDocPhoto(conNo);
                }


            } catch (Exception e) {
                exceptionMessageLog(flagStep);
                // 當連不到ws時，讓狀態直接完成，不影響後面流程。
                fmcdDao.UpdateStatusByUniqId(uniqId, flagStep == 3 ? "12" : "18");
            }
        }
    }


    // SECTOR: PROCESS
    /** STEP: 3. PDF transfer from FM_CaseData and upload */
    private byte[] doUploadPDFToMediaSystem(byte[] pdfBytes, String fileName) throws Exception {
        try {
            Path pdfPath = Paths.get(gc.PdfDirectoryPath + "/" + fileName);
            Files.write(pdfPath, pdfBytes);
        } catch (Exception e) {
            throw new Exception();
        }
        uploadSomethingToFTP("PDF", fileName, null, pdfBytes, null);
        return pdfBytes;
    }


    /** STEP: 4.3 IMAGE UPLOAD TO FTP */
    private OrbitRoot doCreateImageAndSomeStep(List<byte[]> imageList, String idno, String productId, String uniqId) throws IOException {

        OrbitRoot or = new OrbitRoot();
        String casePriority = "F033";

        for (int index = 0; index < imageList.size(); index++) {
            String nameFileImage = uniqId.concat("_" + String.format("%03d", index + 1)).concat(".jpg");
            byte[] image = imageList.get(index);

            try {
                Path imagePath = Paths.get(gc.PdfDirectoryPath + "/" + nameFileImage);
                Files.write( imagePath, image);
            } catch (Exception e) {
                e.printStackTrace();
            }

            uploadSomethingToFTP("IMAGE", nameFileImage, image, null, null);

            // 開始組xml row檔
            if (index == 0) { // XML檔 - 新增scanFiles第一列
                or.addConRow(nameFileImage, idno, productId, "8270", casePriority); }
            else {
                or.addSub(nameFileImage); }
        }
        return or;
    }


    /** STEP: 5. UPLOAD XML TO MEDIA SYSTEM */
    private void doUploadXmlFileToMediaSystem(FM_CaseData fmcd, String uniqId, String branchId4, OrbitRoot orbitRoot) throws IOException {

        InetAddress ip = InetAddress.getLocalHost();

        orbitRoot.orbitHeader.attachmentfrom(
            ip.getHostAddress(),
            "2",
            "",
            "9743",
            "9999",
            "webuser", 
            fmcd.getEmailAddress(),
            "",
            "",
            uniqId + ".pdf",
            "P002");

        String nameFileXML = uniqId.concat(".xml");
        StringBuilder sb = new StringBuilder();
        orbitRoot.toXML(sb);

        if (ofexConfig.isPDF_Export()) {
            ExportUtil.export(gc.PdfDirectoryPath + "/" + nameFileXML, sb.toString().getBytes());
        }

        fmcDocDao.UpdateSingleColumnByUniqId("XmlContent", sb.toString(), fmcd.getUniqId());

        uploadSomethingToFTP("XML", nameFileXML, null, null, sb);
    }


    /** STEP: 6. I don't need to do that */
    private int setMailHistoryByContract() {
        // MailHistory contractMailHistory = soapService.getContractCMBProgress(idno, sendData.getChtName().toString(), branchId4);
        // contractMailHistory.setUniqId(conNo);
        // mailHistoryDao.Create(contractMailHistory);
        return 0;
    }


    // SECTOR: METHOD
    private void uploadSomethingToFTP(String upType, String fileName, byte[] image, byte[] pdf, StringBuilder sb) throws IOException {
        
        if (isMock)  { return; }
        if (isDebug) { return; }

        boolean sendImageOK = true;
        String startTime = "";
        String endTime = "";
        String res = "";

        switch(upType) {
            case "IMAGE":
                sendImageOK = ftpService.uploadImgBase64File(fileName, image);
                break;
            case "PDF":
                sendImageOK = ftpService.uploadImgBase64File(fileName, pdf);
                break;
            case "XML":
                try {
                    startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
                    InputStream in = IOUtils.toInputStream(sb.toString(), "UTF-8");
                    sendImageOK = ftpService.uploadImgXml(fileName, in);
                    endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
                    res = sendImageOK ? "SUCCESSFUL" : "FAILED" ;
                } catch (Exception e) {
                    res = "FAILED";
                } finally {
                    fmalDao.Create(SystemConst.APILOG_TYPE_CALLOUT, fileName, "uploadXML", sb.toString(), res, startTime, endTime);
                }
                break;
        }

        if (!sendImageOK) {
            throw new RuntimeException("上傳ftp失敗!!");
        }
    }


    private void exceptionMessageLog(int flagStep) {
        switch (flagStep) {
            case 1:
                logger.info("執行送件失敗(連線失敗)");
                break;
            case 2:
                logger.info("圖片上傳ftp失敗");
                break;
            case 3:
                logger.info("呼叫影像WebService失敗");
                break;
            case 4:
                logger.info("更新送件完成失敗");
                break;
            case 5:
                logger.info("刪除契約書後送影像系統用圖檔失敗");
                break;
            case 6:
                logger.info("產生立約圖檔失敗");
                break;
            default:
                break;
        }
    }


}
