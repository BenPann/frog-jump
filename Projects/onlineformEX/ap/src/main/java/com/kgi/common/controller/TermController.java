package com.kgi.common.controller;

import javax.servlet.http.HttpServletRequest;

import com.kgi.common.service.TermsService;
import com.kgi.eopend3.common.dto.KGIHeader;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.onlineformex.ap.exception.ErrorResultException;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/terms")
public class TermController extends BaseController {

	@Autowired
	private TermsService ts;

	/**
	 * @param termsName
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{TermsName}", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
	@ResponseBody
	public String getTerms(@PathVariable("TermsName") String termsName) throws Exception {
		String terms = ts.getTerms(termsName);
		if (ESAPI.validator().isValidInput("TermController", terms, "Space", Integer.MAX_VALUE, false)) {
			return terms;
		} else {
			logger.error("ESAPI Valide false");
			return "";
		}
	}


	@PostMapping(value = "/getTermsList")
	public String getTermsList(@RequestBody String reqBody) throws Exception {
		logger.info("前台輸入的資料" + reqBody);		
		if(ESAPI.validator().isValidInput("TermController", reqBody, "SafeJson", Integer.MAX_VALUE, false)){
			return ts.getTermsList(reqBody);
		}else{
			return "[]";
		}
		
	}
	
	// @PostMapping(value = "/getProductList")
	// public String getProductList(@RequestBody String reqBody) throws Exception {
	// 	logger.info("前台輸入的資料" + reqBody);		
	// 	if(ESAPI.validator().isValidInput("TermController", reqBody, "SafeJson", Integer.MAX_VALUE, false)){
	// 		return termsService.getProductList(reqBody);
	// 	}else{
	// 		return "[]";
	// 	}
		
	// }

	/**
	 * @param request
	 * @param reqBody
	 * @return
	 */
	@PostMapping("/setContractMainTerms")
	public String setContractMainTerms(HttpServletRequest request, @RequestBody String reqBody) {
		logger.info("前台輸入的資料" + reqBody);
		try {
			KGIHeader header = this.getHeader(request);
			String uniqId = header.getUniqId();
			if (ESAPI.validator().isValidInput("TermController", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
				return ts.setContractMainTerms(reqBody, uniqId);
			} else {
				throw new ErrorResultException(9, "Invalid Input", null);
			}
		} catch (ErrorResultException e) {
			return WebResult.GetFailResult();
		}

	}
}