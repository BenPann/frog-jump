package com.kgi.onlineformex.ap.pdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.kgi.common.config.GlobalConfig;
import com.kgi.common.service.MailHunterService;
import com.kgi.common.util.ExportUtil;
import com.kgi.onlineformex.ap.config.OFEXConfig;
import com.kgi.onlineformex.ap.dao.FM_CaseDataDao;
import com.kgi.onlineformex.ap.dao.FM_CaseDocumentDao;
import com.kgi.onlineformex.ap.helper.NumConvertCN;
import com.kgi.onlineformex.common.dto.db.FM_CaseData;
import com.kgi.onlineformex.common.dto.db.FM_CaseDocument;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class ContractPDF extends BasePDFDocument {

	// PARAMETER
    private String _uniqId;
    private Boolean replaceBankSeal = true;
	private String templateVersion = "";
	
	// ITEM
    private Logger logger = LoggerFactory.getLogger(this.getClass());
	private FM_CaseData fmcd;

	@Autowired
	private GlobalConfig gc;
    @Autowired
	private OFEXConfig ofexConfig;
    @Autowired
	private FM_CaseDataDao fmcdDao;
	@Autowired
	private FM_CaseDocumentDao fmCaseDocDao;
	@Autowired
	private PageFooter footer;
	@Autowired
	private MailHunterService mhs;


    public ContractPDF(ApplicationContext appContext) {
	    super(appContext);
    }


    public void init(String... params) throws IOException {
        this._uniqId = params[0];
        this.setUseEncrepyt(false);
		this.setUseWaterMark(true);
        this.setWaterMarkText(ofexConfig.IMG_WATER_FONT());
        this.setWaterMarkFont(ofexConfig.PDF_FONT_WATER_TTF());
        this.setPdfToImage(false);
        this.setUseSaveToDB(true);
        this.setUseSendEMail(true);
        if (params[1] != null) {
			this.setReplaceBankSeal(Boolean.valueOf(params[1]));
		}
    }


	// SECTOR: CREATE
    @Override
    public void beforePdfProcess() {
        try {
            this.fmcd = fmcdDao.ReadByUniqId(_uniqId);
			this.setEncryptPassword(fmcd.getIdno());

			String 
				caseType = fmcd.getCaseType() == null ? "" : fmcd.getCaseType(),
				manageStore = fmcd.getManageStore();
			footer.caseType = caseType;

			this.setPageEvent(footer);

			String fileName = manageStore.equals("8861") ? gc.FileOfRplContract : gc.FileOfCreditCardContract;
			String htmlUrl = gc.StaticFilePath + fileName;

			switch (caseType) {
				case "1":
				case "2":
				case "4":
					templateVersion = ofexConfig.OFEXConVal("PDF.Contract.Loan.Version", "v1.0");
					break;
				default:
					throw new RuntimeException("錯誤的產品ID");
			}

			InputStream input = new FileInputStream(new File(htmlUrl));
			this.setRawHtml(this.getContentFromFilePath(input));

			logger.info("======**> StaticFilePath {} ", htmlUrl);
			logger.info("======**> this.getRawHtml() length is {} ", this.getRawHtml().length());

		} catch (RuntimeException rex) {
			throw rex;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
    }


	@Override
	public void afterPdfProcess() {}


	@Override
	public String replaceField(String raw) throws Exception {
		logger.info("========>>> Access the replaceField().... ");
		String 
			rawStr =
				fmcd.getManageStore().equals("8861")
					? replaceFieldByQuickLoan(raw) 
					: replaceFieldByCreditCard(raw);
		return rawStr;
	}


	// SECTOR: METHOD
	private String replaceFieldByQuickLoan(String raw) {
		logger.info("========>>> Access the replaceField() By Quick Loan.... ");

		String
			rawStr    = raw,
			userName  = fmcd.getUserName(),
			idno      = fmcd.getIdno(),
			modify    = fmcd.getCreditModify(),
			appNo     = fmcd.getASystemCaseNo(),
			agrDateYY = String.valueOf((Integer.parseInt(fmcd.getCreateTime().substring(0, 4)) - 1911)),
			agrDateMM = fmcd.getCreateTime().substring(5, 7),
			agrDateDD = fmcd.getCreateTime().substring(8, 10),
			agrDateHH = fmcd.getCreateTime().substring(11, 13),
			agrDateSS = fmcd.getCreateTime().substring(14, 16),
			ipAddress = fmcd.getIpAddress();

		String numberCN = new NumConvertCN(modify).getCnString();

		String[] arrMapKey = { "%agrDateYY%", "%agrDateMM%", "%agrDateDD%", "%ROCyear%", "%year%", "%month%", "%day%", "%hour%", "%minute%", "%user_cname%", "%new_contract_limit%", "%yearly_rate%", "%id_no%", "%app_no%", "%IPAddress%" };
		String[] arrMapVal = { agrDateYY, agrDateMM, agrDateDD, agrDateYY, agrDateYY, agrDateMM, agrDateDD, agrDateHH, agrDateSS, userName, numberCN, "yearly_rate", idno, appNo, ipAddress };
		List<String> listForMapKey = Arrays.asList(arrMapKey);
		List<String> listForMapVal = Arrays.asList(arrMapVal);

		Map<String, String> mapMk2 = IntStream.range(0, listForMapKey.size())
									.boxed()
									.collect(Collectors.toMap(i -> listForMapKey.get(i), i -> listForMapVal.get(i)));

		for (Map.Entry<String, String> entry : mapMk2.entrySet()) {
			rawStr = rawStr.replace(entry.getKey(), entry.getValue());
		}

		rawStr = replaceBankSeal(rawStr);

		return rawStr;
	}

	private String replaceFieldByCreditCard(String raw) {
		logger.info("========>>> Access the replaceField() By Credit Card.... ");

		String
			rawStr    = raw,
			userName  = fmcd.getUserName(),
			idno      = fmcd.getIdno(),
			modify    = fmcd.getCreditModify(),
			appNo     = fmcd.getASystemCaseNo(),
			agrDateYY = String.valueOf((Integer.parseInt(fmcd.getCreateTime().substring(0, 4)) - 1911)),
			agrDateMM = fmcd.getCreateTime().substring(5, 7),
			agrDateDD = fmcd.getCreateTime().substring(8, 10),
			agrDateHH = fmcd.getCreateTime().substring(11, 13),
			agrDateSS = fmcd.getCreateTime().substring(14, 16),
			ipAddress = fmcd.getIpAddress();

		String numberCN = new NumConvertCN(modify).getCnString();
					
		LocalDate today = LocalDate.of(Integer.parseInt(agrDateYY), Integer.parseInt(agrDateMM), Integer.parseInt(agrDateDD));
		LocalDate todayB5 = today.minus(5, ChronoUnit.DAYS);

		String
			agrYYB5 = String.valueOf(todayB5.getYear()),
			agrMMB5 = String.valueOf(todayB5.getMonthValue()),
			agrDDB5 = String.valueOf(todayB5.getDayOfMonth());

		String[] arrMapKey = { "%agrDateYYBefore5Day%", "%agrDateMMBefore5Day%", "%agrDateDDBefore5Day%", "%agrDateYY%", "%agrDateMM%", "%agrDateDD%", "%ROCyear%", "%year%", "%month%", "%day%", "%hour%", "%minute%", "%user_cname%", "%new_contract_limit%", "%yearly_rate%", "%id_no%", "%app_no%", "%IPAddress%" };
		String[] arrMapVal = { agrYYB5, agrMMB5, agrDDB5, agrDateYY, agrDateMM, agrDateDD, agrDateYY, agrDateYY, agrDateMM, agrDateDD, agrDateHH, agrDateSS, userName, numberCN, "yearly_rate", idno, appNo, ipAddress };
		List<String> listForMapKey = Arrays.asList(arrMapKey);
		List<String> listForMapVal = Arrays.asList(arrMapVal);

		Map<String, String> mapMk2 = IntStream.range(0, listForMapKey.size())
									.boxed()
									.collect(Collectors.toMap(i -> listForMapKey.get(i), i -> listForMapVal.get(i)));

		for (Map.Entry<String, String> entry : mapMk2.entrySet()) {
			rawStr = rawStr.replace(entry.getKey(), entry.getValue());
		}

		rawStr = replaceBankSeal(rawStr);

		return rawStr;
	}

	public String replaceBankSeal(String raw) {

		String contentBase64 = "";

		try {
			File file = new File( gc.StaticFilePath + "/image/BankSeal.base64" );
			contentBase64 = new String(Files.readAllBytes(file.toPath()));
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		
		System.out.println("### contentBase64.length() = " + contentBase64.length());
		return raw.replace(
			"<div style=\"display: none;\">[Bank_seal]</div>",
			"<img src=\"data:image/jpeg;base64," + contentBase64 + "\" " + "alt=\"\" style=\"width:120px;\" />"
		);
	}


	// SECTOR: TRANSFER
    @Override
	public void pdfToImage(List<byte[]> byteList) {
		System.out.println("pdfToImage");
		System.out.println(byteList.size());
	}


	@Override
	public void sendEmail(byte[] pdfByte) {
		mhs.sendContractMailHunter(_uniqId, fmcd.getEmailAddress(), pdfByte);
	}


	@Override
	public void saveToDB(byte[] pdfByte) {

		FM_CaseDocument fmCaseDoc = new FM_CaseDocument(this._uniqId, "", "mock.pdf");
		
		// 會有預覽資料 所以先讀取
		fmCaseDoc = fmCaseDocDao.Read(fmCaseDoc);

		// 真的查不到 就塞回預設值吧
		if (fmCaseDoc == null) {
			fmCaseDoc = new FM_CaseDocument(this._uniqId, "03", "mock.pdf");
		}
        fmCaseDoc.setContent(pdfByte);
		fmCaseDoc.setVersion(templateVersion);
		fmCaseDocDao.CreateOrUpdate(fmCaseDoc);

		// PDF檔 - @export 儲存至檔案系統
		if (ofexConfig.isPDF_Export()) {
			ExportUtil.export(gc.PdfDirectoryPath + "/" + _uniqId + ".pdf", pdfByte);
		}
	}


	// SECTOR: SETTER GETTER
    public Boolean getReplaceBankSeal() {
        return replaceBankSeal;
    }


    public void setReplaceBankSeal(Boolean replaceBankSeal) {
        this.replaceBankSeal = replaceBankSeal;
    }


}
