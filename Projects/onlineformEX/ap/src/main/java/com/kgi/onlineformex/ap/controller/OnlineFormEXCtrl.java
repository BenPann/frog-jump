package com.kgi.onlineformex.ap.controller;

import javax.servlet.http.HttpServletRequest;

import com.kgi.common.controller.BaseController;
import com.kgi.onlineformex.ap.mockData.MockDataService;
import com.kgi.onlineformex.ap.service.RPLApplyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/onlineform")
public class OnlineFormEXCtrl extends BaseController {

    @Autowired
    private RPLApplyService rplas;
    @Autowired
    private MockDataService mds;


    /**
     * Execute Login Going to A-System to check where exist the User who data insert
     * Job status 00
     * Write table when finish, FM_Apilog, can copy ED3_APILOG
     * 
     * @param reqBody
     * @return WebResultl
     */
    @PostMapping("/login")
    public String loginCheck(@RequestBody String reqBody) {
        logger.info("========> #CTRL Access login.... ");
        return rplas.loginASystem(reqBody);
    }


    /**
     * Login page for mobile device
     * 
     * @param reqBody
     * @return
     */
    @PostMapping("/freeLogin")
    public String mobileAutoLogin(@RequestBody String reqBody) {
        logger.info("========> #CTRL Access freeLogin.... ");
        return rplas.MobileFreeLogin(reqBody);
    }


    /**
     * Enter of page choose-protocol init
     * 
     *      Operate
     *      1. Read FM_CaseData in DB by uniqId to get value of A-System return
     * 
     * @param reqBody
     * @return
     */
    @PostMapping("/getContractsList")
    public String getContractsList(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("========> #CTRL getContractsList.... ");
        return rplas.getContractsListByASystem(this.getHeader(request).getUniqId(), reqBody);
    }


    @PostMapping("/getModifyRange")
    public String getModifyRange(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> #CRTL getModifyRange.... ");
        return rplas.getModifyRangeByDB();
    }


    @PostMapping("/creditModifyTerm")
    public String getCreditModifyTerm(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("========> #CRTL creditModifyTerm.... ");
        return rplas.getCreditModifyTerms(reqBody, this.getHeader(request).getUniqId());
    }



    @PostMapping("/setMailAndModify")
    public String setMailAndModify(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("========> #CRTL setMailAndModify.... ");
        return rplas.setMailAndModifyToDB( this.getHeader(request).getUniqId(), reqBody);
    }



    /**
     * Active allFinish page management
     * 
     *      Operate
     *      1. Call the API of PFD creator
     *      2. Call the API of MailHunter then send the mail
     * 
     * @param request
     * @param reqBody
     * @return
     */
    @PostMapping("/allFinish")
    public String doAllFinish(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("========> #CRTL Access the allFinish.... ");
        return rplas.getAllFinish( this.getHeader(request).getUniqId(), reqBody);
    }


    @PostMapping("/mockApi")
    public String mockApi(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("========> #CRTL MOCK API.... ");
        return mds.mockApiLogDAO();
    }

}