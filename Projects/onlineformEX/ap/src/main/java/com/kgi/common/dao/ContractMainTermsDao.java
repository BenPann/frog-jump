package com.kgi.common.dao;

import java.util.List;

import com.kgi.eopend3.common.dto.db.ContractMainTerms;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class ContractMainTermsDao extends CRUDQDao<ContractMainTerms> {

    @Override
    public int Create(ContractMainTerms fullItem) {
        return this.getJdbcTemplate().update(
                        "INSERT INTO ContractMainTerms (UniqId,TermSerial,IsAgree,CreateTime) VALUES (?,?,?,getDate())",
                        new Object[] { fullItem.getUniqId(),
                                       fullItem.getTermSerial(),
                                       fullItem.getIsAgree() });
    }

    @Override
    public ContractMainTerms Read(ContractMainTerms keyItem) {
        try {
            return this.getJdbcTemplate().queryForObject(
                            "SELECT * FROM ContractMainTerms WHERE UniqId = ? and TermSerial = ? ", 
                            new BeanPropertyRowMapper<>(ContractMainTerms.class),
                            new Object[] { keyItem.getUniqId(),
                                           keyItem.getTermSerial() });
        } catch (DataAccessException ex) {
			logger.error("ContractMainTerms 查無資料");
            return null;
        }
    }

    @Override
    public int Update(ContractMainTerms fullItem) {
        return this.getJdbcTemplate().update(
                        "Update ContractMainTerms Set IsAgree = ? where UniqId = ? and TermSerial = ? ",
                        new Object[] { fullItem.getIsAgree(),
                                       fullItem.getUniqId(),
                                       fullItem.getTermSerial() });
    }

    @Override
    public int Delete(ContractMainTerms keyItem) {
        return 0;
    }

    public ContractMainTerms getContractMainTermsByTermsName(String uniqId, String termsName) {
        try {
            return this.getJdbcTemplate().queryForObject(
                            "SELECT E.* FROM ContractMainTerms E inner join " +  TermsDao.NAME + " T on E.TermSerial=T.Serial " +
                            " WHERE E.UniqId = ? and T.Name = ? and T.Status='03' ",
                            new BeanPropertyRowMapper<>(ContractMainTerms.class),
                            new Object[] { uniqId, termsName });
        } catch (DataAccessException ex) {
			logger.error("ContractMainTerms 查無資料");
            return null;
        }
    }

    @Override
    public List<ContractMainTerms> Query(ContractMainTerms keyItem) {
        return null;
    }

}