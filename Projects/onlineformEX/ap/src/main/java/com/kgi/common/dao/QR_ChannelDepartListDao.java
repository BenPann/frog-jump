package com.kgi.common.dao;

import java.util.List;

import com.kgi.eopend3.common.dto.db.QR_ChannelDepartList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class QR_ChannelDepartListDao extends CRUDQDao<QR_ChannelDepartList> {

    @Override
    public int Create(QR_ChannelDepartList fullItem) {
        return 0;
    }

    @Override
    public QR_ChannelDepartList Read(QR_ChannelDepartList keyItem) {
        String sql = "SELECT * FROM QR_ChannelDepartList WHERE ChannelId = ? AND DepartId = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_ChannelDepartList.class),
                    new Object[] { keyItem.getChannelId(), keyItem.getDepartId() });
        } catch (DataAccessException ex) {
            return null;
        }
    }

    @Override
    public int Update(QR_ChannelDepartList fullItem) {
        return 0;
    }

    @Override
    public int Delete(QR_ChannelDepartList keyItem) {
        return 0;
    }

    @Override
    public List<QR_ChannelDepartList> Query(QR_ChannelDepartList keyItem) {
        String sql = "SELECT * FROM QR_ChannelDepartList WHERE ChannelId = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getChannelId() },
                    new BeanPropertyRowMapper<>(QR_ChannelDepartList.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }


    public List<QR_ChannelDepartList> QueryByChannelId(String channelId) {
        try {
            return this.getJdbcTemplate().query(
                        "SELECT * FROM QR_ChannelDepartList WHERE ChannelId = ?",
                        new Object[] { channelId },
                        new BeanPropertyRowMapper<>(QR_ChannelDepartList.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }

}