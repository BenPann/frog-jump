package com.kgi.common.service;

import com.kgi.common.dao.SNDao;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SNService {

	@Autowired
	private SNDao sndao;
	
	public String getpCode2566UniqId() {
		Integer sni = sndao.getSerialNumber("PCODE2566", "");
		Integer sn = sni % 10000;
		// 組成 格式 (流程別)(今日日期)(流水號)
		return StringUtils.leftPad(sn.toString(), 4, "0");
	}
	

	/**
	 * Create UniqId at login page
	 * 		1. When user identify successful, gave user a token of unique person for this active only.
	 * 		2. DB's usage of record and check its PK.
	 * 
	 * @return
	 */
	public String getUniqId() {		//先做數三
		String date = DateUtil.GetDateFormatString("yyyyMMdd");
		Integer sni = sndao.getSerialNumber("RPL", date);
		// 組成 格式 (流程別)(今日日期)(流水號)
		return "RPL".concat(date).concat(StringUtils.leftPad(sni.toString(), 5, "0"));
    }


	public String getAumAddCLSn() {
		String prefix = "CL" ;
		String date = DateUtil.GetDateFormatString() ;

//		String date = DateUtil.GetDateFormatString("yyyyMMdd");
		Integer sni = sndao.getSerialNumber("AUM_CL", date.substring(0, 6));
		// 組成 格式 (流程別)(今日日期)(流水號)
		return prefix.concat(date).concat(StringUtils.leftPad(sni.toString(), 6, "0"));
    }


}