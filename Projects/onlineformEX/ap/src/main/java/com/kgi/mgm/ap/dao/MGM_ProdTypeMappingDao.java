package com.kgi.mgm.ap.dao;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.kgi.common.config.DataSourceConfig;
import com.kgi.common.dao.CRUDQDao;
import com.kgi.mgm.common.dto.db.MGM_ProdTypeMapping;
import com.kgi.onlineformex.ap.exception.ErrorResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class MGM_ProdTypeMappingDao extends CRUDQDao<MGM_ProdTypeMapping> {


    @Autowired
    private DataSourceConfig dsc;


    @Override
    public int Create(MGM_ProdTypeMapping fullItem) {
        return 0;
    }

    @Override
    public MGM_ProdTypeMapping Read(MGM_ProdTypeMapping keyItem) {
        return null;
    }

    @Override
    public int Update(MGM_ProdTypeMapping fullItem) {
        return 0;
    }

    @Override
    public int Delete(MGM_ProdTypeMapping keyItem) {
        return 0;
    }

    @Override
    public List<MGM_ProdTypeMapping> Query(MGM_ProdTypeMapping keyItem) {
        return null;
    }


    public List<MGM_ProdTypeMapping> QueryAll() {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).query(
                            "SELECT * FROM MGM_ProdTypeMapping",
                            new BeanPropertyRowMapper<>(MGM_ProdTypeMapping.class));
        } catch (DataAccessException e) {
            logger.error("========<<< #MGM_ProdTypeMapping 查無資料");
            throw new ErrorResultException(9, "MGM_ProdTypeMapping 查無資料", e);
        } catch (Exception e) {
            throw new ErrorResultException(99, "MGM_ProdTypeMapping 尚未處理之錯誤", e);
        }
    }


    public Map<String, MGM_ProdTypeMapping> queryAllMk2() {
        try {
            List<MGM_ProdTypeMapping> mgmptm = 
                this.getJdbcTemplate(dsc.mainDataSource2()).query(
                                "SELECT * FROM MGM_ProdTypeMapping",
                                new BeanPropertyRowMapper<>(MGM_ProdTypeMapping.class));
            return mgmptm.stream().collect(Collectors.toMap(MGM_ProdTypeMapping::getPROD_TYPE, type -> type));
        } catch (DataAccessException e) {
            logger.error("========<<< #MGM_ProdTypeMapping 查無資料");
            throw new ErrorResultException(9, "MGM_ProdTypeMapping 查無資料", e);
        } catch (Exception e) {
            throw new ErrorResultException(99, "MGM_ProdTypeMapping 尚未處理之錯誤", e);
        }
    }


}
