package com.kgi.mgm.ap.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kgi.common.config.DataSourceConfig;
import com.kgi.common.dao.CRUDQDao;
import com.kgi.mgm.common.dto.db.MGM_Config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class MGM_ConfigDao extends CRUDQDao<MGM_Config> {


    @Autowired
    private DataSourceConfig dsc;


    @Override
    public int Create(MGM_Config fullItem) {
        return this.getJdbcTemplate(dsc.mainDataSource2()).update(
                    "INSERT INTO MGM_Config Values(?,?,?,GETDATE())",
                    new Object[] { fullItem.getKeyName(), fullItem.getKeyValue(), fullItem.getDescription() });
    }


    // SECTOR: QUERY
    @Override
    public List<MGM_Config> Query(MGM_Config keyItem) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).query(
                        "SELECT * FROM MGM_Config WHERE KeyName like ?+'.%'",
                        new Object[] { keyItem.getKeyName() },
                        new BeanPropertyRowMapper<>(MGM_Config.class));
        } catch (DataAccessException ex) {
        	ex.printStackTrace();
            return null;
        }
    }


    public List<MGM_Config> QueryAll() {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).query(
                        "SELECT * FROM MGM_Config",
                        new BeanPropertyRowMapper<>(MGM_Config.class));
        } catch (DataAccessException ex) {
        	ex.printStackTrace();
            return null;
        }
    }


	public Map<String, String> QueryToMap(MGM_Config keyItem) {
		List<MGM_Config> list = this.Query(keyItem);
		if (list == null) {
			return null;
		}
		Map<String, String> map = new HashMap<String, String>();
		for (MGM_Config con : list) {
			map.put(con.getKeyName(), con.getKeyValue());
		}
		return map;
    }


    // SECTOR: READ
    @Override
    public MGM_Config Read(MGM_Config keyItem) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).queryForObject(
                            "SELECT * FROM MGM_Config WHERE KeyName = ? ",
                            new BeanPropertyRowMapper<>(MGM_Config.class),
                            new Object[] { keyItem.getKeyName() });
        } catch (DataAccessException ex) {
            logger.error("========<<< #MGM_Config 查無資料 {}", ex);
            return null;
        }
    }


    public MGM_Config ReadByKeyName(String keyName) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).queryForObject(
                            "SELECT * FROM MGM_Config WHERE KeyName = ? ",
                            new BeanPropertyRowMapper<>(MGM_Config.class),
                            new Object[] { keyName });
        } catch (DataAccessException ex) {
            logger.error("========<<< #MGM_Config 查無資料 {}", ex);
            return null;
        }
    }


    public String ReadConfigValue(String name) {
		return ReadConfigValue(name, null);
	}


    public String ReadConfigValue(String name, String defaultValue) {
        MGM_Config con = this.Read(new MGM_Config(name));
        return con == null ? defaultValue : con.getKeyValue();
	}



    // SECTOR: UPDATE
    @Override
    public int Update(MGM_Config fullItem) {
        return this.getJdbcTemplate().update(
                    "UPDATE MGM_Config SET KeyValue = ? WHERE KeyName = ?",
                    new Object[] { fullItem.getKeyValue(), fullItem.getKeyName() });
    }


    // SECTOR: DELETE
    @Override
    public int Delete(MGM_Config keyItem) {
        return this.getJdbcTemplate().update(
                    "DELETE MGM_Config WHERE KeyName = ? ",
                    new Object[] { keyItem.getKeyName() });
    }


}
