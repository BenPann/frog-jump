package com.kgi.common.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.kgi.eopend3.common.dto.customDto.KGIProductListDto;
import com.kgi.eopend3.common.dto.db.KGIProductList;
import com.kgi.eopend3.common.dto.view.ProductListView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;


@Repository
public class KGIProductListDao extends CRUDQDao<KGIProductList> {
	
	@Autowired
	@Qualifier("mainDB")
	private DataSource dataSource;

	@Override
	public int Create(KGIProductList fullItem) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public KGIProductList Read(KGIProductList keyItem) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int Update(KGIProductList fullItem) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int Delete(KGIProductList keyItem) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<KGIProductList> Query(KGIProductList keyItem) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<KGIProductListDto> getProductList(ProductListView view) {
		String procedureCall = "{call sp_SelectProduct(?, ?, ?)}";
		Connection connection = null;
		List<KGIProductListDto> productList = new ArrayList<KGIProductListDto>();
		try {
			connection = dataSource.getConnection();
			CallableStatement callableSt = connection.prepareCall(procedureCall);
			callableSt.setString(1, view.getProductType());
			callableSt.setString(2, view.getCaseSource());
			callableSt.setString(3, view.getUserType());
			// Call Stored Procedure
			ResultSet resultSet = callableSt.executeQuery();
			while(resultSet.next()) {
				KGIProductListDto listItemDto = new KGIProductListDto();
				listItemDto.setEd3CommProdTitle(resultSet.getString(1));
				listItemDto.setEd3CommSubTitle(resultSet.getString(2));
				listItemDto.setEd3CommDesc(resultSet.getString(3));
				listItemDto.setEd3CommDisplayDefault(resultSet.getString(4));
				listItemDto.setEd3CommDisplayOption(resultSet.getString(5));
				listItemDto.setEd3CommSort(resultSet.getString(6));
				productList.add(listItemDto);
			}
		} catch (SQLException e) {
			//logger.error("getProductList", e);
		} finally {
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException e) {
					//logger.error("getProductList", e);
				}
		}
		return productList;
	}
//	
//	public List<KGIProductListDto> getProductList(ProductListView view) {
//		String sql = "SELECT CGProductTitle as ed3CommProdTitle" + " ,CGProductSubTitle as ed3CommSubTitle"
//				+ " ,CGProductDesc as ed3CommDesc" + " ,Select_Default as ed3CommDisplayDefault"
//				+ " ,Can_be_deleted as ed3CommDisplayOption" + " ,Sort as ed3CommSort"
//				+ " FROM dbo.CG_SelectProductList A" + " JOIN" + " dbo.CG_SelectProduct_Pproduct B" + " ON"
//				+ " A.CGProductID=B.CGProductID" + " JOIN" + " dbo.CG_SelectProduct_CustomerType C"
//				+ " ON C.CGProductID=B.CGProductID" + " WHERE PProductType= ? AND ChannelId= ? " + " AND UserTypeID= ?";
//		return this.getJdbcTemplate().query(sql,
//				new Object[] { view.getProductType(), view.getCaseSource(), view.getUserType() },
//				new BeanPropertyRowMapper<>(KGIProductListDto.class));
//	}


}
