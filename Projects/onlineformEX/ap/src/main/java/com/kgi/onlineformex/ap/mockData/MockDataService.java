package com.kgi.onlineformex.ap.mockData;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.esbdto.GMSCSMT24R2;
import com.kgi.eopend3.common.dto.esbdto.GMSCSMT24R2Content;
import com.kgi.eopend3.common.dto.response.HttpResp;
import com.kgi.eopend3.common.dto.response.LogFromValidation;
import com.kgi.eopend3.common.dto.response.SendOTPResp;
import com.kgi.eopend3.common.util.DateUtil;
import com.kgi.onlineformex.ap.dao.FM_ApiLogDao;
import com.kgi.onlineformex.common.dto.customDto.ASystemReq;
import com.kgi.onlineformex.common.dto.customDto.ASystemRes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class MockDataService {

    @Autowired
    private FM_ApiLogDao fmalDao;

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    // SECTOR: MOCK:
    /** 1. QUERY LIST */
    public List<GMSCSMT24R2Content> mockASystemDataManageMk1(ASystemReq req) {
        String
            startTime = "",
            endTime = "",
            res = "";
        try {
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");            
            List<GMSCSMT24R2Content> list = new ArrayList<GMSCSMT24R2Content>();
            GMSCSMT24R2Content item = createItemMk4();
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            list.add(item);
            res = new Gson().toJson(list);
            return list;
        } catch (Exception e) {
            logger.error("<<<<======== #MOCK ERROR 未知錯誤", e);
            return null;
        } finally {
            fmalDao.Create( "MOCK", "-", "GMSCSMT24R2", new Gson().toJson(req), res, startTime, endTime);
        }
    }


    /** 2. QUERY VOID */
    public List<GMSCSMT24R2Content> mockASystemDataManageMk2() {
        try {
            GMSCSMT24R2Content itemVoid = createItemMk6();
            List<GMSCSMT24R2Content> list = new ArrayList<GMSCSMT24R2Content>();
            list.add(itemVoid);
            return list;

        } catch (Exception e) {
            logger.error("<<<<======== #MOCK ERROR 未知錯誤", e);
            return null;
        }
    }


    /** 3. UPDATE */
    public List<GMSCSMT24R2Content> mockASystemDataManageMk3(String docType, String modify) {
        String
            reqJson = "",
            startTime = "",
            endTime = "";
        Gson gson = new Gson();
        List<GMSCSMT24R2Content> res = new ArrayList<GMSCSMT24R2Content>();
            
        try {
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            GMSCSMT24R2Content itemUpdate = createItemMk7(docType, modify);
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            res.add(itemUpdate);

        } catch (Exception e) {
            logger.error("========<<< #MOCK 未知錯誤", e);

        } finally {
            fmalDao.Create( "MOCK", "-", "GMSCSMT24R2", reqJson, gson.toJson(res), startTime, endTime);
        }
        return res;

    }


    /** 4. MOCK XML */    
    public List<GMSCSMT24R2Content> mockASystemDataManageMk4(ASystemReq reqAS) {

        String strXml = "<?xml version='1.0'?><SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:aml='http://www.cosmos.com.tw/aml' xmlns:arws='http://www.cosmos.com.tw/arws' xmlns:as400='http://www.cosmos.com.tw/as400' xmlns:bill='http://www.cosmos.com.tw/bill' xmlns:cardpac='http://www.cosmos.com.tw/cardpac' xmlns:cdi='http://www.cosmos.com.tw/cdi' xmlns:cics='http://www.cosmos.com.tw/cics' xmlns:crc='http://www.cosmos.com.tw/crc' xmlns:esb='http://www.cosmos.com.tw/esb' xmlns:fep='http://www.cosmos.com.tw/fep' xmlns:fund='http://www.cosmos.com.tw/fund' xmlns:gm='http://www.cosmos.com.tw/gm' xmlns:bns='http://www.cosmos.com.tw/kgib' xmlns:memo='http://www.cosmos.com.tw/memo' xmlns:mix='http://www.cosmos.com.tw/mix' xmlns:nefx='http://www.cosmos.com.tw/nefx' xmlns:sps='http://www.cosmos.com.tw/sps' xmlns:wsc='http://www.cosmos.com.tw/wsc' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'><SOAP-ENV:Body><esb:MsgRs><Header><TxnId>GMSCSMT24R2B</TxnId><Status><StatusCode>0</StatusCode><SystemId>GM</SystemId><Severity>INFO</Severity><StatusDesc></StatusDesc></Status><ClientId>ALNWEB</ClientId><ClientPwd></ClientPwd></Header><SvcRs xsi:type='gm:GMSCSMT24R2BSvcRsType'><SO2401> </SO2401><SO2402> </SO2402><SO2403>1</SO2403><SO2404>           </SO2404><SO2405>0000</SO2405><SO2406>0000</SO2406><SO2407>0000</SO2407><SO2408>0000000</SO2408><SO2409>0</SO2409><SO2410>                    </SO2410><SO2411>               </SO2411><SO2412>          </SO2412><SO2413>0000000</SO2413><SO2414>0000000</SO2414><SO2415>0000000</SO2415><SO2416>0000000</SO2416><SO2417>               </SO2417><SO2418>            </SO2418><SO2419>    </SO2419></SvcRs></esb:MsgRs></SOAP-ENV:Body></SOAP-ENV:Envelope>";
        String 
            idno = reqAS.getUserId(),
            startTime = "",
            endTime = "",
            req = "",
            res = "";

        try {
            logger.info("========>>> ESB SOAP A-SYSTEM REQEST" + req);
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            res       = strXml;
            endTime   = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            logger.info("========<<< ESB SOAP A-SYSTEM RESPONSE" + res);

            GMSCSMT24R2 gmscsmt24r2 = new GMSCSMT24R2();
            return gmscsmt24r2.parseXmlMk1(res);

        } catch (Exception e) {
            logger.error("========<<<< #API ERROR 未知錯誤", e);
            res = e.getMessage();
            return null;

        } finally {
            fmalDao.Create( idno, "-", "GMSCSMT24R2", req, res, startTime, endTime) ;
        }
    }


    /** 4. OTP */
    public HttpResp mockApiSendOTP(String txnId) {

        Gson gson = new Gson();

        // RESPONSE
        SendOTPResp sendOTPResp = new SendOTPResp();
        sendOTPResp.setCode("0000");
        sendOTPResp.setMessage("Helloween");
        sendOTPResp.setTxnID(txnId);
        sendOTPResp.setTxnDate(DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS"));

        // OTP RETURE
        LogFromValidation mockResOTP = new LogFromValidation();
        mockResOTP.setReq("Hello");
        mockResOTP.setRes("Again");

        // SEND TO FRONT
        HttpResp mockResp = new HttpResp();
        mockResp.setStatusCode(0);
        mockResp.setResponse(gson.toJson(sendOTPResp));

        return mockResp;
    }


    /** 5. APILOG TEST */
    public String mockApiLogDAO() {
        String 
            startTime = "",
            endTime = "",
            req = "MOCK REQUEST",
            res = "MOCK RESPONSE";
        try {
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            endTime   = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            return WebResult.GetResultString(0, "成功", res);
        } catch (Exception e) {
            return WebResult.GetResultString(99, "系統錯誤", null);
        } finally {
            fmalDao.Create( "", "-", "RPL MOCK TEST", req, res, startTime, endTime);
        }
    }


    // SECTOR: MOCK-DATA
    private GMSCSMT24R2Content createItemMk1() {
        GMSCSMT24R2Content item1 = new GMSCSMT24R2Content();
            item1.setSO2401("I");
            item1.setSO2402("1");
            item1.setSO2403("1");
            item1.setSO2404("A123456789");
            item1.setSO2405("8861");
            item1.setSO2406("1");
            item1.setSO2407("42");
            item1.setSO2408("1984");
            item1.setSO2409("SEQ");
            item1.setSO2410("Alpha");
            item1.setSO2411("ALPHA000001");
            item1.setSO2412("契變書簽署");
            item1.setSO2413("100000");
            item1.setSO2414("200000");
            item1.setSO2415("");
            item1.setSO2416("");
            item1.setSO2417("DontLookAbyss@look.you.too.com");
            item1.setSO2418("0952000001");
        return item1;
    }


    private GMSCSMT24R2Content createItemMk2() {
        GMSCSMT24R2Content item2 = new GMSCSMT24R2Content();
            item2.setSO2401("I");
            item2.setSO2402("1");
            item2.setSO2403("2");
            item2.setSO2404("A123456789");
            item2.setSO2405("8861");
            item2.setSO2406("1");
            item2.setSO2407("42");
            item2.setSO2408("1984");
            item2.setSO2409("SEQ");
            item2.setSO2410("Beta");
            item2.setSO2411("BETA000002");
            item2.setSO2412("調額同意書簽署");
            item2.setSO2413("140000");
            item2.setSO2414("140000");
            item2.setSO2415("280000");
            item2.setSO2416("");
            item2.setSO2417("DontLookAbyss@look.you.too.com");
            item2.setSO2418("0911000002");
        return item2;
    }


    protected GMSCSMT24R2Content createItemMk3() {
        GMSCSMT24R2Content item3 = new GMSCSMT24R2Content();
            item3.setSO2401("I");
            item3.setSO2402("1");
            item3.setSO2403("3");
            item3.setSO2404("A123456789");
            item3.setSO2405("8861");
            item3.setSO2406("1");
            item3.setSO2407("42");
            item3.setSO2408("1984");
            item3.setSO2409("SEQ");
            item3.setSO2410("Charlie");
            item3.setSO2411("GAMMA000003");
            item3.setSO2412("補財力證明及調額同意書簽署");
            item3.setSO2413("420000");
            item3.setSO2414("420000");
            item3.setSO2415("620000");
            item3.setSO2416("");
            item3.setSO2417("DontLookAbyss@look.you.too.com");
            item3.setSO2418("0972000003");
        return item3;
    }


    private GMSCSMT24R2Content createItemMk4() {
        GMSCSMT24R2Content item4 = new GMSCSMT24R2Content();
            item4.setSO2401("I");
            item4.setSO2402("1");
            item4.setSO2403("4");
            item4.setSO2404("A123456789");
            item4.setSO2405("8861");
            item4.setSO2406("1");
            item4.setSO2407("42");
            item4.setSO2408("1984");
            item4.setSO2409("SEQ");
            item4.setSO2410("Delta");
            item4.setSO2411("DELTA000004");
            item4.setSO2412("契變書簽署及調額同意書簽署");
            item4.setSO2413("150000");
            item4.setSO2414("400000");
            item4.setSO2415("300000");
            item4.setSO2416("");
            item4.setSO2417("DontLookAbyss@look.you.too.com");
            item4.setSO2418("0938000004");
        return item4;
    }
    

    protected GMSCSMT24R2Content createItemMk5() {
        GMSCSMT24R2Content item5 = new GMSCSMT24R2Content();
            item5.setSO2401("I");
            item5.setSO2402("1");
            item5.setSO2403("5");
            item5.setSO2404("A123456789");
            item5.setSO2405("8861");
            item5.setSO2406("1");
            item5.setSO2407("42");
            item5.setSO2408("1984");
            item5.setSO2409("SEQ");
            item5.setSO2410("Echo");
            item5.setSO2411("EPSILON000006");
            item5.setSO2412("補財力證明及契變書簽署及調額同意書簽署");
            item5.setSO2413("300000");
            item5.setSO2414("450000");
            item5.setSO2415("");
            item5.setSO2416("");
            item5.setSO2417("DontLookAbyss@look.you.too.com");
            item5.setSO2418("0959000005");
        return item5;
    }


    public GMSCSMT24R2Content createItemMk6() {
        GMSCSMT24R2Content item6 = new GMSCSMT24R2Content();
            item6.setSO2401("I");
            item6.setSO2402(" ");
            item6.setSO2403(" ");
            item6.setSO2404("A123456789");
            item6.setSO2405("8861");
            item6.setSO2406("1");
            item6.setSO2407("42");
            item6.setSO2408("1984");
            item6.setSO2409("SEQ");
            item6.setSO2410("VOID");
            item6.setSO2411("");
            item6.setSO2412("");
            item6.setSO2413("");
            item6.setSO2414("");
            item6.setSO2415("");
            item6.setSO2416("");
            item6.setSO2417("");
            item6.setSO2418("");
        return item6;
    }


    private GMSCSMT24R2Content createItemMk7(String docType, String modify) {
        GMSCSMT24R2Content item7 = new GMSCSMT24R2Content();
            item7.setSO2401("U");
            item7.setSO2402("1");
            item7.setSO2403(docType);
            item7.setSO2404("A123456789");
            item7.setSO2405("8861");
            item7.setSO2406("1");
            item7.setSO2407("42");
            item7.setSO2408("1984");
            item7.setSO2409("SEQ");
            item7.setSO2410("Beta");
            item7.setSO2411("BETA000002");
            item7.setSO2412("調額同意書簽署");
            item7.setSO2413("235000");
            item7.setSO2414("235000");
            item7.setSO2415(modify);
            item7.setSO2416("20200731");
            item7.setSO2417("DontLookAbyss@look.you.too.com");
            item7.setSO2418("0911000002");
        return item7;
    }


    public ASystemRes createItemMk8() {
        ASystemRes res = new ASystemRes();
        res.setCaseType("1");
        return res;
    }


}
