package com.kgi.onlineformex.ap.helper;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPageEvent;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.HTML;
import com.itextpdf.tool.xml.html.TagProcessorFactory;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import com.kgi.onlineformex.ap.pdf.BasePDFDocument;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;


public class PDFHelper {


    private BasePDFDocument pdfDocument;


    public PDFHelper() {}


    public PDFHelper(BasePDFDocument doc) {
        this.pdfDocument = doc;
    }


    // SECTOR: PROCESS
    /** 匯出轉換過的Html */
    public String ExportHtml() throws Exception {
        if (this.pdfDocument == null) {
            throw new IOException("尚未傳入報表類別");
        }

        // 取得Html
        String raw = this.pdfDocument.getRawHtml();
        if (raw == null || raw.trim().equals("")) {
            throw new IOException("尚未傳入Html");
        }

        System.out.println("====###> ExportHtml");
        System.out.println("====###> raw.length()=" + raw.length());
        // Replace
        return this.pdfDocument.replaceField(raw.trim());
    }


    /**
     * 此方法會將傳入類別的的資料 轉成pdf 以及所設定的相關動作
     * 
     * @throws org.ghost4j.document.DocumentException
     * @return PDF的byte陣列
     * @throws Exception
     */
    public byte[] PdfProcess() throws Exception {
        if (this.pdfDocument == null) {
            throw new IOException("尚未傳入報表類別");
        }

        ByteArrayOutputStream pdfStream = null;

        try {
            // VERSION 1. Type is ContractPDF
            this.pdfDocument.beforePdfProcess();
            // 產生暫存檔
            pdfStream = new ByteArrayOutputStream();
            // 產生PDF
            generatePDF(pdfStream, this.ExportHtml(), this.pdfDocument.getPageEvent());

            // 浮水印
            if (this.pdfDocument.getUseWaterMark()) {
                addWaterMark(pdfStream, this.pdfDocument.getWaterMarkText(), this.pdfDocument.getWaterMarkFont());
            }

            // 加密前先做轉圖片
            if (pdfDocument.getPdfToImage()) {
                this.pdfDocument.pdfToImage(this.getImageList(pdfStream.toByteArray()));
            }

            // 加密
            if (this.pdfDocument.getUseEncrepyt()) {
                encryptPDF(pdfStream, this.pdfDocument.getEncryptPassword());
            }

            // 儲存至DB
            if (this.pdfDocument.getUseSaveToDB()) {
                this.pdfDocument.saveToDB(pdfStream.toByteArray());
            }

            // 發送Mail
            // if (this.pdfDocument.getUseSendEMail()) {
            //     this.pdfDocument.sendEmail(pdfStream.toByteArray());
            // }

            this.pdfDocument.afterPdfProcess();

        } finally {
            if (pdfStream != null)
            pdfStream.close();
        }
        return pdfStream.toByteArray();
    }


    // SECTOR: METHOD
    /** BASIC CREATE PDF */
    private void generatePDF(ByteArrayOutputStream pdfFile, String exporthtml, PdfPageEvent event) throws DocumentException, UnsupportedEncodingException, IOException {

        Document document = null;

        try {

            // STEP: 1
            document = new Document();

            // STEP: 2
            PdfWriter writer = PdfWriter.getInstance(document, pdfFile);
            if ( event != null ) {
                writer.setPageEvent(event);
            }

            // STEP: 3 HTML
            document.open();

            // 3.1 CSS
            CSSResolver cssResolver = XMLWorkerHelper.getInstance().getDefaultCssResolver(true);

            TagProcessorFactory tagProcessorFactory = Tags.getHtmlTagProcessorFactory();
            tagProcessorFactory.removeProcessor(HTML.Tag.IMG);
            tagProcessorFactory.addProcessor(new ImageTagProcessor(), HTML.Tag.IMG);

            // 3.2 HTML
            HtmlPipelineContext hpc = new HtmlPipelineContext(null);
            hpc.setTagFactory(Tags.getHtmlTagProcessorFactory());
            hpc.setImageProvider(new Base64ImageProvider());

            // 3.3 Pipeline
            PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
            HtmlPipeline html = new HtmlPipeline(hpc, pdf);
            CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);

            // 3.4 XML worker
            XMLWorker worker = new XMLWorker(css, true);
            XMLParser p = new XMLParser(worker);

            // STEP: 4
            try {
                p.parse(new ByteArrayInputStream(exporthtml.getBytes("UTF-8")), Charset.forName("UTF-8"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } finally {
            if ( document != null ) {
                document.close();
            }
        }

    }


    /** WATERMARK */
    private void addWaterMark(ByteArrayOutputStream pdfFile, String waterMarkText, String waterMarkFont)
        throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        ByteArrayOutputStream outputStream = null;
        try {
            outputStream = new ByteArrayOutputStream();
            PdfReader pdfReader = new PdfReader(pdfFile.toByteArray());
            PdfStamper pdfStamper = new PdfStamper(pdfReader, outputStream);

            for (int i = 1, pdfPageSize = pdfReader.getNumberOfPages() + 1; i < pdfPageSize; i++) {
                PdfContentByte pageContent = pdfStamper.getOverContent(i);
                BaseFont bfc = BaseFont.createFont(waterMarkFont, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                // BaseFont bfc = BaseFont.createFont(waterMarkFont, null, BaseFont.NOT_EMBEDDED);

                // 設定透明度
                PdfGState gstate = new PdfGState();
                gstate.setFillOpacity(0.5f);
                gstate.setStrokeOpacity(1f);
                pageContent.setGState(gstate);
                pageContent.beginText();
                pageContent.setFontAndSize(bfc, 32);
                pageContent.setColorFill(BaseColor.GRAY);
                pageContent.showTextAligned(Element.ALIGN_CENTER, waterMarkText, document.getPageSize().getWidth() / 2,
                        document.getPageSize().getHeight() / 2, -45); // 浮水印角度 45
                pageContent.endText();
            }
            pdfStamper.close();

            pdfFile.reset();
            pdfFile.write(outputStream.toByteArray());
            pdfFile.flush();
        } finally {
            if (outputStream != null)
                outputStream.close();
        }
    }


    /** 取得PDF轉成圖片的列表 */
    public List<byte[]> getImageList(byte[] pdfData) throws IOException {
        List<byte[]> listByte = new ArrayList<byte[]>();
        PDDocument document = null;
        try {
            document = PDDocument.load(pdfData);
            PDFRenderer pdfRenderer = new PDFRenderer(document);
            for (int page = 0; page < document.getNumberOfPages(); ++page) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {
                    BufferedImage bimg = pdfRenderer.renderImageWithDPI(page, 300);
                    ImageIO.write(bimg, "jpg", bos);
                    listByte.add(bos.toByteArray());
                } finally {
                    if (bos != null)
                        bos.close();
                }
            }
        } finally {
            document.close();
        }
        return listByte;
    }


    /** ENCRYPT */
    private void encryptPDF(ByteArrayOutputStream pdfFile, String encryptPassword) throws DocumentException, IOException {
        ByteArrayOutputStream outputStream = null;
        try {
            outputStream = new ByteArrayOutputStream();
            PdfReader reader = new PdfReader(pdfFile.toByteArray());
            PdfStamper stamper = new PdfStamper(reader, outputStream);
            stamper.setEncryption(encryptPassword.getBytes(), encryptPassword.getBytes(), PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_256);
            stamper.close();
            pdfFile.reset();
            pdfFile.write(outputStream.toByteArray());
            pdfFile.flush();
        } finally {
            if (outputStream != null)
                outputStream.close();
        }
    }


    /** DECRYPT
     * 如果有特殊需求 將加密的byte傳進來這邊作解密PDF的動作
     * 
     * @param 加密的PDF資料
     * @param 解密密碼
     */
    public byte[] decryptPDF(byte[] encryptPDFData, String encryptPassword) throws IOException, DocumentException {

        ByteArrayOutputStream outputStream = null;
        PdfReader reader = null;
        PdfStamper stamper = null;
        try {
            outputStream = new ByteArrayOutputStream();
            reader = new PdfReader(encryptPDFData, encryptPassword.getBytes());
            stamper = new PdfStamper(reader, outputStream);
            outputStream.flush();
        } finally {
            if (stamper != null)
                stamper.close();
            if (reader != null)
                reader.close();
            if (outputStream != null)
                outputStream.close();
        }
        return outputStream.toByteArray();
    }

}
