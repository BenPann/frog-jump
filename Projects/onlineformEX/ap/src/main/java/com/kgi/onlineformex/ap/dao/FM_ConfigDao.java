package com.kgi.onlineformex.ap.dao;

import java.util.List;

import com.kgi.common.dao.CRUDQDao;
import com.kgi.onlineformex.common.dto.db.FM_Config;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class FM_ConfigDao extends CRUDQDao<FM_Config> {


    @Override
    public int Create(FM_Config fullItem) {
        return 0;
    }


    @Override
    public FM_Config Read(FM_Config keyItem) {
        try {
            return this.getJdbcTemplate().queryForObject(
                        "SELECT * FROM FM_Config WHERE KeyName = ? ",
                        new BeanPropertyRowMapper<>(FM_Config.class), 
                        new Object[] { keyItem.getKeyName() });
        } catch (DataAccessException ex) {
			logger.error("========<<<< Config NOT FOUND");
            return null;
        }
    }

    @Override
    public int Update(FM_Config fullItem) {
        return 0;
    }

    @Override
    public int Delete(FM_Config keyItem) {
        return 0;
    }

    @Override
    public List<FM_Config> Query(FM_Config keyItem) {
        return null;
    }

    public String ReadConfigValue(String name) {
		return ReadConfigValue(name, null);
	}


    public String ReadConfigValue(String name, String defaultValue) {
		FM_Config con = this.Read(new FM_Config(name));
		if (con == null) {
			return defaultValue;
		}
		return con.getKeyValue();
	}
    
}