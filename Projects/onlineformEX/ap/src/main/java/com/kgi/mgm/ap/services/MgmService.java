package com.kgi.mgm.ap.services;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kgi.common.config.GlobalConfig;
import com.kgi.common.dao.HttpDao;
import com.kgi.common.dao.QR_ChannelDepartListDao;
import com.kgi.common.dao.QR_ShortUrlDao;
import com.kgi.common.service.AccountInfoService;
import com.kgi.common.service.CryptoService;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.db.QR_ChannelDepartList;
import com.kgi.eopend3.common.dto.db.QR_ShortUrl;
import com.kgi.eopend3.common.dto.esbdto.BNS08508100;
import com.kgi.mgm.ap.config.MgmConfig;
import com.kgi.mgm.ap.dao.MGM_ActivityDao;
import com.kgi.mgm.ap.dao.MGM_ActivityShortUrlDao;
import com.kgi.mgm.ap.dao.MGM_ConfigDao;
import com.kgi.mgm.ap.dao.MGM_GiftApplyDataDao;
import com.kgi.mgm.ap.dao.MGM_GiftListDao;
import com.kgi.mgm.ap.dao.MGM_MemberActivityDao;
import com.kgi.mgm.ap.dao.MGM_MemberDao;
import com.kgi.mgm.ap.dao.MGM_ProdTypeMappingDao;
import com.kgi.mgm.ap.dao.QR_AgreeLogDao;
import com.kgi.mgm.common.dto.db.MGM_Activity;
import com.kgi.mgm.common.dto.db.MGM_ActivityShortUrl;
import com.kgi.mgm.common.dto.db.MGM_Config;
import com.kgi.mgm.common.dto.db.MGM_GiftApplyData;
import com.kgi.mgm.common.dto.db.MGM_GiftList;
import com.kgi.mgm.common.dto.db.MGM_Member;
import com.kgi.mgm.common.dto.db.MGM_MemberActivity;
import com.kgi.mgm.common.dto.db.MGM_ProdTypeMapping;
import com.kgi.mgm.common.dto.db.QR_AgreeLog;
import com.kgi.mgm.common.dto.response.MgmActivityNShortUrlResp;
import com.kgi.mgm.common.dto.response.MgmGiftListResponse;
import com.kgi.mgm.common.dto.response.MgmSummaryBonusResponse;
import com.kgi.mgm.common.dto.response.MgmSummaryCaseResponse;
import com.kgi.mgm.common.dto.view.ForgotPassword;
import com.kgi.mgm.common.dto.view.MgmCustomerLogin;
import com.kgi.mgm.common.dto.view.MgmSummaryBonus;
import com.kgi.mgm.common.dto.view.MgmSummaryBonusData;
import com.kgi.mgm.common.dto.view.MgmSummaryCase;
import com.kgi.mgm.common.dto.view.MgmSummaryCaseData;
import com.kgi.mgm.common.dto.view.QueryActivity;
import com.kgi.mgm.common.dto.view.QueryConfig;
import com.kgi.mgm.common.dto.view.ResetPassword;
import com.kgi.onlineformex.ap.exception.ErrorResultException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class MgmService {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    GlobalConfig globalConfig;
    @Autowired
    private MGM_MemberActivityDao mgmmaDao;
    @Autowired
    private MGM_MemberDao mgmmDao;
    @Autowired
    private MGM_ConfigDao mgmcDao;
    @Autowired
    private MGM_ActivityDao mgmaDao;
    @Autowired
    private MGM_ActivityShortUrlDao mgmasuDao;
    @Autowired
    private MGM_GiftListDao mgmGiftListDao;
    @Autowired
    private MGM_GiftApplyDataDao mgmGiftApplyDataDao;
    @Autowired
    private AlphabetService ma;
    @Autowired
    private CryptoService cs;
    @Autowired
    private QR_ShortUrlDao qrsuDao;
    @Autowired
    private MGM_ProdTypeMappingDao mgmptmDao;
    @Autowired
    private QR_ChannelDepartListDao qrcdlDao;
    
    private QR_AgreeLogDao qr_AgreeLogDao; 
    @Autowired
    private HttpDao httpDao;
    @Autowired
    private AccountInfoService ais;
    @Autowired
    MgmConfig config;

    private final String[] MGM_SUMMARYCASE_SORT_CONDITION_ARR = { "CC", "DT", "RPL", "PL", "ML" };

    private final String MASK_ID = "***";
    private final String MASK_NAME = "O";

    public String checkByCustomerId(String reqBody) {
        logger.info("========>> #SERVICE Access the checkByCustomerId().... ");
        try {
            Gson gson = new Gson();
            MgmCustomerLogin cl = gson.fromJson(reqBody, MgmCustomerLogin.class);
            String customerId = cl.getCUST_ID();
            MGM_Member resMgmm = mgmmDao.ReadByCustIdNoError(customerId);

            if (resMgmm == null || resMgmm.getCUST_REGISTER_FLAG().equals("N")) {
                return WebResult.GetResultString(9, "?�無該�??�員資�?", null);
            } else {
                return WebResult.GetResultString(0, "?�詢?��?", null);
            }
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("<<======== #SERIVCE ERROR ?��??��??�誤.... ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    public String checkMemberDetailByCustomerId(String reqBody) {
        logger.info("========>> #SERVICE Access the checkMemberDetailByCustomerId().... ");
        try {
            Gson gson = new Gson();

            MgmCustomerLogin cl = gson.fromJson(reqBody, MgmCustomerLogin.class);
            String customerId = cl.getCUST_ID();

            // CHECK ACCOUNT
            BNS08508100 bns85081 = ais.BNS08508100Check(customerId, "19850121");

            if (bns85081 == null) {
                return WebResult.GetResultString(9, "?�填寫�?資�??�誤，�??�新輸入", null);
            }

            MGM_Member resMgmm = mgmmDao.ReadByCustIdNoError(customerId);

            // NOTE: FIRST LOGIN DB NO DATA
            if (resMgmm == null) {
                // SAVE DATA INTO MGM_MEMBER
                MGM_Member item = new MGM_Member();
                item.setCHANNEL_ID(cl.getCHANNEL_ID());
                // item.setCHANNEL_ID("CL");
                item.setCUST_ID(customerId);
                item.setCUST_NAME(bns85081.getCustName());
                if (bns85081.getMobileNo() == null) {
                    item.setCUST_PHONE(bns85081.getMobileNo());
                } else {
                    item.setCUST_PHONE(bns85081.getMobileNo().trim());
                }
                item.setCUST_EMAIL(bns85081.getEmailAddress());
                item.setCUST_BIRTH_4(bns85081.getBirthDate1());
                item.setCUST_HAS_SET_PW("0");
                mgmmDao.Create(item);
                resMgmm = mgmmDao.ReadByCustIdNoError(customerId);
            }

            Integer errCount = Integer.parseInt(resMgmm.getCUST_HAS_SET_PW());
            if (errCount >= 3) {
                return WebResult.GetResultString(4, config.passwordErrorMsgCount(), null);
            }

            String res = gson.toJson(resMgmm), inputPw = cl.getPASSWORD(), custPw = resMgmm.getCUST_PW();

            // NOTE: FIRST LOGIN
            if (custPw == null || custPw.trim().equals("")) {
                if (!resMgmm.getCUST_BIRTH_4().equals(inputPw)) {
                    mgmmDao.UpdateErrorCount(customerId, String.valueOf(++errCount));
                    return WebResult.GetResultString(4, config.passwordErrorMsgInput(), null);
                } else {
                    mgmmDao.UpdateErrorCount(customerId, "0");
                    return WebResult.GetResultString(1, "SUCCESSFUL", res);
                }

                // NOTE: ALREADY
            } else {
                String dpw = custPw, epw = cs.shaEncode(customerId + inputPw);

                if (!dpw.equals(epw)) {
                    mgmmDao.UpdateErrorCount(customerId, String.valueOf(++errCount));
                    return WebResult.GetResultString(4, config.passwordErrorMsgInput(), null);
                }

                logger.info("========<< #SERVICE {}", res);
                mgmmDao.UpdateErrorCount(customerId, "0");
                return WebResult.GetResultString(0, "SUCCESSFUL", res);
            }

        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("<<======== #SERIVCE ERROR ?��??��??�誤.... ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    public String resetPasswordProcess(String reqBody) {
        logger.info("========>> #SERVICE Access the resetPasswordProcess().... ");
        try {
            MgmCustomerLogin cl = new Gson().fromJson(reqBody, MgmCustomerLogin.class);
            String customerId = cl.getCUST_ID(), pass = cl.getPASSWORD(), empId = cl.getEMP_ID(),
                    departId = cl.getDEPART_ID();

            // NOTE: CHECK DEPART_ID
            QR_ChannelDepartList res =
                    // qrcdlDao.QueryByChannelId("CL")
                    qrcdlDao.QueryByChannelId(cl.getCHANNEL_ID()).stream().filter(e -> departId.equals(e.getDepartId()))
                            .findAny().orElse(null);

            if (res == null) {
                return WebResult.GetResultString(1, config.departIdErrorMsgInput(), null);
            }

            // NOTE: SET new password
            mgmmDao.Register(cs.shaEncode(customerId + pass), customerId, departId);

            MGM_Member mgmMember= mgmmDao.ReadByCustId(customerId);
            QR_AgreeLog insertData = new QR_AgreeLog();
            insertData.setChannelId("CL");
            insertData.setDepartId(departId);
            insertData.setIdNo(mgmMember.getCUST_ID());
            insertData.setMemberId(customerId);
            qr_AgreeLogDao.Create(insertData);
            
            // NOTE: GET product mapping
            Map<String, MGM_ProdTypeMapping> map = mgmptmDao.queryAllMk2();

            mgmaDao.ReadAll().forEach(e3 -> {
                setActivityShortUrl(e3, e3.getPROD_TYPE_LIST(), cl.getCHANNEL_ID(), departId, customerId,
                        map.get(e3.getPROD_TYPE_LIST()));
            });

            return WebResult.GetResultString(0, "successful", null);

        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("<<======== #SERIVCE ERROR ?��??��??�誤.... {} ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    public String forgotPasswordLoginCheck(String reqBody) {
        try {
            logger.info("========>> #SERVICE Access the forgotPasswordLoginCheck().... ");
            ForgotPassword fp = new Gson().fromJson(reqBody, ForgotPassword.class);
            String customerId = fp.getCUST_ID();
            MGM_Member resMgmm = mgmmDao.ReadByCustId(customerId);

            logger.info("========>> #SERVICE MGM_Member {}", resMgmm);

            if (resMgmm.getCUST_PHONE() == null || resMgmm.getCUST_PHONE().trim().equals("")) {
                return WebResult.GetResultString(2, "該�??�帳?�並?��?機�?�?);
            }

            Integer errorCount = Integer.parseInt(resMgmm.getCUST_HAS_SET_PW());
            if (errorCount >= 6) {
                return WebResult.GetResultString(4, config.forgotPasswordErrorMsgCount(), null);
            }

            if (!resMgmm.getCUST_REGISTER_FLAG().equals("Y")) {
                String errorReturn = errorCount >= 3 ? config.forgotPasswordErrorMsgCount() : "?�填寫�?資�??�誤，�??�新輸入";
                return WebResult.GetResultString(2, errorReturn, resMgmm);
            }

            if (resMgmm.getCUST_BIRTH_4().equals(fp.getBIRTH_4())) {
                return WebResult.GetResultString(0, "SUCCESSFUL", resMgmm);
            } else {
                mgmmDao.UpdateErrorCount(customerId, String.valueOf(++errorCount));
                return WebResult.GetResultString(1, config.birthdayErrorMsgInput(), null);
            }

        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("<<======== #SERIVCE ERROR ?��??��??�誤.... ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    public String forgotPasswordResetPassword(String reqBody) {
        logger.info("========>> #SERVICE Access the forgotPasswordResetPassword().... ");
        try {
            ResetPassword fp = new Gson().fromJson(reqBody, ResetPassword.class);
            String customerId = fp.getCUST_ID();
            String pass = fp.getCUST_PW();
            String encryptedPass = cs.shaEncode(customerId + pass);
            mgmmDao.UpdatePassword(encryptedPass, customerId);
            return WebResult.GetResultString(0, "密碼已�??�設置�??��?請�??�登??, null);

        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("<<======== #SERIVCE ERROR ?��??��??�誤.... ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    public String getConfigByKeyName(String reqBody) {
        logger.info("========>> #SERVICE Access the getConfigByKeyName().... ");
        try {
            Gson gson = new Gson();
            QueryConfig qc = gson.fromJson(reqBody, QueryConfig.class);
            MGM_Config resMgmm = mgmcDao.ReadByKeyName(qc.getKeyName());
            return gson.toJson(resMgmm);

        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("<<======== #SERIVCE ERROR ?��??��??�誤.... ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    /**
     * For getting activity list by CustomerID When User login successful and need
     * to find what it list
     */
    public String getActivityListByMemberId(String req) {
        logger.info("========>> #SERVICE Access the getActivityListByMemberId().... ");
        try {
            List<MGM_MemberActivity> list = mgmmaDao
                    .QueryMemberActivityByMemberId(new Gson().fromJson(req, MGM_MemberActivity.class).getCUST_ID());
            return WebResult.GetResultString(0, "SUCCESSFUL", list);

        } catch (Exception e) {
            logger.error("<<======== #SERIVCE ERROR ?��??��??�誤.... ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    public String getActivityContentByActUniqId(String reqBody) {
        logger.info("========>> #SERVICE Access the getActivityDetailByActUniqId().... ");
        try {
            Gson gson = new Gson();
            QueryActivity qa = gson.fromJson(reqBody, QueryActivity.class);
            String actId = qa.getACT_UNIQ_ID();
            String customerId = qa.getCUST_ID();

            MGM_Activity resMgma = mgmaDao.ReadByActUniqId(actId);
            MGM_Member resMgmm = mgmmDao.ReadByCustId(customerId);

            MgmActivityNShortUrlResp res = new MgmActivityNShortUrlResp();
            res.setACT_NAME(resMgma.getACT_NAME());
            res.setACT_DESC(resMgma.getACT_DESC());
            res.setPROD_TYPE_LIST(resMgma.getPROD_TYPE_LIST());
            res.setCUST_NAME(resMgmm.getCUST_NAME());
            res.setACT_UNIQ_ID(actId);

            return gson.toJson(res);

        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("<<======== #SERIVCE ERROR ?��??��??�誤.... ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    public String getActivityContentByActUniqIdMk2(String reqBody) {
        logger.info("========>> #SERVICE Access the getActivityDetailByActUniqId().... ");
        try {
            Gson gson = new Gson();
            QueryActivity qa = gson.fromJson(reqBody, QueryActivity.class);
            String customerId = qa.getCUST_ID();

            MGM_Member resMgmm = mgmmDao.ReadByCustId(customerId);
            List<MGM_Activity> list = mgmaDao.ReadAll();
            List<MgmActivityNShortUrlResp> resList = new ArrayList<>();

            list.forEach(e -> {
                MgmActivityNShortUrlResp res = new MgmActivityNShortUrlResp();
                res.setACT_NAME(e.getACT_NAME());
                res.setACT_DESC(e.getACT_DESC());
                res.setPROD_TYPE_LIST(e.getPROD_TYPE_LIST());
                res.setCUST_NAME(resMgmm.getCUST_NAME());
                res.setACT_UNIQ_ID(e.getACT_UNIQ_ID());
                resList.add(res);
            });

            return gson.toJson(resList);

        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("<<======== #SERIVCE ERROR ?��??��??�誤.... ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    // DEPR:
    public String getShortUrlByActUniqId(String reqBody) {
        logger.info("========>> #SERVICE Access the getShortUrlByActUniqId().... ");
        try {
            Gson gson = new Gson();
            QueryActivity qa = gson.fromJson(reqBody, QueryActivity.class);
            List<MGM_ActivityShortUrl> resMgmasu = mgmasuDao.QueryByActId(qa.getACT_UNIQ_ID());
            return gson.toJson(resMgmasu);

        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("<<======== #SERVICE ERROR ?��??��??�誤.... ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    public String getShortUrlByCustomerId(String reqBody) {
        logger.info("========>> #SERVICE Access the getShortUrlByActUniqId().... ");
        try {
            Gson gson = new Gson();
            QueryActivity qa = gson.fromJson(reqBody, QueryActivity.class);
            List<MGM_ActivityShortUrl> resMgmasu = mgmasuDao.QueryByCustomerId(qa.getCUST_ID());
            return gson.toJson(resMgmasu);

        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("<<======== #SERVICE ERROR ?��??��??�誤.... ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    // SECTOR: METHOD
    private void setActivityShortUrl(MGM_Activity e3, String e4, String channelId, String departId, String customerId,
            MGM_ProdTypeMapping type) {
        MGM_ActivityShortUrl mgmasu = new MGM_ActivityShortUrl();
        String shortUrl = ma.generatingRandomAlphabeticString();
        mgmasu.setACT_UNIQ_ID(e3.getACT_UNIQ_ID());
        mgmasu.setPROD_TYPE(e4);
        mgmasu.setSHORT_URL(shortUrl);
        mgmasu.setCUST_ID(customerId);
        mgmasuDao.Create(mgmasu);

        QR_ShortUrl qrsu = new QR_ShortUrl();
        qrsu.setShortUrl(shortUrl);
        qrsu.setEntry(" ");
        qrsu.setChannelId(channelId);
        qrsu.setDepartId(departId);
        qrsu.setMember(customerId);
        qrsu.setPProductId(type.getQR_SHORT_URL_PProductId());
        qrsu.setPProductType(type.getQR_SHORTURL_PProductType());
        qrsu.setCount("0");
        qrsuDao.Create(qrsu);
    }

    public String getSummaryCaseByCustId(String reqBody) {
        logger.info("========>> #SERVICE Access the getSummaryCaseByCustId() by params : " + reqBody);
        try {
            JsonObject  reqJson = new JsonParser().parse(reqBody).getAsJsonObject();

            String type = "";
            if(reqJson.get("TYPE") != null){
                type = reqJson.get("TYPE").getAsString();
            }
            // type�??��?位�?並設定�?�?1
            type = StringUtils.isNotEmpty(type) ? StringUtils.leftPad(type, 2, "0") : "01";

            String season = reqJson.get("SEASON") != null ? reqJson.get("SEASON").getAsString() : "";
            String apiurl = globalConfig.MgmSummaryServerUrl.concat("MgmSummaryCase");
            String apiResponse = httpDao.doJsonPostRequest(apiurl, reqBody);
            // String apiResponse = this.getAPIDataWFH("C:\\work\\KGI2\\WFH\\apiData\\mgmApi1.txt");
            logger.info("api?�傳結�? : " + apiResponse);

            Gson gson = new Gson();
            MgmSummaryCase summaryCase = gson.fromJson(apiResponse, MgmSummaryCase.class);  

            if(summaryCase == null){
                return WebResult.GetResultString(99, "?�無資�?", null);
            }

            MgmSummaryCaseResponse result = new MgmSummaryCaseResponse();
            List<MgmSummaryCaseData> finalList = new ArrayList<MgmSummaryCaseData>();

            // 依系統當下�??��?得本�?��?��?�?��字串，�?串格式�?�?2021Q1
            String[] seasonArray = this.getCurrentAndLastSeason();

            // ?��?端�?帶季度查詢�?件�??�設使用?�季度查�?
            if(StringUtils.isEmpty(season)){
                season = seasonArray[0];
            }

            // ?�辦中件?�、申辦�??�件?�、審?��??�件?�、失?�件??
            int dashboardApplingCnt = 0, dashboardAppliedCnt = 0
                , dashboardAuditedCnt = 0
                , detailApplingCnt = 0, detailAppliedCnt = 0
                , detailAuditedCnt = 0, detailInvalidCnt = 0;

            MgmSummaryCaseData[] sortArray = this.sortByProd(summaryCase.getData());

            for (MgmSummaryCaseData summaryCaseData : sortArray){
                
                if (StringUtils.isNotEmpty(summaryCaseData.getCASE_STATUS_CODE())){
                    switch(summaryCaseData.getCASE_STATUS_CODE()){
                        case "01": 
                            dashboardApplingCnt++;
                            break;
                        case "12": 
                            dashboardAppliedCnt++;
                            break;
                        case "22": 
                            dashboardAuditedCnt++;
                            break;
                    }
                }

                if(StringUtils.isNotEmpty(summaryCaseData.getQ_ID()) && StringUtils.isNotEmpty(summaryCaseData.getYYYYMM())
                    && summaryCaseData.getYYYYMM().length() == 6){

                    String seasonYear = summaryCaseData.getYYYYMM().substring(0,4) + summaryCaseData.getQ_ID();

                    if (!season.equals(seasonYear)){
                        continue;
                    }
                }
                
                if (StringUtils.isNotEmpty(summaryCaseData.getCASE_STATUS_CODE())){
                    switch(summaryCaseData.getCASE_STATUS_CODE()){
                        case "01": 
                            detailApplingCnt++;
                            break;
                        case "12": 
                            detailAppliedCnt++;
                            break;
                        case "22": 
                            detailAuditedCnt++;
                            break;
                        case "06": 
                            detailInvalidCnt++;
                            break;
                    }
                }

                // 檢查type?�否符�?類�?條件
                if(StringUtils.isNotEmpty(type) && !type.equals(summaryCaseData.getCASE_STATUS_CODE())){
                    continue;
                }

                // data masking
                if(StringUtils.isNotEmpty(summaryCaseData.getEE_ID())){
                    summaryCaseData.setEE_ID(this.maskEEId(summaryCaseData.getEE_ID()));
                }
                if(StringUtils.isNotEmpty(summaryCaseData.getEE_NAME())){
                    summaryCaseData.setEE_NAME(this.maskEEName(summaryCaseData.getEE_NAME()));
                }

                finalList.add(summaryCaseData);
            }

            result.setSeasonCurrent(seasonArray[0]);
            result.setSeasonLast(seasonArray[1]);
            result.setDashboardApplingCnt(String.valueOf(dashboardApplingCnt));
            result.setDashboardAppliedCnt(String.valueOf(dashboardAppliedCnt));
            result.setDashboardAuditedCnt(String.valueOf(dashboardAuditedCnt));
            result.setDetailApplingCnt(String.valueOf(detailApplingCnt));
            result.setDetailAppliedCnt(String.valueOf(detailAppliedCnt));
            result.setDetailAuditedCnt(String.valueOf(detailAuditedCnt));
            result.setDetailInvalidCnt(String.valueOf(detailInvalidCnt));
            result.setDataList(finalList);
            result.setStatus(0);

            String resultStr = gson.toJson(result);
            logger.info("<<======== #SERVICE getSummaryCaseByCustId() response : " + resultStr);
            
            return resultStr;
        } catch (Exception e) {
            logger.error("<<======== #SERIVCE ERROR ?��??��??�誤.... ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    public String getSummaryBonusByCustId(String reqBody) {
        logger.info("========>> #SERVICE Access the getSummaryBonusByCustId() by params : " + reqBody);
        try {
            JsonObject  reqJson = new JsonParser().parse(reqBody).getAsJsonObject();

            String type = "";
            if(reqJson.get("TYPE") != null){
                type = reqJson.get("TYPE").getAsString();
            }
            // type�??��?位�?並設定�?�?1
            type = StringUtils.isNotEmpty(type) ? StringUtils.leftPad(type, 2, "0") : "01";

            String season = reqJson.get("SEASON") != null ? reqJson.get("SEASON").getAsString() : "";
            String apiurl = globalConfig.MgmSummaryServerUrl.concat("MgmSummaryBonus");
            String apiResponse = httpDao.doJsonPostRequest(apiurl, reqBody);
            // String apiResponse = this.getAPIDataWFH("C:\\work\\KGI2\\WFH\\apiData\\mgmApi2.txt");
            logger.info("api?�傳結�? : " + apiResponse);

            Gson gson = new Gson();
            MgmSummaryBonus summaryBonus = gson.fromJson(apiResponse, MgmSummaryBonus.class);  

            if(summaryBonus == null){
                return WebResult.GetResultString(99, "?�無資�?", null);
            }

            MgmSummaryBonusResponse result = new MgmSummaryBonusResponse();
            List<MgmSummaryBonusData> finalList = new ArrayList<MgmSummaryBonusData>();

            // 依系統當下�??��?得本�?��?��?�?��字串，�?串格式�?�?2021Q1
            String[] seasonArray = this.getCurrentAndLastSeason();

            // ?��?端�?帶季度查詢�?件�??�設使用?�季度查�?
            if(StringUtils.isEmpty(season)){
                season = seasonArray[0];
            }

            // dashboard?��??�薦�? ?�薦?�勵?�卡?? ?�薦?��?
            int dashboardBonusTotal = 0
                , detailCardBonusTotal = 0, detailBonusTotal = 0;

            for (MgmSummaryBonusData summaryBonusData : summaryBonus.getData()){

                int bounusAmt = 0;
                if(summaryBonusData.getER_BONUS_AMT() != null){
                    bounusAmt = Integer.parseInt(summaryBonusData.getER_BONUS_AMT());
                }
                
                dashboardBonusTotal += bounusAmt;

                if(StringUtils.isNotEmpty(summaryBonusData.getQ_ID()) && StringUtils.isNotEmpty(summaryBonusData.getYYYYMM())
                    && summaryBonusData.getYYYYMM().length() == 6){

                    String seasonYear = summaryBonusData.getYYYYMM().substring(0,4) + summaryBonusData.getQ_ID();

                    if (!season.equals(seasonYear)){
                        continue;
                    }
                }
                
                if (StringUtils.isNotEmpty(summaryBonusData.getER_BONUS_TYPE())){
                    switch(summaryBonusData.getER_BONUS_TYPE()){
                        case "01": 
                            detailCardBonusTotal += bounusAmt;
                            break;
                        case "02": 
                            detailBonusTotal += bounusAmt;
                            break;
                    }
                }

                // 檢查type?�否符�?類�?條件
                if(StringUtils.isNotEmpty(type) && !type.equals(summaryBonusData.getER_BONUS_TYPE())){
                    continue;
                }

                finalList.add(summaryBonusData);
            }

            result.setSeasonCurrent(seasonArray[0]);
            result.setSeasonLast(seasonArray[1]);
            result.setDashboardBonusTotal(String.valueOf(dashboardBonusTotal));
            result.setDetailCardBonusTotal(String.valueOf(detailCardBonusTotal));
            result.setDetailBonusTotal(String.valueOf(detailBonusTotal));
            result.setDataList(finalList);
            result.setStatus(0);

            String resultStr = gson.toJson(result);
            logger.info("<<======== #SERVICE getSummaryBonusByCustId() response : " + resultStr);
            
            return resultStr;
        } catch (Exception e) {
            logger.error("<<======== #SERIVCE ERROR ?��??��??�誤.... ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    public String getGiftList(String reqBody) {
        logger.info("========>> #SERVICE Access the getGiftList() by params : " + reqBody);
        try {
            JsonObject  reqJson = new JsonParser().parse(reqBody).getAsJsonObject();

            String giftId = reqJson.get("GIFT_ID") != null ? reqJson.get("GIFT_ID").getAsString() : "";

            List<MGM_GiftList> resultList = new ArrayList<MGM_GiftList>();

            if (StringUtils.isNotEmpty(giftId)){
                MGM_GiftList condition = new MGM_GiftList();
                condition.setGIFT_ID(giftId);
                resultList.add(mgmGiftListDao.Read(condition));
            } else {
                resultList = mgmGiftListDao.QueryAll();
            }

            MgmGiftListResponse result = new MgmGiftListResponse();
            result.setDataList(resultList);
            result.setStatus(0);

            Gson gson = new Gson();
            String resultStr = gson.toJson(result);
            logger.info("<<======== #SERVICE getGiftList() response : " + resultStr);
            
            return resultStr;
        } catch (Exception e) {
            logger.error("<<======== #SERIVCE ERROR ?��??��??�誤.... ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    public String applyGift(String reqBody) {
        logger.info("========>> #SERVICE Access the applyGift() by params : " + reqBody);
        try {
            JsonObject  reqJson = new JsonParser().parse(reqBody).getAsJsonObject();

            String giftId = reqJson.get("GIFT_ID") != null ? reqJson.get("GIFT_ID").getAsString() : "";
            String erId = reqJson.get("ER_ID") != null ? reqJson.get("ER_ID").getAsString() : "";
            String applyAmt = reqJson.get("APPLY_AMT") != null ? reqJson.get("APPLY_AMT").getAsString() : "";

            MGM_GiftApplyData insertData = new MGM_GiftApplyData();
            insertData.setSerial(UUID.randomUUID().toString());
            insertData.setGIFT_ID(giftId);
            insertData.setER_ID(erId);
            insertData.setAPPLY_AMT(applyAmt);
            mgmGiftApplyDataDao.Create(insertData);
            
            logger.info("<<======== #SERVICE applyGift() SUCCESS");
            return WebResult.GetResultString(0, "?��??��?!", null);
        } catch (Exception e) {
            logger.error("<<======== #SERIVCE ERROR ?��??��??�誤.... ", e);
            return WebResult.GetResultString(99, "系統?�誤", null);
        }
    }

    public String[] getCurrentAndLastSeason(){
        
        String[] result = new String[2];

        Calendar cal=Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;

        if (month < 4){
            result[0] = year + "Q1";
            result[1] = (year - 1) + "Q4";
        } else if (month < 7){
            result[0] = year + "Q2";
            result[1] = year + "Q1";
        } else if (month < 10){
            result[0] = year + "Q3";
            result[1] = year + "Q2";
        } else {
            result[0] = year + "Q4";
            result[1] = year + "Q3";
        }

        return result;
    }

    public String getAPIDataWFH(String fileName){
        StringBuilder resultSb = new StringBuilder();
        try{
            FileReader fr = new FileReader(fileName);
            // Creates a buffering character-input stream that uses a default-sized input buffer.
            BufferedReader br = new BufferedReader(fr);
            while (br.ready()) {
                resultSb.append(br.readLine());
            }
        }catch(IOException e){
            e.printStackTrace();
        }

        return resultSb.toString();
    }

    private MgmSummaryCaseData[] sortByProd(MgmSummaryCaseData[] dataArray){

        // 使用list?��??�數?�可?��?條件?��??�變�?，這裡?�便濾除PRO_FLG不符?��??��?資�?
        List<MgmSummaryCaseData> tmpList = new ArrayList<MgmSummaryCaseData>();

        if(null != dataArray) {
            for(String condition : MGM_SUMMARYCASE_SORT_CONDITION_ARR){
                for(MgmSummaryCaseData data : dataArray){
                    if(condition.equals(data.getPRO_FLG())){
                        tmpList.add(data);
                    }
                }
            }
        }

        MgmSummaryCaseData[] result = new MgmSummaryCaseData[tmpList.size()];

        return tmpList.toArray(result);
    }

    private String maskEEId(String str){

        if(StringUtils.isEmpty(str)){
            return str;
        }

        if (str.length() != 10){
            return str;
        } else {
            return str.substring(0, 4) + MASK_ID + str.substring(7, 10);
        }

    }

    private String maskEEName(String str){

        if(StringUtils.isEmpty(str)){
            return str;
        }

        if (str.length() == 1){
            return str;
        } else if (str.length() == 2){
            return str.substring(0, 1) + MASK_NAME;
        } else {
            return str.substring(0, str.length() - 2) + MASK_NAME + str.substring(str.length() - 1, str.length());
        }

    }
}
