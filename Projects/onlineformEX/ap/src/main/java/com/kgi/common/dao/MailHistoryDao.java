package com.kgi.common.dao;

import java.util.List;

import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.dto.db.MailHistory;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class MailHistoryDao extends  CRUDQDao<MailHistory> {

    @Override
    public int Create(MailHistory fullItem) {

        String sql = " insert into MailHistory "
                            + "( UniqId, MailType, Status, ErrorMessage, Title, Content, TemplateId, Attatchment, EMailAddress, CreateTime, UpdateTime)"
                    + " VALUES (:UniqId,:MailType,:Status,:ErrorMessage,:Title,:Content,:TemplateId,:Attatchment,:EMailAddress, getdate(),  getdate() )";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public MailHistory Read(MailHistory keyItem) {
        try {
            String sql = "Select * from MailHistory where Serial=?";
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(MailHistory.class),
                    new Object[] { keyItem.getSerial() });
        } catch (DataAccessException ex) {
            return null;
        }
    }

    @Override
    public int Update(MailHistory fullItem) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE MailHistory SET ");
        sb.append("Status = ? ,");
        sb.append("ErrorMessage = ?,");
        sb.append("UpdateTime=getDate()");
        sb.append(" where Serial = ?");

        return this.getJdbcTemplate().update(sb.toString(),
                new Object[] { fullItem.getStatus(), fullItem.getErrorMessage(), fullItem.getSerial() });
    }

    @Override
    public int Delete(MailHistory keyItem) {
        String sql = "DELETE FROM MailHistory where Serial = " + keyItem.getSerial();
        return this.getJdbcTemplate().update(sql);
    }

    @Override
    public List<MailHistory> Query(MailHistory keyItem) {
        String sql = "SELECT * FROM MailHistory where Status = ? and MailType=?";
        return this.getJdbcTemplate().query(sql, new BeanPropertyRowMapper<>(MailHistory.class),
                new Object[] { keyItem.getStatus(), SystemConst.MAILHISTORY_TYPE_SMTP_ALL });
    }

}
