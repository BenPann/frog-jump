package com.kgi.onlineformex.ap.dao;

import java.util.List;

import com.kgi.common.dao.CRUDQDao;
import com.kgi.onlineformex.common.dto.db.FM_CaseDocument;

import org.springframework.stereotype.Repository;

@Repository
public class FM_CaseDocDao extends CRUDQDao<FM_CaseDocument> {

    @Override
    public int Create(FM_CaseDocument fullItem) {
        return 0;
    }

    @Override
    public FM_CaseDocument Read(FM_CaseDocument keyItem) {
        return null;
    }

    @Override
    public int Update(FM_CaseDocument fullItem) {
        return 0;
    }

    @Override
    public int Delete(FM_CaseDocument keyItem) {
        return 0;
    }

    @Override
    public List<FM_CaseDocument> Query(FM_CaseDocument keyItem) {
        return null;
    }


}
