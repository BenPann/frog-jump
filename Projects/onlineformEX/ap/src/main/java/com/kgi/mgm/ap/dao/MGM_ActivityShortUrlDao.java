package com.kgi.mgm.ap.dao;

import java.util.List;

import com.kgi.common.config.DataSourceConfig;
import com.kgi.common.dao.CRUDQDao;
import com.kgi.mgm.common.dto.db.MGM_ActivityShortUrl;
import com.kgi.onlineformex.ap.exception.ErrorResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class MGM_ActivityShortUrlDao extends CRUDQDao<MGM_ActivityShortUrl> {


    @Autowired
    private DataSourceConfig dsc;


    @Override
    public int Create(MGM_ActivityShortUrl fullItem) {
        return this.getJdbcTemplate(dsc.mainDataSource2()).update(
            "INSERT INTO MGM_ActivityShortUrl (ACT_UNIQ_ID, PROD_TYPE, SHORT_URL, CUST_ID, CREATE_TIME) VALUES ( ?, ?, ?, ?, getDate() )", 
            new Object[] { 
                fullItem.getACT_UNIQ_ID(), 
                fullItem.getPROD_TYPE(), 
                fullItem.getSHORT_URL(),
                fullItem.getCUST_ID(),
            }
        );
    }

    @Override
    public MGM_ActivityShortUrl Read(MGM_ActivityShortUrl keyItem) {
        return null;
    }

    @Override
    public int Update(MGM_ActivityShortUrl fullItem) {
        return 0;
    }

    @Override
    public int Delete(MGM_ActivityShortUrl keyItem) {
        return 0;
    }

    @Override
    public List<MGM_ActivityShortUrl> Query(MGM_ActivityShortUrl keyItem) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).query(
                            "SELECT * FROM MGM_ActivityShortUrl WHERE ACT_UNIQ_ID = ? ",
                            new BeanPropertyRowMapper<>(MGM_ActivityShortUrl.class),
                            new Object[] { keyItem.getACT_UNIQ_ID() });
        } catch (DataAccessException ex) {
            logger.error("========<<< #MGM_ActivityShortUrl 查無資料 {}", ex);
            throw new ErrorResultException(9, "MGM_ActivityShortUrl 查無資料", ex);
        }
    }

    public List<MGM_ActivityShortUrl> QueryByActId(String actId) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).query(
                            "SELECT * FROM MGM_ActivityShortUrl WHERE ACT_UNIQ_ID = ? ",
                            new BeanPropertyRowMapper<>(MGM_ActivityShortUrl.class),
                            new Object[] { actId });
        } catch (DataAccessException ex) {
            logger.error("========<<< #MGM_ActivityShortUrl 查無資料 {}", ex);
            throw new ErrorResultException(9, "MGM_ActivityShortUrl 查無資料", ex);
        }
    }

    public List<MGM_ActivityShortUrl> QueryByCustomerId(String customerId) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).query(
                            "SELECT * FROM MGM_ActivityShortUrl WHERE CUST_ID = ? ",
                            new BeanPropertyRowMapper<>(MGM_ActivityShortUrl.class),
                            new Object[] { customerId });
        } catch (DataAccessException ex) {
            logger.error("========<<< #MGM_ActivityShortUrl 查無資料 {}", ex);
            throw new ErrorResultException(9, "MGM_ActivityShortUrl 查無資料", ex);
        }
    }

}
