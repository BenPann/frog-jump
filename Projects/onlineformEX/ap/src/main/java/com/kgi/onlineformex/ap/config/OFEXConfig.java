package com.kgi.onlineformex.ap.config;

import java.nio.charset.Charset;

import com.kgi.common.config.GlobalConfig;
import com.kgi.eopend3.common.util.crypt.FucoAESUtil;
import com.kgi.onlineformex.ap.dao.FM_ConfigDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OFEXConfig {


    @Autowired
    private GlobalConfig config;
    @Autowired
    private FM_ConfigDao fmcDao;


    public String makePazzd(String pazzd) {
        FucoAESUtil instance = FucoAESUtil.getInstance(config.AbbeyRye,config.RayakRoe);
        try {
            return new String(instance.decrypt(pazzd), Charset.forName("UTF-8")) ;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    /** Online-Form EX Config Value, Second value is DEFAULT if DB can't get */
    public String OFEXConVal(String keyName, String defaultValue) {
        return fmcDao.ReadConfigValue(keyName, defaultValue);
    }


    /** #圖片浮水印字 */
    public String IMG_WATER_FONT() {
        return OFEXConVal("IMG.WATER.FONT", "限凱基商業銀行申辦使用");
    }


    public String PDF_FONT_WATER_TTF() {
        return OFEXConVal("PDF.FONT_WATER_TTF", "C:/Windows/Fonts/mingliu.ttc,0");
    }


    // SECTOR: FTP
    /** #FTP */
    public String Img_FTPServer_Address() {
        return OFEXConVal("Img.FTPServer.Address", "172.18.12.20");
    }

    /** #FTP */
    public String Img_FTPServer_Path() {
        return OFEXConVal("Img.FTPServer.Path", "PoolingDir");
    }

    /** #FTP */
    public String Img_FTPServer_Userid() {
        return OFEXConVal("Img.FTPServer.Userid", "phoneapp");
    }

    /** #FTP */
    public String Img_FTPServer_PWD() {
        return OFEXConVal("Img.FTPServer.PWD", "1@34qwer");
    }


    // SECTOR: MAILHUNTER
    /** #MailHunter設定 */
    public String MailHunter_FTPServer_Address() {
        return OFEXConVal("MailHunter.FTPServer.Address", ""); // fix Improper Restriction of Stored XXE Ref - 新興科技科_STP_立約_白箱報告_0831.pdf
    }

    /** #MailHunter設定 */
    public String MailHunter_FTPServer_Userid() {
        return OFEXConVal("MailHunter.FTPServer.Userid", "mhu_ftp");
    }

    /** #MailHunter設定 #MailHunterFTPServerPAZZD=1qaz@WSX */
    public String MailHunter_FTPServer_PAZZD() {
        String pazzd = OFEXConVal("MailHunter.FTPServer.PAZZD", ""); // fix Use of Hard coded Cryptographic Key - 新興科技科_STP_立約_白箱報告_0825_v2.pdf
        pazzd = this.makePazzd(pazzd);
        return pazzd;
    }



    // SECTOR: PDF
    public boolean isPDF_Export() {
        return "Y".equals(PDF_Export());
    }

    /** # 是否匯出檔案至 localhost */
    public String PDF_Export() {
        return OFEXConVal("PDF.Export", "Y");
    }



    // SECTOR: IMAGE
    /** #補件上傳完成之後 立約上傳完成之後 是否刪除圖檔 (1:刪除,0:不刪除) 預設為不刪除 */
    public String AdditionalPhotoDelete() {
        return OFEXConVal("AdditionalPhotoDelete", "0");
    }


    // SECTOR: HTML

}
