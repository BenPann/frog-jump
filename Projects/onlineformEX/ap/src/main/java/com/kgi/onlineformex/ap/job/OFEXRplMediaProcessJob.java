package com.kgi.onlineformex.ap.job;

import java.util.List;

import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.util.DateUtil;
import com.kgi.onlineformex.ap.dao.FM_ApiLogDao;
import com.kgi.onlineformex.ap.dao.FM_CaseDataDao;
import com.kgi.onlineformex.ap.service.OrbitService;
import com.kgi.onlineformex.ap.service.PdfService;
import com.kgi.onlineformex.common.dto.db.FM_CaseData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;



@Component
@PropertySource(value = {"file:${OFEX_CONFIG_PATH}/ScheduledJob.properties"}, encoding = "UTF-8")
public class OFEXRplMediaProcessJob {

    @Autowired
    private FM_CaseDataDao fmcdDao;
    @Autowired
    private OrbitService orbitService;
    @Autowired
    private PdfService pdfs;
    @Autowired
    private FM_ApiLogDao fmalDao;

    @Value("${SendMailHunterJobSwitch}")
    private String sendMailHunterJobSwitch;


    @Scheduled(fixedDelayString = "${SendMailHunterJobSchedule}")
    public void createPDFJob() throws Exception {

        if (sendMailHunterJobSwitch.equals("0")) { return; }

        String
            uniqId = "",
            startTime = "",
            endTime = "",
            req = "",
            res = "";

        List<FM_CaseData> listFmcd = fmcdDao.QueryListByStatus("02");

        if (listFmcd == null) { return; }

        for (FM_CaseData fmcd: listFmcd) {
            uniqId = fmcd.getUniqId();
            try {
                startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
                pdfs.createContractPDF(uniqId);
                // TODO: CHECK PDF CREATE
                Boolean ifPdfCreateSuccessful = true;
                endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
                if (ifPdfCreateSuccessful) {
                    fmcdDao.UpdateStatusByUniqId(uniqId, "03");
                }
            } finally {
                fmalDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "createPDFJob", req, res, startTime, endTime);
            }
        }
    }


    @Scheduled(fixedDelayString = "${SendMailHunterJobSchedule}")
    public void uploadMedia() throws Exception {
        if (sendMailHunterJobSwitch.equals("0")) { return; }

        orbitService.uploadContractDataToOrbit();
    }


}
