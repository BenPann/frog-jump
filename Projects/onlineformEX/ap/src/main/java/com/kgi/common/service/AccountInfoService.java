package com.kgi.common.service;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

import com.ibm.tw.commons.net.mq.MQReceiver;
import com.ibm.tw.commons.net.mq.MQSender;
import com.kgi.common.config.GlobalConfig;
import com.kgi.common.xml.CreateXML;
// import com.kgi.eopend3.ap.dao.ED3_ApiLogDao;
// import com.kgi.eopend3.ap.exception.ErrorResultException;
// import com.kgi.eopend3.ap.xml.CreateXML;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.dto.customDto.BNS08508100Resp;
import com.kgi.eopend3.common.dto.db.ED3_ApiLog;
import com.kgi.eopend3.common.dto.esbdto.BNS06042503Q;
import com.kgi.eopend3.common.dto.esbdto.BNS06042503Q.BNS06042503QDetail;
import com.kgi.eopend3.common.dto.esbdto.BNS06042509Q;
import com.kgi.eopend3.common.dto.esbdto.BNS08508100;
import com.kgi.eopend3.common.dto.esbdto.BNS06042509Q.BNS06042509QDetail;
import com.kgi.eopend3.common.dto.esbdto.BNS08508100Detail;
import com.kgi.eopend3.common.dto.esbdto.BNS08508100MsgRs;
import com.kgi.eopend3.common.dto.esbdto.ESBResp;
import com.kgi.eopend3.common.util.CheckUtil;
import com.kgi.eopend3.common.util.DateUtil;
import com.kgi.onlineformex.ap.exception.ErrorResultException;

@Service
public class AccountInfoService {
    
    @Autowired
    private GlobalConfig gc;
    // @Autowired
    // private ED3_ApiLogDao ed3ApiLogDao;
    @Autowired
	private ESBService esbs;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    
    
	/**
	 * 查詢本行之事故電文
	 * */
	private String xmlBNS06042503Q(String idno) {
		String clientID = gc.ESBClientId;
		String clientPAZZD = gc.ESBClientPAZZD;
		String timeNow = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		clientPAZZD = DigestUtils.md5Hex(clientPAZZD + timeNow);

		StringBuffer sb = new StringBuffer("");

		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append(
				"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:esb=\"http://www.cosmos.com.tw/esb\" xmlns:cics=\"http://www.cosmos.com.tw/cics\" xmlns:bill=\"http://www.cosmos.com.tw/bill\" xmlns:nefx=\"http://www.cosmos.com.tw/nefx\" xmlns:cactx=\"http://www.cosmos.com.tw/cactx\" xmlns:bns=\"http://www.cosmos.com.tw/bns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("  <SOAP-ENV:Header />");
		sb.append("  <SOAP-ENV:Body>");
		sb.append("    <esb:MsgRq>");
		sb.append("      <Header>");
		sb.append("        <ClientId>").append(clientID).append("</ClientId>");
		sb.append("        <ClientPwd>").append(clientPAZZD).append("</ClientPwd>");
		sb.append("        <ClientDt>").append(timeNow).append("</ClientDt>");
		sb.append("        <TxnId>BNS06042503Q</TxnId>");
		sb.append("        <UUID>").append(clientID + DateUtil.GetDateFormatString("yyyyMMddHHmmssSSS")).append("</UUID>");
		sb.append("      </Header>");
		sb.append("      <SvcRq xsi:type=\"bns:BNS06042503QSvcRqType\">");
		sb.append("        <BNSHeader>");
		sb.append("          <BranchNo>").append("").append("</BranchNo>");
		sb.append("          <FlagX>").append("X").append("</FlagX>");
		sb.append("          <TellerNo>").append("").append("</TellerNo>");
		sb.append("          <Flag4>").append("Z").append("</Flag4>");
		sb.append("          <ChannelId>").append("01").append("</ChannelId>");
		sb.append("        </BNSHeader>");
		sb.append("        <IDNo>").append(idno).append("</IDNo>");
		sb.append("        <IDType>").append("11").append("</IDType>");
		sb.append("      </SvcRq>");
		sb.append("    </esb:MsgRq>");
		sb.append("  </SOAP-ENV:Body>");
		sb.append("</SOAP-ENV:Envelope>");

		logger.info(sb.toString());
		
		
		return sb.toString();
	}
	
	private String xmlBNS06042509Q(String idno) {
		String clientID = gc.ESBClientId;
		String clientPAZZD = gc.ESBClientPAZZD;
		String timeNow = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		clientPAZZD = DigestUtils.md5Hex(clientPAZZD + timeNow);

		StringBuffer sb = new StringBuffer("");

//		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:esb=\"http://www.cosmos.com.tw/esb\" xmlns:kgib=\"http://www.cosmos.com.tw/kgib\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("  <SOAP-ENV:Header/>");
		sb.append("  <SOAP-ENV:Body>");
		sb.append("    <esb:MsgRq>");
		sb.append("      <Header>");
		sb.append("        <ClientId>").append(clientID).append("</ClientId>");
		sb.append("        <ClientPwd>").append(clientPAZZD).append("</ClientPwd>");
		sb.append("        <ClientDt>").append(timeNow).append("</ClientDt>");
		sb.append("        <TxnId>BNS06042509Q</TxnId>");
		sb.append("        <UUID>").append(clientID + DateUtil.GetDateFormatString("yyyyMMddHHmmssSSS")).append("</UUID>");
		sb.append("      </Header>");
		sb.append("      <SvcRq xsi:type=\"kgib:BNS06042509QSvcRqType\">");
		sb.append("        <BNSHeader>");
		sb.append("          <BranchNo>").append("").append("</BranchNo>");
		sb.append("          <FlagX>").append("X").append("</FlagX>");
		sb.append("          <TellerNo>").append("").append("</TellerNo>");
		sb.append("          <Flag4>").append("Z").append("</Flag4>");
		sb.append("          <ChannelId>").append("01").append("</ChannelId>");
		sb.append("          <Workstation>").append("").append("</Workstation>");
		sb.append("          <PreviewFlag>").append("").append("</PreviewFlag>");
		sb.append("        </BNSHeader>");
//		sb.append("        <SystemType>").append("DEP").append("</SystemType>");
		sb.append("        <AccountNo>").append("").append("</AccountNo>");
//		sb.append("        <ReasonCode>").append("97").append("</ReasonCode>");
		sb.append("        <IDNo>").append(idno).append("</IDNo>");
		sb.append("        <IDType>").append("11").append("</IDType>");
		sb.append("      </SvcRq>");
		sb.append("    </esb:MsgRq>");
		sb.append("  </SOAP-ENV:Body>");
		sb.append("</SOAP-ENV:Envelope>");

		logger.info(sb.toString());
		
		
		return sb.toString();
	}

	
	/**
	 * 查詢非本行之事故電文
	 * */
	@Deprecated
	private String xmlBNS06042508Q(String idno, String page_count) {
		String clientID = gc.ESBClientId;
		String clientPAZZD = gc.ESBClientPAZZD;
		String timeNow = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		clientPAZZD = DigestUtils.md5Hex(clientPAZZD + timeNow);

		StringBuffer sb = new StringBuffer("");

//		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:esb=\"http://www.cosmos.com.tw/esb\" xmlns:kgib=\"http://www.cosmos.com.tw/kgib\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("  <SOAP-ENV:Header />");
		sb.append("  <SOAP-ENV:Body>");
		sb.append("    <esb:MsgRq>");
		sb.append("      <Header>");
		sb.append("        <ClientId>").append(clientID).append("</ClientId>");
		sb.append("        <ClientPwd>").append(clientPAZZD).append("</ClientPwd>");
		sb.append("        <ClientDt>").append(timeNow).append("</ClientDt>");
		sb.append("        <TxnId>BNS06042508Q</TxnId>");
		sb.append("        <UUID>").append(clientID + DateUtil.GetDateFormatString("yyyyMMddHHmmssSSS")).append("</UUID>");
		sb.append("      </Header>");
		sb.append("      <SvcRq xsi:type=\"kgib:BNS06042508QSvcRqType\">");
		sb.append("        <BNSHeader>");
		sb.append("          <FlagX>").append("X").append("</FlagX>");
		sb.append("          <TellerNo>").append("").append("</TellerNo>");
		sb.append("          <Flag4>").append("Z").append("</Flag4>");
		sb.append("          <ChannelId>").append("01").append("</ChannelId>");
		sb.append("          <BranchNo>").append("").append("</BranchNo>");
		sb.append("        </BNSHeader>");
		sb.append("        <IDNo>").append(idno).append("</IDNo>");
		sb.append("        <IDType>").append("11").append("</IDType>");
		sb.append("        <page_count>").append(page_count).append("</page_count>");
		sb.append("      </SvcRq>");
		sb.append("    </esb:MsgRq>");
		sb.append("  </SOAP-ENV:Body>");
		sb.append("</SOAP-ENV:Envelope>");

		logger.info(sb.toString());
		
		
		return sb.toString();
	}
	
	
    
    /**
     * 	查詢凱基事故電文(BNS06042503Q)判斷是否為警示黑名單
     * */
    public List<BNS06042503QDetail> get6042503Detail(String idno) {
    	String startTime = "", endTime = "" ;
		String req = "", res = "";

		try {
			req = xmlBNS06042503Q(idno);
			MQSender sender = esbs.getEOPSender();
			MQReceiver receiver = esbs.getEOPReceiver();
//			logger.info("xmlBNS06042503Q request: [" + req + "]");
			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			res = esbs.sendMessageQueue(req, sender, receiver);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
//			logger.info("xmlBNS06042503Q respone: [" + res + "]");

			BNS06042503Q bns06042503Q = new BNS06042503Q() ;
			return bns06042503Q.parseXml(res) ;

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("未知錯誤", e);

			res= e.getMessage() ;

			return null ;
		} finally {
			// ed3ApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, idno, "BNS06042503Q", req, res, startTime, endTime) ;
		}

//		return res ;
    }
    
    
    
    @Deprecated
    public String test6042503(String idno) {
		String req = "" ;

		try {
			req = xmlBNS06042503Q(idno);
			MQSender sender = esbs.getEOPSender();
			MQReceiver receiver = esbs.getEOPReceiver();
			return esbs.sendMessageQueue(req, sender, receiver);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("未知錯誤", e);
			
			return e.getMessage() ;
		} finally {
		}
    }
    
    
    /**
     * 檢查是否為事故戶
     * 判斷"註記" = "J  為設定事故戶 " or " C  為更正事故戶 " 
     *   且原因代碼(reason_code) =  8:KGI異常等級3 
     *   且事故類型IncidentType = 0206  or  0200 人頭戶
     * */
    public boolean isIncidentAccount(String idno) {
    	/**
    	 * 檢查 BNS06042509Q
    	 * */
    	List<BNS06042509QDetail> details09 = get6042509Detail(idno) ;
    	if (details09 != null) {
	    	for (BNS06042509QDetail detail : details09) {
//	    		logger.info("RCD=" + detail.getRCD() + ", IncidentType=" + detail.getIncidentType() + ", RM=" + detail.getRM());
	    		if (SystemConst.ESB_REASON_CODE_08.equals(detail.getRCD())
	    			&& (SystemConst.ESB_INCIDENT_TYPE_0200.equals(detail.getIncidentType())|| SystemConst.ESB_INCIDENT_TYPE_0206.equals(detail.getIncidentType()))
	    			&& (SystemConst.ESB_RM_J.equalsIgnoreCase(detail.getRM()) || SystemConst.ESB_RM_C.equalsIgnoreCase(detail.getRM()))
	    				) {
	    			return true ;
	    		}
	    	}
    	}

    	/**
    	 * 檢查 BNS06042503Q
    	 * */
    	List<BNS06042503QDetail> details03 = get6042503Detail(idno) ;
    	if (details03 != null) {
	    	for (BNS06042503QDetail detail : details03) {
//	    		logger.info("ReasonNo=" + detail.getReasonNo() + ", ReasonType=" + detail.getReasonType() + ", SetFlag=" + detail.getSetFlag());
	    		if (SystemConst.ESB_REASON_CODE_08.equals(detail.getReasonNo())
	    			&& (SystemConst.ESB_INCIDENT_TYPE_0200.equals(detail.getReasonType())|| SystemConst.ESB_INCIDENT_TYPE_0206.equals(detail.getReasonType()))
	    			&& (SystemConst.ESB_RM_J.equalsIgnoreCase(detail.getSetFlag()) || SystemConst.ESB_RM_C.equalsIgnoreCase(detail.getSetFlag()))
	    				) {
	    			return true ;
	    		}
	    	}
    	}
    	
		return false ;
    }

    
    /**
     * 非本行客戶事故維護交易
     */
    public List<BNS06042509QDetail> get6042509Detail(String idno) {
    	String startTime = "", endTime = "" ;
		String req = "", res = "";
//		ESBResp resp = new ESBResp() ;

		try {
			req = xmlBNS06042509Q(idno);
			MQSender sender = esbs.getEOPSender();
			MQReceiver receiver = esbs.getEOPReceiver();
//			logger.info("xmlBNS06042509Q request: [" + req + "]");
			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			res = esbs.sendMessageQueue(req, sender, receiver);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
//			logger.info("xmlBNS06042509Q respone: [" + res + "]");

			BNS06042509Q bns06042509Q = new BNS06042509Q() ;
			return bns06042509Q.parseXml(res) ;
//            resp.setXml(res);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("未知錯誤", e);

			res= e.getMessage() ;
			
			return null ;
		} finally {
			// ed3ApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, idno, "BNS06042509Q", req, res, startTime, endTime) ;
		}

//		return res ;
    }


    /**
     * 電文BNS06042509Q測試 
     * */
    @Deprecated
    public String test6042509(String idno) {
		String req = "", res = "";
		try {
			req = xmlBNS06042509Q(idno);
			MQSender sender = esbs.getEOPSender();
			MQReceiver receiver = esbs.getEOPReceiver();

			return esbs.sendMessageQueue(req, sender, receiver);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("未知錯誤", e);

			return e.getMessage() ;
		}
    }


    /**
     * 	查詢凱基事故電文(BNS06042508Q)判斷是否為警示黑名單
     * */
    @Deprecated
    public String check6042508(String idno, String page_count) {
		String req = "";
		String xml = "";
		ESBResp resp = new ESBResp() ;

		String pageCount = "0" ;
		if (!CheckUtil.isEmpty(page_count)) {
			pageCount = page_count ;
		}

		try {
			req = xmlBNS06042508Q(idno, pageCount);
			MQSender sender = esbs.getEOPSender();
			MQReceiver receiver = esbs.getEOPReceiver();
			logger.info("xmlBNS06042508Q request: [" + req + "]");
			xml = esbs.sendMessageQueue(req, sender, receiver);
			logger.info("xmlBNS06042508Q respone: [" + xml + "]");

            resp.setXml(xml);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("未知錯誤", e);
			
			resp.setStatusCode("F990");
			resp.setSeverity("未知錯誤" + e.getMessage());
		} finally {
		}

		return xml ;
    }


	/**
	 * NEW Version of Account check
	 * Just check account has exist or not
	 * 
	 * @param idno
	 * @param birthday
	 * @throws ErrorResultException
	 * @throws Exception
	 */
	public BNS08508100 BNS08508100Check(String idno, String birthday) throws ErrorResultException, Exception {
		String 
			startTime = "",
			endTime = "",
			req = "",
			res = "";

		try {
			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			req = get85081XML(idno);
			MQSender sender = esbs.getEOPSender();
			MQReceiver receiver = esbs.getEOPReceiver();
			res = esbs.sendMessageQueue(req, sender, receiver);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");

			logger.info("========<<< BNS085081 {} ", res);

			// 解析XML 並回傳結果
			JAXBContext jaxbContext = JAXBContext.newInstance(BNS08508100MsgRs.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			ByteArrayInputStream bais = new ByteArrayInputStream(res.getBytes("UTF-8"));

			// SOAP CONNECT
			SOAPMessage response = MessageFactory.newInstance(SOAPConstants.DEFAULT_SOAP_PROTOCOL).createMessage(null, bais);
			SOAPBody soapBody = response.getSOAPBody();

			BNS08508100MsgRs bns08508100MsgRs = (BNS08508100MsgRs) jaxbUnmarshaller.unmarshal(soapBody.extractContentAsDocument());

			if (bns08508100MsgRs.getbNS08508100SvcRs() != null) {
				BNS08508100 bns85081 = bns08508100MsgRs.getbNS08508100SvcRs().getBns08508100();
				return bns85081;
			} else {
				logger.info("========<<<< NULL NULL NULL");
				return null;
			}

		} catch (Exception e) {
			throw new Exception();
		} finally {
			// SET LOG
		}
	}
	
	
	/**
	 * 主要用於帳號檢查 (待確認)
	 * 
	 * @param idno
	 * @param birthday
	 * @return
	 * @throws ErrorResultException
	 * @throws Exception
	 */
    public BNS08508100Resp isKGIDeposit(String idno,String birthday) throws ErrorResultException, Exception {

		String 
			startTime = "",
			endTime = "",
			req = "",
			res = "";
		BNS08508100Resp resp = new BNS08508100Resp() ;
		resp.setKgiDeposit("N");

		try {
			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			req = get85081XML(idno);
			MQSender sender = esbs.getEOPSender();
			MQReceiver receiver = esbs.getEOPReceiver();
			res = esbs.sendMessageQueue(req, sender, receiver);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");

            // 解析XML 並回傳結果
			JAXBContext jaxbContext = JAXBContext.newInstance(BNS08508100MsgRs.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			ByteArrayInputStream bais = new ByteArrayInputStream(res.getBytes("UTF-8"));

			// SOAP CONNECT
			SOAPMessage response = MessageFactory.newInstance(SOAPConstants.DEFAULT_SOAP_PROTOCOL).createMessage(null, bais);
			SOAPBody soapBody = response.getSOAPBody();

			BNS08508100MsgRs bns08508100 = (BNS08508100MsgRs) jaxbUnmarshaller.unmarshal(soapBody.extractContentAsDocument());

			//舊戶: 判斷舊戶有有台幣/ 外幣/ 支存/ 定存 (AcctType = 1XXXX 活存、2XXX 定存、3XXX 支存) ，且其(CloseDt)銷戶/結清日非99999999、00000000
			if (bns08508100.getbNS08508100SvcRs() != null){
				if (bns08508100.getbNS08508100SvcRs().getBns08508100() != null) {
					/*if(!bns08508100.getbNS08508100SvcRs().getBns08508100().getBirthday().equals(birthday)){
						throw new ErrorResultException(4, "檢核生日不符", null);
					}else{*/
						//phone = bns08508100.getbNS08508100SvcRs().getBns08508100().getPhone().trim();
						//chtName = bns08508100.getbNS08508100SvcRs().getBns08508100().getCustName().trim();

						if (bns08508100.getbNS08508100SvcRs().getBns08508100().getDetail() != null) {						
							List<BNS08508100Detail> list = bns08508100.getbNS08508100SvcRs().getBns08508100().getDetail();

							for(BNS08508100Detail detail : list) {
								String acctType = detail.getAcctType().trim();
								resp.setAcctType(acctType); // 主類別
								resp.setIntCat(detail.getIntCat()); // 次類別

								if( (acctType.substring(0, 1).equals("1") || acctType.substring(0, 1).equals("2") || acctType.substring(0, 1).equals("3"))
									&& (detail.getCloseDt().trim().equals("99999999") || detail.getCloseDt().trim().equals("00000000") )
									&& detail.getCurrStatus().trim().equals("00"))
								{
									resp.setKgiDeposit("Y") ;
									break;
								}
							}
						}

					// }

				} // Bns08508100() != null
			} // bNS08508100SvcRs() != null

			return resp;

		} catch (ErrorResultException e) {			
            throw e;
        } catch (Exception e) {
			throw e;
		} finally {
			// ed3ApiLogDao.Create(new ED3_ApiLog(SystemConst.APILOG_TYPE_CALLOUT, idno, "/BNS08508100Q", req, res, startTime, endTime)); 
		}

	}    
  
	
	/**
	 * Do create BNS85081 xml and convert to String
	 */
    private String get85081XML(String idno) {
		CreateXML xFile = new CreateXML();
		String ClientID = gc.ESBClientId;
		String ClientPAZZD = gc.ESBClientPAZZD;
		String timeNow = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		ClientPAZZD = DigestUtils.md5Hex(ClientPAZZD + timeNow);

		Element Envelope = xFile.setRootTag("SOAP-ENV:Envelope");
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("xmlns:SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
		map.put("xmlns:esb", "http://www.cosmos.com.tw/esb");
		map.put("xmlns:bns","http://www.cosmos.com.tw/bns");
		map.put("xmlns:mix","http://www.cosmos.com.tw/mix"); 
		map.put("xmlns:cardpac","http://www.cosmos.com.tw/cardpac");
		map.put("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		xFile.setAttribute(Envelope, map);

		xFile.setChildTagNoTextNode(Envelope, "SOAP-ENV:Header");
		Element Body = xFile.setChildTagNoTextNode(Envelope, "SOAP-ENV:Body");
		Element esb = xFile.setChildTagNoTextNode(Body, "esb:MsgRq");
		Element Header = xFile.setChildTagNoTextNode(esb, "Header");

		xFile.setChildHasTextNode(Header, "ClientId", ClientID);
		xFile.setChildHasTextNode(Header, "ClientPwd", ClientPAZZD);
		xFile.setChildHasTextNode(Header, "ClientDt", timeNow);
		xFile.setChildHasTextNode(Header, "TxnId", "BNS08508100Q");
		xFile.setChildHasTextNode(Header, "UUID", ClientID + DateUtil.GetDateFormatString("yyyyMMddHHmmssSSS"));

		Element SvcRq = xFile.setChildTagNoTextNode(esb, "SvcRq");

		map.clear();
		map.put("xsi:type", "kgib:BNS08508100QSvcRqType");
		map.put("xmlns:kgib", "http://www.cosmos.com.tw/kgib");
		xFile.setAttribute(SvcRq, map);
		Element BNSHeader = xFile.setChildTagNoTextNode(SvcRq, "BNSHeader");
		xFile.setChildHasTextNode(BNSHeader, "FlagX", "X");
		xFile.setChildHasTextNode(BNSHeader, "Flag4", "Z");
		//xFile.setChildHasTextNode(BNSHeader, "SubChannel", "C");
		xFile.setChildHasTextNode(BNSHeader, "ChannelId", "01");
		xFile.setChildHasTextNode(SvcRq, "IDNo", idno);
		xFile.setChildHasTextNode(SvcRq, "yesno1", "N");
		xFile.setChildHasTextNode(SvcRq, "BANCSAppl", "DEP");
		String result = "";
		try {
			result = xFile.toXMLFile();
		} catch (TransformerException e) {
			logger.error("未知錯誤",e);
		}
		return result;
	}


}
