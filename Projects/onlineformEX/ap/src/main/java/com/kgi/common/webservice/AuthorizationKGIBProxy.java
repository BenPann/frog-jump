package com.kgi.common.webservice;

public class AuthorizationKGIBProxy implements com.kgi.common.webservice.AuthorizationKGIB_PortType {
  private String _endpoint = null;
  private com.kgi.common.webservice.AuthorizationKGIB_PortType authorizationKGIB_PortType = null;
  
  public AuthorizationKGIBProxy() {
    _initAuthorizationKGIBProxy();
  }
  
  public AuthorizationKGIBProxy(String endpoint) {
    _endpoint = endpoint;
    _initAuthorizationKGIBProxy();
  }
  
  private void _initAuthorizationKGIBProxy() {
    try {
      authorizationKGIB_PortType = (new com.kgi.common.webservice.AuthorizationKGIB_ServiceLocator()).getDomino();
      if (authorizationKGIB_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)authorizationKGIB_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)authorizationKGIB_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (authorizationKGIB_PortType != null)
      ((javax.xml.rpc.Stub)authorizationKGIB_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.kgi.common.webservice.AuthorizationKGIB_PortType getAuthorizationKGIB_PortType() {
    if (authorizationKGIB_PortType == null)
      _initAuthorizationKGIBProxy();
    return authorizationKGIB_PortType;
  }
  
  public com.kgi.common.webservice.AUTINFO_KGIB FUNGETAUTINFOBYID_KGIB(java.lang.String STRID) throws java.rmi.RemoteException{
    if (authorizationKGIB_PortType == null)
      _initAuthorizationKGIBProxy();
    return authorizationKGIB_PortType.FUNGETAUTINFOBYID_KGIB(STRID);
  }
  
  public com.kgi.common.webservice.AUTINFO_KGIB_NEW FUNGETAUTINFOBYID_KGIB_NEW(java.lang.String STRIDG) throws java.rmi.RemoteException{
    if (authorizationKGIB_PortType == null)
      _initAuthorizationKGIBProxy();
    return authorizationKGIB_PortType.FUNGETAUTINFOBYID_KGIB_NEW(STRIDG);
  }
  
  public com.kgi.common.webservice.UPDATEAUTINFO_KGIB FUNUPDATEAUTINFO_KGIB(java.lang.String STRID, java.lang.String STRCUSTOMERNAME, java.lang.String STRA_ITEM01, java.lang.String STRAUTHORIZETOALLCORP, java.lang.String STRTOCORP01, java.lang.String STRTOCORP13, java.lang.String STRTOCORP22, java.lang.String STRTOCORP23, java.lang.String STRTOCORP24, java.lang.String STRTOCORP25, java.lang.String STRTOCORP26, java.lang.String STRTOCORP27, java.lang.String STRTOCORP29, java.lang.String STRFROMIP, java.lang.String STRVERNO) throws java.rmi.RemoteException{
    if (authorizationKGIB_PortType == null)
      _initAuthorizationKGIBProxy();
    return authorizationKGIB_PortType.FUNUPDATEAUTINFO_KGIB(STRID, STRCUSTOMERNAME, STRA_ITEM01, STRAUTHORIZETOALLCORP, STRTOCORP01, STRTOCORP13, STRTOCORP22, STRTOCORP23, STRTOCORP24, STRTOCORP25, STRTOCORP26, STRTOCORP27, STRTOCORP29, STRFROMIP, STRVERNO);
  }
  
  public com.kgi.common.webservice.UPDATEAUTINFO_KGIB FUNUPDATEAUTINFO_KGIB_NEW(java.lang.String STRID_NEW, java.lang.String STRCUSTOMERNAME, java.lang.String STRA_ITEM01, java.lang.String STRFROMIP, java.lang.String STRVERNO) throws java.rmi.RemoteException{
    if (authorizationKGIB_PortType == null)
      _initAuthorizationKGIBProxy();
    return authorizationKGIB_PortType.FUNUPDATEAUTINFO_KGIB_NEW(STRID_NEW, STRCUSTOMERNAME, STRA_ITEM01, STRFROMIP, STRVERNO);
  }
  
  public com.kgi.common.webservice.UPDATEAUTINFO_KGIB FUNUPDATEAUTINFO_KGIB_ACC(java.lang.String STRID, java.lang.String STRCUSTOMERNAME, java.lang.String STRA_ITEM01, java.lang.String STRA_DEPTNO, java.lang.String STRFROMIP, java.lang.String STRVERNO) throws java.rmi.RemoteException{
    if (authorizationKGIB_PortType == null)
      _initAuthorizationKGIBProxy();
    return authorizationKGIB_PortType.FUNUPDATEAUTINFO_KGIB_ACC(STRID, STRCUSTOMERNAME, STRA_ITEM01, STRA_DEPTNO, STRFROMIP, STRVERNO);
  }
  
  
}