package com.kgi.mgm.ap.dao;

import java.util.List;

import com.kgi.common.config.DataSourceConfig;
import com.kgi.common.dao.CRUDQDao;
import com.kgi.mgm.common.dto.db.MGM_MemberActivity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class MGM_MemberActivityDao extends CRUDQDao<MGM_MemberActivity> {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DataSourceConfig dsc;


    @Override
    public int Create(MGM_MemberActivity fullItem) {
        return 0;
    }

    @Override
    public MGM_MemberActivity Read(MGM_MemberActivity keyItem) {
        return null;
    }

    @Override
    public int Update(MGM_MemberActivity fullItem) {
        return 0;
    }

    @Override
    public int Delete(MGM_MemberActivity keyItem) {
        return 0;
    }

    @Override
    public List<MGM_MemberActivity> Query(MGM_MemberActivity keyItem) {
        return null;
    }

    public List<MGM_MemberActivity> QueryMemberActivityByMemberId(String custId) {
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).query(
                            "SELECT * FROM MGM_MemberActivity WHERE CUST_ID = ?", 
                            new Object[]{ custId }, 
                            new BeanPropertyRowMapper<>(MGM_MemberActivity.class));       
        } catch (Exception e) {
            logger.error("========<<< #ERROR {}", e);
            return null;
        }
    }

}
