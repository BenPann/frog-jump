package com.kgi.common.dao;

import java.util.List;

import com.kgi.eopend3.common.dto.db.MailTemplate;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class MailTemplateDao extends CRUDQDao<MailTemplate> {

    @Override
    public int Create(MailTemplate fullItem) {
        return 0;
    }

    @Override
    public MailTemplate Read(MailTemplate keyItem) {
        try {
            String sql = "Select * from MailTemplate where Type = ? and TagName = ? ";
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(MailTemplate.class),
                    new Object[] { keyItem.getType() , keyItem.getTagName() });
        } catch (DataAccessException ex) {
            return null;
        }
    }

    @Override
    public int Update(MailTemplate fullItem) {
        return 0;
    }

    @Override
    public int Delete(MailTemplate keyItem) {
        return 0;
    }

    @Override
    public List<MailTemplate> Query(MailTemplate keyItem) {
        String sql = "SELECT * FROM MailTemplate where Type = ?";
        return this.getJdbcTemplate().query(sql, new BeanPropertyRowMapper<>(MailTemplate.class),
                new Object[] { keyItem.getType() });
    }

}
