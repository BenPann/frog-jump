package com.kgi.mgm.ap.config;

import com.kgi.mgm.ap.dao.MGM_ConfigDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MgmConfig {

    @Autowired
    private MGM_ConfigDao mgmcDao;

    private String onlineformEXConfigValue(String keyName, String defaultValue) {
        return mgmcDao.ReadConfigValue(keyName, defaultValue);
    }

    public String passwordErrorMsgCount() {
        return onlineformEXConfigValue("PASSWORD.error.count.more.three", "您輸入密碼錯誤次數超過3次，請使用忘記密碼功能解除鎖定");
    }

    public String passwordErrorMsgInput() {
        return onlineformEXConfigValue("PASSWORD.error.input", "密碼輸入錯誤，請重新輸入");
    }

    public String departIdErrorMsgInput() {
        return onlineformEXConfigValue("DEPARTMENT.error.input", "部門編號錯誤，請重新輸入");
    }

    public String birthdayErrorMsgInput() {
        return onlineformEXConfigValue("BIRTHDAY.error.input", "生日認證錯誤");
    }

    public String forgotPasswordErrorMsgCount() {
        return onlineformEXConfigValue("FORGOT.password.error.count", "該帳號已被鎖定，請洽資訊處");
    }

    
}
