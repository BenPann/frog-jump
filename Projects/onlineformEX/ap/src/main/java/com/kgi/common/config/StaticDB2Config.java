package com.kgi.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = { "file:${OFEX_CONFIG_PATH}/DB.properties" }, encoding = "UTF-8")
public class StaticDB2Config implements IBaseDBConfig {

    @Value("${staticDB2Config.username}")
    private String username;
    @Value("${staticDB2Config.password}")
    private String pazzd;
    @Value("${staticDB2Config.url}")
    private String url;
    @Value("${staticDB2Config.driverclassname}")
    private String driverclassname;
    @Value("${staticDB2Config.code}")
    private String code;
    @Value("${staticDB2Config.dbName}")
    private String dbName;
    
    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPazzd() {
        return pazzd;
    }

    @Override
    public void setPazzd(String pazzd) {
        this.pazzd = pazzd;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getDriverClassName() {
        return driverclassname;
    }

    @Override
    public void setDriverClassName(String driverClassName) {
        this.driverclassname = driverClassName;
    }
}