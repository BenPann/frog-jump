package com.kgi.onlineformex.ap.service;

import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.kgi.common.dao.EntryDataDao;
import com.kgi.common.dao.EntryDataDetailDao;
import com.kgi.common.service.CryptoService;
import com.kgi.common.service.SNService;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.db.EntryData;
import com.kgi.eopend3.common.dto.db.EntryDataDetail;
import com.kgi.eopend3.common.dto.response.CaseRes;
import com.kgi.eopend3.common.dto.response.LoginRes;
import com.kgi.eopend3.common.util.CheckUtil;
import com.kgi.onlineformex.ap.dao.FM_CaseDataDao;
import com.kgi.onlineformex.ap.dao.FM_ConfigDao;
import com.kgi.onlineformex.ap.exception.ErrorResultException;
import com.kgi.onlineformex.ap.mockData.MockDataService;
import com.kgi.onlineformex.common.dto.customDto.ASystemReq;
import com.kgi.onlineformex.common.dto.customDto.ASystemRes;
import com.kgi.onlineformex.common.dto.customDto.GetOtpTelResDto;
import com.kgi.onlineformex.common.dto.db.FM_CaseData;
import com.kgi.onlineformex.common.dto.db.FM_Config;
import com.kgi.onlineformex.common.dto.view.CreditModifyTermView;
import com.kgi.onlineformex.common.dto.view.DataConfirmView;
import com.kgi.onlineformex.common.dto.view.MobileLoginView;
import com.kgi.onlineformex.common.dto.view.OFLoginView;
import com.kgi.onlineformex.common.dto.view.TokenDecryptView;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;


@Service
@PropertySource(value = {"file:${OFEX_CONFIG_PATH}/RPL.properties"}, encoding = "UTF-8")
public class RPLApplyService {

    @Autowired
    private SNService sns;
    @Autowired
    private FM_CaseDataDao fmcdDao;
    @Autowired
    private EntryDataDao fmedDao;
    @Autowired
    private EntryDataDetailDao fmdeeDao;
    @Autowired
    private ASystemService ass;
    @Autowired
    private CryptoService ds;
    @Autowired
    private FM_ConfigDao fmcDao;
    @Autowired
    private PdfService pdfs;
    @Autowired
    private MockDataService mds;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${isMock}")
    private boolean isMock;


    // SECTOR: REQUEST
    /** CHAP: 1. User login and connect A-System at PI page
     * 
     *      STEP: 1. CREATE NEW CaseData
     *            2. CONNECT A-SYSTEM
     *            3. QUERY APS, IF A-SYSTEM NOT GET PHONE
     *            4. DB_ACCESS
     *            5. SET RESPONSE
     * 
     * @param reqJson
     * @return
     */
    public String loginASystem(String reqJson) {

        logger.info("========>> #SERVICE Access the loginASystem: {}", reqJson);

        try {

            if (!ESAPI.validator().isValidInput("RPL", reqJson, "SafeJson", Integer.MAX_VALUE, false)) {
                return WebResult.GetFailResult();
            }

            OFLoginView view = new Gson().fromJson(reqJson, OFLoginView.class);
            if (!CheckUtil.check(view)) {
                throw new ErrorResultException(1, "參數錯誤", null);
            }
            String
                uniqId    = sns.getUniqId(),
                idno      = view.getIdno(),
                birthday  = view.getBirthday(),
                ipAddress = view.getIpAddress();

            // STEP: 1. CREATE NEW FM_CaseData 
            fmcdDao.createByLogin(uniqId, view);

            // STEP: 2. CONNECT A-SYSTEM
            List<ASystemRes> aSystemRes = ass.qryUserInfoFromASystem(idno);

            // STEP: 3. QUERY APS, IF A-SYSTEM NOT GET PHONE
            GetOtpTelResDto getOtpTelRes = new GetOtpTelResDto();
            if (aSystemRes.get(0).getPhone().isEmpty()) {
                getOtpTelRes = ass.getOtpTel(uniqId, idno, birthday);
            }

            // STEP: 4. UPDATE A-SYSTEM TO FM_CaseData
            updateEntryAndCaseAfterASystem(uniqId, "", "", aSystemRes);

            // STEP: 5. SET RESPONSE
            LoginRes loginRes = setLoginRespContainer(uniqId, idno, ipAddress);
            CaseRes res = new CaseRes(loginRes, aSystemRes, getOtpTelRes);
            return WebResult.GetResultString(0, "成功", res);

        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (IntrusionException e) {
            return WebResult.GetResultString(9, "Invalid Input", null);
        } catch (Exception e) {
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
    }


    /** 2. User login from PI page but by mobile auto
     * 
     * @param reqJson
     * @return
     */
    public String MobileFreeLogin(String reqJson) {
        logger.info("========>> #SERVICE Access the MobileFreeLogin: {}", reqJson);
        
        try {

            if (!ESAPI.validator().isValidInput("RPL", reqJson, "SafeJson", Integer.MAX_VALUE, false)) {
                return WebResult.GetFailResult();
            }

            Gson gson = new Gson();
            MobileLoginView loginView = gson.fromJson(reqJson, MobileLoginView.class);
            if (!CheckUtil.check(loginView)) {
                throw new ErrorResultException(1, "參數錯誤", null);
            }
            String deJson = ds.Dcodes(loginView.getToken(), "ceet");
            TokenDecryptView tdv = gson.fromJson(deJson, TokenDecryptView.class);

            String 
                uniqId = sns.getUniqId(),
                idno = tdv.getIdno(),
                ipAddress = loginView.getIpAddress();

            // STEP: 1. CREATE New FM_CaseData
            fmcdDao.createByLoginAtMobile(uniqId, idno, ipAddress);

            // STEP: 2. CONNECT A-SYSTEM
            List<ASystemRes> asResList = ass.qryUserInfoFromASystem(idno);

            // STEP: 3. UPDATE A-SYSTEM TO FM_CaseData
            updateEntryAndCaseAfterASystem(uniqId, "", "", asResList);

            // STEP: 4. UPDATE A-SYSTEM TO FM_CaseData
            LoginRes loginRes = setLoginRespContainer(uniqId, idno, ipAddress);
            CaseRes caseRes = new CaseRes(loginRes, asResList, new GetOtpTelResDto());

            return WebResult.GetResultString(0, "成功", caseRes);

        } catch (IntrusionException e) {
            return WebResult.GetResultString(9, "Invalid Input", null);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
    }


    /** 3. Find the CaseData by uniqId to get protocol
     * 
     * @param uniqId
     * @param reqBody
     * @return
     */
    public String getContractsListByASystem(String uniqId, String reqBody) {
        logger.info("========>> Access the getContractsListByASystem: uniqId: {}, reqBody: {}", uniqId, reqBody);
        try {
            if (!ESAPI.validator().isValidInput("RPL", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
                return WebResult.GetFailResult();
            }

            FM_CaseData fmcd = fmcdDao.ReadByUniqId(uniqId);
            ASystemRes resAS = new Gson().fromJson(fmcd.getASystemContent(), ASystemRes.class);
            List<ASystemRes> res = Arrays.asList(resAS);
            return WebResult.GetResultString(0, "成功", res);

        } catch (IntrusionException e) {
            return WebResult.GetResultString(9, "Invalid Input", null);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
    }


    /**
     * 
     * @return
     */
    public String getModifyRangeByDB() {
        logger.info("========>> #SERVICES Access the getModifyRangeByDB().... ");
        try {
            FM_Config fmConfig = new FM_Config();
            fmConfig.setKeyName("Range");
            FM_Config res = fmcDao.Read(fmConfig);
            return new Gson().toJson(res);

        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            return WebResult.GetResultString(99, "系統錯誤", null);
        }

    }


    /** 5. get Credit Modify Terms
     * 
     * @return
     */
    public String getCreditModifyTerms(String reqBody, String uniqId) {
        logger.info("========>> #SERVICE Access the getCreditModifyTerms: reqBody: {}", reqBody);
        try {
            if (!ESAPI.validator().isValidInput( "RPL", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
                return WebResult.GetFailResult();
            }

            FM_CaseData fmcd = fmcdDao.ReadByUniqId(uniqId);
            CreditModifyTermView cmtv = new CreditModifyTermView();

            switch (fmcd.getCaseType()) {
                case "1":
                    String htmlStr1 = pdfs.createAndPreviewPDFMk1(uniqId);
                    cmtv.setCreditModifyTerm(Arrays.asList( htmlStr1, ""));
                    break;
                case "2":
                    String htmlStr2 = pdfs.createAndPreviewHtmlMk1(uniqId);
                    cmtv.setCreditModifyTerm(Arrays.asList( "", htmlStr2));
                    break;
                case "4":
                    String htmlStr4A = pdfs.createAndPreviewPDFMk1(uniqId);
                    String htmlStr4B = pdfs.createAndPreviewHtmlMk1(uniqId);
                    cmtv.setCreditModifyTerm(Arrays.asList( htmlStr4A, htmlStr4B));
                    break;
                default:
                    throw new Exception("Error");
            }
            return WebResult.GetResultString(0, "成功", cmtv);

        } catch (IntrusionException e) {
            return WebResult.GetResultString(9, "Invalid Input", null);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            return WebResult.GetResultString(99, "FAILED", null);
        }
    }


    /** SET MAIL ADDRESS
     * 
     */
    public String setMailAndModifyToDB(String uniqId, String reqBody) {
        logger.info("========>> Access the setMailAddressDB()....");
        try {
            if (!ESAPI.validator().isValidInput( "RPL", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
                return WebResult.GetFailResult();
            }
            DataConfirmView req = new Gson().fromJson(reqBody, DataConfirmView.class);
            fmcdDao.UpdateTwoColumnByUniqId(uniqId, "EmailAddress", req.getEmailAddress(), "CreditModify", req.getCreditModify());
            return WebResult.GetResultString(0, "成功", null);

        } catch (IntrusionException e) {
            return WebResult.GetResultString(9, "Invalid Input", null);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            return WebResult.GetResultString(99, "FAILED", null);
        }
    }


    /** 6. Acitive at last page when everything is down
     * 
     * acitve those step
     * 
     *      1. Update CaseData to A-System & check
     *      2. MailHunter: send the mail to tell the user step finish
     *      3. Create PDF: without API finish, not thing here
     *      4. Update Media: still no API, have example code
     * 
     * @param uniqId
     * @param reqBody
     * @return
     */
    public String getAllFinish(String uniqId, String reqBody) {
        logger.info("========>> Access the getAllFinish {}", reqBody);

        try {
            if (!ESAPI.validator().isValidInput("allFinish", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
                throw new Exception();   
            }

            ASystemReq req = new Gson().fromJson(reqBody, ASystemReq.class);
            if (!CheckUtil.check(req)) {
                throw new ErrorResultException(1, "參數錯誤", null);
            }

            // STEP: SEND Update CaseData to A-System & Get result
            String
                docType = isMock ? "MOCK" : req.getDocType(),
                modify  = req.getCreditModify(),
                idno    = req.getUserId().trim(),
                resStr  = "";
            List<ASystemRes> resp = null;
            switch (docType) {
                case "1":
                    resp = ass.updateUserModifiedData(req, idno, docType, modify);
                    resStr = aSystemUpdateResponseHandlerMk2(resp);
                    break;
                case "2":
                    resp = ass.updateUserModifiedData(req, idno, docType, modify);
                    resStr = aSystemUpdateResponseHandlerMk2(resp);
                    break;
                case "4":
                    resp = ass.updateUserModifiedData(req, idno, "1", modify);
                    if (resp.get(0).getSO2419().equals("0000")) {
                        List<ASystemRes> resp2 = ass.updateUserModifiedData(req, idno, "2", modify);
                        resStr = aSystemUpdateResponseHandlerMk2(resp2);
                    }
                    break;
                case "MOCK":
                    resp = ass.transfer(mds.mockASystemDataManageMk3(docType, modify));
                    resStr = WebResult.GetResultString(0, "SUCCESSFUL", resp);
                    break;
                default:
                    throw new Exception("ERROR");
            }

            if ( resp.size() == 0 ) {
                throw new Exception("FAILED");
            }

            fmcdDao.UpdateSingleColumnByUniqId(uniqId, "CreditModify", req.getCreditModify());
            fmcdDao.UpdateSingleColumnByUniqId(uniqId, "Status", SystemConst.RPL_STATUS_A_SYSTEM_FINISH);

            logger.info("========<< Get updateUserModifiedData of ASystemUpdateResp: {}", resp);
            return resStr;

        } catch (ErrorResultException e) {
            fmcdDao.UpdateStatusByUniqId(uniqId, SystemConst.RPL_STATUS_4);
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("<<======== #SERVICE ERROR UNKNOWN.... {} ", e);
            fmcdDao.UpdateStatusByUniqId(uniqId, SystemConst.RPL_STATUS_4);
            return WebResult.GetResultString( 99, "系統錯誤", e);
        }
    }


    // SECTOR: METHOD
    private void updateEntryAndCaseAfterASystem(String uniqId, String uniqType, String entry, List<ASystemRes> asResList) {
        EntryData ed = new EntryData(uniqId, uniqType, entry, "FM", "", "", "");
        EntryDataDetail edd = new EntryDataDetail();
        fmedDao.Create(ed);
        fmdeeDao.Create(edd);
        fmcdDao.UpdateAfterASytemResponseByUniqId(uniqId, asResList.get(0));
    }


    private LoginRes setLoginRespContainer(String uniqId, String idno, String ipAddress) {
        LoginRes loginResp = new LoginRes();
        loginResp.setUniqId(uniqId);
        loginResp.setIdno(idno);
        loginResp.setIpAddress(ipAddress);
        return loginResp;
    }


    private String aSystemUpdateResponseHandlerMk2(List<ASystemRes> resp) {
        return resp.get(0).getSO2419().equals("0000")
                    ? WebResult.GetResultString( 0, "SUCCESSFUL", resp)
                    : WebResult.GetResultString( 2, "立約失敗 錯誤代碼:(" + resp.get(0).getSO2419() + ")", "");
    }


}
