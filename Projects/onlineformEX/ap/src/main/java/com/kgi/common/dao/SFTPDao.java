package com.kgi.common.dao;

import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.springframework.stereotype.Repository;

@Repository
public class SFTPDao {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    public String download(String serverAddress, int serverPort, String userId, String pwd, String ftppath,
            String fileName) {
        String kgiCard = "";
        InputStream is = null;
        ChannelSftp sftp = null;
        Session session = null;
        try {
            JSch jsch = new JSch();
            /*
             * if (privateKey != null) { jsch.addIdentity(privateKey);// 設定私鑰 }
             */
            session = jsch.getSession(userId, serverAddress, serverPort);
            if (pwd != null) {
                session.setPassword(pwd);
            }
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setTimeout(120000);
            session.setConfig(config);
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            sftp = (ChannelSftp) channel;
            if (ftppath != null && !"".equals(ftppath)) {
                sftp.cd(ftppath);
            }
            is = sftp.get(fileName);
            kgiCard = IOUtils.toString(is, "UTF-8");
        } catch (Exception e) {
            logger.error("Error: " + e.getMessage());
            // e.printStackTrace();
        } finally {
            try {
                if (is != null) {               
                    is.close();
                }       
                if (sftp != null) {
                    if (sftp.isConnected()) {
                        sftp.disconnect();
                    }
                }
                if (session != null) {
                    if (session.isConnected()) {
                        session.disconnect();
                    }
                }
            } catch (Exception e) {
                logger.error("系統錯誤",e.getMessage());
            }
        }
        return kgiCard;
    }


    public boolean upload(String serverAddress,int serverPort, String userId, String  pwd, String ftppath, String filename, InputStream inputStream) {
        ChannelSftp sftp = null;
        Session session = null;
        if (inputStream == null) //處理檔案為空             
			 return false;  
        try {
            JSch jsch = new JSch();
            /*
             * if (privateKey != null) { jsch.addIdentity(privateKey);// 設定私鑰 }
             */
            session = jsch.getSession(userId, serverAddress, serverPort);
            if (pwd != null) {
                session.setPassword(pwd);
            }
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            sftp = (ChannelSftp) channel;
            sftp.cd(ftppath);                 
            sftp.put(inputStream, filename);  //上傳檔案            
            logger.info("The file "+filename+" is uploaded successfully.");        	
			return true;
        } catch (JSchException e) {
            logger.error("JSchException: " + e.getMessage());
            e.printStackTrace();
            return false; 
        } catch (Exception e) {
            logger.error("Error: " + e.getMessage());
            e.printStackTrace();
            return false; 
        } finally {
            if (sftp != null) {
                if (sftp.isConnected()) {
                    sftp.disconnect();
                }
            }
            if (session != null) {
                if (session.isConnected()) {
                    session.disconnect();
                }
            }
        }
    }

}