package com.kgi.common.service;

import org.springframework.stereotype.Service;

@Service
public class EmailContentService {

    //6.1.1.	外開申請未完成提醒
    public String breakpointMailContent(){
        StringBuilder sb = new StringBuilder();
        sb.append("<html><body>");
//        sb.append("<p>警語：此為系統發送之信件，請勿直接回覆</p>");
        sb.append("<p>提醒您 {BatchId}，<br> {Info} <br> 尚未完成本次薪資帳戶申請，請儘速聯繫顧客繼續辦理。若超過{EndDate}仍未完成，顧客需至分行申辦。</p>");
//        sb.append("<p align='right'>凱基銀行  敬上</p>");
        sb.append("</body></html>");
        return sb.toString();
    }

    public String sendOrbitMailContent(){
        StringBuilder sb = new StringBuilder();
        sb.append("<html><body>");
//        sb.append("<p>警語：此為系統發送之信件，請勿直接回覆</p>");
        sb.append("<p>提醒您 {BatchId}，{CorpName}網址已到期，請至後台<a href='{url}'>{url}</a>，查詢顧客續辦理狀況。</p>");
//        sb.append("<p align='right'>凱基銀行  敬上</p>");
        sb.append("</body></html>");
        return sb.toString();
    }

    public String unfinishReminderMailContent(){
        StringBuilder sb = new StringBuilder();
        sb.append("<html><body>");
//        sb.append("<p>警語：此為系統發送之信件，請勿直接回覆</p>");
        sb.append("<p>提醒您 {BatchId}，<br> {Info} <br> 尚未填寫本次薪資帳戶申請，請儘速聯繫顧客繼續辦理。若超過{EndDate}仍未填寫，顧客需至分行申辦。</p>");
//        sb.append("<p align='right'>凱基銀行  敬上</p>");
        sb.append("</body></html>");
        return sb.toString();
    }

}
