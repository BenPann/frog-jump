package com.kgi.common.dao;

import java.util.List;

import com.kgi.eopend3.common.dto.db.BrowseLog;

import org.springframework.stereotype.Repository;

@Repository
public class BrowseLogDao extends CRUDQDao<BrowseLog> {

	@Override
	public int Create(BrowseLog fullItem) {
		String sql = "INSERT INTO BrowseLog (UniqId, UniqType, Page, Action, UTime) VALUES (?,?,?,?,GETDATE())";
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getUniqId(), fullItem.getUniqType(), fullItem.getPage(),fullItem.getAction() });
	}

	@Override
	public BrowseLog Read(BrowseLog keyItem) {
		return null;
	}

	@Override
	public int Update(BrowseLog fullItem) {
		return 0;
	}

	@Override
	public int Delete(BrowseLog keyItem) {
		return 0;
	}

	@Override
	public List<BrowseLog> Query(BrowseLog keyItem) {
		return null;
	}

}
