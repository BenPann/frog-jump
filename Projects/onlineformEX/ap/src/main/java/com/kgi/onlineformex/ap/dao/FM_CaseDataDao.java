package com.kgi.onlineformex.ap.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.kgi.common.dao.CRUDQDao;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.onlineformex.ap.exception.ErrorResultException;
import com.kgi.onlineformex.common.dto.customDto.ASystemRes;
import com.kgi.onlineformex.common.dto.db.FM_CaseData;
import com.kgi.onlineformex.common.dto.view.OFLoginView;

import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class FM_CaseDataDao extends CRUDQDao<FM_CaseData> {


    // SECTOR: CREATE
    @Override
    public int Create(FM_CaseData fullItem) {
        String sql = "INSERT INTO FM_CaseData (UniqId, Status, Idno, Birthday, ASystemContent, ASystemCaseNo,"
        + " UserType, CaseType, UserName, EmailAddress, Phone, ManageStore, CreditCur, CreditContract, CreditModify, EMailSendTime,"
        + " ImageSendTime, AuthStatus, AuthErrCount, AuthTime, ResultHtml, IpAddress, CreateTime, UpdateTime) "
        + " VALUES (:UniqId, :Status, :Idno, :Birthday, :ASystemContent, :ASystemCaseNo, :UserType, :CaseType,"
        + ":UserName, :EmailAddress, :Phone, :ManageStore, :CreditCur, :CreditContract, :CreditModify, :EMailSendTime, :ImageSendTime,"
        + ":AuthStatus, :AuthErrCount, :AuthTime, :ResultHtml, :IpAddress, getdate(), getdate() )";
        return this.getNamedParameterJdbcTemplate().update(
                        sql,
                        new BeanPropertySqlParameterSource(fullItem));
    }


    public int createByLogin(String uniqId, OFLoginView loginView) {
        String
            idno      = loginView.getIdno(),
            ipAddress = loginView.getIpAddress(),
            birthday  = loginView.getBirthday();
        FM_CaseData fmcd = new FM_CaseData(uniqId);
        fmcd.setStatus(SystemConst.CASE_STATUS_INIT);
        fmcd.setIpAddress(ipAddress);
        fmcd.setUniqId(uniqId);
        fmcd.setIdno(idno);
        fmcd.setBirthday(birthday);
        return Create(fmcd);
    }
    

    public int createByLoginAtMobile(String uniqId, String idno, String ip) {
        FM_CaseData fmcd = new FM_CaseData(uniqId);
        fmcd.setStatus(SystemConst.CASE_STATUS_INIT);
        fmcd.setIpAddress(ip);
        fmcd.setUniqId(uniqId);
        fmcd.setIdno(idno);
        return Create(fmcd);
    }


    public FM_CaseData touch(FM_CaseData keyItem) {
        // CHECK UniqId
        if (keyItem == null || StringUtils.isBlank(keyItem.getUniqId())) {
            return null;
        }

        List<FM_CaseData> list = new ArrayList<>();
        try {
            list = this.getJdbcTemplate().query(
                    "SELECT * FROM FM_CaseData WHERE UniqId = ?",
                    new Object[] { keyItem.getUniqId() },
                    new BeanPropertyRowMapper<>(FM_CaseData.class));
        } catch (DataAccessException ex) {
            // ignore
        }

        // CREATE one if not found
        if (list.size() == 0) {
            keyItem.setAuthErrCount("0");
            Create(keyItem);
            return Read(keyItem);
        } else if (list.size() == 1) {
            return list.get(0);
        } else {
            return null;
        }
    }


    // SECTOR: READ
    @Override
    public FM_CaseData Read(FM_CaseData keyItem) {
        try {
            return this.getJdbcTemplate().queryForObject(
                            "SELECT * FROM FM_CaseData WHERE UniqId = ?",
                            new BeanPropertyRowMapper<>(FM_CaseData.class),
                            new Object[] { keyItem.getUniqId() });
        } catch (DataAccessException ex) {
            logger.error("<<<<======== #DB FM_CaseData, 查無資料");           
            throw new ErrorResultException(9, "FM_CaseData 查無資料", ex);
        }
    }


    public FM_CaseData ReadByUniqId(String uniqId) {
        FM_CaseData fmcd = new FM_CaseData(uniqId);
        return this.Read(fmcd);
    }


    @Override
    public List<FM_CaseData> Query(FM_CaseData keyItem) {
		try {
            return this.getJdbcTemplate().query(
                            "SELECT * FROM FM_CaseData WHERE Status = ?", 
                            new Object[] { keyItem.getStatus() }, 
                            new BeanPropertyRowMapper<>(FM_CaseData.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }


    public List<FM_CaseData> QueryListByStatusMk1(String... status) throws Exception {
        try {
            return this.getJdbcTemplate().query(
                            "SELECT * FROM FM_CaseData WHERE Status IN ( ? )",
                            new Object[] { status[0] },
                            new BeanPropertyRowMapper<>(FM_CaseData.class));
        } catch (DataAccessException e) {
            return null;
        }
    }


    public List<FM_CaseData> QueryListByStatus(String... status) throws Exception {
        String qu = "?";
        for (int i = 1; i < status.length; i++) {
            qu += ", ?";
        }
        try {
            return this.getJdbcTemplate().query(
                    "SELECT * FROM FM_CaseData WHERE Status IN ( " + qu + " )",
                    status,
                    new BeanPropertyRowMapper<>(FM_CaseData.class));
        } catch (DataAccessException e) {
            return null;
        }
    }


    /** 取得待處理的專人處理聯絡case名單 */
    public List<Map<String, Object>> queryCaseDocumentList(String uniqId) throws Exception {
        try {
            return this.getJdbcTemplate().queryForList(
                            "SELECT Content FROM dbo.FM_CaseDocument WHERE UniqId = ? ",
                            new Object[] { uniqId });
        } catch (DataAccessException e) {
            return null;
        }

    }


    // SECTOR: UPDATE
    @Override
    public int Update(FM_CaseData fullItem) {
        return 0;
    }


    public int UpdateStatusByUniqId(String uniqId, String status) {
        return this.getJdbcTemplate().update(
                "UPDATE FM_CaseData SET Status = ? , UpdateTime = getdate() WHERE UniqId = ? ", 
                status, 
                uniqId);
    }


    public int UpdateAfterASytemResponseByUniqId(String uniqId, ASystemRes asContent) {
        String strAS = new Gson().toJson(asContent, ASystemRes.class);
        return this.getJdbcTemplate().update(
                "UPDATE FM_CaseData" +
                " SET ASystemContent = ?," +
                    " ASystemCaseNo = ?," +
                    " CaseType = ?," + 
                    " Phone = ?," +
                    " ManageStore = ?," +
                    " CreditCur = ?," +
                    " UserName = ?," +
                    " AuthStatus = ?," +
                    " AuthErrCount = ?," +
                    " EmailAddress = ? " +
                "WHERE UniqId = ? ",
                strAS,
                asContent.getCardNum(),
                asContent.getCaseType(),
                asContent.getPhone(),
                asContent.getManageStore(),
                asContent.getCreditCur(),
                asContent.getUserName(),
                "0",
                "0",
                asContent.getEmail(),
                uniqId);
    }


    public int UpdateAfterOTPCheckResp(FM_CaseData fmcd) {
        return this.getNamedParameterJdbcTemplate().update(
                "UPDATE FM_CaseData" +
                " SET Phone = :Phone," +
                    " IpAddress = :IpAddress," +
                    " AuthStatus = :AuthStatus," +
                    " AuthTime = :AuthTime," +
                    " AuthErrCount = :AuthErrCount," +
                    " Status = :Status " +
                "WHERE UniqId = :UniqId ",
                new BeanPropertySqlParameterSource(fmcd)
        );
    }


    public int UpdateSingleColumnByUniqId(String uniqId, String columnName, String columnValue) {
        return this.getJdbcTemplate().update(
                    "UPDATE FM_CaseData SET " + columnName + " = ?, UpdateTime = getdate() WHERE UniqId = ? ",
                    columnValue,
                    uniqId);
    }


    public int UpdateTwoColumnByUniqId(String uniqId, String columnName1, String columnValue1, String columnName2, String columnValue2) {
        return this.getJdbcTemplate().update(
                        "UPDATE FM_CaseData SET " + columnName1 + " = ?, " + columnName2 + " = ? , UpdateTime = getdate() WHERE UniqId = ? ",
                        columnValue1,
                        columnValue2, 
                        uniqId);
    }


    // SECTOR: DELETE
    @Override
    public int Delete(FM_CaseData keyItem) {
        return 0;
    }



    // SECTOR: METHOD CUSTOM
    public int inDWEmailSuccess(String emailAddress) {
        try {
            // HACK: I guess this is find by KGI inner-DB
            // String sql = "SELECT count(*) FROM ODBNS.BNS_CUMI WHERE EMAIL_ADD1 = ? OR EMAIL_ADD2 = ?";					
            // return this.getJdbcTemplate(kPDB01DataSource).queryForObject(sql,Integer.class, new Object[] { emailAddress  , emailAddress });
            return 1;
        } catch (DataAccessException ex) {
            return 0;
        }
    }


    /**
     * Search AirLoan-DB to get Email status is where success or not
     * 
     * @param emailAddress
     * @param idno
     * @return
     */
    public int inAirloanEmailSuccess(String emailAddress, String idno) {
		String sql = "SELECT count(*) FROM FM_CaseData data left join ED3_AOResult ao on data.UniqId = ao.UniqId ";
        sql += " WHERE EmailAddress = ? and idno <> ? and ( ao.Status <> '1' or ao.Status is null ) ";				
		try {
            return this.getJdbcTemplate().queryForObject(sql,Integer.class, new Object[] { emailAddress  , idno });
        } catch (DataAccessException ex) {
            return 0;
        }
    }



}
