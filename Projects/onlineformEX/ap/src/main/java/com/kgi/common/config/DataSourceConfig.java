package com.kgi.common.config;

import java.util.Arrays;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class DataSourceConfig {


    @Autowired
    private KPDB01DBConfig kPDB01DBConfig;
    @Autowired
    private StaticDBConfig staticDBConfig;
    @Autowired
    private StaticDB2Config staticDB2Config;
    @Autowired
    private PayerDBConfig payerDBConfig;

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String datasourceName = "mainDB";


    @Bean("kPDB01DB")
    public DataSource kPDB01DBDataSource() {
        //引用參數建立datasource
        DataSource build = DataSourceBuilder.create().username(kPDB01DBConfig.getUsername()).password(kPDB01DBConfig.getPazzd())
                .url(kPDB01DBConfig.getUrl()).driverClassName(kPDB01DBConfig.getDriverClassName()).build();
        return build;
    }

   
    @Bean("mainDB")
    @Primary
    public DataSource mainDataSource() {
        //引用參數建立datasource
        DataSource build = DataSourceBuilder.create().username(staticDBConfig.getUsername()).password(staticDBConfig.getPazzd())
                .url(staticDBConfig.getUrl()).driverClassName(staticDBConfig.getDriverClassName()).build();
        return build;
    }


    @Bean("mainDB2")
    public DataSource mainDataSource2() {
        DataSource build = DataSourceBuilder.create().username(staticDB2Config.getUsername()).password(staticDB2Config.getPazzd())
                .url(staticDB2Config.getUrl()).driverClassName(staticDB2Config.getDriverClassName()).build();
        return build;
    }

    
    @Bean("payerDB")
    public DataSource payerDBDataSource() {
        DataSource build = DataSourceBuilder.create().username(payerDBConfig.getUsername()).password(payerDBConfig.getPazzd())
                .url(payerDBConfig.getUrl()).driverClassName(payerDBConfig.getDriverClassName()).build();
        return build;
    }


    @Bean(name = "staticDBManager")
    @Primary
    public PlatformTransactionManager staticDBManager(@Qualifier(datasourceName) DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "kPDB01DBManager")
    public PlatformTransactionManager kgiBranchDBManager(@Qualifier("kPDB01DB") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }


    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration config = new CorsConfiguration();

        logger.info("========> Access the Global");

        config.addAllowedOrigin("/*");
        config.setAllowCredentials(true);
        config.addAllowedMethod("/*");
        config.setAllowedMethods(Arrays.asList("GET", "PUT", "POST","DELETE"));
        config.addAllowedHeader("/*");
        config.addExposedHeader("/*");
        UrlBasedCorsConfigurationSource configSource = new UrlBasedCorsConfigurationSource();
        configSource.registerCorsConfiguration("/**", config);

        return new CorsFilter(configSource);
    }

}
