package com.kgi.common.config;

import java.nio.charset.Charset;

import com.kgi.eopend3.common.util.crypt.FucoAESUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = { "file:${OFEX_CONFIG_PATH}/DB.properties" }, encoding = "UTF-8")
public class UPDBConfig implements IBaseDBConfig {

    @Value("${updb.username}")
    private String username;
    @Value("${updb.pazzd}")
    private String pazzd;
    @Value("${updb.url}")
    private String url;
    @Value("${updb.driverclassname}")
    private String driverclassname;

    @Autowired
    private GlobalConfig config;

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPazzd() {
        FucoAESUtil instance = FucoAESUtil.getInstance(config.EncryptScretKey,config.EncryptIv);

        try {
            return new String(instance.decrypt(pazzd), Charset.forName("UTF-8")) ;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public void setPazzd(String pazzd) {
        this.pazzd = pazzd;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getDriverClassName() {
        return driverclassname;
    }

    @Override
    public void setDriverClassName(String driverClassName) {
        this.driverclassname = driverClassName;
    }
}