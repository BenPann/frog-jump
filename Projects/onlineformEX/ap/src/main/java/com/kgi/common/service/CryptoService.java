package com.kgi.common.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service
public class CryptoService {


    private Logger logger = LoggerFactory.getLogger(this.getClass());


    public boolean EncryptedMD5(String password) throws NoSuchAlgorithmException {
        String hash = "35454B055CC325EA1AF2126E27707052";

	    MessageDigest md = MessageDigest.getInstance("MD5");
	    md.update(password.getBytes());
	    byte[] digest = md.digest();
        String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
        if (myHash.equals(hash)) {
            return true;
        } else {
            return false;
        }
    }

    public String shaEncode(String inStr)  {
        MessageDigest sha = null;
        try {
            sha = MessageDigest.getInstance("SHA");
            byte[] byteArray = inStr.getBytes("UTF-8");
            byte[] md5Bytes = sha.digest(byteArray);
            StringBuffer hexValue = new StringBuffer();
            for (int i = 0; i < md5Bytes.length; i++) {
                int val = ((int) md5Bytes[i]) & 0xff;
                if (val < 16) {
                    hexValue.append("0");
                }
                hexValue.append(Integer.toHexString(val));
            }
            return hexValue.toString();
        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
            return "";
        }
    }


    /** 加密
     * 
     */
    public String Ecodes(String content, String key) {
        logger.info("========>> #CRYPTIC Access the Ecodes()....");
        if (content == null || content.length() < 1) return null;
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(128, new SecureRandom(key.getBytes()));
            SecretKey secretKey = kgen.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec secretKeySpec = new SecretKeySpec(enCodeFormat, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            byte[] byteContent = content.getBytes("UTF-8");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            byte[] byteRresult = cipher.doFinal(byteContent);
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteRresult.length; i++ ) {
                String hex = Integer.toHexString(byteRresult[i] & 0xFF);
                if (hex.length() == 1) {
                    hex = '0' + hex;
                }
                sb.append(hex.toUpperCase());
            }
            return sb.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }


    /** 解密
     * 
     */
    public String Dcodes(String content, String key) {

        logger.info("========>> #CRYPTIC Access the Dcodes()....");

        if (content == null || content.length() < 1) return null;
        if (content.trim().length() < 19) return content;

        byte[] byteRresult = new byte[content.length() / 2];
        for (int i = 0; i < content.length() / 2; i++ ) {
            int high = Integer.parseInt(content.substring(i * 2, i * 2 + 1), 16);
            int low = Integer.parseInt(content.substring(i * 2 + 1, i * 2 + 2), 16);
            byteRresult[i] = (byte) (high * 16 + low);
        }
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(128, new SecureRandom(key.getBytes()));
            SecretKey secretKey = kgen.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec secretKeySpec = new SecretKeySpec(enCodeFormat, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            byte[] result = cipher.doFinal(byteRresult);

            return new String(result);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<String> DcodesAtList(String content, String key) {

        List<String> list1 = new ArrayList<>();
        String res1 = Dcodes(content, key);
        
        String[] resArr = res1.split(",");
        for (String str: resArr) {
            logger.info("========> #CRYPTIC 解密出的元素: {}", str);
            list1.add(str);
        }

        return list1;
    }


    /**
     * 呼叫測試
     * 
     * 詳細解釋
     * 【ceet為加密的密匙】
     * 【admin為需要加密的字串】
     * 【67BE5ED967DBA9B9810C295BE6DEF5D5為解密後的字串】
     * 【如果更改ceet，那麼67BE5ED967DBA9B9810C295BE6DEF5D5字串會發生變化】
     * @param args
     */
    public static void main(String[] args) {
        
        CryptoService ds = new CryptoService();

        // String arg1 = "0827";
        // String res1 = ds.Ecodes(arg1, "ceet");
        // String res2 = ds.Dcodes(res1, "ceet");
        // List<String> res3 = ds.DcodesAtList(res1, "ceet");
        // System.out.println("========> 需要加密的內容：" + res1);
        // System.out.println("========> 經過解密的內容：" + res2);
        // System.out.println("========> 經過解密的內容 List：" + res3);        

        String argSha = "123456";
        String resSha = ds.shaEncode(argSha);
        System.out.println("========> Sha encrypted result: " + resSha);

    }


}