package com.kgi.onlineformex.ap.dao;

import com.kgi.common.dao.BaseDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.TransactionStatus;

public class PDFDao extends BaseDao {

    public void storePDF(String uniqId, String idno, String pdfBase64Str, String imageBase64Str) {

        JdbcTemplate jdbctemp = this.getJdbcTemplate();

        // 增加先刪除的方式
        TransactionStatus status = this.BeginTransaction();
        
        String fir = "delete from CaseDoc where caseNo=? ; ";
        String sec = "insert into CaseDoc values(?,?,?,?); ";

		try {
			jdbctemp.update(fir,
					new Object[] { uniqId });
			jdbctemp.update(sec,
					new Object[] { uniqId, idno, pdfBase64Str, imageBase64Str });
			this.CommitTransaction(status);
		} catch (DataAccessException e) {
			this.RollbakcTransaction(status);
		}


    }
}
