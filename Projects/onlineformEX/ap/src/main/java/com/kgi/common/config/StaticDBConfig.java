package com.kgi.common.config;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.annotation.PostConstruct;
import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = { "file:${OFEX_CONFIG_PATH}/DB.properties" }, encoding = "UTF-8")
public class StaticDBConfig implements IBaseDBConfig {

    @Autowired
    private UPDBConfig updbConfig;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${staticDBConfig.username}")
    private String username;
    // @Value("${staticDBConfig.password}") // FIXME: Charles: Comment me.
    private String pazzd;
    @Value("${staticDBConfig.url}")
    private String url;
    @Value("${staticDBConfig.driverclassname}")
    private String driverclassname;
    @Value("${staticDBConfig.code}")
    private String code;
    @Value("${staticDBConfig.dbName}")
    private String dbName;

    @PostConstruct
    public void init() {
        logger.info(updbConfig.getUrl());
        logger.info(updbConfig.getUsername());
        // 凱基特殊處理 要先去另一個DB 取得帳密

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(updbConfig.getDriverClassName());
        basicDataSource.setUrl(updbConfig.getUrl());
        basicDataSource.setUsername(updbConfig.getUsername());
        basicDataSource.setPassword(updbConfig.getPazzd());
        logger.info("設定updb連線");
        // DataSource dataSource = basicDataSource;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rest = null;
        String dbuser = "";
        String dbpazzd = "";
        try {
            logger.info("開始updb連線");
            conn = basicDataSource.getConnection();
            logger.info("取得KPDB01的帳號密碼");
            String sql = "exec usp_GetConnectAccountData " + code + "," + username + "," + dbName;
            stmt = conn.prepareStatement(sql);
            rest = stmt.executeQuery();
            while (rest.next()) {
                dbuser = rest.getString("ConnectAccount"); // 1
                dbpazzd = rest.getString("Password"); // 3
            }
            // logger.info("StaticDBUser:"+dbuser+" StaticDBpwd:"+dbpazzd);
            this.setUsername(dbuser);
            this.setPazzd(dbpazzd);
        } catch (Exception e) {
            logger.error("未知錯誤", e);
        } finally {
            try {
                if (rest != null) {
                    rest.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                    basicDataSource.close();
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPazzd() {
        return pazzd;
    }

    @Override
    public void setPazzd(String pazzd) {
        this.pazzd = pazzd;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getDriverClassName() {
        return driverclassname;
    }

    @Override
    public void setDriverClassName(String driverClassName) {
        this.driverclassname = driverClassName;
    }
}