package com.kgi.onlineformex.ap.dao;

import java.util.List;
import java.util.Map;

import com.kgi.common.dao.CRUDQDao;
import com.kgi.onlineformex.common.dto.db.FM_CaseDocument;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class FM_CaseDocumentDao extends CRUDQDao<FM_CaseDocument> {

    @Override
    public int Create(FM_CaseDocument fullItem) {
        return this.getJdbcTemplate().update(
                    "INSERT INTO FM_CaseDocument VALUES( ?, ?, ?, ?, ?, ?, ?, GETDATE(), GETDATE())", 
                    fullItem.getUniqId(),
                    fullItem.getUniqType(),
                    fullItem.getDocumentType(),
                    fullItem.getContent(),
                    fullItem.getXmlContent(),
                    fullItem.getVersion(),
                    fullItem.getOther());
    }


    // SECTOR: QUERY
    public FM_CaseDocument QueryDocumentByUniqId(String uniqId) throws Exception {
        try {
            return this.getJdbcTemplate().queryForObject(
                        "SELECT * FROM FM_CaseDocument WHERE UniqId = ?",
                        new BeanPropertyRowMapper<>(FM_CaseDocument.class), 
                        new Object[] { uniqId });
        } catch (DataAccessException ex) {
            logger.error("========<<<< Config 查無資料");
            return null;
        }
    }


    public List<Map<String, Object>> QueryDocumentByUniqIdList(String uniqId) throws Exception {
        try {
            // List<Map<String, Object>> ls = jdbcTemplate.queryForList(
            // 		"SELECT A.*,B.EMail,B.Idno FROM dbo.ContractDoc A JOIN ContractMain B ON A.ConNo=B.UniqId WHERE A.ConNo = ?",
            // 		new Object[] { uniqId });
            return this.getJdbcTemplate().queryForList(
                        " SELECT * dbo.FM_CaseDocument WHERE UniqId = ? ",
                        new Object[] { uniqId }
            );
        } catch (DataAccessException e) {
            return null;
        }
    }


    public List<Map<String, Object>> QueryDocumentByUniqIdListMk2(String uniqId) throws Exception {
        try {
            return this.getJdbcTemplate().queryForList(
                        "SELECT A.UniqId, A.Status, B.Content FROM dbo.FM_CaseData A JOIN FM_CaseDocument B ON A.UniqId=B.UniqId WHERE A.UniqId = ? AND A.Status = '11'",
                        new Object[] { uniqId }
            );
        } catch (DataAccessException e) {
            return null;
        }
    }

    public FM_CaseDocument queryDocumentByUniqIdMk3(String uniqId) throws Exception {
        try {
            return this.getJdbcTemplate().queryForObject(
                        "SELECT B.* FROM dbo.FM_CaseData A JOIN FM_CaseDocument B ON A.UniqId=B.UniqId WHERE A.UniqId = ? AND A.Status = '11'",
                        new BeanPropertyRowMapper<>(FM_CaseDocument.class),
                        new Object[] { uniqId }
            );
        } catch (Exception e) {
            return null;
        }
    }


    @Override
    public FM_CaseDocument Read(FM_CaseDocument keyItem) {
        try {
            return this.getJdbcTemplate().queryForObject(
                        "SELECT * FROM FM_CaseDocument WHERE UniqId = ?",
                        new BeanPropertyRowMapper<>(FM_CaseDocument.class),
                        new Object[] { keyItem.getUniqId() });
        } catch (DataAccessException ex) {
            logger.error("<<<<======== #DB FM_CaseDocument, 查無資料");
            return null;
        }
    }

    @Override
    public int Update(FM_CaseDocument fullItem) {
        return this.getJdbcTemplate().update(
                    "UPDATE FM_CaseDocument SET Content = ?, Version = ?, Other = ?, UpdateTime = GETDATE() WHERE UniqId = ? AND DocumentType = ? ",
                    fullItem.getContent(),
                    fullItem.getVersion(),
                    fullItem.getOther(),
                    fullItem.getUniqId(),
                    fullItem.getDocumentType());
    }


    public int UpdateSingleColumnByUniqId(String columnName, String columnValue, String uniqId) { 
        return this.getJdbcTemplate().update(
                    "UPDATE FM_CaseDocument SET " + columnName + " = ?, UpdateTime = getdate() WHERE UniqId = ? ",
                    columnValue,
                    uniqId);
    }


    @Override
    public int Delete(FM_CaseDocument keyItem) {
        return 0;
    }

    @Override
    public List<FM_CaseDocument> Query(FM_CaseDocument keyItem) {
        return null;
    }


    

}
