
package com.kgi.common.dao;

import java.util.Base64;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;


@Repository
public class SMTPMailDao {

	private static final Logger logger = LogManager.getLogger(SMTPMailDao.class);

	public  void sendEmail(String host, String port, String  mailTo, String mailFrom, String title, String content, String filecontent) throws Exception {

		if(host.length()==0){
			logger.info("mail to:"+mailTo);
			logger.info("mail title:" + title);
			logger.info("mail content:" + content);
			return;
		}
		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host", host);
		properties.setProperty("mail.smtp.port", port);
		Session session = Session.getDefaultInstance(properties);
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(mailFrom));
			message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(mailTo));
			message.setSubject(title, "UTF-8");
			
			//有PDF檔案時 夾到附件
			if( filecontent != null) {
				Multipart multipart = new MimeMultipart();
				BodyPart msg = new MimeBodyPart();
				msg.setContent(content, "text/html; charset=utf-8");
				multipart.addBodyPart(msg);
				//將base64文件轉成pdf夾在信件附件
				MimeBodyPart filePart = new MimeBodyPart();
				byte[] pdfBytes = Base64.getDecoder().decode(filecontent);
				DataSource ds = new ByteArrayDataSource(pdfBytes, "application/pdf"); 
				filePart.setDataHandler(new DataHandler(ds));;
				multipart.addBodyPart(filePart);
				message.setContent(multipart);
				System.out.println("EMail 夾附件檔OK");
			}else {
				//只有文字內容就直接弄成mail寄出
				message.setContent(content, "text/html; charset=utf-8");
			}
			Transport.send(message);
    }
    
}
