package com.kgi.common.dao;

import java.util.List;

import com.kgi.eopend3.common.dto.db.EntryDataDetail;

import org.springframework.stereotype.Repository;

@Repository
public class EntryDataDetailDao extends CRUDQDao<EntryDataDetail> {

	@Override
	public int Create(EntryDataDetail fullItem) {
		return 0;
	}

	@Override
	public EntryDataDetail Read(EntryDataDetail keyItem) {
		return null;
	}

	@Override
	public int Update(EntryDataDetail fullItem) {
		return 0;
	}

	@Override
	public int Delete(EntryDataDetail keyItem) {
		return 0;
	}

	@Override
	public List<EntryDataDetail> Query(EntryDataDetail keyItem) {
		return null;
	}

}
