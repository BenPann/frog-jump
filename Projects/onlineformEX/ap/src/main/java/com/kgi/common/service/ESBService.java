package com.kgi.common.service;

import com.ibm.mq.MQException;
import com.ibm.tw.commons.net.mq.CMQStrMessage;
import com.ibm.tw.commons.net.mq.MQReceiver;
import com.ibm.tw.commons.net.mq.MQSender;
import com.ibm.tw.commons.util.ConvertUtils;
import com.kgi.common.config.GlobalConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ESBService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private GlobalConfig globalConfig;

    public MQSender getEOPSender() throws MQException {
		MQSender sender = new MQSender();
		logger.info("ESB Port : "+ Integer.parseInt(globalConfig.ESBPort));
		sender.setMQEnvironment(globalConfig.ESBHostName, Integer.parseInt(globalConfig.ESBPort) ,globalConfig.ESBChannel);
		sender.connect(globalConfig.ESBQueueManagerName, globalConfig.ESBQueueSendName);
		return sender;
	}

	public MQReceiver getEOPReceiver() throws MQException {
		MQReceiver receiver = new MQReceiver();
		receiver.setMQEnvironment(globalConfig.ESBHostName, Integer.parseInt(globalConfig.ESBPort) ,globalConfig.ESBChannel);
		receiver.connect(globalConfig.ESBQueueManagerName,globalConfig.ESBQueueReceiveName);
		return receiver;
	}

	public String sendMessageQueue(String rqObject, MQSender sender, MQReceiver receiver) {
		String returnObject = null;
		if (sender == null || receiver == null) {
			throw new RuntimeException("尚未初始化Sender或Receier");
		}
		try {
			byte[] bytes = rqObject.getBytes("UTF8");
			CMQStrMessage message = new CMQStrMessage();
			message.setCCSID(1208);
			message.setMessageData(bytes);
			sender.send(message);
			CMQStrMessage returnMessage = (CMQStrMessage) receiver.receive(message.getMessageId(), 30000);

			returnObject = ConvertUtils.bytes2Str(returnMessage.getMessageData(), "UTF8");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sender != null) {
				sender.close();
			}
			if (receiver != null) {
				receiver.close();
			}
		}
		return returnObject;
	}
    
}
