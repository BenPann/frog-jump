package com.kgi.onlineformex.ap.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.transform.TransformerException;

import com.google.gson.Gson;
import com.ibm.tw.commons.net.mq.MQReceiver;
import com.ibm.tw.commons.net.mq.MQSender;
import com.kgi.common.config.GlobalConfig;
import com.kgi.common.service.APSService;
import com.kgi.common.service.ESBService;
import com.kgi.common.xml.CreateXML;
import com.kgi.eopend3.common.dto.esbdto.GMSCSMT24R2;
import com.kgi.eopend3.common.dto.esbdto.GMSCSMT24R2Content;
import com.kgi.eopend3.common.util.DateUtil;
import com.kgi.onlineformex.ap.dao.FM_ApiLogDao;
import com.kgi.onlineformex.ap.exception.ErrorResultException;
import com.kgi.onlineformex.ap.mockData.MockDataService;
import com.kgi.onlineformex.common.dto.customDto.ASystemReq;
import com.kgi.onlineformex.common.dto.customDto.ASystemRes;
import com.kgi.onlineformex.common.dto.customDto.GetOtpTelReqDto;
import com.kgi.onlineformex.common.dto.customDto.GetOtpTelResDto;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;


@Service
@PropertySource(value = {"file:${OFEX_CONFIG_PATH}/RPL.properties"}, encoding = "UTF-8")
public class ASystemService {


    @Autowired
    private FM_ApiLogDao fmalDao;
    @Autowired
    private GlobalConfig gc;
    @Autowired
    private ESBService esbs;
    @Autowired
    private APSService apss;
    @Autowired
    private MockDataService mds;

    @Value("${isMock}")
    private boolean isMock;

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * 1. A-System Query User-Info at A-System
     * 
     * @param req
     * @return ASystemGetResp
     * @throws Exception
     * @throws ErrorResultException
     */
    public List<ASystemRes> qryUserInfoFromASystem(String idno) {
        logger.info("========>>> #SERVICE Access the qyrUserInfoFromASystem()....");
        ASystemReq req = new ASystemReq("I", " ", idno + " ", "", "FFFFFFF1  ", "");
        List<GMSCSMT24R2Content> list = isMock ? mds.mockASystemDataManageMk1(req) : getGMSCSMT24R2(req);
        return transfer(list);
    }


    /**
     * 2. A-System Update user modifier data what to change
     * 
     * @return ASystemUpdateResp
     */
    public List<ASystemRes> updateUserModifiedData(ASystemReq req, String idno, String docType, String modify) {
        logger.info("========>>> #SERVICE Access the updateUserModifiedData()....");
        List<GMSCSMT24R2Content> list = updateGMSCSMT24R2(idno, docType, modify);
        return transfer(list);
    }

    
    // SECTOR: REQUEST & RESPONSE
    public List<GMSCSMT24R2Content> getGMSCSMT24R2(ASystemReq reqAS) {
        String 
            idno = reqAS.getUserId(),
            ipAddress = reqAS.getShortUrl(),
            startTime = "",
            endTime = "",
            req = "",
            res = "";

		try {
            // INIT:
			req = xmlGMSCSMT24R2BV1Mk1(idno, ipAddress);
            logger.info("========>>> ESB SOAP A-SYSTEM REQEST" + req);
			MQSender sender = esbs.getEOPSender();
            MQReceiver receiver = esbs.getEOPReceiver();

            // STEP: ACCESS THE A-SYSTEM
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			res       = esbs.sendMessageQueue(req, sender, receiver);
			endTime   = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");

            // HACK: PARSE
            GMSCSMT24R2 gmscsmt24r2 = new GMSCSMT24R2();
            logger.info("========<<< ESB SOAP A-SYSTEM RESPONSE" + res);

            return gmscsmt24r2.parseXmlMk1(res);

		} catch (Exception e) {
			logger.error("========<<<< #API ERROR 未知錯誤", e);
			res = e.getMessage();
            return null;

		} finally {
            fmalDao.Create( idno, "-", "GMSCSMT24R2", req, res, startTime, endTime) ;
		}
    }


    public List<GMSCSMT24R2Content> updateGMSCSMT24R2(String idno, String type, String creditModify) {
        String 
            startTime = "",
            endTime = "",
            req = "",
            res = "";
		try {
            // INIT:
			req = xmlGMSCSMT24R2BV2Mk1(type, idno, creditModify);
			MQSender sender = esbs.getEOPSender();
            MQReceiver receiver = esbs.getEOPReceiver();
            // STEP: ACCESS THE A-SYSTEM
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			res       = esbs.sendMessageQueue(req, sender, receiver);
            endTime   = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            logger.info("========<<<< {}", res);
            // HACK: PARSE
			GMSCSMT24R2 gmscsmt24r2 = new GMSCSMT24R2();
            return gmscsmt24r2.parseXmlMk1(res);
		} catch (Exception e) {
			logger.error("========<<<< #API ERROR 未知錯誤", e);
			res = e.getMessage();
            return null;
		} finally {
            fmalDao.Create( idno, "-", "GMSCSMT24R2", req, res, startTime, endTime) ;
		}
    }


    // SECTOR: CREATE SOAP CONTENT A-SYSTEM
    // QUERY
    private String xmlGMSCSMT24R2BV1Mk1(String idno, String ipAddress) {
        String clientID = gc.ESBClientId;
        String clientPAZZD = gc.ESBClientPAZZD;

        logger.info("========>>> #SOAP clientPAZZD: {}", clientPAZZD);

		String timeNow = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		clientPAZZD = DigestUtils.md5Hex(clientPAZZD + timeNow);

        logger.info("========>>> #SOAP clientPAZZD: {}", clientPAZZD);

		StringBuffer sb = new StringBuffer("");

        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:kgib=\"http://www.cosmos.com.tw/kgib\" xmlns:esb=\"http://www.cosmos.com.tw/esb\" xmlns:cics=\"http://www.cosmos.com.tw/cics\" xmlns:bill=\"http://www.cosmos.com.tw/bill\" xmlns:nefx=\"http://www.cosmos.com.tw/nefx\" xmlns:cactx=\"http://www.cosmos.com.tw/cactx\" xmlns:bns=\"http://www.cosmos.com.tw/bns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("<SOAP-ENV:Header />");
		sb.append("<SOAP-ENV:Body>");
		sb.append("<esb:MsgRq>");
		sb.append("<Header>");
		sb.append("<ClientId>").append(clientID).append("</ClientId>");
		sb.append("<ClientPwd>").append(clientPAZZD).append("</ClientPwd>");
		sb.append("<ClientDt>").append(timeNow).append("</ClientDt>");
		sb.append("<TxnId>GMSCSMT24R2B</TxnId>");
		sb.append("<UUID>").append(clientID + DateUtil.GetDateFormatString("yyyyMMddHHmmssSSS")).append("</UUID>");
        sb.append("</Header>");
        sb.append("<SvcRq xsi:type=\"kgib:GMSCSMT24R2BSvcRqType\">");
		sb.append("<SI2401>").append("I").append("</SI2401>");
		sb.append("<SI2402>").append(" ").append("</SI2402>");
		sb.append("<SI2403>").append(idno).append("</SI2403>");
		sb.append("<SI2404>").append("               ").append("</SI2404>");
		sb.append("<SI2405>").append(ipAddress).append("</SI2405>");
		sb.append("<SI2406>").append("       ").append("</SI2406>");
		sb.append("<SI2407>").append("      ").append("</SI2407>");
        sb.append("</SvcRq>");
        sb.append("</esb:MsgRq>");
		sb.append("</SOAP-ENV:Body>");
        sb.append("</SOAP-ENV:Envelope>");
        
        logger.info(sb.toString());
		return sb.toString();
    }


    // UPDATE
    private String xmlGMSCSMT24R2BV2Mk1(String type, String idno, String creditModify) {

        String
            clientID    = gc.ESBClientId,
            clientPAZZD = gc.ESBClientPAZZD,
		    timeNow     = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ"),
            contractVer = type.equals("1") ? timeNow.substring(0, 4).concat(timeNow.substring(5, 7)) : "      ";
            clientPAZZD = DigestUtils.md5Hex(clientPAZZD + timeNow);
        logger.info("========>>> #SOAP clientPAZZD: {}", clientPAZZD);

		StringBuffer sb = new StringBuffer("");

		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:kgib=\"http://www.cosmos.com.tw/kgib\" xmlns:esb=\"http://www.cosmos.com.tw/esb\" xmlns:cics=\"http://www.cosmos.com.tw/cics\" xmlns:bill=\"http://www.cosmos.com.tw/bill\" xmlns:nefx=\"http://www.cosmos.com.tw/nefx\" xmlns:cactx=\"http://www.cosmos.com.tw/cactx\" xmlns:bns=\"http://www.cosmos.com.tw/bns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("<SOAP-ENV:Header />");
		sb.append("<SOAP-ENV:Body>");
		sb.append("<esb:MsgRq>");
		sb.append("<Header>");
		sb.append("<ClientId>").append(clientID).append("</ClientId>");
		sb.append("<ClientPwd>").append(clientPAZZD).append("</ClientPwd>");
		sb.append("<ClientDt>").append(timeNow).append("</ClientDt>");
		sb.append("<TxnId>GMSCSMT24R2B</TxnId>");
		sb.append("<UUID>").append(clientID + DateUtil.GetDateFormatString("yyyyMMddHHmmssSSS")).append("</UUID>");
        sb.append("</Header>");
        sb.append("<SvcRq xsi:type=\"kgib:GMSCSMT24R2BSvcRqType\">");
		sb.append("<SI2401>").append("U").append("</SI2401>");
		sb.append("<SI2402>").append(type).append("</SI2402>");
		sb.append("<SI2403>").append(idno).append("</SI2403>");
		sb.append("<SI2404>").append("               ").append("</SI2404>");
		sb.append("<SI2405>").append("FFFFFFF1  ").append("</SI2405>");
        sb.append("<SI2406>").append(String.format("%07d", Integer.valueOf(creditModify))).append("</SI2406>");
		sb.append("<SI2407>").append(contractVer).append("</SI2407>");
		sb.append("</SvcRq>");
		sb.append("</esb:MsgRq>");
		sb.append("</SOAP-ENV:Body>");
		sb.append("</SOAP-ENV:Envelope>");

		logger.info(sb.toString());

        return sb.toString();
    }


    // QUERY
    public String xmlGMSCSMT24R2BV1Mk2(String idno) {
        CreateXML xFile = new CreateXML();
		String ClientID = gc.ESBClientId;
		String ClientPAZZD = gc.ESBClientPAZZD;
		String timeNow = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		ClientPAZZD = DigestUtils.md5Hex(ClientPAZZD + timeNow);

		Element Envelope = xFile.setRootTag("SOAP-ENV:Envelope");
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("xmlns:SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
		map.put("xmlns:esb", "http://www.cosmos.com.tw/esb");
		map.put("xmlns:bns","http://www.cosmos.com.tw/bns");
		map.put("xmlns:mix","http://www.cosmos.com.tw/mix"); 
		map.put("xmlns:cardpac","http://www.cosmos.com.tw/cardpac");
		map.put("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		xFile.setAttribute(Envelope, map);

		xFile.setChildTagNoTextNode(Envelope, "SOAP-ENV:Header");
		Element Body = xFile.setChildTagNoTextNode(Envelope, "SOAP-ENV:Body");
		Element esb = xFile.setChildTagNoTextNode(Body, "esb:MsgRq");
		Element Header = xFile.setChildTagNoTextNode(esb, "Header");

		xFile.setChildHasTextNode(Header, "ClientId", ClientID);
		xFile.setChildHasTextNode(Header, "ClientPwd", ClientPAZZD);
		xFile.setChildHasTextNode(Header, "ClientDt", timeNow);
		xFile.setChildHasTextNode(Header, "TxnId", "BNS08508100Q");
		xFile.setChildHasTextNode(Header, "UUID", ClientID + DateUtil.GetDateFormatString("yyyyMMddHHmmssSSS"));

		Element SvcRq = xFile.setChildTagNoTextNode(esb, "SvcRq");

		map.clear();
		map.put("xsi:type", "kgib:BNS08508100QSvcRqType");
		map.put("xmlns:kgib", "http://www.cosmos.com.tw/kgib");
		xFile.setAttribute(SvcRq, map);
		xFile.setChildHasTextNode(SvcRq, "", "I");
		xFile.setChildHasTextNode(SvcRq, "", "N");
		xFile.setChildHasTextNode(SvcRq, "", "D");
		String result = "";
		try {
			result = xFile.toXMLFile();
		} catch (TransformerException e) {
			logger.error("========<<< #SOAP 未知錯誤", e);
		}
		return result;
    }


    // UPDATE
    public String xmlGMSCSMT24R2BV2Mk2(String type, String idno, String currentModify) {
        return "";
    }


    protected GetOtpTelResDto getOtpTel(String uniqId, String idno, String birthday) throws Exception {

        GetOtpTelReqDto req_2 = new GetOtpTelReqDto(idno, birthday);
        Gson gson = new Gson();

        String rspString = apss.queryAPS("/KGI/GET_OTP_TEL", gson.toJson(req_2), uniqId);
        GetOtpTelResDto rtn = gson.fromJson(rspString, GetOtpTelResDto.class);
        return rtn;
    }


    // SECTOR: METHOD
    protected List<ASystemRes> transfer(List<GMSCSMT24R2Content> list) {
        List<ASystemRes> listStr = new ArrayList<>();
        for (GMSCSMT24R2Content content: list) {
            ASystemRes res = new ASystemRes();
            res.setFuncType(content.getSO2401());
            res.setUserType(content.getSO2402());
            res.setCaseType(content.getSO2403());
            res.setCustomerId(content.getSO2404());
            res.setManageStore(content.getSO2405());
            res.setCompanyNum(content.getSO2406());
            res.setDepartNum(content.getSO2407());
            res.setMemberNum(content.getSO2408());
            res.setSeq(content.getSO2409());
            res.setUserName(content.getSO2410());
            res.setCardNum(content.getSO2411());
            res.setProjectId(content.getSO2412());
            res.setCreditCur(content.getSO2413());
            res.setCreditContract(content.getSO2414());
            res.setCreditModify(content.getSO2415());
            res.setDateModify(content.getSO2416());
            res.setEmail(content.getSO2417());
            res.setPhone(content.getSO2418());
            res.setSO2419(content.getSO2419());
            listStr.add(res);
        }
        return listStr;
    }


}
