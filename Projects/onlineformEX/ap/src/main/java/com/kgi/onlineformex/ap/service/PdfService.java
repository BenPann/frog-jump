package com.kgi.onlineformex.ap.service;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.gson.Gson;
import com.kgi.onlineformex.ap.dao.FM_CaseDataDao;
import com.kgi.onlineformex.ap.dao.FM_CaseDocumentDao;
import com.kgi.onlineformex.ap.helper.PDFHelper;
import com.kgi.onlineformex.ap.pdf.ContractPDF;
import com.kgi.onlineformex.common.dto.customDto.ASystemRes;
import com.kgi.onlineformex.common.dto.db.FM_CaseData;
import com.kgi.onlineformex.common.dto.db.FM_CaseDocument;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class PdfService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ApplicationContext appContect;
    @Autowired
    private FM_CaseDataDao fmcdDao;
    @Autowired
    private FM_CaseDocumentDao fmcDocDao;

    @Value("${PROTOCOL.CONTENT.A.MK2}")
    public String PROTOCOLCONTENTAMK2;


    // SECTOR: CREATE
    /**
     *  CREATE AND MODIFY HTML WITH PARAMETER 
     */
    public String createAndPreviewPDFMk1(String uniqId) {
        try {
            logger.info("========>> Access the createAndPreviewPDF().... ");
            String previewContract = createContractAsHtml(uniqId);
            saveContractDoc(uniqId, previewContract);
            return previewContract;

        } catch (Exception e) {
            logger.error("========<<< #ERROR {}", e);
            return "FAILED";
        }
    }


    public String createAndPreviewHtmlMk1(String uniqId) {
        logger.info("========>> #SERVICE Access the createAndPreviewHtmlMk2().... ");
        try {
            FM_CaseData fmcd = fmcdDao.ReadByUniqId(uniqId);
            ASystemRes asRes = new Gson().fromJson(fmcd.getASystemContent(), ASystemRes.class);

            String
                idno         = fmcd.getIdno(),
                manageStore  = fmcd.getManageStore(),
                productName  = "PRODUCTNAME",
                accNum       = asRes.getCardNum(),
                creditCur    = Integer.valueOf(fmcd.getCreditCur()).toString(),
                creditModify = asRes.getCreditModify(),
                rawStr       = PROTOCOLCONTENTAMK2;

            DecimalFormat fmt = new DecimalFormat("##,###,###,###,##0"); 
            String 
                creditCurFormat = fmt.format(Double.parseDouble(creditCur)),
                creditModFormat = fmt.format(Double.parseDouble(creditModify));
            productName = manageStore.equals("8861") ? "速還金" : "靈活卡"  ;

			String[] arrMapKey = { "%idno%", "%product_name%", "%accout_number%", "%credit_current%", "%credit_modify%" };
			String[] arrMapVal = { idno, productName, accNum, creditCurFormat, creditModFormat };
			List<String> listForMapKey = Arrays.asList(arrMapKey);
			List<String> listForMapVal = Arrays.asList(arrMapVal);

			Map<String, String> mapMk2 = IntStream.range(0, listForMapKey.size())
            						  .boxed()
            						  .collect(Collectors.toMap(i -> listForMapKey.get(i), i -> listForMapVal.get(i)));

			for (Map.Entry<String, String> entry : mapMk2.entrySet()) {
				rawStr = rawStr.replace(entry.getKey(), entry.getValue());
            }
            logger.info("====----> @ " + rawStr);
		    return rawStr;

        } catch (Exception e) {
            logger.error("========<<< ERROR {}" + e);
            return null;
        }
    }



    // SECTOR: METHOD
    /**
     * 產生契約書PDF寄送EMAIL給客戶並儲存到DB
     *   CALLED BY SendRplMailHunterJob()
     */
    public void createContractPDF(String uniqId) throws Exception {
        logger.info("========>>> #SERVICE Access createContractPDF().... ");
        ContractPDF pdf = new ContractPDF(appContect);
        pdf.init(uniqId, "true");
        PDFHelper helper = new PDFHelper(pdf);
        helper.PdfProcess();
    }


    /** 產生HTML */
	private String createContractAsHtml(String uniqId) throws Exception {
        ContractPDF pdf = new ContractPDF(appContect);
		pdf.init(uniqId, "true");
        pdf.beforePdfProcess();

        String raw = pdf.getRawHtml();
        if (raw == null || raw.trim().equals("")) {
            throw new IOException("尚未傳入Html");
        }

        System.out.println("====###> ExportHtml");
        System.out.println("====###> raw.length()=" + raw.length());
        // Replace
        return pdf.replaceField(raw.trim());
    }



    /** 儲存預覽用契約書到DB */
    private int saveContractDoc(String uniqId, String previewcontract) throws Exception {
        // STEP: Update CaseDocument status to '03'
        FM_CaseDocument fmCaseDoc = new FM_CaseDocument();
        fmCaseDoc.setUniqId(uniqId);
        fmCaseDoc.setUniqType("03");
        fmCaseDoc.setDocumentType("contract.pdf");
        fmCaseDoc.setVersion("v1.0");
		fmCaseDoc.setContent(new byte[1]);
        fmCaseDoc.setOther(previewcontract);
        fmcDocDao.CreateOrUpdate(fmCaseDoc);
        return -1;
    }


}
