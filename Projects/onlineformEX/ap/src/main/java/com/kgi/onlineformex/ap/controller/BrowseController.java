package com.kgi.onlineformex.ap.controller;

import javax.servlet.http.HttpServletRequest;

import com.kgi.common.controller.BaseController;
import com.kgi.eopend3.common.dto.KGIHeader;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.onlineformex.ap.exception.ErrorResultException;
import com.kgi.onlineformex.ap.service.BrowseService;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/browse")
public class BrowseController extends BaseController {
    
    private static String context = "BrowseLogController";

    @Autowired
    private BrowseService bs;


    @PostMapping("/setPage")
    public String setPage(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("========> #CRTL Access the setPage().... ");
        logger.info("========> #CRTL 前台輸入的資料" + reqBody);
        try {
            KGIHeader header = this.getHeader(request);
            String uniqId = header.getUniqId();
            String uniqType = header.getUniqType();
            return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                        ? bs.setPage(reqBody, uniqId, uniqType)
                        : WebResult.GetFailResult();
        } catch (ErrorResultException e) {
            return WebResult.GetFailResult();
        }
    }

}
