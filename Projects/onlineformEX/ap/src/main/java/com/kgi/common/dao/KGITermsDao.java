package com.kgi.common.dao;

import java.util.List;

import com.kgi.eopend3.common.dto.customDto.KGITermsCustDto;
import com.kgi.eopend3.common.dto.db.KGITerms;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class KGITermsDao extends CRUDQDao<KGITerms> {

    @Override
    public int Create(KGITerms fullItem) {
        return 0;
    }

    @Override
    public KGITerms Read(KGITerms keyItem) {
       return null;
    }

    @Override
    public int Update(KGITerms fullItem) {
        return 0;
    }

    @Override
    public int Delete(KGITerms keyItem) {
        return 0;
    }

    @Override
    public List<KGITerms> Query(KGITerms keyItem) {
        return null;
    }

    public List<KGITermsCustDto> getTermsList(String page) {
        String sql = " select kgi.Name as termName ,substring(dd.DataName,charindex('-',dd.DataName)+1,len(dd.DataName)) as termTitle,kgi.Type " +
                     " from KGITerms kgi inner join DropdownData dd " +
                     " on kgi.Name = dd.DataKey and dd.Name = 'terms' "+
                     " where kgi.Page = ? order by kgi.Type, kgi.Sort ";
        return this.getJdbcTemplate().query(sql, new Object[] { page },
                    new BeanPropertyRowMapper<>(KGITermsCustDto.class));
    }
}