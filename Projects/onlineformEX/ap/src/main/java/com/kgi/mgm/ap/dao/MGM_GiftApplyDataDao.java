package com.kgi.mgm.ap.dao;

import java.util.List;

import com.kgi.common.config.DataSourceConfig;
import com.kgi.common.dao.CRUDQDao;
import com.kgi.mgm.common.dto.db.MGM_GiftApplyData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class MGM_GiftApplyDataDao extends CRUDQDao<MGM_GiftApplyData> {

    @Autowired
    private DataSourceConfig dsc;

    @Override
    public int Create(MGM_GiftApplyData fullItem) {

        return this.getNamedParameterJdbcTemplate(dsc.mainDataSource2())
                .update("INSERT INTO MGM_GiftApplyData (ApplyTime ,ER_ID ,GIFT_ID ,APPLY_AMT)"
                    + "VALUES (getDate(), :ER_ID, :GIFT_ID, :APPLY_AMT)"
                    , new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public MGM_GiftApplyData Read(MGM_GiftApplyData keyItem) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int Update(MGM_GiftApplyData fullItem) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int Delete(MGM_GiftApplyData keyItem) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public List<MGM_GiftApplyData> Query(MGM_GiftApplyData keyItem) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
