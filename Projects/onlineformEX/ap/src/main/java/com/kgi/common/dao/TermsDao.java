package com.kgi.common.dao;

import java.util.List;

import com.kgi.eopend3.common.dto.db.Terms;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;


@Repository
public class TermsDao extends CRUDQDao<Terms> {
	public static final String NAME = "Terms" ;

    @Override
    public int Create(Terms fullItem) {
        return 0;
    }

    @Override
    public Terms Read(Terms keyItem) {
        String sql = "SELECT Top 1 * FROM " + NAME + " WHERE Status = '03' AND AddedTime <= GETDATE() AND Name = ? " +
                "ORDER BY AddedTime DESC,ApproveTime DESC;";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(Terms.class),
                    new Object[] { keyItem.getName() });
        } catch (DataAccessException ex) {
			logger.error(NAME + "查無資料");
            return null;
        }
    }

    @Override
    public int Update(Terms fullItem) {
        return 0;
    }

    @Override
    public int Delete(Terms keyItem) {
        return 0;
    }

    @Override
    public List<Terms> Query(Terms keyItem) {
        return null;
    }
    
}