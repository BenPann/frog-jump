package com.kgi.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = { "file:${OFEX_CONFIG_PATH}/AP.properties" }, encoding = "UTF-8")
public class GlobalConfig {

    @Value("${upload.image.alpha}")
    public String ImageAlpha;
    // APS設定
    @Value("${APSServerHost}")
    public String APSServerHost;

    // 共銷同意註記
    @Value("${AutInfoUrl}")
    public String AutInfoUrl;
    // MailHunter設定
    @Value("${MailHunterFTPServerAddress}")
    public String MailHunterFTPServerAddress;
    @Value("${MailHunterFTPServerUserid}")
    public String MailHunterFTPServerUserid;

    @Value("#{new String(T(com.kgi.eopend3.common.util.crypt.FucoAESUtil).getInstance('${EncryptScretKey}','${EncryptIv}').decrypt('${MailHunterFTPServerPAZZD}'),'UTF-8')}")
    public String MailHunterFTPServerPAZZD;
    @Value("${MailHunterUrl}")
    public String MailHunterUrl;

    // 圖片浮水印字
    @Value("${IMG.WATER.FONT}")
    public String IMGWATERFONT;
    // OCR
    @Value("${OCRServerHost}")
    public String OCRServerHost;

    // NCCC驗證
    // @Value("${NCCCDomainName}")
    // public String NCCCDomainName;
    // @Value("${NCCCAuthUrl}")
    // public String NCCCAuthUrl;
    // @Value("${NCCCSecurityProvider}")
    // public String NCCCSecurityProvider;
    // @Value("${NCCCDebug}")
    // public boolean NCCCDebug;
    // @Value("${NCCCTransCode}")
    //public String NCCCTransCode;
    @Value("${NCCCMerchantID}")
    public String NCCCMerchantID;
    @Value("${NCCCTID}")
    public String NCCCTID;
    @Value("${NCCCTransMode}")
    public String NCCCTransMode;
    @Value("${NCCCTransAmount}")
    public String NCCCTransAmount;

    // ESB的設定
    @Value("${ESBClientId}")
    public String ESBClientId;

    @Value("#{new String(T(com.kgi.eopend3.common.util.crypt.FucoAESUtil).getInstance('${EncryptScretKey}','${EncryptIv}').decrypt('${ESBClientPAZZD}'),'UTF-8')}")
    public String ESBClientPAZZD;
    @Value("${ESBHostName}")
    public String ESBHostName;
    @Value("${ESBChannel}")
    public String ESBChannel;
    @Value("${ESBPort}")
    public String ESBPort;
    @Value("${ESBQueueManagerName}")
    public String ESBQueueManagerName;
    @Value("${ESBQueueSendName}")
    public String ESBQueueSendName;
    @Value("${ESBQueueReceiveName}")
    public String ESBQueueReceiveName;

    // PCode2566
    @Value("${PCODE2566TMNL_TYPE}")
    public String PCODE2566TMNL_TYPE;
    @Value("${PCODE2566TMNL_ID}")
    public String PCODE2566TMNL_ID;

    @Value("${SecuritiesHost}")
    public String SecuritiesHost;

    @Value("${PCode2566KGIErrorMsg}")
    public String PCode2566KGIErrorMsg;


    @Value("${EncryptScretKey}")
    public String EncryptScretKey;

    @Value("${EncryptIv}")
    public String EncryptIv;

    @Value("${PortalUrl}")
    public String protalUrl;

    @Value("${EopenImageServerHost}")
    public String EopenImageServerHost;

    @Value("${ValidationServerUrl}")
    public String validationServerUrl;

    @Value("${OTPOpID}")
    public String OTPOpID;
    @Value("${OTPBillDep}")
    public String OTPBillDep;

    @Value("${NcccTotalCount}")
    public String ncccTotalCount;
    @Value("${Pcode2566TotalCount}")
    public String pcode2566TotalCount;

    @Value("${BookingDay}")
    public String bookingDay;
    @Value("${BookingMonth}")
    public String bookingMonth;


    // KGI APIM
     @Value("${APIMApiKey}")
    public String apimApiKey ;
     @Value("${APIMSecret}")
    public String apimSecret ;
    @Value("${APIMServerHost}")
    public String apimServerHost;
    // @Value("${APIMClientCode}")
    // public String apimClientCode ;
    @Value("${APIMChinalifeCif}")
    public String apimChinalifeCif;
    @Value("${APIMChinalifeCaseSts}")
    public String apimChinalifeCaseSts;
    @Value("${ChinalifeCif}")
    public String chinalifeCif;
    @Value("${ChinalifeCaseSts}")
    public String chinalifeCaseSts;

    // 代收付平台
    /**系統代碼*/
    @Value("${PayerSysId}")
    public String payerSysId = "PAY" ;
    /**認證鑰匙*/
    @Value("${PayerApKey}")
    public String payerApKey = "f403034e08cc4e80b8cbf5418336e8b8" ;
    /**委託單位代號*/
    @Value("${PayerApplyCode}")
    public String payerApplyCode = "ACH02" ;
    /**轉帳類型代碼*/
    @Value("${PayerTransType}")
    public String payerTransType = "704" ;
    /**發動者統編*/
    @Value("${PayerLaunchUniformId}")
    public String payerLaunchUniformId = "03434016" ;
    /**動作類型, A表示新增、M表示修改、D表示刪除、R表示註銷後新增*/
    @Value("${PayerActionType}")
	public String payerActionType = "A" ;
    /**委託金融機構代號*/
    @Value("${PayerTransBankCode}")
	public String payerTransBankCode = "8090407" ;
    /**授權繳費帳號類別 (1：存款帳號、2：信用卡、3：本行支票、4：靈活卡)*/
    @Value("${PayerAccountType}")
	public String payerAccountType = "1" ;


    // KGI AUM 徵審
    //  @Value("${AUMHost}")
    public String aumHost = "http://172.18.12.18:6877/" ;
    //  @Value("${AUMCmd_AddCLAUMInfo}")
    public String aumCmd_AddCLAUMInfo = "KGI/ADD_CL_AUMINFO";

    @Value("${BreakPointRemainDays}")
    public String breakPointRemainDays;
  
    @Value("${FAKE_EMAIL}")
    public String FAKE_EMAIL;

    @Value("${pdfServerUrl}")
    public String pdfServerUrl;

    @Value("${FONT_WATER_TTF}")
    public String FONT_WATER_TTF;


    // ITEM: makePazzd set
    @Value("${AbbeyRye}")
    public String AbbeyRye; 
    @Value("${RayakRoe}")
    public String RayakRoe;
    @Value("${StaticFilePath}")
    public String StaticFilePath;
    @Value("${PdfDirectoryPath}")
    public String PdfDirectoryPath;


    @Value("${FileOfRplContract}")
    public String FileOfRplContract;
    @Value("${FileOfCreditCardContract}")
    public String FileOfCreditCardContract;

    @Value("${MgmSummaryServerUrl}")
    public String MgmSummaryServerUrl;

}