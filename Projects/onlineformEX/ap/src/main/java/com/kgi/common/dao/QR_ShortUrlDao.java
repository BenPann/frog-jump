package com.kgi.common.dao;

import java.util.List;

import com.kgi.common.config.DataSourceConfig;
import com.kgi.eopend3.common.dto.db.QR_ShortUrl;
import com.kgi.onlineformex.ap.exception.ErrorResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class QR_ShortUrlDao  extends CRUDQDao<QR_ShortUrl> {

	@Autowired
	private DataSourceConfig dsc;

	public static final String NAME = "QR_ShortUrl";

	@Override
	public int Create(QR_ShortUrl fullItem) {
		String sql = "INSERT INTO " + NAME + " ( ShortUrl, ChannelId, DepartId, Member, PProductType, PProductId, Entry, Count, CreateTime, UpdateTime) " +
			"VALUES ( :ShortUrl, :ChannelId, :DepartId, :Member, :PProductType, :PProductId, :Entry, :Count, getDate(), getDate())";
		try {
			return this.getNamedParameterJdbcTemplate(dsc.mainDataSource2()).update(sql, new BeanPropertySqlParameterSource(fullItem));
		} catch (Exception e) {
			throw new ErrorResultException(99, NAME + " 尚未處理之錯誤", e);
		}
	}

	@Override
	public QR_ShortUrl Read(QR_ShortUrl fullItem) {
		String sql = "select * FROM QR_ShortUrl where shortUrl=? ";
        try {
            return this.getJdbcTemplate(dsc.mainDataSource2()).queryForObject(sql, new BeanPropertyRowMapper<>(QR_ShortUrl.class),
                    new Object[] { fullItem.getShortUrl()});
        } catch (DataAccessException ex) {
//            System.out.println("QR_ShortUrl查無資料");
            return null;
        }
	}

	@Override
	public int Update(QR_ShortUrl fullItem) {
		return 0;
	}

	@Override
	public int Delete(QR_ShortUrl keyItem) {
		return 0;
	}

	@Override
	public List<QR_ShortUrl> Query(QR_ShortUrl keyItem) {
		return null;
	}

	public List<QR_ShortUrl> getByChannelId(String channelId) {
		String sql = " select * From QR_ShortUrl where ChannelId = ? ORDER BY CreateTime DESC ";
		try {
			return this.getJdbcTemplate(dsc.mainDataSource2()).query(sql, new BeanPropertyRowMapper<>(QR_ShortUrl.class),new Object[] { channelId });
		}catch(DataAccessException ex) {
			return null;
		}
	}
}
