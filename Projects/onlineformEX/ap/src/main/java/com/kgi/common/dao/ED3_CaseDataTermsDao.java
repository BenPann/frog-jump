package com.kgi.common.dao;

import java.util.List;

import com.kgi.eopend3.common.dto.db.ED3_CaseDataTerms;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class ED3_CaseDataTermsDao extends CRUDQDao<ED3_CaseDataTerms> {
	public static final String NAME = "ED3_CaseDataTerms" ;

    @Override
    public int Create(ED3_CaseDataTerms fullItem) {
        String sql = "INSERT INTO " + NAME + " (UniqId,TermSerial,IsAgree,CreateTime) VALUES (?,?,?,getDate()) ";
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getUniqId(),fullItem.getTermSerial(),fullItem.getIsAgree() });
    }

    @Override
    public ED3_CaseDataTerms Read(ED3_CaseDataTerms keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE UniqId = ? and TermSerial = ? ";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(ED3_CaseDataTerms.class),
                    new Object[] { keyItem.getUniqId(),keyItem.getTermSerial() });
        } catch (DataAccessException ex) {
			logger.error(NAME + "查無資料");
            return null;
        }
    }

    @Override
    public int Update(ED3_CaseDataTerms fullItem) {
        String sql = "Update " + NAME + " Set IsAgree = ? where UniqId = ? and TermSerial = ?  ";
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getIsAgree(),fullItem.getUniqId(),fullItem.getTermSerial() });
    }

    @Override
    public int Delete(ED3_CaseDataTerms keyItem) {
        return 0;
    }

    public ED3_CaseDataTerms getCaseDataTermsByTermsName(String uniqId, String termsName) {
        String sql = "SELECT E.* FROM " + NAME + " E inner join " +  TermsDao.NAME + " T on E.TermSerial=T.Serial "
        			+ " WHERE E.UniqId = ? and T.Name = ? and T.Status='03' ";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(ED3_CaseDataTerms.class),
                    new Object[] { uniqId, termsName });
        } catch (DataAccessException ex) {
			logger.error(NAME + "查無資料");
            return null;
        }
    }

    @Override
    public List<ED3_CaseDataTerms> Query(ED3_CaseDataTerms keyItem) {
        return null;
    }

}