package com.kgi.onlineformex.ap.dao;

import java.util.List;

import com.kgi.common.dao.CRUDQDao;
import com.kgi.onlineformex.common.dto.db.FM_ApiLog;

import org.springframework.stereotype.Repository;

@Repository
public class FM_ApiLogDao extends CRUDQDao<FM_ApiLog> {

    @Override
    public int Create(FM_ApiLog fullItem) {
        return this.getJdbcTemplate().update(
                        "INSERT INTO FM_ApiLog (UniqId, FlowPath, ApiName, Request, Response, StartTime, EndTime, UTime) VALUES "
                            + "( ?, ?, ?, ?, ?, ?, ?, GETDATE())",
                        new Object[] { 
                                fullItem.getUniqId(), 
                                fullItem.getFlowPath(), 
                                fullItem.getApiName(),
                                fullItem.getRequest(), 
                                fullItem.getResponse(), 
                                fullItem.getStartTime(), 
                                fullItem.getEndTime() });
    }


    public int Create(String type, String keyInfo, String apiName, String req, String rep, String startTime, String endTime) {
        FM_ApiLog dto = FM_ApiLog.builder().UniqId(type).FlowPath(keyInfo).ApiName(apiName).Request(req).Response(rep).StartTime(startTime).EndTime(endTime).build();
        return this.Create(dto);
    }


    @Override
    public FM_ApiLog Read(FM_ApiLog keyItem) {
        return null;
    }

    @Override
    public int Update(FM_ApiLog fullItem) {
        return 0;
    }

    @Override
    public int Delete(FM_ApiLog keyItem) {
        return 0;
    }

    @Override
    public List<FM_ApiLog> Query(FM_ApiLog keyItem) {
        return null;
    }

	
}
