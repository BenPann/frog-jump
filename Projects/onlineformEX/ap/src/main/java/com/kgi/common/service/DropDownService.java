package com.kgi.common.service;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.kgi.common.dao.DropdownDao;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.db.Dropdown;
import com.kgi.eopend3.common.dto.response.DropDownResp;
import com.kgi.eopend3.common.dto.view.DropDownView;
import com.kgi.eopend3.common.util.CheckUtil;
import com.kgi.onlineformex.ap.exception.ErrorResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DropDownService {

    @Autowired
    private DropdownDao dropdownDao;
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	public String getDropDownData(String reqJson) {
        try {
            Gson gson = new Gson();
            DropDownView dropDownView = gson.fromJson(reqJson, DropDownView.class);
            if (!CheckUtil.check(dropDownView)) {
                throw new ErrorResultException(1, "參數錯誤", null);
        	}else{
                List<DropDownResp> respList = new ArrayList<DropDownResp>();    
                Dropdown dropDown = new Dropdown();
                dropDown.setName(dropDownView.getName());
                List<Dropdown> list = dropdownDao.Query(dropDown);
                if(list!=null&&list.size()>0){                    
                    for(Dropdown result : list){
                        DropDownResp resp = new DropDownResp();
                        resp.setDataKey(result.getDataKey());
                        resp.setDataName(result.getDataName());
                        respList.add(resp);
                    }
                    return WebResult.GetResultString(0,"成功",respList);
                } else{
                    throw new ErrorResultException(2,"查無資料",null);
                }               
            }			
    	} catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
	}
    
}
