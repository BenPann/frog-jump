package com.kgi.onlineformex.ap.helper;

public class NumConvertCN {
    private String integerPart;
    private String floatPart;

    private static final char[] cnNumbers = { '零', '壹', '貳', '叄', '肆', '伍', '陸', '柒', '捌', '玖' };

    private static final char[] series = { ' ', '拾', '佰', '仟', '萬', '拾', '佰', '仟', '億' };

    /**
     * 構造函數,通過它將阿拉伯數字形式的字符串傳入
     * 
     * @param original
     */
    public NumConvertCN(String original) {
        // 成員變量初始化
        integerPart = "";
        floatPart = "";

        if (original.contains(".")) {
            // 如果包含小數點
            int dotIndex = original.indexOf(".");
            integerPart = original.substring(0, dotIndex);
            floatPart = original.substring(dotIndex + 1);
        } else {
            // 不包含小數點
            integerPart = original;
        }
    }

    /**
     * 取得大寫形式的字符串
     * 
     * @return
     */
    public String getCnString() {
        // 因為是累加所以用StringBuffer
        StringBuffer sb = new StringBuffer();

        // 整數部分處理
        for (int i = 0; i < integerPart.length(); i++) {
            int number = getNumber(integerPart.charAt(i));

            sb.append(cnNumbers[number]);
            sb.append(series[integerPart.length() - 1 - i]);
        }

        // 小數部分處理
        if (floatPart.length() > 0) {
            sb.append("點");
            for (int i = 0; i < floatPart.length(); i++) {
                int number = getNumber(floatPart.charAt(i));

                sb.append(cnNumbers[number]);
            }
        }

        // 返回拼接好的字符串
        return sb.toString();
    }

    /**
     * 將字符形式的數字轉化為整形數字 因為所有實例都要用到所以用靜態修飾
     * 
     * @param c
     * @return
     */
    private static int getNumber(char c) {
        String str = String.valueOf(c);
        return Integer.parseInt(str);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(new NumConvertCN("123456789.12345").getCnString());
        System.out.println(new NumConvertCN("123456789").getCnString());
        System.out.println(new NumConvertCN("100000000.12345").getCnString());
        System.out.println(new NumConvertCN("0.1234").getCnString());
    }
}
