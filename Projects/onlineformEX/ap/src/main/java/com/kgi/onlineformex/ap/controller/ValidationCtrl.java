package com.kgi.onlineformex.ap.controller;

import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;
import com.kgi.common.controller.BaseController;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.dto.KGIHeader;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.response.CheckOTPResp;
import com.kgi.eopend3.common.dto.response.SendOTPResp;
import com.kgi.eopend3.common.dto.view.CheckOTPView;
import com.kgi.eopend3.common.dto.view.SendOTPView;
import com.kgi.eopend3.common.util.CheckUtil;
import com.kgi.onlineformex.ap.exception.ErrorResultException;
import com.kgi.onlineformex.ap.service.ValidationService;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/validate")
public class ValidationCtrl extends BaseController {

    @Autowired
    ValidationService vs;

    // SECTOR: OTP place
    /**
     * 
     * @param request
     * @param reqBody
     * @return
     */
    @PostMapping("/sendOTP")
    public String sendOTP(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("========> #CRTL Access the sendOTP().... request: {}, reqBody: {}", request, reqBody);

        try {
            KGIHeader header = this.getHeader(request);
            String uniqId = header.getUniqId();
            String idno = header.getIdno();

            logger.info("### sendOTP header ==> " + header);
            logger.info("### sendOTP uniqId ==> " + uniqId);
            logger.info("### sendOTP idno ==> " + idno);

            if (!ESAPI.validator().isValidInput("onlineform", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
                return WebResult.GetFailResult();
            }

            logger.info("========> #Validation Access {}", reqBody);

            Gson gson = new Gson();
            SendOTPView view = gson.fromJson(reqBody, SendOTPView.class);

            logger.info("### sendOTP view.getIdno ==> " + view.getIdno());
            logger.info("### sendOTP view.getOpId ==> " + view.getOpId());
            logger.info("### sendOTP view.getBillDep ==> " + view.getBillDep());
            logger.info("### sendOTP view.getPhone ==> " + view.getPhone());
            logger.info("### sendOTP view.getTxnId ==> " + view.getTxnId());

            if (!CheckUtil.check(view)) {
                return WebResult.GetResultString(1, "參數錯誤", null);
            }

            // HACK: return result
            SendOTPResp resp = vs.getSessionKey(view, uniqId, idno);

            return !SystemConst.KGI_OTP_SUCESS.equals(resp.getCode())
                    ? WebResult.GetResultString(-1, resp.getMessage(), null)
                    : WebResult.GetResultString(0, "成功", resp);

        } catch (IntrusionException e1) {
            return WebResult.GetResultString(9, "Invalid Input", null);
        } catch (ErrorResultException er) {
            return WebResult.GetResultString(er.getStatus(), er.getMessage(), er.getResult());
        } catch (Exception e) {
            logger.error("<======== #CRTL ERROR 未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        } finally {
            logger.info("<======== #CRTL terminate the sendOTP()....");
        }
    }

    /**
     * 
     * @param request
     * @param reqBody
     * @return
     */
    @PostMapping("/checkOTP")
    public String checkOTP(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("========> #CRTL Access the checkOTP().... " + reqBody);
        Gson gson = new Gson();

        try {
            if (!ESAPI.validator().isValidInput("onlineform", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
                return WebResult.GetFailResult();
            }

            CheckOTPView view = gson.fromJson(reqBody, CheckOTPView.class);
            logger.info("========> #CRTL Variable get {}", gson.toJson(view));

            if (!CheckUtil.check(view)) {
                return WebResult.GetResultString(1, "參數錯誤", null);
            }

            KGIHeader header = this.getHeader(request);
            String uniqId = header.getUniqId();
            String idno = header.getIdno();
            String ipAddress = header.getIpAddress();

            CheckOTPResp resp = vs.checkSessionKeyOtp(view, uniqId, idno, ipAddress);
            return WebResult.GetResultString(0, "成功", resp);

        } catch (IntrusionException e) {
            return WebResult.GetResultString(9, "Invalid Input", null);
        } catch (ErrorResultException er) {
            return WebResult.GetResultString(er.getStatus(), er.getMessage(), er.getResult());
        } catch (Exception e) {
            logger.error("<======== #Crtl ERROR 未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        } finally {
            logger.info("<======== #Crtl terminate the checkOtp()....");
        }
    }

    /**
     * 取得驗身總次數
     * 
     * @param request
     * @return
     */
    @GetMapping("/getVerifyCount")
    public String getVerifyCount(HttpServletRequest request) {
        logger.info("========> getVerifyCount()");
        try {
            KGIHeader header = this.getHeader(request);
            String ulid = header.getUniqId();
            return vs.getVerifyCount(ulid);
        } catch (IntrusionException e) {
            return WebResult.GetResultString(9, "Invalid Input", null);
        } catch (ErrorResultException er) {
            return WebResult.GetResultString(er.getStatus(), er.getMessage(), er.getResult());
        } finally {
            logger.info("<-- getVerifyCount()");
        }
    }

    @PostMapping("/isEmailSuccess")
    public String isEmailSuccess(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("========> #CTRL 前台輸入的資料" + reqBody);
        try {
            KGIHeader header = this.getHeader(request);
            String uniqId = header.getUniqId(), idno = header.getIdno(), ipAddress = header.getIpAddress();
            return vs.isEmailSuccess(reqBody, uniqId, idno, ipAddress);
        } catch (ErrorResultException e) {
            return WebResult.GetFailResult();
        }
    }

    /**
     * 
     * @param request
     * @param reqBody
     * @return
     */
    @PostMapping("/sendOTPNoLogin")
    public String sendOTPNoLogin(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("========> #CRTL Access the sendOTPNoLogin().... request: {}, reqBody: {}", request, reqBody);

        try {
            if (!ESAPI.validator().isValidInput("onlineform", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
                return WebResult.GetFailResult();
            }

            logger.info("========> #Validation Access {}", reqBody);

            Gson gson = new Gson();
            SendOTPView view = gson.fromJson(reqBody, SendOTPView.class);

            if (!CheckUtil.check(view)) {
                return WebResult.GetResultString(1, "參數錯誤", null);
            }

            KGIHeader header = this.getHeader(request);
            String uniqId = "";
            String idno = "";
            if (header != null) {
                uniqId = header.getUniqId();
                idno = header.getIdno();
            } else {
                uniqId = "41234242134424123";
                idno = "A123456789";
            }

            // HACK: return result
            SendOTPResp resp = vs.getSessionKey(view, uniqId, idno);

            return !SystemConst.KGI_OTP_SUCESS.equals(resp.getCode())
                    ? WebResult.GetResultString(-1, resp.getMessage(), null)
                    : WebResult.GetResultString(0, "成功", resp);

        } catch (IntrusionException e1) {
            return WebResult.GetResultString(9, "Invalid Input", null);
        } catch (ErrorResultException er) {
            return WebResult.GetResultString(er.getStatus(), er.getMessage(), er.getResult());
        } catch (Exception e) {
            logger.error("<======== #CRTL ERROR 未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        } finally {
            logger.info("<======== #CRTL terminate the sendOTP()....");
        }
    }

    /**
     * 
     * @param request
     * @param reqBody
     * @return
     */
    @PostMapping("/checkOTPNoLogin")
    public String checkOTPNoLogin(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("========> #CRTL Access the checkOTPNoLogin().... " + reqBody);
        Gson gson = new Gson();

        try {
            if (!ESAPI.validator().isValidInput("onlineform", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
                return WebResult.GetFailResult();
            }

            CheckOTPView view = gson.fromJson(reqBody, CheckOTPView.class);
            logger.info("========> #CRTL Variable get {}", gson.toJson(view));

            KGIHeader header = this.getHeader(request);
            String uniqId = "";
            String idno = "";
            String ipAddress = "";
            if (header != null) {
                uniqId = header.getUniqId();
                idno = header.getIdno();
                ipAddress = header.getIpAddress();
            } else {
                uniqId = "AIRLOAN202012305555";
                idno = "A123456789";
                ipAddress = "172.0.0.1";
            }

            CheckOTPResp resp = vs.checkSessionKeyOtp(view, uniqId, idno, ipAddress);
            return WebResult.GetResultString(0, "成功", resp);

        } catch (IntrusionException e) {
            return WebResult.GetResultString(9, "Invalid Input", null);
        } catch (ErrorResultException er) {
            return WebResult.GetResultString(er.getStatus(), er.getMessage(), er.getResult());
        } catch (Exception e) {
            logger.error("<======== #Crtl ERROR 未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        } finally {
            logger.info("<======== #Crtl terminate the checkOtp()....");
        }
    }

}