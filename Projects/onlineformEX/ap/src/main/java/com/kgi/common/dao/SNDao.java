package com.kgi.common.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class SNDao extends BaseDao  {

	private static final Logger logger = LoggerFactory.getLogger(SNDao.class.getName());

	@Autowired
	@Qualifier("mainDB")
	private DataSource dataSource;

	public int getSerialNumber(String group, String key) {

		logger.info("========> Access the getSerialNumber()....");

		String procedureCall = "{? = call sp_gen_serialnumber(?, ?)}";
		Connection connection = null;
		int returnVal = -1;
		try {

			connection = dataSource.getConnection();
			CallableStatement callableSt = connection.prepareCall(procedureCall);
			callableSt.registerOutParameter(1, Types.INTEGER);
			callableSt.setString(2, group);
			callableSt.setString(3, key);

			logger.info("========> Create the serial number finished....");

			// Call Stored Procedure
			callableSt.executeUpdate();
			returnVal = callableSt.getInt(1);

		} catch (SQLException e) {
			logger.error("getSerialNumber", e);
		} finally {
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error("getSerialNumber", e);
				}
		}
		return returnVal;
	}
}