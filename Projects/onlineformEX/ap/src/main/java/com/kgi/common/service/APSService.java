package com.kgi.common.service;

import com.kgi.common.config.GlobalConfig;
import com.kgi.common.dao.HttpDao;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.util.DateUtil;
import com.kgi.onlineformex.ap.dao.FM_ApiLogDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class APSService {

    @Autowired
    GlobalConfig globalConfig;

    @Autowired
    HttpDao httpDao;

    @Autowired
    private FM_ApiLogDao fmalDao;
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());  

    public String queryAPS(String qryName, String queryJson,String keyInfo) throws Exception {
        String apsUrl = globalConfig.APSServerHost;
        return queryAPS(apsUrl, qryName, queryJson,keyInfo);
    }

    public String queryAPS(String apsUrl, String qryName, String queryJson,String keyInfo) throws Exception {
        String 
            response = "",
            startTime = "",
            endTime = "";
        try {
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            response  = this.httpDao.queryAPI(apsUrl, qryName, queryJson);
            endTime   = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
        } catch(Exception e){
            logger.error("========<< 未知錯誤", e);
            throw new Exception();
        } finally {
            fmalDao.Create(SystemConst.APILOG_TYPE_CALLOUT, keyInfo, qryName, queryJson, response, startTime, endTime);
        }
        return response;
    }


    public String queryOCR(String qryName, String queryJson,String uniqId) throws Exception {
        String 
            apiurl = globalConfig.OCRServerHost.concat(qryName),
            startTime = "",
            endTime = "",
            rep = "";
        try {
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            logger.info("========>> #SERVICE APS {} Start At {}", qryName, startTime);
            rep     = httpDao.doJsonPostRequest(apiurl, queryJson);
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            logger.info("========>> #SERVICE APS {} End At {}, RESPONSE: {}", qryName, endTime, rep);
            return rep;
        } catch (Exception e) {
            throw new Exception();
        } finally {
            fmalDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, qryName, queryJson, rep, startTime, endTime);
        }
    }

}
