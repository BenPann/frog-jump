package com.kgi.mgm.ap.controller;

import javax.servlet.http.HttpServletRequest;

import com.kgi.common.controller.BaseController;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.mgm.ap.mockData.mockMgmData;
import com.kgi.mgm.ap.services.MgmService;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/mgm")
public class MgmController extends BaseController {

    @Autowired
    private MgmService mgms;
    @Autowired
    private mockMgmData mgmms;

    private static final String context = "MgmController";


    @PostMapping("/getActivity")
    public String getActivityList(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                    ? mgms.getActivityListByMemberId(reqBody)
                    : WebResult.GetFailResult();
    }

    @PostMapping("/checkMemberId")
    public String checkMemberId(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
            ? mgms.checkByCustomerId(reqBody)
            : WebResult.GetFailResult();
    }
    
    @PostMapping("/checkMemberDetail")
    public String checkMemberDetail(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                    ? mgms.checkMemberDetailByCustomerId(reqBody)
                    : WebResult.GetFailResult();
    }


    @PostMapping("/getConfig")
    public String getConfig(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                    ? mgms.getConfigByKeyName(reqBody)
                    : WebResult.GetFailResult();
    }


    @PostMapping("/getContent")
    public String getActivityContent(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                    ? mgms.getActivityContentByActUniqIdMk2(reqBody)
                    : WebResult.GetFailResult();
    }


    @PostMapping("/getShortUrl")
    public String getActivityShortUrl(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                    ? mgms.getShortUrlByCustomerId(reqBody)
                    : WebResult.GetFailResult();
    }


    @PostMapping("/resetPassword")
    public String resetPassword(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                    ? mgms.resetPasswordProcess(reqBody)
                    : WebResult.GetFailResult();
    }


    @PostMapping("/forgotPassword")
    public String forgotPassword(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                    ? mgms.forgotPasswordLoginCheck(reqBody)
                    : WebResult.GetFailResult();
    }


    @PostMapping("/forgotPasswordReset")
    public String forgotPasswordReset(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                    ? mgms.forgotPasswordResetPassword(reqBody)
                    : WebResult.GetFailResult();
    }


    // SECTOR: MOCK
    @PostMapping("/mockApiTest")
    public String mockApiTest(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        try {
            return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                ? mgmms.mockApiTestMethod(reqBody)
                : WebResult.GetFailResult();
        } catch (Exception e) {
            return WebResult.GetFailResult();
        }
    }


    @PostMapping("/delectIdPass")
    public String delectCustomerPass(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        try {
            return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                ? mgmms.delectCustomerPasswordForTest(reqBody)
                : WebResult.GetFailResult();
        } catch (Exception e) {
            return WebResult.GetFailResult();
        }
    }


    @GetMapping("/mockMgmApi")
    public String mockMGMServerResponse() {
        logger.info("========> Access the mockMGMServerResponse().... ");
        String res = mgmms.arrayFormForDemo();
        return res;
    }
    
    @PostMapping("/getSummaryCase")
    public String getSummaryCase(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                    ? mgms.getSummaryCaseByCustId(reqBody)
                    : WebResult.GetFailResult();
    }
    
    @PostMapping("/getSummaryBonus")
    public String getSummaryBonus(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                    ? mgms.getSummaryBonusByCustId(reqBody)
                    : WebResult.GetFailResult();
    }
    
    @PostMapping("/getGiftList")
    public String getGiftList(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                    ? mgms.getGiftList(reqBody)
                    : WebResult.GetFailResult();
    }
    
    @PostMapping("/applyGift")
    public String applyGift(HttpServletRequest req, @RequestBody String reqBody) {
        logger.info("========> 前台輸入的資料 req: {}", req);
        return ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)
                    ? mgms.applyGift(reqBody)
                    : WebResult.GetFailResult();
    }


}
