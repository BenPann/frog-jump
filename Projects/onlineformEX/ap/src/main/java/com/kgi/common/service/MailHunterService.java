package com.kgi.common.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPMessage;

import com.kgi.common.config.GlobalConfig;
import com.kgi.common.dao.ConfigDao;
import com.kgi.common.dao.HttpDao;
import com.kgi.common.dao.MailHistoryDao;
import com.kgi.common.dao.MailHunterHistoryDao;
import com.kgi.common.dao.MailTemplateDao;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.customDto.MailHunterReq;
import com.kgi.eopend3.common.dto.customDto.MailHunterRsp;
import com.kgi.eopend3.common.dto.db.MailHistory;
import com.kgi.eopend3.common.dto.db.MailHunterHistory;
import com.kgi.eopend3.common.dto.db.MailTemplate;
import com.kgi.eopend3.common.util.DateUtil;
import com.kgi.onlineformex.ap.dao.FM_ApiLogDao;
import com.kgi.onlineformex.ap.dao.FTPDao;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.NodeList;

import net.sf.json.JSONObject;

@Service
public class MailHunterService {


    @Autowired
    HttpDao httpDao;
    @Autowired
    GlobalConfig globalConfig;
    @Autowired
    FTPDao ftpDao;
    @Autowired
    private ConfigDao configDao;
    @Autowired
    private MailHistoryDao mailHistoryDao;
    @Autowired
    private MailTemplateDao mailTemplateDao;
    @Autowired
    private MailHunterHistoryDao mailHunterHistoryDao;
    @Autowired
    private FM_ApiLogDao fmalDao;

    // PARAMETER
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String MAILTYPE = "1";
    private final static QName ServiceBody_QNAME = new QName("http://tempuri.org/", "SendToOneAttResponse");


  	public String sendMailHunter(MailHunterReq mailhunterReq) {
        String 
            startTime = "",
            endTime = "",
            req = "",
            rsp = "";
        MailHunterRsp mailHunterRsp = new MailHunterRsp();
        Map<String,Object> map = null;

        try {	               		
            StringBuilder sb = new StringBuilder();
            sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            sb.append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            sb.append("<soap:Body>");
            sb.append("<SendToOneAtt xmlns=\"http://tempuri.org/\">");
            sb.append("<project_category_code>"+mailhunterReq.getProjectCode()+"</project_category_code>");
            sb.append("<toname>"+mailhunterReq.getChtName()+"</toname>");
            if (SystemConst.isTEST) {
            	sb.append("<toemail>"+ globalConfig.FAKE_EMAIL +"</toemail>");
            } else {
            	sb.append("<toemail>"+mailhunterReq.getEMailAddress()+"</toemail>");
            }
            sb.append("<cust_id>"+mailhunterReq.getIdno()+"</cust_id>");
            sb.append("<template_id>"+mailhunterReq.getTemplateId()+"</template_id>");//公版的
            sb.append("<att_path>"+mailhunterReq.getFilename()+"</att_path>");//放附件檔名
            sb.append("<owner_id>"+mailhunterReq.getOwnerID()+"</owner_id>");
            sb.append("<product_category_code>"+mailhunterReq.getProductCode()+"</product_category_code>");
            sb.append("<reserve_columns>");
            String[] titleAndContents = mailhunterReq.getTitleAndContents();
            int emptyStart = 0; 
            if(titleAndContents!=null){
                emptyStart = titleAndContents.length ;
                for(int i = 0; i < titleAndContents.length ;i++){
                    sb.append("<string>"+StringEscapeUtils.escapeHtml(titleAndContents[i])+"</string>");
                }                
            }
            for(int i =  emptyStart; i<20; i++){                    
                sb.append("<string></string>");                
            }
            sb.append("</reserve_columns>");
            sb.append("</SendToOneAtt>");
            sb.append("</soap:Body>");
            sb.append("</soap:Envelope>");
            String esbUrl = globalConfig.MailHunterUrl; // "http://172.31.1.182/mailhunter_api/SendNow.asmx?wsdl";
            req = sb.toString();
            mailHunterRsp.setReq(req);
            
            // CALL
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            rsp       = this.httpDao.doSoapPostRequest(esbUrl, sb.toString());
            endTime   = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");

            map = parseMailHunterSoap(rsp);
            logger.info("========<< {}", map);
            mailHunterRsp.setRsp(rsp);
            return WebResult.GetSuccessResult(mailHunterRsp);
    	}catch (Exception e) {
            logger.error("未知錯誤", e);
            return WebResult.GetSuccessResult(mailHunterRsp);
        } finally {
            fmalDao.Create(SystemConst.APILOG_TYPE_CALLOUT, "uniqId", "/mailhunter", req, rsp, startTime, endTime);
        }
    }
    

    public Map<String,Object> parseMailHunterSoap(String soapStr) throws Exception {
        Map<String,Object> map = new HashMap<String,Object>();
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(soapStr.getBytes("UTF-8"))) {
            SOAPMessage response = MessageFactory.newInstance(SOAPConstants.DEFAULT_SOAP_PROTOCOL).createMessage(null,
                    byteArrayInputStream);
            SOAPBody soapBody = response.getSOAPBody();
            Iterator<?> itServiceBody = soapBody.getChildElements(ServiceBody_QNAME);
            if (itServiceBody.hasNext()) {
                SOAPElement emtServiceBody = (SOAPBodyElement) itServiceBody.next();
                NodeList nodelist = (NodeList) emtServiceBody.getChildNodes();
                Node sendToOneAttResult = getNode("SendToOneAttResult", nodelist);                
                if (sendToOneAttResult != null) {
                    map.put("result", sendToOneAttResult.getValue());
                    Node errorMsg = getNode("error_msg", nodelist);  
                    map.put("errorMsg", errorMsg.getValue());
                    return map;
                } else{                    
                    throw new Exception();
                }
            } else {                    
                throw new Exception();
            }
            
        } catch (Exception e) {
          throw e;
        }
    }


    private static Node getNode(String nodeName, NodeList nodelist) {
        Node result = null;

        for (int i = 0; i < nodelist.getLength(); i++) {
            // return tag
            Node childNode = (Node) nodelist.item(i);
            if (nodeName.equalsIgnoreCase(childNode.getNodeName())) {
                result = childNode;
                break;
            }
        }
        return result;
    }


    public boolean uploadPdfTemplate(String fileName , String uniqId, byte[] file) throws Exception {
        InputStream is = ftpDao.fileToInputStream(file);
        if(is!=null){
            //給個檔名上傳到ftp
            return ftpDao.upload(
                globalConfig.MailHunterFTPServerAddress,
                21,
                globalConfig.MailHunterFTPServerUserid,
                globalConfig.MailHunterFTPServerPAZZD,
                "File",
                fileName,
                is
            );
        }else{
            throw new Exception("PDF有誤");
        }

    }


    public boolean uploadPdfTemplate(String fileName , String uniqId, InputStream is) throws Exception {
  	    if (is!=null) {
            //給個檔名上傳到ftp
            return ftpDao.upload(globalConfig.MailHunterFTPServerAddress, 21, globalConfig.MailHunterFTPServerUserid, globalConfig.MailHunterFTPServerPAZZD, "File", fileName, is);
        } else {
            throw new Exception("PDF有誤");
        }

    }


    public void createMailHunter(String uniqId,String chtName,String idno,String email,String type,byte[] attatchment,String title,String content){
        /*MailTemplate mailTitle = new MailTemplate(type);
        mailTitle.setTagName("resv1");
        mailTitle = mailTemplateDao.Read(mailTitle);
        /*List<MailTemplate> mailTemplateList = mailTemplateDao.Query(mailTemplate);
        StringBuilder content = new StringBuilder();
        content.append(mailTitle.getContent());
        for(MailTemplate template : mailTemplateList){
            if(!template.getTagName().equals("resv1")){
                content.append(",");
                content.append(template.getContent());
            }
        }*/
        MailHunterHistory mailHunterHistory = new MailHunterHistory();
        mailHunterHistory.setUniqId(uniqId);                
        mailHunterHistory.setType(type);
        mailHunterHistory.setTemplateId(configDao.ReadConfigValue("MailHunter.ED3FinishTemplateId"));
        mailHunterHistory.setChtName(chtName);
        mailHunterHistory.setIdno(idno);
        mailHunterHistory.setEMailAddress(email);
        mailHunterHistory.setStatus("0");              
        mailHunterHistory.setTitle(title);                
        /*String productMsg = "";
        if(productId.equals(SystemConst.CASEDATA_PRODUCTID_ACCOUNT)){
            productMsg = configDao.ReadConfigValue("MailHunter.ED3PrdouctAMsg");
        }else{
            productMsg = configDao.ReadConfigValue("MailHunter.ED3PrdouctCCAMsg");
        }*/        
        mailHunterHistory.setContent(content);
        /*mailHunterHistory.setProjectCode(globalConfig.MailHunterProjectCode);
        mailHunterHistory.setOwnerID(globalConfig.MailHunterOwnerID);
        mailHunterHistory.setProductCode(globalConfig.MailHunterProductCode);*/
        mailHunterHistory.setProjectCode(configDao.ReadConfigValue("MailHunter.ED3ProjectCode"));
        mailHunterHistory.setOwnerID(configDao.ReadConfigValue("MailHunter.ED3OwnerID"));
        mailHunterHistory.setProductCode(configDao.ReadConfigValue("MailHunter.ED3ProductCode"));
        if(attatchment.length>0){
            mailHunterHistory.setAttatchment(attatchment);
        }
        mailHunterHistoryDao.Create(mailHunterHistory);
    }    


    public String combinedContent(String type){
        MailTemplate mailTemplate = new MailTemplate(type);
        mailTemplate.setTagName("resv1");
        MailTemplate mailTitle = mailTemplateDao.Read(mailTemplate);
        List<MailTemplate> mailTemplateList = mailTemplateDao.Query(mailTemplate);
        StringBuilder content = new StringBuilder();
        content.append(mailTitle.getContent());
        for(MailTemplate template : mailTemplateList){
            if(!template.getTagName().trim().equals("resv1")){
                content.append(",");
                content.append(template.getContent());
            }
        }
        return content.toString();   
    }


    public String getMailTitle(String type,String productMsg){
        MailTemplate mailTitle = new MailTemplate(type);
        mailTitle.setTagName("resv1");
        mailTitle = mailTemplateDao.Read(mailTitle);
        return mailTitle.getContent().replace("%productId", productMsg);
    }


    // SECTOR: RPL JOB MAIL SENDED
    public void sendContractMailHunter(String uniqId, String email, byte[] pdfByte) {

        // 處理templateId,title
        String title = "凱基完成通知";
        String templateName = "MOCK";
        // String TemplateId = getConfigQuery(templateName);
        String TemplateId = templateName;
        TemplateId = TemplateId.equals("") ? "1263" : TemplateId;
        // 處理後續會用到的ID資訊，目前想法是以JSON存在content內
        String idno = uniqId;/** 看原本的 template中的ID是帶入contractData的UniqId ，所以延用 */
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("idno", idno);
        String content = jsonObject.toString();

        logger.info("========>>> #SERVICE mailHunter.... uniqId: {}, title: {}, content: {}, email: {}, TemplateId: {}", uniqId, title, content, email, TemplateId);
        MailHistory mailHistory = setMailHunterCaseMailHistory(uniqId, title, content, email, TemplateId);
        mailHistory.setAttatchment(pdfByte);
        mailHistoryDao.Create(mailHistory);

        System.out.println("set Email done!");
    }





    // SECTOR: METHOD
    /**
     * MailHunter用的template
     * @param uniqId
     * @param title
     * @param content
     * @param eMail
     * @param templateName
     * @return
     */
    private MailHistory setMailHunterCaseMailHistory(String uniqId, String title, String content, String eMail, String templateName) {
        MailHistory mailHistory = MailHistory.getInstance().setMailType(MAILTYPE).setStatus("0").setUniqId(uniqId).setEMailAddress(eMail).setTemplateId(templateName).setTitle(title).setContent(content);
        return mailHistory;
    }

}
