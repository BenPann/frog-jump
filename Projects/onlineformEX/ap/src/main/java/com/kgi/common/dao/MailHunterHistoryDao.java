package com.kgi.common.dao;

import java.util.List;

import com.kgi.eopend3.common.dto.db.MailHunterHistory;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class MailHunterHistoryDao extends  CRUDQDao<MailHunterHistory> {

    @Override
    public int Create(MailHunterHistory fullItem) {
        String sql = " insert into MailHunterHistory (UniqId,Type,TemplateId,ChtName,Idno,EMailAddress,Status,ErrorMessage," +
                     " Title,Content,ProjectCode,OwnerID,ProductCode,Attatchment,CreateTime,UpdateTime) " +
                     " values(:UniqId, :Type, :TemplateId, :ChtName, :Idno, :EMailAddress, :Status, :ErrorMessage, :Title, :Content, :ProjectCode, " +
                     " :OwnerID, :ProductCode, :Attatchment," +
                     " GETDATE(), GETDATE()) ";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public MailHunterHistory Read(MailHunterHistory keyItem) {
        try {
            String sql = "Select * from MailHistory where Serial=?";
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(MailHunterHistory.class),
                    new Object[] { keyItem.getSerial() });
        } catch (DataAccessException ex) {
            return null;
        }
    }

    @Override
    public int Update(MailHunterHistory fullItem) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE MailHunterHistory SET ");
        sb.append("Status = ? ,");
        sb.append("ErrorMessage = ?,");
        sb.append("UpdateTime=getDate()");
        sb.append(" where Serial = ?");

        return this.getJdbcTemplate().update(sb.toString(),
                new Object[] { fullItem.getStatus(), fullItem.getErrorMessage(), fullItem.getSerial() });
    }

    @Override
    public int Delete(MailHunterHistory keyItem) {
        String sql = "DELETE FROM MailHistory where Serial = " + keyItem.getSerial();
        return this.getJdbcTemplate().update(sql);
    }

    @Override
    public List<MailHunterHistory> Query(MailHunterHistory keyItem) {
        String sql = "SELECT * FROM MailHunterHistory where Status = ?";
        return this.getJdbcTemplate().query(sql, new BeanPropertyRowMapper<>(MailHunterHistory.class),
                new Object[] { keyItem.getStatus() });
    }

}
