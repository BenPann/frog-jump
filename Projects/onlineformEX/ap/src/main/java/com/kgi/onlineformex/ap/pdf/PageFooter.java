package com.kgi.onlineformex.ap.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import org.springframework.stereotype.Component;

@Component
public class PageFooter extends PdfPageEventHelper {

    public String caseType = "";
    
    public void onEndPage(PdfWriter writer, Document document) {}
    public void onOpenDocument(PdfWriter writer, Document document) {}
    public void onCloseDocument(PdfWriter writer, Document document) {}


}
