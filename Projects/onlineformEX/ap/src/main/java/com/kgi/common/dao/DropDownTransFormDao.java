package com.kgi.common.dao;

import java.util.ArrayList;
import java.util.List;

import com.kgi.eopend3.common.dto.db.DropDownTransForm;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class DropDownTransFormDao extends CRUDQDao<DropDownTransForm> {

    @Override
    public int Create(DropDownTransForm fullItem) {
        return 0;
    }

    @Override
    public DropDownTransForm Read(DropDownTransForm keyItem) {
        List<String> params = new ArrayList<>();
		String sql = "SELECT * FROM DropDownTransForm WHERE DropDownDataKey = ? ";
		params.add(keyItem.getDropDownDataKey());
		if(!keyItem.getCredit().equals("")){
			sql += " AND Credit = ? ";			
			params.add(keyItem.getCredit());
		}
		if(!keyItem.getLoan().equals("")){
			sql += " AND Loan = ? ";			
			params.add(keyItem.getLoan());
		}
		if(!keyItem.getEop().equals("")){
			sql += " AND Eop = ? ";
			params.add(keyItem.getEop());
		}			
		try {
			return this.getJdbcTemplate().queryForObject(sql, params.toArray() ,new BeanPropertyRowMapper<>(DropDownTransForm.class));
		} catch (DataAccessException ex) {
			logger.error("DropDownTransForm查無資料");                        
			return null;
		}
    }

    @Override
    public int Update(DropDownTransForm fullItem) {
        return 0;
    }

    @Override
    public int Delete(DropDownTransForm keyItem) {
        return 0;
    }

    @Override
    public List<DropDownTransForm> Query(DropDownTransForm keyItem) {
        return null;
    }
}
