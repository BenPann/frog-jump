package com.kgi.common.dao;

import java.util.List;
import com.kgi.eopend3.common.dto.db.QR_ShortUrlCaseMatch;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class QR_ShortUrlCaseMatchDao  extends CRUDQDao<QR_ShortUrlCaseMatch> {

	@Override
	public int Create(QR_ShortUrlCaseMatch fullItem) {
		String sql = "INSERT INTO QR_ShortUrlCaseMatch (ShortUrl,UniqId,UniqType) VALUES (:ShortUrl,:UniqId,:UniqType)";
		return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
	}

	@Override
	public QR_ShortUrlCaseMatch Read(QR_ShortUrlCaseMatch fullItem) {	
		String sql = "SELECT * FROM QR_ShortUrlCaseMatch WHERE UniqId = ? ";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_ShortUrlCaseMatch.class),
                    new Object[] { fullItem.getUniqId() });
        } catch (DataAccessException ex) {
//            System.out.println("QR_ShortUrlCaseMatch查無資料");
            return null;
        }
	}

	@Override
	public int Update(QR_ShortUrlCaseMatch fullItem) {
		return 0;
	}

	@Override
	public int Delete(QR_ShortUrlCaseMatch keyItem) {
		return 0;
	}

	@Override
	public List<QR_ShortUrlCaseMatch> Query(QR_ShortUrlCaseMatch keyItem) {
		return null;
	}
}