package com.kgi.eopend3.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//@AllArgsConstructor
public class LoginView {

    private String promoChannelID;
    private String promoDepart;
    private String promoMember;    
    private String uniqType;
    private String entry;
    private String shortUrl;
    @CheckNullAndEmpty
    private String productType;
    @CheckNullAndEmpty
    private String idno ;
    @CheckNullAndEmpty
    private String birthday ;
//    @CheckNullAndEmpty
    private String ipAddress ;
//    @CheckNullAndEmpty
    private String browser ;
//    @CheckNullAndEmpty
    private String platform ;
//    @CheckNullAndEmpty
    private String os ;

}