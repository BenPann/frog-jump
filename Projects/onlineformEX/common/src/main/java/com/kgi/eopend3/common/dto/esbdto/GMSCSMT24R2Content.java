package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "sendType" })
public class GMSCSMT24R2Content {

    // @XmlElement(name = "SEND-TYPE")
    // private String sendType;
    @XmlElement(name = "SO2401")
    private String SO2401;
    @XmlElement(name = "SO2402")
    private String SO2402;
    @XmlElement(name = "SO2403")
    private String SO2403;
    @XmlElement(name = "SO2404")
    private String SO2404;
    @XmlElement(name = "SO2405")
    private String SO2405;
    @XmlElement(name = "SO2406")
    private String SO2406;
    @XmlElement(name = "SO2407")
    private String SO2407;
    @XmlElement(name = "SO2408")
    private String SO2408;
    @XmlElement(name = "SO2409")
    private String SO2409;
    @XmlElement(name = "SO2410")
    private String SO2410;
    @XmlElement(name = "SO2411")
    private String SO2411;
    @XmlElement(name = "SO2412")
    private String SO2412;
    @XmlElement(name = "SO2413")
    private String SO2413;
    @XmlElement(name = "SO2414")
    private String SO2414;
    @XmlElement(name = "SO2415")
    private String SO2415;
    @XmlElement(name = "SO2416")
    private String SO2416;
    @XmlElement(name = "SO2417")
    private String SO2417;
    @XmlElement(name = "SO2418")
    private String SO2418;
    @XmlElement(name = "SO2419")
    private String SO2419;

   
}
