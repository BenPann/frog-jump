package com.kgi.eopend3.common.dto.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RtnResp {
    private Integer status;
    private LoginRes result;
    private String message;
}