package com.kgi.mgm.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MgmSummaryBonusData {

    private String Q_ID;
    private String YYYYMM;
    private String ER_ID;
    private String EE_ID;
    private String EE_NAME;
    private String PRO_FLG;
    private String PRO_NAME;
    private String ER_ACC_TYPE;
    private String ER_BONUS_TYPE;
    private String ER_BONUS_NAME;
    private String ER_BONUS_AMT;
}