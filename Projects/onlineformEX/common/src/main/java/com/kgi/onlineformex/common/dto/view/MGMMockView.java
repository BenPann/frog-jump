package com.kgi.onlineformex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MGMMockView {
    private String Alpha;
    private String Beta;
    private String Gamma;
    private String Delta;
    private String Epsilon;
}