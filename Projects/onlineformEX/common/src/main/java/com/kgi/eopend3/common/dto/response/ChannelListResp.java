package com.kgi.eopend3.common.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChannelListResp {

    private String channelId;
    private String channelName;
    private String idLength;
    private String idLogic;

}