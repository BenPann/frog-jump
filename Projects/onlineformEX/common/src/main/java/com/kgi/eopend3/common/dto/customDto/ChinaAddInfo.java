package com.kgi.eopend3.common.dto.customDto;

public class ChinaAddInfo {

	private String chanId = "" ;
	private String promoDepart = "" ;
	private String promoMmbId = "" ;
	private String promoMmbName = "" ;

	public String getChanId() {
		return chanId;
	}
	public void setChanId(String chanId) {
		this.chanId = chanId;
	}
	public String getPromoDepart() {
		return promoDepart;
	}
	public void setPromoDepart(String promoDepart) {
		this.promoDepart = promoDepart;
	}
	public String getPromoMmbId() {
		return promoMmbId;
	}
	public void setPromoMmbId(String promoMmbId) {
		this.promoMmbId = promoMmbId;
	}
	public String getPromoMmbName() {
		return promoMmbName;
	}
	public void setPromoMmbName(String promoMmbName) {
		this.promoMmbName = promoMmbName;
	}

	
	
	
}
