package com.kgi.onlineformex.common.dto.orbit;

import java.util.ArrayList;

public class OrbitRoot extends OrbitData {

	public OrbitImage orbitImage = new OrbitImage();
	public OrbitHeader orbitHeader = new OrbitHeader();
	public ArrayList<OrbitImage> lsOrbitImage = new ArrayList<OrbitImage>();

	@Override
	public void toXML(StringBuilder sb) {
		sb.append("<?xml version='1.0' encoding='UTF-8'?>");
		sb.append("<Orbit>");

		orbitHeader.toXML(sb);

		sb.append("<scanFiles>");

		for (OrbitImage orbitImage : lsOrbitImage) {
			orbitImage.toXML(sb);
		}
		sb.append("</scanFiles>");
		sb.append("</Orbit>");
	}

	public void add(String fileName, String caseId, String documentType, String idNumber, String productid, boolean isApplyLoanCC,String casePriority) {
		lsOrbitImage.add(OrbitImage.from(fileName, caseId, documentType, idNumber, "9743", productid, isApplyLoanCC,casePriority));
	}

	/**不傳unitId的版本 就傳預設值 */
	public void addMain(String fileName, String documentType, String idNumber, String productid, boolean isApplyLoanCC,String casePriority) {
		this.addMain(fileName, documentType, idNumber, "9743", productid, isApplyLoanCC,casePriority);
	}

	/** 補件用的版本 多一個unitId(Brachtype) */
	public void addMain(String fileName, String documentType, String idNumber, String unitId, String productid, boolean isApplyLoanCC,String casePriority) {
		lsOrbitImage.add(OrbitImage.from(fileName, "1", documentType, idNumber, unitId, productid, isApplyLoanCC,casePriority));
	}

	public void addRejectMain(String fileName, String documentType, String idNumber, String productId, String branchId) {
		lsOrbitImage.add(OrbitImage.rejectFrom(fileName, "1", documentType, idNumber, productId, branchId));
	}

	public void addSub(String fileName, String documentType, String idNumber) {
		lsOrbitImage.add(OrbitImage.from(fileName));
	}

	public void addConRow(String fileName, String idNumber, String product, String branchType, String casePriority) {
		lsOrbitImage.add(OrbitImage.contractfrom(fileName, "1", idNumber, product, branchType, casePriority));
	}

	public void addSub(String fileName) {
		lsOrbitImage.add(OrbitImage.from(fileName));
	}

}
