package com.kgi.eopend3.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IDOCRRes {

    private String RspCode = "";
    private String Name = "";
    private String IDCARD_ISSUE_DT = "";
    private String IDCARD_ISSUE_Type = "";
    private String IDCARD_ISSUE_City = "";
    private String HouseholdAddr1 = "";
    private String HouseholdAddr2 = "";
    private String HouseholdAddr3 = "";
    private String HouseholdAddr4 = "";
    private String Marriage = "";
}