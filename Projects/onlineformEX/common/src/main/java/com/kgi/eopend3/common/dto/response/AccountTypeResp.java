package com.kgi.eopend3.common.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AccountTypeResp {
    private String ed3OfflineContent;
    private String ed3OnlineContent;
    private String ed3OfflineTitle;
    private String ed3OnlineTitle;
}