package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class QR_ChannelDepartListEOP {

    @NonNull
    private String ChannelId;
    @NonNull
    private String DepartId;
    private String PromoDepart1 = "";
    private String PromoMember1 = "";
    private String PromoDepart2 = "";
    private String PromoMember2 = "";
    private String AssignDepart = "";
	
}