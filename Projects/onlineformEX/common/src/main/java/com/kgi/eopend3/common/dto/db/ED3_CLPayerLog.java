package com.kgi.eopend3.common.dto.db;

public class ED3_CLPayerLog {

	private int PolicySerial ;
	private String PayersStatus;
	private String PayerFailMsg = "";
	private String CreateTime ;
	public int getPolicySerial() {
		return PolicySerial;
	}
	public void setPolicySerial(int policySerial) {
		PolicySerial = policySerial;
	}
	public String getPayersStatus() {
		return PayersStatus;
	}
	public void setPayersStatus(String payersStatus) {
		PayersStatus = payersStatus;
	}
	public String getPayerFailMsg() {
		return PayerFailMsg;
	}
	public void setPayerFailMsg(String payerFailMsg) {
		PayerFailMsg = payerFailMsg;
	}
	public String getCreateTime() {
		return CreateTime;
	}
	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}
	

	
	
	
}