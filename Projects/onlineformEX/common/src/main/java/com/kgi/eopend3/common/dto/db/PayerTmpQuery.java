package com.kgi.eopend3.common.dto.db;

public class PayerTmpQuery {

	private String SerialId = "" ;
	private String ApplyCode = "" ;
	private String ParentApplyCode = "" ;
	private String TransType = "" ;
	private String LaunchBankCode = "" ;
	private String LaunchUniformId = "" ;
	private String LaunchName = "" ;
	private String PayerId = "" ;
	private String PayerName = "" ;
	private String PayerNo = "" ;
	private String PayerNo1 = "" ;
	private String PayerNo2 = "" ;
	private String TransBankCode = "" ;
	private String PayerAccountType = "" ;
	private String PayerAccountNo = "" ;
	private String CHTStationNo = "" ;
	private String HealthInsuranceDataType = "" ;
	private String HealthGroupInsuranceApplicantId = "" ;
	private String LicencePlateType = "" ;
	private String Tel = "" ;
	private String Email = "" ;
	private String ChargeDate = "" ;
	private String ChargeTimes = "" ;
	private String IncludeFail = "" ;
	private String ChargeMaxAmountBySingle = "" ;
	private String IsNeedApprove = "" ;
	private String Status = "" ;
	private String ResultCode = "" ;
	private String ResultCodeDescription = "" ;
	private String ApplyDate = "" ;
	private String FileRejectDate = "" ;
	private String ApproveLevel3SignDate = "" ;
	private String CancelDate = "" ;
	private String CancelFileRejectDate = "" ;
	private String CancelLevel3SignDate = "" ;
	private String FileExportDate = "" ;
	private String FileImportDate = "" ;
	private String CreateUserId = "" ;
	private String CreateUser = "" ;
	private String CreateDate = "" ;
	private String ModiUserId = "" ;
	private String ModiDate = "" ;
	private String ModiUser = "" ;

	public String getSerialId() {
		return SerialId;
	}
	public void setSerialId(String serialId) {
		SerialId = serialId;
	}
	public String getApplyCode() {
		return ApplyCode;
	}
	public void setApplyCode(String applyCode) {
		ApplyCode = applyCode;
	}
	public String getParentApplyCode() {
		return ParentApplyCode;
	}
	public void setParentApplyCode(String parentApplyCode) {
		ParentApplyCode = parentApplyCode;
	}
	public String getTransType() {
		return TransType;
	}
	public void setTransType(String transType) {
		TransType = transType;
	}
	public String getLaunchBankCode() {
		return LaunchBankCode;
	}
	public void setLaunchBankCode(String launchBankCode) {
		LaunchBankCode = launchBankCode;
	}
	public String getLaunchUniformId() {
		return LaunchUniformId;
	}
	public void setLaunchUniformId(String launchUniformId) {
		LaunchUniformId = launchUniformId;
	}
	public String getLaunchName() {
		return LaunchName;
	}
	public void setLaunchName(String launchName) {
		LaunchName = launchName;
	}
	public String getPayerId() {
		return PayerId;
	}
	public void setPayerId(String payerId) {
		PayerId = payerId;
	}
	public String getPayerName() {
		return PayerName;
	}
	public void setPayerName(String payerName) {
		PayerName = payerName;
	}
	public String getPayerNo() {
		return PayerNo;
	}
	public void setPayerNo(String payerNo) {
		PayerNo = payerNo;
	}
	public String getPayerNo1() {
		return PayerNo1;
	}
	public void setPayerNo1(String payerNo1) {
		PayerNo1 = payerNo1;
	}
	public String getPayerNo2() {
		return PayerNo2;
	}
	public void setPayerNo2(String payerNo2) {
		PayerNo2 = payerNo2;
	}
	public String getTransBankCode() {
		return TransBankCode;
	}
	public void setTransBankCode(String transBankCode) {
		TransBankCode = transBankCode;
	}
	public String getPayerAccountType() {
		return PayerAccountType;
	}
	public void setPayerAccountType(String payerAccountType) {
		PayerAccountType = payerAccountType;
	}
	public String getPayerAccountNo() {
		return PayerAccountNo;
	}
	public void setPayerAccountNo(String payerAccountNo) {
		PayerAccountNo = payerAccountNo;
	}
	public String getCHTStationNo() {
		return CHTStationNo;
	}
	public void setCHTStationNo(String cHTStationNo) {
		CHTStationNo = cHTStationNo;
	}
	public String getHealthInsuranceDataType() {
		return HealthInsuranceDataType;
	}
	public void setHealthInsuranceDataType(String healthInsuranceDataType) {
		HealthInsuranceDataType = healthInsuranceDataType;
	}
	public String getHealthGroupInsuranceApplicantId() {
		return HealthGroupInsuranceApplicantId;
	}
	public void setHealthGroupInsuranceApplicantId(String healthGroupInsuranceApplicantId) {
		HealthGroupInsuranceApplicantId = healthGroupInsuranceApplicantId;
	}
	public String getLicencePlateType() {
		return LicencePlateType;
	}
	public void setLicencePlateType(String licencePlateType) {
		LicencePlateType = licencePlateType;
	}
	public String getTel() {
		return Tel;
	}
	public void setTel(String tel) {
		Tel = tel;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getChargeDate() {
		return ChargeDate;
	}
	public void setChargeDate(String chargeDate) {
		ChargeDate = chargeDate;
	}
	public String getChargeTimes() {
		return ChargeTimes;
	}
	public void setChargeTimes(String chargeTimes) {
		ChargeTimes = chargeTimes;
	}
	public String getIncludeFail() {
		return IncludeFail;
	}
	public void setIncludeFail(String includeFail) {
		IncludeFail = includeFail;
	}
	public String getChargeMaxAmountBySingle() {
		return ChargeMaxAmountBySingle;
	}
	public void setChargeMaxAmountBySingle(String chargeMaxAmountBySingle) {
		ChargeMaxAmountBySingle = chargeMaxAmountBySingle;
	}
	public String getIsNeedApprove() {
		return IsNeedApprove;
	}
	public void setIsNeedApprove(String isNeedApprove) {
		IsNeedApprove = isNeedApprove;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getResultCode() {
		return ResultCode;
	}
	public void setResultCode(String resultCode) {
		ResultCode = resultCode;
	}
	public String getResultCodeDescription() {
		return ResultCodeDescription;
	}
	public void setResultCodeDescription(String resultCodeDescription) {
		ResultCodeDescription = resultCodeDescription;
	}
	public String getApplyDate() {
		return ApplyDate;
	}
	public void setApplyDate(String applyDate) {
		ApplyDate = applyDate;
	}
	public String getFileRejectDate() {
		return FileRejectDate;
	}
	public void setFileRejectDate(String fileRejectDate) {
		FileRejectDate = fileRejectDate;
	}
	public String getApproveLevel3SignDate() {
		return ApproveLevel3SignDate;
	}
	public void setApproveLevel3SignDate(String approveLevel3SignDate) {
		ApproveLevel3SignDate = approveLevel3SignDate;
	}
	public String getCancelDate() {
		return CancelDate;
	}
	public void setCancelDate(String cancelDate) {
		CancelDate = cancelDate;
	}
	public String getCancelFileRejectDate() {
		return CancelFileRejectDate;
	}
	public void setCancelFileRejectDate(String cancelFileRejectDate) {
		CancelFileRejectDate = cancelFileRejectDate;
	}
	public String getCancelLevel3SignDate() {
		return CancelLevel3SignDate;
	}
	public void setCancelLevel3SignDate(String cancelLevel3SignDate) {
		CancelLevel3SignDate = cancelLevel3SignDate;
	}
	public String getFileExportDate() {
		return FileExportDate;
	}
	public void setFileExportDate(String fileExportDate) {
		FileExportDate = fileExportDate;
	}
	public String getFileImportDate() {
		return FileImportDate;
	}
	public void setFileImportDate(String fileImportDate) {
		FileImportDate = fileImportDate;
	}
	public String getCreateUserId() {
		return CreateUserId;
	}
	public void setCreateUserId(String createUserId) {
		CreateUserId = createUserId;
	}
	public String getCreateUser() {
		return CreateUser;
	}
	public void setCreateUser(String createUser) {
		CreateUser = createUser;
	}
	public String getCreateDate() {
		return CreateDate;
	}
	public void setCreateDate(String createDate) {
		CreateDate = createDate;
	}
	public String getModiUserId() {
		return ModiUserId;
	}
	public void setModiUserId(String modiUserId) {
		ModiUserId = modiUserId;
	}
	public String getModiDate() {
		return ModiDate;
	}
	public void setModiDate(String modiDate) {
		ModiDate = modiDate;
	}
	public String getModiUser() {
		return ModiUser;
	}
	public void setModiUser(String modiUser) {
		ModiUser = modiUser;
	}

	
	
}
