package com.kgi.eopend3.common.dto.view;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WebBankView {

    private String bankUser = "";
    @SerializedName(value = "bankpwd")
    private String bankpazzd = "";
    @SerializedName(value = "ivrBankpwd")
    private String ivrBankpazzd = "";
       
}