package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class ED3_CaseDataTerms {

    @NonNull
    private String UniqId; 
    @NonNull
    private int TermSerial;
    private String IsAgree;
    
}
