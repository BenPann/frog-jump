package com.kgi.onlineformex.common.dto.orbit;

public class OrbitImage extends OrbitData {

	public String 
		fileName,
		caseId = "",
		FMID = "",
		covertPage = "",
		productType = "",
		branchType = "",
		casePriority = "",
		documentType = "",
		cardApplyType = "",
		cardType = "",
		idNumber = "",
		cardNumber = "",
		detailId = "";
	
	
	//MainRow  web進件
	public static OrbitImage from(String fileName, String caseId, String documentType, String idNumber, String branchType, String productId, boolean isApplyLoanCC, String casePriority) {

		OrbitImage orbitImage = new OrbitImage();
		orbitImage.fileName = fileName;
		orbitImage.caseId = caseId;
		orbitImage.FMID = "A001";				// FMID       固定A001
		orbitImage.covertPage = "A001";			// CovertPage	固定A001
		
		//如果確認後是卡加貸產品 將P035 改成 P037
		if(isApplyLoanCC){
			orbitImage.productType = "P037";  		// 產品別              卡加貸為037
		} else {
			orbitImage.productType = "P035";		// 產品別               web進件案件固定為P035
		}
		orbitImage.branchType = "B".concat(branchType);		// branchType
		//orbitImage.casePriority = "F001";		// 固定F001	
		
		//4/23 因應中壽認股案 修改成
		if(!casePriority.equals("")) {
			orbitImage.casePriority = casePriority;
		}else {
			if(productId.equals("3")){
				//E貸寶 P002  
				orbitImage.casePriority = "F036";		
			}else if(productId.equals("2")){
				//PLOAN P003
				orbitImage.casePriority = "F037";
			}else{
				//預設值
				orbitImage.casePriority = "F001";
			}
			
		}
		
		
		orbitImage.documentType = documentType; // 合約C003
		orbitImage.idNumber = idNumber;
		
		return orbitImage;
	}


	//MainRow  退件專用
	/** 此為退件專用 */
	public static OrbitImage rejectFrom(String fileName, String caseId, String documentType, String idNumber,
			String productId, String branchId) {
		OrbitImage orbitImage = new OrbitImage();
		orbitImage.fileName = fileName;
		orbitImage.caseId = caseId;
		orbitImage.FMID = "A001"; // FMID 固定A001
		orbitImage.covertPage = "A001"; // CovertPage 固定A001

		// TODO 之後因應QRCode產生器 會再做調整
		// E貸寶不支援自動簽收 所以帶P002 其餘帶P035
		// 產品別
		if (productId.equals("3")) {
			// E貸寶 P002
			orbitImage.productType = "P002";
		} else if (productId.equals("2")) {
			// PLOAN P003
			orbitImage.productType = "P035";
		} else {
			// 預設值
			orbitImage.productType = "P035";
		}

		orbitImage.branchType = "B".concat(branchId); // branchType
		orbitImage.casePriority = "F001"; // 固定F001
		orbitImage.documentType = documentType; // 合約C003
		orbitImage.idNumber = idNumber;

		return orbitImage;
	}


	//SubRow
	public static OrbitImage from(String fileName) {
		OrbitImage orbitImage = new OrbitImage();
		orbitImage.fileName = fileName;
		return orbitImage;
	}


	//MainRow 契約書
	public static OrbitImage contractfrom(String fileName, String caseId, String idNumber, String product,String branchId,String casePriority) {
		OrbitImage orbitImage = new OrbitImage();
		orbitImage.fileName = fileName;
		orbitImage.caseId = caseId;
		orbitImage.FMID = "A001";				// FMID       固定A001
		orbitImage.covertPage = "A001";			// CovertPage	固定A001
		
		// 產品別 
		switch(product) {
			default:
				orbitImage.productType = "P002";//預設時暫時給P002
				break;
		}

		orbitImage.branchType = "B".concat(branchId);
		orbitImage.casePriority = !casePriority.equals("") ? casePriority : "F033";	// F033 線上簽署契變書
		orbitImage.documentType = "C005"; 		// C005
		orbitImage.idNumber = idNumber;
		return orbitImage;
	}


	@Override
	public void toXML(StringBuilder sb) {
		sb.append("<row");
		putXML(sb, "fileName", fileName);
		putXML(sb, "caseId", caseId);
		putXML(sb, "FMID", FMID);
		putXML(sb, "covertPage", covertPage);
		putXML(sb, "productType", productType);
		putXML(sb, "branchType", branchType);
		putXML(sb, "casePriority", casePriority);
		putXML(sb, "documentType", documentType);
		putXML(sb, "cardApplyType", cardApplyType);
		putXML(sb, "cardType", cardType);
		putXML(sb, "idNumber", idNumber);
		putXML(sb, "cardNumber", cardNumber);
		putXML(sb, "detailId", detailId);
		putXML(sb, "imgType", ""); // 2020-08-19 demand by Ben
		sb.append(" />");
	}


}
