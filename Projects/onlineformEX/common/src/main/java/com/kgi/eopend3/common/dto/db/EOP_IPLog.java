package com.kgi.eopend3.common.dto.db;

public class EOP_IPLog {

    private String UniqId = "";
    private String IPAddress = "";

    public EOP_IPLog() {
    }

    public EOP_IPLog(String UniqId, String IPAddress) {
        this.UniqId = UniqId;
        this.IPAddress = IPAddress;
    }

    public String getUniqId() {
        return UniqId;
    }

    public void setUniqId(String uniqId) {
        UniqId = uniqId;
    }

    public String getIPAddress() {
        return IPAddress;
    }

    public void setIPAddress(String iPAddress) {
        IPAddress = iPAddress;
    }
    
}
