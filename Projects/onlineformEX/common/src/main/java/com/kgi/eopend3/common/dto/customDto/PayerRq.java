package com.kgi.eopend3.common.dto.customDto;

import com.google.gson.GsonBuilder;

public class PayerRq {

	private String ApplyCode = "" ;
	private String TransType = "" ;
	private String LaunchUniformId = "" ;
	private String AccountOwnerID = "" ;
	private String PayerId = "" ;
	private String PayerName = "" ;
	private String PayerNo = "" ;
	private String TransBankCode = "" ;
	private String PayerAccountType = "" ;
	private String PayerAccountNo = "" ;
	private String CHTStationNo = "" ;
	private String HealthInsuranceDataType = "" ;
	private String LicencePlateType = "" ;
	private String Tel = "" ;
	private String Email = "" ;
	private String ChargeDate = "" ;
	private String IncludeFail = "" ;
	private String ChargeMaxAmountBySingle = "0" ;
	private String Memo1 = "" ;
	private String Memo2 = "" ;
	private String ActionType = "" ;
	private String SYSID = "" ;
	private String APKey = "" ;
	private String RequestId = "" ;

	

	public String getApplyCode() {
		return ApplyCode;
	}



	public void setApplyCode(String applyCode) {
		ApplyCode = applyCode;
	}



	public String getTransType() {
		return TransType;
	}



	public void setTransType(String transType) {
		TransType = transType;
	}



	public String getLaunchUniformId() {
		return LaunchUniformId;
	}



	public void setLaunchUniformId(String launchUniformId) {
		LaunchUniformId = launchUniformId;
	}



	public String getAccountOwnerID() {
		return AccountOwnerID;
	}



	public void setAccountOwnerID(String accountOwnerID) {
		AccountOwnerID = accountOwnerID;
	}



	public String getPayerId() {
		return PayerId;
	}



	public void setPayerId(String payerId) {
		PayerId = payerId;
	}



	public String getPayerName() {
		return PayerName;
	}



	public void setPayerName(String payerName) {
		PayerName = payerName;
	}



	public String getPayerNo() {
		return PayerNo;
	}



	public void setPayerNo(String payerNo) {
		PayerNo = payerNo;
	}



	public String getTransBankCode() {
		return TransBankCode;
	}



	public void setTransBankCode(String transBankCode) {
		TransBankCode = transBankCode;
	}



	public String getPayerAccountType() {
		return PayerAccountType;
	}



	public void setPayerAccountType(String payerAccountType) {
		PayerAccountType = payerAccountType;
	}



	public String getPayerAccountNo() {
		return PayerAccountNo;
	}



	public void setPayerAccountNo(String payerAccountNo) {
		PayerAccountNo = payerAccountNo;
	}



	public String getCHTStationNo() {
		return CHTStationNo;
	}



	public void setCHTStationNo(String cHTStationNo) {
		CHTStationNo = cHTStationNo;
	}



	public String getHealthInsuranceDataType() {
		return HealthInsuranceDataType;
	}



	public void setHealthInsuranceDataType(String healthInsuranceDataType) {
		HealthInsuranceDataType = healthInsuranceDataType;
	}



	public String getLicencePlateType() {
		return LicencePlateType;
	}



	public void setLicencePlateType(String licencePlateType) {
		LicencePlateType = licencePlateType;
	}



	public String getTel() {
		return Tel;
	}



	public void setTel(String tel) {
		Tel = tel;
	}



	public String getEmail() {
		return Email;
	}



	public void setEmail(String email) {
		Email = email;
	}



	public String getChargeDate() {
		return ChargeDate;
	}



	public void setChargeDate(String chargeDate) {
		ChargeDate = chargeDate;
	}



	public String getIncludeFail() {
		return IncludeFail;
	}



	public void setIncludeFail(String includeFail) {
		IncludeFail = includeFail;
	}



	public String getChargeMaxAmountBySingle() {
		return ChargeMaxAmountBySingle;
	}



	public void setChargeMaxAmountBySingle(String chargeMaxAmountBySingle) {
		ChargeMaxAmountBySingle = chargeMaxAmountBySingle;
	}



	public String getMemo1() {
		return Memo1;
	}



	public void setMemo1(String memo1) {
		Memo1 = memo1;
	}



	public String getMemo2() {
		return Memo2;
	}



	public void setMemo2(String memo2) {
		Memo2 = memo2;
	}



	public String getActionType() {
		return ActionType;
	}



	public void setActionType(String actionType) {
		ActionType = actionType;
	}



	public String getSYSID() {
		return SYSID;
	}



	public void setSYSID(String sYSID) {
		SYSID = sYSID;
	}



	public String getAPKey() {
		return APKey;
	}



	public void setAPKey(String aPKey) {
		APKey = aPKey;
	}



	public String getRequestId() {
		return RequestId;
	}



	public void setRequestId(String requestId) {
		RequestId = requestId;
	}



	public String toJsonString() {
		return new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(this) ;
	}
}
