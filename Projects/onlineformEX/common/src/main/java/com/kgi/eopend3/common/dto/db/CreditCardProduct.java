package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreditCardProduct {
    int CardSerial;
    String ProductId = "";
    String CardType = "";
    String CardTitle = "";    
    String CardContent = "";    
    String CardImage = "";
    String StartDate = "";
    String EndDate = "";
    String CardStatus = "";
  
}
