package com.kgi.eopend3.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SendOrbitMainDataDto {
    private String eOpenUnit;
    private String eOpenUnitName;
    private String eOpenEPCode;
    private String eOpenEPName;
    private String eOpenCoId;
    private String eOpenConame;
    private String eOpenCoTelCode;
    private String eOpenCoTel;
    private String eOpenJobOcp;
    private String eOpenHRisk;
    private String eOpenExist;
    private String eOpenDate;
    private String eOpenIDNo;
    private String eOpenBrdDate;
    private String eOpenCellNo;
    private String eOpenCnName;
    private String eOpenCnName2;
    private String eOpenEgLName;
    private String eOpenTWTx;
    private String eOpenAgmntConf;
    private String eOpenAgmntNo;
    private String eOpenBrhId;
    private String eOpenBrhName;
    private String eOpenPayAcc;
    private String eOpenAccCrcy;
    private String eOpenCrdCard;
    private String eOpenCrdCardConf;
    private String eOpenCrdCardUrl;
    private String eOpenBKCnty;
    private String eOpenBKCntyCode;
    private String eOpenBKCode;
    private String eOpenBKName;
    private String eOpenIDCChgType;
    private String eOpenIDCChgDate;
    private String eOpenIDCChgPlace;
    private String eOpenIDCAddrCode;
    private String eOpenIDCAddr;
    private String eOpenCntAddrCode;
    private String eOpenIDCTelNo;
    private String eOpenMarrStatus;
    private String eOpenBillSendType;
    private String eOpenIDCTelCode;
    private String eOpenContTelCode;
    private String eOpenContTel;
    private String eOpenEDU;
    private String eOpenTWBBook;
    private String eOpenFCBBook;
    private String eOpenJobTitle;
    private String eOpenAnlIncome;


}
