package com.kgi.eopend3.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@CheckNullAndEmpty
public class ChannelListView {
   
    private String channelID = "";
    
}