package com.kgi.mgm.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ForgotPassword {
    public String CUST_ID;
    public String BIRTH_4;
}
