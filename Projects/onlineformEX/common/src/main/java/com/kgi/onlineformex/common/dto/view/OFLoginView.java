package com.kgi.onlineformex.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class OFLoginView {
    @CheckNullAndEmpty
    private String idno;
    @CheckNullAndEmpty
    private String birthday;
    private String account;
    private String url;
    private String uniqId;
    private String uniqType;
    private String ipAddress;
    private String entry;
    private String userAgent;
}