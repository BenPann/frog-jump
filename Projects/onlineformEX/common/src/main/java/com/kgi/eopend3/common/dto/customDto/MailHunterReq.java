package com.kgi.eopend3.common.dto.customDto;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MailHunterReq {

    @CheckNullAndEmpty
    private String projectCode;
    @CheckNullAndEmpty(checkEmpty = "N")
    private String chtName = "";
    @CheckNullAndEmpty
    private String eMailAddress = "";
    @CheckNullAndEmpty
    private String idno = "";
    @CheckNullAndEmpty
    private String templateId = "";
    @CheckNullAndEmpty(checkEmpty = "N")
    private String filename = "";
    @CheckNullAndEmpty
    private String ownerID = "";
    @CheckNullAndEmpty
    private String productCode = "";
    private String[] titleAndContents;    
}
