package com.kgi.onlineformex.common;

public class OFEXContractConst {

    /** PDF-API URL */
    public static final String API_HTML2PDF = "api/v1/htmlToPdf";
    /** 立約產品代號 - 信貸 */
    public final static String CONTRACT_PRODUCT_ID_TYPE_LOAN = "1";
    /** 立約產品代號 - 現金卡 */
    public final static String CONTRACT_PRODUCT_ID_TYPE_CASH = "2";
    /** 立約產品代號 - 一般e貸寶 */
    public final static String CONTRACT_PRODUCT_ID_TYPE_ELOAN = "3";


    /**案件狀態 -init*/
	public final static String CASE_STATUS_INIT = "00";
	/**案件狀態 -填寫中*/
	public final static String CASE_STATUS_WRITING = "01";
	/**案件狀態 -填寫完成*/
	public final static String CASE_STATUS_WRITE_SUCCESS = "02";
	/**案件狀態 -填寫完成*/
	public final static String CASE_STATUS_CREATE_SUCCESS = "03";
	/**案件狀態 -30天自動結案*/
	public final static String CASE_STATUS_SUSPENDBY30DAYS = "06";
	/**案件狀態 -斷點取消*/
	public final static String CASE_STATUS_CANCEL = "07";
	/**案件狀態 -拒貸*/
	public final static String CASE_STATUS_REJECT = "08";
	/**案件狀態 -拒貸已送件*/
	public final static String CASE_STATUS_REJECT_SENDED = "09";
	/**案件狀態 -送件中*/
	public final static String CASE_STATUS_SUBMITTING = "11";
	/**案件狀態 -送件完成*/
	public final static String CASE_STATUS_SUBMIT_SUCCESS = "12";
	/**案件狀態 -無對應之信用卡產品*/
	public final static String CASE_STATUS_PRODUCT_ERROR = "17";
	/**案件狀態 -送件失敗*/
	public final static String CASE_STATUS_SUBMIT_FAIL = "18";
	/**案件狀態 -查無申請書無法送件*/
	public final static String CASE_STATUS_SUBMIT_ERROR = "19";
}