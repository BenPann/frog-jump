package com.kgi.mgm.common.dto.view;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class MgmCustomerLogin {
    public String CHANNEL_ID;
    public String CUST_ID;
    public String PASSWORD;
    public String EMP_ID;
    public String DEPART_ID;
}
