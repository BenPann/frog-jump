package com.kgi.eopend3.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	public static final String YYYYMMDD = "yyyyMMdd" ;
	
	
    /** 取得格式化後的日期 (預設 yyyyMMddHHmmss) */
    public static String GetDateFormatString() {
        return GetDateFormatString("yyyyMMddHHmmss");
    }

    /**
     * 格式化8碼日期
     * */
    public static String addDateSlash(String date) {
    	return addDateSlash(date, "/") ;
    }
    public static String addDateSlash(String date, String concat) {
    	if (date == null || date.length() != 8) {
    		return date ;
    	}
    	
    	return new StringBuffer(date.substring(0, 4)).append(concat).append(date.substring(4, 6)).append(concat).append(date.substring(6, 8)).toString() ;
    	
    }

    /**
     * 移除日期分隔號
     * */
    public static String removeDateSlash(String date) {
    	if (date == null || date.length() != 10) {
    		return date ;
    	}
    	return date.substring(0, 4) + date.substring(5, 7) + date.substring(8, 10) ;
    	
    }

    /**
     * 取得格式化後的日期
     * 
     * @param format 日期格式字串
     */
    public static String GetDateFormatString(String format) {
        return GetDateFormatString(new Date(), format);
    }

    public static String GetDateFormatString(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    public static String GetDateFotmatString(Calendar cal, String format) {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(cal.getTime());
    }

    
    
    public static void main(String argc[]) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);
    	System.out.println(DateUtil.GetDateFotmatString(cal, "yyyyMMdd"));
    	
        cal.add(Calendar.DATE, -7);
    	System.out.println(DateUtil.GetDateFotmatString(cal, "yyyyMMdd"));
    	
    }
    
    
}
