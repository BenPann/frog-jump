package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BrowseLog {

    String UniqId = "";
    String UniqType = "";
    String Page = "";
    String Action = "";
    
}
