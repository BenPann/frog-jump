package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class EntryData {
    @NonNull
    String uniqId;
    @NonNull
    String uniqType;
    @NonNull
    String entry;
    String member = "";
    @NonNull
    String process;
    @NonNull
    String browser;
    @NonNull
    String platform;
    @NonNull
    String os;
    String other = "";
    String utime;
}
