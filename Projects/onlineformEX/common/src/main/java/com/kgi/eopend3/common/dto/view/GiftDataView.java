package com.kgi.eopend3.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class GiftDataView {

    @CheckNullAndEmpty(checkEmpty = "N")
    private String firstPresent = "";

}