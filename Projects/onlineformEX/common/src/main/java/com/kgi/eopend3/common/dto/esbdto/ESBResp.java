package com.kgi.eopend3.common.dto.esbdto;

import com.google.gson.GsonBuilder;

public class ESBResp {
	
	private String statusCode = "" ;
	private String statusDesc = "" ;
	private String severity = "" ;
	
	private String response = "" ;
	
	private String xml = "" ;

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	
	
	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	} 

	
	public String toJsonString() {
		return new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(this) ;
	}
	
}
