package com.kgi.eopend3.common.dto.response;

public class AumAddCLAumInfoResp {

    private String CHECK_CODE = "";    
    private String ERR_MSG = "";
    
    
	public String getCHECK_CODE() {
		return CHECK_CODE;
	}
	public void setCHECK_CODE(String cHECK_CODE) {
		CHECK_CODE = cHECK_CODE;
	}
	public String getERR_MSG() {
		return ERR_MSG;
	}
	public void setERR_MSG(String eRR_MSG) {
		ERR_MSG = eRR_MSG;
	}
    
    
    
}