package com.kgi.eopend3.common.dto.db;

public class ED3_CLCaseSts {

	private int Serial ;
	private String UniqId;
	private String PdctSts = "";
	private String CLStatus = "";
	private String CLTime ;
	private String CreateTime ;
	private String UpdateTime ;

	// ED3_CLPolicyInfo
	private String ApplNo ;
	private String EffDt ;

	//ED3_CLCif
	private String Token ;

	// ED3_CaseData
	private String Idno ;

	public int getSerial() {
		return Serial;
	}
	public void setSerial(int serial) {
		Serial = serial;
	}
	public String getUniqId() {
		return UniqId;
	}
	public void setUniqId(String uniqId) {
		UniqId = uniqId;
	}
	public String getPdctSts() {
		return PdctSts;
	}
	public void setPdctSts(String pdctSts) {
		PdctSts = pdctSts;
	}
	public String getCLStatus() {
		return CLStatus;
	}
	public void setCLStatus(String cLStatus) {
		CLStatus = cLStatus;
	}
	public String getCLTime() {
		return CLTime;
	}
	public void setCLTime(String cLTime) {
		CLTime = cLTime;
	}
	public String getCreateTime() {
		return CreateTime;
	}
	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}
	public String getUpdateTime() {
		return UpdateTime;
	}
	public void setUpdateTime(String updateTime) {
		UpdateTime = updateTime;
	}
	public String getApplNo() {
		return ApplNo;
	}
	public void setApplNo(String applNo) {
		this.ApplNo = applNo;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		this.Token = token;
	}
	public String getEffDt() {
		return EffDt;
	}
	public void setEffDt(String effDt) {
		EffDt = effDt;
	}
	public String getIdno() {
		return Idno;
	}
	public void setIdno(String idno) {
		Idno = idno;
	}

	
	
}