package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class QR_PProductTypeProductMatch {
    public String PProductType;
    public String PProductId;
}
