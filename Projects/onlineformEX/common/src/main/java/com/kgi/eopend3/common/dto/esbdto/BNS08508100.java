package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BNS085105", propOrder = { "IDType", "IDNo", "CustName", "CustNo", "PhoneNoRegister", "VipCode", "Description", "OfficerBranch", "RelMgr", "AoMgrGr", "CorpBelUnit", "IndustryCode", "BirthDate1", "CBFXIDTyp", "CBFXIDNo", "IDEffectDate", "IDExpiryDate", "RemitterPayeeTyp", "NationalityCode", "MobileNo", "MobileNo2", "RegAddress", "Address", "EmailAddress", "AnnotatFlags", "FatcaType", "USIndex", "StatPostMethod", "CreateDt", "detail" })
public class BNS08508100 { 

    @XmlElement(name="IDType")
    private String IDType;
    @XmlElement(name="IDNo")
    private String IDNo;
    @XmlElement(name="CustName")
    private String CustName;
    @XmlElement(name="CustNo")
    private String CustNo;
    @XmlElement(name="PhoneNoRegister")
    private String PhoneNoRegister;
    @XmlElement(name="VipCode")
    private String VipCode;
    @XmlElement(name="Description")
    private String Description;
    @XmlElement(name="OfficerBranch")
    private String OfficerBranch;
    @XmlElement(name="RelMgr")
    private String RelMgr;
    @XmlElement(name="AoMgrGr")
    private String AoMgrGr;
    @XmlElement(name="CorpBelUnit")
    private String CorpBelUnit;
    @XmlElement(name="IndustryCode")
    private String IndustryCode;
    @XmlElement(name="BirthDate1")
    private String BirthDate1;
    @XmlElement(name="CBFXIDTyp")
    private String CBFXIDTyp;
    @XmlElement(name="CBFXIDNo")
    private String CBFXIDNo;
    @XmlElement(name="IDEffectDate")
    private String IDEffectDate;
    @XmlElement(name="IDExpiryDate")
    private String IDExpiryDate;
    @XmlElement(name="RemitterPayeeTyp")
    private String RemitterPayeeTyp;
    @XmlElement(name="NationalityCode")
    private String NationalityCode;
    @XmlElement(name="MobileNo")
    private String MobileNo;
    @XmlElement(name="MobileNo2")
    private String MobileNo2;
    @XmlElement(name="RegAddress")
    private String RegAddress;
    @XmlElement(name="Address")
    private String Address;
    @XmlElement(name="EmailAddress")
    private String EmailAddress;
    @XmlElement(name="AnnotatFlags")
    private String AnnotatFlags;
    @XmlElement(name="FatcaType")
    private String FatcaType;
    @XmlElement(name="USIndex")
    private String USIndex;
    @XmlElement(name="StatPostMethod")
    private String StatPostMethod;
    @XmlElement(name="CreateDt")
    private String CreateDt;
    @XmlElement(name = "Detail")
    private List<BNS08508100Detail> detail;

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String iDType) {
        IDType = iDType;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String iDNo) {
        IDNo = iDNo;
    }

    public String getCustName() {
        return CustName;
    }

    public void setCustName(String custName) {
        CustName = custName;
    }

    public String getCustNo() {
        return CustNo;
    }

    public void setCustNo(String custNo) {
        CustNo = custNo;
    }

    public String getPhoneNoRegister() {
        return PhoneNoRegister;
    }

    public void setPhoneNoRegister(String phoneNoRegister) {
        PhoneNoRegister = phoneNoRegister;
    }

    public String getVipCode() {
        return VipCode;
    }

    public void setVipCode(String vipCode) {
        VipCode = vipCode;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getOfficerBranch() {
        return OfficerBranch;
    }

    public void setOfficerBranch(String officerBranch) {
        OfficerBranch = officerBranch;
    }

    public String getRelMgr() {
        return RelMgr;
    }

    public void setRelMgr(String relMgr) {
        RelMgr = relMgr;
    }

    public String getAoMgrGr() {
        return AoMgrGr;
    }

    public void setAoMgrGr(String aoMgrGr) {
        AoMgrGr = aoMgrGr;
    }

    public String getCorpBelUnit() {
        return CorpBelUnit;
    }

    public void setCorpBelUnit(String corpBelUnit) {
        CorpBelUnit = corpBelUnit;
    }

    public String getIndustryCode() {
        return IndustryCode;
    }

    public void setIndustryCode(String industryCode) {
        IndustryCode = industryCode;
    }

    public String getBirthDate1() {
        return BirthDate1;
    }

    public void setBirthDate1(String birthDate1) {
        BirthDate1 = birthDate1;
    }

    public String getCBFXIDTyp() {
        return CBFXIDTyp;
    }

    public void setCBFXIDTyp(String cBFXIDTyp) {
        CBFXIDTyp = cBFXIDTyp;
    }

    public String getCBFXIDNo() {
        return CBFXIDNo;
    }

    public void setCBFXIDNo(String cBFXIDNo) {
        CBFXIDNo = cBFXIDNo;
    }

    public String getIDEffectDate() {
        return IDEffectDate;
    }

    public void setIDEffectDate(String iDEffectDate) {
        IDEffectDate = iDEffectDate;
    }

    public String getIDExpiryDate() {
        return IDExpiryDate;
    }

    public void setIDExpiryDate(String iDExpiryDate) {
        IDExpiryDate = iDExpiryDate;
    }

    public String getRemitterPayeeTyp() {
        return RemitterPayeeTyp;
    }

    public void setRemitterPayeeTyp(String remitterPayeeTyp) {
        RemitterPayeeTyp = remitterPayeeTyp;
    }

    public String getNationalityCode() {
        return NationalityCode;
    }

    public void setNationalityCode(String nationalityCode) {
        NationalityCode = nationalityCode;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getMobileNo2() {
        return MobileNo2;
    }

    public void setMobileNo2(String mobileNo2) {
        MobileNo2 = mobileNo2;
    }

    public String getRegAddress() {
        return RegAddress;
    }

    public void setRegAddress(String regAddress) {
        RegAddress = regAddress;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }

    public String getAnnotatFlags() {
        return AnnotatFlags;
    }

    public void setAnnotatFlags(String annotatFlags) {
        AnnotatFlags = annotatFlags;
    }

    public String getFatcaType() {
        return FatcaType;
    }

    public void setFatcaType(String fatcaType) {
        FatcaType = fatcaType;
    }

    public String getUSIndex() {
        return USIndex;
    }

    public void setUSIndex(String uSIndex) {
        USIndex = uSIndex;
    }

    public String getStatPostMethod() {
        return StatPostMethod;
    }

    public void setStatPostMethod(String statPostMethod) {
        StatPostMethod = statPostMethod;
    }

    public String getCreateDt() {
        return CreateDt;
    }

    public void setCreateDt(String createDt) {
        CreateDt = createDt;
    }

    public List<BNS08508100Detail> getDetail() {
        return detail;
    }

    public void setDetail(List<BNS08508100Detail> detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "BNS08508100 [Address=" + Address + ", AnnotatFlags=" + AnnotatFlags + ", AoMgrGr=" + AoMgrGr
                + ", BirthDate1=" + BirthDate1 + ", CBFXIDNo=" + CBFXIDNo + ", CBFXIDTyp=" + CBFXIDTyp
                + ", CorpBelUnit=" + CorpBelUnit + ", CreateDt=" + CreateDt + ", CustName=" + CustName + ", CustNo="
                + CustNo + ", Description=" + Description + ", EmailAddress=" + EmailAddress + ", FatcaType="
                + FatcaType + ", IDEffectDate=" + IDEffectDate + ", IDExpiryDate=" + IDExpiryDate + ", IDNo=" + IDNo
                + ", IDType=" + IDType + ", IndustryCode=" + IndustryCode + ", MobileNo=" + MobileNo + ", MobileNo2="
                + MobileNo2 + ", NationalityCode=" + NationalityCode + ", OfficerBranch=" + OfficerBranch
                + ", PhoneNoRegister=" + PhoneNoRegister + ", RegAddress=" + RegAddress + ", RelMgr=" + RelMgr
                + ", RemitterPayeeTyp=" + RemitterPayeeTyp + ", StatPostMethod=" + StatPostMethod + ", USIndex="
                + USIndex + ", VipCode=" + VipCode + ", detail=" + detail + "]";
    }
    

}
