package com.kgi.onlineformex.common.dto.db;

import com.kgi.onlineformex.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class FM_CaseDoc {
    @NonNull
    @CheckNullAndEmpty
    private String CaseNo;
    private String idno;
    private String PdfContent;
    private String Image;
}