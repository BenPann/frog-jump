package com.kgi.eopend3.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CIFInfoResDto {
    private String CHECK_CODE = "";
    private String ERR_MSG = "";
    private String CUST_ID = "";
    private String NAME = "";
    private String EML_ADDR = "";
    private String MOBILE_TEL = "";
    private String ADR_ZIP = "";
    private String ADR = "";
    private String TEL_NO_AREACODE = "";
    private String TEL_NO = "";
    private String EDU = "";
    private String MARRIAGE = "";
    private String COMPANY_NAME = "";
    private String COMPANY_ADDR_ZIP = "";
    private String COMPANY_ADDR = "";
    private String COMPANY_TEL_AREACODE = "";
    private String COMPANY_TEL = "";
    private String OCCUPATION = "";
    private String ON_BOARD_DATE = "";
    private String INCOME = "";
    private String HOUSE_OWNER = "";
}
