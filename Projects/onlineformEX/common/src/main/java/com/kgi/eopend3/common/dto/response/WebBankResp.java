package com.kgi.eopend3.common.dto.response;

public class WebBankResp {

    private String hasWebBank = "";
    private String hasIvrBank = "";

    public String getHasWebBank() {
        return hasWebBank;
    }

    public void setHasWebBank(String hasWebBank) {
        this.hasWebBank = hasWebBank;
    }

    public String getHasIvrBank() {
        return hasIvrBank;
    }

    public void setHasIvrBank(String hasIvrBank) {
        this.hasIvrBank = hasIvrBank;
    }
           
}