package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class MailTemplate {

    @NonNull    
    private String Type;    
    private String TagName;
    private String Content;   

}
