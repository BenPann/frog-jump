package com.kgi.eopend3.common.dto.esbdto;

import java.util.ArrayList;
import java.util.List;

import com.kgi.eopend3.common.util.XmlUtil;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class BNS06042509Q {
	
	List<BNS06042509QDetail> detail ;
	
	
	
	
	public List<BNS06042509QDetail> getDetail() {
		return detail;
	}
	public void setDetail(List<BNS06042509QDetail> detail) {
		this.detail = detail;
	}

	public List<BNS06042509QDetail> parseXml(String xml) {
		List<BNS06042509QDetail> details = null ;
		try {
    	NodeList nodeList = XmlUtil.getXmlNodes(xml, "Detail") ;
    	
    	details = new ArrayList<BNS06042509QDetail>(); ;
    	if (nodeList != null && nodeList.getLength() > 0) {
    		for (int cnt=0; cnt< nodeList.getLength(); cnt++) {
    			Node node = nodeList.item(cnt) ;
				 if (node.getNodeType() == Node.ELEMENT_NODE) {
				    Element ele = (Element) node;
				    BNS06042509QDetail detail = new BNS06042509QDetail() ;
				    detail.setRCD(XmlUtil.getNodeValue(ele, "RCD"));
				    detail.setRM(XmlUtil.getNodeValue(ele, "RM"));
				    detail.setIncidentType(XmlUtil.getNodeValue(ele, "IncidentType"));
				    
				    details.add(detail) ;
				 }
    		}
    	} 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
    	return details;
	}


	public class BNS06042509QDetail {
		private String RCD ;
		private String ReasonDesc ;
		private String RM ;
		private String IncidentType ;
		public String getRCD() {
			return RCD;
		}
		public void setRCD(String rCD) {
			RCD = rCD;
		}
		public String getReasonDesc() {
			return ReasonDesc;
		}
		public void setReasonDesc(String reasonDesc) {
			ReasonDesc = reasonDesc;
		}
		public String getRM() {
			return RM;
		}
		public void setRM(String rM) {
			RM = rM;
		}
		public String getIncidentType() {
			return IncidentType;
		}
		public void setIncidentType(String incidentType) {
			IncidentType = incidentType;
		}
		
	}
}
