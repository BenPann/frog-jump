package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SvcRs")
public class BNS00040007SvcRs {

    @XmlElement(name="BNS032041")
    private BNS00040007 bns00040007;

    public BNS00040007 getBns00040007() {
        return bns00040007;
    }

    public void setBns00040007(BNS00040007 bns00040007) {
        this.bns00040007 = bns00040007;
    }

    @Override
    public String toString() {
        return "BNS00040007SvcRs [bns00040007=" + bns00040007 + "]";
    }

        
}