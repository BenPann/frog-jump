package com.kgi.eopend3.common.dto.customDto;

import com.google.gson.GsonBuilder;

public class ChinalifeCaseStsRs {

	private String msgId = "";
	private String stsCode = "";
	private String stsDesc = "";
	
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getStsCode() {
		return stsCode;
	}
	public void setStsCode(String stsCode) {
		this.stsCode = stsCode;
	}
	public String getStsDesc() {
		return stsDesc;
	}
	public void setStsDesc(String stsDesc) {
		this.stsDesc = stsDesc;
	}


	public String toJsonString() {
		return new GsonBuilder().disableHtmlEscaping().create().toJson(this) ;
	}
	
	
}
