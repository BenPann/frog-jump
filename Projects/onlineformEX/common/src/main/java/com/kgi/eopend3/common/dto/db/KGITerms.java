package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KGITerms {
    
    private String name = "";
    private String page = "";
    private String type = "";
    private String sort = "";
  
}