package com.kgi.eopend3.common.dto.view;

public class PageView {

    private String pageName = "";    

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }           
}