package com.kgi.eopend3.common.dto.db;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
public class QR_ShortUrlCaseMatch {

	private String ShortUrl;
	@NonNull
	private String UniqId; 
    private String UniqType;
        
 }