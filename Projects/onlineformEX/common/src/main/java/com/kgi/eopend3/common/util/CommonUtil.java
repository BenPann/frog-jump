package com.kgi.eopend3.common.util;

import java.util.Arrays;
import java.util.List;

public class CommonUtil {

    public static String addressHandler(String addr, String spliter) {
        int times = 0;
        StringBuilder sb = new StringBuilder();
        String[] split = addr.split("");
        List<String> strings = Arrays.asList(split);
        for (String s : strings) {
            if (times < 2) {
                switch (s) {
                case "縣":
                case "市":
                case "區":
                case "鄉":
                case "鎮":
                    sb.append(s).append(spliter);
                    times++;
                    break;
                default:
                    sb.append(s);
                }

            } else {
                sb.append(s);
            }
        }
        return sb.toString();
    }

    public static String addressHandler(String addr) {
        return addressHandler(addr, ";");
    }

    public static String hiddenUserDataWithStar(String str, int beginIndex, int length) {

        return hiddenUserData(str, beginIndex, length, "*");
    }

    public static String hiddenUserData(String str, int beginIndex, int length, String hiddenSymbol) {
        int endIndex = beginIndex + length;
        String newStr = "";
        if (str.length() >= endIndex && beginIndex >= 0) {
            String star = "";
            for (int i = 0; i < length; i++) {
                star += hiddenSymbol;
            }
            newStr = str.substring(0, beginIndex) + star + str.substring(endIndex, str.length());
        }
        return newStr;
    }

    public static String getGenderFromId(String idno) {
        if (idno == null || idno.equals("")) {
            return "";
        }
        String genderNum = idno.substring(1, 2);
        String gender = "";
        switch (genderNum) {
        case "1":
            gender = "男";
            break;
        case "2":
            gender = "女";
        }
        return gender;
    }

    // public static String handleJsonStr(JSONObject json, String name) {
    //     return handleJsonStr(json, name, "");
    // }

    // public static String handleJsonStr(JSONObject json, String name, String defaultValue) {
    //     String r;
    //     try {
    //         r = json.optString(name, defaultValue);
    //         if (r == null || r.equals("null")) {
    //             r = defaultValue;
    //         }
    //     } catch (Exception e) {
    //         e.printStackTrace();
    //         r = defaultValue;
    //     }
    //     return r;
    // }

    public static boolean StringIsNullOrWhiteSpace(String... arg) {
        for (String s : arg) {
            if (s == null)
                return true;
            if (s.trim().equals(""))
                return true;
        }
        return false;
    }

    public static boolean StringIsNull(String... arg) {
        for (String s : arg) {
            if (s == null)
                return true;
        }
        return false;
    }

}
