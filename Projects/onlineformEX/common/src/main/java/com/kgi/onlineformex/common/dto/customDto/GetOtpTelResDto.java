package com.kgi.onlineformex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetOtpTelResDto {
    /** 
     * (1)	CHECK_CODE：
     * A.	『0』檢核資料錯誤(空白代表沒有錯誤)
     * B.	『10』代表查無資料
     * C.	『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
     */
    private String CHECK_CODE = "";
    
    /**
     * (2)	ERR_MSG：
     * 當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
     */
    private String ERR_MSG = "";

    /** 
     * (3)	OTP_TEL：
     * 電話號碼:VARCHAR(20)
     */
    private String OTP_TEL = "";

    /**
     * (4)	OTP_SOURCE：
     * 電話號碼來原系統:CHAR(1)
     * 代碼		說明
     * ============================
     * 1	 	台幣(存款)
     * 2	 	信用卡
     * 3		現金卡
     * 4		台幣(純放款戶)
     */
    private String OTP_SOURCE = "";
}