package com.kgi.mgm.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResetPassword {
    public String CUST_ID;
    public String CUST_PW;
}
