package com.kgi.mgm.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryConfig {
    public String keyName;
}
