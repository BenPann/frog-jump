package com.kgi.eopend3.common.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CIFInfoCCResp {
    
    private String sourceType;
    private String chtName;
    private String engName;
    private String resAddrZipCode;    
    private String resAddr;    
    private String commAddrZipCode;    
    private String commAddr;    
    private String marriage;
    private String email;
    private String resTelArea;
    private String resTel;
    private String homeTelArea;
    private String homeTel;
    private String estateType;   
    private String corpName; 
    private String corpTelArea; 
    private String corpTel; 
    private String corpAddrZipCode; 
    private String corpAddr; 
    private String yearlyIncome; 
    private String onBoardDate;

}