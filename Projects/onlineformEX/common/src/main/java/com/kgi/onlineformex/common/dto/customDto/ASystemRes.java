package com.kgi.onlineformex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ASystemRes {
    private String funcType = "";   // I:查詢  U:簽署
    private String userType = "";   // 1：專案調額客戶 2：自行申請調額客戶
    private String caseType = "";   // 1：契變書簽署 2：調額同意書簽署 3：補財力證明及調額同意書簽署 4：契變書簽署及調額同意書簽署 5：補財力證明及契變書簽署及調額同意書千署
    private String customerId = ""; // 身份證字號  左靠右空白 
    private String manageStore = "";// 管理店
    private String companyNum = ""; // 固定為1
    private String departNum = "";
    private String memberNum = "";
    private String seq = "";
    private String userName = "";   // 左靠右空白
    private String cardNum = "";    // 左靠右空白
    private String projectId = "";
    private String creditCur = "";
    private String creditContract = "";
    private String creditModify = "";
    private String dateModify = ""; // 西元年 YYYYMMDD               
    private String email = "";      // 左靠右空白 
    private String phone = "";      // 左靠右空白(半型)
    private String SO2419 = "";     // 左靠右空白(半型)
}

