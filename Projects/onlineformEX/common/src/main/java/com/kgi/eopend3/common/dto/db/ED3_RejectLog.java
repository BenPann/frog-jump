package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class ED3_RejectLog {

    private String uniqId;
    @NonNull
    private String Idno;
    @NonNull
    private String IpAddress;    
    private String Phone;   
    private String EmailAddress;
    private String RejectCode;
    private String CreateTime;
    private String AccountType;
    private String SubCategory;
   
}