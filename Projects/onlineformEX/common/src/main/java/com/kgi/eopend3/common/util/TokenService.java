package com.kgi.eopend3.common.util;

import java.util.Date;

import com.kgi.eopend3.common.SystemConst;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jwt.EncryptedJWT;
import com.nimbusds.jwt.JWTClaimsSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TokenService {
    

    protected static Logger logger = LoggerFactory.getLogger(TokenService.class);

    public static String addAuthentication(String json) {

        //System.out.println(principal);
        // 產生JWT
        String jwt = "";
        try {

            // Create the header
            JWEHeader header = new JWEHeader(JWEAlgorithm.DIR, EncryptionMethod.A128GCM);

            JWTClaimsSet.Builder claimsSet = new JWTClaimsSet.Builder();
            
            claimsSet.subject(json);

            claimsSet.expirationTime(new Date(System.currentTimeMillis() + SystemConst.EXPIRATIONTIME));
            // claimsSet.notBeforeTime(new Date());

            // create the EncryptedJWT object
            EncryptedJWT jweObject = new EncryptedJWT(header, claimsSet.build());

            jweObject.encrypt(new DirectEncrypter(SystemConst.key));

            // Serialise to compact JOSE form...
            jwt = jweObject.serialize();
//            System.out.println(jwt);
            return jwt;

        } catch (Exception e) {
            logger.error("未知錯誤", e);
            return jwt;
        }

    }

}