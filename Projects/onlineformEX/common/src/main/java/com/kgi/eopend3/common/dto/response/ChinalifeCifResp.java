package com.kgi.eopend3.common.dto.response;

import com.google.gson.GsonBuilder;

public class ChinalifeCifResp {

	private String msgId = "";
	private String stsCode = "";
	private String stsDesc = "";
	private String token = "";
	private String tokenEndDt = "";
	private String chtName = "";
	private String pyName = "";
	private String engName = "";
	private String email = "";
	private String marriage = "";
	private String mobNo = "";
	private String homeTelArea = "" ;
	private String homeTel = "";
	private String resAddrZipCode = "";
	private String resAddrArea = "";
	private String resAddr = "";
	private String comAddrZipCode = "";
	private String comAddrArea = "";
	private String comAddr = "";
	private String tokenExp="";

	public String getMsgId() {
		return msgId;
	}



	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}



	public String getStsCode() {
		return stsCode;
	}



	public void setStsCode(String stsCode) {
		this.stsCode = stsCode;
	}



	public String getStsDesc() {
		return stsDesc;
	}



	public void setStsDesc(String stsDesc) {
		this.stsDesc = stsDesc;
	}



	public String getToken() {
		return token;
	}



	public void setToken(String token) {
		this.token = token;
	}



	public String getTokenEndDt() {
		return tokenEndDt;
	}



	public void setTokenEndDt(String tokenEndDt) {
		this.tokenEndDt = tokenEndDt;
	}



	public String getChtName() {
		return chtName;
	}



	public void setChtName(String chtName) {
		this.chtName = chtName;
	}



	public String getPyName() {
		return pyName;
	}



	public void setPyName(String pyName) {
		this.pyName = pyName;
	}



	public String getEngName() {
		return engName;
	}



	public void setEngName(String engName) {
		this.engName = engName;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getMarriage() {
		return marriage;
	}



	public void setMarriage(String marriage) {
		this.marriage = marriage;
	}



	public String getMobNo() {
		return mobNo;
	}



	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}



	public String getHomeTelArea() {
		return homeTelArea;
	}



	public void setHomeTelArea(String homeTelArea) {
		this.homeTelArea = homeTelArea;
	}



	public String getHomeTel() {
		return homeTel;
	}



	public void setHomeTel(String homeTel) {
		this.homeTel = homeTel;
	}



	public String getResAddr() {
		return resAddr;
	}



	public void setResAddr(String resAddr) {
		this.resAddr = resAddr;
	}



	public String getComAddr() {
		return comAddr;
	}



	public void setComAddr(String comAddr) {
		this.comAddr = comAddr;
	}



	public String getResAddrZipCode() {
		return resAddrZipCode;
	}



	public void setResAddrZipCode(String resAddrZipCode) {
		this.resAddrZipCode = resAddrZipCode;
	}



	public String getResAddrArea() {
		return resAddrArea;
	}



	public void setResAddrArea(String resAddrArea) {
		this.resAddrArea = resAddrArea;
	}



	public String getComAddrZipCode() {
		return comAddrZipCode;
	}



	public void setComAddrZipCode(String comAddrZipCode) {
		this.comAddrZipCode = comAddrZipCode;
	}



	public String getComAddrArea() {
		return comAddrArea;
	}



	public void setComAddrArea(String comAddrArea) {
		this.comAddrArea = comAddrArea;
	}



	public String getTokenExp() {
		return tokenExp;
	}



	public void setTokenExp(String tokenExp) {
		this.tokenExp = tokenExp;
	}



	public String toJsonString() {
		return new GsonBuilder().disableHtmlEscaping().create().toJson(this) ;
	}

}
