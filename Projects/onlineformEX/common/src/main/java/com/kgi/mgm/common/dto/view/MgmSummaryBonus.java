package com.kgi.mgm.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MgmSummaryBonus {

    private String checkCode;
    private String errMsg;
    private MgmSummaryBonusData[] data;
}
