package com.kgi.onlineformex.common.dto.view;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CreditModifyTermView {
    private List<String> creditModifyTerm;
}