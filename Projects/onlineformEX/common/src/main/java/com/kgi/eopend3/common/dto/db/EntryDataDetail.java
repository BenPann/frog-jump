package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class EntryDataDetail {
    String UniqId;
    String UserAgent;
}