package com.kgi.eopend3.common.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRes {
    private String uniqId = "";
    private String uniqType = "";
    private String idno = "";
    private String ipAddress = "";
}