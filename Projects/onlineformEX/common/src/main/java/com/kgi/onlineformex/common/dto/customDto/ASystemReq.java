package com.kgi.onlineformex.common.dto.customDto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class ASystemReq {
    private String funcType = "";     // I：查詢    U：簽署
    private String docType = "";     // 功能種類 = I 時：為空白 功能種類 = U 時：1-簽署契變書   2-簽署調額同意書
    private String userId = "";     // 身份證字號  左靠右空白 (若ID、帳號擇一輸入，但若皆有值時，兩者需為同一客戶)
    private String account = "";     // 帳號或卡號
    private String shortUrl = "";     // 機號或網址
    private String creditModify = "";     // 希望調整之契約額度
}
