package com.kgi.eopend3.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BNS08508100Resp {

    private String AcctType = "";
    private String KgiDeposit = "";
    private String IntCat = "" ;

 }
