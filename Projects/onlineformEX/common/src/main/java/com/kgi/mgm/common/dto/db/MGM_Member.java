package com.kgi.mgm.common.dto.db;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class MGM_Member {
    private String CHANNEL_ID;
    private String CUST_ID;
    private String CUST_NAME;
    private String CUST_PHONE;
    private String CUST_EMAIL;
    private String CUST_BIRTH_4;
    private String CUST_REGISTER_FLAG;
    private String CUST_HAS_SET_PW;
    private String CUST_UNIT_ID;
    private String CUST_EMP_ID;
    private String CUST_PW;
    private String IMPORT_TIME;
    private String REGISTER_TIME;
    private String PW_SETTING_TIME;
    private String STATUS;
}














