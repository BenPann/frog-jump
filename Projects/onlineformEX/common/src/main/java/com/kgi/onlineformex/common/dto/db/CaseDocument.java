package com.kgi.onlineformex.common.dto.db;

public class CaseDocument {
    String UniqId = "";
    String UniqType = "";
    String DocumentType = "";
    byte[] Content = null;
    String Version = "";
    String Other = "";

    public CaseDocument() {
    }

    public CaseDocument(String uniqId, String uniqType, String documentType) {
        this.setUniqId(uniqId);
        this.setUniqType(uniqType);
        this.setDocumentType(documentType);
    }

    public String getUniqId() {
        return UniqId;
    }

    public void setUniqId(String uniqId) {
        UniqId = uniqId;
    }

    public String getUniqType() {
        return UniqType;
    }

    public void setUniqType(String uniqType) {
        UniqType = uniqType;
    }

    public String getDocumentType() {
        return DocumentType;
    }

    public void setDocumentType(String documentType) {
        DocumentType = documentType;
    }

    public byte[] getContent() {
        return Content;
    }

    public void setContent(byte[] content) {
        Content = content;
    }

    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public String getOther() {
        return Other;
    }

    public void setOther(String other) {
        Other = other;
    }
}
