package com.kgi.eopend3.common.dto.customDto;

import com.google.gson.GsonBuilder;

public class ChinalifeCifRq {

	private String msgId = "";
	private String token = "";
	private String qryType = "";
	private String idNo = "";
	private String finInd = "";
	private String caseId = "";

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getQryType() {
		return qryType;
	}

	public void setQryType(String qryType) {
		this.qryType = qryType;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	public String getFinInd() {
		return finInd;
	}

	public void setFinInd(String finInd) {
		this.finInd = finInd;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public String toJsonString() {
		return new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(this) ;
	}
}
