package com.kgi.eopend3.common.dto.response;

public class VerifyCountResp {

    private Integer errorCount;
    private Integer ncccErrorCount;
    private Integer pcode2566ErrorCount;

    public Integer getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(Integer errorCount) {
        this.errorCount = errorCount;
    }

    public Integer getNcccErrorCount() {
        return ncccErrorCount;
    }

    public void setNcccErrorCount(Integer ncccErrorCount) {
        this.ncccErrorCount = ncccErrorCount;
    }

    public Integer getPcode2566ErrorCount() {
        return pcode2566ErrorCount;
    }

    public void setPcode2566ErrorCount(Integer pcode2566ErrorCount) {
        this.pcode2566ErrorCount = pcode2566ErrorCount;
    }
}