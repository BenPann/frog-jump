package com.kgi.eopend3.common.dto.db;

public class ED3_CLPolicyInfo {

	private int Serial ;
	private String MsgId;
	private String UniqId;
	private String Token;
	private String ActionType ;
	private String ApplNo;
	private String EffDt;
	private int CashValue = 0;
	private int LoanAmt = 0;
	private int RePremiu = 0;
	private String RePmtMtd ;
	private String OriPmtMtd;
	private String PolicyImage;
	private String CalDate;
	private String QryDate;
	private String ApplSts;
	private String ApplStsUpdDate;
	private String MsgDate;
	private String PolicySts;
	private String PolicyStsUpdDate;

	private String AumStatus;
	private String CreateTime ;

	private String PayerStatus;
	private String UpdateTime ;
	private String PayerTime ;

	// ED3_CLCaseSts
	private String PdctSts;
	private String CLStatus ;
	private String CLTime ;

	private byte[] bPolicyImage ;

	// ED3_CaseData
	private String Idno ;
	private String Phone ;
	private String ChtName ;
	private String EmailAddress ;

	// ED3_AOResult
	private String SalaryAccountTw ;
	
	
	public int getSerial() {
		return Serial;
	}
	public void setSerial(int serial) {
		Serial = serial;
	}
	public String getMsgId() {
		return MsgId;
	}
	public void setMsgId(String msgId) {
		MsgId = msgId;
	}
	public String getActionType() {
		return ActionType;
	}
	public void setActionType(String actionType) {
		ActionType = actionType;
	}
	public String getApplNo() {
		return ApplNo;
	}
	public void setApplNo(String applNo) {
		ApplNo = applNo;
	}
	public String getEffDt() {
		return EffDt;
	}
	public void setEffDt(String effDt) {
		EffDt = effDt;
	}
	public int getCashValue() {
		return CashValue;
	}
	public void setCashValue(int cashValue) {
		CashValue = cashValue;
	}
	public int getLoanAmt() {
		return LoanAmt;
	}
	public void setLoanAmt(int loanAmt) {
		LoanAmt = loanAmt;
	}
	public int getRePremiu() {
		return RePremiu;
	}
	public void setRePremiu(int rePremiu) {
		RePremiu = rePremiu;
	}
	public String getRePmtMtd() {
		return RePmtMtd;
	}
	public void setRePmtMtd(String rePmtMtd) {
		RePmtMtd = rePmtMtd;
	}
	public String getOriPmtMtd() {
		return OriPmtMtd;
	}
	public void setOriPmtMtd(String oriPmtMtd) {
		OriPmtMtd = oriPmtMtd;
	}
	public String getPolicyImage() {
		return PolicyImage;
	}
	public void setPolicyImage(String policyImage) {
		PolicyImage = policyImage;
	}
	public byte[] getbPolicyImage() {
		return bPolicyImage;
	}
	public void setbPolicyImage(byte[] bPolicyImage) {
		this.bPolicyImage = bPolicyImage;
	}
	public String getCalDate() {
		return CalDate;
	}
	public void setCalDate(String calDate) {
		CalDate = calDate;
	}
	public String getQryDate() {
		return QryDate;
	}
	public void setQryDate(String qryDate) {
		QryDate = qryDate;
	}
	public String getApplSts() {
		return ApplSts;
	}
	public void setApplSts(String applSts) {
		ApplSts = applSts;
	}
	public String getApplStsUpdDate() {
		return ApplStsUpdDate;
	}
	public void setApplStsUpdDate(String applStsUpdDate) {
		ApplStsUpdDate = applStsUpdDate;
	}
	public String getMsgDate() {
		return MsgDate;
	}
	public void setMsgDate(String msgDate) {
		MsgDate = msgDate;
	}
	public String getPolicySts() {
		return PolicySts;
	}
	public void setPolicySts(String policySts) {
		PolicySts = policySts;
	}
	public String getPolicyStsUpdDate() {
		return PolicyStsUpdDate;
	}
	public void setPolicyStsUpdDate(String policyStsUpdDate) {
		PolicyStsUpdDate = policyStsUpdDate;
	}
	
	public String getAumStatus() {
		return AumStatus;
	}
	public void setAumStatus(String status) {
		AumStatus = status;
	}
	public String getCreateTime() {
		return CreateTime;
	}
	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	
	}
	public String getUniqId() {
		return UniqId;
	}
	public void setUniqId(String uniqId) {
		UniqId = uniqId;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
	public String getPayerStatus() {
		return PayerStatus;
	}
	public void setPayerStatus(String payerStatus) {
		PayerStatus = payerStatus;
	}
	public String getUpdateTime() {
		return UpdateTime;
	}
	public void setUpdateTime(String updateTime) {
		UpdateTime = updateTime;
	}
	public String getPayerTime() {
		return PayerTime;
	}
	public void setPayerTime(String payerTime) {
		PayerTime = payerTime;
	}
	public String getPdctSts() {
		return PdctSts;
	}
	public void setPdctSts(String pdctSts) {
		PdctSts = pdctSts;
	}
	public String getCLStatus() {
		return CLStatus;
	}
	public void setCLStatus(String cLStatus) {
		CLStatus = cLStatus;
	}
	public String getCLTime() {
		return CLTime;
	}
	public void setCLTime(String cLTime) {
		CLTime = cLTime;
	}
	public String getIdno() {
		return Idno;
	}
	public void setIdno(String idno) {
		Idno = idno;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getChtName() {
		return ChtName;
	}
	public void setChtName(String chtName) {
		ChtName = chtName;
	}
	public String getEmailAddress() {
		return EmailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}
	public String getSalaryAccountTw() {
		return SalaryAccountTw;
	}
	public void setSalaryAccountTw(String salaryAccountTw) {
		SalaryAccountTw = salaryAccountTw;
	}

	
	
}