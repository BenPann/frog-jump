package com.kgi.eopend3.common.dto.db;

public class ED3_CLCif {

	private int Serial ;
	private String MsgId; 
	private String UniqId; 
	private String StsCode; 
	private String StsDesc; 
	private String ShortUrl; 
	private String Entry; 
	private String CaseId; 
	private String ApplNo; 
	private String ApplyPdc; 
	private String ApplyPdcRePmt; 
	private String ApplyPmtEndDt; 
	private String MsgSendDt; 
	private String Token; 
	private String TokenStrDt; 
	private String TokenEndDt; 
	private String IdNo; 
	private String ChtName; 
	private String EngName; 
	private String PyName; 
	private String BirthDt; 
	private String Email; 
	private String Marriage; 
	private String MobNo; 
	private String HomeTel; 
	private String ResAddr; 
	private String ComAddr; 
	private String HomeAddr;
	private String ApplType;
	private String ChanId;
	private String PromoDepart;
	private String PromoMmbId;
	private String PromoMmbName;
	private String CreateTime;
	public int getSerial() {
		return Serial;
	}
	public void setSerial(int serial) {
		Serial = serial;
	}
	public String getMsgId() {
		return MsgId;
	}
	public void setMsgId(String msgId) {
		MsgId = msgId;
	}
	
	public String getUniqId() {
		return UniqId;
	}
	public void setUniqId(String uniqId) {
		UniqId = uniqId;
	}
	public String getStsCode() {
		return StsCode;
	}
	public void setStsCode(String stsCode) {
		StsCode = stsCode;
	}
	public String getStsDesc() {
		return StsDesc;
	}
	public void setStsDesc(String stsDesc) {
		StsDesc = stsDesc;
	}
	public String getShortUrl() {
		return ShortUrl;
	}
	public void setShortUrl(String shortUrl) {
		ShortUrl = shortUrl;
	}
	public String getEntry() {
		return Entry;
	}
	public void setEntry(String entry) {
		Entry = entry;
	}
	public String getCaseId() {
		return CaseId;
	}
	public void setCaseId(String caseId) {
		CaseId = caseId;
	}
	public String getApplNo() {
		return ApplNo;
	}
	public void setApplNo(String applNo) {
		ApplNo = applNo;
	}
	public String getApplyPdc() {
		return ApplyPdc;
	}
	public void setApplyPdc(String applyPdc) {
		ApplyPdc = applyPdc;
	}
	public String getApplyPdcRePmt() {
		return ApplyPdcRePmt;
	}
	public void setApplyPdcRePmt(String applyPdcRePmt) {
		ApplyPdcRePmt = applyPdcRePmt;
	}
	public String getApplyPmtEndDt() {
		return ApplyPmtEndDt;
	}
	public void setApplyPmtEndDt(String applyPmtEndDt) {
		ApplyPmtEndDt = applyPmtEndDt;
	}
	public String getMsgSendDt() {
		return MsgSendDt;
	}
	public void setMsgSendDt(String msgSendDt) {
		MsgSendDt = msgSendDt;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
	public String getTokenStrDt() {
		return TokenStrDt;
	}
	public void setTokenStrDt(String tokenStrDt) {
		TokenStrDt = tokenStrDt;
	}
	public String getTokenEndDt() {
		return TokenEndDt;
	}
	public void setTokenEndDt(String tokenEndDt) {
		TokenEndDt = tokenEndDt;
	}
	public String getIdNo() {
		return IdNo;
	}
	public void setIdNo(String idNo) {
		IdNo = idNo;
	}
	public String getChtName() {
		return ChtName;
	}
	public void setChtName(String chtName) {
		ChtName = chtName;
	}
	public String getEngName() {
		return EngName;
	}
	public void setEngName(String engName) {
		EngName = engName;
	}
	public String getPyName() {
		return PyName;
	}
	public void setPyName(String pyName) {
		PyName = pyName;
	}
	public String getBirthDt() {
		return BirthDt;
	}
	public void setBirthDt(String birthDt) {
		BirthDt = birthDt;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getMarriage() {
		return Marriage;
	}
	public void setMarriage(String marriage) {
		Marriage = marriage;
	}
	public String getMobNo() {
		return MobNo;
	}
	public void setMobNo(String mobNo) {
		MobNo = mobNo;
	}
	public String getHomeTel() {
		return HomeTel;
	}
	public void setHomeTel(String homeTel) {
		HomeTel = homeTel;
	}
	public String getResAddr() {
		return ResAddr;
	}
	public void setResAddr(String resAddr) {
		ResAddr = resAddr;
	}
	public String getComAddr() {
		return ComAddr;
	}
	public void setComAddr(String comAddr) {
		ComAddr = comAddr;
	}
	public String getHomeAddr() {
		return HomeAddr;
	}
	public void setHomeAddr(String homeAddr) {
		HomeAddr = homeAddr;
	}
	public String getApplType() {
		return ApplType;
	}
	public void setApplType(String applType) {
		ApplType = applType;
	}
	public String getChanId() {
		return ChanId;
	}
	public void setChanId(String chanId) {
		ChanId = chanId;
	}
	public String getPromoDepart() {
		return PromoDepart;
	}
	public void setPromoDepart(String promoDepart) {
		PromoDepart = promoDepart;
	}
	public String getPromoMmbId() {
		return PromoMmbId;
	}
	public void setPromoMmbId(String promoMmbId) {
		PromoMmbId = promoMmbId;
	}
	public String getPromoMmbName() {
		return PromoMmbName;
	}
	public void setPromoMmbName(String promoMmbName) {
		PromoMmbName = promoMmbName;
	}
	public String getCreateTime() {
		return CreateTime;
	}
	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}

	
	
}