package com.kgi.eopend3.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BasicDataView{

    private String emailAddress = "";
    private String sendType = "";
    private String resTelArea = "";
    private String resTel = "";
    private String homeTelArea = "";
    private String homeTel = "";
    private String education = "";
    private String estateType = "";
    private String purpose = "";
    private String authorizeToallCorp = "";
    private String autVer = "";
    private String branchID = "";
      
}