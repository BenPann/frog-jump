package com.kgi.eopend3.common.dto.response;

import java.util.List;

import com.kgi.onlineformex.common.dto.customDto.ASystemRes;
import com.kgi.onlineformex.common.dto.customDto.GetOtpTelResDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CaseRplInitResp {
    private String token;    
    private List<ASystemRes> asystem;
    private GetOtpTelResDto getOtpTelResDto;
}