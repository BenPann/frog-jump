package com.kgi.mgm.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MGM_ProdTypeMapping {
    private String PROD_TYPE;
    private String QR_SHORTURL_PProductType;    
    private String QR_SHORT_URL_PProductId;
}
