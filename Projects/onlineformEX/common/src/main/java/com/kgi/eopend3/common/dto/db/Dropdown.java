package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Dropdown {
    
    String Name = "";
    String DataKey = "";
    String DataName = "";
    String Enable = "";
    String Sort = "";
   
}
