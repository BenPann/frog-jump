package com.kgi.eopend3.common.dto.view;

import com.google.gson.annotations.SerializedName;
import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CheckOTPView {

    private String idno = "";
    @SerializedName(value="opId", alternate={"opID"})
    private String opId = "";
    private String billDep = "";
    @CheckNullAndEmpty
    private String otp = "";
    @CheckNullAndEmpty
    private String sk = "";
    @CheckNullAndEmpty
    @SerializedName(value="txnId", alternate={"txnID"})
    private String txnId = "";
    @CheckNullAndEmpty
    private String txnDate = "";
    @CheckNullAndEmpty
    private String phone = "";

}