package com.kgi.eopend3.common.dto.customDto;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CheckNullAndEmpty
public class BreakPointMsgDto {
    private String BatchId;
    private String ApplyList;
    private String EndDate;
}

