package com.kgi.eopend3.common.dto.view;

import com.google.gson.GsonBuilder;
import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

public class PCode2566View {

	@CheckNullAndEmpty
    private String phone = "";
	@CheckNullAndEmpty
    private String bank = "";
	@CheckNullAndEmpty
    private String account = "";

    private String idno = "" ;
    private String pcodeUid = "" ;
    private String birthday = "" ;

    private String tmnlId = "" ;
    private String tmnlType = "" ;

    private String ESBClientId ;
    private String ESBClientPAZZD;
    private String ESBTimestamp;
    private String ESBChannel ;
    private String ESBQueueReceiveName;
    
    public String getIdno() {
		return idno;
	}



	public void setIdno(String idno) {
		this.idno = idno;
	}


	public String getPcodeUid() {
		return pcodeUid;
	}



	public void setPcodeUid(String pcodeUid) {
		this.pcodeUid = pcodeUid;
	}



	public String getBirthday() {
		return birthday;
	}



	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}



	public String getPhone() {
		return phone;
	}



	public void setPhone(String phone) {
		this.phone = phone;
	}



	public String getBank() {
		return bank;
	}



	public void setBank(String bank) {
		this.bank = bank;
	}



	public String getAccount() {
		return account;
	}



	public void setAccount(String account) {
		this.account = account;
	}



	public String getTmnlId() {
		return tmnlId;
	}



	public void setTmnlId(String tmnlId) {
		this.tmnlId = tmnlId;
	}



	public String getTmnlType() {
		return tmnlType;
	}



	public void setTmnlType(String tmnlType) {
		this.tmnlType = tmnlType;
	}

	public String getESBClientId() {
		return ESBClientId;
	}



	public void setESBClientId(String eSBClientId) {
		ESBClientId = eSBClientId;
	}



	public String getESBClientPAZZD() {
		return ESBClientPAZZD;
	}



	public void setESBClientPAZZD(String eSBClientPAZZD) {
		ESBClientPAZZD = eSBClientPAZZD;
	}



	public String getESBTimestamp() {
		return ESBTimestamp;
	}



	public void setESBTimestamp(String eSBTimestamp) {
		ESBTimestamp = eSBTimestamp;
	}



	public String getESBChannel() {
		return ESBChannel;
	}



	public void setESBChannel(String eSBChannel) {
		ESBChannel = eSBChannel;
	}



	public String getESBQueueReceiveName() {
		return ESBQueueReceiveName;
	}



	public void setESBQueueReceiveName(String eSBQueueReceiveName) {
		ESBQueueReceiveName = eSBQueueReceiveName;
	}



	public String toJsonString() {
		return new GsonBuilder().disableHtmlEscaping().create().toJson(this) ;
	}
}