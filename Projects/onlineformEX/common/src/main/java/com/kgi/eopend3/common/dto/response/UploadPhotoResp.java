package com.kgi.eopend3.common.dto.response;

import lombok.*;

@Getter
@Setter
public class UploadPhotoResp {

    private String waterMarkImage = "";
    private String name = "";
    private String idCard_Issue_DT = "";
    private String idCard_Issue_Type = "";
    private String idCard_Issue_City = "";     
    private String householdZipCode="";
    private String householdAddr2 = "";
    private String householdAddr3 = "";
    private String householdAddr4 = "";
    private String marriage = "";
    private String subSerial="";

}