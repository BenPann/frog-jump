package com.kgi.eopend3.common.dto.customDto;

import com.google.gson.GsonBuilder;

public class NCCCDto {
	int result = -1 ;
	String message = "";
	
	String statusCode = "";

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String toString() {
		return new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(this);
	}

}
