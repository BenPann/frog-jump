package com.kgi.eopend3.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CheckNullAndEmpty
public class ChooseProductView  {

    private String creditType;
    private String twTaxpayer;
}