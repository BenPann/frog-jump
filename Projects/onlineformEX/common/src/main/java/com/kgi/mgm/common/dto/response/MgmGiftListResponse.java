package com.kgi.mgm.common.dto.response;

import java.util.List;

import com.kgi.mgm.common.dto.db.MGM_GiftList;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MgmGiftListResponse {

    private Integer status;
    
    private List<MGM_GiftList> dataList;
}
