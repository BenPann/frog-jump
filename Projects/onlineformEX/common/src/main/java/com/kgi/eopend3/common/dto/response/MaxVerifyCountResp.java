package com.kgi.eopend3.common.dto.response;

public class MaxVerifyCountResp {
	
    private int ncccTotalCount ;
    private int pcode2566TotalCount ;

	public int getNcccTotalCount() {
		return ncccTotalCount;
	}
	public void setNcccTotalCount(int ncccTotalCount) {
		this.ncccTotalCount = ncccTotalCount;
	}
	public int getPcode2566TotalCount() {
		return pcode2566TotalCount;
	}
	public void setPcode2566TotalCount(int pcode2566TotalCount) {
		this.pcode2566TotalCount = pcode2566TotalCount;
	}

    
    

}