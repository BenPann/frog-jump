package com.kgi.eopend3.common.dto.db;

import com.google.gson.annotations.SerializedName;

public class ED3_BranchList {
    
	@SerializedName(value="branchID", alternate={"BranchID"})
	private String BranchID = "" ;
	@SerializedName(value="branchAddrZipCode", alternate={"BranchAddrZipCode"}) 
	private String BranchAddrZipCode = "" ;
	@SerializedName(value="branchName", alternate={"BranchName"})
	private String BranchName = "" ;
	@SerializedName(value="branchAddr", alternate={"BranchAddr"})
	private String BranchAddr = "" ;
	@SerializedName(value="branchAddrCity", alternate={"BranchAddrCity"})
	private String BranchAddrCity = "" ;
	@SerializedName(value="branchAddrDist", alternate={"BranchAddrDist"})
	private String BranchAddrDist = "" ;

	public String getBranchID() {
		return BranchID;
	}
	public void setBranchID(String branchID) {
		BranchID = branchID;
	}
	public String getBranchAddrZipCode() {
		return BranchAddrZipCode;
	}
	public void setBranchAddrZipCode(String branchAddrZipCode) {
		BranchAddrZipCode = branchAddrZipCode;
	}
	public String getBranchName() {
		return BranchName;
	}
	public void setBranchName(String branchName) {
		BranchName = branchName;
	}
	public String getBranchAddr() {
		return BranchAddr;
	}
	public void setBranchAddr(String branchAddr) {
		BranchAddr = branchAddr;
	}
	public String getBranchAddrCity() {
		return BranchAddrCity;
	}
	public void setBranchAddrCity(String branchAddrCity) {
		BranchAddrCity = branchAddrCity;
	}
	public String getBranchAddrDist() {
		return BranchAddrDist;
	}
	public void setBranchAddrDist(String branchAddrDist) {
		BranchAddrDist = branchAddrDist;
	}


	
  
}