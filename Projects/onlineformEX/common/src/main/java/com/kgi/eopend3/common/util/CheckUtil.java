package com.kgi.eopend3.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;


public class CheckUtil {
    public static boolean check(Object obj){
        try {
            CheckNullAndEmpty annotation1 = obj.getClass().getAnnotation(CheckNullAndEmpty.class);
            if(annotation1!=null){
                Field[] declaredFields = obj.getClass().getDeclaredFields();
                for (Field field : declaredFields) {
                    String filedName = turnFirstWordToUpperCase(field.getName());
                    Method method = obj.getClass().getMethod("get" + filedName);
                    Object o = method.invoke(obj, null);
                    if (o == null) {
                        return false;
                    }
                    if ("".equals(String.valueOf(o))) {
                        return false;
                    }
                }
                return true;
            }else {
                Field[] declaredFields = obj.getClass().getDeclaredFields();
                List<Field> collect = Arrays.stream(declaredFields).filter(field -> field.getAnnotation(CheckNullAndEmpty.class) != null).collect(Collectors.toList());
                for (Field field : collect) {
                    CheckNullAndEmpty annotation = field.getAnnotation(CheckNullAndEmpty.class);
                    String filedName = turnFirstWordToUpperCase(field.getName());
                    Method method = obj.getClass().getMethod("get" + filedName);
                    Object o = method.invoke(obj, null);
                    if (annotation.checkNull().equalsIgnoreCase("Y") && o == null) {
                        return false;
                    }
                    if (annotation.checkEmpty().equalsIgnoreCase("Y") && "".equals(String.valueOf(o))) {
                        return false;
                    }
                }
                return true;
            }
        }catch (NullPointerException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e){
            return false;
        }
    }

    
	/**
	 * 檢查是否為空值或空字串
	 */
	public static boolean isEmpty(String ... strs) {
		for (String str : strs) {
			if (CheckUtil.isEmpty(str)) {
				return true ;
			}
		}
		return false ;
	}
	public static boolean isEmpty(String str) {
		if (str == null || "".equals(str)) {
			return true;
		}
		return false;
	}

	/**
	 * 檢查日期是否符合格式
	 */
	public static boolean checkDate(String date, String format) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			sdf.setLenient(false);

			Date d = sdf.parse(date);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	/**
	 * 檢核是否為整數
	 * */
	public static boolean isInt(String str) {
		boolean ret = false ;
		
		try {
			Integer.parseInt(str) ;
			
			return true ;
		} catch (Exception e) {
			return ret ;
		}
	}
	
    private static String turnFirstWordToUpperCase(String str){
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }



    public static void main(String argc[]) {
//    	System.out.println(CheckUtil.isInt("123"));
//    	System.out.println(CheckUtil.isInt("123.00"));
//    	System.out.println(CheckUtil.isInt("123.1"));
//    	System.out.println(CheckUtil.isInt("-123"));
//    	System.out.println(CheckUtil.isInt("-123.1"));
    	
    	
    }

}
