package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement(name = "MsgRs", namespace = "http://www.cosmos.com.tw/esb")
@XmlAccessorType(XmlAccessType.FIELD)
public class CRD00EC5500MsgRs {

    @XmlElement(name="Header")
    private CRD00EC5500Header crd00EC5500Header;

    @XmlElement(name="SvcRs")
    private CRD00EC5500SvcRs crd00EC5500SvcRs;

    public CRD00EC5500Header getCrd00EC5500Header() {
        return crd00EC5500Header;
    }

    public void setCrd00EC5500Header(CRD00EC5500Header crd00ec5500Header) {
        crd00EC5500Header = crd00ec5500Header;
    }

    public CRD00EC5500SvcRs getCrd00EC5500SvcRs() {
        return crd00EC5500SvcRs;
    }

    public void setCrd00EC5500SvcRs(CRD00EC5500SvcRs crd00ec5500SvcRs) {
        crd00EC5500SvcRs = crd00ec5500SvcRs;
    }

    @Override
    public String toString() {
        return "CRD00EC5500MsgRs [crd00EC5500Header=" + crd00EC5500Header + ", crd00EC5500SvcRs=" + crd00EC5500SvcRs
                + "]";
    }

    
}