package com.kgi.onlineformex.common.dto.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RplLoginResp {

    private String ID_NO;   // 客戶 ID。 
    private String QUALIFIED;
    private String EMAIL;
    private String BIRTHDAY;
    private String MOBILE_PHONE;
    private String APP_NO;

}