package com.kgi.mgm.common.dto.response;

import java.util.List;

import com.kgi.mgm.common.dto.view.MgmSummaryCaseData;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MgmSummaryCaseResponse {

    private Integer status;

    private String seasonCurrent;

    private String seasonLast;

    private String dashboardApplingCnt;

    private String dashboardAppliedCnt;

    private String dashboardAuditedCnt;

    private String detailApplingCnt;

    private String detailAppliedCnt;

    private String detailAuditedCnt;

    private String detailInvalidCnt;

    private List<MgmSummaryCaseData> dataList;

}
