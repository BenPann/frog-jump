package com.kgi.onlineformex.common.dto.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class FM_ApiLog {

    // private String Type;
    // private String KeyInfo;
    // private String ApiName;
    // private String Request;
    // private String Response;
    // private String StartTime;
    // private String EndTime;

    // NEW
    private String UniqId;
    private String FlowPath;
    private String ApiName;
    private String Request;
    private String Response;
    private String StartTime;
    private String EndTime;
    private String UTime;

}
