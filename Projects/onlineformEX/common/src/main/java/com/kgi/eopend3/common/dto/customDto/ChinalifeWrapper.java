package com.kgi.eopend3.common.dto.customDto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class ChinalifeWrapper {

	@SerializedName(value="cifRq")
	private ChinalifeCifRq cifRq;

	@SerializedName(value="cifRs")
	private ChinalifeCifRs cifRs;

	@SerializedName(value="caseStsRq")
	private ChinalifeCaseStsRq caseStsRq;

	@SerializedName(value="caseStsRs")
	private ChinalifeCaseStsRs caseStsRs;

	
	public void setCifRq(ChinalifeCifRq cifRq) {
		this.cifRq = cifRq;
	}



	public ChinalifeCaseStsRs getCaseStsRs() {
		return caseStsRs;
	}



	public void setCaseStsRq(ChinalifeCaseStsRq caseStsRq) {
		this.caseStsRq = caseStsRq;
	}


	public ChinalifeCifRs getCifRs() {
		return cifRs;
	}

	public String toJsonString() {
		return new GsonBuilder().disableHtmlEscaping().create().toJson(this) ;
	}
	
	
	
	public static void main(String[] argc ) {
		/*
		String cifRq = "{	\"cifRq\": {		\"msgId\": \"KG20190618074500111\",		\"token\": \"c3RyaW5ndGVzdA==\",		\"qryType\": \"cif\",		\"idNo\": \"A1234XXXXX\",		\"finInd\": \"N\",		\"caseId\": \"NB12345678\"	}}" ;

		Gson gson = new Gson();
		ChinalifeWrapper wrapperCifRq = gson.fromJson(cifRq, ChinalifeWrapper.class);
		System.out.println("cifRq = " + wrapperCifRq.toJsonString());

		String cifRs = "{	\"cifRs\": {		\"msgId\": \"KG20190618074500111\",		\"stsCode\": \"200\",		\"stsDesc\": \"Success\",		\"shortUrl\": \"\",		\"entry\": \"\",		\"applNo\": \"PO12345\",		\"applyPdc\": \"1\",		\"applyPdcRePmt\": \"1\",		\"applyPmtEndDt\": \"2019-12-03\",		\"msgSendDt\": \"2019-07-03 10:12:14.123\",		\"tokentInfo\": {			\"token\": \"c3RyaW5ndGVzdA==\",			\"tokenStrDt\": \"2019-07-01\",			\"tokenEndDt\": \"2019-07-15\"		},		\"cifInfo\": {			\"idNo\": \"A1234XXXXX\",			\"name\": \"中文名\",			\"engName\": \"ENGNAME\",			\"birthDt\": \"2000-02-27\",			\"email\": \"test@mail.com\",			\"marriage\": \"1\",			\"mobNo\": \"0912XXX311\",			\"resAddr\": \"台北市松山區敦化x路xxx號\",			\"comAddr\": \"台北市松山區敦化x路xxx號\",			\"homeAddr\": \"台北市松山區敦化x路xxx號\",			\"applType\": \"1\"		},		\"addInfo\": {			\"chanId\": \"KS\",			\"promoDepart\": \"a01\",			\"promoMmbId\": \"00001\",			\"promoMmbName\": \"丁xx\"		}	}}" ;
		ChinalifeWrapper wrapperCifRs = gson.fromJson(cifRs, ChinalifeWrapper.class);
		System.out.println("cifRs = " + wrapperCifRs.toJsonString());
		
		String caseStsRq = "{	\"caseStsRq\": {		\"msgId\": \"KG20190618074500111\",		\"token\": \"c3RyaW5ndGVzdA==\",		\"msgSendDt\": \"2019-07-03 10:12:14.123\",		\"caseStsInfo\": {			\"id\": \"A1234XXX789\",			\"applNo\": \"PO12345\",			\"effDt\": \"2019-08-05\",			\"rePdctType\": \"1\",			\"acctNo\": \"12356799\",			\"rePmtFromDt\": \"2020-01-15\",			\"rePmtEndDt\": \"2020-01-31\"		}	}}" ;
		ChinalifeWrapper wrapperCaseStsRq = gson.fromJson(caseStsRq, ChinalifeWrapper.class);
		System.out.println("caseStsRq = " + wrapperCaseStsRq.toJsonString());
		
		
		String caseStsRs = "  {\"caseStsRs\":{    \"msgId\":\"KG20190618074500111\",    \"stsCode\":\"200\",    \"stsDesc\":\"Success\"     }}" ;
		ChinalifeWrapper wrapperCaseStsRs = gson.fromJson(caseStsRs, ChinalifeWrapper.class);
		System.out.println("caseStsRs = " + wrapperCaseStsRs.toJsonString());
		*/
		
	}
	
	
}
