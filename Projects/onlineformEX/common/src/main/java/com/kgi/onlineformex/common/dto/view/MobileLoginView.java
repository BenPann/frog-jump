package com.kgi.onlineformex.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MobileLoginView {
    @CheckNullAndEmpty
    private String token;
    private String ipAddress;
    private String entry;
    private String userAgent;
}