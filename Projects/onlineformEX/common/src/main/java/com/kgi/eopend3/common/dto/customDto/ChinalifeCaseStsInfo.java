package com.kgi.eopend3.common.dto.customDto;

public class ChinalifeCaseStsInfo {

	private String id = "" ;
	private String applNo = "" ;
	private String effDt = "" ;
	private String pdctSts = "" ;
	private String pdctType = "" ;
	private String rePmtAcctNo = "" ;
	private String rePmtFromDt = "" ;
	private String rePmtEndDt = "" ;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getApplNo() {
		return applNo;
	}
	public void setApplNo(String applNo) {
		this.applNo = applNo;
	}
	public String getEffDt() {
		return effDt;
	}
	public void setEffDt(String effDt) {
		this.effDt = effDt;
	}
	public String getPdctSts() {
		return pdctSts;
	}
	public void setPdctSts(String pdctSts) {
		this.pdctSts = pdctSts;
	}
	public String getPdctType() {
		return pdctType;
	}
	public void setPdctType(String pdctType) {
		this.pdctType = pdctType;
	}
	public String getRePmtAcctNo() {
		return rePmtAcctNo;
	}
	public void setRePmtAcctNo(String rePmtAcctNo) {
		this.rePmtAcctNo = rePmtAcctNo;
	}
	public String getRePmtFromDt() {
		return rePmtFromDt;
	}
	public void setRePmtFromDt(String rePmtFromDt) {
		this.rePmtFromDt = rePmtFromDt;
	}
	public String getRePmtEndDt() {
		return rePmtEndDt;
	}
	public void setRePmtEndDt(String rePmtEndDt) {
		this.rePmtEndDt = rePmtEndDt;
	}
	
}
