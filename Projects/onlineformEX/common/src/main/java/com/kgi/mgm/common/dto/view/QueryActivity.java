package com.kgi.mgm.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryActivity {
    public String CUST_ID;
    public String ACT_UNIQ_ID;
}
