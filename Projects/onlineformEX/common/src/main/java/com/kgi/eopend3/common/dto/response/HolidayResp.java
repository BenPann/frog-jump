package com.kgi.eopend3.common.dto.response;

import java.util.List;

import lombok.*;

@Getter
@Setter
public class HolidayResp {

    String bookingMonth = "";
    List<String> holiday;

}