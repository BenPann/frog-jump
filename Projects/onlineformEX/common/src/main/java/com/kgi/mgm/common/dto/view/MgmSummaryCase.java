package com.kgi.mgm.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MgmSummaryCase {

    private String checkCode;
    private String errMsg;
    private MgmSummaryCaseData[] data;
}
