package com.kgi.onlineformex.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ContractView {
    @CheckNullAndEmpty
    private String userId;
    private String protocolType;
    private String caseName;
    private String seriesNumber;
}