package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Header")
public class BNS08508100Header {

    @XmlElement(name="Status")
    private BNS08508100Status bns08508100Status;

    public BNS08508100Status getBns08508100Status() {
        return bns08508100Status;
    }

    public void setBns08508100Status(BNS08508100Status bns08508100Status) {
        this.bns08508100Status = bns08508100Status;
    }

    @Override
    public String toString() {
        return "BNS08508100Header [bns08508100Status=" + bns08508100Status + "]";
    }

        
}