package com.kgi.onlineformex.common.dto.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RplRtnResp {

    private Integer status;
    private RplLoginResp result;
    private String message;

}