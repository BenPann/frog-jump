package com.kgi.onlineformex.common.dto.db;

import com.kgi.onlineformex.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class FM_CaseData {
    @NonNull
    @CheckNullAndEmpty
    private String UniqId; // => FM - YYYYMMDD - series
    private String Status;
    private String Idno;
    private String Birthday;
    private String ASystemContent;
    private String ASystemCaseNo;
    private String UserType;
    private String CaseType;
    private String UserName;
    private String EmailAddress;
    private String Phone;
    private String ManageStore;
    private String CreditCur;
    private String CreditContract;
    private String CreditModify;
    private String EMailSendTime;
    private String ImageSendTime;
    private String AuthStatus;
    private String AuthErrCount;
    private String AuthTime;
    private String ResultHtml;
    private String IpAddress;
    private String CreateTime;
    private String UpdateTime;
}