package com.kgi.eopend3.common.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class VerifyPageResp {
    private String ed3VerifyBankAcct;
    private String ed3VerifyCreditCard;
    private String ed3VerifyOffline;
    private String ed3VerifyNextTime;
}