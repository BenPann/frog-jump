package com.kgi.eopend3.common.dto.customDto;

import java.math.BigDecimal;

import com.google.gson.GsonBuilder;

public class Aum_Chinalife_LngData {

	private BigDecimal LNG_CASH_QTY ;
	private BigDecimal LNG_LOAN_QTY;
	private String EFF_DATE = "";
	private String MSG_DATE = "";
	
	



	public BigDecimal getLNG_CASH_QTY() {
		return LNG_CASH_QTY;
	}



	public void setLNG_CASH_QTY(BigDecimal lNG_CASH_QTY) {
		LNG_CASH_QTY = lNG_CASH_QTY;
	}



	public BigDecimal getLNG_LOAN_QTY() {
		return LNG_LOAN_QTY;
	}



	public void setLNG_LOAN_QTY(BigDecimal lNG_LOAN_QTY) {
		LNG_LOAN_QTY = lNG_LOAN_QTY;
	}



	public String getEFF_DATE() {
		return EFF_DATE;
	}



	public void setEFF_DATE(String effDATE) {
		EFF_DATE = effDATE;
	}



	public String getMSG_DATE() {
		return MSG_DATE;
	}



	public void setMSG_DATE(String msgDATE) {
		MSG_DATE = msgDATE;
	}



	public String toJsonString() {
		return new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(this) ;
	}

}
