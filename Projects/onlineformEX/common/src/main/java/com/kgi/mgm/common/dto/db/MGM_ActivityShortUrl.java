package com.kgi.mgm.common.dto.db;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class MGM_ActivityShortUrl {
    private String ACT_UNIQ_ID;
    private String PROD_TYPE;
    private String SHORT_URL;
    private String CUST_ID;
}
