package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KGIProductList {
	private String CGProductID = "";
	private String PProductType = "";
	private String ChannelId = "";
	private String Select_Default = "";
	private String Can_be_deleted = "";
	private String Sort = "";
}
