package com.kgi.eopend3.common.dto.response;

public class WebBankKeyResp {

    private String webBankKey = "";

    public String getWebBankKey() {
        return webBankKey;
    }

    public void setWebBankKey(String webBankKey) {
        this.webBankKey = webBankKey;
    }
}