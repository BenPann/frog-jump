package com.kgi.eopend3.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KGITermsCustDto {

    private String termTitle = "";
    private String termName = "";   
    private String type = "";   
}
