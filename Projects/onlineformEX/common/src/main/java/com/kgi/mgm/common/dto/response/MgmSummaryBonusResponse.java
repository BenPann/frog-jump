package com.kgi.mgm.common.dto.response;

import java.util.List;

import com.kgi.mgm.common.dto.view.MgmSummaryBonusData;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MgmSummaryBonusResponse {

    private Integer status;
    
    private String seasonCurrent;
    
    private String seasonLast;

    private String dashboardBonusTotal;

    private String detailCardBonusTotal;

    private String detailBonusTotal;

    private List<MgmSummaryBonusData> dataList;
}
