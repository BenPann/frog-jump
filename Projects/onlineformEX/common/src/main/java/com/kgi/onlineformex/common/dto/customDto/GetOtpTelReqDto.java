package com.kgi.onlineformex.common.dto.customDto;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CheckNullAndEmpty
@AllArgsConstructor
public class GetOtpTelReqDto {
    
    /** (1)	CUSTOMER_ID：客戶ID，VARCHAR(11) */
    private String CUSTOMER_ID;

    /** (2)	BIRTHDATE：生日YYYYMMDD，CHAR(8) */
    private String BIRTHDATE;
}