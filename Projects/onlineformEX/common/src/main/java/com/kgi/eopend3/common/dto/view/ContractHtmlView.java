package com.kgi.eopend3.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class ContractHtmlView {
    
    @NonNull
    @CheckNullAndEmpty
    private String FILE_NAME;
    
    @NonNull
    @CheckNullAndEmpty
    private String FILE_PATH;
    
    private String FILE_PW;
    
    @NonNull
    @CheckNullAndEmpty
    private String IMG_CONVERT;

    private String BRANCH_ID; // "9743",

    private String EMAIL; // "aaaa@bbb.ccc",

    private String CUSTOMER_ID; // "A131075118",

    @NonNull
    @CheckNullAndEmpty
    private String PRODUCT_TYPE; // "PL",

    private String APS_SEQ_NO; // "10066000"

    @NonNull
    @CheckNullAndEmpty
    private String HTML_CONTENT;

}