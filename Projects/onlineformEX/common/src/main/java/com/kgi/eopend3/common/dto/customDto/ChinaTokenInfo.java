package com.kgi.eopend3.common.dto.customDto;

public class ChinaTokenInfo {
	private String token = "" ;
	private String tokenStrDt = "" ;
	private String tokenEndDt = "" ;
	
	
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getTokenStrDt() {
		return tokenStrDt;
	}
	public void setTokenStrDt(String tokenStrDt) {
		this.tokenStrDt = tokenStrDt;
	}
	public String getTokenEndDt() {
		return tokenEndDt;
	}
	public void setTokenEndDt(String tokenEndDt) {
		this.tokenEndDt = tokenEndDt;
	}
	
	
	
	
}
