package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Header")
public class CRD00EC5500Header {

    @XmlElement(name="Status")
    private CRD00EC5500Status crd00EC5500Status;

    public CRD00EC5500Status getCrd00EC5500Status() {
        return crd00EC5500Status;
    }

    public void setCrd00EC5500Status(CRD00EC5500Status crd00ec5500Status) {
        crd00EC5500Status = crd00ec5500Status;
    }

    @Override
    public String toString() {
        return "CRD00EC5500Header [crd00EC5500Status=" + crd00EC5500Status + "]";
    }

   
        
}