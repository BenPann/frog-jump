package com.kgi.onlineformex.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
public class FM_Config {
    private String KeyGroup;
    @NonNull
    private String KeyName;
    private String KeyValue;
    private String KeyDesc;
    private String CreateTime;
    private String UpdateTime;
}