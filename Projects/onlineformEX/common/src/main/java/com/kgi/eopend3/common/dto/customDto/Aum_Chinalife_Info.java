package com.kgi.eopend3.common.dto.customDto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Aum_Chinalife_Info {

	private String CUSTOMER_ID = "";
	private String UUID = "";
	private String LNG_DATA ;
	

	public String getCUSTOMER_ID() {
		return CUSTOMER_ID;
	}

	
	
	public void setLNG_DATA(Aum_Chinalife_LngData lNG_DATA) {
		if (lNG_DATA != null) {
			LNG_DATA = new Gson().toJson(new Object[] {lNG_DATA});
		} else {
			LNG_DATA = "" ;
		}
	}



	public void setCUSTOMER_ID(String cUSTOMER_ID) {
		CUSTOMER_ID = cUSTOMER_ID;
	}


	public String getUUID() {
		return UUID;
	}



	public void setUUID(String uUID) {
		UUID = uUID;
	}


	public String toJsonString() {
		return new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(this) ;
	}

}
