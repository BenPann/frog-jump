package com.kgi.eopend3.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MailHunterRsp {

    private String req = "";
    private String rsp = "";
}
