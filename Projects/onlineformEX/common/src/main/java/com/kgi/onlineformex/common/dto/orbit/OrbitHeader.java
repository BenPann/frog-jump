package com.kgi.onlineformex.common.dto.orbit;

import java.text.SimpleDateFormat;
import java.util.Date;

public class OrbitHeader extends OrbitData {
	private SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	public String machineIP; // IP
	public String machineName = "mweb"; // 進件代號
	public String subcaseType; // subcaseType 流程說明 0預核 1非預核 2非預核快速件
	public String uuid; // webcaseid
	public String branchID = ""; // branchID 固定值 9743
	public String userID; // 影像系統設定id值
	public String userName; // id名稱
	public String createTime; // 系統日期
	public String email = ""; // 寄件email
	public String notes = ""; // notes
	public String attachment = ""; // 附件

	public String parentUUID = "";// <!--第二次進件時，指定要撈的(影像)主件其UUID-->
	public String cMarketingProgramId = ""; // <!--專案別 ex:A2M,A5M,D0021....-->
	public String cSalesUnit = ""; // <!--Sales code-單位代號,放PromoDepart-->
	public String cSalesId = "";// <!--Sales code-員編,放PromoMember-->
	public String mApplyAmount = "";// <!--貸款申請金額-->
	public String cReceiveBranch = "";// <!--領卡行放空值-->

	public String referralUnit = ""; //轉介單位
	public String referralMemberCode = ""; //轉介人員代碼
	public String applicant = ""; //申請人姓名
	public String mobileNum = ""; //申請人手機號碼
	public String highRisk = ""; //AML系統查詢
	public String jobType = ""; //高風險職業類別

	public String APSSeqNo = "";



	public String productTypeID = "";

	// 貸款用
	public void from(String machineIP, String subcaseType, String uuid, String userID, String userName,
			String parentUUID, String cMarketingProgramId, String cSalesUnit, String cSalesId, String mApplyAmount, String productTypeId,String referralUnit,String referralMemberCode,String applicant,String mobileNum,String highRisk, String jobType) {

		this.machineIP = machineIP;
		this.subcaseType = subcaseType;
		this.uuid = uuid;
		this.userID = userID;
		this.userName = userName;
		this.parentUUID = parentUUID;
		this.productTypeID = productTypeId;
		this.cMarketingProgramId = cMarketingProgramId;
		this.cSalesUnit = cSalesUnit;
		this.cSalesId = cSalesId;
		this.mApplyAmount = mApplyAmount;
		this.createTime = sdfDateTime.format(new Date());
		this.referralUnit = referralUnit;
		this.referralMemberCode = referralMemberCode;
		this.applicant = applicant;
		this.mobileNum = mobileNum;
		this.highRisk = highRisk;
		this.jobType = jobType;
	}

	// 看起來是給立約使用的
	public void attachmentfrom(String machineIP, String subcaseType, String uuid, String branchID, String userID,
			String userName, String email, String notes, String APSSeqNo, String attachment, String productTypeID) {

		this.machineIP = machineIP;
		this.subcaseType = subcaseType;
		this.uuid = uuid;
		this.userID = userID;
		this.branchID = branchID;
		this.userName = userName;
		this.createTime = sdfDateTime.format(new Date());
		this.email = email;
		this.notes = notes;
		this.APSSeqNo = APSSeqNo;
		this.attachment = attachment;
		this.productTypeID = productTypeID;
	}

	@Override
	public void toXML(StringBuilder sb) {
		sb.append("<machineIP>" + machineIP + "</machineIP>");
		sb.append("<machineName>" + machineName + "</machineName>");
		sb.append("<subcaseType>" + subcaseType + "</subcaseType>");
		sb.append("<uuid>" + uuid + "</uuid>");
		sb.append("<branchID>" + branchID + "</branchID>");
		sb.append("<userID>" + userID + "</userID>");
		sb.append("<userName>" + userName + "</userName>");
		sb.append("<createTime>" + createTime + "</createTime>");
		sb.append("<email>" + email + "</email>");
		sb.append("<notes>" + notes + "</notes>");
		sb.append("<attachment>" + attachment + "</attachment>");
		sb.append("<parentUUID>").append(parentUUID).append("</parentUUID>");
		sb.append("<productTypeID>").append(productTypeID).append("</productTypeID>");
		sb.append("<cMarketingProgramId>").append(cMarketingProgramId).append("</cMarketingProgramId>");
		sb.append("<cSalesUnit>").append(cSalesUnit).append("</cSalesUnit>");
		sb.append("<cSalesId>").append(cSalesId).append("</cSalesId>");
		sb.append("<mApplyAmount>").append(mApplyAmount).append("</mApplyAmount>");
		sb.append("<cReceiveBranch>").append(cReceiveBranch).append("</cReceiveBranch>");
		sb.append("<referralUnit>").append(referralUnit).append("</referralUnit>");
		sb.append("<referralMemberCode>").append(referralMemberCode).append("</referralMemberCode>");
		sb.append("<applicant>").append(applicant).append("</applicant>");
		sb.append("<mobileNum>").append(mobileNum).append("</mobileNum>");
		sb.append("<highRisk>").append(highRisk).append("</highRisk>");
		sb.append("<jobType>").append(jobType).append("</jobType>");
		sb.append("<APSSeqNo>").append(APSSeqNo).append("</APSSeqNo>");
	}
}
