package com.kgi.eopend3.common.dto.db;

public class ED3_CLCaseStsLog {

	private int CaseStsSerial ;
	private String MsgId;
	private String StsCode = "";
	private String StsDesc = "";
	private String CreateTime ;


	public int getCaseStsSerial() {
		return CaseStsSerial;
	}
	public void setCaseStsSerial(int caseStsSerial) {
		CaseStsSerial = caseStsSerial;
	}
	public String getMsgId() {
		return MsgId;
	}
	public void setMsgId(String msgId) {
		MsgId = msgId;
	}
	public String getStsCode() {
		return StsCode;
	}
	public void setStsCode(String stsCode) {
		StsCode = stsCode;
	}
	public String getStsDesc() {
		return StsDesc;
	}
	public void setStsDesc(String stsDesc) {
		StsDesc = stsDesc;
	}
	public String getCreateTime() {
		return CreateTime;
	}
	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}

	
	
}