package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class QR_ChannelDepartList {
    @NonNull
    private String ChannelId;
    @NonNull
    private String DepartId;
    private String DepartName = "";
    private String Enable = "";
    private String EMail = "";
    private String TransDepart = "";
    private String TransMember = "";
    private String PromoDepart = "";
    private String PromoMember = "";
    private String AssignDepart = "";
    private String PromoDepart1 = "";
    private String PromoMember1 = "";
    private String PromoDepart2 = "";
    private String PromoMember2 = "";
    private String AssignDepartExist = "";
    private String AssignDepartNew = "";

    
}