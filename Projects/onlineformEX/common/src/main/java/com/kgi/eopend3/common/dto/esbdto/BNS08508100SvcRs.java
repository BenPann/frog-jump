package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SvcRs")
public class BNS08508100SvcRs {

    @XmlElement(name="BNS085105")
    private BNS08508100 bns08508100;

    public BNS08508100 getBns08508100() {
        return bns08508100;
    }

    public void setBns08508100(BNS08508100 bns08508100) {
        this.bns08508100 = bns08508100;
    }

    @Override
    public String toString() {
        return "BNS08508100SvcRs [bns08508100=" + bns08508100 + "]";
    }
    
}