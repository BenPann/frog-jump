package com.kgi.mgm.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class MGM_MemberActivity {
    @NonNull
    private String CUST_ID;
    @NonNull
    private String ACT_UNIQ_ID;
}
