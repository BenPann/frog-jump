package com.kgi.eopend3.common.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenContinueResp extends TokenResp {

    private String nexttime;

}