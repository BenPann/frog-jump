package com.kgi.onlineformex.common.dto.view;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DataConfirmView {
    public String EmailAddress;
    public String CreditModify;
}
