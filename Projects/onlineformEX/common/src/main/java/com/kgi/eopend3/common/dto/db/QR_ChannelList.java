package com.kgi.eopend3.common.dto.db;

import java.util.Date;

import lombok.*;

@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
public class QR_ChannelList {
	@NonNull
    private String ChannelId;
    private String ChannelName = "";
    private String ChannelType = "";
    private String Enable = "";
    private String ThemePath = "";
    private String DomainUrl = "";
    private String MBCFlag = "";
    private String IdLogic = "";
    private String IdLength = "";
    private String Sort = "";
    private String CanGen = "";
    private String GetCif = "";
	private String CifUrl = "";
	private String MemoUrl = "";
	private String UrlType = "";
	private Date GetCifStartTime;
	private Date GetCifEndTime;
    
}