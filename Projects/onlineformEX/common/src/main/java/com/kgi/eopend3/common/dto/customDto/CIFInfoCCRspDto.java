package com.kgi.eopend3.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CIFInfoCCRspDto {

    private String COMPANY_TEL = "";
    private String CUST_ID= "";
    private String TEL_NO= "";
    private String COMPANY_ADDR= "";
    private String TEL_NO_AREACODE_1= "";
    private String TEL_NO_AREACODE= "";
    private String TEL_NO_AREACODE_2= "";
    private String MARRIAGE= "";
    private String ERR_MSG= "";
    private String MOBILE_TEL= "";
    private String ADR= "";
    private String INCOME= "";
    private String EML_ADDR= "";
    private String CUST_SOURCE_TYPE= "";
    private String EDU= "";
    private String COMPANY_ADDR_ZIP= "";
    private String COMPANY_TEL_EXT= "";
    private String ADR_ZIP_1= "";
    private String TEL_NO_EXT_2= "";
    private String TEL_NO_EXT_1= "";
    private String COMPANY_TEL_AREACODE= "";
    private String CHECK_CODE= "";
    private String ADR_ZIP_2= "";
    private String ON_BOARD_DATE= "";
    private String NAME= "";
    private String COMPANY_NAME= "";
    private String ENG_NAME= "";
    private String TEL_NO_EXT= "";
    private String ADR_1= "";
    private String ADR_2= "";
    private String HOUSE_OWNER= "";
    private String TEL_NO_1= "";
    private String TEL_NO_2= "";
    private String OCCUPATION= "";
    private String ADR_ZIP= "";

}