package com.kgi.eopend3.common.dto.customDto;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CheckNullAndEmpty
@AllArgsConstructor
public class CIFInfoReqDto {
    private String CUSTOMER_ID;
    private String BIRTHDATE;
}