package com.kgi.eopend3.common.dto.db;

public class BNS_BRHM {
	private String BRANCH_NO ;
	private String BR_NAME ;
	private String POST_CODE ;
	private String ADDRESS_2 ;
	private String ADDRESS_1 ;

	public String getBRANCH_NO() {
		return BRANCH_NO;
	}

	public void setBRANCH_NO(String bRANCH_NO) {
		BRANCH_NO = bRANCH_NO;
	}

	public String getBR_NAME() {
		return BR_NAME;
	}

	public void setBR_NAME(String bR_NAME) {
		BR_NAME = bR_NAME;
	}

	public String getPOST_CODE() {
		return POST_CODE;
	}

	public void setPOST_CODE(String pOST_CODE) {
		POST_CODE = pOST_CODE;
	}

	public String getADDRESS_2() {
		return ADDRESS_2;
	}

	public void setADDRESS_2(String aDDRESS_2) {
		ADDRESS_2 = aDDRESS_2;
	}

	public String getADDRESS_1() {
		return ADDRESS_1;
	}

	public void setADDRESS_1(String aDDRESS_1) {
		ADDRESS_1 = aDDRESS_1;
	}

}