package com.kgi.eopend3.common.dto.response;

import lombok.*;

@Getter
@Setter
public class CreditCardListResp {

    int cardSerial;
    String cardType = "";
    String productId = "";
    String cardTitle = "";    
    String cardContent = "";    
    String cardImage = "";

}