package com.kgi.mgm.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
public class MGM_Config {
    @NonNull
    private String KeyName;
    private String KeyValue;
    private String Description;
    private String UpdateTime;
}
