package com.kgi.eopend3.common.dto.customDto;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AOResult {

    @CheckNullAndEmpty
    private String uniqId = "";
    @CheckNullAndEmpty
    private String status = "";
    private String openfailMsg = "";
    private String statusTw = "";
    private String salaryAccountTw = "";
    @SerializedName(value = "passBookTw",alternate = {"PassBookTw"})
    private String PBT = "";
    private String statusFore = "";
    private String salaryAccountFore = "";
    @SerializedName(value = "passBookFore",alternate = {"PassBookFore"})
    private String PBF = "";
    private String statusTrust = "";
    private String d3Message = "";
    private String d3Base64PDF = "";
    private List<FailImage> failedImage;

 }
