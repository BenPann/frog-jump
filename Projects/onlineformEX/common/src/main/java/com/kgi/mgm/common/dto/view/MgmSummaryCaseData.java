package com.kgi.mgm.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MgmSummaryCaseData {

    private String Q_ID;
    private String YYYYMM;
    private String ER_ID;
    private String EE_ID;
    private String EE_NAME;
    private String PRO_FLG;
    private String PRO_NAME;
    private String ER_ACC_TYPE;
    private String APPLY_DT;
    private String APPRV_DT;
    private String CASE_STATUS_CODE;
    private String CASE_STATUS_DESC;
    
}
