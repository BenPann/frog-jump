package com.kgi.eopend3.common.dto.response;


public class SecuritiesDeliveryResp {

    private String securitiesCity = "";
    private String securitiesDist = "";
    private String securitiesAddrZipCode = "";
    private String securitiesCode = "";
    private String securitiesName = "";

    public String getSecuritiesCity() {
        return securitiesCity;
    }

    public void setSecuritiesCity(String securitiesCity) {
        this.securitiesCity = securitiesCity;
    }

    public String getSecuritiesDist() {
        return securitiesDist;
    }

    public void setSecuritiesDist(String securitiesDist) {
        this.securitiesDist = securitiesDist;
    }

    public String getSecuritiesAddrZipCode() {
        return securitiesAddrZipCode;
    }

    public void setSecuritiesAddrZipCode(String securitiesAddrZipCode) {
        this.securitiesAddrZipCode = securitiesAddrZipCode;
    }

    public String getSecuritiesCode() {
        return securitiesCode;
    }

    public void setSecuritiesCode(String securitiesCode) {
        this.securitiesCode = securitiesCode;
    }

    public String getSecuritiesName() {
        return securitiesName;
    }

    public void setSecuritiesName(String securitiesName) {
        this.securitiesName = securitiesName;
    }
    
}