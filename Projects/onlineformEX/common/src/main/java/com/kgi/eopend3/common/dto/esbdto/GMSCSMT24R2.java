package com.kgi.eopend3.common.dto.esbdto;

import java.util.ArrayList;
import java.util.List;

import com.kgi.eopend3.common.util.XmlUtil;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class GMSCSMT24R2 {


    List<GMSCSMT24R2Content> content;
	
	public List<GMSCSMT24R2Content> getContent() {
		return content;
	}
	public void setContent(List<GMSCSMT24R2Content> content) {
		this.content = content;
	}



    public List<GMSCSMT24R2Content> parseXmlMk1(String xml) {

        List<GMSCSMT24R2Content> contents = null;

        try {

            // 09-23
            // NodeList nodeList = XmlUtil.getXmlNodes(xml, "Detail");
            NodeList nodeList = XmlUtil.getXmlNodes(xml, "SvcRs");
            
            contents = new ArrayList<GMSCSMT24R2Content>();

            if (nodeList != null && nodeList.getLength() > 0) {

                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node node = nodeList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        GMSCSMT24R2Content content = funcA(nodeList.item(i));
                        contents.add(content);
                    }
                }

            }
            
        } catch (Exception e) {
            e.printStackTrace();

        }        
        return contents;
    }


    public GMSCSMT24R2Content parseXmlMk2(String xml) {
        GMSCSMT24R2Content contentMk2 = null;

        try {
            NodeList nodeList = XmlUtil.getXmlNodes(xml, "SvcRs");

            if (nodeList != null && nodeList.getLength() > 0) {
                System.out.println("A-SYSTEM RESPONSE LENGTH" + nodeList.getLength());
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node node = nodeList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        contentMk2 = funcA(nodeList.item(i));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }        
        return contentMk2;
    }


    public GMSCSMT24R2Content funcA(Node node) {
        Element ele = (Element) node;
        GMSCSMT24R2Content content = new GMSCSMT24R2Content();
        content.setSO2401(XmlUtil.getNodeValue(ele, "SO2401"));
        content.setSO2402(XmlUtil.getNodeValue(ele, "SO2402"));
        content.setSO2403(XmlUtil.getNodeValue(ele, "SO2403"));
        content.setSO2404(XmlUtil.getNodeValue(ele, "SO2404"));
        content.setSO2405(XmlUtil.getNodeValue(ele, "SO2405"));
        content.setSO2406(XmlUtil.getNodeValue(ele, "SO2406"));
        content.setSO2407(XmlUtil.getNodeValue(ele, "SO2407"));
        content.setSO2408(XmlUtil.getNodeValue(ele, "SO2408"));
        content.setSO2409(XmlUtil.getNodeValue(ele, "SO2409"));
        content.setSO2410(XmlUtil.getNodeValue(ele, "SO2410"));
        content.setSO2411(XmlUtil.getNodeValue(ele, "SO2411"));
        content.setSO2412(XmlUtil.getNodeValue(ele, "SO2412"));
        content.setSO2413(XmlUtil.getNodeValue(ele, "SO2413"));
        content.setSO2414(XmlUtil.getNodeValue(ele, "SO2414"));
        content.setSO2415(XmlUtil.getNodeValue(ele, "SO2415"));
        content.setSO2416(XmlUtil.getNodeValue(ele, "SO2416"));
        content.setSO2417(XmlUtil.getNodeValue(ele, "SO2417"));
        content.setSO2418(XmlUtil.getNodeValue(ele, "SO2418"));
        content.setSO2419(XmlUtil.getNodeValue(ele, "SO2419"));
        return content;
    }


}
