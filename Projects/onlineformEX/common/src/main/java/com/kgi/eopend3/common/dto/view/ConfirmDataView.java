package com.kgi.eopend3.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConfirmDataView {

    private IDCardFrontDataView idCardFrontData;
    private IDCardBackDataView idCardBackData;
    private BasicDataView basicData;
    private CorpDataView corpData;
    private GiftDataView giftData;     
    private ChooseCardView chooseCard; 
    private String active_recommendation ;
    
}