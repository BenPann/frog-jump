package com.kgi.eopend3.common;

public class SystemConst{
	
	//TODO 上線前記得拿掉
	public final static boolean isTEST = true ;
	
	/**QR_ChannelDepartListEOP 預設值*/
	public static final String QR_CHANNELDEPARTLISTEOP_DEFAULT = "-";
	/**預設Channel Id*/
	public static final String DEFAULT_CHANNEL_ID = "KB";
	
	
	/**短期間內密集申請_三個月內申請2次數位帳戶，均未完成開戶流程者*/
	public static final String MSG_EOPEN_TOO_MANY_TIMES = "您已有多筆線上開戶流程未完成，系統已暫停服務，若您仍有開戶需求，請洽各分行辦理。";
	/**申辦資料與投保資料不符，如填寫無誤，仍可繼續申辦，但無法綁定保費續期扣繳(998)*/
	public static final String MSG_ED3_CL_TOKEN_ID_INCORRECT = "申辦資料與投保資料不符，如填寫無誤，仍可繼續申辦，但無法綁定保費續期扣繳(998)";
	/**中壽系統有誤，請洽中壽業務人員。(501)*/
	public static final String MSG_ED3_CL_SYSTEM_TIMEOUT = "中壽系統有誤，請洽中壽業務人員。(501)";
	/**中壽系統有誤，請洽中壽業務人員。(502)*/
	public static final String MSG_ED3_CL_SYSTEM_ERROR = "中壽系統有誤，請洽中壽業務人員。(502)";
	/**中壽系統有誤，請洽中壽業務人員。(503)*/
	public static final String MSG_ED3_CL_ESB_ERROR = "中壽系統有誤，請洽中壽業務人員。(503)";
	/**綁定中壽保費續扣時效已過(999)*/
	public static final String MSG_ED3_CL_TOKEN_EXPIRED = "綁定中壽保費續扣時效已過，您仍可繼續申辦銀行產品，但無法綁定保費續期扣繳(999)";

	public static final String KGIHEADER = "KGIeOpenD3";

	public static final String KGI_BANK_ID = "809";

	/** 數三 */
	public static final String CASE_UNINQTYPE_D3 = "13";	
	/** 中壽 */
	public static final String CASE_UNINQTYPE_CL = "14";

	/**數位帳戶*/
	public static final String CL_API_PDCT_TYPE_D3 = "1";
	/**信用卡*/
	public static final String CL_API_PDCT_TYPE_CC = "2";

	/**中壽API 產品狀態-成功*/
	public static final String CL_API_PDCT_STATUS_OK = "00";

    /**縣市鄉鎮區代碼*/
	public final static String DROPDOWNDATA_NAME_ZIPCODEMAPPING = "zipcodemapping";

	/**主動推薦*/
	public final static String TERMS_NAME_ACTIVE_R = "active_recommendation";
	/**存款總約定書*/
	public final static String TERMS_NAME_EOP_DEPOSIT = "EOPDepositPromise";

	/**案件狀態 -init*/
	public final static String CASE_STATUS_INIT = "00";
	/**案件狀態 -填寫中*/
	public final static String CASE_STATUS_WRITING = "01";
	/**案件狀態 -填寫完成*/
	public final static String CASE_STATUS_WRITE_SUCCESS = "02";
	/**案件狀態 -30天自動結案*/
	public final static String CASE_STATUS_SUSPENDBY30DAYS = "06";
	/**案件狀態 -舊案件的斷點取消*/
	public final static String CASE_STATUS_OLD_UNIQID_CANCEL = "07";
	/**案件狀態 -新案件取消*/
	public final static String CASE_STATUS_NEW_UNIQID_CANCEL = "08";
	/**案件狀態 -送件中*/
	public final static String CASE_STATUS_SUBMITTING = "11";
	/**案件狀態 -送件完成*/
	public final static String CASE_STATUS_SUBMIT_SUCCESS = "12";
	/**案件狀態 -補件失敗*/
	public final static String CASE_STATUS_ADDTIONAL_FAIL = "18";
	/**案件狀態 -送件失敗*/
	public final static String CASE_STATUS_SUBMIT_FAIL = "19";
	/**案件狀態 - 中壽投保逾時撤件 */
	public final static String CASE_STATUS_POLICYTIMEOUT = "20";
	// 以下是數合信貸用到的狀態碼
	/**案件狀態 -拒貸已送件*/
	public final static String CASE_STATUS_REJECT_SENDED = "09";
/**案件狀態 -無對應之信用卡產品*/
	public final static String CASE_STATUS_PRODUCT_ERROR = "17";
	/**案件狀態 -APS作業中*/
	public final static String CASE_STATUS_APS_PROCESSING = "21";
	/**案件狀態 -APS作業完成*/
	public final static String CASE_STATUS_APS_COMPLETE = "22";
	/**案件狀態-等待立約*/
	public final static String CASE_STATUS_CONTRACT_WAIT = "91";
	/**案件狀態-立約送出*/
	public final static String CASE_STATUS_CONTRACT_SUBMIT = "92";
	/**案件狀態-立約成功*/
	public final static String CASE_STATUS_CONTRACT_SUCCESS = "93";

	
	/**婚姻狀態 - 已婚*/
	public final static String CASE_MARRIAGE_YES = "M";
	/**婚姻狀態 - 未婚*/
	public final static String CASE_MARRIAGE_NO = "S";
	
	public static final String KGI_OTP_SUCESS = "0000";


    /** 使用者圖片狀態 - 暫存 */
	public static final String USERPHOTO_STATUS_TEMP = "0";
	/** 使用者圖片狀態 - 使用者處理中(上傳中還未送出) */
	public static final String USERPHOTO_STATUS_USER_PROCESS = "1";
	/** 使用者圖片狀態 - 使用者上傳完成 等待JOB處裡 */
	public static final String USERPHOTO_STATUS_WAIT_UPLOAD = "2";
	/** 使用者圖片狀態 - 圖片上傳成功 */
	public static final String USERPHOTO_STATUS_UPLOAD_SUCCESS = "3";
	/** 使用者圖片狀態 - 圖片上傳失敗 */
	public static final String USERPHOTO_STATUS_UPLOAD_FAIL = "9";

	/**ED3_CLPolicyInfo 中壽通知資料狀態-未檢核*/
	public static final String CL_POLICYINFO_STATUS_NOT_CHECK = "0";
	/**ED3_CLPolicyInfo 中壽通知資料狀態-已檢核*/
	public static final String CL_POLICYINFO_STATUS_CHECKED = "1";

	/**更新同意書及財力證明*/
	public static final String CL_POLICYINFO_ACTION_AGREEMENT = "1";
	/**更新保單狀態*/
	public static final String CL_POLICYINFO_ACTION_UPDATE = "2";

	/**保單承保狀態 - 結案(承保)*/
	public static final String CL_POLICYINFO_POLICYSTS_YES = "1";
	/**保單承保狀態 - 未承保*/
	public static final String CL_POLICYINFO_POLICYSTS_NO = "2";

	/**中壽代收付設定狀態 - 已處理中*/
	public static final String CL_POLICYINFO_PAYERSTATUS_OK = "0";
	/**中壽代收付設定狀態 - 處理中*/
	public static final String CL_POLICYINFO_PAYERSTATUS_ONGOING = "1";
	/**中壽代收付設定狀態 - 設定失敗*/
	public static final String CL_POLICYINFO_PAYERSTATUS_FAILED = "3";
	/**中壽代收付設定狀態 - 重試失敗*/
	public static final String CL_POLICYINFO_PAYERSTATUS_FAILED_AGAIN = "5";

	/**中壽徵審AUM設定狀態 - 已處理中*/
	public static final String CL_POLICYINFO_AUMSTATUS_OK = "0";
	/**中壽徵審AUM設定狀態 - 處理中*/
	public static final String CL_POLICYINFO_AUMSTATUS_ONGOING = "1";
	/**中壽徵審AUM設定狀態 - 設定失敗*/
	public static final String CL_POLICYINFO_AUMSTATUS_FAILED = "3";
	/**中壽徵審AUM設定狀態 - 重試失敗*/
	public static final String CL_POLICYINFO_AUMSTATUS_FAILED_AGAIN = "5";

	/**中壽案件通知傳送狀態 - 已處理中*/
	public static final String CL_CASESTS_CLSTATUS_OK = "0";
	/**中壽案件通知傳送狀態 - 處理中*/
	public static final String CL_CASESTS_CLSTATUS_ONGOING = "1";
	/**中壽案件通知傳送狀態 - 設定失敗*/
	public static final String CL_CASESTS_CLSTATUS_FAILED = "3";
	/**中壽案件通知傳送狀態 - 重試失敗*/
	public static final String CL_CASESTS_CLSTATUS_FAILED_AGAIN = "5";

	/**中壽產品狀態 - 成功*/
	public static final String CL_PDCTSTS_OK = "00";
	/**中壽產品狀態 - 保單不承保*/
	public static final String CL_PDCTSTS_NOT_APPLY = "10";
	/**中壽產品狀態 - 行動投保同意書逾時*/
	public static final String CL_PDCTSTS_EXPIRED = "11";
	/**中壽產品狀態 - QRCode逾15天*/
	public static final String CL_PDCTSTS_QR_EXPIRED = "20";
	/**中壽產品狀態 - KGI網頁申辦逾30天*/
	public static final String CL_PDCTSTS_WEB_EXPIRED = "21";
	/**中壽產品狀態 - KGI申辦失敗*/
	public static final String CL_PDCTSTS_APPLY_FAILED = "22";
	/**中壽產品狀態 - 續扣申辦逾120天*/
	public static final String CL_PDCTSTS_PAYER_EXPIRED = "23";
	/**中壽產品狀態 - 續扣約定檔設定失敗逾時*/
	public static final String CL_PDCTSTS_PAYER_FAILED = "24";
	

	
	/**VerifyLog 驗證別-中華電信*/
	public static final String VERIFYLOG_TYPE_CHT = "Cht";
	/**VerifyLog 驗證別-信用卡信用驗證*/
	public static final String VERIFYLOG_TYPE_NCCC = "NCCC";
	/**VerifyLog 驗證別-銀行信用驗證*/
	public static final String VERIFYLOG_TYPE_PCODE = "PCode2566";

	/**IdentityVerification 驗證別-信用卡信用驗證*/
	public static final String VERIFY_TYPE_NCCC = "1";
	/**IdentityVerification 驗證別-銀行信用驗證*/
	public static final String VERIFY_TYPE_PCODE = "2";
	/**IdentityVerification 驗證別-預約分行驗證*/
	public static final String VERIFY_TYPE_BOOKBRANCH = "3";

	/**IdentityVerification 驗證成功*/
	public static final String VERIFY_STATUS_SUCCESS = "1";
	/**IdentityVerification 驗證失敗*/
	public static final String VERIFY_STATUS_FAIL = "0";
	
	/**ED3_CaseData Process預約分行流程 */
	public static final String CASEDATA_PROCESS_BOOKBRANCH = "1";
	/**ED3_CaseData Process 數位帳戶流程  */
	public static final String CASEDATA_PROCESS_DIGIT = "2";


	/**ED3_CaseData ProductId 新戶選純帳戶*/
	public static final String CASEDATA_PRODUCTID_ACCOUNT = "1";
	/**ED3_CaseData ProductId 新戶選帳加卡*/
	public static final String CASEDATA_PRODUCTID_ACCOUNT_AND_CREDIT = "2";


	/**ED3_EX_CreditCaseData UserType 信用卡-使用者類型-新戶*/
	public static final String CREDITCASEDATA_USERTYPE_NEW = "0";
	/**ED3_EX_CreditCaseData UserType 信用卡-使用者類型-信用卡戶*/
	public static final String CREDITCASEDATA_USERTYPE_CREDIT = "1";
	/**ED3_EX_CreditCaseData UserType 信用卡-使用者類型-存款戶*/
	public static final String CREDITCASEDATA_USERTYPE_ACCOUNT = "2";
	/**ED3_EX_CreditCaseData UserType 信用卡-使用者類型-純貸款戶*/
	public static final String CREDITCASEDATA_USERTYPE_LOAN = "3";
	/**ED3_EX_CreditCaseData UserType 信用卡-使用者類型-其他*/
	public static final String CREDITCASEDATA_USERTYPE_OTHER = "4";


	/**ED3_CaseData UserType -使用者類型-新戶*/
	public static final String CASEDATA_USERTYPE_NEW = "0";
	/**ED3_CaseData UserType 信用卡-使用者類型-信用卡戶*/
	public static final String CASEDATA_USERTYPE_CREDIT = "1";
	/**ED3_CaseData UserType 信用卡-使用者類型-存款戶*/
	public static final String CASEDATA_USERTYPE_ACCOUNT = "2";
	/**ED3_CaseData UserType 信用卡-使用者類型-純貸款戶*/
	public static final String CASEDATA_USERTYPE_LOAN = "3";
	/**ED3_CaseData UserType 信用卡-使用者類型-其他*/
	public static final String CASEDATA_USERTYPE_OTHER = "4";	
	/**ED3_CaseData UserType 信用卡-使用者類型-卡戶+存戶*/
	public static final String CASEDATA_USERTYPE_CCA = "5";

	/**ED3_CaseData IsPayer 中壽案是否設定續期扣繳*/
	public static final String CASEDATA_ISPAYER_YES = "1";

	/**ApplyItem 附加服務-舊戶本身已有辦過網路銀行*/
	public static final String APPLYITEM_OLD_APPLYED_WEBBANK = "11";
	/**ApplyItem 附加服務-新戶辦網路銀行*/
	public static final String APPLYITEM_NEW_WEBBANK = "13";
	/**ApplyItem 附加服務-跳過信用卡驗身*/
	public static final String APPLYITEM_CREDIT_DONOT_VERIFY = "14";	
	/**ApplyItem 附加服務-舊戶本身已有辦過電話銀行*/
	public static final String APPLYITEM_OLD_APPLYED_IVRBANK = "16";
	/**ApplyItem 附加服務-新戶辦電話銀行*/
	public static final String APPLYITEM_NEW_IVRBANK = "18";
	/**ApplyItem 附加服務-下次再驗*/
	public static final String APPLYITEM_NEXTTIME = "19";


	public static final String APILOG_TYPE_CALLOUT = "1";
	public static final String APILOG_TYPE_CALLIN = "2";

	public static final int IMAGE_SIZE_LIMIT = 6*1024*1366 ; // 上傳圖片大小限制 (base64 會大一點  1024/3*4 ==> 1366)

	/** ED3_UserPhoto */
	public static final String USERPHOTO_SUBTYPE_IDCARDFRONT = "1" ; // 身分證正面
	public static final String USERPHOTO_SUBTYPE_IDCARDBANK = "2" ; // 身分正反面
	public static final String USERPHOTO_SUBTYPE_HEALTHIDCARD = "3" ; // 健保卡/駕照
	
	
	public static final String API_SEND_OTP = "sendOTP" ;
	public static final String API_CHECK_OTP = "checkOTP" ;
	public static final String API_KGI_PCODE2566 = "kgiPcode2566" ;
	public static final String API_OTHER_PCDOE2566 = "otherPCode2566" ;
	public static final String API_KGI_NCCC = "kgiNCCC" ;
	public static final String API_OTHER_NCCC = "otherNCCC" ;

	/** API 回覆 下次再驗：無*/
	public static final String API_NEXTTIME_NO = "0" ;
	/** API 回覆 下次再驗：有*/
	public static final String API_NEXTTIME_YES = "1" ;

	/**
     * jwt有效時間 5天
     */
    public static final long EXPIRATIONTIME = 432_000_000;
    /**
     * JWT密碼
     */
    public static final byte[] key = "kgied3fuco201910".getBytes();
    /**
     * Token前缀
     */
    public static final String TOKEN_PREFIX = "Bearer";
    /**
     * 存放Token的Header Key
     */
	public static final String HEADER_STRING = "Authorization";
	

	/** 開戶狀態 - 成功 */
	public static final String AORESULT_STATUS_SUCCESS = "0";
	/** 開戶狀態 - 退件 */
	public static final String AORESULT_STATUS_REJECT = "1";
	/** 開戶狀態 - 補件 */
	public static final String AORESULT_STATUS_WAIT_UPLOAD = "2";

	/** 是否為本行卡 */
	public static final String VERIFY_CARD_KGI = "Y";
	/** 是否為本行卡 */
	public static final String VERIFY_CARD_OTHER = "N";

	/** 拒絕申辦原因 - 舊戶 */
	public static final String REJECTLOG_COED_OLDUSER = "0";
	/** 拒絕申辦原因 - 兩次未接續斷點 */
	public static final String REJECTLOG_COED_SECONDAPPLYNOFINISH = "1";
	/** 拒絕申辦原因 - 手機申辦成功 */
	public static final String REJECTLOG_COED_PHONEAPPLYED = "2";
	/** 拒絕申辦原因 - 電子信箱申辦成功 */
	public static final String REJECTLOG_COED_EMAILAPPLYED = "3";
	/** 拒絕申辦原因 - 事故戶 */
	public static final String REJECTLOG_COED_INCIDENT = "4";
	

	/** MailHistory SMTP 單筆寄 */
	public static final String MAILHISTORY_TYPE_SMTP = "2";
	/** MailHistory JOB */
	public static final String MAILHISTORY_TYPE_JOB = "3";
	/**MailHistory 簡訊*/
	public static final String MAILHISTORY_TYPE_SM = "4";
	/**MailHistory 目前只有數三使用，全部一起寄*/
	public static final String MAILHISTORY_TYPE_SMTP_ALL = "5";

	/**中壽 CaseStsRq 成功碼 */
	public static final String CL_CASESTSRQ_API_SUCCESS = "200";
	/**中壽 GetCif 成功碼 */
	public static final String CL_GETCIF_API_SUCCESS = "200";
	/**中壽 GetCif token與ID比對 不符 */
	public static final String CL_GETCIF_API_ID_INCORRECT = "998";
	public static final String CL_GETCIF_API_SYSTEM_TIMEOUT = "501";
	public static final String CL_GETCIF_API_SYSTEM_ERROR = "502";
	public static final String CL_GETCIF_API_ESB_ERROR = "503";
	public static final String CL_GETCIF_API_TOKEN_EXPIRED = "999";

	/**中壽 GetCif 的 QryType*/
	public static final String CL_GETCIF_QRYTYPE = "cif";

	/**中壽 GetCif 沒有加辦信用卡(FinInd) */
	public static final String CL_GETCIF_FININD_NO = "N";
	/**中壽 GetCif 有加辦信用卡(FinInd) */
	public static final String CL_GETCIF_FININD_YES = "Y";
	
	/**中壽申辦項目:數位帳戶*/
	public static final String CL_PDCTTYPE_ACCT = "1" ;

	/**中壽 API 信用卡效期無日期時的值*/
	public static final String CL_CASESTS_API_CREDIT_DATE = "00000000";

	/**中壽 GetCif 未婚 */
	public static final String CL_GETCIF_MARRIAGE_NO = "1";
	/**中壽 GetCif 已婚 */
	public static final String CL_GETCIF_MARRIAGE_YES = "2";

	/**中壽 GetCif token過期 */
	public static final String TOKEN_EXPIRED = "Y";
	/**中壽 GetCif token未過期 */
	public static final String TOKEN_NOT_EXPIRE = "N";
	
	/**KGI異常等級3*/
	public static final String ESB_REASON_CODE_08 = "08";
	/***/
	public static final String ESB_INCIDENT_TYPE_0206 = "0206";
	/**人頭戶*/
	public static final String ESB_INCIDENT_TYPE_0200 = "0200";
	/**設定事故戶*/
	public static final String ESB_RM_J = "J";
	/**更正事故戶*/
	public static final String ESB_RM_C = "C";

	/**代收付平台 委繳戶單筆授權資料維護API－成功*/
	public static final String PAYER_PAYER_STATUS_SUCCESS = "0";

	/**代收付平台 資料庫－成功*/
	public static final String PAYER_DB_STATUS_SUCCESS = "0";
	public static final String PAYER_DB_STATUS_SUCCESS2 = "00";
	public static final String PAYER_DB_STATUS_SUCCESS3 = "80";

	public static final int JOBLOG_STATUS_INFO = 1;
	public static final int JOBLOG_STATUS_DATA_INCORRECT = 2;
	public static final int JOBLOG_STATUS_DISCONNECT = 3;
	public static final int JOBLOG_STATUS_SYSTEM_ERR = 4;

	public static final String CASE_ISPAYER_YES = "1";
	public static final String CASE_ISPAYER_NO = "0";



	// SECTOR: RPL STATUS COUNT
	/** CaseData Status variable */
	public static final String RPL_STATUS_INIT = "00";
	public static final String RPL_STATUS_SEND_OTP = "01";
	public static final String RPL_STATUS_A_SYSTEM_FINISH = "02";
	public static final String RPL_STATUS_SEND_MEDIA = "03";
	public static final String RPL_STATUS_1 = "04";
	public static final String RPL_STATUS_2 = "11";
	public static final String RPL_STATUS_3 = "12";
	public static final String RPL_STATUS_4 = "18";

	/** MailHistory Status variable */
	public static final String RPL_MAIL_HISTORY_STATUS_INIT = "00";
	public static final String RPL_MAIL_HISTORY_STATUS_SUCCESSFUL = "01";
	public static final String RPL_MAIL_HISTORY_STATUS_FAILED = "09";


	/** A-SYSTEM CONNECT API NAME */
	public static final String RPL_A_SYSTEM_CONNECT_GET = "KGI/GET_DGT_CONST";
	public static final String RPL_A_SYSTEM_CONNECT_UPDATE = "KGI/UPDATE_DGT_CONST";


}