package com.kgi.mgm.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MGM_GiftList {
    private String GIFT_ID;
    private String GIFT_NAME;
    private String GIFT_DESC;
    private String GIFT_NOTES;
    private String GIFT_SPEND_POINT;
    private String GIFT_IMG_LINK_PATH;
    private String CREATE_TIME;
    private String ENABLE;
}
