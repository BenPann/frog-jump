package com.kgi.eopend3.common.dto.db;

import org.springframework.lang.NonNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class ContractMainTerms {

    @NonNull
    private String UniqId; 
    @NonNull
    private int TermSerial;
    private String IsAgree;
}