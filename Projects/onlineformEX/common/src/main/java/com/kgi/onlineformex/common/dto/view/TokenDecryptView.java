package com.kgi.onlineformex.common.dto.view;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TokenDecryptView {
    private String idno;
    private String token;
}