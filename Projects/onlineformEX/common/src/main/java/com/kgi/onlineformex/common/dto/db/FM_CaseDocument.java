package com.kgi.onlineformex.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class FM_CaseDocument {
    @NonNull
    private String UniqId;
    @NonNull
    private String UniqType;
    @NonNull
    private String DocumentType;
    private byte[] Content;
    private String XmlContent;
    private String Version;
    private String Other;
}