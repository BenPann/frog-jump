package com.kgi.mgm.common.dto.db;

import lombok.Getter;
import lombok.Setter;
import lombok.RequiredArgsConstructor;

@Getter
@Setter
@RequiredArgsConstructor
public class MGM_Activity {
    private String ACT_UNIQ_ID;
    private String ACT_NAME;
    private String ACT_DESC;
    private String ACT_TYPE;
    private String PROD_TYPE_LIST;
    private String ACT_PRJ_CODE;
    private String ACT_CREATE_TIME;
    private String ACT_START_TIME;
    private String ACT_END_TIME;
}
