package com.kgi.mgm.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MGM_GiftApplyData {
    private String Serial;
    private String ApplyTime;
    private String ER_ID;
    private String GIFT_ID;
    private String APPLY_AMT;
}
