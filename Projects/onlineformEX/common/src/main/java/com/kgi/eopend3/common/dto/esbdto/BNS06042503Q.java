package com.kgi.eopend3.common.dto.esbdto;

import java.util.ArrayList;
import java.util.List;

import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.util.XmlUtil;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class BNS06042503Q {
	
	List<BNS06042503QDetail> detail ;
	
	public List<BNS06042503QDetail> getDetail() {
		return detail;
	}
	public void setDetail(List<BNS06042503QDetail> detail) {
		this.detail = detail;
	}

	public List<BNS06042503Q.BNS06042503QDetail> parseXml(String xml) {
		List<BNS06042503QDetail> details = null ;
		try {
    	NodeList nodeList = XmlUtil.getXmlNodes(xml, "Detail") ;
    	
    	details = new ArrayList<BNS06042503QDetail>(); ;
    	if (nodeList != null && nodeList.getLength() > 0) {
//    		System.out.println("nodeList.getLength()=" + nodeList.getLength());
    		for (int cnt=0; cnt< nodeList.getLength(); cnt++) {
    			Node node = nodeList.item(cnt) ;
				 if (node.getNodeType() == Node.ELEMENT_NODE) {
				    Element ele = (Element) node;
				    BNS06042503QDetail detail = new BNS06042503QDetail() ;
				    detail.setReasonNo(XmlUtil.getNodeValue(ele, "ReasonNo"));
				    detail.setSetFlag(XmlUtil.getNodeValue(ele, "SetFlag"));
				    detail.setReasonType(XmlUtil.getNodeValue(ele, "ReasonType"));
				    
				    details.add(detail) ;
				 }
    		}
    	} 
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return details;
	}


	public class BNS06042503QDetail {
		private String ReasonNo ;
		private String Remark ;
		private String SetFlag ;
		private String ReasonType ;
		public String getReasonNo() {
			return ReasonNo;
		}
		public void setReasonNo(String reasonNo) {
			ReasonNo = reasonNo;
		}
		public String getRemark() {
			return Remark;
		}
		public void setRemark(String remark) {
			Remark = remark;
		}
		public String getSetFlag() {
			return SetFlag;
		}
		public void setSetFlag(String setFlag) {
			SetFlag = setFlag;
		}
		public String getReasonType() {
			return ReasonType;
		}
		public void setReasonType(String reasonType) {
			ReasonType = reasonType;
		}
	}
	
	
	public static void main(String argc[]) {
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:esb=\"http://www.cosmos.com.tw/esb\" xmlns:mix=\"http://www.cosmos.com.tw/mix\" xmlns:kgib=\"http://www.cosmos.com.tw/kgib\"><SOAP-ENV:Header/><SOAP-ENV:Body><esb:MsgRs><Header><ClientId>EOPSYS</ClientId><ClientPwd></ClientPwd><ClientDt></ClientDt><TxnId>BNS06042503Q</TxnId><UUID>EOPSYS20191205163227393</UUID><Status><SystemId>BNS</SystemId><StatusCode>0</StatusCode><Severity>INFO</Severity><StatusDesc></StatusDesc></Status></Header><SvcRs xsi:type=\"kgib:BNS06042503QSvcRsType\"><BNSHeader><BranchNo>00960</BranchNo><TellerNo>90000130</TellerNo><JournalNo>000024500</JournalNo><Date>20191205</Date><Flag4>Z</Flag4><SubChannel>0</SubChannel><ChannelId>01</ChannelId><FlagX>X</FlagX><Workstation>000</Workstation><PreviewFlag> </PreviewFlag><Time>163227</Time></BNSHeader><BNS060429><CifNo>00000000012061197</CifNo><Detail><ReasonNo>08</ReasonNo><SetDate>29/11/2019</SetDate><AcctNo>00000000000000000</AcctNo><SetFlag>J</SetFlag><ReDate>0000000000</ReDate><Remark>KGI異常等級3                        </Remark><TellerS>00014047</TellerS><TellerR>00000000</TellerR><ReasonType>0206</ReasonType></Detail><Detail><ReasonNo>06</ReasonNo><SetDate>29/11/2019</SetDate><AcctNo>00000000000000000</AcctNo><SetFlag>J</SetFlag><ReDate>0000000000</ReDate><Remark>KGI異常等級1                        </Remark><TellerS>00014047</TellerS><TellerR>00000000</TellerR><ReasonType>0334</ReasonType></Detail><Detail><ReasonNo>06</ReasonNo><SetDate>29/11/2019</SetDate><AcctNo>00000000000000000</AcctNo><SetFlag>J</SetFlag><ReDate>0000000000</ReDate><Remark>KGI異常等級1                        </Remark><TellerS>00014047</TellerS><TellerR>00000000</TellerR><ReasonType>0335</ReasonType></Detail><Detail><ReasonNo>06</ReasonNo><SetDate>29/11/2019</SetDate><AcctNo>00000000000000000</AcctNo><SetFlag>J</SetFlag><ReDate>0000000000</ReDate><Remark>KGI異常等級1                        </Remark><TellerS>00014047</TellerS><TellerR>00000000</TellerR><ReasonType>0314</ReasonType></Detail><Detail><ReasonNo>07</ReasonNo><SetDate>29/11/2019</SetDate><AcctNo>00000000000000000</AcctNo><SetFlag>J</SetFlag><ReDate>0000000000</ReDate><Remark>KGI異常等級2                        </Remark><TellerS>00014047</TellerS><TellerR>00000000</TellerR><ReasonType>0316</ReasonType></Detail><Detail><ReasonNo>06</ReasonNo><SetDate>29/11/2019</SetDate><AcctNo>00000000000000000</AcctNo><SetFlag>J</SetFlag><ReDate>0000000000</ReDate><Remark>KGI異常等級1                        </Remark><TellerS>00014047</TellerS><TellerR>00000000</TellerR><ReasonType>0336</ReasonType></Detail><CustName>魏ＸＸ                                                                                 </CustName><IDNo>A223306875              </IDNo><IDType>11</IDType></BNS060429></SvcRs></esb:MsgRs></SOAP-ENV:Body></SOAP-ENV:Envelope>" ;
		
		BNS06042503Q bns06042503Q = new BNS06042503Q() ;
		List<BNS06042503Q.BNS06042503QDetail> details03 = bns06042503Q.parseXml(xml) ;
    	if (details03 != null) {
	    	for (BNS06042503QDetail detail : details03) {
	    		System.out.println("RCD=" + detail.getReasonNo() + ", IncidentType=" + detail.getReasonType() + ", RM=" + detail.getSetFlag());
	    		if (SystemConst.ESB_REASON_CODE_08.equals(detail.getReasonNo())
	    			&& (SystemConst.ESB_INCIDENT_TYPE_0200.equals(detail.getReasonType())|| SystemConst.ESB_INCIDENT_TYPE_0206.equals(detail.getReasonType()))
	    			&& (SystemConst.ESB_RM_J.equalsIgnoreCase(detail.getSetFlag()) || SystemConst.ESB_RM_C.equalsIgnoreCase(detail.getSetFlag()))
	    				) {
	    			System.out.println("found");
	    		}
	    	}
    	}
		
		
		
	}
}
