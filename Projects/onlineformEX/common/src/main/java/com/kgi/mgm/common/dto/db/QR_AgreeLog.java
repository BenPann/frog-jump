package com.kgi.mgm.common.dto.db;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class QR_AgreeLog {
    private String Serial;
    private String ChannelId;
    private String DepartId;
    private String IdNo;
    private String MemberId;
    private String AgreeTime;
    
}














