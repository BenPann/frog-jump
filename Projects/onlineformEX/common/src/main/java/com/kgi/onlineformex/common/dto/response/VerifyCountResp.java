package com.kgi.onlineformex.common.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VerifyCountResp {

    private String authErrCount;
}