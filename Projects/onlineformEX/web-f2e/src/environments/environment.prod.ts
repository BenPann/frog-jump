export const environment = {
  production: true,
  turnOnRouterGuard: true,
  useFakeKgiApi: false,
  useFakeBackendApi: false,
  useFakeOtp: false,
};
