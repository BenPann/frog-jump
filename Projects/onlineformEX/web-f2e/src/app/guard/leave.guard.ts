import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  CanDeactivate
} from '@angular/router';
import { Observable } from 'rxjs';
import { RouterHelperService } from '../shared/service/common/router-helper.service';
import { environment } from 'src/environments/environment';

export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable({
  providedIn: 'root'
})
export class LeaveGuard implements CanDeactivate<CanComponentDeactivate> {
  constructor(private routerHelperService: RouterHelperService) {}

  canDeactivate(
    component: CanComponentDeactivate,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    if (!environment.turnOnRouterGuard) {
      return true;
    } else if (!this.routerHelperService.canLeave) {
      return false;
    } else {
      this.routerHelperService.canLeave = false;
      return true;
    }
  }
}
