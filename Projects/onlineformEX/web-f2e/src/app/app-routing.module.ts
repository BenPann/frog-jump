import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';
import { EntryComponent } from './component/entry/entry/entry.component';
import { FormsModule } from '@angular/forms';
// 開戶頁
import { ErrorPageComponent } from './component/error-page/error-page.component';
import { QrComponent } from './component/entry/qr/qr.component';

const routes: Routes = [
  { path: '', component: PageNotFoundComponent },
  { path: 'CL/:token', component: EntryComponent },
  { path: 'qr/entry/:qrCode', component: QrComponent },
  {
    path: 'rpl',
    loadChildren: () =>
      import('./rpl-user/rpl-user.module').then(m => m.RplUserModule)
  },
  { path: 'error', component: ErrorPageComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'top' }),
    FormsModule
  ],
  exports: [RouterModule, FormsModule]
})
export class AppRoutingModule {}
