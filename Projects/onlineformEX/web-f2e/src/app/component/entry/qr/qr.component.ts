import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RouterHelperService} from '../../../shared/service/common/router-helper.service';
import {EntryService} from '../../../service/entry/entry.service';
import {ModalFactoryService} from '../../../shared/service/common/modal-factory.service';
import {ShortUrlDataView} from '../entry.interface';
import {takeWhile} from 'rxjs/operators';

@Component({
  selector: 'app-qr',
  templateUrl: './qr.component.html',
  styleUrls: ['./qr.component.scss']
})
export class QrComponent implements OnInit, OnDestroy {
  private alive = true;

  constructor(private route: ActivatedRoute,
              private routerHelperService: RouterHelperService,
              private entryService: EntryService,
              private modalFactoryService: ModalFactoryService) {
  }

  ngOnInit() {
    sessionStorage.clear();
    const qrCode = this.route.snapshot.params['qrCode'];
    console.log(qrCode);
    const shortUrlDataView: ShortUrlDataView = {
      channelID: '',
      departID: '',
      shortUrl: qrCode,
      member: '',
      entry: '',
      exp: '',
      token: '',
    };
    this.entryService.saveShortUrlData(shortUrlDataView).pipe(takeWhile(() => this.alive)).subscribe(data => {
      console.log(data);
      if (data.status === 0) {
        this.entryService.shortUrlData = data.result;
        this.routerHelperService.navigate(['init'], {queryParams: {entry: data.result.entry}});
      } else {
        this.modalFactoryService
          .modalDefault({
            message: data.message
          })
          .result.then(() => {
          this.routerHelperService.navigate(['error']);
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.alive = false;
  }


}
