export interface ShortUrlDataView {
  channelID: string;
  departID: string;
  shortUrl: string;
  member: string;
  entry: string;
  exp: string;
  token: string;
}

export interface ShortUrlDataRsp {
  ShortUrl: string;
  ChannelId: string;
  DepartId: string;
  Member: string;
  PProductType: string;
  PProductId: string;
  Entry: string;
  exp: string;
}
