import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {EntryService} from 'src/app/service/entry/entry.service';
import {switchMap, tap} from 'rxjs/operators';
import {ModalFactoryService} from '../../../shared/service/common/modal-factory.service';
import {ShortUrlDataView} from '../entry.interface';
import {RouterHelperService} from 'src/app/shared/service/common/router-helper.service';

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.scss']
})
export class EntryComponent implements OnInit {
  token: string;
  result: string;
  shortUrlData: ShortUrlDataView;

  constructor(
    private route: ActivatedRoute,
    private routerHelperService: RouterHelperService,
    private entryService: EntryService,
    private modalFactoryService: ModalFactoryService
  ) {
  }

  ngOnInit() {
    // 清除所有舊資料
    sessionStorage.clear();
    this.shortUrlData = {
      channelID: 'CL',
      departID: this.route.snapshot.queryParams['Dept'],
      entry: this.route.snapshot.queryParams['entry'],
      exp: this.route.snapshot.queryParams['exp'],
      member: this.route.snapshot.queryParams['memb'],
      shortUrl: '',
      token: ''
    };
    this.route.paramMap
      .pipe(
        tap((params: ParamMap) => (this.shortUrlData.token = params.get('token'))),
        switchMap((params: ParamMap) =>
          this.entryService.saveShortUrlData(this.shortUrlData)
        )
      )
      .subscribe(data => {
        if (data.status === 0) {
          this.entryService.shortUrlData = {
            ShortUrl: '',
            ChannelId: this.shortUrlData.channelID,
            DepartId: this.shortUrlData.departID,
            Member: this.shortUrlData.member,
            PProductType: '',
            PProductId: '',
            Entry: this.shortUrlData.entry,
            exp: this.shortUrlData.exp
          };
          this.routerHelperService.navigate(['init'], {queryParams: {entry: this.shortUrlData.entry}});
        } else {
          this.modalFactoryService
            .modalDefault({
              message: data.message
            })
            .result.then(() => {
            this.routerHelperService.navigate(['error']);
          });
        }
      });
  }
}
