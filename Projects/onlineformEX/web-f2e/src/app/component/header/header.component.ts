import { Component, OnInit } from '@angular/core';
import { ModalFactoryService } from '../../shared/service/common/modal-factory.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor(private modalFactoryService: ModalFactoryService) {}

  ngOnInit() {}

  showConsume() {
    this.modalFactoryService.modalDefault({
      message: '欲洽詢更多貸款方案，請洽客服專線：(02)8023-9088'
    });
  }
}
