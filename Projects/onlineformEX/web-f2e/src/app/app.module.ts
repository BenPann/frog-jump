import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EntryComponent } from './component/entry/entry/entry.component';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';
import { ShowMessageComponent } from './shared/component/show-message/show-message.component';
import { HttpTokenInterceptor } from './core/interceptor';
import { SpinnerOverlayComponent } from './shared/component/spinner-overlay/spinner-overlay.component';
import { SpinnerOverlayService } from './shared/service/common/spinner-overlay.service';
import { OverlayModule } from '@angular/cdk/overlay';
import { AngularCropperjsModule } from 'angular-cropperjs';
import { UploadCropperComponent } from './shared/component/upload-cropper/upload-cropper.component';
// 註冊語系資料
import { registerLocaleData } from '@angular/common';
import localeZhHant from '@angular/common/locales/zh-Hant';
import localeZhHantExtra from '@angular/common/locales/extra/zh-Hant';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalFactoryService } from './shared/service/common/modal-factory.service';

// 開戶頁
import { AddPhotoComponent } from './component/add-photo/add-photo.component';

import { RouterHelperService } from './shared/service/common/router-helper.service';
import { DatePipe } from '@angular/common';
import { fakeBackendProvider } from './mock/FakeBackendInterceptor';
import { SharedModule } from './shared/shared.module';
import { ShowImageComponent } from './shared/component/show-image/show-image.component';
import { ShowHtmlComponent } from './shared/component/show-html/show-html.component';
import { ErrorPageComponent } from './component/error-page/error-page.component';
import { LeaveGuard } from './guard/leave.guard';
import { TokenGuard } from './guard/token.guard';
import { QrComponent } from './component/entry/qr/qr.component';

const entryComp = [
  ShowMessageComponent,
  SpinnerOverlayComponent,
  UploadCropperComponent,
  ShowImageComponent,
  ShowHtmlComponent
];
registerLocaleData(localeZhHant, localeZhHantExtra);
@NgModule({
  declarations: [
    AppComponent,
    EntryComponent,
    PageNotFoundComponent,
    HeaderComponent,
    FooterComponent,
    AddPhotoComponent,
    ErrorPageComponent,
    QrComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularCropperjsModule,
    HttpClientModule,
    ReactiveFormsModule,
    CommonModule,
    OverlayModule,
    NgbModule,
    SharedModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'zh-Hant' },
    SpinnerOverlayService,
    ModalFactoryService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpTokenInterceptor,
      multi: true
    },
    RouterHelperService,
    DatePipe,
    fakeBackendProvider,
    LeaveGuard,
    TokenGuard
  ],
  entryComponents: [...entryComp],
  bootstrap: [AppComponent]
})
export class AppModule {}
