import { NgModule } from '@angular/core';

// Import
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RplUserRoutingModule } from './rpl-user-routing.module';
import { SharedModule } from '../shared/shared.module';

// Declaration
import { OtpComponent } from './component/otp/otp.component';
import { PersonalIdentifyComponent } from './component/personal-identify/personal-identify.component';
import { ChooseContractsComponent } from './component/choose-contracts/choose-contracts.component';
import { DataConfirmComponent } from './component/data-confirm/data-confirm.component';
import { AllFinishComponent } from './component/all-finish/all-finish.component';
import { MobileLoginComponent } from './component/mobile-login/mobile-login.component';
import { ContractNotFoundComponent } from './component/contract-not-found/contract-not-found.component';
import { ContractTermsComponent } from './component/contract-terms/contract-terms.component';
import { RplMockPageComponent } from './component/rpl-mock-page/rpl-mock-page.component';

@NgModule({
  declarations: [
    OtpComponent,
    PersonalIdentifyComponent,
    ChooseContractsComponent,
    DataConfirmComponent,
    ContractTermsComponent,
    AllFinishComponent,
    MobileLoginComponent,
    ContractNotFoundComponent,
    RplMockPageComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RplUserRoutingModule,
  ]
})
export class RplUserModule { }
