export class Miscellansous {
  // DARK:
  // DEPR:
  // MOCK:
  // INIT:
  // TEMP:
  // HACK:
  // MODE:
  // ITEM:
  // NOTE:
  // TEST:
  // COND:
  // CHAP:
  // STEP:
  // WARN:
  // KING:
  // BLOCK:
  // WTF?

  /*
  "todohighlight.isEnable": true,
  "todohighlight.isCaseSensitive": true,
  "todohighlight.keywords": [
      "DEFAULT:",
      {
          "text": "DARK:",
          "color": "White",
          "backgroundColor": "Black",
          "overviewRulerColor": "Silver",
      },
      {
          "text": "DEPR:",
          "color": "AliceBlue",
          "backgroundColor": "MidnightBlue",
          "overviewRulerColor": "Silver",
      },
      {
          "text": "MOCK:",
          "color": "AliceBlue",
          "backgroundColor": "DarkViolet",
          "overviewRulerColor": "Silver",
      },
      {
          "text": "INIT:",
          "color": "AliceBlue",
          "backgroundColor": "RebeccaPurple",
          "overviewRulerColor": "Gold",
      },
      { 
          "text": "TEMP:",
          "color": "AliceBlue",
          "backgroundColor": "SlateBlue",
          "overviewRulerColor": "Gold",
      },
      {
          "text": "HACK:",
          "color": "AliceBlue",
          "backgroundColor": "RoyalBlue",
          "overviewRulerColor": "Gold",
      },
      { 
          "text": "MODE:",
          "color": "SeaShell",
          "backgroundColor": "MediumTurquoise",
          "overviewRulerColor": "Gold",
      },
      {
          "text": "ITEM:",
          "color": "Snow",
          "backgroundColor": "Teal",
      },
      {
          "text": "NOTE:",
          "color": "Snow",
          "backgroundColor": "OliveDrab",
          "overviewRulerColor": "Silver",
      },
      { 
          "text": "TEST:",
          "color": "Maroon",
          "backgroundColor": "SeaShell",
          "overviewRulerColor": "Gold",
      },
      {
          "text": "COND:",
          "color": "Snow",
          "backgroundColor": "GoldenRod",
          "overviewRulerColor": "Gold",
      },
      {
          "text": "CHAP:",
          "color": "Snow",
          "backgroundColor": "Peru",
      },
      {
          "text": "STEP:",
          "color": "Wheat",
          "backgroundColor": "Sienna",
          "overviewRulerColor": "Gold"
      },
      { 
          "text": "WARN:",
          "color": "PapayaWhip",
          "backgroundColor": "Tomato",
          "overviewRulerColor": "Crimson",
      },
      {
          "text": "KING:",
          "color": "PapayaWhip",
          "backgroundColor": "Crimson",
          "overviewRulerColor": "Crimson",
      },
      {
          "text": "BLOCK:",
          "color": "FireBrick",    
          "backgroundColor": "NavajoWhite",
          "overviewRulerColor": "Sliver",
          "border": "1px solid Peru",
          "cursor": "crosshair",
      },
      {
          "text": "WTF?",
          "color": "MidnightBlue",
          "font-weight": "bold",
          "backgroundColor": "LightCoral",
          "overviewRulerColor": "Crimson",
          "border": "1px double LightGray",
      },
      {
          "text": "WARNING:",
          "color": "Crimson",
          "backgroundColor": "Gold",
          "isWholeLine": true,
          "border": "2px dashed Crimson",
          "border-width": "medium",
      },
      {
          "text": "WTF!?",
          "color": "MidnightBlue",
          "font-weight": "bold",
          "backgroundColor": "LightCoral",
          "overviewRulerColor": "Crimson",
          "isWholeLine": true,
          "border": "4px double LightGray",
      },
      {
      "text": "SECTOR:",
          "color": "FireBrick",    
          "backgroundColor": "NavajoWhite",
          "overviewRulerColor": "Sliver",
          "isWholeLine": true,
          "border": "1px solid Peru",
          "cursor": "crosshair",
      }
  ],
  */
}
