import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-contract-not-found',
  templateUrl: './contract-not-found.component.html',
  styleUrls: ['./contract-not-found.component.css']
})
export class ContractNotFoundComponent implements OnInit, OnDestroy {

  stepStr = 2;
  private alive = true;

  constructor(
  ) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.alive = false;
  }


  onSubmit() {
    console.log('====> Access the onSubmit().... ');
  }

}
