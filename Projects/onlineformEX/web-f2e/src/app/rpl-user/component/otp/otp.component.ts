import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { interval } from 'rxjs';
import { takeWhile, tap } from 'rxjs/operators';
import { TermsInput } from 'src/app/interface/resultType';
import { UserContractView } from 'src/app/service/a-system/a-system.interface';
import { ASystemService } from 'src/app/service/a-system/a-system.service';
import { OtpService } from 'src/app/service/otp/otp.service';
import { RplRouterDirectService } from 'src/app/service/router/rpl-router-direct.service';
import { TermsService } from 'src/app/service/terms/terms.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { FormCheckService } from 'src/app/shared/service/common/form-check.service';
import { ModalFactoryService } from 'src/app/shared/service/common/modal-factory.service';


@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss']
})
export class OtpComponent implements OnInit {

  // INIT:
  stepStr = 3;
  otpForm: FormGroup;
  rplContractData = this.ass.ContractData;

  // VERIFY CODE 計秒
  secText = 60; // how long can you resend verify code again
  sendSuccess = false;  // flag of button lock
  private resetTimer = true;

  private alive = true; // for pipe alive

  // COND: OTP
  phoneRe = this.rplContractData.phone.length > 10
            ? this.rplContractData.phone.substr(0, 10)
            : this.rplContractData.phone;
  errorMsgPhone: string;
  isInputPhoneNumberShow = true;

  // TERMS
  termsDiv: TermsInput[];


  constructor(
    public fcs: FormCheckService,
    private fb: FormBuilder,
    private os: OtpService,
    private mfs: ModalFactoryService,
    private rrds: RplRouterDirectService,
    private ts: TermsService,
    private ass: ASystemService,
    private uds: UserDataService,
  ) { }


  ngOnInit(): void {
    console.log('====> Access the OTP-page ngOnInit().... ');

    this.nukePreviousPage();

    this.otpForm = this.fb.group({
      inputcaptcha: [
        '',
        [
          Validators.required,
          Validators.minLength(6),
          Validators.pattern(/^-?(0|[0-9]\d*)?$/)
        ]
      ],
      agreement: [
        '', Validators.required,
      ],
    });

    this.sendOtp(this.rplContractData);
    // this.termsDiv = this.setMockTerms();
    this.termsDiv = this.setMockTermsMk2();
    console.log(this.termsDiv);
  }


  // SECTOR: REQUEST
  /**
   * Send OTP-mail by API to service from OtpService
   */
  sendOtp(info: UserContractView) {
    // 本次已進行過OTP驗證的動作，不再進行OTP驗證
    if (this.rrds.passOTP !== 'Y') {
      console.log('====>> $POST Access the sendOtp()....');
      this.os
        .sendOtp(this.phoneRe)
        .pipe(takeWhile(() => this.alive))
        .subscribe(data => {
          this.doSendOtpHandler(data);
        });
    }
  }


  onSubmit() {
    console.log('====>> $POST Access the onSubmit()....');

    // COND: form
    const phone   = this.phoneRe;
    const captcha = this.otpForm.value.inputcaptcha;

    this.os
      .checkOtp(phone, captcha)
      .pipe(takeWhile(() => this.alive))
      .subscribe(
        data => {
          this.onSubmitHandler(data);
        }
      );
  }


  /**
   * 重領驗證碼
   */
  sendSMS(): void {
    this.otpForm.get('inputcaptcha').setValue('');
    this.sendOtp(this.rplContractData);
  }


  goPreviousPage() {
    this.rrds.backStep();
  }


  // SECTOR: METHOD
  doSendOtpHandler(data: any) {
    console.log('====<< Access the doSendOtpHandler()....');

    if (data.status === 0) {  // status is zero
      if (data.result.code === '0000') {  // send successful
        this.mfs.modalDefault({
          message: '已發送簡訊',
          customerWidth: '50'
        }).result.then(res => {
          console.log('====<<< OTP result is ok');
        }).catch(rea => {
          console.log('====<<< OTP failed');
        });
        this.sendSuccess = true;
        this.setTime(60); // 計時
        return;
      }

      this.mfs.modalDefault({
        message: data.result.message
      });
      this.secText = 0; // send timer zero, can send mail again
    }

    if (data.status !== 0) {
      this.mfs.modalDefault({
        message: data.message
      });
      this.secText = 0; // send timer zero, can send mail again
    }

  }


  onSubmitHandler(data: any) {
    console.log('====< RECEIVE checkOtp response....');

    if (data.status === 0) {

      // CHAP: SUCCESSFUL PASS OTP
      if (data.result.code === '0000') {
        // STEP: 2.保存OTP驗證結果
        this.rrds.passOTP = 'Y';
        // HACK: decide how to router by protocol-TYPE
        const psrt = this.ass.ContractData.caseType;
        this.rrds.nextStep(psrt === '2' || psrt === '3' ? 'C' : null);
      }

      // CHAP: FAILED PASS OTP
      if (data.result.code !== '0000') {
        this.mfs.modalDefault({
          message: data.result.message
        });
      }
    }

    if (data.status !== 0) {
      // CHAP: FAILED CONNECT
      this.mfs.modalDefault({
        message: data.message
      });
    }
  }


  clearErrorMsg(): void {
    this.errorMsgPhone = '';
  }


  isDisabled(): boolean {
    return !this.otpForm.controls['agreement'].value || this.otpForm.invalid;
  }


  setTime(tempT: number): void {
    this.resetTimer = true;
    let total = tempT;
    this.secText = total;
    interval(1000)
      .pipe(
        takeWhile(() => total-- !== 0),
        takeWhile(() => this.resetTimer),
        takeWhile(() => this.alive),
        tap(() => (this.secText = total))
      )
      .subscribe();
  }


  // SECTOR: TERMS
  showTerms(termsName: string) {
    this.ts
      .getTerms(termsName)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        let termText = '';
        try {
          // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
          const json = JSON.parse(data);
          termText = '查詢條款失敗';
        } catch (error) {
          termText = data;
        }
        this.mfs.modalHtml({message: termText});
      });
  }


  initTerms() {
    this.ts.getTermList()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.termsDiv = data.filter(terms => terms.type === '2');
      });
  }


  showConsume() {
    this.mfs.modalDefault({
      message: '客服專線：(02)8023-9088',
    });
  }


  // SECTOR: MOCK: TERMS
  setMockTerms(): TermsInput[] {
    return [
      {
        termTitle: 'Alpha',
        termName: 'Beta',
        type: 'Gamma',
      },
      {
        termTitle: 'Delta',
        termName: 'Epsilon',
        type: 'Zeta',
      },
      {
        termTitle: 'Eta',
        termName: 'Theta',
        type: 'Iota',
      }
    ];
  }


  setMockTermsMk2(): TermsInput[] {
    return [
      {
        termTitle: '電話網路行動銀行約定條款',
        termName: 'ebankingDealItems',
        type: 'ebankingDealItems',
      }
    ];
  }


  nukePreviousPage() {
    // STEP: STOP GO BACKWARD
    history.pushState(null, null);
    window.addEventListener('popstate', function() {
      history.pushState(null, null);
    });
  }

}
