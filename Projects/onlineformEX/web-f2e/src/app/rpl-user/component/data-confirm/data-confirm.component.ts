import { ModifyRange } from './../../../service/a-system/a-system.interface';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RplRouterDirectService } from 'src/app/service/router/rpl-router-direct.service';
import { ASystemService } from 'src/app/service/a-system/a-system.service';
import { UserContractView } from 'src/app/service/a-system/a-system.interface';
import { formatCurrency } from '@angular/common';


@Component({
  selector: 'app-data-confirm',
  templateUrl: './data-confirm.component.html',
  styleUrls: ['./data-confirm.component.scss']
})
export class DataConfirmComponent implements OnInit, AfterViewInit {

  // INIT:
  stepStr = 3;
  dataForm: FormGroup;
  rangeArr = [];

  status: UserContractView = this.ass.ContractData;
  creditCurr = this.status.creditCur;
  creditModify = this.status.creditModify;
  userEmail  = this.status.email.trim();


  constructor(
    private fb: FormBuilder,
    private rrds: RplRouterDirectService,
    private ass: ASystemService,
  ) {}


  ngOnInit(): void {
    this.nukePreviousPage();
    this.dataForm = this.fb.group({
      creditCur: [
        formatCurrency(+this.creditCurr, 'zh-Hant', '$', '', '4.0'),
      ],
      creditModify: [],
      email:    [
        this.userEmail,
        [
          Validators.email,
          Validators.required,
        ]
      ],
    });
  }

  ngAfterViewInit(): void {
    const reqSome: ModifyRange = {
      modifyRange: 'range'
    };

    this.ass.getModifyRange(reqSome)
      .subscribe(
        data => this.setCreditRangeOptions(data, JSON.parse(sessionStorage.getItem('creditModifyNative')))
      );
  }


  // SECTOR: REQUEST
  onSubmit() {
    this.status.creditModify = this.dataForm.controls['creditModify'].value;
    this.status.email = this.dataForm.controls['email'].value;
    this.ass.ContractData = this.status;
    this.ass.setUserMailAndCreditModify(this.status.email, this.status.creditModify)
      .subscribe(data => {
        if (data.status === 0) {
          this.rrds.nextStep();
        }
      });
  }


  goPreviousPage() {
    const psrt = this.ass.ContractData.caseType;
    this.rrds.backStep(psrt === '2' ? 'C' : null);
  }


  isDisabled(): boolean {
    return !this.dataForm.valid;
  }


  formStatus() {
    console.log(this.dataForm);
  }


  // SECTOR: METHOD
  /**
   * Create credit range
   *
   *    Item
   *    1. arr1: select from DB
   *    2. arr2: just name from control
   *
   *    Step
   *    1. compare with creditCurr with arr1, only choose can be greater than compare.
   *    2. represent with rangeArr, saft with creditCurr greater than any arr1's element.
   *
   * @param creditCurr from this.ass.ContractData.creditCurr
   */
  setCreditRangeOptions(data: any, creditModify: string) {
    console.log('====< Receive DB response of modify range....');
    const range: number = +data.KeyValue * 10000;
    for ( let modify = range; modify <= 1000000; modify += range) {
      if (modify >= +creditModify) {
        this.rangeArr.push( { key: modify, value: modify } );
      }
    }

    // SAFT
    if (this.rangeArr.length === 0) {
      this.rangeArr.push({ key: 'SAFT OPTION', value: '100000' });
    }

    this.dataForm.controls['creditModify'].setValue(this.rangeArr[this.rangeArr.length - 1].key);
  }


  nukePreviousPage() {
    history.pushState(null, null);
    window.addEventListener('popstate', function() {
      history.pushState(null, null);
    });
  }


}
