import { UserDataService } from './../../../service/user-data/user-data.service';
import { ASystemService } from './../../../service/a-system/a-system.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rpl-mock-page',
  templateUrl: './rpl-mock-page.component.html',
  styleUrls: ['./rpl-mock-page.component.scss']
})
export class RplMockPageComponent implements OnInit {

  constructor(
    private ass: ASystemService,
    private uds: UserDataService,
  ) { }

  ngOnInit(): void {
  }

  onFuncAlpha(): void {
    console.log('====> onFuncAlpha().... ');
    this.ass.mockApiLog()
      .subscribe(
        data => console.log(data)
      );
  }

  onFuncBeta(): void {
    
  }

}
