import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RplMockPageComponent } from './rpl-mock-page.component';

describe('RplMockPageComponent', () => {
  let component: RplMockPageComponent;
  let fixture: ComponentFixture<RplMockPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RplMockPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RplMockPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
