import { ModalFactoryService } from 'src/app/shared/service/common/modal-factory.service';
import { UserContractRequest } from 'src/app/service/a-system/a-system.interface';
import { takeWhile } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TermsInput, WebResult } from 'src/app/interface/resultType';
import { ASystemService } from 'src/app/service/a-system/a-system.service';
import { RplRouterDirectService } from 'src/app/service/router/rpl-router-direct.service';
import { TermsService } from 'src/app/service/terms/terms.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';


@Component({
  selector: 'app-contract-terms',
  templateUrl: './contract-terms.component.html',
  styleUrls: ['./contract-terms.component.scss']
})
export class ContractTermsComponent implements OnInit {

  // INIT:
  stepStr = 3;
  pcForm: FormGroup;

  isLookdocOpenR1 = false;
  isLookdocOpenR2 = false;
  termName: String;
  hideLeft = true;
  hideRow = true;
  termsDivR1: boolean;
  termsDivR2: boolean;
  termTextR1: String;
  termTextR2: String;


  constructor(
    private fb: FormBuilder,
    private rrds: RplRouterDirectService,
    private rhs: RouterHelperService,
    private ts: TermsService,
    private ass: ASystemService,
    private mfs: ModalFactoryService,
  ) {}


  ngOnInit(): void {
    this.nukePreviousPage();

    this.pcForm = this.fb.group({
      agreementR1: [],
      agreementR2: [],
    });

    // STEP:
    this.initTerms();
  }


  // SECTOR: REQUEST
  onSubmit() {
    console.log('====> Access the onSubmit()....');
    const contractModify: UserContractRequest = {
      funcType: 'U',
      docType: this.ass.ContractData.caseType,
      userId: this.ass.ContractData.customerId,
      account: '',
      shortUrl: '',
      creditModify: this.ass.ContractData.creditModify,
    };

    this.ass.allFinish(contractModify)
      .subscribe( data => {
        this.doAllFinishHandler(data);
      });
  }


  goPreviousPage() {
    console.log('====> Access the goPreviousPage()....');
    const caseType = this.ass.ContractData.caseType;
    switch (caseType) {
      case '1':
      case '4':
        this.rrds.backStep();
        break;
      case '2':
        this.rhs.navigate(['rpl/otp']);
        break;
      default:
        break;
    }
  }


  initTerms(): void {
    switch (this.ass.ContractData.caseType) {
      case '1':
        this.termsDivR1 = true;
        this.getCreditModifyTerm();
        break;
      case '2':
        this.termsDivR2 = true;
        this.getCreditModifyTerm();
        break;
      case '4':
        this.termsDivR1 = true;
        this.termsDivR2 = true;
        this.getCreditModifyTerm();
        break;
      default:
        break;
    }
  }


  // SECTOR: METHOD
  isDisabled() {
    switch (this.ass.ContractData.caseType) {
      case '1':
        return !this.pcForm.controls['agreementR1'].value;
      case '2':
        return !this.pcForm.controls['agreementR2'].value;
      case '4':
        return !this.pcForm.controls['agreementR1'].value || !this.pcForm.controls['agreementR2'].value;
      default:
        break;
    }
  }


  getCreditModifyTerm() {
    this.ass.getCreditModifyTermDetail()
      .subscribe(data => {
        this.termTextR1 = data.result.creditModifyTerm[0];
        this.termTextR2 = data.result.creditModifyTerm[1];
      }
    );
  }


  nukePreviousPage() {
    history.pushState(null, null);
    window.addEventListener('popstate', function() {
      history.pushState(null, null);
    });
  }


  doAllFinishHandler(data: WebResult) {
    switch (data.status) {
      case 0:
        this.ass.ContractData = data.result[0];
        this.rrds.passOTP = 'N';
        this.rrds.nextStep();
        break;
      case 99:
        this.mfs.modalDefault(
          { message: data.message })
        .result.then(_ =>
          this.rhs.navigate(['rpl/pi']));
        break;
      default:
        this.mfs.modalDefault(
          { message: data.message })
        .result.then(_ =>
          this.rhs.navigate(['rpl/pi']));
        break;
    }
  }


}
