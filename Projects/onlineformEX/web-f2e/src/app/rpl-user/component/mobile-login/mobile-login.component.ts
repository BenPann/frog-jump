import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { WebResult } from 'src/app/interface/resultType';
import { MoblieLoginRequest } from 'src/app/service/a-system/a-system.interface';
import { ASystemService } from 'src/app/service/a-system/a-system.service';
import { RplRouterDirectService } from 'src/app/service/router/rpl-router-direct.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ModalFactoryService } from 'src/app/shared/service/common/modal-factory.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';


@Component({
  selector: 'app-mobile-login',
  templateUrl: './mobile-login.component.html',
  styleUrls: ['./mobile-login.component.css']
})
export class MobileLoginComponent implements OnInit {


  // INIT:
  stepStr = 1;
  alive = true;
  token: string;


  constructor(
    private route: ActivatedRoute,
    private rrds: RplRouterDirectService,
    private rhs: RouterHelperService,
    private mfs: ModalFactoryService,
    private uds: UserDataService,
    private ass: ASystemService,
  ) { }


  ngOnInit(): void {
    this.route.paramMap
      .subscribe(
        (param: ParamMap) => {
          this.token = param.get('token');
          const req: MoblieLoginRequest = {
            token: this.token
          };
          console.log('====> Token this acquired as: ' + this.token);
          this.ass.doLoginFromMobile(req)
          .subscribe(
            (res) => {
              this.doLoginResultHandler(res);
            }
          );
        }
      );
  }


  // SECTOR: METHOD
  doLoginResultHandler(res: WebResult) {
    if (res.status === 0) {
      // COND: CLEAR preview data when login successful
      sessionStorage.clear();
      // ITEM: RECORD UserData
      this.uds.token = res.result.token;
      this.uds.cifInfo = res.result.cifinfo;
      return this.rhs.navigate(['rpl/list']);
    }

    // CHAP: FAILED
    if (res.status !== 0) {
      return this.mfs.modalDefault({message: res.message});
    }

  }


}
