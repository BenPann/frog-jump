import { ModalFactoryService } from 'src/app/shared/service/common/modal-factory.service';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ASystemService } from 'src/app/service/a-system/a-system.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { RplRouterDirectService } from 'src/app/service/router/rpl-router-direct.service';


@Component({
  selector: 'app-all-finish',
  templateUrl: './all-finish.component.html',
  styleUrls: ['./all-finish.component.css']
})
export class AllFinishComponent implements OnInit {

  // INIT:
  stepStr = 4;
  showGreenCheck = true;
  productName;
  title = '恭喜你，完成契變書簽署！';
  contractStatus = this.ass.ContractData;
  dateStr: string;
  dateShow = new Date();
  typeA;
  typeB;
  typeC;

  // ITEM: 同意契變書內容
  @ViewChild('data1', { static: true })
  private data1: TemplateRef<any>;
  // ITEM: 同意調額同意書
  @ViewChild('data2', { static: true })
  private data2: TemplateRef<any>;


  constructor(
    private rhs: RouterHelperService,
    private ass: ASystemService,
    private rrds: RplRouterDirectService,
    private mfs: ModalFactoryService,
  ) { }


  ngOnInit(): void {
    this.nukePreviousPage();
    this.dateStr = this.ass.ContractData.dateModify;
    this.productName = this.ass.ContractData.manageStore === '8861' ? '速還金' : '靈活卡';
    this.dateShow = new Date(+this.dateStr.substr(0, 4), +this.dateStr.substr(4, 2)-1, +this.dateStr.substr(6));
  }


  // SECTOR: REQUEST
  onSubmit() {
    this.rhs.navigate(['rpl/pi']);
  }


  // SECTOR: METHOD
  getTemplateSetV1() {
    console.log(this.ass.ContractData);
    return ['1', '4'].some(
      e => e === this.ass.ContractData.caseType
    ) ? this.data1 : null;
  }


  getTemplateSetV2() {
    console.log(this.ass.ContractData);
    return ['2', '4'].some(
      e => e === this.ass.ContractData.caseType
    ) ? this.data2 : null;
  }


  nukePreviousPage() {
    // STEP: STOP GO BACKWARD
    history.pushState(null, null);
    window.addEventListener('popstate', function() {
      history.pushState(null, null);
    });
  }

}
