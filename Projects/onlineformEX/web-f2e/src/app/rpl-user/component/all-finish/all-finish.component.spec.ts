import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllFinishComponent } from './all-finish.component';

describe('AllFinishComponent', () => {
  let component: AllFinishComponent;
  let fixture: ComponentFixture<AllFinishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllFinishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllFinishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
