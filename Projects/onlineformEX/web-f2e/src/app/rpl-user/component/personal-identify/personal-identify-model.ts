import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { TermsInput, TermsView } from 'src/app/interface/resultType';

export class BirthClass {
    minDate: NgbDateStruct;
    maxDate: NgbDateStruct;
    today: Date;
    termsDiv: TermsInput[];
    termsLink: TermsInput[];
    terms: TermsView;
    uniqType: string;
}
