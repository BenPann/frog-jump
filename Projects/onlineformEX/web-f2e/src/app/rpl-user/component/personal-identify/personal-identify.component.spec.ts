import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalIdentifyComponent } from './personal-identify.component';

describe('PersonalIdentifyComponent', () => {
  let component: PersonalIdentifyComponent;
  let fixture: ComponentFixture<PersonalIdentifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalIdentifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalIdentifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
