import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { takeWhile } from 'rxjs/operators';
import { WebResult } from 'src/app/interface/resultType';
import { UserLoginInfoRequest, UserLoginResponseMk1 } from 'src/app/service/a-system/a-system.interface';
import { ASystemService } from 'src/app/service/a-system/a-system.service';
import { RplRouterDirectService } from 'src/app/service/router/rpl-router-direct.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ModalFactoryService } from 'src/app/shared/service/common/modal-factory.service';
import { idCardValidator } from 'src/app/validator/idCard-validator';
import { BirthClass } from './personal-identify-model';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';


@Component({
  selector: 'app-personal-identify',
  templateUrl: './personal-identify.component.html',
  styleUrls: ['./personal-identify.component.scss']
})
export class PersonalIdentifyComponent implements OnInit, OnDestroy {


  // INIT: parameter
  stepStr = 1;
  // NOTE: 發出值，直到提供的表達式結果為 false。接收的是 function ，一旦返回值為false 就完成.'
  private alive = true;
  piForm: FormGroup;
  birthC = new BirthClass();

  constructor(
    private fb: FormBuilder,
    private rrds: RplRouterDirectService,
    private ass: ASystemService,
    private mfs: ModalFactoryService,
    private df: NgbDateParserFormatter,
    private ar: ActivatedRoute,
    private uds: UserDataService,
    private rhs: RouterHelperService,
  ) { }


  ngOnInit(): void {
    // INIT: ngb birth & form
    this.initBirthDatePicker();
    this.piForm = this.fb.group({
      userid:   ['', [ Validators.required, idCardValidator(), ], ],
      birthday: ['', [ Validators.required, ], ],
    });
  }


  /**
   * According native usage, alive will only turn false at onDestory,
   * that meaning this is control of observable flag for page
   */
  ngOnDestroy(): void {
    this.alive = false;
    console.log(this.alive);
  }


  // SECTOR: REQUEST
  /**
   * onSubmit()
   *
   *    STEP: LOGIN
   *    1. Login => Check A-System by idno
   *    2. Behave => Call "GMSCSMT24R2" Api to connect at A-System receive the user info
   *    3. API => ass.doASystemLoginCheck(req: UserLoginInfoRequest)
   *    ITEM: 1. browseLog => auto
   *    ITEM: 2. Login => to get data from A-System
   *    ITEM: 3. ApiLog => as record what api you did in here
   *    ITEM: 4. EntryData => user-agent detail
   */
  onSubmit() {
    console.log('====> Access the onSubmit()....');
    const req = this.getUserLoginInitRequest();
    this.ass.doLoginCheckByASystem(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(
        (data: WebResult) => {
          this.doLoginResultHandler(req, data);
        }
      );
  }


  // SECTOR: METHOD
  doLoginResultHandler(req: UserLoginInfoRequest, res: UserLoginResponseMk1) {
    console.log('====<< Access the doLoginHandler()....');

    // CHAP: SUCCESS
    if (res.status === 0 || res.status === 1) {
      if(res.result.asystem[0]){
        const so2419 = res.result.asystem[0].SO2419;
        if (so2419 === 'X045' || so2419 === 'X047' || so2419 === 'X052' || so2419 === 'X053') {
          this.errorHandler(so2419);
        }
      }
      // HACK: save entry data? COND: 登入成功之前 把所有資料先清空
      sessionStorage.clear();

      // ITEM: save UserData into sessionStorage
      this.uds.userLoginData = req;
      this.uds.token = res.result.token;

      const arrAS = res.result.asystem;
      arrAS.forEach( e => {
        return e.caseType.trim()
          ? this.rrds.nextStep()
          : this.rhs.navigate(['rpl/contractNotFound']);
      });
    }

    // CHAP: FAILED
    if (res.status !== 0) {
      return this.mfs.modalDefault({message: res.message});
    }
  }

  errorHandler( errSo2419 : string) {
    let messageText = '很抱歉！今日為您的繳款日，請您於繳款後再進行調整額度作業，若有疑義，請致電本行免付費專線：0800-255-777';
    if (errSo2419){
      if (errSo2419 === 'X045' || errSo2419 === 'X047' ) {
        messageText = '很抱歉！很抱歉因您貸款延滯中本功能無法使用，若有疑義，請電：0800-255-777';
        //errSo2419 = 'X045';
      } else if (errSo2419 === 'X052' || errSo2419 === 'X053' ) {
        messageText = '很抱歉！今日為您的繳款日，請您於繳款後再進行調整額度作業，若有疑義，請致電本行免付費專線：0800-255-777';
        //errSo2419 = 'X052';
      }
    }
    this.mfs.modalDefault({message: messageText}, errSo2419);
  }


  /**
   * 身份證轉大寫
   *
   * @param event input entry to get personal id value
   */
  turnValueUpperCase(event: any): void {
    if (event.target.value) {
      event.target.value = event.target.value.trim().toUpperCase();
    }
  }


  /**
   * Button locker
   */
  isDisabled(): boolean {
    return !(this.piForm.valid);
  }


  // SECTOR: SETTING
  /**
   * format data type by date:{Y:M:D:} to single string
   */
  getUserLoginInitRequest(): UserLoginInfoRequest {
    const dateString = this.df
      .format(this.piForm.controls['birthday'].value)
      .split('-')
      .join('');
    const req = {
      idno:     this.piForm.controls['userid'].value,
      birthday: dateString,
      account: 'A',
      url: 'A',
      entry: this.ar.snapshot.queryParams['entry'] ? this.ar.snapshot.queryParams['entry'] : '',
      uniqType: '13',
    };
    return req;
  }


  initBirthDatePicker(): void {
    this.birthC.uniqType = '13';
    this.birthC.today = new Date();
    const y = this.birthC.today.getFullYear();
    const m = this.birthC.today.getMonth() + 1;
    const d = this.birthC.today.getDate();
    this.birthC.minDate = {
      year: y - 100,
      month: m,
      day: d
    };
    this.birthC.maxDate = {
      year: y,
      month: m,
      day: d
    };
  }


}
