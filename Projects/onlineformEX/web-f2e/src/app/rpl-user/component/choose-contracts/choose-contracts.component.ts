import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
import { WebResult } from 'src/app/interface/resultType';
import {
  UserContractResponseMk2,
  UserContractView, UserLoginInfoRequest
} from 'src/app/service/a-system/a-system.interface';
import { ASystemService } from 'src/app/service/a-system/a-system.service';
import { RplRouterDirectService } from 'src/app/service/router/rpl-router-direct.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ModalFactoryService } from 'src/app/shared/service/common/modal-factory.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';


@Component({
  selector: 'app-choose-contracts',
  templateUrl: './choose-contracts.component.html',
  styleUrls: ['./choose-contracts.component.scss']
})
export class ChooseContractsComponent implements OnInit {

  // INIT:
  stepStr = 2;
  private alive = true;
  contractForm: FormGroup;
  userInfo: UserLoginInfoRequest;
  itemsFromASystem$: UserContractResponseMk2;
  selectItem: UserContractView = { // contract model for init
    funcType: '',
    userType: '',
    caseType: '',
    customerId: '',
    manageStore: '',
    companyNum: '',
    departNum: '',
    memberNum: '',
    seq: '',
    userName: '',
    cardNum: '',
    projectId: '',
    creditCur: '',
    creditContract: '',
    creditModify: '',
    dateModify: '',
    email: '',
    phone: '',
    SO2419: '',
  };


  constructor(
    private fb: FormBuilder,
    private rrds: RplRouterDirectService,
    private rhs: RouterHelperService,
    private ass: ASystemService,
    private mfs: ModalFactoryService,
    private uds: UserDataService,
  ) { }


  ngOnInit(): void {
    // STEP: STOP GO BACKWARD
    history.pushState(null, null);
    window.addEventListener('popstate', function() {
      history.pushState(null, null);
    });

    this.contractForm = this.fb.group({});
    this.userInfo = this.uds.userLoginData;
    this.ass.getUserContractsByASystem(this.userInfo)
      .pipe(takeWhile(() => this.alive))
      .subscribe(
        (data: WebResult) => {
          console.log('====< Receive data from A-System ' + data);
          data.result.length > 0
              ? this.itemsFromASystem$ = data
              : this.errorHandler();
      });
  }

  // SECTOR: REQUEST
  onSubmit() {
    console.log('====> Access the onSubmit()....');
    this.ass.ContractData = this.selectItem;
    sessionStorage.setItem('creditModifyNative', JSON.stringify(this.selectItem.creditModify));
    this.doNavigateByType(this.selectItem);
  }


  goPreviousPage() {
    this.rrds.backStep();
  }


  // SECTOR: METHOD
  isDisabled(): boolean {
    return !this.selectItem;
  }


  buttonClick(item: UserContractView) {
    this.selectItem = item;
  }


  errorHandler( ) {
    this.mfs.modalDefault({
      message: '很抱歉！今日為您的繳款日，請您於繳款後再進行調整額度作業，若有疑義，請致電本行免付費專線：0800-255-777'
    }).result.then((res) => {
      this.rhs.navigate(['rpl/pi']);
    });
  }


  doNavigateByType(data: UserContractView) {
    switch (this.selectItem.caseType) {
      case '1':
      case '4':
        this.rrds.nextStep('');
        break;
      case '2':
        this.rrds.nextStep('C');
        break;
      case '3': case '5':
        // Error
        break;
    }
  }


  getNameByCaseType(selectItem: UserContractView): String {
    switch (selectItem.caseType) {
      case '1': return '契變書簽署';
      case '2': return '調額同意書簽署';
      case '3': return '補財力證明及調額同意書簽署';
      case '4': return '契變書簽署及調額同意書簽署';
      case '5': return '補財力證明及契變書簽署及調額同意書簽署';
    }
  }

}
