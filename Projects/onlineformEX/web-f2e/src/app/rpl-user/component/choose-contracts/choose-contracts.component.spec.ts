import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ChooseContractsComponent } from './choose-contracts.component';

describe('ChooseContractsComponent', () => {
  let component: ChooseContractsComponent;
  let fixture: ComponentFixture<ChooseContractsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseContractsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseContractsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
