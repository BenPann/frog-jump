import { RplMockPageComponent } from './component/rpl-mock-page/rpl-mock-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonalIdentifyComponent } from './component/personal-identify/personal-identify.component';
import { TokenGuard } from '../guard/token.guard';
import { LeaveGuard } from '../guard/leave.guard';
import { OtpComponent } from './component/otp/otp.component';
import { ChooseContractsComponent } from './component/choose-contracts/choose-contracts.component';
import { DataConfirmComponent } from './component/data-confirm/data-confirm.component';
import { AllFinishComponent } from './component/all-finish/all-finish.component';
import { MobileLoginComponent } from './component/mobile-login/mobile-login.component';
import { ContractNotFoundComponent } from './component/contract-not-found/contract-not-found.component';
import { ContractTermsComponent } from './component/contract-terms/contract-terms.component';


const routes: Routes = [
  {
    path: 'pi',
    component: PersonalIdentifyComponent,
    canDeactivate: [ LeaveGuard ],
    data: { browseLog: false }
  },
  {
    path: 'otp',
    component: OtpComponent,
    canActivate:   [ TokenGuard ],
    canDeactivate: [ LeaveGuard ],
    data: { browseLog: true }
  },
  {
    path: 'list',
    component: ChooseContractsComponent,
    canActivate:   [ TokenGuard ],
    canDeactivate: [ LeaveGuard ],
    data: { browseLog: true },
  },
  {
    path: 'data-confirm',
    component: DataConfirmComponent,
    canActivate:   [ TokenGuard ],
    canDeactivate: [ LeaveGuard ],
    data: { browseLog: true },
  },
  {
    path: 'contract-terms',
    component: ContractTermsComponent,
    canActivate:   [ TokenGuard ],
    canDeactivate: [ LeaveGuard ],
    data: { browseLog: true },
  },
  {
    path: 'all-finish',
    component: AllFinishComponent,
    canActivate:   [ TokenGuard ],
    canDeactivate: [ LeaveGuard ],
    data: { browseLog: true },
  },
  {
    path: 'mobile/:token',
    component: MobileLoginComponent,
    canDeactivate: [ LeaveGuard ],
    data: { browseLog: false },
  },
  {
    path: 'contractNotFound',
    component: ContractNotFoundComponent,
    data: { browseLog: true },
  },
  {
    path: 'mockPage',
    component: RplMockPageComponent,
    data: { browseLog: true },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RplUserRoutingModule { }
