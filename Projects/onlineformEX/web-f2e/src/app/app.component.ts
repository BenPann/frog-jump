import { Component } from '@angular/core';
import { NgbModalConfig, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { I18n, NgDatePicker } from './i18n/ng-date-picker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [I18n, { provide: NgbDatepickerI18n, useClass: NgDatePicker }]
})
export class AppComponent {
  constructor(private config: NgbModalConfig) {
    config.backdrop = 'static';
    config.keyboard = false;
  }
}
