import { Injectable } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { MOCKTERMS } from './mockTerms';


@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

  // 回應正常訊息
  regularResponseList = [
    { url: 'api/apply/finish', method: 'GET' },   // 完成頁面
    { url: 'api/apply/webBank', method: 'POST' }, // 儲存網銀頁面內容
    { url: 'api/terms/setCaseDataTerms', method: 'POST' },
    { url: 'api/browse/setPage', method: 'POST' },
  ];

  constructor() {
  }

  /**
   * Detective the anomaly active at radar beacon
   * Send interceptor
   *
   *    battle
   *      1. environment.production: Show log at console or not
   *      2. OTP => CLEAR UP
   *        2.1 sendOTP(): send OTP mail to your phone
   *        2.2 chekcOTP(): check your pass is correct or what
   *
   * @param request Get Input
   * @param next who know?
   */
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    return of(null).pipe(
      mergeMap(() => {


        // CHAP: Read environment setting choose which MODE: active HACK: 產品模式不顯示此段LOG
        if (!environment.production) {
          console.log('FakeBackendInterceptor request:', request);
        }


        // CHAP: OTP sendOTP & checkOTP
        if (environment.useFakeOtp) {

          // Send OTP-mail
          if (
            request.url.endsWith('api/validate/sendOTP') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '',
              result: {
                code: '0000',
                message: '',
                sk: 'sk',
                txnID: 'txn',
                txnDate: 'date'
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          // 驗證OTP
          if (
            request.url.endsWith('api/validate/checkOTP') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '',
              result: {
                code: '0000',
                message: ''
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }
        }


        // CHAP: 是否使用假的KGI的API
        if (environment.useFakeKgiApi) {

          // Find URL match list we create
          if (
            this.regularResponseList.find(
              value =>
                value.method === request.method &&
                request.url.endsWith(value.url)
            )
          ) {
            const body = {
              status: 0,
              message: '',
              result: ''
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/onlineform/login') &&
            request.method === 'POST'
          ) {
            const body = {
              'status': 0,
              'message': 'Successful',
              'result': {
                cifinfo: '',
                asystem: [
                  { funcType: 'I',
                    userType: '1',
                    caseType: '1',
                    customerId: 'A123456789',
                    manageStore: 'outspace',
                    companyNum: '1',
                    departNum: '42',
                    memberNum: '1984',
                    seq: 'SEQ',
                    userName: 'Alpha',
                    cardNum: 'ALPHA000001',
                    projectId: '契變書簽署',
                    creditCur: '1000000',
                    creditContract: '2000000',
                    creditModify: '',
                    dateModify: '',
                    email: 'DontLookAbyss@look.you.too.com',
                    phone: '0952000001',
                    SO2419: ''
                  },
                  { funcType: 'I',
                    userType: '1',
                    caseType: '2',
                    customerId: 'A123456789',
                    manageStore: 'outspace',
                    companyNum: '1',
                    departNum: '42',
                    memberNum: '1984',
                    seq: 'SEQ',
                    userName: 'Beta',
                    cardNum: 'BETA000002',
                    projectId: '調額同意書簽署',
                    creditCur: '1400000',
                    creditContract: '1400000',
                    creditModify: '2800000',
                    dateModify: '',
                    email: 'DontLookAbyss@look.you.too.com',
                    phone: '0911000002'
                  },
                  { funcType: 'I',
                    userType: '1',
                    caseType: '3',
                    customerId: 'A123456789',
                    manageStore: 'outspace',
                    companyNum: '1',
                    departNum: '42',
                    memberNum: '1984',
                    seq: 'SEQ',
                    userName: 'Charlie',
                    cardNum: 'GAMMA000003',
                    projectId: '補財力證明及調額同意書簽署',
                    creditCur: '420000',
                    creditContract: '420000',
                    creditModify: '620000',
                    dateModify: '',
                    email: 'DontLookAbyss@look.you.too.com',
                    phone: '0972000003'
                  },
                  { funcType: 'I',
                    userType: '1',
                    caseType: '4',
                    customerId: 'A123456789',
                    manageStore: 'outspace',
                    companyNum: '1',
                    departNum: '42',
                    memberNum: '1984',
                    seq: 'SEQ',
                    userName: 'Delta',
                    cardNum: 'DELTA000004',
                    projectId: '契變書簽署及調額同意書簽署',
                    creditCur: '2500000',
                    creditContract: '4000000',
                    creditModify: '',
                    dateModify: '',
                    email: 'DontLookAbyss@look.you.too.com',
                    phone: '0938000004'
                  },
                  { funcType: 'I',
                    userType: '1',
                    caseType: '5',
                    customerId: 'A123456789',
                    manageStore: 'outspace',
                    companyNum: '1',
                    departNum: '42',
                    memberNum: '1984',
                    seq: 'SEQ',
                    userName: 'Echo',
                    cardNum: 'EPSILON000006',
                    projectId: '補財力證明及契變書簽署及調額同意書簽署',
                    creditCur: '3000000',
                    creditContract: '4500000',
                    creditModify: '',
                    dateModify: '',
                    email: 'DontLookAbyss@look.you.too.com',
                    phone: '0959000005'
                  },
                ],
                token: 'token',
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/onlineform/getContractsList') &&
            request.method === 'POST'
          ) {
            const body = {
              'status': 0,
              'message': 'Successful',
              'result': [
                { funcType: 'I',
                  userType: '1',
                  caseType: '1',
                  customerId: 'A123456789',
                  manageStore: 'outspace',
                  companyNum: '1',
                  departNum: '42',
                  memberNum: '1984',
                  seq: 'SEQ',
                  userName: 'Alpha',
                  cardNum: 'ALPHA000001',
                  projectId: '契變書簽署',
                  creditCur: '1000000',
                  creditContract: '2000000',
                  creditModify: '',
                  dateModify: '',
                  email: 'DontLookAbyss@look.you.too.com',
                  phone: '0952000001',
                  SO2419: ''
                },
                { funcType: 'I',
                  userType: '1',
                  caseType: '2',
                  customerId: 'A123456789',
                  manageStore: 'outspace',
                  companyNum: '1',
                  departNum: '42',
                  memberNum: '1984',
                  seq: 'SEQ',
                  userName: 'Beta',
                  cardNum: 'BETA000002',
                  projectId: '調額同意書簽署',
                  creditCur: '1400000',
                  creditContract: '1400000',
                  creditModify: '2800000',
                  dateModify: '',
                  email: 'DontLookAbyss@look.you.too.com',
                  phone: '0911000002'
                },
                { funcType: 'I',
                  userType: '1',
                  caseType: '3',
                  customerId: 'A123456789',
                  manageStore: 'outspace',
                  companyNum: '1',
                  departNum: '42',
                  memberNum: '1984',
                  seq: 'SEQ',
                  userName: 'Charlie',
                  cardNum: 'GAMMA000003',
                  projectId: '補財力證明及調額同意書簽署',
                  creditCur: '420000',
                  creditContract: '420000',
                  creditModify: '620000',
                  dateModify: '',
                  email: 'DontLookAbyss@look.you.too.com',
                  phone: '0972000003'
                },
                { funcType: 'I',
                  userType: '1',
                  caseType: '4',
                  customerId: 'A123456789',
                  manageStore: 'outspace',
                  companyNum: '1',
                  departNum: '42',
                  memberNum: '1984',
                  seq: 'SEQ',
                  userName: 'Delta',
                  cardNum: 'DELTA000004',
                  projectId: '契變書簽署及調額同意書簽署',
                  creditCur: '2500000',
                  creditContract: '4000000',
                  creditModify: '',
                  dateModify: '',
                  email: 'DontLookAbyss@look.you.too.com',
                  phone: '0938000004'
                },
                { funcType: 'I',
                  userType: '1',
                  caseType: '5',
                  customerId: 'A123456789',
                  manageStore: 'outspace',
                  companyNum: '1',
                  departNum: '42',
                  memberNum: '1984',
                  seq: 'SEQ',
                  userName: 'Echo',
                  cardNum: 'EPSILON000006',
                  projectId: '補財力證明及契變書簽署及調額同意書簽署',
                  creditCur: '3000000',
                  creditContract: '4500000',
                  creditModify: '',
                  dateModify: '',
                  email: 'DontLookAbyss@look.you.too.com',
                  phone: '0959000005'
                },
              ]
            };
            return of(new HttpResponse({status: 200, body: body}));
          }


          if (
            request.url.endsWith('api/onlineform/allFinish') &&
            request.method === 'POST'
          ) {
            const body = {
              'status': 0,
              'message': 'Successful',
              'result': [
                { funcType: 'U',
                  userType: '1',
                  caseType: '1',
                  customerId: 'A123456789',
                  manageStore: 'outspace',
                  companyNum: '1',
                  departNum: '42',
                  memberNum: '1984',
                  seq: 'SEQ',
                  userName: 'Alpha',
                  cardNum: 'ALPHA000001',
                  projectId: '契變書簽署',
                  creditCur: '1000000',
                  creditContract: '2000000',
                  creditModify: '300000',
                  dateModify: '',
                  email: 'LookAbyss@embrace.us.whole.com',
                  phone: '0952000001',
                },
              ]
            };
            return of (new HttpResponse( {status: 200, body: body }));
          }

        }


        // CHAP: 是否使用假的BACKEND的API
        if (environment.useFakeBackendApi) {

          // NOTE: TERMS
          if (
            request.url.includes('/publicApi/terms/') &&
            request.method === 'GET'
          ) {
            return of(new HttpResponse({status: 200, body: '我是條款'}));
          }

          // NOTE: CHANNEL
          if (
            request.url.endsWith('/publicApi/channel/getChannelList') &&
            request.method === 'GET'
          ) {
            const body = {
              'status': 0,
              'message': '成功',
              'result': [
                {
                  'channelID': 'KB',
                  'channelName': '凱基商銀',
                  'idLength': 'Hello, world!',
                  'idLogic': '0'
                }
              ]
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          // NOTE: TERM-LIST
          if (
            request.url.includes('/publicApi/terms/getTermsList') &&
            request.method === 'POST'
          ) {
            let body: any[];
            switch (request.body.url) {
              case 'ed3-init':
                body = [
                  {
                    termTitle: '個人資料運用告知聲明使用者條款及約定',
                    termName: 'EOPPersonalDataUsing',
                    type: '2',
                  },
                  {
                    termTitle: '利率費用告知事項',
                    termName: 'EOPRateFee',
                    type: '2',
                  },
                  {
                    termTitle: '非代辦宣告',
                    termName: 'EOPNoAgency',
                    type: '2',
                  },
                  {
                    termTitle: '電話/網路/行動銀行約定條款',
                    termName: 'EOPBankPromise',
                    type: '1',
                  }
                ];
                break;
              case 'ed3-fillData4':
                body = [
                  {
                    termTitle: '美國海外帳戶稅收遵循法(FATCA)相關事宜',
                    termName: 'EOPFatca',
                    type: '2',
                  }
                ];
                break;
              case 'ed3-chooseProduct':
                body = [
                  {
                    termTitle: '美國海外帳戶稅收遵循法(FATCA)相關事宜',
                    termName: 'EOPFatca',
                    type: '2'
                  },
                  {
                    termTitle: '存款總約定書',
                    termName: 'EOPDepositPromise',
                    type: '2'
                  },
                  {
                    termTitle: '聯名卡同意條款',
                    termName: 'CreditPromise',
                    type: '2'
                  }
                ];
                break;
                case 'ed3-chooseCard':
                body = [
                  {
                    termTitle: '聯名卡同意條款',
                    termName: 'CreditPromise',
                    type: '2'
                  }
                ];
                break;
                case 'ed3-product_CCA':
                  body = [
                    {
                      termTitle: '美國海外帳戶稅收遵循法(FATCA)相關事宜',
                      termName: 'EOPFatca',
                      type: '2'
                    },
                    {
                      termTitle: '存款總約定書',
                      termName: 'EOPDepositPromise',
                      type: '2'
                    },
                    {
                      termTitle: '聯名卡同意條款',
                      termName: 'CreditPromise',
                      type: '2'
                    }
                  ];
                  break;
                  case 'ed3-product_A':
                  body = [
                    {
                      termTitle: '美國海外帳戶稅收遵循法(FATCA)相關事宜',
                      termName: 'EOPFatca',
                      type: '2'
                    },
                    {
                      termTitle: '存款總約定書',
                      termName: 'EOPDepositPromise',
                      type: '2'
                    },
                  ];
                  break;
            }


            return of(new HttpResponse({status: 200, body: body}));
          }


          //
          if (
            request.url.endsWith('/publicApi/moblieBank/freeLogin') &&
            request.method === 'POST'
          ) {
            const body = {
              'status': 0,
              'message': '成功',
              'result': {}
            };
            return of(new HttpResponse({status: 200, body: body}));
          }


          //
          if (
            request.url.endsWith('/api/onlineform/creditModifyTerm') &&
            request.method === 'POST'
          ) {
            const body = {
              'status': 0,
              'message': 'Successful',
              'result': MOCKTERMS,
            };
            return of (new HttpResponse( { status: 200, body: body } ));
          }

        }


        return next.handle(request);

      })
    );

    // .pipe(materialize())
    // .pipe(delay(500))
    // .pipe(dematerialize());
  }
}


/**
 * What the this
 */
export let fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
