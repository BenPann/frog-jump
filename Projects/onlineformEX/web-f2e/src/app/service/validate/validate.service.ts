import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebResult } from 'src/app/interface/resultType';
import { NCCCReq, PCODE2566Req } from './validate.interface';

@Injectable({
  providedIn: 'root'
})
export class ValidateService {
  constructor(private httpClient: HttpClient) {}

  setNCCC(req: NCCCReq) {
    return this.httpClient.post<WebResult>(
      '../api/validate/nccc',
      req
    );
  }

  setPCode2566(req: PCODE2566Req) {
    return this.httpClient.post<WebResult>(
      '../api/validate/pcode2566',
      req
    );
  }

  getVerifyCount() {
    return this.httpClient.get<WebResult>('../api/validate/getVerifyCount', {});
  }

}
