import { HandlerError } from './../error-handle.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { WebResult } from 'src/app/interface/resultType';
import { OtpData } from '../otp/otp.interface';
import {
  MoblieLoginRequest,
  UserLoginInfoRequest,
  UserContractResponseMk2,
  UserContractView,
  UserContractRequest,
  MockApiContainer
} from './a-system.interface';


@Injectable({
  providedIn: 'root'
})
export class ASystemService {

  private handleError: HandlerError;

  constructor(
    private http: HttpClient,
  ) { }


  // SECTOR: REQ & RES
  doLoginCheckByASystem(req: UserLoginInfoRequest): Observable<WebResult> {
    return this.http.post<WebResult>('../api/onlineform/login', req)
      .pipe(
        catchError(this.handleError)
      );
  }


  doLoginFromMobile(req: MoblieLoginRequest): Observable<WebResult> {
    return this.http.post<WebResult>('../publicApi/onlineform/freeLogin', req);
  }


  getUserContractsByASystem(req: UserLoginInfoRequest): Observable<UserContractResponseMk2> {
    return this.http.post<UserContractResponseMk2>('../api/onlineform/getContractsList', req);
  }


  getModifyRange(req: any): Observable<any> {
    return this.http.post<WebResult>('../api/onlineform/getModifyRange', req);
  }


  getCreditModifyTermDetail() {
    const reqData = {
      item: 'item',
    };
    return this.http.post<WebResult>('../api/onlineform/creditModifyTerm', reqData);
  }


  setUserMailAndCreditModify(email: any, creditModify: any) {
    const reqData = {
      EmailAddress: email,
      CreditModify: creditModify,
    };
    return this.http.post<WebResult>('../api/onlineform/setMailAndModify', reqData);
  }


  allFinish(view: UserContractRequest) {
    return this.http.post<WebResult>('../api/onlineform/allFinish', view)
      .pipe(
        tap(data => {
          if (data.status === 0) {
            this.otpData = data.result;
          }
        })
      );
  }


  doRequestToAPService(url: string, req: any) {
    return this.http.post<WebResult>(url, req);
  }


  // SECTOR: GET SET
  // ITEM: variable set
  public get ContractData(): UserContractView {
    return this.getJSON('rplDetail');
  }
  public set ContractData(rpl: UserContractView) {
    sessionStorage.setItem('rplDetail', JSON.stringify(rpl));
  }


  // ITEM:
  public get userLoginData(): UserLoginInfoRequest {
    return this.getJSON('userLoginData');
  }
  public set userLoginData(info: UserLoginInfoRequest) {
    sessionStorage.setItem('userLoginData', JSON.stringify(info));
  }


  // ITEM:
  public get otpData(): OtpData {
    return this.getJSON('OtpData');
  }
  public set otpData(otpData: OtpData) {
    sessionStorage.setItem('OtpData', JSON.stringify(otpData));
  }


  // SECTOR: METHOD
  private getJSON(key: string): any {
    return sessionStorage.getItem(key)
      ? JSON.parse(sessionStorage.getItem(key))
      : undefined;
  }


  // SECTOR: MOCK
  mockApiLog() {
    const view: MockApiContainer = {
      test: 'test'
    };
    return this.http.post<String>('../publicApi/onlineform/mockApi', view);
  }


}
