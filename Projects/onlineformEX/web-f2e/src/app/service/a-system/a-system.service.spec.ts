import { TestBed } from '@angular/core/testing';

import { ASystemService } from './a-system.service';

describe('ASystemService', () => {
  let service: ASystemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ASystemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
