/**
 * 1. Type of A-System request
 */
export interface UserLoginInfoRequest {
  idno: string;
  birthday: string;
  account: string;
  url: string;
  entry: string;
  uniqType: string;
}


/**
 * 2. Type of A-System request at all-finish
 */
export interface UserContractRequest {
  funcType: string;
  docType: string;
  userId: string;
  account: string;
  shortUrl: string;
  creditModify: string;
}


/**
 * 3. NO USE
 */
export interface MoblieLoginRequest {
  token: string;
}


/**
 * 4. Type of response of User login
 */
export interface UserLoginResponseMk1 {
  status: number;
  message: string;
  result: {
    cifinfo: any;
    asystem: UserContractView[];
    token: string;
  };
}


/**
 * 5. Type of response of get contract from A-System
 */
export interface UserContractResponseMk2 {
  status: number;
  message: string;
  result: {
    asystem: UserContractView[];
  };
}


/**
 * 6. Type of telegraph of A-System
 */
export interface UserContractView {
  funcType: string;
  userType: string;
  caseType: string;
  customerId: string;
  manageStore: string;
  companyNum: string;
  departNum: string;
  memberNum: string;
  seq: string;
  userName: string;
  cardNum: string;
  projectId: string;
  creditCur: string;
  creditContract: string;
  creditModify: string;
  dateModify: string;
  email: string;
  phone: string;
  SO2419: string;
}

export interface ModifyRange {
  modifyRange: string;
}

export interface MockApiContainer {
  test: string;
}
