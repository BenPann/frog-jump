import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WebResult } from 'src/app/interface/resultType';
import { tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { OtpData } from './otp.interface';

@Injectable({
  providedIn: 'root'
})
export class OtpService {
  public get otpData(): OtpData {
    return this.getJSON('OtpData');
  }
  public set otpData(otpData: OtpData) {
    sessionStorage.setItem('OtpData', JSON.stringify(otpData));
  }

  constructor(private httpClient: HttpClient) {}


  /**
   * Send OTP-mail to indicate phone number
   *
   * @param phone number
   */
  sendOtp(phone: string): Observable<WebResult> {
    const reqData = {
      phone: phone
    };
    return this.httpClient.post<WebResult>('../api/validate/sendOTP', reqData)
      .pipe(
        tap(data => {
          if (data.status === 0) {
            this.otpData = data.result;
          }
        })
      );
  }


  /**
   * Check target fo phone is where verify OTP-pass
   *
   * @param phone number
   * @param otp your return data recevice from sendOtp()
   */
  checkOtp(phone: string, otp: string): Observable<WebResult> {
    const otpData = this.otpData;
    const reqData = {
      phone: phone,
      txnId: otpData.txnID,
      txnDate: otpData.txnDate,
      sk: otpData.sk,
      otp: otp
    };
    return this.httpClient.post<WebResult>('../api/validate/checkOTP', reqData);
  }


  isEmailSuccess(email: string): Observable<WebResult> {
    const reqData = {
      email: email,
    };
    return this.httpClient.post<WebResult>('../api/validate/isEmailSuccess', reqData);
  }



  // SECTOR: Method
  private getJSON(key: string): any {
    return sessionStorage.getItem(key)
      ? JSON.parse(sessionStorage.getItem(key))
      : undefined;
  }


}
