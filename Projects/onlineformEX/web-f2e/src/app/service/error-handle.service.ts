import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

export type HandlerError =
    <T> (operation?: string, result?: T) =>
            (error: HttpErrorResponse) =>
                Observable<T>;

export class HttpErrorHandler {

    constructor() {}

}
