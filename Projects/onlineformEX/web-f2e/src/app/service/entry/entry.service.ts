import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {WebResult} from '../../interface/resultType';
import {ShortUrlDataRsp, ShortUrlDataView} from '../../component/entry/entry.interface';

@Injectable({
  providedIn: 'root'
})
export class EntryService {

  public set shortUrlData(data: ShortUrlDataRsp) {
    sessionStorage.setItem('shortUrlData', JSON.stringify(data));
  }

  public get shortUrlData(): ShortUrlDataRsp {
    return this.getJSON('shortUrlData');
  }

  public set portal(data: string) {
    sessionStorage.setItem('chinaLife', JSON.stringify(data));
  }

  public get portal(): string {
    return this.getJSON('chinaLife');
  }

  public isCL(): boolean {
    return this.portal === 'CL';
  }

  constructor(private httpClient: HttpClient) {
  }

  saveShortUrlData(shortUrlData: ShortUrlDataView): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../publicApi/channel/saveShortUrlData', shortUrlData);
  }

  private getJSON(key: string): any {
    return sessionStorage.getItem(key) && sessionStorage.getItem(key) !== 'undefined'
      ? JSON.parse(sessionStorage.getItem(key))
      : undefined;
  }
}
