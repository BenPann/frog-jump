import { TestBed } from '@angular/core/testing';

import { RplRouterDirectService } from './rpl-router-direct.service';

describe('RplRouterDirectService', () => {
  let service: RplRouterDirectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RplRouterDirectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
