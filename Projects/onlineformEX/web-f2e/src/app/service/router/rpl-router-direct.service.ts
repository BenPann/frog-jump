import { Injectable } from '@angular/core';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RplRouterDirectService {

  // Sorted of index how page change at submit
  routerArr = [
    'pi',
    'list',
    'otp',
    'data-confirm',
    'contract-terms',
    'all-finish',
  ];

  routerArrRplTypeC = [
    'pi',
    'list',
    'otp',
    'contract-terms',
    'all-finish',
  ];


  constructor(
    private router: Router,
    private rhs: RouterHelperService,
  ) { }


  /**
   * How it work
   * 1. find this router url is what, and split it by '/' to get the array contain it address
   * 2. Take the array of it contain number at index of 1, for example if your router url is 'rpl/list'
   *    You will get 'list' this key to search where you are.
   * 3. Use this key compare with array you sorted for router: routerArr, find the next page you need to navigate.
   */
  nextStep(type?: string) {
    const array = this.router.url.split('/').filter( e => e !== '');
    const index = this.getRouterIndex(this.routerArr, array[1]);
    const route = type === 'C' ? this.routerArrRplTypeC : this.routerArr;

    index >= 0
      ? this.rhs.navigate([array[0], route[index + 1]])
      : console.warn('錯誤的網址', this.router.url);
  }


  /**
   * same the job of nextStep()
   * only reverse the step of find next navigate of router by find previous.
   */
  backStep(type?: string) {
    const array = this.router.url.split('/').filter(element => element !== '');
    const index = this.getRouterIndex(this.routerArr, array[1]);
    const route = type === 'C' ? this.routerArrRplTypeC : this.routerArr;

    index >= 0
      ? this.rhs.navigate([array[0], route[index - 1]])
      : console.warn('錯誤的網址', this.router.url);
  }


  /**
   * @param arr Your routerArray which index how their line
   * @param url Put url to find which index your position
   */
  getRouterIndex(arr: string[], url: string) {
    return arr.findIndex(e => e === url);
  }


  public get passOTP(): string {
    return sessionStorage.getItem('passOTP');
  }

  public set passOTP(answer: string) {
    sessionStorage.setItem('passOTP', answer);
  }

}
