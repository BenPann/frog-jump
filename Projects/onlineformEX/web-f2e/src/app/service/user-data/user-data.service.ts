import { Injectable } from '@angular/core';
import { UserLoginInfoRequest } from '../a-system/a-system.interface';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {


  constructor() {
  }

  /**
   * 取JSON
   * return any
   */
  private getJSON(key: string): any {
    return sessionStorage.getItem(key)
      ? JSON.parse(sessionStorage.getItem(key))
      : undefined;
  }


  // SECTOR: NEW SECTOR OF RPL
  // ITEM:
  public get userLoginData(): UserLoginInfoRequest {
    return this.getJSON('userLoginData');
  }
  public set userLoginData(info: UserLoginInfoRequest) {
    sessionStorage.setItem('userLoginData', JSON.stringify(info));
  }

  public get token(): string {
    return sessionStorage.getItem('kgiToken');
  }
  public set token(token: string) {
    sessionStorage.setItem('kgiToken', token);
  }

  public get cifInfo(): string {
    return sessionStorage.getItem('cifInfo');
  }
  public set cifInfo(cifInfo: string) {
    sessionStorage.setItem('cifInfo', cifInfo);
  }


}
