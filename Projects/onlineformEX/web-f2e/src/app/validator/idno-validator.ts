// import { AbstractControl } from '@angular/forms';
export function validateIdno  (idStr: string) {
  // 依照字母的編號排列，存入陣列備用。
  const letters = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'W', 'Z', 'I', 'O'
  ];
  // 儲存各個乘數
  const multiply = [1, 9, 8, 7, 6, 5, 4, 3, 2, 1];
  const nums = [];
  let firstChar;
  let firstNum;
  let lastNum;
  let total = 0;
  // 撰寫「正規表達式」。
  const regExpID = /^[A-Z](1|2)\d{8}$/g;
  // 使用「正規表達式」檢驗格式
  if (idStr.search(regExpID) === -1) {
    // 基本格式錯誤
    console.warn('基本格式錯誤');
    return false;
  } else {
    // 取出第一個字元和最後一個數字。
    firstChar = idStr.charAt(0);
    lastNum = parseInt(idStr.charAt(9), 10);
  }
  // 找出第一個字母對應的數字，並轉換成兩位數數字。
  for (let i = 0; i < 26; i++) {
    if (firstChar === letters[i]) {
      firstNum = i + 10;
      nums[0] = Math.floor(firstNum / 10);
      nums[1] = firstNum - (nums[0] * 10);
      console.warn(nums);
      break;
    }
  }
  // 執行加總計算
  for (let i = 0; i < multiply.length; i++) {
    if (i < 2) {
      total += nums[i] * multiply[i];
    } else {
      total += parseInt(idStr.charAt(i - 1), 10) *
        multiply[i];
    }
  }
  // 檢查碼必須跟10-total%10相同
  if ((10 - (total % 10)) % 10 !== lastNum) {
    console.warn('檢查碼驗證錯誤');
    return false;
  }

  return true;
}

// export default function idnoValidator(control: AbstractControl) {
//   return !control.value || validateIdno(control.value) ? null :  { validIdno: true } ;
// }
