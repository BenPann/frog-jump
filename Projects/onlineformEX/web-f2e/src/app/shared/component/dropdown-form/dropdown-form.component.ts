import {ModalFactoryService} from '../../service/common/modal-factory.service';
import {Component, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {CommonDataService} from '../../service/common/common-data.service';
import {RouterHelperService} from '../../service/common/router-helper.service';
import {takeWhile} from 'rxjs/operators';
import {FormCheckService} from '../../service/common/form-check.service';
import {idCardValidator} from '../../../validator/idCard-validator';
import { EntryService } from 'src/app/service/entry/entry.service';

export interface ChannelList {
  channelId: string;
  channelName: string;
  idLength: string;
  idLogic: string; // 0:ID身分驗證 1:驗證數字 2:驗證英數字
}

export interface DepartList {
  departId: string;
  departName: string;
}

@Component({
  selector: 'app-dropdown-form',
  templateUrl: './dropdown-form.component.html',
  styleUrls: ['./dropdown-form.component.scss']
})
export class DropdownFormComponent implements OnInit, OnDestroy {
  @Input()
  isLookdocOpen: boolean;

  @Input()
  hideLeft = false;

  orgID: string;
  empID: string;
  channelListAry: ChannelList[];
  validType: string; // 0:ID身分驗證 1:驗證數字 2:驗證英數字
  validLength: string;
  empErrorMsg: string;
  departListAry: DepartList[];

  // form表單
  @Input()
  public dropdownForm: FormGroup;
  private alive = true;

  constructor(private commonDataService: CommonDataService,
              private modalFactoryService: ModalFactoryService,
              private routerHelperService: RouterHelperService,
              private entryService: EntryService,
              public formCheckService: FormCheckService) {

  }

  ngOnInit() {

    this.commonDataService.getChannelList().pipe(takeWhile(() => this.alive)).subscribe(data => {
      if (data.status === 0) {
        this.channelListAry = data.result;
        this.initEvent();
        if (!this.dropdownForm.controls['refereeClass'].value) {
          this.dropdownForm.controls['refereeClass'].setValue('KB');
        }
      } else {
        this.modalFactoryService
          .modalDefault({
            message: '取得推廣單位出錯'
          })
          .result.then(() => {
          this.routerHelperService.navigate(['error']);
        });
      }
    });


  }

  getPortal(): string {
    return this.entryService.portal;
  }

  initEvent() {
    this.dropdownForm.controls['refereeClass'].valueChanges.pipe(takeWhile(() => this.alive)).subscribe(result => {
      const array = this.channelListAry.slice();
      if (result) {
        const validArray: ValidatorFn[] = [];
        this.validLength = array.filter(value => value.channelId === result)[0].idLength;
        const empIdControl = this.dropdownForm.controls['empID'];
        this.validType = array.filter(value => value.channelId === result)[0].idLogic;
        if (this.validType === '0') {
          validArray.push(idCardValidator());
          this.empErrorMsg = '請輸入身分證字號(首字大寫)';
        } else if (this.validType === '1') {
          validArray.push(Validators.pattern('[0-9]*'));
          validArray.push(Validators.maxLength(Number(this.validLength)));
          this.empErrorMsg = '請輸入限' + this.validLength + '位數字';
        } else if (this.validType === '2') {
          validArray.push(Validators.pattern('[0-9a-zA-Z]{' + this.validLength + '}'));
          this.empErrorMsg = '請輸入英數字';
        }
        empIdControl.setValidators(validArray);
        empIdControl.reset();
        this.commonDataService.getDepartList(result).pipe(takeWhile(() => this.alive)).subscribe(data => {
          if (data.status === 0) {
            this.departListAry = data.result;
            const control = this.dropdownForm.controls['orgID'];
            control.setValidators(this.departValidator.bind(this));
            control.reset();
            control.setValue('');
          }
        });

      }
    });
  }


  departValidator(control: FormControl): { [s: string]: boolean } {
    if (control.value === '' || control.value === null) {
      return null;
    }
    if (this.departListAry.filter(value => value.departId === control.value).length < 1) {
      return {'Invalid': true};
    }
    return null;
  }


  ngOnDestroy(): void {
    this.alive = false;
  }

}
