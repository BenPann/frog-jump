// 純粹只是包裝一層 讓angular不要報錯
export interface DisableButtonParameter {
  disabled: boolean;
}
