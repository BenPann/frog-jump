import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwPhoneComponent } from './tw-phone.component';

describe('TwPhoneComponent', () => {
  let component: TwPhoneComponent;
  let fixture: ComponentFixture<TwPhoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwPhoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwPhoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
