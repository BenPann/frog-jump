import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalSetting } from 'src/app/interface/resultType';

@Component({
  selector: 'app-show-message',
  templateUrl: './show-message.component.html',
  styleUrls: ['./show-message.component.scss']
})
export class ShowMessageComponent implements OnInit {
  public setting: ModalSetting;
  errorName = '失敗代碼為';
  constructor(public modal: NgbActiveModal) {}

  public ngOnInit(): void {
    const defaultSetting: ModalSetting = {
      title: '',
      subTitle: '',
      showConfirm: false,
      showClose: true,
      message: '',
      message2: '',
      btnOK: '確認',
      btnCancel: '取消',
      customerWidth: '',
    };
    this.setting = Object.assign(defaultSetting, this.setting);
  }

  notSubTitle(): string {
    if (this.setting.subTitle) {
      return '';
    } else {
      return 'w-100';
    }
  }
}
