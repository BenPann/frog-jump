import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  Input,
  SimpleChanges,
  OnChanges
} from '@angular/core';
import { CropperComponent, ImageCropperResult } from 'angular-cropperjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-upload-cropper',
  templateUrl: './upload-cropper.component.html',
  styleUrls: ['./upload-cropper.component.scss']
})
export class UploadCropperComponent implements OnInit {
  public dataImage: string | ArrayBuffer;
  public originImage: string | ArrayBuffer;
  public resultImage: string | ArrayBuffer;
  public config: any = {
    movable: true,
    scalable: true,
    zoomable: true,
    checkOrientation: true,
    checkCrossOrigin: false
  };
  @Input()
  public inputFile: File;

  @ViewChild('cropper', { static: false })
  public angularCropper: CropperComponent;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.upload();
  }

  public upload() {
    if (this.inputFile) {
      const reader: FileReader = new FileReader();
      reader.onloadend = (ev: ProgressEvent) => {
        this.dataImage = reader.result;
        this.originImage = reader.result;
      };
      reader.readAsDataURL(this.inputFile);
    }
  }

  public rotate(deg: number) {
    this.angularCropper.cropper.rotate(deg);
    // this.angularCropper.cropper.clear();
  }

  public zoom(rate){
    this.angularCropper.cropper.zoom(rate);
  }


  public crop() {
    // this.angularCropper.exportCanvas();
    this.resultImage = this.angularCropper.cropper.getCroppedCanvas({
      maxWidth: 2200,
      maxHeight: 2200,
    }).toDataURL('image/png');
  }

  public close() {
    this.activeModal.close();
  }

  public exportEvent(event: ImageCropperResult) {
    const reader: FileReader = new FileReader();
    reader.onloadend = (ev: ProgressEvent) => {
      this.resultImage = reader.result;
      this.dataImage = this.originImage;
    };
    reader.readAsDataURL(event.blob);
  }

  public reset() {
    this.resultImage = '';
  }

  public sendResult() {
    this.activeModal.close(this.resultImage);
  }
}
