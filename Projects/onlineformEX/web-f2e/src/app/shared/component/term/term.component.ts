import { Component, OnInit, Input } from '@angular/core';
import { TermsService } from '../../../service/terms/terms.service';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-term',
  templateUrl: './term.component.html',
  styleUrls: ['./term.component.scss']
})
export class TermComponent implements OnInit {

  isLookdocOpen = false;

  @Input()
  termName: String;
  @Input()
  termTitle: String;
  @Input()
  hideLeft = false;
  @Input()
  hideRow = false;

  constructor(public termService: TermsService) {}

  termText: String;

  ngOnInit() {
    console.log('====>> Access the term.component');
    this.termService
      .getTerms(this.termName)
      .pipe(shareReplay(1))
      .subscribe(
        data => {
          try {
            // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
            const json = JSON.parse(data);
            this.termText = '查詢條款失敗';
          } catch (error) {
            this.termText = data;
          }
        },
        error => {
          console.log('條款錯誤', error);
          this.termText = '查詢條款失敗';
        }
      );
  }
}
