import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-simple-root',
  templateUrl: './simple-root.component.html',
  styleUrls: ['./simple-root.component.scss']
})
export class SimpleRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
