import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  SimpleChanges,
  OnChanges
} from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormGroup,
  AbstractControl
} from '@angular/forms';
import { Observable, Subscription, Subject } from 'rxjs';
import { takeUntil, takeWhile } from 'rxjs/operators';
import { FormCheckService } from 'src/app/shared/service/common/form-check.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { zipcodeData } from 'src/app/mock/twzipcode';

@Component({
  selector: 'app-tw-zip-code',
  templateUrl: './tw-zip-code.component.html',
  styleUrls: ['./tw-zip-code.component.scss']
})
export class TwZipCodeComponent implements OnInit, OnDestroy {
  zipcodeFull: any;

  @Input()
  parentform: FormGroup;
  @Input()
  hiddenZipcode: boolean;

  districtList: any;


  addressOtherErrorMessage: string;
  private alive = true;

  constructor(public formCheckService: FormCheckService) {}

  ngOnInit() {
    this.zipcodeFull = zipcodeData;

    // 綁定事件
    this.parentform.controls['county'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {
        if (result) {
          this.districtList = this.zipcodeFull[result];
          const dis = this.parentform.controls['district'].value;
          if (this.districtList[dis]) {
            this.parentform.controls['district'].setValue(dis);
          } else {
            this.parentform.controls['district'].setValue('');
          }
        } else {
          this.districtList = [];
          this.parentform.controls['district'].setValue('');
        }
      });

      this.parentform.controls['addressOther']
      .valueChanges.pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.addressOtherErrorMessageUpdate();
      });

    this.parentform.controls['district'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {
        if (result) {
          this.parentform.controls['zip'].setValue(this.districtList[result]);
        }
      });

    if (this.parentform.get('zip').value) {
      this.setZip(this.parentform.get('zip').value, true);
    }
  }


  public setZip(zipCode: string, setZip: boolean) {
    if (zipCode) {
      for (const county in this.zipcodeFull) {
        if (this.zipcodeFull.hasOwnProperty(county)) {
          const element = this.zipcodeFull[county];
          for (const district in element) {
            if (element.hasOwnProperty(district)) {
              const element2 = element[district];
              if (element2 === zipCode) {
                this.parentform.controls['county'].setValue(county);
                this.parentform.controls['district'].setValue(district);
                if (setZip) {
                  this.parentform.controls['zip'].setValue(zipCode);
                }
                break;
              }
            }
          }
        }
      }
    }
  }

  descOrder(a, b) {
    if (a.key < b.key) {
      return b.key;
    }
  }

  ngOnDestroy(): void {
    this.alive = false;
  }


  addressOtherErrorMessageUpdate() {
    const ctl =  this.parentform.controls['addressOther'];
    let message = '';
    if (ctl.touched) {
      if (ctl.valid) {
        message = '';
      } else {
        if (ctl.errors.required) {
          message = '請輸入地址';
        } else if (ctl.errors.pattern) {
          message = '僅能輸入中、英、數字及"-"符號';
        } else if (ctl.errors.minlength) {
          message = '請輸入地址';
        }
      }
    } else {
      message = '';
    }
    this.addressOtherErrorMessage = message;
  }

}
