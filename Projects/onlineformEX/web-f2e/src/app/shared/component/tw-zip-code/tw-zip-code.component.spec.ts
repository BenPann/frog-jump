import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwZipCodeComponent } from './tw-zip-code.component';

describe('TwZipCodeComponent', () => {
  let component: TwZipCodeComponent;
  let fixture: ComponentFixture<TwZipCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwZipCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwZipCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
