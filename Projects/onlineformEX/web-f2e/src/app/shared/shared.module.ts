import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TwZipCode2Component } from './component/tw-zip-code2/tw-zip-code2.component';
import { TwZipCodeComponent } from './component/tw-zip-code/tw-zip-code.component';
import { UploadComponent } from './component/upload/upload.component';
import { TwPhoneComponent } from './component/tw-phone/tw-phone.component';
import { StepNumComponent } from './component/step-num/step-num.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UploadCropperComponent } from './component/upload-cropper/upload-cropper.component';
import { ShowMessageComponent } from './component/show-message/show-message.component';
import { SpinnerOverlayComponent } from './component/spinner-overlay/spinner-overlay.component';
import { SecurePipe } from './pipe/secure.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TermComponent } from './component/term/term.component';
import { HiddenStrPipe } from './pipe/hidden-str.pipe';
import { CommonDataService } from './service/common/common-data.service';
import { DatepickerService } from './service/common/datepicker.service';
import { FormCheckService } from './service/common/form-check.service';
import { ModalFactoryService } from './service/common/modal-factory.service';
import { RouterHelperService } from './service/common/router-helper.service';
import { SpinnerOverlayService } from './service/common/spinner-overlay.service';
import { FillDataStepBarComponent } from './component/fill-data-step-bar/fill-data-step-bar.component';
import { RouterModule } from '@angular/router';
import { SimpleRootComponent } from './component/simple-root/simple-root.component';
import { ShowImageComponent } from './component/show-image/show-image.component';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Term2Component } from './component/term2/term2.component';
import { SubmitButtonGroupComponent } from './component/submit-button-group/submit-button-group.component';
import { ShowHtmlComponent } from './component/show-html/show-html.component';
import { SafePipe } from './pipe/safe.pipe';
import { KeydownRegexpDirective } from './directive/keydown-regexp.directive';
import { CropperComponent } from './component/cropper/cropper.component';
import { AddrPipe } from './pipe/addr.pipe';
import { DatePlusChinesePipe } from './pipe/date-plus-chinese.pipe';
import { DropdownFormComponent } from './component/dropdown-form/dropdown-form.component';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};

const entryComp = [
  ShowMessageComponent,
  SpinnerOverlayComponent,
  UploadCropperComponent,
  ShowImageComponent,
  ShowHtmlComponent
];

const exportComp = [
  TwZipCodeComponent,
  TwPhoneComponent,
  UploadComponent,
  StepNumComponent,
  TwZipCode2Component,
  TermComponent,
  Term2Component,
  FillDataStepBarComponent,
  SimpleRootComponent,
  SubmitButtonGroupComponent,
  CropperComponent,
  DropdownFormComponent,
];

const exportPipe = [
  SecurePipe,
  HiddenStrPipe,
  SafePipe,
  AddrPipe,
  DatePlusChinesePipe
];
const exportDirective = [KeydownRegexpDirective];

const service = [
  CommonDataService,
  DatepickerService,
  FormCheckService,
  ModalFactoryService,
  RouterHelperService,
  SpinnerOverlayService
];

@NgModule({
  declarations: [
    ...entryComp,
    ...exportComp,
    ...exportPipe,
    ...exportDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule,
    SwiperModule
  ],
  exports: [
    ...entryComp,
    ...exportComp,
    ...exportPipe,
    ...exportDirective,
    NgbModule
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
export class SharedModule {}
