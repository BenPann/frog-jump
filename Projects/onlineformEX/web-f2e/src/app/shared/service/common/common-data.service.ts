import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {WebResult} from 'src/app/interface/resultType';
import {filter, map} from 'rxjs/operators';

/**
 *共用資料的Service
 */
@Injectable({
  providedIn: 'root'
})
export class CommonDataService {
  constructor(private httpClient: HttpClient) {
  }

  /**
   * 取得下拉選單的資料
   * @param dropName 下拉選單的名字
   */
  public getDropdownData(dropName: string) {
    return this.httpClient
      .post<WebResult>('../api/apply/getDropDown', {name: dropName})
      .pipe(
        filter(r => r.status === 0),
        map(r => r.result)
      );
  }

  /**
   * 取得信用卡列表資料
   */
  public getCards() {
    return this.httpClient.get<WebResult>('../api/credit/getCreditCardList');
  }

  public queryCommonData(types: string[]) {
    return this.httpClient.post('queryCommonData', {
      typeArray: types
    });
  }

  /**
   * 取得推廣單位列表
   */
  public getChannelList() {
    return this.httpClient.get<WebResult>('../publicApi/channel/getChannelList');
  }

  /**
   * 取得單位代號列表
   */
  public getDepartList(channelID: string) {
    return this.httpClient.post<WebResult>('../publicApi/channel/getChannelDepartList', {channelID: channelID});
  }
}
