import {Injectable} from '@angular/core';
import {DatePipe} from '@angular/common';
import {AbstractControl, FormControl, FormGroup, ValidatorFn} from '@angular/forms';
import {validateIdno} from 'src/app/validator/idno-validator';

@Injectable({
  providedIn: 'root'
})
export class FormCheckService {
  public invalidFeedback: {
    [key: string]: string;
  };
  private scrollTry = 0;

  constructor(public datePipe: DatePipe) {
    this.invalidFeedback = {
      required: '此欄位必填',
      idno: '請輸入正確的身分證字號',
      number: '只能輸入數字',
      cardNumber: '請輸入正確格式的卡號',
      cvc: '請輸入正確格式的檢查碼'
    };
  }

  /** 驗證此control是否合法 */
  public checkIsInValid(formGroup: AbstractControl, controlName: string) {
    const ctl = formGroup.get(controlName);
    if (ctl.touched) {
      return ctl.invalid;
    } else {
      return false;
    }
  }

  /** 依據此control是否合法 決定要顯示的css */
  public validClass(formGroup: AbstractControl, controlName: string) {
    const ctl = formGroup.get(controlName);
    if (ctl.touched) {
      return ctl.invalid ? 'is-invalid' : 'is-valid';
    } else {
      return '';
    }
  }

  public checkHasError(
    formGroup: AbstractControl,
    controlName: string,
    errorType: string
  ) {
    const ctl = formGroup.get(controlName);
    if (ctl.touched) {
      // console.log(ctl.errors);
      return ctl.errors[errorType];
    } else {
      return false;
    }
  }

  public async validateAllFormFields(formGroup: FormGroup) {
    const keys = await Object.keys(formGroup.controls);
    for (let i = 0; i < keys.length; i++) {
      const control = formGroup.get(keys[i]);
      if (control instanceof FormControl) {
        await control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        await this.validateAllFormFields(control);
      }
    }
    this.scrollTry = 0;
    setTimeout(this.scrollToError, 200);
  }

  /** 將所有已經有資料的formControl 設定為已經touch */
  public async markTouchedWhenHaveValue(formGroup: FormGroup) {
    const keys = await Object.keys(formGroup.controls);
    for (let i = 0; i < keys.length; i++) {
      const control = formGroup.get(keys[i]);
      if (control instanceof FormControl) {
        if (control.value) {
          await control.markAsTouched({onlySelf: true});
        }
      } else if (control instanceof FormGroup) {
        await this.markTouchedWhenHaveValue(control);
      }
    }
  }

  public scrollToError() {
    this.scrollTry++;
    const element: HTMLElement = document.querySelector('.is-invalid');
    if (element === null && this.scrollTry < 4) {
      setTimeout(this.scrollToError, 300);
      return;
    } else if (element === null) {
      return;
    }
    // window.scroll(0, element.offsetTop - 15);
    window.scroll(
      0,
      window.pageYOffset +
      element.getBoundingClientRect().top -
      element.ownerDocument.documentElement.clientTop -
      40
    );
  }

  /** 驗證不能與驗證文字相同 */
  public validatorNotEqual(str: string, validateName?: string): ValidatorFn {
    validateName = validateName || 'notequal';
    return (control: AbstractControl): { [key: string]: any } | null => {
      return str === control.value
        ? {[validateName]: {value: control.value}}
        : null;
    };
  }

  /** 驗證英數不能遞增或遞減 */
  public validatorMatchIncrese(flag: string, len?: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      let res = false;
      const number = '0123456789';
      const number2 = number
        .split('')
        .reverse()
        .join('');
      const eng = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      const eng2 = eng
        .split('')
        .reverse()
        .join('');
      const input = control.value;
      len = flag === '0' ? len : input.length;
      if (input.length >= len) {
        for (let i = 0; i < input.length; i++) {
          if (i + len > input.length) {
            break;
          }
          const matchStr = input.substr(i, len);
          if (
            number.includes(matchStr) ||
            number2.includes(matchStr) ||
            eng.includes(matchStr) ||
            eng2.includes(matchStr)
          ) {
            res = true;
            break;
          }
        }
      }
      return res ? {matchincrese: {value: control.value}} : null;
    };
  }

  /** 驗證英數不能連續出現三次 */
  public validatorMatchSame(flag: string, len?: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      let res = false;
      const input = control.value;
      len = flag === '0' ? len : input.length;
      let temp = input[0];
      let count = 1;
      if (input.length >= len) {
        for (let i = 0; i < input.length; i++) {
          /*if (i + len > input.length) {
            break;
          }*/
          if (temp === input[i + 1]) {
            count++;
            if (len === count) {
              res = true;
              break;
            }
          } else {
            res = false;
          }
          temp = input[i + 1];
        }
      }
      return res ? {matchsame: {value: control.value}} : null;
    };
  }

  /** 驗證身份證字號 */
  public idnoValidator(control: AbstractControl) {
    return !control.value || validateIdno(control.value)
      ? null
      : {validIdno: true};
  }

  /** 驗證生日是否小於20年 */
  public birthdayValidator(control: AbstractControl) {
    return !control.value ||
    !this.ifDateLessThanYear(
      20,
      control.value.year,
      control.value.month,
      control.value.day
    )
      ? null
      : {invalidBirthday: true};
  }

  public ifDateLessThanYear(flag, year, month, day) {
    const date = new Date();
    const yearNow = +this.datePipe.transform(date, 'yyyy');
    const monthNow = +this.datePipe.transform(date, 'MM');
    const dayNow = +this.datePipe.transform(date, 'dd');
    // console.log($filter('date')(date,'yyyy,MM,dd,HH,mm,ss'))
    // console.log(yearNow - vm.liveDate.year);
    // console.log(monthNow - vm.liveDate.month);
    // console.log(dayNow - vm.liveDate.day);

    // 3年內顯示前居住地址，不含3年前當天(例如：20140816 ~ 20170816 不顯示)
    const yearSelect = +year;
    const monthSelect = +month;
    const daySelect = +day;
    let differY, differM, ifYes;

    const leakMonth = daySelect > dayNow ? -1 : 0;
    // console.log(leakMonth);
    // console.log(monthNow);
    // console.log(monthSelect);
    // 3年內顯示前居住地址，不含3年前當天(例如：20140816 ~ 20170816 不顯示)
    differY =
      yearNow -
      yearSelect +
      (monthSelect === monthNow
        ? daySelect > dayNow
          ? -1
          : 0
        : monthSelect > monthNow
          ? -1
          : 0);

    differM = monthNow - monthSelect;
    if (monthSelect === monthNow) {
      differM = daySelect > dayNow ? 11 : 0;
    } else {
      if (monthSelect > monthNow) {
        differM = 12 + differM;
      }
      differM += leakMonth;
    }

    if (differY < 0) {
      // 如果年差<0，一律0年0月
      differY = 0;
      differM = 0;
    }

    return (ifYes = differY < flag);
  }

  public validatorTelAreaCode(control: FormControl): { [s: string]: boolean } {
    let res = false;
    const input = control.value;
    const orignCode = input.replace(' ', '');
    const areaCodeLen = orignCode.length;
    let allowAreaCode = '';
    // console.log('areaCodeLen = ' + areaCodeLen);
    switch (areaCodeLen) {
      case 4:
        allowAreaCode = '0836';
        break;
      case 3:
        allowAreaCode = '037,049,082,089';
        break;
      case 2:
        allowAreaCode = '02,03,04,05,06,07,08';
        break;
    }
    // console.log('inputAreaCode = ' + orignCode);
    // console.log('allowAreaCode = ' + allowAreaCode);

    if (allowAreaCode.indexOf(orignCode) === -1) {
      res = true;
    } else {
      res = false;
    }
    // console.log(res);
    return res ? {'checksuccess': true} : null;
  }


  public validCorpTel(formGroup: AbstractControl) {
    const cortTelAreaControl = formGroup.get('corpTelArea');
    const cortTelControl = formGroup.get('corpTel');
    let res = false;
    const input = cortTelAreaControl.value;
    if (input === '02' && (cortTelControl.value.substr(0, 1) === '0' || cortTelControl.value.length < 6)) {
      res = true;
    } else {
      res = false;
    }
    return res ? 'is-invalid' : this.validClass(formGroup, 'corpTel');
  }

  public checkCorpTelIsInValid(formGroup: AbstractControl) {
    const cortTelAreaControl = formGroup.get('corpTelArea');
    const cortTelControl = formGroup.get('corpTel');
    let res = false;
    const input = cortTelAreaControl.value;
    if (input === '02' && (cortTelControl.value.substr(0, 1) === '0' || cortTelControl.value.length < 6)) {
      res = true;
    } else {
      res = false;
    }
    return res;
  }

  public checkRepeatEmail(emailArray: string[]): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value) {
        return;
      }
      if (control.value.toString().toUpperCase().indexOf('@KGI.COM') > -1) {
        return {'innerEmailCheck': 'Invalid'};
      }

      if (emailArray.length > 0 && emailArray.indexOf(control.value) > -1) {
        return {'repeatEmailCheck': 'Invalid'};
      }
      return null;
    };
  }
}
