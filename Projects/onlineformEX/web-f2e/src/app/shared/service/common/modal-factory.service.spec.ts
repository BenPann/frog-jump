import { TestBed } from '@angular/core/testing';

import { ModalFactoryService } from './modal-factory.service';

describe('ModalFactoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModalFactoryService = TestBed.get(ModalFactoryService);
    expect(service).toBeTruthy();
  });
});
