# 凱基線上開戶

## 安裝前說明
- 目前將所有指令都放在根目錄package.json

## 專案架構
- 前端網頁(web-f2e)
- 前端站台(web)
- 後端站台(ap)
- 共用專案(common)
- 文件資料(book)
- 外部設定檔(config)
- 發行資料夾(deploy)
- Log資料夾(log)

## 系統需求(必須)
- NodeJS
- OpenJDK(OracleJDK應該也可以)
- Angular CLI
- Apache Maven 3.2+
  
## 系統需求(選擇性)
- Visual Studio Code (IDE)
  - Java套件(Java Extension Pack)
  - Angular套件(Angular Extension Pack)
  - 程式碼美化套件(Prettier - Code formatter)
- cmder (Command Line工具)

## 安裝過程
- 於根目錄輸入指令
  ``` npm run install-all ```
- 此指令會安裝三個站台所需的所有外部元件
- 安裝jar檔 (檔案在專案的lib資料夾)
``` shell
mvn install:install-file -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.1.0.6.0 -Dpackaging=jar -Dfile=C:\ojdbc6.jar
mvn install:install-file -DgroupId=com.ibm.mq -DartifactId=IBM_Common_Component -Dversion=1.0 -Dpackaging=jar -Dfile=C:\IBM_Common_Component.jar
mvn install:install-file -DgroupId=com.nccc.evops -DartifactId=MbecApiJavaTls -Dversion=1.2 -Dpackaging=jar -Dfile=C:\MbecApiJavaTls1.2.jar
mvn install:install-file -DgroupId=com.nccc.evops -DartifactId=MbecUpApiJavaTls -Dversion=1.2 -Dpackaging=jar -Dfile=C:\MbecUpApiJavaTls1.2.jar
```
## 開發方法
- 前端網頁
  - 於根目錄輸入指令
    ``` npm run startup-web-f2e ```
- WebServer
  - 於根目錄輸入指令
    ``` npm run startup-web ```
- APServer
    - 於根目錄輸入指令
 ``` npm run startup-ap ```

## 發行方法
- 於根目錄輸入指令
   ``` npm run deploy-all ```
- 至deploy資料夾取得War檔

## Gitbook 安裝開發及部屬(選用)
- 安裝 ``` npm install gitbook-cli -g ```
- 到book資料夾
- 開啟Gitbook ```gitbook serve ```
- (特別注意) 使用上敘的指令開發的時候 先將資料夾中的_gitbook刪除一次 這樣重新部屬才不會失敗
- 打開瀏覽器至 http://localhost:4000/
- 發行成pdf ``` gitbook pdf ./ ./book.pdf ```
(需安裝calibre)

## 開發想法說明
- Angular要做的事情
    - 使用者直接使用前端(Angular)的Router
    - 進來頁面會先經由Angular的Guard來檢查是否有登入
    - 前端的檢核(ReactiveForm)跟送資料給Web
- Web站台 只做兩件事情
    - 客戶登入之後 會使用jwt做驗證
    - 檢查資料完整性
    - 直接後送到AP
    - 中間建議不含任何商業邏輯
- AP站台 
    - 跟DB串接
    - 跟客戶的API串接
    - 商業邏輯部分
    - Job
- 共用專案
    - 放兩個站台都會用到的東西
    - dto
    - 系統參數
    - 共用函式
- 文件資料
    - 預計用gitbook放文件
    - 內容都為markdown撰寫
    - 此為選用
    - 若客戶需要看此部分的文件 就匯出成PDF檔