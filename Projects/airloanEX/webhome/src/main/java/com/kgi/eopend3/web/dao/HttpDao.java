package com.kgi.eopend3.web.dao;

import java.nio.charset.Charset;
import com.google.gson.Gson;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.web.config.GlobalConfig;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.SocketConfig;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.owasp.esapi.AccessReferenceMap;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;
import org.owasp.esapi.reference.RandomAccessReferenceMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Repository;

@Repository
public class HttpDao {

    @Autowired
    GlobalConfig globalConfig;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 共用的取得AP回應 如果需要加解密 需實作handleData的方法
     */
    public String getAPResponse(String controller, String action, String reqString, UsernamePasswordAuthenticationToken userAuth) {
        String result = "";

        int timeOut = 90000;//90秒
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        // 對連線池設定SocketConfig物件
        connManager.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(timeOut).build());

        try (CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build()) {
            // AP網址
            String url = validatorUrl(controller, action);
            // logger.info(url);
            HttpPost post = new HttpPost(url);
            // 必須用UTF-8 不然會有亂碼
            if(ESAPI.validator().isValidInput(this.getClass().getName(), reqString, "Space", Integer.MAX_VALUE,false)){
                StringEntity entity = new StringEntity(this.handleDataBeforeSend(reqString), "UTF-8");
                post.setEntity(entity);
                post.setHeader("Accept", "application/json");
                post.setHeader("Content-type", "application/json");
                post.setHeader(SystemConst.KGIHEADER, makeKGIHEADER(userAuth)); // 將Web JWT的主KEY傳過去給AP

                HttpResponse response = client.execute(post);
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // 必須用UTF-8 不然會有亂碼
                    result = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
                    result = this.handleDataAfterSend(result);
                } else if (statusCode == 404) {
                    // 要回傳找不到給打API的人知道
                    result = "404";
                } else {
                    logger.info("回應代碼:" + statusCode);
                    result = "";
                }
            }else{
                logger.warn("Invalid Input"); 
            }
        } catch (Exception ex) {
            logger.warn("getAPResponse", ex);
        } 

        return result;
    }

    /**
     * 資料送出之前所做的處理(加密)
     */
    public String handleDataBeforeSend(String data) {
        return data;
    }

    /**
     * 資料送出之後所做的處理(解密)
     */
    public String handleDataAfterSend(String data) {
        return data;
    }


    public String getAPResponseNoAuth(String controller, String action, String reqString) {
        String result = "";
        int timeOut = 90000;//90秒
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        // 對連線池設定SocketConfig物件
        connManager.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(timeOut).build());

        try (CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build()) {
            // AP網址
            String url = validatorUrl(controller, action);
            // logger.info(url);
            HttpPost post = new HttpPost(url);
            // 必須用UTF-8 不然會有亂碼
            
            if(ESAPI.validator().isValidInput(this.getClass().getName(), reqString, "Space", Integer.MAX_VALUE,false)){
                StringEntity entity = new StringEntity(this.handleDataBeforeSend(reqString), "UTF-8");
                post.setEntity(entity);
                post.setHeader("Accept", "application/json");
                post.setHeader("Content-type", "application/json");
                // no post.setHeader(SystemConst.KGIHEADER, makeKGIHEADER(userAuth)); // 將Web JWT的主KEY傳過去給AP

                HttpResponse response = client.execute(post);
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // 必須用UTF-8 不然會有亂碼
                    result = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
                    result = this.handleDataAfterSend(result);
                } else if (statusCode == 404) {
                    // 要回傳找不到給打API的人知道
                    result = "404";
                } else {
                    logger.info("回應代碼:" + statusCode);
                    result = "";
                }
            }else{
                logger.warn("Invalid Input"); 
            }
        } catch (Exception ex) {
            logger.warn("getAPResponse", ex);
        }

        return result;
    }


    public String getAPResponseNoAuthByGetMethod(String controller, String action) {
        String result = "";
        int timeOut = 90000;//90秒
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        // 對連線池設定SocketConfig物件
        connManager.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(timeOut).build());

        try (CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build()) {
            // AP網址
            String url = validatorUrl(controller, action);
            HttpGet get = new HttpGet(url);
            // 必須用UTF-8 不然會有亂碼
            get.setHeader("Accept", "application/json");
            get.setHeader("Content-type", "application/json");
            // no get.setHeader(SystemConst.KGIHEADER, makeKGIHEADER(userAuth)); // 將Web JWT的主KEY傳過去給AP

            HttpResponse response = client.execute(get);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // 必須用UTF-8 不然會有亂碼
                result = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
                result = this.handleDataAfterSend(result);
            } else if (statusCode == 404) {
                // 要回傳找不到給打API的人知道
                result = "404";
            } else {
                logger.info("回應代碼:" + statusCode);
                result = "";
            }
        } catch (Exception ex) {
            logger.warn("getAPResponse", ex);
            ;
        } 

        return result;
    }


    public String getAPResponseByGetMethod(String controller, String action, UsernamePasswordAuthenticationToken userAuth) {
        String result = "";
        int timeOut = 90000;//90秒
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        // 對連線池設定SocketConfig物件
        connManager.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(timeOut).build());
        try (CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build()){
            // AP網址
            String url = validatorUrl(controller, action);
            HttpGet get = new HttpGet(url);
            // 必須用UTF-8 不然會有亂碼
            get.setHeader("Accept", "application/json");
            get.setHeader("Content-type", "application/json");    
            get.setHeader(SystemConst.KGIHEADER, makeKGIHEADER(userAuth)); // 將Web JWT的主KEY傳過去給AP

            HttpResponse response = client.execute(get);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // 必須用UTF-8 不然會有亂碼
                result = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
                result = this.handleDataAfterSend(result);
            } else if (statusCode == 404) {
                // 要回傳找不到給打API的人知道
                result = "404";
            } else {
                logger.info("回應代碼:" + statusCode);
                result = "";
            }
        } catch (Exception ex) {
            logger.warn("getAPResponse", ex);
            ;
        } 

        return result;
    }

    private String validatorUrl(String controller, String action){
        String url = "";
        
        if(ESAPI.validator().isValidInput(this.getClass().getName(), controller, "Space", Integer.MAX_VALUE,false)&&
            ESAPI.validator().isValidInput(this.getClass().getName(), action, "Space", Integer.MAX_VALUE,false)){
            url = globalConfig.APServerHost.concat(controller).concat("/").concat(action);
        }else{
            AccessReferenceMap<String> map = new RandomAccessReferenceMap();
            String indirect_controller = map.addDirectReference(controller);
            String indirect_action = map.addDirectReference(action);
            try {
                controller = map.getDirectReference(indirect_controller);                
                action = map.getDirectReference(indirect_action);
                url = globalConfig.APServerHost.concat(controller).concat("/").concat(action);
            } catch (AccessControlException e) {
                logger.error("系統錯誤",e);
            }
        }        
        return url;        
    }

    /**
     * 
     * @see com.kgi.eopend3.common.dto.KGIHeader
     */
    private String makeKGIHEADER(UsernamePasswordAuthenticationToken userAuth) {
        // 將Web JWT的主KEY傳過去給AP
        userAuth = validatorUserAuth(userAuth);
        String header = userAuth.getName();
        logger.info("### From web to ap: post.setHeader = " + header);
        return header;
    }

    private UsernamePasswordAuthenticationToken validatorUserAuth(UsernamePasswordAuthenticationToken userAuth){
        Gson gson = new Gson();        
        if(ESAPI.validator().isValidInput(this.getClass().getName(), gson.toJson(userAuth), "Space", Integer.MAX_VALUE,false)){
            return userAuth;
        }else{
            return null;
        }
    }
}