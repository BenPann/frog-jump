package com.kgi.eopend3.web.jwt;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kgi.airloanex.common.dto.response.ApplyStartInitResp;
import com.kgi.airloanex.common.dto.response.ApplyStartResp;
import com.kgi.airloanex.common.dto.response.LogonInitResp;
import com.kgi.airloanex.common.dto.response.LogonResp;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.util.TokenService;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jwt.EncryptedJWT;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import static com.kgi.eopend3.web.jwt.JWTConst.*;
// import java.util.Date;
// import com.nimbusds.jose.EncryptionMethod;
// import com.nimbusds.jose.JWEAlgorithm;
// import com.nimbusds.jose.JWEHeader;
// import com.nimbusds.jose.crypto.DirectEncrypter;
// import com.nimbusds.jwt.EncryptedJWT;
// import com.nimbusds.jwt.JWTClaimsSet;

public class TokenAuthenticationService {

    /**
     * 有效時間 5天
     */
    // static final long EXPIRATIONTIME = 432_000_000;
    /**
     * JWT密碼
     */
    // static final String SECRET = "P@ssw02d";

    // static final byte[] key = "kgieopfuco201907".getBytes();
    /**
     * Token前缀
     */
    // static final String TOKEN_PREFIX = "Bearer";
    /**
     * 存放Token的Header Key
     */
    // static final String HEADER_STRING = "Authorization";

    protected static Logger logger = LoggerFactory.getLogger(TokenAuthenticationService.class);

    public static void addAuthentication(HttpServletRequest req, HttpServletResponse response, String principal) {
        // 產生JWT
        try {
            if (req.getRequestURI().endsWith(APPLY_START_URL)) {
                logger.info("### addAuthenticationApplyStart");
                addAuthenticationApplyStart(response, principal);
            } else if (req.getRequestURI().endsWith(LOGON_URL)) {
                logger.info("### addAuthenticationLogon");
                addAuthenticationLogon(response, principal);
            }
        } catch (Exception e) {
            logger.error("未知錯誤", e);
        }
    }

    private static void addAuthenticationApplyStart(HttpServletResponse response, String principal) throws IOException {
        // 產生JWT
        Gson gson = new Gson();
        ApplyStartResp caseResp = gson.fromJson(principal, ApplyStartResp.class);
        String jwt = TokenService.addAuthentication(gson.toJson(caseResp.getLoginResp()));          

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains; preload");
        response.setStatus(HttpServletResponse.SC_OK);
        logger.info("### jwt applyStart = " + jwt);
        ApplyStartInitResp resp = new ApplyStartInitResp(jwt, caseResp.getCaseDataResp(), caseResp.getBreakPoint(), caseResp.isCaseCreated());
        //response.getOutputStream().println(WebResult.GetSuccessResult(resp));
        PrintWriter pw = response.getWriter();            
        pw.print(WebResult.GetSuccessResult(resp));
    }
    private static void addAuthenticationLogon(HttpServletResponse response, String principal) throws IOException {
        // 產生JWT
        Gson gson = new Gson();
        LogonResp caseResp = gson.fromJson(principal, LogonResp.class);
        String jwt = TokenService.addAuthentication(gson.toJson(caseResp.getLoginResp()));          

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains; preload");
        response.setStatus(HttpServletResponse.SC_OK);
        logger.info("### jwt logon = " + jwt);
        LogonInitResp resp = new LogonInitResp(jwt);
        //response.getOutputStream().println(WebResult.GetSuccessResult(resp));
        PrintWriter pw = response.getWriter();            
        pw.print(WebResult.GetSuccessResult(resp));
    }

    static Authentication getAuthentication(HttpServletRequest request) throws Exception {
        // 从Header中拿到token
        String token = request.getHeader(SystemConst.HEADER_STRING);
        //System.out.println("token:" + token);
        String header = null;
        if (token != null) {
            // 解析 Token
            String jwt = token.substring(SystemConst.TOKEN_PREFIX.length());
            EncryptedJWT jweObject = EncryptedJWT.parse(jwt);

            // do the decryption
            jweObject.decrypt(new DirectDecrypter(SystemConst.key));

            header = jweObject.getJWTClaimsSet().getSubject();
           
            // 回傳驗證過的資料
            return (header != null&&!header.equals("")) ? new UsernamePasswordAuthenticationToken(header, null, null) : null;

        }
        return null;
    }

}