package com.kgi.eopend3.web.jwt;

import static com.kgi.eopend3.web.jwt.JWTConst.APPLY_START;
import static com.kgi.eopend3.web.jwt.JWTConst.LOGON;
import static com.kgi.eopend3.web.jwt.JWTConst.STP;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.kgi.airloanex.common.dto.response.ApplyStartResp;
import com.kgi.airloanex.common.dto.response.LogonResp;
import com.kgi.airloanex.common.dto.view.ApplyStartView;
import com.kgi.airloanex.common.dto.view.LogonView;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.util.CheckUtil;
import com.kgi.eopend3.web.dao.HttpDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private HttpDao httpDao;

    
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public CustomAuthenticationProvider(ApplicationContext appContext) {
        appContext.getAutowireCapableBeanFactory().autowireBean(this);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Authentication auth = null;
        Object printcipal = authentication.getPrincipal();
        if (printcipal instanceof ApplyStartView) {
            logger.info("### authenticateApplyStartView");
            auth = authenticateApplyStartView(authentication);
        } else if (printcipal instanceof LogonView) {
            logger.info("### authenticateLogonView");
            auth = authenticateLogonView(authentication);
        }
        return auth;
    }
    private Authentication authenticateApplyStartView(Authentication authentication) throws AuthenticationException {
        Object printcipal = authentication.getPrincipal();
        if (printcipal instanceof ApplyStartView) {
            // 取得驗證的帳號密碼 
            ApplyStartView ac = (ApplyStartView) authentication.getPrincipal();
            // 驗證邏輯
            Gson gson = new Gson();
            String reqString = gson.toJson(ac);
            if (!CheckUtil.check(ac)) {
                logger.info("### 驗證失敗");
                throw new BadCredentialsException("驗證失敗");
            } else {
                String resp = httpDao.getAPResponseNoAuth(STP, APPLY_START, reqString);
                logger.info("ap resp:" + resp);
                WebResult result = gson.fromJson(resp, WebResult.class);
                if (result.getStatus() == 0 && !result.getResult().equals("")) {
                    String principal = gson.toJson(result.getResult());
                    ApplyStartResp caseResp = gson.fromJson(principal, ApplyStartResp.class);
                    if (!caseResp.getLoginResp().getUniqId().equals("")) {
                        // 依據回傳的結果設定權限(如果有)
                        ArrayList<GrantedAuthority> authorities = new ArrayList<>();
                        // authorities.add(new GrantedAuthorityImpl("ROLE_ADMIN"));
                        // authorities.add(new GrantedAuthorityImpl("AUTH_WRITE"));
                        // 生成令牌
                        Authentication auth = new UsernamePasswordAuthenticationToken(principal, null,
                                authorities);
                        return auth;
                    } else {
                        throw new BadCredentialsException(result.getMessage());
                    }
                } else {
                    throw new BadCredentialsException(result.getMessage());
                }
            }
        } else {
            return null;
        }
    }

    private Authentication authenticateLogonView(Authentication authentication) throws AuthenticationException {
        Object printcipal = authentication.getPrincipal();
        if (printcipal instanceof LogonView) {
            // 取得驗證的帳號密碼 
            LogonView ac = (LogonView) authentication.getPrincipal();
            // 驗證邏輯
            Gson gson = new Gson();
            String reqString = gson.toJson(ac);
            if (!CheckUtil.check(ac)) {
                logger.info("### 驗證失敗");
                throw new BadCredentialsException("驗證失敗");
            } else {
                String resp = httpDao.getAPResponseNoAuth(STP, LOGON, reqString);
                logger.info("ap resp:" + resp);
                WebResult result = gson.fromJson(resp, WebResult.class);
                if (result.getStatus() == 0 && !result.getResult().equals("")) {
                    String principal = gson.toJson(result.getResult());
                    LogonResp caseResp = gson.fromJson(principal, LogonResp.class);
                    if (!caseResp.getLoginResp().getUniqId().equals("")) {
                        // 依據回傳的結果設定權限(如果有)
                        ArrayList<GrantedAuthority> authorities = new ArrayList<>();
                        // authorities.add(new GrantedAuthorityImpl("ROLE_ADMIN"));
                        // authorities.add(new GrantedAuthorityImpl("AUTH_WRITE"));
                        // 生成令牌
                        Authentication auth = new UsernamePasswordAuthenticationToken(principal, null,
                                authorities);
                        return auth;
                    } else {
                        throw new BadCredentialsException(result.getMessage());
                    }
                } else {
                    throw new BadCredentialsException(result.getMessage());
                }
            }
        } else {
            return null;
        }
    }

    // 是否可以提供输入類型的驗證服務
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}