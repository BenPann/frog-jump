package com.kgi.eopend3.web.config;

import com.kgi.eopend3.web.jwt.CustomAuthenticationProvider;
import com.kgi.eopend3.web.jwt.JWTAuthenticationFilter;
import com.kgi.eopend3.web.jwt.JWTLoginFilter;
import com.kgi.eopend3.web.jwt.JwtAuthenticationEntryPoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static com.kgi.eopend3.web.jwt.JWTConst.LOGON_URL;
import static com.kgi.eopend3.web.jwt.JWTConst.APPLY_START_URL;

@Configuration
@EnableWebSecurity
// @EnableGlobalMethodSecurity(securedEnabled = true)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @Autowired
    private ApplicationContext appContext;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
                // 開啟cors
                http.cors()
                // 關閉csrf
                .and().csrf().disable()
                // 驗證需求
                .authorizeRequests()
                // client底下都允許通過
                //.antMatchers("/front/**").permitAll()
                // api2底下都允許通過
                //.antMatchers("/publicApi/**").permitAll()
                // 所有需求都需要驗證
                .anyRequest().authenticated()
                // exception處理
                .and().exceptionHandling().authenticationEntryPoint(unauthorizedHandler)
                // session處理
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                //.addFilterBefore(filterDemo3, UsernamePasswordAuthenticationFilter.class)
                // 加入驗證登入的filter
                .addFilterBefore(new JWTLoginFilter(LOGON_URL, authenticationManager()), UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(new JWTLoginFilter(APPLY_START_URL, authenticationManager()), UsernamePasswordAuthenticationFilter.class)
                // 加入驗證token的filter
                .addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 使用自訂的身分驗證類別
        auth.authenticationProvider(new CustomAuthenticationProvider(appContext));
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                // Auth service
                .antMatchers("/publicApi/**").antMatchers("/front/**");

    }

    @Bean
    public BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

}
