package com.kgi.eopend3.web.jwt;

public class JWTConst {
    public static final String STP = "stp";
    public static final String LOGON = "logon";
    public static final String LOGON_URL = "/api/stp/logon";
    public static final String APPLY_START = "applyStart";
    public static final String APPLY_START_URL = "/api/stp/applyStart";
}
