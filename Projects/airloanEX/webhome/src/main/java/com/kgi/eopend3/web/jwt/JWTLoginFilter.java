package com.kgi.eopend3.web.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kgi.airloanex.common.dto.view.ApplyStartView;
import com.kgi.airloanex.common.dto.view.LogonView;
import com.kgi.eopend3.common.dto.WebResult;

import org.owasp.esapi.ESAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import nl.basjes.parse.useragent.UserAgent;
import nl.basjes.parse.useragent.UserAgentAnalyzer;

import static com.kgi.eopend3.common.util.RequestUtil.getIpAddr;

import static com.kgi.eopend3.web.jwt.JWTConst.*;

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final UserAgentAnalyzer uaa;

    public JWTLoginFilter(String url, AuthenticationManager authManager) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authManager);

        uaa = UserAgentAnalyzer.newBuilder().withAllFields().build();
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
            throws AuthenticationException, IOException, ServletException {
        Authentication auth = null; 
        if (req.getRequestURI().endsWith(APPLY_START_URL)) {
            logger.info("### doApplyStart");
            auth = doApplyStart(req, res);
        } else if (req.getRequestURI().endsWith(LOGON_URL)) {
            logger.info("### doLogon");
            auth = doLogon(req, res);
        }
        return auth;
    }

    private Authentication doApplyStart(HttpServletRequest req, HttpServletResponse res)
        throws JsonParseException, JsonMappingException, IOException {
        // 登入資訊解密 有需要的話在此做
        // JSON反序列化成 AccountCredentials
        ApplyStartView loginAuth = new ObjectMapper().readValue(req.getInputStream(), ApplyStartView.class);
        loginAuth.setIpAddress(getIpAddr(req));
        UserAgent userAgent = uaa.parse(req.getHeader("User-Agent"));
        String browser = userAgent.getValue("AgentNameVersionMajor");
        String platform = userAgent.getValue("DeviceClass");
        String os = userAgent.getValue("OperatingSystemNameVersionMajor");
        loginAuth.setBrowser(browser);
        loginAuth.setPlatform(platform);
        loginAuth.setOs(os);

        UsernamePasswordAuthenticationToken uToken = new UsernamePasswordAuthenticationToken(loginAuth, null);
        Authentication auth = getAuthenticationManager().authenticate(uToken);
        // 返回驗證TOKEN
        return auth;
    }

    private Authentication doLogon(HttpServletRequest req, HttpServletResponse res)
            throws JsonParseException, JsonMappingException, IOException {
        // 登入資訊解密 有需要的話在此做
        // JSON反序列化成 AccountCredentials
        LogonView loginAuth = new ObjectMapper().readValue(req.getInputStream(), LogonView.class);
        loginAuth.setIpAddress(getIpAddr(req));
        
        // UserAgent 詳細如下，
        // - user_agent_string: 'Mozilla/5.0 (Linux; Android 7.0; Nexus 6 Build/NBD90Z) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.124 Mobile Safari/537.36'
        // DeviceClass                      : 'Phone'
        // DeviceName                       : 'Google Nexus 6'
        // DeviceBrand                      : 'Google'
        // OperatingSystemClass             : 'Mobile'
        // OperatingSystemName              : 'Android'
        // OperatingSystemVersion           : '7.0'
        // OperatingSystemVersionMajor      : '7'
        // OperatingSystemNameVersion       : 'Android 7.0'
        // OperatingSystemNameVersionMajor  : 'Android 7'
        // OperatingSystemVersionBuild      : 'NBD90Z'
        // LayoutEngineClass                : 'Browser'
        // LayoutEngineName                 : 'Blink'
        // LayoutEngineVersion              : '53.0'
        // LayoutEngineVersionMajor         : '53'
        // LayoutEngineNameVersion          : 'Blink 53.0'
        // LayoutEngineNameVersionMajor     : 'Blink 53'
        // AgentClass                       : 'Browser'
        // AgentName                        : 'Chrome'
        // AgentVersion                     : '53.0.2785.124'
        // AgentVersionMajor                : '53'
        // AgentNameVersion                 : 'Chrome 53.0.2785.124'
        // AgentNameVersionMajor            : 'Chrome 53'
        UserAgent userAgent = uaa.parse(req.getHeader("User-Agent"));
		String browser = userAgent.getValue("AgentNameVersionMajor");
		String platform = userAgent.getValue("DeviceClass");
        String os = userAgent.getValue("OperatingSystemNameVersionMajor");
        loginAuth.setBrowser(browser);
        loginAuth.setPlatform(platform);
        loginAuth.setOs(os);
        
        UsernamePasswordAuthenticationToken uToken = new UsernamePasswordAuthenticationToken(loginAuth, null);
        Authentication auth = getAuthenticationManager().authenticate(uToken);
        // 返回驗證TOKEN
        return auth;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
            Authentication auth) throws IOException, ServletException {
        TokenAuthenticationService.addAuthentication(req, res, auth.getName());
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException failed) throws IOException, ServletException {
        response.setContentType("application/json;charset=UTF-8");
        response.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains; preload");
        response.setStatus(HttpServletResponse.SC_OK);
        String msg = failed.getMessage();
        if(ESAPI.validator().isValidInput("unsuccessfulAuthentication", msg, "Space", Integer.MAX_VALUE, false)){
            response.getWriter().print(new WebResult(500, msg, "").toString());
        }else{
            response.getWriter().print(new WebResult(500, "", "").toString());
        }
    }
}