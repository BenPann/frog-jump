package com.kgi.eopend3.web;

import com.nimbusds.jose.JWEHeader;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Date;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jwt.EncryptedJWT;
import com.nimbusds.jwt.JWTClaimsSet;
// import javax.servlet.http.HttpServletRequest;
// import javax.servlet.http.HttpServletResponse;
// import com.kgi.eopend3.common.dto.WebResult;
// import com.nimbusds.jose.JWEHeader;
// import com.nimbusds.jose.crypto.DirectDecrypter;
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
// import org.springframework.security.core.Authentication;
// import com.google.gson.Gson;
// import com.kgi.eopend3.common.dto.respone.LoginResp;
// import com.kgi.eopend3.common.dto.view.LoginView;
// import com.kgi.eopend3.common.dto.WebResult;
// import com.kgi.eopend3.common.util.CheckUtil;
// import com.kgi.eopend3.web.dao.HttpDao;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.context.ApplicationContext;
// import org.springframework.security.authentication.AuthenticationProvider;
// import org.springframework.security.authentication.BadCredentialsException;
// import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
// import org.springframework.security.core.Authentication;
// import org.springframework.security.core.AuthenticationException;
// import org.springframework.security.core.GrantedAuthority;
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StartWebApplicationTests {

	/**
     * 有效時間 5天
     */
    static final long EXPIRATIONTIME = 432_000_000;
    /**
     * JWT密碼
     */
    static final String SECRET = "P@ssw02d";

    static final byte[] key = "kgieopfuco201907".getBytes();
    /**
     * Token前缀
     */
    static final String TOKEN_PREFIX = "Bearer";
    /**
     * 存放Token的Header Key
     */
    static final String HEADER_STRING = "Authorization";

	@Test
	public void main() {		
		try {

            // Create the header
            JWEHeader header = new JWEHeader(JWEAlgorithm.DIR, EncryptionMethod.A128GCM);

            JWTClaimsSet.Builder claimsSet = new JWTClaimsSet.Builder();
            claimsSet.subject("{\"uniqid\":\"ED320190101000001\",\"uniqType\":\"13\",\"idno\":\"F111111111\",\"ipAddress\":\"127.0.0.1\"}");
            claimsSet.expirationTime(new Date(System.currentTimeMillis() + EXPIRATIONTIME));
            // claimsSet.notBeforeTime(new Date());

            // create the EncryptedJWT object
            EncryptedJWT jweObject = new EncryptedJWT(header, claimsSet.build());

            jweObject.encrypt(new DirectEncrypter(key));

            // Serialise to compact JOSE form...
            String JWT = jweObject.serialize();
            System.out.println(JWT);

            
        } catch (Exception e) {
            
        }
    }
    
}

