﻿echo on
chcp 65001
cls 
echo off
cls
echo.
del /Q "\\172.31.1.117\mapower\jboss-7.0_For_STP_UAT\standalone\tmp\*"
FOR /D %%p IN ("\\172.31.1.117\mapower\jboss-7.0_For_STP_UAT\standalone\tmp\*.*") DO rmdir "%%p" /s /q
echo 已清除UAT-WEB-tmp \\172.31.1.117\mapower\jboss-7.0_For_STP_UAT\standalone\tmp\*.*
echo.
del /Q "\\172.31.1.117\mapower\jboss-7.0_For_STP_UAT\standalone\log\*"
FOR /D %%p IN ("\\172.31.1.117\mapower\jboss-7.0_For_STP_UAT\standalone\log\*.*") DO rmdir "%%p" /s /q
echo 已清除UAT-WEB-log \\172.31.1.117\mapower\jboss-7.0_For_STP_UAT\standalone\log\*.*
echo.
del /Q "\\172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_airloanEX_UAT\tmp\*"
FOR /D %%p IN ("\\172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_airloanEX_UAT\tmp\*.*") DO rmdir "%%p" /s /q
echo 已清除UAT-AP-tmp \\172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_airloanEX_UAT\tmp\*.*
echo.
del /Q "\\172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_airloanEX_UAT\log\*"
FOR /D %%p IN ("\\172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_airloanEX_UAT\log\*.*") DO rmdir "%%p" /s /q
echo 已清除UAT-AP-log \\172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_airloanEX_UAT\log\*.*
echo.
echo.
echo 複製 airloanEX.war to UAT-WEB.....
copy airloanEX.war \\172.31.1.117\mapower\jboss-7.0_For_STP_UAT\standalone\deployments\airloanEX.war /y /z
echo. 
echo. 
echo 複製 airloanEXHOME.war to UAT-WEB.....
copy airloanEXHOME.war \\172.31.1.117\mapower\jboss-7.0_For_STP_UAT\standalone\deployments\airloanEXHOME.war /y /z
echo. 
echo.  
echo 複製 airloanEXAP.war to UAT-AP.....
copy airloanEXAP.war \\172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_airloanEX_UAT\deployments\airloanEXAP.war /y /z
echo. 
echo. 
echo 複製完成!
pause