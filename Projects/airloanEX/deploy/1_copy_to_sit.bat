﻿echo on
chcp 65001
cls 
echo off
cls
echo.
del /Q "\\172.31.1.117\mapower\jboss-7.0_For_STP\standalone\tmp\*"
FOR /D %%p IN ("\\172.31.1.117\mapower\jboss-7.0_For_STP\standalone\tmp\*.*") DO rmdir "%%p" /s /q
echo 已清除 SIT-WEB-tmp \\172.31.1.117\mapower\jboss-7.0_For_STP\standalone\tmp\*.*
echo.
del /Q "\\172.31.1.117\mapower\jboss-7.0_For_STP\standalone\log\*"
FOR /D %%p IN ("\\172.31.1.117\mapower\jboss-7.0_For_STP\standalone\log\*.*") DO rmdir "%%p" /s /q
echo 已清除 SIT-WEB-log \\172.31.1.117\mapower\jboss-7.0_For_STP\standalone\log\*.*
echo.
del /Q "\\172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_airloanEX\tmp\*"
FOR /D %%p IN ("\\172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_airloanEX\tmp\*.*") DO rmdir "%%p" /s /q
echo 已清除 SIT-AP-tmp \\172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_airloanEX\tmp\*.*
echo.
del /Q "\\172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_airloanEX\log\*"
FOR /D %%p IN ("\\172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_airloanEX\log\*.*") DO rmdir "%%p" /s /q
echo 已清除 SIT-AP-log \\172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_airloanEX\log\*.*
echo.
echo.
echo 複製 airloanEX.war to SIT-WEB.....
copy airloanEX.war \\172.31.1.117\mapower\jboss-7.0_For_STP\standalone\deployments\airloanEX.war /y /z
echo.
echo. 
echo 複製 airloanEXHOME.war to SIT-WEB.....
copy airloanEXHOME.war \\172.31.1.117\mapower\jboss-7.0_For_STP\standalone\deployments\airloanEXHOME.war /y /z
echo.
echo. 
echo 複製 airloanEXAP.war to SIT-AP.....
copy airloanEXAP.war \\172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_airloanEX\deployments\airloanEXAP.war /y /z
echo.
echo. 
echo 複製完成!
pause