package com.kgi.crosssell.web.service;

import java.io.IOException;

import com.kgi.crosssell.common.dto.UserObject;
import com.kgi.crosssell.web.dao.HttpDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.json.JSONObject;

@Service
public class LogService {

	@Autowired
	private HttpDao httpDao;

	private final String FUNCTION_PATH = "log";

	// 寫瀏覽歷程記錄
	public String saveBrowseRecord(String uniqId, String uniqType, String page, String action) throws IOException {

		String apiurl = FUNCTION_PATH.concat("/saveBrowseRecord");

		JSONObject  paramJson = new JSONObject();
		paramJson.put("UniqId", uniqId);
		paramJson.put("UniqType", uniqType);
		paramJson.put("Page", page);
		paramJson.put("Action", action);

		return httpDao.getAPResponse(apiurl, paramJson);
	}

	public String saveBrowseRecord(UserObject uo, String page, String action) throws IOException {

		String apiurl = FUNCTION_PATH.concat("/saveBrowseRecord");

		JSONObject  paramJson = new JSONObject();
		paramJson.put("UniqId", uo.getUniqId());
		paramJson.put("UniqType", uo.getUniqType());
		paramJson.put("Page", page);
		paramJson.put("Action", action);

		return httpDao.getAPResponse(apiurl, paramJson);
	}

	public String processStart(String uniqId, String uniqType) throws IOException {

		String apiurl = FUNCTION_PATH.concat("/processStart");

		return httpDao.getAPResponseByGetMethod(apiurl, new String[][] { {"uniqId", uniqId}, {"uniqType", uniqType}});
	}

	public String processEnd(String uniqId, String uniqType) throws IOException {

		String apiurl = FUNCTION_PATH.concat("/processEnd");

		return httpDao.getAPResponseByGetMethod(apiurl, new String[][] { {"uniqId", uniqId}, {"uniqType", uniqType}});
	}

	public String processPageStart(String uniqId, String uniqType, String page) throws IOException {

		String apiurl = FUNCTION_PATH.concat("/processPageStart");

		return httpDao.getAPResponseByGetMethod(apiurl, new String[][] { {"uniqId", uniqId}, {"uniqType", uniqType}
				, {"page", page}});
	}

	public String processPageEnd(String uniqId, String uniqType, String page) throws IOException {

		String apiurl = FUNCTION_PATH.concat("/processPageEnd");

		return httpDao.getAPResponseByGetMethod(apiurl, new String[][] { {"uniqId", uniqId}, {"uniqType", uniqType}
				, {"page", page}});
	}

	public String processPageEnd(String uniqId, String uniqType) throws IOException {

		String apiurl = FUNCTION_PATH.concat("/processPageEndByUniqId");

		JSONObject  paramJson = new JSONObject();
		paramJson.put("uniqId", uniqId);
		paramJson.put("uniqType", uniqType);

		return httpDao.getAPResponse(apiurl, paramJson);
	}

	public String updateBrowseRecordbpId(String UniqId, String bpUniqId) throws IOException {

		String apiurl = FUNCTION_PATH.concat("/updateBrowseRecordbpId");

		JSONObject  paramJson = new JSONObject();
		paramJson.put("UniqId", UniqId);
		paramJson.put("bpUniqId", bpUniqId);

		return httpDao.getAPResponse(apiurl, paramJson);
	}

	public String updateProcessRecordbpId(String UniqId, String bpUniqId) throws IOException {

		String apiurl = FUNCTION_PATH.concat("/updateProcessRecordbpId");

		JSONObject  paramJson = new JSONObject();
		paramJson.put("UniqId", UniqId);
		paramJson.put("bpUniqId", bpUniqId);

		return httpDao.getAPResponse(apiurl, paramJson);
	}

	public String updateProcessPageRecordbpId(String UniqId, String bpUniqId) throws IOException {

		String apiurl = FUNCTION_PATH.concat("/updateProcessPageRecordbpId");

		JSONObject  paramJson = new JSONObject();
		paramJson.put("UniqId", UniqId);
		paramJson.put("bpUniqId", bpUniqId);

		return httpDao.getAPResponse(apiurl, paramJson);
	}

	public String saveIndexClick(String entry, String browser, String platform, String os, String action)
			throws IOException {

		String apiurl = FUNCTION_PATH.concat("/saveIndexClick");

		JSONObject  paramJson = new JSONObject();
		paramJson.put("browser", browser);
		paramJson.put("platform", platform);
		paramJson.put("os", os);
		paramJson.put("action", action);

		return httpDao.getAPResponse(apiurl, paramJson);
	}

}
