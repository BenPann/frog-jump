package com.kgi.crosssell.web.service;

import java.io.IOException;

import com.kgi.crosssell.common.constants.ResponseCode;
import com.kgi.crosssell.web.dao.HttpDao;
import com.kgi.eopend3.common.util.CommonFunctionUtil;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import net.sf.json.JSONObject;

@Service
public class CrossSellService {

	@Autowired
	private HttpDao httpDao;

	private final String FUNCTION_PATH = "cs";

	public String getCSData(String prodid, String channelId, String token, String idno, String ip) throws IOException {

		String apiurl = FUNCTION_PATH.concat("/getCSData");

		JSONObject  paramJson = new JSONObject();
		paramJson.put("prodid", prodid);
		paramJson.put("channelId", channelId);
		paramJson.put("token", token);
		paramJson.put("idno", idno);
		paramJson.put("ip", ip);

		return httpDao.getAPResponse(apiurl, paramJson);
	}

	public String getCSCommonData(String prodId, String channelId, String prodType, String token) throws IOException {

		String apiurl = FUNCTION_PATH.concat("/getCSCommonData");

		JSONObject  paramJson = new JSONObject();
		paramJson.put("prodId", prodId);
		paramJson.put("channelId", channelId);
		paramJson.put("prodType", prodType);
		paramJson.put("token", token);

		return httpDao.getAPResponse(apiurl, paramJson);
	}

	public String insertFucoApiLog(String prodid, String channelId, String token, String idno, String ip,
			String shortUrl, String depart, String member, String entry) throws IOException {

		String apiurl = FUNCTION_PATH.concat("/insertFucoApiLog");

		JSONObject  paramJson = new JSONObject();
		paramJson.put("prodid", prodid);
		paramJson.put("channelId", channelId);
		paramJson.put("token", token);
		paramJson.put("idno", idno);
		paramJson.put("ip", ip);
		paramJson.put("shortUrl", shortUrl);
		paramJson.put("depart", depart);
		paramJson.put("member", member);
		paramJson.put("entry", entry);

		return httpDao.getAPResponse(apiurl, paramJson);
	}

	public String insertCSCommonData(String uniqId, String uniqType, String CSType, String channelId,
			String resultData) {

		String apiurl = FUNCTION_PATH.concat("/insertCSCommonData");

		JSONObject  paramJson = new JSONObject();
		paramJson.put("uniqId", uniqId);
		paramJson.put("uniqType", uniqType);
		paramJson.put("CSType", CSType);
		paramJson.put("channelId", channelId);
		paramJson.put("resultData", resultData);

		return httpDao.getAPResponse(apiurl, paramJson);
	}

	// 20200908取得跨售利率forPchome
	public String getOrderRate(String UniqId) {

		String apiurl = FUNCTION_PATH.concat("/getOrderRate");

		JSONObject  paramJson = new JSONObject();
		paramJson.put("UniqId", UniqId);

		return httpDao.getAPResponse(apiurl, paramJson);
	}

	public Boolean checkCSParameter(String prodId, String channelId, String token, String idno, String ip,
			String shortUrl, String depart, String member, String entry) {
		if (CommonFunctionUtil.StringIsNull(prodId, channelId, token, idno, ip, shortUrl, depart, member, entry)) {
			return false;
		}
		prodId = prodId.trim();
		if (!prodId.equals("loan") && !prodId.equals("credit")) {
			return false;
		}
		return true;
	}
	
	public String getApplyPageUrl(String prodType, String id, ResponseCode code){
		String url = FUNCTION_PATH.concat("/getApplyPage").concat("?prodType=")
			.concat(prodType);
		String applyUrl = httpDao.getAPResponseByGetMethod(url);
		
		String result = applyUrl + "?";

		Boolean isHasId = Boolean.FALSE;
		if(!StringUtils.isEmpty(id)){
			isHasId = Boolean.TRUE;
			result = result + "CrossSellId=" + id; 
		}
		if(!StringUtils.isEmpty(code.name())){
			if(isHasId){
				result = result + "&";
			}
			result = result + "CrossSellCode=" + code.name().substring(1);
		}
		return result;
	}
}
