package com.kgi.crosssell.web.aspectJ;

import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.kgi.crosssell.common.dto.UserObject;
import com.kgi.crosssell.web.service.LogService;

@Aspect
public class ControllerAOP {

    @Autowired
    private LogService logService;
    @Autowired
    private HttpSession session;
    @Autowired
    private HttpServletRequest request;

    @Pointcut("@annotation(org.springframework.web.bind.annotation.ResponseBody) || execution(private * *(..))")
    public void avoidMethod() {
    }

    /**
     * Point to controller with requestMappin and excution any method in
     * CreditCardApplyCtl.Class
     */
    @Pointcut("within(@org.springframework.stereotype.Controller *) &&" + "@annotation(requestMapping) &&"
            + " !avoidMethod() &&" + "execution(* com.kgi.ploa.ctl.CreditCardApplyCtl.*(..)) &&"
            + "!execution(* *.*BreakPoint(..))")
    public void pointer(RequestMapping requestMapping) {

    }

    @AfterReturning("pointer(requestMapping)")
    public void advice(RequestMapping requestMapping, JoinPoint thisJoinPoint) {
        try {
            /** Another way to get current session and request */
            // HttpSession session = (HttpSession)
            // RequestContextHolder.currentRequestAttributes().resolveReference(RequestAttributes.REFERENCE_SESSION);
            // HttpServletRequest request = (HttpServletRequest)
            // RequestContextHolder.currentRequestAttributes().resolveReference(RequestAttributes.REFERENCE_REQUEST);
            UserObject uo = (UserObject) session.getAttribute("applyccuo");
            logService.saveBrowseRecord(uo, request.getRequestURI(), request.getMethod());
            if (StringUtils.containsIgnoreCase(requestMapping.method()[0].toString(), "get")) {
                if (StringUtils.containsIgnoreCase(requestMapping.value()[0], "init")) {
                    logService.processStart(uo.getUniqId(), uo.getUniqType());
                }
                if (!StringUtils.containsIgnoreCase(requestMapping.value()[0], "complete")) {
                    logService.processPageStart(uo.getUniqId(), uo.getUniqType(), request.getRequestURI());
                }
            } else {
                if (StringUtils.containsIgnoreCase(requestMapping.value()[0], "filldata")) {
                    logService.processEnd(uo.getUniqId(), uo.getUniqType());
                }
                logService.processPageEnd(uo.getUniqId(), uo.getUniqType(), request.getRequestURI());
            }
        } catch (Exception e) {
            System.out.println("null Session or null UserObject error");
        }

    }

}
