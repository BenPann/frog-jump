package com.kgi.crosssell.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = { "file:${FUCO_CONFIG_PATH}/WEB.properties" }, encoding = "UTF-8")
public class GlobalConfig {

    @Value("${Server.AP.Host}")
    public String APServerHost;
    
}