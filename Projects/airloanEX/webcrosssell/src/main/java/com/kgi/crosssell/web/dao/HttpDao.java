package com.kgi.crosssell.web.dao;

import java.nio.charset.Charset;

import com.kgi.crosssell.web.config.GlobalConfig;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.SocketConfig;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.sf.json.JSONObject;

@Repository
public class HttpDao {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    GlobalConfig globalConfig;

    /**
     * 共用的取得AP回應
     */
    public String getAPResponse(String url, JSONObject paramJson) {
        String result = "";

        url = globalConfig.APServerHost.concat(url);

        int timeOut = 30000;
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        // 對連線池設定SocketConfig物件
        connManager.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(timeOut).build());

        try (CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build()) {

            // logger.info(url);
            HttpPost post = new HttpPost(url);

            StringEntity entity = new StringEntity(paramJson.toString(), "UTF-8");
            entity.setContentEncoding("UTF-8");

            post.setEntity(entity);
            post.setHeader("Accept", "application/json");
            post.setHeader("Content-type", "application/json;charset=utf-8");

            HttpResponse response = client.execute(post);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // 必須用UTF-8 不然會有亂碼
                result = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
            } else if (statusCode == 404) {
                // 要回傳找不到給打API的人知道
                result = "404";
            } else {
                logger.info("回應代碼:" + statusCode);
                result = String.valueOf(statusCode);
            }
        } catch (Exception ex) {
            logger.warn("getAPResponse", ex);
        }

        return result;
    }

    /**
     * 共用的取得AP回應
     */
    public String getAPResponseByGetMethod(String url) {
        String result = "";

        url = globalConfig.APServerHost.concat(url);

        int timeOut = 30000;
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        // 對連線池設定SocketConfig物件
        connManager.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(timeOut).build());

        try (CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build()) {

            // logger.info(url);
            HttpGet get = new HttpGet(url);
            // 必須用UTF-8 不然會有亂碼
            get.setHeader("Accept", "application/json");
            get.setHeader("Content-type", "application/json");

            HttpResponse response = client.execute(get);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // 必須用UTF-8 不然會有亂碼
                result = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
            } else if (statusCode == 404) {
                // 要回傳找不到給打API的人知道
                result = "404";
            } else {
                logger.info("回應代碼:" + statusCode);
                result = String.valueOf(statusCode);
            }
        } catch (Exception ex) {
            logger.warn("getAPResponse", ex);
        }

        return result;
    }

    /**
     * 共用的取得AP回應
     */
    public String getAPResponseByGetMethod(String url, String[][] params) {
        String result = "";

        url = globalConfig.APServerHost.concat(url);

		if (params != null){
			Boolean tag = Boolean.FALSE;
			for(String[] rowArray : params){
				if(rowArray.length == 2){
					if(!tag){
						url.concat("?");
					}
					url.concat(rowArray[0]).concat("=").concat(rowArray[1]).concat("&");
				}
			}
			if(tag){
				url = url.substring(1);
			}
		}

        int timeOut = 30000;
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        // 對連線池設定SocketConfig物件
        connManager.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(timeOut).build());

        try (CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build()) {

            // logger.info(url);
            HttpGet get = new HttpGet(url);
            // 必須用UTF-8 不然會有亂碼
            get.setHeader("Accept", "application/json");
            get.setHeader("Content-type", "application/json");

            HttpResponse response = client.execute(get);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // 必須用UTF-8 不然會有亂碼
                result = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
            } else if (statusCode == 404) {
                // 要回傳找不到給打API的人知道
                result = "404";
            } else {
                logger.info("回應代碼:" + statusCode);
                result = String.valueOf(statusCode);
            }
        } catch (Exception ex) {
            logger.warn("getAPResponse", ex);
        }

        return result;
    }
}