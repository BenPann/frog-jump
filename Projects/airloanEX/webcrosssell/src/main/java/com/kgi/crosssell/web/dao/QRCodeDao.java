package com.kgi.crosssell.web.dao;

import java.io.IOException;
import java.nio.charset.Charset;

import com.kgi.crosssell.web.config.GlobalConfig;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class QRCodeDao {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private String functionPath = "qr";

    static final int MAX_SIZE = 2147483647;
    public static int ConnectTimeout = 30000;
    public static int ReadTimeout = 30000;

    @Autowired
    GlobalConfig globalConfig;

    public void createUrlCaseMatch(String shortUrl, String uniqId, String uniqType) throws IOException {
        String apiurl = globalConfig.APServerHost.concat(functionPath).concat("/createUrlCaseMatch");
        this.getAPResponseByGetMethod(apiurl
            , new String[][] {{"shortUrl", shortUrl}, {"uniqId", uniqId}, {"uniqType", uniqType}});
    }

    public String getAPResponseByGetMethod(String url, String[][] paramsArray) {
        String result = "";

        if (paramsArray != null && paramsArray.length > 0){
            String queryString = "";
            for (String[] rowArray : paramsArray){
                if(rowArray.length == 2){
                    queryString.concat("&")
                        .concat(rowArray[0])
                        .concat("=")
                        .concat(rowArray[1]);
                }
            }
            if(queryString.length() > 0){
                queryString = queryString.substring(1);
                url = url.concat("?")
                    .concat(queryString);
            }
        }

        int timeOut = 30000;
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        // 對連線池設定SocketConfig物件
        connManager.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(timeOut).build());

        try (CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build()) {

            // logger.info(url);
            HttpGet get = new HttpGet(url);
            // 必須用UTF-8 不然會有亂碼
            get.setHeader("Accept", "application/json");
            get.setHeader("Content-type", "application/json");

            HttpResponse response = client.execute(get);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // 必須用UTF-8 不然會有亂碼
                result = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
            } else if (statusCode == 404) {
                // 要回傳找不到給打API的人知道
                result = "404";
            } else {
                logger.info("回應代碼:" + statusCode);
                result = String.valueOf(statusCode);
            }
        } catch (Exception ex) {
            logger.warn("getAPResponse", ex);
        }

        return result;
    }
}
