package com.kgi.crosssell.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

// @SpringBootApplication
@Configuration
@EnableAutoConfiguration
@ComponentScan({ "com.kgi.crosssell.web" })
public class StartWebcrosssellApplication extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(StartWebcrosssellApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(StartWebcrosssellApplication.class, args);
	}
}
