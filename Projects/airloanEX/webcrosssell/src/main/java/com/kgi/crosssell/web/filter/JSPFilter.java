package com.kgi.crosssell.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;

public class JSPFilter implements Filter {

	private boolean denyJsp = true;

	public JSPFilter() {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// if(denyJsp && (request instanceof HttpServletRequest)){
		// 	//[2019.11.13 GaryLiu] ==== START : csrf 黑箱 ====
		// 	if(
		// 		(((HttpServletRequest)request).getHeader("Referer") != null)
		// 		&&
		// 		// 當不是異業跨售或是證券異業才繼續往後檢查
		// 		(
		// 			((HttpServletRequest)request).getRequestURI() !=null 
		// 			&&
		// 			(		
		// 				!((HttpServletRequest)request).getRequestURI().contains("/CSCommon/credit/")
		// 				&&
		// 				!((HttpServletRequest)request).getRequestURI().contains("/CSCommon/loan/") 
		// 				&&
		// 				!((HttpServletRequest)request).getRequestURI().contains("/CS/credit/") 
		// 				&& 
		// 				!((HttpServletRequest)request).getRequestURI().contains("/CS/loan/")
		// 			)
		// 		)
		// 		&&
		// 		(!(((HttpServletRequest)request).getHeader("Referer").trim().startsWith(csrfToken)))
		// 		&&
		// 		(!(((HttpServletRequest)request).getHeader("Referer").trim().startsWith(ed3CsrfToken)))
		// 	){
		// 		System.out.println("不合法來源網址 : CSRF檢測失敗 -> Referer = " + ((HttpServletRequest)request).getHeader("Referer"));
		// 		request.getRequestDispatcher("/PageNotFound").forward(request, response);
		// 		return;
		// 	}
		// 	else
		// 	{
		// 		HttpServletRequest httpReq = (HttpServletRequest)request;
		// 		String lowcaseQueryStr = httpReq.getRequestURI().toLowerCase();
		// 		int q = lowcaseQueryStr.indexOf("?");
		// 		if(q>0){
		// 			lowcaseQueryStr = lowcaseQueryStr.substring(q);
		// 		}
		// 		if(lowcaseQueryStr.endsWith(".jsp")){
		// 			request.getRequestDispatcher("/PageNotFound").forward(request, response);
		// 			return;
		// 		}
		// 	}
		// 	//[2019.11.13 GaryLiu] ====  END  : csrf 黑箱 ====
		// }
		
		// // pass the request along the filter chain
		// chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}