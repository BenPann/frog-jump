package com.kgi.crosssell.web.ctl;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.kgi.airloanex.common.dto.db.ChannelData;
import com.kgi.crosssell.common.constants.ResponseCode;
import com.kgi.crosssell.common.dto.CSCommon;
import com.kgi.crosssell.common.dto.CSCommonData;
import com.kgi.crosssell.common.dto.CS_PersonalData;
import com.kgi.crosssell.common.dto.LoginFreeCif;
import com.kgi.crosssell.common.dto.UserObject;
import com.kgi.crosssell.web.service.CrossSellService;
import com.kgi.crosssell.web.service.EntryService;
import com.kgi.crosssell.web.service.LogService;
import com.kgi.crosssell.web.service.QRCodeService;
import com.kgi.crosssell.web.util.UserAgentInfo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.view.RedirectView;

import net.sf.json.JSONObject;

public abstract class GeneralCtl {

	protected abstract String getProcessName();

	private UserObject uo = null;

	private static final Logger logger = LogManager.getLogger(GeneralCtl.class.getName());

	@Autowired
	private QRCodeService qrCodeService;

	@Autowired
	private LogService logservice;
	
	@Autowired
	private EntryService entryService;

	@Autowired
	private CrossSellService cService;

	protected void createUserObject(HttpSession session, UserObject uo) {
		// 如果有ChannelData 就加入對應Table
		ChannelData cd = this.getChannelData(session);
		if (cd != null) {
			System.out.println("#### [createUserObject] 有channelData,目前短網址=" + cd.getShortUrl());
			try {
				if(!cd.getShortUrl().equals("")) {
					qrCodeService.createUrlCaseMatch(cd.getShortUrl(), uo.getUniqId(), uo.getUniqType());
				}
			} catch (IOException ex) {
				logger.warn("createUserObject", ex);
			}

		}
		// 預計會加入 每個流程存在不同session中

		// session.setAttribute(this.getProcessName(), uo);
	}

	protected ChannelData getChannelData(HttpSession session) {
		if (session == null) {
			return null;
		}
		ChannelData cd = (ChannelData) session.getAttribute("cd");
		if (cd == null) {
			return null;
		}
		return cd;
	}

	protected Boolean checkUserObject(HttpSession session) {
		if (session == null)
			return false;
		System.out.println(this.getProcessName());
		this.uo = (UserObject) session.getAttribute(this.getProcessName());
		if (this.uo == null) {
			return false;
		}
		if (this.uo.getUniqId() == null || this.uo.getUniqId().equals("")) {
			return false;
		}
		return true;
	}

	protected String getIpAddr(HttpServletRequest request) {
		// [2019.08.23 GaryLiu]==== START : 白箱測試 ====
		String ip = "";
		try {
			ip = ESAPI.validator().getValidInput("", request.getHeader("X-FORWARDED-FOR"), "IPAddress",
					Integer.MAX_VALUE, true);
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = ESAPI.validator().getValidInput("", request.getHeader("Proxy-Client-IP"), "IPAddress",
						Integer.MAX_VALUE, true);
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = ESAPI.validator().getValidInput("", request.getHeader("WL-Proxy-Client-IP"), "IPAddress",
						Integer.MAX_VALUE, true);
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = ESAPI.validator().getValidInput("", request.getHeader("HTTP_CLIENT_IP"), "IPAddress",
						Integer.MAX_VALUE, true);
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = ESAPI.validator().getValidInput("", request.getHeader("HTTP_X_FORWARDED_FOR"), "IPAddress",
						Integer.MAX_VALUE, true);
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = ESAPI.validator().getValidInput("", request.getRemoteAddr(), "IPAddress", Integer.MAX_VALUE, true);
			}
			System.out.println("Orinin IP : " + ip);

		} catch (IntrusionException | ValidationException e) {

			logger.info("Exception ESAPI", e);
			System.out.println("Exception ESAPI : " + e);
			// 補IP
			ip = request.getHeader("x-forwarded-for");
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("Proxy-Client-IP");
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("WL-Proxy-Client-IP");
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getRemoteAddr();
			}
			System.out.println("補查IP : " + ip);

		} finally {
			// IPV4的情況 把PORT號濾掉
			if (ip.split(":").length == 2) {
				ip = ip.split(":")[0];
			} else if (ip.split(":").length > 6) {
				// 如果是IP V6 先用此IP
				ip = "172.31.1.116";
			}
			System.out.println("Last IP : " + ip);
		}
		return ip;
		// [2019.08.23 GaryLiu]==== END : 白箱測試 ====
	}

	protected CSCommonData transCrossData(HttpSession session) {
		if (session == null) {
			System.out.println("session is null");
			return null;
		}
		Object data = session.getAttribute("PersonalData");
		//System.out.println("session.getAttribute(\"PersonalData\") "+data);
		if (data == null) {
			System.out.println("session.getAttribute(\"PersonalData\") is null");
			return null;
		}
		CSCommonData result = null;

		String CSType = "";
		// 如果session裡面存的是CS_PersonalData 表示從跨售來 要整併資料
		if (data instanceof CS_PersonalData) {
			System.out.println("表示從跨售來 要整併資料");
			result = new CSCommonData();
			// 將共用資料轉換到CSCommonData
			CS_PersonalData temp = (CS_PersonalData) data;
			/*System.out.println(temp);
			System.out.println(temp.getIdno());
			System.out.println(temp.getBirthday());*/
			result.setIdno(temp.getIdno());
			result.setBirthday(temp.getBirthday());
			result.setChtName(temp.getName());
			result.setMobile(temp.getMobile());
			result.setEmail(temp.getEMail());
			result.setCommAddrZipCode(temp.getCommAddrZipCode());
			result.setCommAddr(temp.getCommAddress());
			result.setHomeTelArea(temp.getResTelArea());
			result.setHomeTel(temp.getResPhone());
			result.setType("CS");
			CSType = "1";
			//加上記錄產品			
			result.setProductType((String)session.getAttribute("ProductType"));
		}
		// 如果session裡面是CSCommon 表示從cscommon來的 直接取得data
		if (data instanceof CSCommon) {
			System.out.println("表示從cscommon來的 直接取得data");
			CSCommon cs = (CSCommon) data;
			result = cs.getData();
			result.setType("CSCommon");
			CSType = "2";
			ChannelData cd = this.getChannelData(session);
			if(cd.getChannelId().equals("")){
				cd.setChannelId(result.getChannelID());
			}
			if(cd.getDepartId().equals("")){
				cd.setDepartId(result.getPromoDepart());
			}
			if(cd.getMember().equals("")){
				cd.setMember(result.getPromoMember());
			}
		}
		
		if (data instanceof LoginFreeCif) {
			System.out.println("表示從loginfree來的 直接取得data");
			LoginFreeCif cs = (LoginFreeCif) data;
			result = cs.getResult();
			result.setType("LoginFree");
			CSType = "3";
			ChannelData cd = this.getChannelData(session);
			if(cd.getChannelId().equals("")){
				cd.setChannelId(result.getChannelID());
			}
			if(cd.getDepartId().equals("")){
				cd.setDepartId(result.getPromoDepart());
			}
			if(cd.getMember().equals("")){
				cd.setMember(result.getPromoMember());
			}
		}
		System.out.println(result);
		// 將資料塞回去session		
		session.setAttribute("CSCommonData", result);
		String resultData = "";
		if(result!=null) {
			resultData = JSONObject.fromObject(result).toString();
		}
		if(CSType.equals("1")||CSType.equals("2")) {
			ChannelData cd = this.getChannelData(session);
			UserObject uo = (UserObject) session.getAttribute("uo");
			cService.insertCSCommonData(uo.getUniqId(), uo.getUniqType(), CSType, cd.getChannelId(),resultData );
		}else{
			session.setAttribute("FreeOTP", "Y");
		}
		return result;
	}

	protected CSCommonData getCsCommonData(HttpSession session){
		if (session == null) {
			System.out.println("session is null");
			return null;
		}
		Object data = session.getAttribute("CSCommonData");
		if (data == null){
			System.out.println("session CSCommonData is null");
			return null;
		}
		return (CSCommonData) data;
	}

	protected void saveBrowseRecord(UserObject uo, String page, String action) throws IOException {
		logservice.saveBrowseRecord(uo, page, action);
	}

	protected void ProcessStart(String uniqId, String uniqType) throws IOException {
		logservice.processStart(uniqId, uniqType);
	}

	protected void ProcessPageStart(String uniqId, String uniqType, String page) throws IOException {
		logservice.processPageStart(uniqId, uniqType, page);
	}

	/** 儲存入口資料 */
	protected void saveEntryData(String process, UserObject uo2, HttpServletRequest request,
			Map<String, String> allRequestParams) {
		try {
			// 取得連入資訊
			// 從哪邊連入 (ex:pchome)
			String entryKey = "Entry";
			// 推薦人
			String memberKey = "Member";
			String entry = "";
			String member = "";
			
			String nowIpAddr = getIpAddr(request);	//add @ 2020-11-19 by Ben
			
			StringBuilder sb = new StringBuilder();
			for (Map.Entry<String, String> e : allRequestParams.entrySet()) {
				String key = e.getKey();
				String value = e.getValue();
				if (key.equals(entryKey)) {
					entry = value.trim();
				} else if (key.equals(memberKey)) {
					member = value.trim();
				} else {
					sb.append(key).append("=").append(value).append(";");
				}
			}
			
			sb.append("ip").append("=").append(nowIpAddr).append(";");

			// 組儲存資訊
			JSONObject userAgentEntryData = JSONObject.fromObject(UserAgentInfo.getUserAgent(request));

			userAgentEntryData.put("UniqId", uo2.getUniqId());
			userAgentEntryData.put("UniqType", uo2.getUniqType());
			userAgentEntryData.put("Entry", entry);
			userAgentEntryData.put("Member", member);
			userAgentEntryData.put("Process", process);
			userAgentEntryData.put("Other", sb.toString());
			System.out.println(userAgentEntryData.toString());
			entryService.saveEntryData(userAgentEntryData);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
    // 進入申辦頁
    public RedirectView applyLoan(HttpServletRequest request, HttpSession session, String prodType) {
    	//[2019.09.04 GaryLiu]==== START : 白箱測試 ====
        try {
			logger.info("applyLoan start!");
            System.out.println(session.getId());
            UserObject uo = (UserObject) session.getAttribute("uo");
            ChannelData channelData = this.getChannelData(session);
            String userid = "";
            String birthday = "";
            String csType = "";
            if (uo != null) {
                // 先將跨售Common的資料 以及 跨售CS的資料 整併起來
                  this.transCrossData(session);
                // 取得整併好的資料
                CSCommonData commonData = this.getCsCommonData(session);
                if (commonData != null) {
					if(session.getAttribute("FreeOTP")!=null&&session.getAttribute("FreeOTP").equals("Y")) {
                		uo.setPhoneNum(commonData.getMobile());
                    }
                    System.out.println("產品:" + commonData.getProductType());
                    userid = commonData.getIdno();
                    birthday = commonData.getBirthday();
                    csType = commonData.getType();
                       
                }
            } else {
                // return "redirect:/PageNotFound";
                return new RedirectView(cService.getApplyPageUrl(prodType, null, ResponseCode.C999));
            }
            
			logger.info("createUserObject start!");
            createUserObject(session, uo);
			logger.info("saveBrowseRecord start!");
            saveBrowseRecord(uo,  request.getRequestURI(),  request.getMethod() );
			logger.info("ProcessStart start!");
            ProcessStart(uo.getUniqId(), uo.getUniqType());
			logger.info("ProcessPageStart start!");
            ProcessPageStart(uo.getUniqId(), uo.getUniqType(), request.getRequestURI());
            System.out.println("uniqid:" + uo.getUniqId());
            //System.out.println("userid:" + userid);
            //System.out.println("birthday:" + birthday);
            request.setAttribute("userid", userid);
            request.setAttribute("birthday", birthday);
            request.setAttribute("cstype", csType);
            
            // String redirectUrl = session.getAttribute("redirectUrl").toString() + "&CS=1";
            String redirectUrl = session.getAttribute("redirectUrl").toString();
            if (redirectUrl.contains("?")){
                redirectUrl += "&CS=" + uo.getUniqId() + "&channelId=" + channelData.getChannelId();
            } else{
                redirectUrl += "?CS=" + uo.getUniqId() + "&channelId=" + channelData.getChannelId();
            }
			logger.info("redirectUrl = " + redirectUrl);
            // return "/apply/apply_loan_step1";
            return new RedirectView(redirectUrl);
        } catch (IntrusionException e) {
        	logger.info("Exception ESAPI", e);
        	return new RedirectView(cService.getApplyPageUrl(prodType, null, ResponseCode.C999));
        } catch (Exception ex) {
			logger.info("Exception catch : " + ex.getMessage() + ex.getStackTrace().toString());
            ex.printStackTrace();
            return new RedirectView(cService.getApplyPageUrl(prodType, null, ResponseCode.C999));
        }
        //[2019.09.04 GaryLiu]====  END  : 白箱測試 ====
    }
}
