package com.kgi.crosssell.web.ctl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.kgi.airloanex.common.dto.db.ChannelData;
import com.kgi.crosssell.common.constants.ResponseCode;
import com.kgi.crosssell.common.dto.CS_PersonalData;
import com.kgi.crosssell.common.dto.UserObject;
import com.kgi.crosssell.web.service.CrossSellService;
import com.kgi.crosssell.web.service.QRCodeService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/CS")
public class CrossSellCtl extends GeneralCtl {

	private static final Logger logger = LogManager.getLogger(CrossSellCtl.class.getName());
	// private static final Logger logger =
	// LogManager.getLogger(CrossSellCtl.class.getName());
	// +prop.getString("csredirecturl","http://60.251.29.16/airloan")

	@Autowired
	private CrossSellService crossSellService;

	@Autowired
	private QRCodeService qrCodeService;

    @Value("${server.servlet.context-path}")
    String contextPath;

	@RequestMapping(value = "/{prodid}/{channelId}", method = RequestMethod.POST)
	public RedirectView entry(HttpSession session, HttpServletRequest request, @PathVariable("prodid") String prodid,
			@PathVariable("channelId") String channelId, String token, String idno, String ip, String shortUrl,
			String depart, String member, String entry) throws Exception {

		System.out.println("#### 進入證券跨售 function (mathod=POST, channelId=" + channelId + ", prodid=" + prodid
				+ ", token=" + token + ")");
		logger.info("#### 進入證券跨售 function (mathod=POST, channelId=" + channelId + ", prodid=" + prodid + ", token="
				+ token + ")");

		String prodType = "CC";
		
		// [2019.08.13 GaryLiu]==== START : 白箱測試 ====
		try {

			// [2019.08.23 GaryLiu] ==== START : ESAPI驗證 ====
			prodid = ESAPI.validator().getValidInput("", prodid, "AntiXSS", Integer.MAX_VALUE, false);
			// ESAPI.validator().isValidInput("", prodid, "AntiXSS", Integer.MAX_VALUE,
			// false);
			channelId = ESAPI.validator().getValidInput("", channelId, "AntiXSS", Integer.MAX_VALUE, false);
			// [2019.09.04 GaryLiu] ==== START : ESAPI驗證 ====
			// token = ESAPI.validator().getValidInput("", token, "Space",
			// Integer.MAX_VALUE, true);
			// idno = "";//ESAPI.validator().getValidInput("", idno, "AntiXSS",
			// Integer.MAX_VALUE, true);
			// ip = "";//ESAPI.validator().getValidInput("", ip, "IPAddress",
			// Integer.MAX_VALUE, true);
			// shortUrl = "";//ESAPI.validator().getValidInput("", shortUrl, "AntiXSS",
			// Integer.MAX_VALUE, true);
			// depart = "";//ESAPI.validator().getValidInput("", depart, "AntiXSS",
			// Integer.MAX_VALUE, true);
			// member = "";//ESAPI.validator().getValidInput("", member, "AntiXSS",
			// Integer.MAX_VALUE, true);
			// entry = "";//ESAPI.validator().getValidInput("", entry, "AntiXSS",
			// Integer.MAX_VALUE, true);
			// [2019.09.04 GaryLiu] ==== END : ESAPI驗證 ====
			// [2019.08.23 GaryLiu] ==== END : ESAPI驗證 ====

			// 先寫log
			String fucoapilog = crossSellService.insertFucoApiLog(prodid, channelId, token, idno, ip, shortUrl, depart,
					member, entry);
			if (fucoapilog.equals("error")) {
				// return "redirect:/PageNotFound";
				return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
			}
			// 驗證資料正確性
			if (!crossSellService.checkCSParameter(prodid, channelId, token, idno, ip, shortUrl, depart, member,
					entry)) {
				// return "redirect:/PageNotFound";
				return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
			}

			String csdata = crossSellService.getCSData(prodid, channelId, token, idno, ip);
			System.out.println(session.getId());
			JSONObject json = JSONObject.fromObject(csdata);
			
			// 2021/4/7 須檢核channelType為分行1
			String channelType = json.getString("channelType");

			if (!"1".equals(channelType)){
				// return "redirect:/PageNotFound";
				return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
			}

			// channel 才會有uniqId
			if (!json.has("UniqId")) {
				System.out.println("此Channel未設定或Channel未啟用或未定義跨售取得AUM的API");
				// 檢查有沒有網址
				if (json.has("url")) {
					// 有的話就導頁過去
					// return "redirect:" + json.getString("url");
					return new RedirectView(json.getString("url"));
				} else {
					// return "redirect:/PageNotFound";
					return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
				}
			} else {

				session.setAttribute("redirectUrl", json.getString("redirectUrl"));

				ChannelData channelData = qrCodeService.getChannelData(shortUrl);
				if (channelData == null) {
					channelData = new ChannelData();
					channelData.setChannelId(channelId);
					channelData.setChannelType(channelType);
					channelData.setDepartId(depart);
					channelData.setMember(member);
				}

				session.setAttribute("cd", channelData);
				if (channelData.getThemePath() != null && !channelData.getThemePath().equals("")) {
					session.setAttribute("ThemePath", channelData.getThemePath());
				} else {
					session.removeAttribute("ThemePath");
				}
				UserObject uo = new UserObject();
				uo.setUniqId(json.getString("UniqId"));
				System.out.println("uniqid:" + json.getString("UniqId"));
				uo.setUniqType(json.getString("UniqType"));
				System.out.println("UniqType:" + json.getString("UniqType"));
				session.setAttribute("uo", uo);
				System.out.println("CS:將資料寫到session");
				// 做字串轉物件
				System.out.println("PersonalData:" + json.getString("PersonalData"));
				if (json.getString("PersonalData").equals("error")) {
					System.out.println("CS:取得aum資料有問題");
					session.setAttribute("PersonalData", "error");
					System.out.println("CS:導頁到首頁");
					if (!entry.equals("")) {
						// return "redirect:" + json.getString("url").concat("?cs=y").concat("&Entry=" + entry);
						return new RedirectView(json.getString("url").concat("?cs=y").concat("&Entry=" + entry));
					} else {
						// return "redirect:" + json.getString("url").concat("?cs=y");
						return new RedirectView(json.getString("url").concat("?cs=y"));
					}
					// return "redirect:/PageNotFound";
				} else {
					if (json.getString("PersonalData").equals("")) {
						session.setAttribute("PersonalData", "");
					} else {
						JSONObject personJson = JSONObject.fromObject(json.getString("PersonalData"));
						if (prodid.equals("credit")) {
							session.setAttribute("ProductType", "1");
						} else {
							session.setAttribute("ProductType", "0");
						}
						session.setAttribute("PersonalData",
								(CS_PersonalData) JSONObject.toBean(personJson, CS_PersonalData.class));
						uo.setHasCSAUMData(true);
					}

					return applyLoan(request, session, prodType);
				}

			}
		} catch (IntrusionException e) {
			logger.info("Exception ESAPI", e);
			// return "redirect:/PageNotFound";
			return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
		}
		// [2019.08.13 GaryLiu]==== END : 白箱測試 ====
		catch (Exception e) {
			System.out.println(e.getStackTrace());
			// return "redirect:/PageNotFound";
			return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
		}

	}

	@RequestMapping(value = "/{prodid}/{channelId}", method = RequestMethod.GET)
	public RedirectView getTest(HttpSession session, HttpServletRequest request, @PathVariable("prodid") String prodid,
	@PathVariable("channelId") String channelId, @RequestParam Map<String, String> allRequestParams) throws Exception {

		String token = allRequestParams.get("token");
		String idno = allRequestParams.get("idno");
		String ip = allRequestParams.get("ip");
		String shortUrl = allRequestParams.get("shortUrl");
		String depart = allRequestParams.get("depart");
		String member = allRequestParams.get("member");
		String entry = allRequestParams.get("entry");

		System.out.println("#### 進入證券跨售 function (mathod=POST, channelId=" + channelId + ", prodid=" + prodid
				+ ", token=" + token + ")");
		logger.info("#### 進入證券跨售 function (mathod=POST, channelId=" + channelId + ", prodid=" + prodid + ", token="
				+ token + ")");

		String prodType = "CC";

		// [2019.08.13 GaryLiu]==== START : 白箱測試 ====
		try {

			// [2019.08.23 GaryLiu] ==== START : ESAPI驗證 ====
			prodid = ESAPI.validator().getValidInput("", prodid, "AntiXSS", Integer.MAX_VALUE, false);
			// ESAPI.validator().isValidInput("", prodid, "AntiXSS", Integer.MAX_VALUE,
			// false);
			channelId = ESAPI.validator().getValidInput("", channelId, "AntiXSS", Integer.MAX_VALUE, false);
			// [2019.09.04 GaryLiu] ==== START : ESAPI驗證 ====
			// token = ESAPI.validator().getValidInput("", token, "Space",
			// Integer.MAX_VALUE, true);
			// idno = "";//ESAPI.validator().getValidInput("", idno, "AntiXSS",
			// Integer.MAX_VALUE, true);
			// ip = "";//ESAPI.validator().getValidInput("", ip, "IPAddress",
			// Integer.MAX_VALUE, true);
			// shortUrl = "";//ESAPI.validator().getValidInput("", shortUrl, "AntiXSS",
			// Integer.MAX_VALUE, true);
			// depart = "";//ESAPI.validator().getValidInput("", depart, "AntiXSS",
			// Integer.MAX_VALUE, true);
			// member = "";//ESAPI.validator().getValidInput("", member, "AntiXSS",
			// Integer.MAX_VALUE, true);
			// entry = "";//ESAPI.validator().getValidInput("", entry, "AntiXSS",
			// Integer.MAX_VALUE, true);
			// [2019.09.04 GaryLiu] ==== END : ESAPI驗證 ====
			// [2019.08.23 GaryLiu] ==== END : ESAPI驗證 ====

			// 先寫log
			String fucoapilog = crossSellService.insertFucoApiLog(prodid, channelId, token, idno, ip, shortUrl, depart,
					member, entry);
			if (fucoapilog.equals("error")) {
				// return "redirect:/PageNotFound";
				return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
			}
			// 驗證資料正確性
			if (!crossSellService.checkCSParameter(prodid, channelId, token, idno, ip, shortUrl, depart, member,
					entry)) {
				// return "redirect:/PageNotFound";
				return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
			}

			String csdata = crossSellService.getCSData(prodid, channelId, token, idno, ip);
			System.out.println(session.getId());
			JSONObject json = JSONObject.fromObject(csdata);
			
			// 2021/4/7 須檢核channelType為分行1
			String channelType = json.getString("channelType");

			if (!"1".equals(channelType)){
				// return "redirect:/PageNotFound";
				return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
			}

			// channel 才會有uniqId
			if (!json.has("UniqId")) {
				System.out.println("此Channel未設定或Channel未啟用或未定義跨售取得AUM的API");
				// 檢查有沒有網址
				if (json.has("url")) {
					// 有的話就導頁過去
					// return "redirect:" + json.getString("url");
					return new RedirectView(json.getString("url"));
				} else {
					// return "redirect:/PageNotFound";
					return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
				}
			} else {

				session.setAttribute("redirectUrl", json.getString("redirectUrl"));

				ChannelData channelData = qrCodeService.getChannelData(shortUrl);
				if (channelData == null) {
					channelData = new ChannelData();
					channelData.setChannelId(channelId);
					channelData.setChannelType(channelType);
					channelData.setDepartId(depart);
					channelData.setMember(member);
				}

				session.setAttribute("cd", channelData);
				if (channelData.getThemePath() != null && !channelData.getThemePath().equals("")) {
					session.setAttribute("ThemePath", channelData.getThemePath());
				} else {
					session.removeAttribute("ThemePath");
				}
				UserObject uo = new UserObject();
				uo.setUniqId(json.getString("UniqId"));
				System.out.println("uniqid:" + json.getString("UniqId"));
				uo.setUniqType(json.getString("UniqType"));
				System.out.println("UniqType:" + json.getString("UniqType"));
				session.setAttribute("uo", uo);
				System.out.println("CS:將資料寫到session");
				// 做字串轉物件
				System.out.println("PersonalData:" + json.getString("PersonalData"));
				if (json.getString("PersonalData").equals("error")) {
					System.out.println("CS:取得aum資料有問題");
					session.setAttribute("PersonalData", "error");
					System.out.println("CS:導頁到首頁");
					if (!entry.equals("")) {
						// return "redirect:" + json.getString("url").concat("?cs=y").concat("&Entry=" + entry);
						return new RedirectView(json.getString("url").concat("?cs=y").concat("&Entry=" + entry));
					} else {
						// return "redirect:" + json.getString("url").concat("?cs=y");
						return new RedirectView(json.getString("url").concat("?cs=y"));
					}
					// return "redirect:/PageNotFound";
				} else {
					if (json.getString("PersonalData").equals("")) {
						session.setAttribute("PersonalData", "");
					} else {
						JSONObject personJson = JSONObject.fromObject(json.getString("PersonalData"));
						if (prodid.equals("credit")) {
							session.setAttribute("ProductType", "1");
						} else {
							session.setAttribute("ProductType", "0");
						}
						session.setAttribute("PersonalData",
								(CS_PersonalData) JSONObject.toBean(personJson, CS_PersonalData.class));
						uo.setHasCSAUMData(true);
					}

					return applyLoan(request, session, prodType);
				}

			}
		} catch (IntrusionException e) {
			logger.info("Exception ESAPI", e);
			// return "redirect:/PageNotFound";
			return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
		}
		// [2019.08.13 GaryLiu]==== END : 白箱測試 ====
		catch (Exception e) {
			System.out.println(e.getStackTrace());
			// return "redirect:/PageNotFound";
			return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
		}

	}

	@Override
	protected String getProcessName() {
		// TODO Auto-generated method stub
		return null;
	}

}
