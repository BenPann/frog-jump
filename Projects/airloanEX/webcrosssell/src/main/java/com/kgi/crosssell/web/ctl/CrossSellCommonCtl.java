package com.kgi.crosssell.web.ctl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.kgi.airloanex.common.dto.db.ChannelData;
import com.kgi.crosssell.common.constants.ResponseCode;
import com.kgi.crosssell.common.dto.CSCommon;
import com.kgi.crosssell.common.dto.UserObject;
import com.kgi.crosssell.web.service.CrossSellService;
import com.kgi.crosssell.web.service.QRCodeService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.errors.IntrusionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/CSCommon")
public class CrossSellCommonCtl extends GeneralCtl {

	private static final Logger logger = LogManager.getLogger(GeneralCtl.class.getName());

	@Autowired
	private CrossSellService crossSellService;
	@Autowired	
	private QRCodeService qrCodeService;

    @Value("${server.servlet.context-path}")
    String contextPath;
	
	@RequestMapping(value = "/{prodId}/{channelId}/{prodType}/{token}", method = { RequestMethod.POST })
	public RedirectView entry(HttpSession session, @PathVariable("prodId") String prodId,
			@PathVariable("channelId") String channelId, @PathVariable("prodType") String prodType, 
			@PathVariable("token") String token, String shortUrl,
			String depart, String member, String entry, HttpServletRequest request) {

		// System.out.println("prodId=" + prodId + ", channelId=" + channelId + ", token=" + token);
		// String url = "https://mvnrepository.com/artifact/net.sf.uadetector/uadetector-resources/2014.10";
		// return new RedirectView(url); // 重新導向到指定的url
				
			System.out.println("#### 進入異業跨售 function (mathod=" + request.getMethod() + ", channelId=" + channelId + ", prodid=" + prodId + ", token=" + token + ", shortUrl=" + shortUrl + ", depart=" + depart + ", member=" + member + ", entry=" + entry +  ")");	

			logger.info("#### 進入異業跨售 function (mathod=" + request.getMethod() + ", channelId=" + channelId + ", prodid=" + prodId + ", token=" + token + ", shortUrl=" + shortUrl + ", depart=" + depart + ", member=" + member + ", entry=" + entry +  ")");			
				
		//[2019.08.23 GaryLiu]==== START : 白箱測試 ====
		try {
			String ip = this.getIpAddr(request);
			if("RT".equals(channelId)) {
				session.setAttribute("csType",channelId);
			}
			
			// 先寫log
			crossSellService.insertFucoApiLog(prodId, channelId, token, "", ip, shortUrl, depart, member,entry);
			// 驗證資料正確性
			if (!crossSellService.checkCSParameter(prodId, channelId, token, "", ip, shortUrl, depart, member,entry)) {
				System.out.println("token:(" + token + ") 資料欄位驗證不正確 回404");
				// return new RedirectView("redirect:/PageNotFound?msg=onlySupportPostMethod");
				return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
			}
			// 驗證是否需要反查CIF跟MEMO
			String csdata = crossSellService.getCSCommonData(prodId, channelId, prodType, token);
			
			JSONObject json = JSONObject.fromObject(csdata);
			
			System.out.println("csdata=" +  json.toString());
			
			// 2021/4/7 須檢核channelType為其他2
			String channelType = json.getString("channelType");

			if (!"2".equals(channelType)){
				// return new RedirectView("redirect:/PageNotFound");
				return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
			}
			
			// 只有有取資料 才會有uniqId
			if (!json.has("UniqId")) {
				// 當由異業查無資料時，直接走這邊導到登入頁
				// 檢查有沒有網址
				if (json.has("url")) {
					// 有的話就導頁過去
					// return "redirect:" + json.getString("url");
					return new RedirectView(json.getString("url"));
				} else {
					// return new RedirectView("redirect:/PageNotFound");
					return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
				}
			} else {
				// 當由異業查有資料時，走這邊
				UserObject uo = new UserObject();
				uo.setUniqId(json.getString("UniqId"));
				uo.setUniqType(json.getString("UniqType"));

				session.setAttribute("redirectUrl", json.getString("redirectUrl"));

				ChannelData channelData = qrCodeService.getChannelData(shortUrl);	
				
				System.out.println("#### channelData=" +  channelData);
				//System.out.println("ThemePath=" +  channelData.getThemePath());

				
				if (channelData == null) {
					System.out.println("#### channelData is Null...");
					channelData = new ChannelData();
					channelData.setChannelId(channelId);
					channelData.setChannelType(channelType);
					channelData.setDepartId(depart);
					channelData.setMember(member);
				}
				
				
				session.setAttribute("cd", channelData);
				if (channelData.getThemePath() != null && !channelData.getThemePath().equals("")) {
					session.setAttribute("ThemePath", channelData.getThemePath());
				} else {
					session.removeAttribute("ThemePath");
				}

				session.setAttribute("uo", uo);
				// 做字串轉物件
				CSCommon data = (CSCommon) JSONObject.toBean(json.optJSONObject("CSCommon"), CSCommon.class);
				if (data == null) {
					session.setAttribute("PersonalData", "");
				} else {
					// uo.setCrossSellFlag("cs");
					session.setAttribute("PersonalData", data);
				}
				if (data != null && !json.getString("url").equals("")) {
					return applyLoan(request, session, prodType);
				} else {
					// return new RedirectView("redirect:/PageNotFound");
					return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
				}
			}
		} catch (IntrusionException e) {
        	System.out.println("Exception ESAPI error:"+ e);
        	// return new RedirectView("redirect:/InternalError");
			return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
		} catch (Exception e) {
			e.printStackTrace();
			// return new RedirectView("redirect:/InternalError");
			return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
		}
		//[2019.08.23 GaryLiu]====  END : 白箱測試 ====
	}
	
	@RequestMapping(value = "/{prodId}/{channelId}/{prodType}/{token}", method = { RequestMethod.GET })
	public RedirectView getTestRedirectView(HttpSession session, HttpServletRequest request, @PathVariable("prodId") String prodId,
			@PathVariable("channelId") String channelId, @PathVariable("prodType") String prodType, @PathVariable("token") String token, @RequestParam Map<String, String> allRequestParams) {

			String shortUrl = allRequestParams.get("shortUrl");
			String depart = allRequestParams.get("depart");
			String member = allRequestParams.get("member");
			String entry = allRequestParams.get("entry");

		// System.out.println("prodId=" + prodId + ", channelId=" + channelId + ", token=" + token);
		// String url = "https://mvnrepository.com/artifact/net.sf.uadetector/uadetector-resources/2014.10";
		// return new RedirectView(url); // 重新導向到指定的url
				
			System.out.println("#### 進入異業跨售 function (mathod=" + request.getMethod() + ", channelId=" + channelId + ", prodid=" + prodId + ", token=" + token + ", shortUrl=" + shortUrl + ", depart=" + depart + ", member=" + member + ", entry=" + entry +  ")");	

			logger.info("#### 進入異業跨售 function (mathod=" + request.getMethod() + ", channelId=" + channelId + ", prodid=" + prodId + ", token=" + token + ", shortUrl=" + shortUrl + ", depart=" + depart + ", member=" + member + ", entry=" + entry +  ")");			
				
		//[2019.08.23 GaryLiu]==== START : 白箱測試 ====
		try {
			String ip = this.getIpAddr(request);
			if("RT".equals(channelId)) {
				session.setAttribute("csType",channelId);
			}
			
			// 先寫log
			crossSellService.insertFucoApiLog(prodId, channelId, token, "", ip, shortUrl, depart, member,entry);
			// 驗證資料正確性
			if (!crossSellService.checkCSParameter(prodId, channelId, token, "", ip, shortUrl, depart, member,entry)) {
				System.out.println("token:(" + token + ") 資料欄位驗證不正確 回404");
				// return new RedirectView("redirect:/PageNotFound?msg=onlySupportPostMethod");
				return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
			}
			// 驗證是否需要反查CIF跟MEMO
			String csdata = crossSellService.getCSCommonData(prodId, channelId, prodType, token);
			
			JSONObject json = JSONObject.fromObject(csdata);
			
			System.out.println("csdata=" +  json.toString());
			
			// 2021/4/7 須檢核channelType為其他2
			String channelType = json.getString("channelType");

			if (!"2".equals(channelType)){
				// return new RedirectView("redirect:/PageNotFound");
				return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
			}
			
			// 只有有取資料 才會有uniqId
			if (!json.has("UniqId")) {
				// 當由異業查無資料時，直接走這邊導到登入頁
				// 檢查有沒有網址
				if (json.has("url")) {
					// 有的話就導頁過去
					// return "redirect:" + json.getString("url");
					return new RedirectView(json.getString("url"));
				} else {
					// return new RedirectView("redirect:/PageNotFound");
					return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
				}
			} else {
				// 當由異業查有資料時，走這邊
				UserObject uo = new UserObject();
				uo.setUniqId(json.getString("UniqId"));
				uo.setUniqType(json.getString("UniqType"));
				
				session.setAttribute("redirectUrl", json.getString("redirectUrl"));

				ChannelData channelData = qrCodeService.getChannelData(shortUrl);	
				
				System.out.println("#### channelData=" +  channelData);
				//System.out.println("ThemePath=" +  channelData.getThemePath());

				
				if (channelData == null) {
					System.out.println("#### channelData is Null...");
					channelData = new ChannelData();
					channelData.setChannelId(channelId);
					channelData.setChannelType(channelType);
					channelData.setDepartId(depart);
					channelData.setMember(member);
				}
				
				
				session.setAttribute("cd", channelData);
				if (channelData.getThemePath() != null && !channelData.getThemePath().equals("")) {
					session.setAttribute("ThemePath", channelData.getThemePath());
				} else {
					session.removeAttribute("ThemePath");
				}

				session.setAttribute("uo", uo);
				// 做字串轉物件
				CSCommon data = (CSCommon) JSONObject.toBean(json.optJSONObject("CSCommon"), CSCommon.class);
				
				if (data == null) {
					session.setAttribute("PersonalData", "");
				} else {
					// uo.setCrossSellFlag("cs");
					session.setAttribute("PersonalData", data);
				}
				if (data != null && !json.getString("url").equals("")) {
					return applyLoan(request, session, prodType);
				} else {
					// return new RedirectView("redirect:/PageNotFound");
					return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
				}
			}
		} catch (IntrusionException e) {
        	System.out.println("Exception ESAPI error:"+ e);
        	// return new RedirectView("redirect:/InternalError");
			return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
		} catch (Exception e) {
			e.printStackTrace();
			// return new RedirectView("redirect:/InternalError");
			return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
		}
		//[2019.08.23 GaryLiu]====  END : 白箱測試 ====
	}

	@Override
	protected String getProcessName() {
		return null;
	}

}
