//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.kgi.crosssell.web.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;

public class HttpUtil3 {
    static final int MAX_SIZE = 2147483647;
    private static int KEY = 0;
    private static int VALUE = 1;
    public static int ConnectTimeout = 30000;
    public static int ReadTimeout = 30000;
    public static final HttpUtil3.Prop TEXT = (new HttpUtil3.Prop()).setContentType("text/plain", "utf-8");
    public static final HttpUtil3.Prop FORM = (new HttpUtil3.Prop()).setContentType("application/x-www-form-urlencoded", "utf-8");
    public static final HttpUtil3.Prop JSON = (new HttpUtil3.Prop()).setContentType("application/json", "utf-8").setAccept("application/json");

    static {
        try {
            ignoreVerifyHttpsHostName();
            ignoreVerifyHttpsTrustManager();
        } catch (Exception var1) {
            throw new RuntimeException(var1);
        }
    }

    public HttpUtil3() {
    }

    public static String getPostData(HttpServletRequest request) throws IOException {
        return getPostData(request, "text/plain; charset=utf-8");
    }

    public static String getPostData(HttpServletRequest request, String def_charset) throws IOException {
        String ct = request.getHeader("Content-type");
        if (ct == null) {
            ct = "";
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        InputStream is = request.getInputStream();
        byte[] buf = new byte[4096];
        boolean var6 = false;

        int r;
        while((r = is.read(buf)) > 0) {
            bos.write(buf, 0, r);
        }

        is.close();
        buf = bos.toByteArray();
        String encode = "";
        int idx = ct.indexOf("charset=");
        if (idx > 0) {
            encode = ct.substring(idx + 8);
        } else {
            encode = def_charset;
        }

        String ret;
        if (encode.length() > 0) {
            ret = new String(buf, encode);
        } else {
            ret = new String(buf);
        }

        return ct.startsWith("application/x-www-form-urlencoded") ? URLDecoder.decode(ret, encode) : ret;
    }

    public static String bypassRequest4TextAPI(String wsserv, String queryStr, String postData) throws IOException {
        String url = wsserv;
        if (queryStr != null) {
            url = wsserv.concat("?").concat(queryStr);
        }

        return postData != null && postData.length() != 0 ? doJsonPostRequest(wsserv, postData) : doGetRequest(url);
    }

    public static String buildQueryString(String[][] params, String enc) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        int p = 0;

        for(int i = 0; i < params.length; ++i) {
            if (params[i] != null && params[i].length == 2) {
                if (p > 0) {
                    sb.append("&");
                }

                if (enc != null && enc.length() > 0) {
                    sb.append(URLEncoder.encode(params[i][KEY], enc));
                    sb.append("=");
                    sb.append(URLEncoder.encode(params[i][VALUE], enc));
                } else {
                    sb.append(params[i][KEY]);
                    sb.append("=");
                    sb.append(params[i][VALUE]);
                }

                ++p;
            }
        }

        return sb.toString();
    }

    public static String doGetRequest(String urlStr) throws IOException {
        return doGetRequest(urlStr, (HttpUtil3.Param)null, (HttpUtil3.Prop)FORM);
    }

    public static String doGetRequest(String urlStr, HttpUtil3.Param params) throws IOException {
        return doGetRequest(urlStr, params, FORM);
    }

    public static String doGetRequest(String urlStr, HttpUtil3.Param params, HttpUtil3.Prop prop) throws IOException {
        return doGetRequest(urlStr, HttpUtil3.Param.toArray(params), HttpUtil3.Prop.toArray(prop));
    }

    private static String doGetRequest(String urlStr, String[][] params, String[][] prop) throws IOException {
        int connectTimeout = ConnectTimeout;
        int readTimeout = ReadTimeout;
        String queryString = "";
        String param_encode = "utf-8";
        int i;
        if (params != null && params.length > 0) {
            for(i = 0; i < params.length; ++i) {
                if (params[i] != null && params[i].length == 2 && "@encode".equalsIgnoreCase(params[i][KEY])) {
                    param_encode = params[i][VALUE];
                    params[i] = null;
                }
            }

            queryString = buildQueryString(params, param_encode);
        }

        if (prop != null) {
            for(i = 0; i < prop.length; ++i) {
                if (prop[i] != null && prop[i].length == 2) {
                    if ("@ConnectTimeout".equalsIgnoreCase(prop[i][KEY])) {
                        connectTimeout = Integer.parseInt(prop[i][VALUE]);
                        prop[i] = null;
                    } else if ("@ReadTimeout".equalsIgnoreCase(prop[i][KEY])) {
                        readTimeout = Integer.parseInt(prop[i][VALUE]);
                        prop[i] = null;
                    }
                }
            }
        }

        if (queryString.length() > 0) {
            if (urlStr.indexOf("?") > 0) {
                urlStr = urlStr.concat("&").concat(queryString);
            } else {
                urlStr = urlStr.concat("?").concat(queryString);
            }
        }

        StringBuilder response = new StringBuilder();
        URL url = null;
        HttpURLConnection conn = null;
        InputStream is = null;
        InputStreamReader isr = null;
        char[] buff = new char[4096];
        int size = 0;
        boolean var14 = false;

        try {
            url = new URL(urlStr);
            conn = (HttpURLConnection)url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(connectTimeout);
                conn.setReadTimeout(readTimeout);
                String encode = "";
                if (prop != null) {
                    for(int j = 0; j < prop.length; ++j) {
                        if (prop[j] != null && prop[j].length == 2) {
                            conn.setRequestProperty(prop[j][KEY], prop[j][VALUE]);
                            if ("Accept-Charset".equalsIgnoreCase(prop[j][KEY])) {
                                encode = prop[j][VALUE];
                            }
                        }
                    }
                }

                conn.setDoInput(true);
                is = conn.getInputStream();
                if (encode != null && encode.length() > 0) {
                    isr = new InputStreamReader(is, encode);
                } else {
                    isr = new InputStreamReader(is);
                }

                while(true) {
                    int r;
                    if ((r = isr.read(buff)) > 0) {
                        response.append(buff, 0, r);
                        size += r;
                        if (size < 2147483647) {
                            continue;
                        }
                    }

                    String var18 = response.toString();
                    return var18;
                }
            }
        } finally {
            safeClose(is, (OutputStream)null);
        }

        return "";
    }

    public static String doFormPostRequest(String urlStr, HttpUtil3.Param params) throws IOException {
        return doFormPostRequest(urlStr, params, FORM);
    }

    public static String doFormPostRequest(String urlStr, HttpUtil3.Param params, HttpUtil3.Prop prop) throws IOException {
        String queryString = buildQueryString(HttpUtil3.Param.toArray(params), prop.getContentCharset());
        return doPostRequest(urlStr, queryString, HttpUtil3.Prop.toArray(prop));
    }

    public static String doJsonPostRequest(String urlStr, Object jsonData) throws IOException {
        return doPostRequest(urlStr, jsonData.toString(), HttpUtil3.Prop.toArray(JSON));
    }

    public static String doJsonPostRequest(String urlStr, Object jsonData, HttpUtil3.Prop prop) throws IOException {
        return doPostRequest(urlStr, jsonData.toString(), HttpUtil3.Prop.toArray(prop));
    }

    public static String doPostRequest(String urlStr) throws IOException {
        return doPostRequest(urlStr, (String)null, (String[][])HttpUtil3.Prop.toArray(TEXT));
    }

    public static String doPostRequest(String urlStr, String postData, HttpUtil3.Prop prop) throws IOException {
        return doPostRequest(urlStr, postData, HttpUtil3.Prop.toArray(prop));
    }

    private static String doPostRequest(String urlStr, String postData, String[][] prop) throws IOException {
        int connectTimeout = ConnectTimeout;
        int readTimeout = ReadTimeout;
        String encode = "";
        String urlencode = "utf-8";
        if (prop != null) {
            for(int i = 0; i < prop.length; ++i) {
                if (prop[i] != null && prop[i].length == 2) {
                    if ("Accept-Charset".equalsIgnoreCase(prop[i][KEY])) {
                        encode = prop[i][VALUE];
                    }

                    if ("Content-type".equalsIgnoreCase(prop[i][KEY])) {
                        int idx = prop[i][VALUE].indexOf("charset=");
                        if (idx > 0) {
                            urlencode = prop[i][VALUE].substring(idx + 8);
                        }
                    }

                    if ("@ConnectTimeout".equalsIgnoreCase(prop[i][KEY])) {
                        connectTimeout = Integer.parseInt(prop[i][VALUE]);
                        prop[i] = null;
                    } else if ("@ReadTimeout".equalsIgnoreCase(prop[i][KEY])) {
                        readTimeout = Integer.parseInt(prop[i][VALUE]);
                        prop[i] = null;
                    }
                }
            }
        }

        StringBuffer response = new StringBuffer();
        URL url = null;
        HttpURLConnection conn = null;
        InputStream is = null;
        OutputStream os = null;
        InputStreamReader isr = null;
        char[] buff = new char[4096];
        int size = 0;
        boolean var15 = false;

        try {
            url = new URL(urlStr);
            conn = (HttpURLConnection)url.openConnection();
            if (conn == null) {
                return "";
            } else {
                System.out.println("connectTimeout=" + connectTimeout);
                System.out.println("readTimeout=" + readTimeout);
                conn.setConnectTimeout(connectTimeout);
                conn.setReadTimeout(readTimeout);
                if (prop != null) {
                    for(int i = 0; i < prop.length; ++i) {
                        if (prop[i] != null && prop[i].length == 2) {
                            conn.setRequestProperty(prop[i][KEY], prop[i][VALUE]);
                        }
                    }
                }

                conn.setDoOutput(true);
                conn.setDoInput(true);
                if (postData != null && postData.length() > 0) {
                    os = conn.getOutputStream();
                    os.write(postData.getBytes(urlencode));
                    os.flush();
                }

                is = conn.getInputStream();
                if (encode != null && encode.length() > 0) {
                    isr = new InputStreamReader(is, encode);
                } else {
                    isr = new InputStreamReader(is);
                }

                while(true) {
                    int r;
                    if ((r = isr.read(buff)) > 0) {
                        response.append(buff, 0, r);
                        size += r;
                        if (size < 2147483647) {
                            continue;
                        }
                    }

                    String var18 = response.toString();
                    return var18;
                }
            }
        } finally {
            safeClose(is, os);
        }
    }

    private static void safeClose(InputStream is, OutputStream os) {
        if (is != null) {
            try {
                is.close();
            } catch (IOException var4) {
                new RuntimeException(var4);
            }
        }

        if (os != null) {
            try {
                os.close();
            } catch (IOException var3) {
                new RuntimeException(var3);
            }
        }

    }

    public static void ignoreVerifyHttpsTrustManager() throws Exception {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init((KeyManager[])null, trustAllCerts, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    }

    public static void ignoreVerifyHttpsHostName() {
        HostnameVerifier hv = new HostnameVerifier() {
            public boolean verify(String urlHostName, SSLSession session) {
                return true;
            }
        };
        HttpsURLConnection.setDefaultHostnameVerifier(hv);
    }

    public static String doSoapPostRequest(String urlStr, String postData) throws IOException {
        return doSoapPostRequest(urlStr, postData, "utf-8", "utf-8");
    }

    public static String doSoapPostRequest(String urlStr, String postData, String acceptEnc, String contentEnc) throws IOException {
        String[][] prop = new String[][]{{"Content-Length", "" + postData.getBytes().length}, {"Accept-Charset", acceptEnc}, {"Content-type", "text/xml;charset=".concat(contentEnc)}, {"Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7"}};
        return doPostRequest(urlStr, postData, prop);
    }

    public static String doSoapSSLPostRequest(String urlStr, String postData) throws IOException {
        return doSoapSSLPostRequest(urlStr, postData, "utf-8");
    }

    public static String doSoapSSLPostRequest(String urlStr, String postData, String encode) throws IOException {
        StringBuffer response = new StringBuffer();
        URL url = null;
        HttpsURLConnection conn = null;
        InputStream is = null;
        OutputStream os = null;
        InputStreamReader isr = null;
        char[] buff = new char[4096];
        int size = 0;
        boolean var11 = false;

        try {
            url = new URL(urlStr);
            conn = (HttpsURLConnection)url.openConnection();
            if (conn == null) {
                return "";
            } else {
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(10000);
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
                conn.setRequestProperty("SOAPAction", urlStr);
                conn.setRequestProperty("Content-Length", "" + postData.getBytes().length);
                os = conn.getOutputStream();
                OutputStreamWriter wr = new OutputStreamWriter(os);
                wr.write(postData);
                wr.flush();
                is = conn.getInputStream();
                isr = new InputStreamReader(is, encode);

                while(true) {
                    int r;
                    if ((r = isr.read(buff)) > 0) {
                        response.append(buff, 0, r);
                        size += r;
                        if (size < 2147483647) {
                            continue;
                        }
                    }

                    String var14 = response.toString();
                    return var14;
                }
            }
        } finally {
            safeClose(is, os);
        }
    }

    public static class Param {
        private Map<String, String> kv = new HashMap();

        public Param() {
        }

        public HttpUtil3.Param add(String k, String v) {
            this.kv.put(k, v);
            return this;
        }

        public static String[][] toArray(HttpUtil3.Param p) {
            if (p != null && p.kv.size() != 0) {
                String[][] ret = new String[p.kv.size()][];
                Iterator<Entry<String, String>> it = p.kv.entrySet().iterator();

                for(int i = 0; it.hasNext(); ++i) {
                    Entry<String, String> entry = (Entry)it.next();
                    ret[i] = new String[]{(String)entry.getKey(), (String)entry.getValue()};
                }

                return ret;
            } else {
                return new String[0][];
            }
        }
    }

    public static class Prop {
        private String contentCharset = "utf-8";
        private String contentType = "text/plain";
        private Map<String, String> kv = new HashMap();

        public Prop() {
            this.setAccept("text/plain");
            this.setAcceptCharset("utf-8");
            this.setAcceptLang("zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7");
            this.setContentType("text/plain", "utf-8");
        }

        public HttpUtil3.Prop setAccept(String type) {
            this.kv.put("Accept", type);
            return this;
        }

        public HttpUtil3.Prop setAcceptCharset(String charset) {
            this.kv.put("Accept-Charset", charset);
            return this;
        }

        public HttpUtil3.Prop setAcceptLang(String lang) {
            this.kv.put("Accept-Language", lang);
            return this;
        }

        public HttpUtil3.Prop setContentType(String type, String charset) {
            this.contentCharset = charset;
            this.contentType = type;
            this.kv.put("Content-type", type.concat("; charset=").concat(charset));
            return this;
        }

        public HttpUtil3.Prop setContentType(String type) {
            this.contentType = type;
            this.kv.put("Content-type", type.concat("; charset=").concat(this.contentCharset));
            return this;
        }

        public HttpUtil3.Prop setContentCharset(String charset) {
            this.contentCharset = charset;
            this.kv.put("Content-type", this.contentType.concat("; charset=").concat(charset));
            return this;
        }

        public HttpUtil3.Prop setConnectTimeout(String millis) {
            this.kv.put("@ConnectTimeout", millis);
            return this;
        }

        public HttpUtil3.Prop setReadTimeout(String millis) {
            this.kv.put("@ReadTimeout", millis);
            return this;
        }

        public String getContentCharset() {
            return this.contentCharset;
        }

        public HttpUtil3.Prop setProp(String key, String value) {
            this.kv.put(key, value);
            return this;
        }

        public static String[][] toArray(HttpUtil3.Prop prop) {
            if (prop != null && prop.kv.size() != 0) {
                String[][] ret = new String[prop.kv.size()][];
                Iterator<Entry<String, String>> it = prop.kv.entrySet().iterator();

                for(int i = 0; it.hasNext(); ++i) {
                    Entry<String, String> entry = (Entry)it.next();
                    ret[i] = new String[]{(String)entry.getKey(), (String)entry.getValue()};
                }

                return ret;
            } else {
                return new String[0][];
            }
        }

        public HttpUtil3.Prop clone() {
            HttpUtil3.Prop ret = new HttpUtil3.Prop();
            ret.kv.putAll(this.kv);
            ret.contentCharset = this.contentCharset;
            return ret;
        }
    }
}
