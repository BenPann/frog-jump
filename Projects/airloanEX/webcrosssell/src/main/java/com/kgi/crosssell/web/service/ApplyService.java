package com.kgi.crosssell.web.service;

import java.io.IOException;

import com.kgi.crosssell.common.dto.UserObject;
import com.kgi.crosssell.web.dao.SNDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplyService {

	@Autowired
	private SNDao sndao;

	public UserObject getCaseUserObject() throws IOException {
		return sndao.getCaseUserObject();
	}
}
