package com.kgi.crosssell.web.dao;

import java.io.IOException;
import java.nio.charset.Charset;

import com.kgi.crosssell.common.dto.UserObject;
import com.kgi.crosssell.web.config.GlobalConfig;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.sf.json.JSONObject;

@Repository
public class SNDao {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

	private String functionPath = "sn";

    @Autowired
    GlobalConfig globalConfig;

	public UserObject getExpUserObject() throws IOException {
		return getUniqNo("01");
	}

	public UserObject getCaseUserObject() throws IOException {

		return getUniqNo("02");
	}

	public UserObject getConUserObject() throws IOException {
		return getUniqNo("03");
	}

	public UserObject getContactUserObject() throws IOException {
		return getUniqNo("04");
	}

	public UserObject getCreditUserObject() throws IOException {
		return getUniqNo("05");
	}

	public UserObject getUniqNo(String uniqType) throws IOException {
		String apiurl = globalConfig.APServerHost.concat(functionPath).concat("/createUniqId");
		String json = this.getAPResponse(apiurl, new String[][] { { "UniqType", uniqType } });
		JSONObject data = JSONObject.fromObject(json);
		UserObject uo = new UserObject();
		uo.setUniqId(data.getString("UniqId"));
		uo.setUniqType(uniqType);
		return uo;
	}

    public String getAPResponse(String url, String[][] params) {
        String result = "";

		if (params != null){
			Boolean tag = Boolean.FALSE;
			for(String[] rowArray : params){
				if(rowArray.length == 2){
					if(!tag){
						url.concat("?");
					}
					url.concat(rowArray[0]).concat("=").concat(rowArray[1]).concat("&");
				}
			}
			if(tag){
				url = url.substring(1);
			}
		}

        int timeOut = 30000;
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        // 對連線池設定SocketConfig物件
        connManager.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(timeOut).build());

        try (CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build()) {

            // logger.info(url);
            HttpGet get = new HttpGet(url);
			get.setHeader("Accept", "application/json,application/text,text/plain");
            get.setHeader("Accept-Charset", "utf-8");
            get.setHeader("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7");
            get.setHeader("@ConnectTimeout", "90000");
            get.setHeader("@ReadTimeout", "90000");

            HttpResponse response = client.execute(get);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // 必須用UTF-8 不然會有亂碼
                result = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
            } else if (statusCode == 404) {
                // 要回傳找不到給打API的人知道
                result = "404";
            } else {
                logger.info("回應代碼:" + statusCode);
                result = String.valueOf(statusCode);
            }
        } catch (Exception ex) {
            logger.warn("getAPResponse", ex);
        }

        return result;
    }
}
