package com.kgi.crosssell.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class ClickjackingPreventionFilter implements Filter {
	private String mode = "DENY";

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// HttpServletResponse res = (HttpServletResponse) response;
		// res.addHeader("X-FRAME-OPTIONS", mode);
		// //[2019.10.08 GaryLiu] ==== START : 白箱 : Frameable Login Page ====
		// chain.doFilter(request, res);
		// //[2019.10.08 GaryLiu] ====  END  : 白箱 : Frameable Login Page  ====
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String configMode = filterConfig.getInitParameter("mode");
		if (configMode != null) {
			mode = configMode;
		}
	}
}
