package com.kgi.crosssell.web.ctl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.kgi.airloanex.common.dto.db.ChannelData;
import com.kgi.crosssell.common.constants.ResponseCode;
import com.kgi.crosssell.common.dto.CSCommonData;
import com.kgi.crosssell.common.dto.UserObject;
import com.kgi.crosssell.web.service.ApplyService;
import com.kgi.crosssell.web.service.CrossSellService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/apply")
public class ApplyLoanCtl extends GeneralCtl {

    private static final Logger logger = LogManager.getLogger(ApplyLoanCtl.class.getName());

    @Autowired
    private ApplyService applyService;
    
	@Autowired
	private CrossSellService crossSellService;
    
    // 進入申辦頁
    @RequestMapping(value = "/init", method = RequestMethod.GET)
    public RedirectView apply_loan1(HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session,
                              @RequestParam Map<String, String> allRequestParams) {
    	//[2019.09.04 GaryLiu]==== START : 白箱測試 ====
        String prodType = allRequestParams.get("prodType");
        try {
            System.out.println(session.getId());
            UserObject uo = null;
            ChannelData channelData = this.getChannelData(session);
            String userid = "";
            String birthday = "";
            String csType = "";
            if (ESAPI.validator().getValidInput("", request.getParameter("cs"), "AntiXSS", Integer.MAX_VALUE, true) != null) {
                uo = (UserObject) session.getAttribute("uo");
                if (uo != null) {
                    // 先將跨售Common的資料 以及 跨售CS的資料 整併起來
                    this.transCrossData(session);
                    // 取得整併好的資料
                    CSCommonData commonData = this.getCsCommonData(session);
                    if (commonData != null) {
						if(session.getAttribute("FreeOTP")!=null&&session.getAttribute("FreeOTP").equals("Y")) {
                    		uo.setPhoneNum(commonData.getMobile());
                    	}
                        System.out.println("產品:" + commonData.getProductType());
                        userid = commonData.getIdno();
                        birthday = commonData.getBirthday();
                        csType = commonData.getType();
                       
                    }
                } else {
                    // return "redirect:/PageNotFound";
                    return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
                }
            //[2020.01.21 GaryLiu] ==== START : 新增處理大首頁免登的Data資料 ====
            } else if(session.getAttribute("ed3FreeLoginData") != null) {
                session.removeAttribute("CSCommonData");
                uo = applyService.getCaseUserObject();
                
                //將免登資料塞進TempData
                JSONObject ed3_FreeLoginData_json = (JSONObject) session.getAttribute("ed3FreeLoginData");
                
                JSONObject tempData = new JSONObject();
                
                String ed3_idno = ed3_FreeLoginData_json.getString("Idno").trim();
                String ed3_birthday = ed3_FreeLoginData_json.getString("Birthday").trim();
                String ed3_uniqType = ed3_FreeLoginData_json.getString("UniqType").trim();
                uo.setUniqType(ed3_uniqType);                
//                tempData.put("idno", ed3_FreeLoginData_json.getString("Idno").trim());
//                tempData.put("birthday", ed3_FreeLoginData_json.getString("Birthday").trim());
//                tempData.put("uniqType", ed3_FreeLoginData_json.getString("UniqType").trim());
                tempData.put("idno", ed3_idno);
                tempData.put("birthday", ed3_birthday);
                tempData.put("uniqType", ed3_uniqType);
                
                session.setAttribute("TempData", tempData.toString());
                
                //[2020.01.31 GaryLiu] ==== START : 設定一次性資料 ====
                request.setAttribute("ed3_idno", ed3_idno);
                request.setAttribute("ed3_birthday", ed3_birthday);
                //[2020.01.31 GaryLiu] ====  END  : 設定一次性資料 ====
                
                //設定一個一次性標記，用來判斷是否是免登
                request.setAttribute("ED3LoginFree", "true");
            	
            	//使用完直接移除，避免殘留資料
            	//session.removeAttribute("ed3FreeLoginData");
            //[2020.01.21 GaryLiu] ====  END  : 新增處理大首頁免登的Data資料 ====
/*            } else if (request.getParameter("freeLoginFromEOpen") != null) {
            	System.out.println("freeLoginFromEOpen is not null");

                uo = (UserObject) session.getAttribute("uo");
                userid = uo.getIdno() ;
                birthday = uo.getBirthday();
        		uo.setPhoneNum(request.getParameter("mobile"));
        		session.setAttribute("FreeOTP", "Y") ;
        		csType = "3" ;
*/
            	
	        	if(channelData !=null && channelData.getEntry() !=null) {
	        		allRequestParams.put("Entry", channelData.getEntry());
	        	}
            } else {
            	session.removeAttribute("CSCommonData");
				uo = applyService.getCaseUserObject();
            }
            createUserObject(session, uo);
            saveBrowseRecord(uo,  request.getRequestURI(),  request.getMethod() );
            ProcessStart(uo.getUniqId(), uo.getUniqType());
            ProcessPageStart(uo.getUniqId(), uo.getUniqType(), request.getRequestURI());
            saveEntryData("Apply", uo, request, allRequestParams);
            System.out.println("uniqid:" + uo.getUniqId());
            //System.out.println("userid:" + userid);
            //System.out.println("birthday:" + birthday);
            request.setAttribute("userid", userid);
            request.setAttribute("birthday", birthday);
            request.setAttribute("cstype", csType);
            
            // String redirectUrl = session.getAttribute("redirectUrl").toString() + "&CS=1";
            String redirectUrl = session.getAttribute("redirectUrl").toString();
            if (redirectUrl.contains("?")){
                redirectUrl += "&CS=" + uo.getUniqId() + "&channelId=" + channelData.getChannelId();
            } else{
                redirectUrl += "?CS=" + uo.getUniqId() + "&channelId=" + channelData.getChannelId();
            }
            // return "/apply/apply_loan_step1";
            return new RedirectView(redirectUrl);
        } catch (IntrusionException | ValidationException e) {
        	logger.info("Exception ESAPI", e);
        	return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
        } catch (Exception ex) {
            ex.printStackTrace();
            return new RedirectView(crossSellService.getApplyPageUrl(prodType, null, ResponseCode.C999));
        }
        //[2019.09.04 GaryLiu]====  END  : 白箱測試 ====
    }

	@Override
	protected String getProcessName() {
        return "applyuo";
	}
}
