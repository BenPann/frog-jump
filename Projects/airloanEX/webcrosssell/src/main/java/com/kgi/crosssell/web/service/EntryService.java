package com.kgi.crosssell.web.service;

import java.io.IOException;

import com.kgi.crosssell.web.dao.EntryDataDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.json.JSONObject;

@Service
public class EntryService {
	@Autowired
	EntryDataDao entryDataDao;
	
	public void saveEntryData(JSONObject json) throws IOException {
		
		entryDataDao.saveEntryData(json);
	}

}
