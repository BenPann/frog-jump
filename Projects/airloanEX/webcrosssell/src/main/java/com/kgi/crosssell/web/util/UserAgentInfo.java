package com.kgi.crosssell.web.util;

import javax.servlet.http.HttpServletRequest;

import eu.bitwalker.useragentutils.UserAgent;
import net.sf.json.JSONObject;

public class UserAgentInfo {
	public static String getUserAgent(HttpServletRequest request) throws Exception {
		JSONObject r = new JSONObject();
		// [2019.09.11 GaryLiu] ==== START : 白箱測試 ====
		UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
		System.out.println("#### User-Agent : [" + request.getHeader("User-Agent") + "]");
		// [2019.09.11 GaryLiu] ==== END : 白箱測試 ====
		String browser = userAgent.getBrowser().getName();
		String platform = userAgent.getOperatingSystem().getDeviceType().name();
		String os = userAgent.getOperatingSystem().getName();
		// System.out.println("brower : " + browsername);
		// System.out.println("os device : " + os);
		// System.out.println("os : " + osname);
		// [2019.09.04 GaryLiu]==== START : 白箱測試 ====
		r.put("Action", request.getRequestURI());
		// [2019.09.04 GaryLiu]==== END : 白箱測試 ====
		r.put("Browser", browser);
		r.put("Platform", platform);
		r.put("OS", os);
		r.put("UserAgentOrignal", request.getHeader("User-Agent"));
		return r.toString();
	}
}
