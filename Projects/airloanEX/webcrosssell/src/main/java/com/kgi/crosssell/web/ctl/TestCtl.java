package com.kgi.crosssell.web.ctl;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.kgi.crosssell.web.dao.HttpDao;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import net.sf.json.JSONObject;

@RestController
public class TestCtl {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private HttpDao httpDao;

    @GetMapping("/test/cs")
    public RedirectView postCommonEntry(
          HttpServletRequest request) {
        
        String url = "http://10.86.19.39:8080/airloanEX/CS/loan/KS/PL";
        
		JSONObject  paramJson = new JSONObject();
		paramJson.put("token", "abcdef");
		paramJson.put("channelId", 100);
		paramJson.put("idno", "A123456789");
		paramJson.put("ip", "127.0.0.1");
		paramJson.put("shortUrl", "05rp0cfk");
		paramJson.put("depart", "testDepart");
		paramJson.put("member", "testMember");
		paramJson.put("entry", "testEntry");

        List<NameValuePair> params = new ArrayList<NameValuePair>(2);
        params.add(new BasicNameValuePair("token", "abcdef"));
        params.add(new BasicNameValuePair("channelId", "100"));
        params.add(new BasicNameValuePair("idno", "A123456789"));
        params.add(new BasicNameValuePair("ip", "127.0.0.1"));
        params.add(new BasicNameValuePair("shortUrl", "05rp0cfk"));
        params.add(new BasicNameValuePair("depart", "testDepart"));
        params.add(new BasicNameValuePair("member", "testMember"));
        params.add(new BasicNameValuePair("entry", "testEntry"));

        this.getTestResponse(url, params);

        return new RedirectView("http://172.31.1.117:8087/airloan/applycc/init");
    }

    private String getTestResponse(String url, List<NameValuePair> params) {
        String result = "";

        int timeOut = 30000;
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        // 對連線池設定SocketConfig物件
        connManager.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(timeOut).build());

        try {
            CloseableHttpClient client = HttpClientBuilder.create()
                .setRedirectStrategy(new LaxRedirectStrategy()).build();

            // logger.info(url);
            HttpPost post = new HttpPost(url);

            post.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            post.setHeader("Accept", "*/*");
            post.setHeader("Content-type", "application/x-www-form-urlencoded");

            HttpResponse response = client.execute(post);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // 必須用UTF-8 不然會有亂碼
                result = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
            } else if (statusCode == 404) {
                // 要回傳找不到給打API的人知道
                result = "404";
            } else {
                logger.info("回應代碼:" + statusCode);
                result = String.valueOf(statusCode);
            }
        } catch (Exception ex) {
            logger.warn("getAPResponse", ex);
        }

        return result;
    }
    
}
