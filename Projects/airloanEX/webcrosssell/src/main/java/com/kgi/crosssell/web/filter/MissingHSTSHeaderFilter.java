package com.kgi.crosssell.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class MissingHSTSHeaderFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// //[2019.10.15 GaryLiu] ==== START : 白箱 : 修正Missing HSTS Header ====
		// HttpServletResponse res = (HttpServletResponse) response;
		// res.setHeader("Strict-Transport-Security", "max-age=31536000");
		// chain.doFilter(request, res);
		// //[2019.10.15 GaryLiu] ==== END : 白箱 : 修正Missing HSTS Header ====
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}
}
