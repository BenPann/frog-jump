
package com.kgi.crosssell.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyAppWebMvcConfigurer implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // registry.addResourceHandler("/**/*").addResourceLocations("classpath:/static/").resourceChain(true)
        // .addResolver(new PathResourceResolver() {
        // @Override
        // protected Resource getResource(String resourcePath, Resource location) throws
        // IOException {
        // Resource requestedResource = location.createRelative(resourcePath);
        // // 導頁到Angular的首頁
        // return requestedResource.exists() && requestedResource.isReadable() ?
        // requestedResource
        // : new ClassPathResource("/static/front/index.html");
        // }
        // });
    }
}