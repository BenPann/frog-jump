package com.kgi.crosssell.web.service;

import java.io.IOException;

import com.kgi.airloanex.common.dto.db.ChannelData;
import com.kgi.crosssell.web.dao.HttpDao;
import com.kgi.crosssell.web.dao.QRCodeDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.json.JSONObject;

@Service
public class QRCodeService {

	@Autowired
	private HttpDao httpDao;

    @Autowired
    private QRCodeDao qrCodeDao;

	private String FUNCTION_PATH = "qr";

	public ChannelData getChannelData(String shortUrl) throws IOException {

		String url = FUNCTION_PATH.concat("/getNewChannelData").concat("?shortUrl=")
				.concat(shortUrl);
		String channelData = httpDao.getAPResponseByGetMethod(url);
		JSONObject json = JSONObject.fromObject(channelData);
		if (json.isEmpty()) {
			return null;
		} else {
			return (ChannelData) JSONObject.toBean(json, ChannelData.class);
		}
	}

    public void createUrlCaseMatch(String shortUrl, String uniqId, String uniqType) throws IOException {
        if (shortUrl == null || shortUrl.equals(""))
            return;
        if (uniqId == null || uniqId.equals(""))
            return;
        if (uniqType == null || uniqType.equals(""))
            return;
        qrCodeDao.createUrlCaseMatch(shortUrl,uniqId,uniqType);
    }
}
