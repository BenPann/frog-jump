package com.kgi.eopend3.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

// @SpringBootApplication
@Configuration
@EnableAutoConfiguration
@ComponentScan({"com.kgi.eopend3.web","com.kgi.airloanex.web"})
public class StartWebApplication extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(StartWebApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(StartWebApplication.class, args);		
	}
}
