package com.kgi.eopend3.web.jwt;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.kgi.eopend3.common.dto.WebResult;

import com.kgi.eopend3.common.util.CheckUtil;
import com.kgi.eopend3.web.dao.HttpDao;

import com.kgi.airloanex.common.dto.view.LoginView;

import com.kgi.airloanex.common.dto.response.CaseResp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private HttpDao httpDao;

    
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public CustomAuthenticationProvider(ApplicationContext appContext) {
        appContext.getAutowireCapableBeanFactory().autowireBean(this);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // 取得驗證的帳號密碼 
        LoginView ac = (LoginView) authentication.getPrincipal();
        // 驗證邏輯
        Gson gson = new Gson();
        String reqString = gson.toJson(ac);
        if (!CheckUtil.check(ac)) {
            throw new BadCredentialsException("驗證失敗");
        } else {
            String resp = httpDao.getAPResponseNoAuth("stp", "initPlContract", reqString);
            logger.info("ap resp:" + resp);
            WebResult result = gson.fromJson(resp, WebResult.class);
            if (result.getStatus() == 0 && !result.getResult().equals("")) {
                String principal = gson.toJson(result.getResult());
                CaseResp caseResp = gson.fromJson(principal, CaseResp.class);
                if (!caseResp.getLoginResp().getUniqId().equals("")) {
                    // 依據回傳的結果設定權限(如果有)
                    ArrayList<GrantedAuthority> authorities = new ArrayList<>();
                    // authorities.add(new GrantedAuthorityImpl("ROLE_ADMIN"));
                    // authorities.add(new GrantedAuthorityImpl("AUTH_WRITE"));
                    // 生成令牌
                    Authentication auth = new UsernamePasswordAuthenticationToken(principal, null,
                            authorities);
                    return auth;
                } else {
                    throw new BadCredentialsException(result.getMessage());
                }
            } else {
                throw new BadCredentialsException(result.getMessage());
            }

        }

    }

    // 是否可以提供输入類型的驗證服務
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}