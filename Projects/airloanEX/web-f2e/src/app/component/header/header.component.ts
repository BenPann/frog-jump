import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
import { ActivatedRoute, NavigationEnd, ParamMap, Router } from "@angular/router";
import { Observable, of } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { InitApiService } from '../../service/init/init-api.service';
import { UserDataService } from "../../service/user-data/user-data.service";
import { ModalFactoryService } from '../../shared/service/common/modal-factory.service';
import { showChatBot, closeChatBot, jsShowContactUsDialog, jsCloseDialog } from "src/app/js/start.page.js";

declare let $:any;
declare const myTest: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})



export class HeaderComponent implements OnInit {
  page = '';
  
  reqTrigger = {productType: '', flowType: '', pageName: '', subPageName: ''}; // input json
  
  chatbotUrl$: Observable<string>;
  chatbotUrl: string;
  videoUrl;

  G_CHATBOT_STATUS = "close";
  display = "display: none";

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private userDataService: UserDataService,
    private initApiService: InitApiService,
    private modalFactoryService: ModalFactoryService
  ) {
    // angular 10 calling component function from iframe
    // angular 10 iframe parent javascript function call
    (<any>window).showContactUsDialog = this.showContactUsDialog.bind(this);
    (<any>window).closeChatBot = this.closeChatBot.bind(this);

    this.router.events
    .pipe(
      filter(event => event instanceof NavigationEnd),
      map((evnets: NavigationEnd) => {
        const n1 = this.activatedRoute.firstChild ? this.activatedRoute.firstChild : null;
        const n2 = n1 ? n1.firstChild : null;
        return {
          // route: evnets,
          n1,
          n2
        };
      }),
      switchMap(item => {
        const n1 = item.n1;
        const n2 = item.n2;
        let flowType: string = '';
        let pageName: string = ''; 
        if (n1) {
          this.page += n1.snapshot.url.toString();
          flowType = n1.snapshot.url.toString();
        }
        if (n2) {
          this.page += n2.snapshot.url.toString();
          pageName = n2.snapshot.url.toString();
        }
    
        const main = this.userDataService.contractMain;
        const ProductId = main ? main.productId : null;
        let productType = this.initApiService.makeProductType(ProductId);
        // 測試程式碼
        // ProductType = 'PL';
        // flowType = 'apply';
        // pageName = 'start';
    
        const req = {productType, flowType, pageName, subPageName: ''};
        return of(req);
      })
    ).subscribe(req => { 
      this.reqTrigger = req;
      
      this.initApiService.getTrigger(this.reqTrigger.productType, this.reqTrigger.flowType, this.reqTrigger.pageName).subscribe(data => {
        if (data.status === 0 && data.result) {
          this.chatbotUrl = data.result.chatbotUrl;
          this.chatbotUrl = this.chatbotUrl.replace('${welcome}', 'true');
          this.chatbotUrl = this.chatbotUrl.replace('${session_id}', '');
          this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.chatbotUrl);
          // console.log(this.reqTrigger); // 例如： {productType: "PL", flowType: "apply", pageName: "start"}
          // console.log(this.chatbotUrl); // 例如： http://172.16.253.124/chat-room/index.html?userId=&feedback=true&welcome=true&te_trigger=PL_Application_Start&dimension=PL&session_id=
        }
      });
    });
  }

  ngOnInit() {
	  
  }

  alterChatBotUrl() {
    const main = this.userDataService.contractMain;
    const ProductId = main ? main.productId : null;
    const calProductType = this.initApiService.makeProductType(ProductId);
    const welcome = this.userDataService.welcome;
    if (calProductType) {
      this.reqTrigger.productType = calProductType;
    }
    console.log('### welcome=', welcome);    
    console.log('### this.reqTrigger=', this.reqTrigger);
    this.chatbotUrl$ = this.initApiService.getTrigger(this.reqTrigger.productType, this.reqTrigger.flowType, this.reqTrigger.pageName, this.reqTrigger.subPageName).pipe(
      switchMap(data => {
      if (data.status === 0 && data.result) {
        this.chatbotUrl = data.result.chatbotUrl.replace('${welcome}', welcome);

        const main = this.userDataService.contractMain;
        if (main && main.session_id) {
          this.chatbotUrl = this.chatbotUrl.replace('${session_id}', main.session_id);
        } else {
          this.chatbotUrl = this.chatbotUrl.replace('${session_id}', '');
        }
        console.log('### this.chatbotUrl=', this.chatbotUrl);
        this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.chatbotUrl); 
      }
      return of(this.videoUrl);
    }));
  }

  showChatBot() {
    showChatBot();
  }

  showConsume() {
    this.modalFactoryService.modalDefault({
      message: '如有存款相關問題請撥打02-80239088'
    });
  }
  
  showTest() {
	  myTest(); 
	  
  }
  
  showContactUsDialog(){
	  jsShowContactUsDialog('#dialogContactMethod');
  }

  closeContactUsDialog(){
    jsCloseDialog('#dialogContactMethod');
  }

  // showDialog(target) {
  //   showDialog(target); 
  // }
  
  // closeDialog() {
  //   closeDialog();
  // }

  closeChatBot() {
    // closeChatBot();
    // document.getElementById('SpiritAI').style.display = 'none';
    // document.getElementById("talk-triangle-top").style.display = "none";
    // document.getElementById("talk-triangle-base").style.display = "none";
    // this.display = "display: none";
    // this.G_CHATBOT_STATUS = "close";
    closeChatBot();
  }
}
