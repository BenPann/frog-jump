import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

declare const Rolldate: any;

@Component({
  selector: 'app-rolldate',
  templateUrl: './rolldate.component.html',
  styleUrls: ['./rolldate.component.scss']
})
export class RolldateComponent implements OnInit {
  // example: <app-rolldate></app-rolldate>

  @Input() income: string;
  @Output() outcome = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    this.initrolldate();
  }

  initrolldate(): void {
    // tslint:disable-next-line: no-unused-expression
    const r = new Rolldate({
      el: '#date-group1-2',
      format: 'YYYY-MM-DD',
      beginYear: 2000,
      endYear: 2100
    });
  }
}
