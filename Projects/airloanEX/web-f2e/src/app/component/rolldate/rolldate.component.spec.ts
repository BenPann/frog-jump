import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolldateComponent } from './rolldate.component';

describe('RolldateComponent', () => {
  let component: RolldateComponent;
  let fixture: ComponentFixture<RolldateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolldateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolldateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
