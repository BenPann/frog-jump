import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitPlContractComponent } from './init-pl-contract.component';

describe('InitPlContractComponent', () => {
  let component: InitPlContractComponent;
  let fixture: ComponentFixture<InitPlContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitPlContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitPlContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
