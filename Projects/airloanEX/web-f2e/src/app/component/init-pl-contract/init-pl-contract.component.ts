
import { PCODE2566Req } from 'src/app/service/validate/validate.interface';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,ValidationErrors,FormControl } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
// validator
import { idCardValidator } from '../../validator/idCard-validator';
// service
import { RouterHelperService } from '../../shared/service/common/router-helper.service';
import { ModalFactoryService } from '../../shared/service/common/modal-factory.service';
import { DatepickerService } from '../../shared/service/common/datepicker.service';
import { RouterDirectService } from 'src/app/service/router-direct/router-direct.service';
import { InitApiService } from '../../service/init/init-api.service';
import { EntryService } from '../../service/entry/entry.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { TermsService } from '../../service/terms/terms.service';
import { TermsInput, TermsView } from 'src/app/interface/resultType';

import { RouteGo } from 'src/app/route-go/route-go';

import { ContractMain, ContractMainModel } from 'src/app/service/airloanex/airloanex.interface';
import {CleaveService} from "../../service/cleave/cleave.service";

// declare const Rolldate: any;

@Component({
  selector: 'app-init-pl-contract',
  templateUrl: './init-pl-contract.component.html',
  styleUrls: ['./init-pl-contract.component.scss']
})
export class InitPlContractComponent implements OnInit, OnDestroy {
  private alive = true;
  stepStr = 1;

  // form表單
  initForm: FormGroup;
  // 生日設定
  minDate: NgbDateStruct;
  maxDate: NgbDateStruct;
  today = new Date();
  // termList
  termsDiv: TermsInput[];
  termsLink: TermsInput[];
  // terms: TermsView;
  uniqType = '03'; // 立約

  entry: string;
  entryOther: string;

  resultPayDate: string;

  constructor(
    private fb: FormBuilder,
    private routerHelperService: RouterHelperService,
    private routerDirectService: RouterDirectService,
    private modalService: ModalFactoryService,
    private datepickerService: DatepickerService,
    private initApiService: InitApiService,
    private entryService: EntryService,
    private userDataService: UserDataService,
    private termsService: TermsService,
    private dateFormatter: NgbDateParserFormatter,
    private activatedRoute: ActivatedRoute,
    public cleave: CleaveService,
  ) {
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  ngOnInit() {

    this.userDataService.welcomeReset();

    this.initMessage();

    // 表單init
    this.initForm = this.fb.group({
      userid: ['', [Validators.required, idCardValidator()]],
      birthday: ['', [Validators.required, Validators.maxLength(10), Validators.pattern(/^\d{4}\/\d{2}\/\d{2}$/), LoanAge]],
      agreement: false
    });

    // 生日設定init
    this.initdatepicker();
    this.initTerms();
    // this.initrolldate();

    this.activatedRoute.queryParamMap.subscribe((params: ParamMap) => {
      const entry = params.get('entry');
      const entryOther = []; // EntryData.other
      const keys = params.keys;

      for (const key of keys) {
        if (key !== 'entry') {
          const o = new Object();
          o[key] = params.get(key);
          entryOther.push(o);
        }
      }
      this.entry = entry ? entry : '';
      this.entryOther = JSON.stringify(entryOther);

      // console.log('entry', entry);
      // console.log(entryOther);
    });

  } // end ngOnInit


  initMessage(): void {
    this.modalService.modalHtml({
      showClose: false,
      // tslint:disable-next-line:max-line-length
      message: 
      '謹提醒！<br />' + 
      '若如以下時間完成簽約，將順延至次一營業日完成撥款/額度啟用。<br />' + 
      '<br />' + 
      '個人信用貸款逾撥款日15:00後完成線上簽約。<br />' + 
      '額度型貸款(速還金/好貼薪)或現金卡逾當日16:00後完成線上簽約。'
    });

    // 代償銀行有值 撥款日期過期
    const warnMessage = this.userDataService.warnMsg;
    if (warnMessage) {
      this.modalService.modalHtml({
        showClose: false,
        message: warnMessage
      });
      this.userDataService.removeWarnMsg();
    }
  }

  initTerms(): void {
    this.termsService.getTermList().subscribe(data => {
      if (data) {
        this.termsDiv = data.filter(value => value.type === '2');
        this.termsLink = data.filter(value => value.type === '1');
      }
    });
  }

  // 生日設定init
  initdatepicker(): void {
    const y = this.today.getFullYear();
    const m = this.today.getMonth() + 1;
    const d = this.today.getDate();
    this.minDate = {
      year: y - 100,
      month: m,
      day: d
    };
    this.maxDate = {
      year: y,
      month: m,
      day: d
    };
  }

  /*initrolldate(): void {
    // tslint:disable-next-line: no-unused-expression
    const r = new Rolldate({
      el: '#date-group1-2',
      format: 'YYYY-MM-DD',
      beginYear: 2000,
      endYear: 2100
    });
    console.log(r);
  }*/

  // 身分證轉大寫
  upperCase(event): void {
    if (!event.target.value) {
      return;
    }
    event.target.value = event.target.value.trim().toUpperCase();
  }

  // 取得條款內容
  showTerms(termsName: string) {
    this.termsService
      .getTerms(termsName)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        let termText = '';
        try {
          // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
          const json = JSON.parse(data);
          termText = '查詢條款失敗';
        } catch (error) {
          termText = data;
        }
        this.modalService.modalHtml({ message: termText });
      });
  }

  // 同意按鈕解鎖
  isAgreeDisabled(): boolean {
    return !(this.initForm.valid && this.initForm.get('agreement').value);
  }

  // nextStep()
  // 按下同意鈕，call Api
  sendAgree(): void {
    if (this.initForm.invalid) {
      return;
    }
    const birthday:string = this.initForm.get('birthday').value;
    const idno: string = this.initForm.get('userid').value;
    // // 判斷是否不足20歲
    // if (!this.datepickerService.checkBirthday(birthday)) {
    //   this.modalService.modalDefault({
    //     showConfirm: false,
    //     message:
    //       '線上開戶僅限本國年滿20歲的自然人，若您有開戶需求，請洽各分行辦理。'
    //   });
    //   return;
    // }

    const dateString = birthday
      .split('/')
      .join('');
    // @see init-interface.ts InitDataRequest
    const req = {
      uniqType: this.uniqType,
      // entry: this.activatedRoute.snapshot.queryParams['entry'] ? this.activatedRoute.snapshot.queryParams['entry'] : '',
      entry: this.entry,
      entryOther: this.entryOther,
      // tslint:disable-next-line: max-line-length
      // shortUrl: this.entryService.shortUrlData && this.entryService.shortUrlData.ShortUrl ? this.entryService.shortUrlData.ShortUrl : '', // 短網址
      idno: idno,
      birthday: dateString
    };

    // 先登入
    this.initApiService
      .doLogin(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        console.log("DoLogin",data);
        if (data.status === 0) {
          // 登入成功之前 把所有資料先清空
          sessionStorage.clear();
          // @save to sessionStorage
          this.userDataService.initRqData = req; // 'initRqData' FIXME: Charles: Not needed.
          this.userDataService.token = data.result.token; // 'kgiToken'
          const main: ContractMain = new ContractMainModel();

          const initBankInfo = data.result.initBankInfo;
          const rateDatas = data.result.rateDatas;

          // 20201120 判斷力約案件及有無代償
          this.initApiService.initPlPayBank()
          .pipe(takeWhile(() => this.alive))
          .subscribe(initData => {
            // 只有PL及有代償 status=0
            if (initData.status === 0) {
                this.initApiService.judgeExceedPayDate()
                .pipe(takeWhile(() => this.alive))
                .subscribe(judgeData => {
                  // 沒有逾期
                  if (judgeData.status === 0) {
                    if (this.resultPayDate) {
                      main.payDate = this.resultPayDate;
                    }
                    this.initMainAndUpdateTerms(main, initBankInfo, rateDatas, data);
                  } else {
                     this.initForm.reset();
                     this.modalService.modalDefault({ message: judgeData.message });
                     this.routerHelperService.navigate(RouteGo.initPlContract.navi);
                  }
                });
            } else {
              this.initMainAndUpdateTerms(main, initBankInfo, rateDatas, data);
            } // end else
          });
        } else {
          this.modalService.modalDefault({ message: data.message });
        }
      });
    this.routerHelperService.navigate(RouteGo.initPlContract.navi);
  } // end sendAgree

  /**  */
  initMainAndUpdateTerms(main: ContractMain, initBankInfo: any, rateDatas: any, data: any): void {
    if (initBankInfo) {
      this.userDataService.doInitBankInfo(main, initBankInfo);
      this.userDataService.doInitRateDatas(main, rateDatas);
      this.userDataService.contractMain = main;
      if (initBankInfo.phone) {
        this.routerDirectService.otpPrevPage = RouteGo.initPlContract.navi;
        this.routerDirectService.otpNextPage = RouteGo.choosePlContract.navi;
        this.routerDirectService.otpPhoneNum = initBankInfo.phone;
        this.routerDirectService.prevPage = RouteGo.initPlContract.navi;
      }
    } // end if

    // 清空的時候 保留entry資料
    const termsArray = this.termsDiv.concat(this.termsLink);
    forkJoin([this.termsService.updateContractMainTerms(
      // 儲存同意條款
      termsArray.map(value => {
        return {
          termName: value.termName,
          check: 'Y'
        };
      })
    )
    ]).pipe(takeWhile(() => this.alive)).subscribe(resultArray => {
      // console.log(resultArray);
      if (resultArray[0].status === 0) {
        if (data.message !== '成功') {
          this.modalService.modalDefault({ message: data.message });
        } else {
          // if (main.initBankNo && main.initAccount && main.initPhone) {
          //   this.routerHelperService.navigate(RouteGo.identifyByBankAccountPlContract.navi);
          // } else {
          this.routerHelperService.navigate(RouteGo.find(data.result.routeGo).navi);
          // }
        }
      } else {
        this.modalService.modalDefault({ message: resultArray[0].message });
      }
    });
  } // end initBankInfoAndUpdateTerms

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.initForm.controls[id].invalid && this.initForm.controls[id].touched && this.initForm.controls[id].hasError(errorName);
    } else {
      return this.initForm.controls[id].invalid && this.initForm.controls[id].touched;
    }
  }
}

/**
 * 檢核貸款年齡: 不可小於20歲 不可大於65歲
 */
export function LoanAge(control: FormControl): ValidationErrors {
  const birthday = control;
  if (birthday && birthday.value && birthday.value.length == 10) {
    const gt20 = checkAge(birthday.value, -20);
    const lt65 = checkAge(birthday.value, 65);
    // console.log(`gt20:${gt20} lt65:${lt65}`);
    if (!gt20) {
      return {forbiddenName: {value: birthday.value}, underAge20: true}
    }
    if (!lt65) {
      return {forbiddenName: {value: birthday.value}, overAge65: true}
    }
  }
  return null;
}
/**
 * age=-20: 不可小於20歲
 * age= 65: 不可大於65歲
 * @param yyyy_mm_dd
 * @param age
 */
function checkAge(yyyy_mm_dd: string, age: number): boolean {
  if (!yyyy_mm_dd || yyyy_mm_dd.length != 10) {
    return;
  }
  const date = new Date();

  // 今天日期減20年
  date.setFullYear(date.getFullYear() - Math.abs(age));
  const validate = new Date(date).getTime();
  const birthday = new Date(yyyy_mm_dd).getTime();

  if (age < 0) {
    // console.log(`${validate >= birthday} validate:${date.toISOString().substring(0,10)} ${age < 0 ? '>=':'<='} birthday:${yyyy_mm_dd}`);
    return validate >= birthday;
  } else {
    // console.log(`${validate <= birthday} validate:${date.toISOString().substring(0,10)} ${age < 0 ? '>=':'<='} birthday:${yyyy_mm_dd}`);
    return validate <= birthday;
  }

}