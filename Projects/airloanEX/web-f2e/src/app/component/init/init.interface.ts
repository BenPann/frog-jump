export interface InitDataRequest {
  idno: string;
  birthday: string;
  // promoChannelID: string;
  // promoDepart: string;
  // promoMember: string;
  entry: string;
  // shortUrl: string;
  uniqType: string;
  // productType: string;
}

export interface UserTypeResponse {
  breakPointPage: string;
  chtName: string;
  isVerify: string;
  phone: string;
  nexttime: string;
}

export interface UsedPhoneAndEmailList {
  phone: string[];
  emailAddress: string[];
}

export interface ChosenPlContract {
  contract: string;
}

export interface AgreePlContract {
  email: string;
  reviewDate: string;
  authInfo: string;
}

export interface InfoPlContract {
  payMentDay: string;
}
