import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-photo',
  templateUrl: './add-photo.component.html',
  styleUrls: ['./add-photo.component.scss']
})
export class AddPhotoComponent implements OnInit {

  // 身份證明區塊
  isIdCardOpen = false;

  // 財力證明區塊
  isProveOpen = false;

  constructor() { }

  ngOnInit() {
  }

}
