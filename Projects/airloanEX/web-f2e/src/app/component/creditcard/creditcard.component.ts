import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  /*selector: 'app-creditcard',
  template: '<input type="text" [cleave]="cleaveOptions">'*/
  selector: 'app-creditcard',
  templateUrl: './creditcard.component.html',
  styleUrls: ['./creditcard.component.scss']
})
export class CreditComponent implements OnInit {
  // example: <app-creditcard [creditcardNo]="ccn"></app-creditcard>
  @Input() creditcardNo: string;
  @Output() creditcardNoChange = new EventEmitter<string>();

  cleaveOptions = {
    creditcard: true,
    delimiter: ' ',
    blocks: [4, 4, 4, 4]
  };
  /*
  cleaveOptions = {
    date: true,

    delimiter: '-',
    datePattern: ['Y', 'm', 'd']
  };
  */

  constructor() { }

  ngOnInit() {
  }

  // var selectedCardIcon = null;
  // // credit card
  // var cleaveCreditCard = new Cleave(".input-credit-card", {
  //   creditCard: true,
  //   onCreditCardTypeChanged: function(type) {
  //     type = type.split("15")[0];

  //     if (selectedCardIcon) {
  //       DOM.removeClass(selectedCardIcon, "active");
  //     }

  //     selectedCardIcon = DOM.select(".icon-" + type);

  //     if (selectedCardIcon) {
  //       DOM.addClass(selectedCardIcon, "active");
  //     }
  //   }
  // });

  // var btnClear = DOM.select(".btn-clear");
  // var creditCardInput = DOM.select(".input-credit-card");
  // creditCardInput.addEventListener("focus", function() {
  //   DOM.removeClass(btnClear, "hidden-right");
  // });
  // btnClear.addEventListener("click", function() {
  //   cleaveCreditCard.setRawValue("");
  //   DOM.addClass(btnClear, "hidden-right");
  //   creditCardInput.focus();
  // });

}
