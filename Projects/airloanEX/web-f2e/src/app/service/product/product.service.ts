import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {WebResult} from 'src/app/interface/resultType';
import {ChooseProductReq, ChooseAccountReq, BookBranchInfo} from './product.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private httpClient: HttpClient) {}

  getMaxVerifyCount() {
    return this.httpClient.get<WebResult>('../api/data/getMaxVerifyCount');
  }

  getSecuritiesInfo() {
    return this.httpClient.get<WebResult>('../api/apply/getSecuritiesInfo');
  }

  getBranchBankInfo() {
    return this.httpClient.get<WebResult>('../api/apply/getBranchBankInfo');
  }
/*
  getProductContent() {
    return this.httpClient.get<WebResult>('../api/apply/product');
  }

  getAccountTypeContent() {
    return this.httpClient.get<WebResult>('../api/apply/accountType');
  }

  getMaxVerifyCount() {
    return this.httpClient.get<WebResult>('../api/apply/getMaxVerifyCount');
  }

  getBookMonth() {
    return this.httpClient.get<WebResult>('../api/apply/getBookMonth');
  }

  newUserChooseProduct(reqData: ChooseProductReq) {
    return this.httpClient.post<WebResult>('../api/apply/product', reqData);
  }

  newUserChooseAccount(reqData: ChooseAccountReq) {
    return this.httpClient.post<WebResult>('../api/apply/accountType', reqData);
  }

  setCommonProduct(obj: any) {
    return this.httpClient.post<WebResult>('../api/apply/setCommonProduct', obj);
  }

  getHoliday(month: string) {
    return this.httpClient.post<WebResult>('../api/apply/getHoliday', {
      bookingmonth: month
    });
  }

  getAppointmentInfo() {
    return this.httpClient.get<WebResult>('../api/apply/bookBranch');
  }

  saveAppointmentInfo(reqData: BookBranchInfo) {
    return this.httpClient.post<WebResult>('../api/apply/bookBranch', reqData);
  }

  // 剛進產品選擇頁時，發API去把信用卡Table資料改成狀態為00
  // setCreditCardStatus() {
  //   return this.httpClient.get<WebResult>('../api/apply/revertCreditCaseStatus');
  // }*/
}
