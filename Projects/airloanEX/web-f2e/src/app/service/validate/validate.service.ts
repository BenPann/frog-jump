import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebResult } from 'src/app/interface/resultType';
import { NCCCReq, PCODE2566Req, EddaReq } from './validate.interface';

@Injectable({
  providedIn: 'root'
})
export class ValidateService {
  constructor(private httpClient: HttpClient) {}

  setNCCC(req: NCCCReq) {
    return this.httpClient.post<WebResult>(
      '../api/validate/nccc',
      req
    );
  }

  /**
   * Legecy code of ed3
   * @deprecated
   */
  setPCode2566(req: PCODE2566Req) {
    return this.httpClient.post<WebResult>(
      '../api/validate/pcode2566',
      req
    );
  }

  /**
   * auth - 登入驗身時的 PCode2566
   */
  setPCode2566Auth(req: PCODE2566Req) {
    return this.httpClient.post<WebResult>(
      '../api/validate/pcode2566Auth',
      req
    );
  }

  /**
   * sign - 授扣款時的 PCode2566
   */
  setPCode2566Sign(req: PCODE2566Req) {
    return this.httpClient.post<WebResult>(
      '../api/validate/pcode2566Sign',
      req
    );
  }

  getVerifyCount() {
    return this.httpClient.get<WebResult>('../api/validate/getVerifyCount', {});
  }

  /**
   * 繳款資訊 驗證edda
   */
  getVerifyEdda(req: EddaReq) {
    return this.httpClient.post<WebResult>(
      '../api/validate/getVerifyEdda',
      req
    );
  }

}
