import {Injectable} from '@angular/core';
import {UserDataService} from '../user-data/user-data.service';
import {ModalFactoryService} from 'src/app/shared/service/common/modal-factory.service';
import {RouterHelperService} from 'src/app/shared/service/common/router-helper.service';
import {RouterDirectService} from '../router/router-direct.service';
import {forkJoin} from 'rxjs';
import {ConfirmDataReq} from '../apply-account/apply-account.interface';
import {ApplyAccountService} from '../apply-account/apply-account.service';
import { DocumentService } from 'src/app/shared/service/document/document.service';
import { UploadDocument } from 'src/app/shared/component/upload-id-card/upload-id-card.interface';
import { BookBranchInfo, AppointmentInfo } from 'src/app/service/product/product.interface';

@Injectable({
  providedIn: 'root'
})
export class BreakPointService {
  constructor(
    private userDataService: UserDataService,
    private modalFactoryService: ModalFactoryService,
    private routerHelperService: RouterHelperService,
    private routerDirectService: RouterDirectService,
    private documentService: DocumentService,
    private applyAccountService: ApplyAccountService
  ) {
  }

  breakPoingHandle() {
    const route = this.routerDirectService.getBreakPointPageRouter(
      this.userDataService.initUserTypeData.breakPointPage
    );

    this.hadleBreakPointData(route);
  }

  public hadleBreakPointData(route: string[]) {
    // 先看有沒有找到對應router
    if (route.length > 0) {
      forkJoin([
        this.applyAccountService.getConfirmData(),
        this.documentService.getPhotoList(),
        // this.applyAccountService.getCCProductId(),
        // this.applyAccountService.getUploadDocument(),
        // this.applyAccountService.getNewUserProduct()
      ]).subscribe(
        resultArray => {
          let success = true;
          for (const r of resultArray) {
            if (r.status !== 0) {
              this.modalFactoryService.modalDefault({message: r.message});
              success = false;
              break;
            }
          }

          const tempData: ConfirmDataReq = resultArray[0].result;
          // 處理 選擇的信用卡
          this.userDataService.creditCardProductId = resultArray[0].result
            .chooseCard.creditProductId
            ? resultArray[0].result.chooseCard.creditProductId
            : '';

          // OCR相關都塞空
          this.userDataService.idCardResponse = {
            waterMarkImage: '',
            name: '',
            idCard_Issue_DT: '',
            idCard_Issue_Type: '',
            idCard_Issue_City: '',
            householdAddr1: '',
            householdAddr2: '',
            householdAddr3: '',
            householdAddr4: '',
            householdZipCode: '',
            marriage: '',
            subSerial: ''
          };


          this.userDataService.giftFillDataReq = this.setObjectToEmpty(
            tempData.giftData
          );

          // 處理 上傳圖片
          const newUploadDocument: UploadDocument = Object.assign(
            {},
            this.userDataService.uploadDocument
          );
          resultArray[1].result.map(value => {
            if (value.sType === '1') {
              newUploadDocument.frontId = value.waterMarkImage;
              newUploadDocument.frontIdSerial = value.subSerial;
            }
            if (value.sType === '2') {
              newUploadDocument.backId = value.waterMarkImage;
              newUploadDocument.backIdSerial = value.subSerial;
            }
            if (value.sType === '3') {
              newUploadDocument.secCardId = value.waterMarkImage;
              newUploadDocument.secCardIdSerial = value.subSerial;
            }
            this.userDataService.uploadDocument = this.setObjectToEmpty(
              newUploadDocument
            );
          });

          // this.userDataService.uploadDocument = this.setObjectToEmpty(
          //   newUploadDocument
          // );

          // this.userDataService.uploadDocument = newUploadDocument;

          // *** 同上一次的選擇 ***
          // 處理 選擇的產品 (帳戶 帳加卡)
          // this.userDataService.newUserChooseProductReq = this.setObjectToEmpty(
          //   resultArray[3].result
          // );

          // *** 同這次session的生日 ***
          // this.userDataService.initRqData = {
          //   ...this.userDataService.initRqData,
          //   birthday: resultArray[3].result.birthday
          // };

          this.userDataService.idCardFrontFillDataReq = this.setObjectToEmpty(
            tempData.idCardFrontData
          );

          this.userDataService.idCardBackFillDataReq = this.setObjectToEmpty(
            tempData.idCardBackData
          );

          this.userDataService.basicFillDataReq = this.setObjectToEmpty(
            tempData.basicData
          );

          // for fillData3
          if (this.userDataService.appointmentInfo.branchID === '' && resultArray[0].result.basicData.branchID !== '') {
            const sessionReq: BookBranchInfo = {
              bookingDate: '',
              bookingTime: '',
              bookingBranchId:
              resultArray[0].result.basicData.branchID
            };

            const branchInfo: AppointmentInfo = {
              phone: '',
              bookBranchInfo: sessionReq,
              branchAddrCity: '',
              branchAddrDist: '',
              branchName: '',
              branchAddrZipCode: '',
              branchAddr: '',
              branchID:
              resultArray[0].result.basicData.branchID
            };
            this.userDataService.appointmentInfo = branchInfo;
          }

          tempData.corpData.agreement = resultArray[0].result.active_recommendation;
          this.userDataService.corpFillDataReq = this.setObjectToEmpty(
            tempData.corpData
          );
          // 1.	前一次流程在"3.6數位帳戶選擇驗身"選擇"下次再驗"(序號4)，有往下進行其他流程，接續斷點時應從"3.6數位帳戶選擇驗身"開始，但"下次再驗"(序號4)隱藏，重新進行流程，但已填入資料會自動帶入。
          // isVerify: "0"
          // breakPointPage: "N02"

          // 2.	前一次驗身成功，有往下進行其他流程，下一次接續斷點，會先進行OTP驗證，然後跳至斷點頁面。
          // isVerify: "1"

          // 3.	前一次驗身6次失敗轉預約分行，下一次接續斷點，會先進行OTP驗證，然後跳至斷點頁面。
          // isVerify: "1"
          if (this.userDataService.initUserTypeData.isVerify === '0') {

            this.routerDirectService.appointmentPrevStep =
                'chooseIdentificationStyle';
            // tslint:disable-next-line: max-line-length
            if (this.userDataService.initUserTypeData.breakPointPage === 'N00') {
              this.routerHelperService.navigate(route);
            } else {
              this.routerHelperService.navigate([
                'n',
                'chooseIdentificationStyle'
              ]);
            }
          } else {
            this.routerHelperService.navigate(route);
          }
        },
        error => {
          this.modalFactoryService.modalDefault({
            message: error.message
          });
        }
      );
    } else {
      this.modalFactoryService.modalDefault({
        message: '查無此斷點資訊'
      });
    }
  }

  setObjectToEmpty(object: any) {
    for (const key in object) {
      if (object.hasOwnProperty(key)) {
        const element = object[key];
        if (!element) {
          object[key] = '';
        }
      }
    }
    return object;
  }
}
