import { TestBed } from '@angular/core/testing';

import { RouterDirectService } from './router-direct.service';

describe('RouterDirectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RouterDirectService = TestBed.get(RouterDirectService);
    expect(service).toBeTruthy();
  });
});
