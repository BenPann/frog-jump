import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { filter, tap, map, switchMap } from 'rxjs/operators';
import { ApplyAccountService } from 'src/app/service/apply-account/apply-account.service';
import { UserDataService } from '../user-data/user-data.service';
import { EntryService } from 'src/app/service/entry/entry.service';

import { RouteGo } from 'src/app/route-go/route-go';

export enum PreviousPage {
  identifyByBankAccount = 1,
  identifyByCreditCard,
  appointment,
  chooseIdentificationStyle,
}

@Injectable({
  providedIn: 'root'
})
export class RouterDirectService {
  accountAndCardProcess = [
    'chooseProduct',
    'chooseAccountType',
    'chooseCard',
    'uploadDocument',
    'fillData1',
    'fillData2',
    'fillData3',
    'fillData4',
    'fillData5',
    'confirmData',
    'setWebBank',
    'allFinish'
  ];

  accountProcess = [
    'chooseProduct',
    'chooseAccountType',
    'uploadDocument',
    'fillData1',
    'fillData2',
    'fillData3',
    'fillData4',
    'confirmData',
    'setWebBank',
    'allFinish'
  ];
  accountAndCardProcessForCL = [
    'chooseProduct',
    'chooseIdentificationStyle',
    'chooseCard',
    'uploadDocument',
    'fillData1',
    'fillData2',
    'fillData3',
    'fillData4',
    'fillData5',
    'confirmData',
    'setWebBank',
    'allFinish'
  ];

  accountProcessForCL = [
    'chooseProduct',
    'chooseIdentificationStyle',
    'uploadDocument',
    'fillData1',
    'fillData2',
    'fillData3',
    'fillData4',
    'confirmData',
    'setWebBank',
    'allFinish'
  ];

  // N00 請選擇帳戶類型
  // N01 預約完成申辦分行
  // N02 請選擇驗身方式
  // N03 選擇信用卡
  // N04 上傳證件頁
  // N05 fillData1
  // N06 fillData2
  // N07 fillData3
  // N08 fillData4
  // N09 fillData5
  // N10 confirmdata
  // N11 網銀
  breakPoingMapping = [
    { pageCode: 'N02', pageName: RouteGo.chooseIdentificationStylePlContract.path }, // 1-2
    { pageCode: 'N02', pageName: RouteGo.identifyByBankAccountPlContract.path }, // 1-3

    { pageCode: 'N00', pageName: 'chooseAccountType' },
    { pageCode: 'N01', pageName: 'appointment' },
    { pageCode: 'N02', pageName: 'chooseIdentificationStyle' },
    { pageCode: 'N02', pageName: 'identifyByBankAccount' },
    { pageCode: 'N02', pageName: 'identifyByCreditCard' },
    { pageCode: 'N03', pageName: 'chooseCard' },
    { pageCode: 'N04', pageName: 'uploadDocument' },
    { pageCode: 'N05', pageName: 'fillData1' },
    { pageCode: 'N06', pageName: 'fillData2' },
    { pageCode: 'N07', pageName: 'fillData3' },
    { pageCode: 'N08', pageName: 'fillData4' },
    { pageCode: 'N09', pageName: 'fillData5' },
    { pageCode: 'N10', pageName: 'confirmData' },
    { pageCode: 'N11', pageName: 'setWebBank' }
  ];

  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
    private entryService: EntryService,
    private activatedRoute: ActivatedRoute,
    private userDataService: UserDataService,
    private applyAccountService: ApplyAccountService
  ) {
    // 在導頁之後
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => {
          // 取得最下層的router
          let child = this.activatedRoute.firstChild;
          while (child.firstChild) {
            child = child.firstChild;
          }
          return child;
        })/*,
        // 此段為 確認為新戶 且有需要斷點
        filter(
          route =>
            route.routeConfig.data &&
            route.routeConfig.data.breakPoint === true
        ),
        switchMap(route => {
          return this.applyAccountService.setBreakPointPage(
            this.getBreakPointPageCode(route.routeConfig.path)
          );
        })*/
      )
      .subscribe();
  }

  newUserNextStep(productId: string) {
    const routerArray = this.getRouterArray(productId);
    const array = this.router.url.split('/').filter(element => element !== '');
    const index = this.getRouterIndex(routerArray, array[1]);

    if (index >= 0) {
      if (this.previousStep === '4' && routerArray[index + 1] === 'setWebBank') {
        this.newUserAllFinish();
      // } else if (this.entryService.isCreditCard && routerArray[index + 1] === 'chooseAccountType' ) {
      //   this.routerHelperService.navigate(['n', 'chooseIdentificationStyle']);
      } else {
        this.routerHelperService.navigate([array[0], routerArray[index + 1]]);
      }
    } else {
      console.warn('錯誤的網址', this.router.url);
    }
  }

  newUserBackStep(productId: string) {
    const routerArray = this.getRouterArray(productId);
    const array = this.router.url.split('/').filter(element => element !== '');
    const index = this.getRouterIndex(routerArray, array[1]);

    if (index >= 0) {
      // 網銀特殊處理 要跳過
      if (routerArray[index - 1] === 'setWebBank') {
        this.routerHelperService.navigate([array[0], routerArray[index - 2]]);
      } else if (routerArray[index] === 'uploadDocument') {
        if (this.userDataService.newUserChooseProductReq.creditType === '2') {
          this.routerHelperService.navigate(['n', 'chooseCard']);
        } else {
          switch ( this.previousStep) {
            case '1':
                this.routerHelperService.navigate(['n', 'identifyByBankAccount']);
              break;
            case '2':
              this.routerHelperService.navigate(['n', 'identifyByCreditCard']);
              break;
            case '3': // 若於"3.5一般帳戶(預約分行開戶)OTP驗證"進入，點擊進入"3.6數位帳戶選擇驗身"
              this.routerHelperService.navigate(['n', 'appointment']);
              break;
            case '4': // 若於"3.12數位帳戶下次再驗-OTP驗證"進入，點擊進入"3.6數位帳戶選擇驗身"
              this.routerHelperService.navigate(['n', 'chooseIdentificationStyle']);
              break;
            default:
              this.routerHelperService.navigate(['n', 'chooseIdentificationStyle']);
              break;
          }
        }
      } else {
        this.routerHelperService.navigate([array[0], routerArray[index - 1]]);
      }
    }  else {
      console.warn('錯誤的網址', this.router.url);
    }
  }

  newUserAllFinish() {
    this.routerHelperService.navigate(['n', 'allFinish']);
  }

  getRouterArray(productId: string) {
    if (productId === '1') {
      if (this.entryService.isCL()) {
        return this.accountProcessForCL;
      } else {
        return this.accountProcess;
      }
    } else if (productId === '2') {
      if (this.entryService.isCL()) {
        return this.accountAndCardProcessForCL;
      } else {
        return this.accountAndCardProcess;
      }
    } else {
      console.warn('錯誤的產品代號', productId);
      return undefined;
    }
  }

  getRouterIndex(routerArray: string[], routerUrl: string) {
    return routerArray.findIndex(element => {
      return element === routerUrl;
    });
  }

  getBreakPointPageRouter(pageCode: string) {
    const page = this.breakPoingMapping.find(value => value.pageCode === pageCode);
    if (page) {
      return ['n', page.pageName];
    } else {
      return [];
    }
  }

  getBreakPointPageCode(pageName: string) {
    return this.breakPoingMapping.find(value => value.pageName === pageName).pageCode;
  }

  public get previousStep(): string {
    return sessionStorage.getItem('previousStep');
  }

  public set previousStep(key: string) {
    sessionStorage.setItem('previousStep', PreviousPage[key]);
  }

  public get passID(): string {
    return sessionStorage.getItem('passIdentification');
  }

  public set passID(answer: string) {
    sessionStorage.setItem('passIdentification', answer);
  }

  public get passOTP(): string {
    return sessionStorage.getItem('passOTP');
  }

  public set passOTP(answer: string) {
    sessionStorage.setItem('passOTP', answer);
  }

  public get appointmentPrevStep(): string {
    return sessionStorage.getItem('appointmentPreviousStep');
  }

  public set appointmentPrevStep(answer: string) {
    sessionStorage.setItem('appointmentPreviousStep', answer);
  }
}

