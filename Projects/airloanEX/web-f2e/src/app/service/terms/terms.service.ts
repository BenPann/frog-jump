import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TermsInput, TermsView, WebResult, ProductView, ProductList} from '../../interface/resultType';
import {RouterHelperService} from '../../shared/service/common/router-helper.service';

@Injectable({
  providedIn: 'root'
})
export class TermsService {
  public token: String;

  constructor(
    private httpClient: HttpClient,
    private routerHelperService: RouterHelperService
  ) {}

  getTerms(termsName: String): Observable<any> {
    const headers = new HttpHeaders().set(
      'Content-Type',
      'text/plain; charset=utf-8'
    );
    return this.httpClient.get('../publicApi/terms/' + termsName, {
      headers,
      responseType: 'text'
    });
  }

  getFuncDesc() {
    return this.httpClient.get<WebResult>('../api/apply/verifyPageDesc');
  }

  getTermList(url?: string): Observable<TermsInput[]> {
    const pageName = url ? url : this.routerHelperService.getRouterAddress();
    return this.httpClient.post<TermsInput[]>(
      '../publicApi/terms/getTermsList',
      {
        url: 'cont-' + pageName // FIXME: Charles: fix to ed3-init for STP. Now it is depend on FakeBackendInterceptor.ts
      }
    );
  }

  getProductList(productView: ProductView): Observable<ProductList[]> {
    return this.httpClient.post<ProductList[]>(
      '../publicApi/terms/getProductList',
      productView
    );
  }

  getCommonProduct(obj: any): Observable<any> {
    return this.httpClient.post<any>('../api/apply/getCommonProduct', obj);
  }

  updateCaseDataTerms(termList: TermsView[]) {
    return this.httpClient.post<WebResult>(
      '../api/terms/setCaseDataTerms',
      termList
    );
  }

  updateContractMainTerms(termList: TermsView[]) {
    return this.httpClient.post<WebResult>(
      '../api/terms/setContractMainTerms',
      termList
    );
  }
}
