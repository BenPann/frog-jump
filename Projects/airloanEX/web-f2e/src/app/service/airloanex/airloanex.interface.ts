
export interface KgiBankInfo {
    kgiBankNo: string;
    kgiAcctNo: string;
    kgiAcctName: string;
    kgiBranchNo: string;
}
export interface ContractMain {

    uniqType: string;
    entry: string;

    idno: string;
    birthday: string;
    email: string;
    productId: string;

    initPhone: string;
    initEmail: string;
    initBankNo: string;
    initAccount: string;
    initSordBankNo: string;
    initSordAccNo: string;
    initPayKindDay: string;

    kgiBankInfos: KgiBankInfo[];

    bank: string;
    account: string;
    phone: string;
    eddaBankId: string;
    eddaAccountNo: string;

    branchId: string;
    payMentDay: string;
    readDate: string;
    authInfo: string;

    payBankOption: string;
    hasPayBankOption: boolean;
    hasSordBankOption: boolean;

    inputMode: string; // N 時撥款資訊頁僅顯示電文中撥款/繳款資訊內容(並鎖定) / Y 時撥款資訊頁讓使用者自行填寫

    payBank: string; // 目的是為了初步判斷繳款日
    payDate: string; // 撥款日

    rate: string;
    aprove_period: string;
    apy_fee: string;
    b_range: string;
    rate_desc: string;
    p_prj_code: string;
    int_type: string;
    p_now_rate: string;
    int_pre_1: string;
    fix_rate1: string;
    tbA_aprove_amount: string;
    session_id: string;

    rateDatas: RateData[];
    sum_rate: string; // 總費用年百分率 用於畫面顯示
}

export class ContractMainModel implements ContractMain {

    uniqType: string;
    entry: string;

    idno: string;
    birthday: string;
    email: string;
    productId: string;

    initPhone: string;
    initEmail: string;
    initBankNo: string;
    initAccount: string;
    initSordBankNo: string;
    initSordAccNo: string;
    initPayKindDay: string;

    kgiBankInfos: KgiBankInfo[] = [];

    bank: string;
    account: string;
    phone: string;
    eddaBankId: string;
    eddaAccountNo: string;

    branchId: string;
    payMentDay: string;
    readDate: string;
    authInfo: string;

    payBankOption: string;
    hasPayBankOption: boolean;
    hasSordBankOption: boolean;

    inputMode: string; // N 時撥款資訊頁僅顯示電文中撥款/繳款資訊內容(並鎖定) / Y 時撥款資訊頁讓使用者自行填寫

    payBank: string; // 目的是為了初步判斷繳款日
    payDate: string; // 撥款日

    rate: string;
    aprove_period: string;
    apy_fee: string;
    b_range: string;
    rate_desc: string;
    p_prj_code: string;
    int_type: string;
    p_now_rate: string;
    int_pre_1: string;
    fix_rate1: string;
    tbA_aprove_amount: string;
    session_id: string;

    rateDatas: RateData[] = [];
    sum_rate: string; // 總費用年百分率 用於畫面顯示
}

export interface RateData {
    pay_rate_m;
    pay_rate_date: string;
    pay_rate;
}

export interface GetDgtConstResp {
    case_no: string;
}
