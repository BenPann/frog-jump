import {Injectable} from '@angular/core';
import {WebResult} from 'src/app/interface/resultType';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AirloanexService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  getAutInfo(termName: string) {
    return this.httpClient.post<WebResult>('../api/stp/getAutInfo', {termName: termName});
  }
}