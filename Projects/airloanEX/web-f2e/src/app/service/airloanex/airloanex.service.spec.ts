import { TestBed } from '@angular/core/testing';

import { AirloanexService } from './airloanex.service';

describe('AirloanexService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AirloanexService = TestBed.get(AirloanexService);
    expect(service).toBeTruthy();
  });
});
