import {Injectable} from '@angular/core';
import {InitDataRequest, UsedPhoneAndEmailList, UserTypeResponse} from 'src/app/component/init/init.interface';
import {AppointmentInfo, ChooseAccountReq, ChooseProductReq} from '../product/product.interface';
import {IDCardResponse} from 'src/app/shared/service/document/document.interface';
import {UploadDocument} from 'src/app/shared/component/upload-id-card/upload-id-card.interface';
import {
  BasicFillDataReq,
  CorpFillDataReq,
  CreditCardCifInfo,
  GiftFillDataReq,
  IDCardBackFillDataReq,
  IDCardFrontFillDataReq,
  ChinaLifeCifInfo,
} from '../apply-account/apply-account.interface';
import {
  ContractMain, RateData
} from '../airloanex/airloanex.interface';
import {NCCCReqPlus, PCODE2566Req, VerifyCountResp, MaxVerifyCount} from '../validate/validate.interface';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  public welcomeReset() {
    sessionStorage.removeItem("welcome");
  }

  public get welcome(): string {
    let welcome = sessionStorage.getItem("welcome");
        welcome = (welcome === null ? 'true' : 'false');
    this.welcome = welcome;
    return welcome;
  }

  public set welcome(data: string) {
    sessionStorage.setItem('welcome', data);
  }

  /**
   * 開戶認證session存取
   */
  public get initRqData(): InitDataRequest {
    return this.getJSON('initRqData');
  }

  public set initRqData(initRqData: InitDataRequest) {
    sessionStorage.setItem('initRqData', JSON.stringify(initRqData));
  }

  public get initUserTypeData(): UserTypeResponse {
    return this.getJSON('initUserTypeData');
  }

  public set initUserTypeData(initUserTypeData: UserTypeResponse) {
    sessionStorage.setItem(
      'initUserTypeData',
      JSON.stringify(initUserTypeData)
    );
  }

  public get usedPhoneAndEmail(): UsedPhoneAndEmailList {
    return this.getJSON('usedPhoneAndEmail');
  }

  public set usedPhoneAndEmail(list: UsedPhoneAndEmailList) {
    sessionStorage.setItem(
      'usedPhoneAndEmail',
      JSON.stringify(list)
    );
  }

  public get newUserChooseProductReq(): ChooseProductReq {
    return this.getJSON('newUserChooseProductReq');
  }

  public set newUserChooseProductReq(chooseProductReq: ChooseProductReq) {
    sessionStorage.setItem(
      'newUserChooseProductReq',
      JSON.stringify(chooseProductReq)
    );
  }

  public get newUserChooseAccountReq(): ChooseAccountReq {
    return this.getJSON('newUserChooseAccountReq');
  }

  public set newUserChooseAccountReq(chooseAccountReq: ChooseAccountReq) {
    sessionStorage.setItem(
      'newUserChooseAccountReq',
      JSON.stringify(chooseAccountReq)
    );
  }

  //#region 上傳證件頁面相關開始

  public get idCardResponse(): IDCardResponse {
    return this.getJSON('idCardResponse');
  }

  public set idCardResponse(idCardResponse: IDCardResponse) {
    // 先取出原本的
    let resp: any = {};
    if (this.idCardResponse) {
      resp = this.idCardResponse;
    }
    // AP回來，如果有值，才填入原本的之前
    for (const key in idCardResponse) {
      if (!resp.hasOwnProperty(key) || (idCardResponse[key] && idCardResponse[key] !== '')) {
        resp[key] = idCardResponse[key];
      }
    }

    sessionStorage.setItem(
      'idCardResponse',
      JSON.stringify(resp)
    );
  }

  /**
   * 預載initBankInfo預設值
   */
  public doInitBankInfo(main: ContractMain, initBankInfo: any): void {

    if (initBankInfo) {
      if (initBankInfo.phone) {
        main.phone = initBankInfo.phone; // 預設值，RPL需要
        main.initPhone = initBankInfo.phone;
      }

      if (initBankInfo.email) {
        main.initEmail = initBankInfo.email;
      }
      // 20201116 調整 如果為本行 帳號取14碼
      if (initBankInfo.expBankNo && initBankInfo.expAccount) {
        main.initBankNo = initBankInfo.expBankNo;
        if (main.initBankNo.substring(0, 3) === '809') {
          if (initBankInfo.expAccount.length === 15) {
            initBankInfo.expAccount = initBankInfo.expAccount.substr(1, 14);
          } else if (initBankInfo.expAccount.length === 16) {
            initBankInfo.expAccount = initBankInfo.expAccount.substr(2, 14);
          }
        }
        main.initAccount = initBankInfo.expAccount;
      }
      if (initBankInfo.sordBankNo && initBankInfo.sordAcctNo) {
        main.initSordBankNo = initBankInfo.sordBankNo;
        // 20201116 調整 如果為本行 帳號取14碼
        if (main.initSordBankNo.substring(0, 3) === '809') {
          if (initBankInfo.sordAcctNo.length === 15) {
            initBankInfo.sordAcctNo = initBankInfo.sordAcctNo.substr(1, 14);
          } else if (initBankInfo.sordAcctNo.length === 16) {
            initBankInfo.sordAcctNo = initBankInfo.sordAcctNo.substr(2, 14);
          }
        }
        main.initSordAccNo = initBankInfo.sordAcctNo;
      }

      main.kgiBankInfos = [];
      if (initBankInfo.kgiBankInfos) {
        for (const k of initBankInfo.kgiBankInfos) {
          if (k.kgiBankNo && k.kgiAcctNo) {
            // 20201116 調整 如果為本行 帳號取14碼
            if (k.kgiBankNo.substring(0, 3) === '809') {
              if (k.kgiAcctNo.length === 15) {
                k.kgiAcctNo = k.kgiAcctNo.substr(1, 14);
              } else if (k.kgiAcctNo.length === 16) {
                k.kgiAcctNo = k.kgiAcctNo.substr(2, 14);
              }
            }
            main.kgiBankInfos.push(k);
          }
        }
      }

      if (initBankInfo.payKindDay) {
        main.initPayKindDay = initBankInfo.payKindDay;
      }

      if (initBankInfo.payBank) {
        main.payBank = initBankInfo.payBank;
      }

      main.hasPayBankOption = initBankInfo.hasPayBankOption;
      main.hasSordBankOption = initBankInfo.hasSordBankOption;

      main.rate = initBankInfo.rate;
      main.aprove_period = initBankInfo.aprove_period;
      main.apy_fee = initBankInfo.apy_fee;
      main.b_range = initBankInfo.b_range;
      main.rate_desc = initBankInfo.rate_desc;
      main.p_prj_code = initBankInfo.p_prj_code;
      main.int_type = initBankInfo.int_type;
      main.p_now_rate = initBankInfo.p_now_rate;
      main.int_pre_1 = initBankInfo.int_pre_1;
      main.fix_rate1 = initBankInfo.fix_rate1;
      main.tbA_aprove_amount = initBankInfo.tbA_aprove_amount;
      main.session_id = initBankInfo.session_id;
      main.sum_rate = initBankInfo.sum_rate;
    } // end if
  } // end doInitBankInfo

  public doInitRateDatas(main: ContractMain, rateDatas: any): void {
    if (rateDatas) {
      main.rateDatas = rateDatas as RateData[];
    }
  }

  public get contractMain(): ContractMain {
    return this.getJSON('contractMain');
  }

  public set contractMain(data: ContractMain) {
    sessionStorage.setItem('contractMain', JSON.stringify(data));
  }

  public get uploadDocument(): UploadDocument {
    return this.getJSON('uploadDocument');
  }

  public set uploadDocument(uploadDocument: UploadDocument) {
    sessionStorage.setItem('uploadDocument', JSON.stringify(uploadDocument));
  }

  // public get getCifInfoResp(): GetCifInfoResp {
  //   return this.getJSON('getCifInfoResp');
  // }

  // public set getCifInfoResp(data: GetCifInfoResp) {
  //   sessionStorage.setItem('getCifInfoResp', JSON.stringify(data));
  // }

  // public get getOtpTelResp(): GetOtpTelResp {
  //   return this.getJSON('getOtpTelResp');
  // }

  // public set getOtpTelResp(data: GetOtpTelResp) {
  //   sessionStorage.setItem('getOtpTelResp', JSON.stringify(data));
  // }

  public get creditCardCifInfo(): CreditCardCifInfo {
    return this.getJSON('CreditCardCifInfo');
  }

  public set creditCardCifInfo(data: CreditCardCifInfo) {
    sessionStorage.setItem('CreditCardCifInfo', JSON.stringify(data));
  }

  public get chinaLifeCifInfo(): ChinaLifeCifInfo {
    return this.getJSON('chinaLifeCifInfo');
  }

  public set chinaLifeCifInfo(data: ChinaLifeCifInfo) {
    sessionStorage.setItem('chinaLifeCifInfo', JSON.stringify(data));
  }

  public get idCardFrontFillDataReq(): IDCardFrontFillDataReq {
    return this.getJSON('idCardFrontFillDataReq');
  }

  public set idCardFrontFillDataReq(
    idCardFrontFillDataReq: IDCardFrontFillDataReq
  ) {
    sessionStorage.setItem(
      'idCardFrontFillDataReq',
      JSON.stringify(idCardFrontFillDataReq)
    );
  }

  public get idCardBackFillDataReq(): IDCardBackFillDataReq {
    return this.getJSON('idCardBackFillDataReq');
  }

  public set idCardBackFillDataReq(
    idCardBackFillDataReq: IDCardBackFillDataReq
  ) {
    sessionStorage.setItem(
      'idCardBackFillDataReq',
      JSON.stringify(idCardBackFillDataReq)
    );
  }

  public get basicFillDataReq(): BasicFillDataReq {
    return this.getJSON('basicFillDataReq');
  }

  public set basicFillDataReq(basicFillDataReq: BasicFillDataReq) {
    sessionStorage.setItem(
      'basicFillDataReq',
      JSON.stringify(basicFillDataReq)
    );
  }

  public get corpFillDataReq(): CorpFillDataReq {
    return this.getJSON('corpFillDataReq');
  }

  public set corpFillDataReq(corpFillDataReq: CorpFillDataReq) {
    sessionStorage.setItem('corpFillDataReq', JSON.stringify(corpFillDataReq));
  }

  public get giftFillDataReq(): GiftFillDataReq {
    return this.getJSON('giftFillDataReq');
  }

  public set giftFillDataReq(giftFillDataReq: GiftFillDataReq) {
    sessionStorage.setItem('giftFillDataReq', JSON.stringify(giftFillDataReq));
  }

  //#endregion

  public get creditCardProductId(): string {
    return sessionStorage.getItem('creditCardProductId');
  }

  public set creditCardProductId(token: string) {
    sessionStorage.setItem('creditCardProductId', token);
  }

  public get token(): string {
    return sessionStorage.getItem('kgiToken');
  }

  public set token(token: string) {
    sessionStorage.setItem('kgiToken', token);
  }

  public get prevToken(): string {
    return sessionStorage.getItem('prevToken');
  }

  public set prevToken(token: string) {
    sessionStorage.setItem('prevToken', token);
  }

  public get breakPoint(): string {
    return sessionStorage.getItem('breakPoint');
  }

  public set breakPoint(token: string) {
    sessionStorage.setItem('breakPoint', token);
  }

  public get creditCard(): NCCCReqPlus {
    return this.getJSON('creditCardInfo');
  }

  public set creditCard(req: NCCCReqPlus) {
    sessionStorage.setItem(
      'creditCardInfo',
      JSON.stringify(req)
    );
  }

  // public get verifiedAccount(): PCODE2566Req {
  //   return this.getJSON('accountInfo');
  // }

  // public set verifiedAccount(req: PCODE2566Req) {
  //   sessionStorage.setItem(
  //     'accountInfo',
  //     JSON.stringify(req)
  //   );
  // }

  public get appointmentInfo(): AppointmentInfo {
    return this.getJSON('appointmentInfo');
  }

  public set appointmentInfo(req: AppointmentInfo) {
    sessionStorage.setItem(
      'appointmentInfo',
      JSON.stringify(req)
    );
  }

  public get verifyCount(): VerifyCountResp {
    return this.getJSON('verifyCount');
  }

  public set verifyCount(req: VerifyCountResp) {
    sessionStorage.setItem(
      'verifyCount',
      JSON.stringify(req)
    );
  }

  public get maxVerifyCount(): MaxVerifyCount {
    return this.getJSON('verifyCount');
  }

  public set maxVerifyCount(req: MaxVerifyCount) {
    sessionStorage.setItem(
      'verifyCount',
      JSON.stringify(req)
    );
  }

  public get plContractType(): string {
    return sessionStorage.getItem('plContractType');
  }

  public set plContractType(plContractType: string) {
    sessionStorage.setItem('plContractType', plContractType);
  }

  // 目前針對撥款日期超過 且是代償件 而跳出的錯誤訊息紀錄
  public get warnMsg(): string {
    return this.getJSON('warnMsg');
  }

  public set warnMsg(warnMsg: string) {
    sessionStorage.setItem('warnMsg', JSON.stringify(warnMsg));
  }

  public removeWarnMsg() {
    sessionStorage.removeItem('warnMsg');
  }

  constructor() {
  }

  /**
   * 取JSON
   * return any
   */
  private getJSON(key: string): any {
    return sessionStorage.getItem(key)
      ? JSON.parse(sessionStorage.getItem(key))
      : undefined;
  }
}
