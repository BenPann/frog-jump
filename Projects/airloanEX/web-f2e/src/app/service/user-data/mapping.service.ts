import { Injectable } from '@angular/core';
import { DropdownItem } from '../../interface/resultType';
import { zipcodeData } from 'src/app/mock/twzipcode';

@Injectable({
  providedIn: 'root'
})
export class MappingService {
  public idCardRecord: DropdownItem[] = [
    { dataKey: '1', dataName: '初領' },
    { dataKey: '2', dataName: '補領' },
    { dataKey: '3', dataName: '換發' }
  ];

  public marriage: DropdownItem[] = [
    { dataKey: 'M', dataName: '已婚' },
    { dataKey: 'S', dataName: '未婚' }
  ];

  public authorizeToallCorp: DropdownItem[] = [
    { dataKey: 'Y', dataName: '同意' },
    { dataKey: 'N', dataName: '不同意' }
  ];

  public injury: DropdownItem[] = [
    { dataKey: 'Y', dataName: '是' },
    { dataKey: 'N', dataName: '否' }
  ];

  public agreement: DropdownItem[] = [
    { dataKey: 'Y', dataName: '同意' },
    { dataKey: 'N', dataName: '不同意' }
  ];

  public docType: DropdownItem[] = [
    { dataKey: '20', dataName: '薪資轉帳證明' },
    { dataKey: '21', dataName: '土地謄本' },
    { dataKey: '22', dataName: '存款證明' },
    { dataKey: '23', dataName: '扣繳憑單' },
    { dataKey: '24', dataName: '所得清單' }
  ];

  constructor() {}

  findNameByKey(key: string, list: DropdownItem[]) {
    if (!key || !list) {
      return '';
    }
    const selected = list.find((value, index, obj) => {
      return value.dataKey === key.trim();
    });

    if (selected) {
      return selected.dataName;
    } else {
      console.warn(key);
      console.warn(list);
      return '';
    }
  }

  findKeyByName(name: string, list: DropdownItem[]) {
    if (!name || !list) {
      return '';
    }
    const selected = list.find((value, index, obj) => {
      return value.dataName === name.trim();
    });

    if (selected) {
      return selected.dataKey;
    } else {
      console.warn(name);
      console.warn(list);
      return '';
    }
  }

  findAddressByZipCode(zipCode: string) {
    if (!zipCode) {
      return '';
    }
    const data = {
      zipCode: '',
      district: '',
      county: ''
    };
    for (const county in zipcodeData) {
      if (zipcodeData.hasOwnProperty(county)) {
        const element = zipcodeData[county];
        for (const district in element) {
          if (element.hasOwnProperty(district)) {
            const element2 = element[district];
            if (element2 === zipCode) {
              data.county = county;
              data.district = district;
              data.zipCode = zipCode;
              break;
            }
          }
        }
      }
    }
    return data.county + data.district;
  }
}
