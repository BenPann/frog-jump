import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { filter, tap, map, switchMap } from 'rxjs/operators';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { ApplyAccountService } from 'src/app/service/apply-account/apply-account.service';
import { UserDataService } from '../user-data/user-data.service';
import { EntryService } from 'src/app/service/entry/entry.service';

@Injectable({
  providedIn: 'root'
})
export class RouterDirectService {

  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
    private entryService: EntryService,
    private activatedRoute: ActivatedRoute,
    private userDataService: UserDataService,
    private applyAccountService: ApplyAccountService
  ) {
    // router.events
    // .pipe(filter(event => event instanceof NavigationEnd))
    // .subscribe((event: NavigationEnd) => {
    //   console.log('prev:', event.url);
    // });
    /*router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        console.log(event.toString());
      } else if (event instanceof NavigationEnd) {
        console.log(event.toString());
      } else if (event instanceof NavigationCancel) {
        console.log(event.toString());
      } else if (event instanceof NavigationError) {
        console.log(event.toString());
      }
    });*/
    // console.log(activatedRoute);
  }
  // prevPage
  public get prevPage(): any {
    return this.getJSON('prevPage');
  }
  public set prevPage(prevPage: any) {
    this.setJSON('prevPage', prevPage);
  }
  // otpPrevPage
  public get otpPrevPage(): any {
    return this.getJSON('otpPrevPage');
  }
  public set otpPrevPage(otpPrevPage: any) {
    this.setJSON('otpPrevPage', otpPrevPage);
  }
  // otpNextPage
  public get otpNextPage(): any {
    return this.getJSON('otpNextPage');
  }
  public set otpNextPage(otpNextPage: any) {
    this.setJSON('otpNextPage', otpNextPage);
  }
  // otpPhoneNum
  public get otpPhoneNum(): any {
    return this.getJSON('otpPhoneNum');
  }
  public set otpPhoneNum(otpPhoneNum: any) {
    this.setJSON('otpPhoneNum', otpPhoneNum);
  }
  // passID
  public get passID(): string {
    return sessionStorage.getItem('passIdentification');
  }
  public set passID(answer: string) {
    sessionStorage.setItem('passIdentification', answer);
  }
  // passOTP
  public get passOTP(): string {
    return sessionStorage.getItem('passOTP');
  }
  public set passOTP(answer: string) {
    sessionStorage.setItem('passOTP', answer);
  }
  /**
   * 取JSON
   * return any
   */
  private getJSON(key: string): any {
    return sessionStorage.getItem(key)
      ? JSON.parse(sessionStorage.getItem(key))
      : undefined;
  }
    /**
   * 存JSON
   * return any
   */
  private setJSON(key: string, value: any) {
    sessionStorage.setItem(key, JSON.stringify(value));
  }
}
