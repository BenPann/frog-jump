import { TestBed } from '@angular/core/testing';

import { ApplyAccountService } from './apply-account.service';

describe('ApplyAccountService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApplyAccountService = TestBed.get(ApplyAccountService);
    expect(service).toBeTruthy();
  });
});
