import {Injectable} from '@angular/core';
import {WebResult} from 'src/app/interface/resultType';
import {HttpClient} from '@angular/common/http';
import {
  AuthInfo,
  BasicFillDataReq,
  ConfirmDataReq,
  CorpFillDataReq,
  GiftFillDataReq,
  IDCardBackFillDataReq,
  IDCardFrontFillDataReq
} from './apply-account.interface';

@Injectable({
  providedIn: 'root'
})
export class ApplyAccountService {
  constructor(
    private httpClient: HttpClient
  ) {
  }

  getAccountList() {
    return this.httpClient.get<WebResult>('../api/apply/getAccountList');
  }

  setAccount(account: string) {
    const req = {
      account: account
    };
    return this.httpClient.post<WebResult>(
      '../api/apply/setAccount',
      req
    );
  }

  setIDCardFrontData(idCardFrontDataReq: IDCardFrontFillDataReq) {
    return this.httpClient.post<WebResult>(
      '../api/apply/setIDCardFrontData',
      idCardFrontDataReq
    );
  }

  setIDCardBackData(idCardBackDataReq: IDCardBackFillDataReq) {
    return this.httpClient.post<WebResult>(
      '../api/apply/setIDCardBackData',
      idCardBackDataReq
    );
  }

  setBasicData(basicData: BasicFillDataReq) {
    return this.httpClient.post<WebResult>(
      '../api/apply/setBasicData',
      basicData
    );
  }

  setCorpData(corpData: CorpFillDataReq) {
    return this.httpClient.post<WebResult>(
      '../api/apply/setCorpData',
      corpData
    );
  }

  setGiftData(giftData: GiftFillDataReq) {
    return this.httpClient.post<WebResult>(
      '../api/apply/setGiftData',
      giftData
    );
  }

  getCreditCardList() {
    return this.httpClient.get<WebResult>('../api/apply/creditcard');
  }

  chooseCardView(creditProductId: string) {
    return this.httpClient.post<WebResult>(
      '../api/apply/creditcard',
      {
        creditProductId: creditProductId
      }
    );
  }


  getGiftList() {
    return this.httpClient.get<WebResult>('../api/apply/getGiftList');
  }

  creditCardFinish() {
    return this.httpClient.post<WebResult>('../api/apply/setGiftData', {});
  }

  getConfirmData() {
    return this.httpClient.get<WebResult>('../api/apply/confirmData');
  }

  setConfirmData(req: ConfirmDataReq) {
    return this.httpClient.post<WebResult>('../api/apply/confirmData', req);
  }

  caseFinish() {
    return this.httpClient.get<WebResult>('../api/apply/finish');
  }

  getEOPProduct() {
    return this.httpClient.post<WebResult>('../api/apply/getEOPProduct', {});
  }

  getAuthInfo(termName: string) {
    return this.httpClient.post<WebResult>('../api/apply/getAuthInfo', {termName: termName});
  }

  updateAuthInfo(authInfo: AuthInfo) {
    return this.httpClient.post<WebResult>(
      '../api/apply/updateAuthInfo',
      authInfo
    );
  }

  getUploadDocument() {
    return this.httpClient.get<WebResult>('../api/apply/getUploadDocument');
  }

  getNewUserProduct() {
    return this.httpClient.get<WebResult>('../api/apply/getNewUserProduct');
  }

  getCCProductId() {
    return this.httpClient.get<WebResult>('../api/credit/getProductId');
  }

  setBreakPointPage(pageCode: string) {
    return this.httpClient.post<WebResult>('../api/apply/breakPointPage', {
      pageName: pageCode
    });
  }

  getBreakPointPage() {
    return this.httpClient.get<WebResult>('../api/apply/breakPointPage');
  }

  // 是否接續斷點
  setContinue(req: string) {
    return this.httpClient.post<WebResult>(
      '../api/apply/setContinue',
      { isContinue: req }
    );
  }

  // 中壽投保客戶登入後，才須Call此API
  getCLCif(req: string) {
    return this.httpClient.post<WebResult>(
      '../api/CL/getCLCif',
      { token: req }
    );
  }

  // 中壽投保通知
  isCLNotify() {
    return this.httpClient.post<WebResult>(
      '../api/apply/isCLNotify',
      {}
    );
  }

  // 中壽投保案件換成數三案件
  changeCLToD3() {
    return this.httpClient.get<WebResult>('../api/CL/changeCLToD3');
  }

  cleanData(birthday: string) {
    return this.httpClient.post<WebResult>(
      '../api/apply/cleanData',
      {
        birthday: birthday
      }
    );
  }


}
