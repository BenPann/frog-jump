export interface UserAccount {
  account: string;
  branchName: string;
}

export interface PhotoView {
  sType: string;
  base64Image: string | ArrayBuffer;
  subSerial?: string;
}

export interface IDCardFrontFillDataReq {
  chtName: string;
  engName: string;
  idCardDate: string;
  idCardLocation: string;
  idCardRecord: string;
}

export interface IDCardBackFillDataReq {
  resAddrZipCode: string;
  resAddrArea: string;
  resAddr: string;
  commAddrZipCode: string;
  commAddrArea: string;
  commAddr: string;
  marriage: string;
  checkTerms: string;
  birthPlace: string;
}

export interface BasicFillDataReq {
  emailAddress: string;
  sendType: string;
  resTelArea: string;
  resTel: string;
  homeTelArea: string;
  homeTel: string;
  education: string;
  estateType: string;
  purpose: string;
  authorizeToallCorp: string;
  autVer: string;
  branchID: string;
  checkTerms: string;
}

export interface CorpFillDataReq {
  corpName: string;
  corpTelArea: string;
  corpTel: string;
  corpAddrZipCode: string;
  corpAddrArea: string;
  corpAddr: string;
  occupation: string;
  jobTitle: string;
  yearlyIncome: string;
  onBoardDate: string;
  injury: string;
  agreement: string;
}

export interface GiftFillDataReq {
  firstPresent: string;
}

export interface ChooseCard {
  creditProductId: string;
}

export interface CreditCardView {
  cardSerial: number;
  productId: string;
  cardTitle: string;
  cardImage: string;
  cardContent: string;
}

export interface GiftData {
  giftTitle: string;
  giftCode: string;
  giftImage: string;
  giftNote: string;
}

export interface ConfirmDataReq {
  idCardFrontData: IDCardFrontFillDataReq;
  idCardBackData: IDCardBackFillDataReq;
  basicData: BasicFillDataReq;
  corpData: CorpFillDataReq;
  giftData: GiftFillDataReq;
  chooseCard: ChooseCard;
}

export interface ChinaLifeCifInfo {
  stsCode: string;
  stsDesc: string;
  token: string;
  tokenEndDt: string;
  chtName: string;
  pyName: string;
  engName: string;
  email: string;
  marriage: string;
  mobNo: string;
  homeTelArea: string;
  homeTel: string;
  resAddrZipCode: string;
  resAddrArea: string;
  resAddr: string;
  comAddrZipCode: string;
  comAddrArea: string;
  comAddr: string;
}

export interface CreditCardCifInfo {
  sourceType: string;
  chtName: string;
  engName: string;
  resAddrZipCode: string;
  resAddr: string;
  commAddrZipCode: string;
  commAddr: string;
  marriage: string;
  email: string;
  resTelArea: string;
  resTel: string;
  homeTelArea: string;
  homeTel: string;
  education: string;
  estateType: string;
  corpName: string;
  corpTelArea: string;
  corpTel: string;
  corpAddrZipCode: string;
  corpAddr: string;
  jobTtitle: string;
  yearlyIncome: string;
  onBoardDate: string;
}


export interface AuthInfo {
  authorizeToallCorp: string;
  verNo: string;
}

export interface BcOcrRes {
  CompanyName: string;
  CompanyTelNum1: string;
  CompanyTelNum2: string;
  CompanyAddr3: string;
  ZIPCODE: string;
}
