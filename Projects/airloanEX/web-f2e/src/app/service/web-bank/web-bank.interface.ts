export interface WebBankResponse {
  hasWebBank: string;
  hasIvrBank: string;
}
