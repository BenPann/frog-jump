import { TestBed } from '@angular/core/testing';

import { WebBankService } from './web-bank.service';

describe('WebBankService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WebBankService = TestBed.get(WebBankService);
    expect(service).toBeTruthy();
  });
});
