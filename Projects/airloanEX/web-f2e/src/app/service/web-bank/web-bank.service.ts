import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebResult } from 'src/app/interface/resultType';

@Injectable({
  providedIn: 'root'
})
export class WebBankService {
  constructor(private httpClient: HttpClient) {}

  getWebBank() {
    return this.httpClient.get<WebResult>('../api/apply/webBank');
  }

  // setWebBank(webBankFlag: string, ivrBankFlag: string) {
  //   return this.httpClient.post<WebResult>(
  //     '../api/apply/webBank',
  //     {
  //       webBankFlag: webBankFlag,
  //       ivrBankFlag: ivrBankFlag
  //     }
  //   );
  // }

  getWebBankKey() {
    return this.httpClient.get<WebResult>('../api/apply/getWebBankKey');
  }

  setWebBank(bankUser: string, bankPwd: string, ivrBankpwd: string) {
    return this.httpClient.post<WebResult>(
      '../api/apply/webBank',
      {
        bankUser: bankUser,
        bankpwd: bankPwd,
        ivrBankpwd: ivrBankpwd
      }
    );
  }
}
