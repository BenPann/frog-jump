import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebResult } from 'src/app/interface/resultType';

@Injectable({
  providedIn: 'root'
})
export class LoanService {
  constructor(private httpClient: HttpClient) {}

  setLoanType(houseLoan: string, airLoan: string) {
    return this.httpClient.post<WebResult>(
      '../api/apply/setLoanType/LoanTypeView',
      {
        houseLoan: houseLoan,
        airLoan: airLoan
      }
    );
  }

  setLoan(airLoanType: string) {
    return this.httpClient.post<WebResult>('../api/apply/setLoan/LoanView', {
      airLoanType: airLoanType
    });
  }
}
