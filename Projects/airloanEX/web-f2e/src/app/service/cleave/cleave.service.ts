import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CleaveService {

  constructor() { }

  Date = {
    date: true,
    datePattern: ['Y', 'm', 'd'],
  };

  Upper = {
    uppercase: true,
    blocks: [9999],
  };

  Number = {
    numericOnly: true,
    blocks: [9999],
  };

  Money = {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand'
  };
}
