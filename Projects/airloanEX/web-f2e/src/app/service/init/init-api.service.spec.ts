import { TestBed } from '@angular/core/testing';

import { InitApiService } from './init-api.service';

describe('InitApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InitApiService = TestBed.get(InitApiService);
    expect(service).toBeTruthy();
  });
});
