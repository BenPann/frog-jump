import { InfoPlContract } from './../../component/init/init.interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebResult } from '../../interface/resultType';
import { Observable } from 'rxjs';
import { InitDataRequest, ChosenPlContract, AgreePlContract } from '../../component/init/init.interface';
import {filter, map} from 'rxjs/operators';
// import { request } from 'https';

@Injectable({
  providedIn: 'root'
})
export class InitApiService {
  constructor(private httpClient: HttpClient) {}

  /** 1 PL */
  isPL(productId: string) {
    return productId === '1';
  }

  /** 3 RPL */
  isRPL(productId: string) {
    return productId === '3';
  }

  /** 2 CASHCARD */
  isCASHCARD(productId: string) {
    return productId === '2';
  }

  /** PL/RPL/CASHCARD */
  makeProductType(productId: string): string{
    let productType;
    if (productId && this.isPL(productId)) {
      productType = 'PL';
    } else if (productId && this.isRPL(productId)) {
      productType = 'RPL';
    } else if (productId && this.isCASHCARD(productId)) {
      productType = 'GM';
    } else {
      productType = 'PL';
    }
    return productType;
  }

  doLogin(req: InitDataRequest): Observable<WebResult> {
    console.log("doLogin(req: InitDataRequest),InitDataRequest = ",req);
    return this.httpClient.post<WebResult>('../api/stp/initPlContract', req);
  }

  initPlPayBank(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/initPlPayBank', {});
  }

  // getUserType(): Observable<WebResult> {
  //   return this.httpClient.get<WebResult>('../api/apply/getUserType');
  // }

  prepareDgtConst(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/prepareDgtConst', {});
  }

  chosenDgtConst(req: ChosenPlContract): Observable<WebResult> {
    console.log("chosenDgtConst(req: ChosenPlContract),ChosenPlContract = ",req);
    return this.httpClient.post<WebResult>('../api/stp/chosenDgtConst', req);
  }

  previewContract(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/contract/previewContract', {});
  }

  uploadContract(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/contract/uploadContract', {});
  }

  agreePlContract(req: AgreePlContract): Observable<WebResult> {
    console.log("agreePlContract(req: AgreePlContract),AgreePlContract = ",req);
    return this.httpClient.post<WebResult>('../api/stp/agreePlContract', req);
  }

  judgeExceedPayDate(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/judgeExceedPayDate', {});
  }

  judgeExceedPayDateByExpBank(expBankCode: string, expBankAcctNo: string): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/judgeExceedPayDateByExpBank', {expBankCode, expBankAcctNo});
  }

  infoPlContract(req: InfoPlContract): Observable<WebResult> {
    console.log("infoPlContract(req: InfoPlContract),InfoPlContract = ",req);
    return this.httpClient.post<WebResult>('../api/stp/infoPlContract', req);
  }

  updateDgtConst(productId?: string): Observable<WebResult> {
    console.log("updateDgtConst(productId),productId = "+productId);
    return this.httpClient.post<WebResult>('../api/stp/updateDgtConst', productId);
  }

  agreeDownloadPdf(productId?: string): Observable<WebResult> {
    console.log("agreeDownloadPdf(productId?: string),productId = "+productId);
    return this.httpClient.post<WebResult>('../api/stp/agreeDownloadPdf', productId);
  }

  getTrigger(productType: string, flowType: string, pageName: string, subPageName?: string): Observable<WebResult> {
    return this.httpClient.post<any>('../publicApi/chatbot/trigger', {productType, flowType, pageName, subPageName});
  }
}
