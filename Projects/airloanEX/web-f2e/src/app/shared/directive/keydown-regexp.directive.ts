import {
  Directive,
  Input,
  ElementRef,
  HostListener,
  OnInit
} from '@angular/core';
import { AbstractControl, NgControl } from '@angular/forms';

@Directive({
  selector: '[KeydownRegexp]'
})
export class KeydownRegexpDirective implements OnInit {
  // ref : http://www.pribluda.de/articles/numberinput/
  // 保留按鈕
  private specialKeys: Array<string> = [
    'Backspace',
    'Tab',
    'End',
    'Home',
    'Clear',
    'Copy',
    'Control',
    'Delete',
    'Shift',
    'ArrowLeft',
    'ArrowRight'
  ];

  //  externally configured regex - for numbers shall be:  /^[0-9]+(\.[0-9]*){0,1}$/g
  @Input() KeydownRegexp: 'num' | 'eng' | 'numeng' | 'engName' | 'cht' | 'addr' | 'numStar';

  private numberReg = '[0-9]';
  private numberReplaceReg = '[^0-9]';
  private englishReg = '[A-Za-z]';
  private englishReplaceReg = '[^A-Za-z]';
  private engNameReg = '[A-Za-z ,.-]';
  private engNameReplaceReg = '[^\\sA-Za-z ,.-]';
  private numEngReg = '[0-9A-Za-z]';
  private numEngReplaceReg = '[^0-9A-Za-z]';
  private chtNameReg = '[\u4e00-\u9fa5\\sa-zA-Z]';
  private chtNameReplaceReg = '[^\u4e00-\u9fa5\\sa-zA-Z]';
  private addrReg = '[\u4e00-\u9fa5a-zA-Z0-9\-]';
  private addrReplaceReg = '[^\u4e00-\u9fa5a-zA-Z0-9\-]';
  private numStarReg = '[0-9, .*]';
  private numStarReplaceReg = '[^0-9, .*]';

  private regex: RegExp;
  private replaceRegex: RegExp;

  constructor(private control: NgControl, private el: ElementRef) {}

  // initialise regex here, input parameters are not yet set while construction
  ngOnInit() {
    switch (this.KeydownRegexp) {
      case 'eng':
        this.regex = new RegExp(this.englishReg, 'g');
        this.replaceRegex = new RegExp(this.englishReplaceReg, 'g');
        break;
      case 'engName':
        this.regex = new RegExp(this.engNameReg, 'g');
        this.replaceRegex = new RegExp(this.engNameReplaceReg, 'g');
        break;
      case 'num':
        this.regex = new RegExp(this.numberReg, 'g');
        this.replaceRegex = new RegExp(this.numberReplaceReg, 'g');
        break;
      case 'numeng':
        this.regex = new RegExp(this.numEngReg, 'g');
        this.replaceRegex = new RegExp(this.numEngReplaceReg, 'g');
        break;
      case 'cht':
        this.regex = new RegExp(this.chtNameReg, 'g');
        this.replaceRegex = new RegExp(this.chtNameReplaceReg, 'g');
        break;
      case 'addr':
        this.regex = new RegExp(this.addrReg, 'g');
        this.replaceRegex = new RegExp(this.addrReplaceReg, 'g');
        break;
      case 'numStar':
        this.regex = new RegExp(this.numStarReg, 'g');
        this.replaceRegex = new RegExp(this.numStarReplaceReg, 'g');
        break;
    }
  }

  @HostListener('keydown', ['$event']) onKeyDown(event: KeyboardEvent) {
    if (
      this.specialKeys.indexOf(event.key) !== -1 ||
      event.ctrlKey ||
      event.altKey ||
      event.metaKey
    ) {
      return;
    }

    // current string in element
    const current: string = this.el.nativeElement.value;
    const next: string =
      current.substring(0, this.el.nativeElement.selectionStart) +
      event.key +
      current.substring(this.el.nativeElement.selectionEnd);

    //  if it does not match, we do not let it pass
    if (next && !String(next).match(this.regex)) {
      event.preventDefault();
    }
  }

  @HostListener('keyup', ['$event']) onkeyup(event: KeyboardEvent) {
    const formGroup: AbstractControl = this.control.control;
    const reg = '^ *';
    const value = this.el.nativeElement.value.replace(this.replaceRegex, '').replace('\\s', '').replace(new RegExp(reg, 'g'), '');
    if (formGroup) {
      formGroup.setValue(value);
    }
    this.el.nativeElement.value = value;
  }

  @HostListener('focusout', ['$event']) onfocusout(event: FocusEvent) {
    const formGroup: AbstractControl = this.control.control;
    const reg = '^ *';
    const value = this.el.nativeElement.value.replace(this.replaceRegex, '').replace('\\s', '').replace(new RegExp(reg, 'g'), '');
    if (formGroup) {
      formGroup.setValue(value);
    }
    this.el.nativeElement.value = value;
  }
}
