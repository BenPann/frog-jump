import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'hiddenStr'
})
export class HiddenStrPipe implements PipeTransform {
  transform(value: string, args?: any): any {
    console.log(value);
    console.log(args);
    const startIndex = args.start || 0;
    const length = args.length || 1;
    return value.substring(0, startIndex + 1) + '*'.padStart(length, '*') + value.substring(startIndex + length + 1);
    // return value.replace(
    //   value.substr(startIndex, length),
    //   '*'.padStart(length, '*')
    // );
    // 0910910123
    // var hiddenLength = 3;
    // var startChar = 0;
    // var value = '0912341232';
    // var endChar =
    //   startChar + hiddenLength > value.length
    //     ? value.length
    //     : startChar + hiddenLength;
    // return value.replace(
    //   value.substring(startChar, endChar),
    //   '*'.padStart(hiddenLength, '*')
    // );
  }
}
