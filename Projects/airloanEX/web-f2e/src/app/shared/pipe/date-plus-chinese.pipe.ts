import { Pipe, PipeTransform } from '@angular/core';
import {
  NgbDateStruct,
  NgbDateParserFormatter
} from '@ng-bootstrap/ng-bootstrap';

@Pipe({
  name: 'datePlusChinese'
})
export class DatePlusChinesePipe implements PipeTransform {
  constructor(private dateFormatter: NgbDateParserFormatter) {}
  transform(value: NgbDateStruct, ...args: any[]): any {
    if (value) {
      return `${this.dateFormatter.format(value)} 民國(${+value.year -
        1911}年)`;
    } else {
      return '';
    }
  }
}
