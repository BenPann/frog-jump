import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addr'
})
export class AddrPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    if (value) {
      const addrArr = value.split(';');
      return addrArr.length > 1 ? addrArr[1] : addrArr[0];
    } else {
      return '';
    }
  }
}
