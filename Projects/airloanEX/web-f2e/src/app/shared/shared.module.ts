import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TwZipCode2Component } from './component/tw-zip-code2/tw-zip-code2.component';
import { TwZipCodeComponent } from './component/tw-zip-code/tw-zip-code.component';
import { BankListComponent } from './component/bank-list/bank-list.component';
import { BankList2Component } from './component/bank-list2/bank-list2.component';
import { UploadComponent } from './component/upload/upload.component';
import { UploadIdCardComponent } from './component/upload-id-card/upload-id-card.component';
import { TwPhoneComponent } from './component/tw-phone/tw-phone.component';
import { StepNumComponent } from './component/step-num/step-num.component';
import { StepNumPlContractComponent } from './component/step-num-pl-contract/step-num-pl-contract.component';
import { SetOnlineBankComponent } from './component/set-online-bank/set-online-bank.component';
import { CcProveComponent } from './component/cc-prove/cc-prove.component';
import { CcChooseComponent } from './component/cc-choose/cc-choose.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UploadCropperComponent } from './component/upload-cropper/upload-cropper.component';
import { ShowMessageComponent } from './component/show-message/show-message.component';
import { SpinnerOverlayComponent } from './component/spinner-overlay/spinner-overlay.component';
import { SecurePipe } from './pipe/secure.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TermComponent } from './component/term/term.component';
import { HiddenStrPipe } from './pipe/hidden-str.pipe';
import { CommonDataService } from './service/common/common-data.service';
import { DatepickerService } from './service/common/datepicker.service';
import { FormCheckService } from './service/common/form-check.service';
import { ModalFactoryService } from './service/common/modal-factory.service';
import { RouterHelperService } from './service/common/router-helper.service';
import { SpinnerOverlayService } from './service/common/spinner-overlay.service';
import { FillDataStepBarComponent } from './component/fill-data-step-bar/fill-data-step-bar.component';
import { RouterModule } from '@angular/router';
import { SimpleRootComponent } from './component/simple-root/simple-root.component';
import { ShowImageComponent } from './component/show-image/show-image.component';
import { FillData1Component } from './component/fill-data1/fill-data1.component';
import { FillData2Component } from './component/fill-data2/fill-data2.component';
import { FillData3Component } from './component/fill-data3/fill-data3.component';
import { FillData4Component } from './component/fill-data4/fill-data4.component';
import { FillData5Component } from './component/fill-data5/fill-data5.component';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Term2Component } from './component/term2/term2.component';
import { SubmitButtonGroupComponent } from './component/submit-button-group/submit-button-group.component';
import { ShowHtmlComponent } from './component/show-html/show-html.component';
import { SafePipe } from './pipe/safe.pipe';
import { KeydownRegexpDirective } from './directive/keydown-regexp.directive';
import { CropperComponent } from './component/cropper/cropper.component';
import { AddrPipe } from './pipe/addr.pipe';
import { DatePlusChinesePipe } from './pipe/date-plus-chinese.pipe';
import { DropdownFormComponent } from './component/dropdown-form/dropdown-form.component';
import { BankList3Component } from './component/bank-list3/bank-list3/bank-list3.component';
import { NgxCleaveDirectiveModule } from 'ngx-cleave-directive';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};

const entryComp = [
  ShowMessageComponent,
  SpinnerOverlayComponent,
  UploadCropperComponent,
  ShowImageComponent,
  ShowHtmlComponent
];

const exportComp = [
  TwZipCodeComponent,
  TwPhoneComponent,
  BankListComponent,
  UploadComponent,
  StepNumComponent,
  StepNumPlContractComponent,
  SetOnlineBankComponent,
  CcProveComponent,
  CcChooseComponent,
  UploadIdCardComponent,
  TwZipCode2Component,
  BankList2Component,
  BankList3Component,
  TermComponent,
  Term2Component,
  FillDataStepBarComponent,
  SimpleRootComponent,
  FillData1Component,
  FillData2Component,
  FillData3Component,
  FillData4Component,
  FillData5Component,
  SubmitButtonGroupComponent,
  CropperComponent,
  DropdownFormComponent,
];

const exportPipe = [
  SecurePipe,
  HiddenStrPipe,
  SafePipe,
  AddrPipe,
  DatePlusChinesePipe
];
const exportDirective = [KeydownRegexpDirective];

const service = [
  CommonDataService,
  DatepickerService,
  FormCheckService,
  ModalFactoryService,
  RouterHelperService,
  SpinnerOverlayService
];

@NgModule({
  declarations: [
    ...entryComp,
    ...exportComp,
    ...exportPipe,
    ...exportDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule,
    SwiperModule,
    NgxCleaveDirectiveModule
  ],
  exports: [
    ...entryComp,
    ...exportComp,
    ...exportPipe,
    ...exportDirective,
    NgbModule,
    NgxCleaveDirectiveModule
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
export class SharedModule {}
