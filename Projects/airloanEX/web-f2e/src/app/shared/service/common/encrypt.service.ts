import { Injectable } from '@angular/core';


declare var CGJSCrypt: any;
declare var UTIL: any;

@Injectable({
  providedIn: 'root'
})
export class EncryptService {
  constructor() {}

  encrypt(publicKey: string, val: string) {
    // 日期格式YYYY-MM-DD HH24:MI:SS
    // 範例:2014-07-10 15:37:00
    const now = new Date();
    const str_year = now.getFullYear();
    const str_month =
      ('' + (now.getMonth() + 1)).length === 1
        ? '0' + (now.getMonth() + 1)
        : '' + (now.getMonth() + 1);
    const str_date =
      ('' + now.getDate()).length === 1
        ? '0' + now.getDate()
        : '' + now.getDate();
    const str_hours =
      ('' + now.getHours()).length === 1
        ? '0' + now.getHours()
        : '' + now.getHours();
    const str_minutes =
      ('' + now.getMinutes()).length === 1
        ? '0' + now.getMinutes()
        : '' + now.getMinutes();
    const str_seconds =
      ('' + now.getSeconds()).length === 1
        ? '0' + now.getSeconds()
        : '' + now.getSeconds();
    const str_yyyymmdd_hh24miss =
      str_year +
      '-' +
      str_month +
      '-' +
      str_date +
      ' ' +
      str_hours +
      ':' +
      str_minutes +
      ':' +
      str_seconds;

    // 網銀登入-使用者代碼加密
    const base64_usno = CGJSCrypt.CertEncrypt(
      publicKey,
      val + str_yyyymmdd_hh24miss,
      0
    );
    const obj_usno = CGJSCrypt.DecodeBase64(base64_usno);
    return UTIL.bytesToHex(obj_usno);
  }
}
