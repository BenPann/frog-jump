import { Injectable } from '@angular/core';
import { NgbModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

// class or interface
import { DatepickerSetting } from 'src/app/interface/datepickerSetting';

@Injectable({
  providedIn: 'root'
})
export class DatepickerService {
  constructor(private modalService: NgbModal) {}

  /**
   * 取得重組的日期字串，格式為yyyy-mm-dd
   *
   * @param {DatepickerSetting} dataObj -ngbDatapicker日期物件
   * @returns {string}
   * @memberof DatepickerService
   */
  getDataStr(dataObj: DatepickerSetting): string {
    if (!dataObj) {
      return;
    }

    let m = '';
    let d = '';

    if (dataObj.month < 10) {
      m = '0' + dataObj.month;
    } else {
      m = '' + dataObj.month;
    }

    if (dataObj.day < 10) {
      d = '0' + dataObj.day;
    } else {
      d = '' + dataObj.day;
    }

    const dateStr = dataObj.year + '-' + m + '-' + d;
    return dateStr;
  }

  /**
   * 檢核生日是否不足20歲
   *
   * @param {DatepickerSetting} dateObj -ngbDatapicker日期物件
   * @returns {boolean}
   * @memberof DatepickerService
   */
  checkBirthday(dateObj: NgbDateStruct): boolean {
    if (!dateObj) {
      return;
    }
    const date = new Date();

    // 今天日期減20年
    date.setFullYear(date.getFullYear() - 20);

    const validate = new Date(date).getTime();
    const birthday = new Date(
      dateObj.year,
      dateObj.month - 1,
      dateObj.day
    ).getTime();
    return validate > birthday;
  }

  /**
   * 檢核年齡是否不足70歲
   *
   * @param {DatepickerSetting} dateObj -ngbDatapicker日期物件
   * @returns {boolean}
   * @memberof DatepickerService
   */
  checkAge(dateObj: NgbDateStruct, age: number): boolean {
    if (!dateObj) {
      return;
    }
    const date = new Date();

    // 今天日期減70年
    date.setFullYear(date.getFullYear() - age);

    const validate = new Date(date).getTime();
    const birthday = new Date(
      dateObj.year,
      dateObj.month - 1,
      dateObj.day
    ).getTime();
    return validate > birthday;
  }

  /**
   * 日期比較
   *
   * @param {DatepickerSetting} dateObj -ngbDatapicker日期物件
   * @returns {boolean}
   * @memberof DatepickerService
   */
  checkDate(dateObj: NgbDateStruct): boolean {
    if (!dateObj) {
      return;
    }
    const date = new Date();

    const validate = date.getTime();
    const birthday = new Date(
      dateObj.year,
      dateObj.month - 1,
      dateObj.day + 1
    ).getTime() - 1;
    return validate > birthday;
  }
}
