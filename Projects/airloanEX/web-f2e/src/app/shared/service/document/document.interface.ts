// export interface IDCardFrontResponse {
//   waterMarkImage: string;
//   name: string;
//   idCard_Issue_DT: string;
//   idCard_Issue_Type: string;
//   idCard_Issue_City: string;
//   subSerial: string;
// }
//
// export interface IDCardBackResponse {
//   waterMarkImage: string;
//   householdAddr1: string;
//   householdAddr2: string;
//   householdAddr3: string;
//   householdAddr4: string;
//   householdZipCode: string;
//   marriage: string;
//   subSerial: string;
// }

export interface IDCardResponse {
  waterMarkImage: string;
  name: string;
  idCard_Issue_DT: string;
  idCard_Issue_Type: string;
  idCard_Issue_City: string;
  householdAddr1: string;
  householdAddr2: string;
  householdAddr3: string;
  householdAddr4: string;
  householdZipCode: string;
  marriage: string;
  subSerial: string;
}

export interface OtherDocumentResponse {
  waterMarkImage: string;
  subSerial: string;
  sType?: string;
}
