import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ViewChild,
  ElementRef
} from '@angular/core';
import {UploadCropperComponent} from '../upload-cropper/upload-cropper.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {
  FormControl,
  FormGroup,
  AbstractControl,
  Validators
} from '@angular/forms';
import {DocumentResult} from 'src/app/interface/resultType';
import {ModalFactoryService} from 'src/app/shared/service/common/modal-factory.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit, OnChanges {
  @Input() doc: DocumentResult;
  @Input() docNowIndex: any;
  @Output() docResult: EventEmitter<DocumentResult> = new EventEmitter();

  // 畫面上還未上傳圖片時，要顯示的img路徑
  @Input() imgName: String;
  @Input() showCamera = '';
  @ViewChild('fileInput', {static: false}) el: ElementRef;
  @ViewChild('fileInput2', {static: false}) fileInput2: ElementRef;

  public idcardfront: string;
  public idcardback: string;
  public uploadForm: FormGroup;
  public webType;
  public mainData;
  public controls: {
    [key: string]: AbstractControl;
  } = {};

  constructor(
    private modalService: NgbModal,
    private modalFactoryService: ModalFactoryService,
  ) {
  }

  ngOnInit() {
    const defaultDoc: DocumentResult = {
      name: 'default',
      image: '',
      title: ''
    };
    this.doc = Object.assign(defaultDoc, this.doc);
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  triggerUpload(showCamera?: boolean) {
    if (showCamera) {
      this.fileInput2.nativeElement.click();
    } else {
      this.el.nativeElement.click();
    }
  }

  fileUpload(event) {
    // console.log(doc);
    // const input = showCamera ? document.getElementById(doc.name + '2') : document.getElementById(doc.name);
    const input = event.target;
    if (input.files[0]) {
      const _ifJpgPngMatch = input.files[0].type.match('jpeg|png') !== null;
      if (!_ifJpgPngMatch) {
        this.modalFactoryService.modalDefault({
          title: '檔案名稱不符合',
          message: '上傳檔案以JPG、PNG為限'
        });
        return;
      }
      const modalRef = this.modalService.open(UploadCropperComponent, {
        centered: true
      });
      modalRef.componentInstance.inputFile = input.files[0];
      // console.log(doc);
      modalRef.result.then(
        result => {
          if (result) {
            console.log(result.length);
            if (result.length > 6291456) {
              this.modalFactoryService.modalDefault({
                title: '檔案大小不符',
                message: '上傳檔案大小以6MB為限'
              });
              this.el.nativeElement.value = '';
              return;
            }
            this.doc.image = result;
            this.docResult.emit(this.doc);
          }
          this.el.nativeElement.value = '';
        },
        reason => {
          console.log(reason);
          this.el.nativeElement.value = '';
        }
      );
    }

  }
}
