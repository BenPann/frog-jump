import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormGroup} from '@angular/forms';
import {takeWhile} from 'rxjs/operators';
import {FormCheckService} from 'src/app/shared/service/common/form-check.service';
import {ProductService} from '../../../service/product/product.service';
import {SecuritiesInfo} from 'src/app/service/product/product.interface';

@Component({
  selector: 'app-bank-list2',
  templateUrl: './bank-list2.component.html',
  styleUrls: ['./bank-list2.component.scss']
})
export class BankList2Component implements OnInit, OnDestroy {
  // 所有資料列表
  public countyList: string[];
  // 區域列表
  public districtList: string[];
  // 分行列表
  public branchs: string[];

  // 父元件的表單
  @Input()
  public parentform: FormGroup;

  // 傳進的區域
  @Input()
  public securities: SecuritiesInfo;

  // 傳進的分行名
  @Input()
  public bankNameId: string;

  securitiesInfo: SecuritiesInfo[];
  private setDefaultData: boolean;
  private alive = true;

  constructor(
    private formCheckService: FormCheckService,
    private productService: ProductService
  ) {
  }

  ngOnInit() {
    this.initEvent();
    // 取資料
    this.productService
      .getSecuritiesInfo()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.securitiesInfo = data.result;
          // 取得清單內 所有的地區

          this.countyList = this.securitiesInfo
            .map(value => {
              return value.securitiesCity;
            })
            .filter((value, index, self) => {
              return self.indexOf(value) === index;
            });
          if (this.securities && this.securities.securitiesCode) {
            this.setDefaultData = true;

            const security = this.securitiesInfo.find(f => f.securitiesCode === this.securities.securitiesCode);

            this.parentform
              .get('securitiesCity')
              .setValue(security.securitiesCity);
            this.parentform
              .get('securitiesDist')
              .setValue(security.securitiesDist);
            this.parentform
              .get('securitiesName')
              .setValue(security.securitiesName);
            this.setDefaultData = false;
          }
        }
      });
  }

  initEvent() {
    // 綁定事件
    this.parentform.controls['securitiesCity'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {
        if (result) {
          this.districtList = this.securitiesInfo
            .filter(value => {
              return value.securitiesCity === result;
            })
            .map(value => {
              return value.securitiesDist;
            })
            .filter((value, index, self) => {
              return self.indexOf(value) === index;
            });
        } else {
          this.districtList = [];
          this.branchs = [];
        }
        if (!this.setDefaultData) {
          this.parentform.controls['securitiesDist'].setValue('');
          this.parentform.controls['securitiesName'].setValue('');
        }
      });

    this.parentform.controls['securitiesDist'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {
        if (result) {
          this.branchs = this.securitiesInfo
            .filter(value => {
              return (
                value.securitiesCity ===
                this.parentform.controls['securitiesCity'].value &&
                value.securitiesDist === result
              );
            })
            .map(value => {
              return value.securitiesName;
            })
            .filter((value, index, self) => {
              return self.indexOf(value) === index;
            });
        } else {
          this.branchs = [];
        }
        this.parentform.controls['securitiesName'].setValue('');
      });

    this.parentform.controls['securitiesName'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {
        if (result) {
          const selected = this.securitiesInfo.find(value => {
            return (
              value.securitiesCity ===
              this.parentform.controls['securitiesCity'].value &&
              value.securitiesDist ===
              this.parentform.controls['securitiesDist'].value &&
              value.securitiesName === result
            );
          });
          this.parentform.controls['securitiesAddrZipCode'].setValue(
            selected.securitiesAddrZipCode
          );
          this.parentform.controls['securitiesCode'].setValue(
            selected.securitiesCode
          );
        } else {
          this.parentform.controls['securitiesAddrZipCode'].setValue('');
          this.parentform.controls['securitiesCode'].setValue('');
        }
      });
  }

  public checkIsInValid(formGourp: AbstractControl, controlName: string) {
    return this.formCheckService.checkIsInValid(formGourp, controlName);
  }

  ngOnDestroy(): void {
    this.alive = false;
  }
}
