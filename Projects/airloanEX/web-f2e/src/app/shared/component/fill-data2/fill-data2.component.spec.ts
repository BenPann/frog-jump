import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FillData2Component } from './fill-data2.component';

describe('FillData2Component', () => {
  let component: FillData2Component;
  let fixture: ComponentFixture<FillData2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FillData2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FillData2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
