import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserDataService} from 'src/app/service/user-data/user-data.service';
import {DocumentResult} from 'src/app/interface/resultType';
import {filter, takeUntil, takeWhile} from 'rxjs/operators';
import {UploadDocument} from '../upload-id-card/upload-id-card.interface';
import {DocumentService} from '../../service/document/document.service';
import {ModalFactoryService} from '../../service/common/modal-factory.service';
import {IDCardBackFillDataReq, PhotoView} from 'src/app/service/apply-account/apply-account.interface';
import {ApplyAccountService} from 'src/app/service/apply-account/apply-account.service';
import {MappingService} from '../../../service/user-data/mapping.service';
import {FormCheckService} from '../../service/common/form-check.service';
import {Subject} from 'rxjs';
import {IDCardResponse} from '../../service/document/document.interface';
import { EntryService } from 'src/app/service/entry/entry.service';
import { RouterDirectService } from 'src/app/service/router/router-direct.service';

@Component({
  selector: 'app-common-fill-data2',
  templateUrl: './fill-data2.component.html',
  styleUrls: ['./fill-data2.component.scss']
})
export class FillData2Component implements OnInit, OnDestroy {
  @Input() hideButton = false;
  @Input() hideStepBar = false;
  @Output() back = new EventEmitter<void>();
  @Output() next = new EventEmitter<void>();
  stepbar = 2;
  stepStr = 3;
  leftBtnStr = '上一步';

  // 表單
  dataForm: FormGroup;


  private unsubscribe: Subject<void> = new Subject();

  backId = '';
  backIdDoc: DocumentResult;
  private alive = true;
  birthPlaceErrorMessage: string;

  constructor(
    private documentService: DocumentService,
    private fb: FormBuilder,
    private userDataService: UserDataService,
    private entryService: EntryService,
    private routerDirectService: RouterDirectService,
    private modalFactoryService: ModalFactoryService,
    private applyAccountService: ApplyAccountService,
    public mappingService: MappingService,
    public formCheckService: FormCheckService
  ) {
  }

  ngOnInit() {
    // 取得先前填寫的資料
    let backData = this.userDataService.idCardBackFillDataReq;
    // 如果有必填的資料 才表示是之前有填過的
    if (backData && backData.resAddrZipCode) {

    } else {
      // 此為CIF資料加上OCR 優先度為 OCR > CIF
      let resAddrZipCode: string;
      let resAddrArea: string;
      let resAddr: string;
      let commAddrZipCode: string;
      let commAddr: string;
      let marriage: string;
      if (this.entryService.isCL() && this.routerDirectService.passID === 'Y' && this.userDataService.chinaLifeCifInfo) {
        // 1.戶籍地以OCR為主 2.通訊地、婚姻中壽 帶入
        resAddrZipCode =
          this.userDataService.idCardResponse.householdZipCode ||
          this.userDataService.chinaLifeCifInfo.resAddrZipCode ||
          '';
        resAddrArea =
          this.userDataService.idCardResponse.householdAddr1 +
            this.userDataService.idCardResponse.householdAddr2 ||
          this.userDataService.chinaLifeCifInfo.resAddr ||
          '';
        resAddr =
          this.userDataService.idCardResponse.householdAddr2 +
            this.userDataService.idCardResponse.householdAddr3 +
            this.userDataService.idCardResponse.householdAddr4 ||
          this.userDataService.chinaLifeCifInfo.resAddr ||
          '';

        commAddrZipCode =
          this.userDataService.chinaLifeCifInfo.comAddrZipCode || '';
        commAddr = this.userDataService.chinaLifeCifInfo.comAddr || '';

        // OCR > CIF
        marriage =
        this.mappingService.findKeyByName(
          this.userDataService.idCardResponse.marriage,
          this.mappingService.marriage
        ) || this.userDataService.chinaLifeCifInfo.marriage || '';
      } else {
        resAddrZipCode =
          this.userDataService.creditCardCifInfo.resAddrZipCode ||
          this.userDataService.idCardResponse.householdZipCode ||
          '';
        resAddrArea =
          this.userDataService.creditCardCifInfo.resAddr ||
          this.userDataService.idCardResponse.householdAddr1 +
            this.userDataService.idCardResponse.householdAddr2 ||
          '';
        resAddr =
          this.userDataService.creditCardCifInfo.resAddr ||
          this.userDataService.idCardResponse.householdAddr2 +
            this.userDataService.idCardResponse.householdAddr3 +
            this.userDataService.idCardResponse.householdAddr4 ||
          '';

        commAddrZipCode =
          this.userDataService.creditCardCifInfo.commAddrZipCode || '';
        commAddr = this.userDataService.creditCardCifInfo.commAddr || '';
        marriage =
          this.mappingService.findKeyByName(
            this.userDataService.idCardResponse.marriage,
            this.mappingService.marriage
          ) || '';
      }

      const birthPlace = '';
      const checkTerms = this.userDataService.idCardBackFillDataReq ?  this.userDataService.idCardBackFillDataReq.checkTerms : '';

      backData = {
        resAddrZipCode: resAddrZipCode,
        resAddrArea: resAddrArea,
        resAddr: resAddr,
        commAddrZipCode: commAddrZipCode,
        commAddr: commAddr,
        commAddrArea: '',
        marriage: marriage,
        birthPlace: birthPlace,
        checkTerms: checkTerms
      };
    }

    this.dataForm = this.fb.group({
      checkTerms: backData.checkTerms || '',
      backId: '',
      resAddr: this.fb.group({
        county: ['', [Validators.required]],
        district: ['', [Validators.required]],
        zip: [
          backData.resAddrZipCode,
          [
            Validators.required,
            Validators.minLength(3),
            Validators.pattern(/^[0-9]{3}$/)
          ]
        ],
        addressOther: [
          backData.resAddr,
          [Validators.required, Validators.minLength(2), Validators.pattern(/^[\u4e00-\u9fa5a-zA-Z0-9\-]*$/)]
        ]
      }),
      commAddr: this.fb.group({
        county: ['', [Validators.required]],
        district: ['', [Validators.required]],
        zip: [
          backData.commAddrZipCode,
          [
            Validators.required,
            Validators.minLength(3),
            Validators.pattern(/^[0-9]{3}$/)
          ]
        ],
        addressOther: [
          backData.commAddr,
          [Validators.required, Validators.minLength(2), Validators.pattern(/^[\u4e00-\u9fa5a-zA-Z0-9\-]*$/)]
        ]
      }),
      marriage: [backData.marriage, Validators.required],
      birthPlace: [backData.birthPlace, [Validators.required, Validators.pattern(/^[\u4e00-\u9fa5a-zA-Z0-9\-]*$/)]]
    });

    this.formCheckService.markTouchedWhenHaveValue(this.dataForm);

    // 設定預設值
    const uploadDocument = this.userDataService.uploadDocument;
    if (uploadDocument) {
      this.dataForm.controls['backId'].setValue(uploadDocument.backId);
      this.backId = uploadDocument.backId
        ? uploadDocument.backId
        : 'assets/image/id-back-icon.png';
      this.backIdDoc = {
        title: '',
        image: this.backId,
        name: 'back',
        serial: uploadDocument.backIdSerial
      };
    }

    this.dataForm
      .get('resAddr')
      .valueChanges.pipe(
      filter(data => !!data),
      takeUntil(this.unsubscribe))
      .subscribe(data => {
        if (this.dataForm.get('checkTerms').value) {
          this.dataForm.get('commAddr').setValue(this.dataForm.get('resAddr').value);
        }
      });

      this.dataForm.controls['birthPlace'].valueChanges
        .pipe(takeWhile(() => this.alive))
        .subscribe(data => {
          this.birthPlaceErrorMessageUpdate();
        });
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  preview() {
    this.modalFactoryService.modalImage({
      message: this.backId
    });
  }

  refreshImage(e: DocumentResult) {
    const photoView: PhotoView = {
      sType: '2',
      base64Image: e.image,
      subSerial: this.userDataService.idCardResponse.subSerial
    };
    this.documentService
      .upload(photoView)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          const result: IDCardResponse = data.result;
          this.userDataService.idCardResponse = result;
          this.backIdDoc = {...this.backIdDoc, serial: result.subSerial};
          // 將剛剛取得的OCR結果塞到前端

          if (this.dataForm.get('marriage').value === '' && result.marriage) {
            const marriage =
              this.mappingService.findKeyByName(
                result.marriage,
                this.mappingService.marriage
              ) || '';
            this.dataForm.get('marriage').setValue(marriage);
          }

          if (
            this.dataForm.get('resAddr.zip').value === '' &&
            result.householdZipCode
          ) {
            this.dataForm.get('resAddr.zip').setValue(result.householdZipCode);
          }

          if (this.dataForm.get('resAddr.addressOther').value === '') {
            this.dataForm
              .get('resAddr.addressOther')
              .setValue(
                result.householdAddr2 +
                result.householdAddr3 +
                result.householdAddr4
              );
          }

          this.backId = result.waterMarkImage;
          this.dataForm.controls['backId'].setValue(this.backId);

          const newUploadDocument: UploadDocument = Object.assign(
            {},
            this.userDataService.uploadDocument
          );
          newUploadDocument.backId = this.backId;
          newUploadDocument.backIdSerial = result.subSerial;
          this.userDataService.uploadDocument = newUploadDocument;
        } else {
          this.modalFactoryService.modalDefault({message: data.message});
        }
      });
  }

  asResAddr(event) {
    if (event.target.checked) {
      this.dataForm
        .get('commAddr')
        .setValue(this.dataForm.get('resAddr').value);
      this.dataForm.get('commAddr.county').disable();
      this.dataForm.get('commAddr.district').disable();
      this.dataForm.get('commAddr.addressOther').disable();
    } else {
      this.dataForm.get('commAddr.county').enable();
      this.dataForm.get('commAddr.district').enable();
      this.dataForm.get('commAddr.addressOther').enable();
      this.dataForm.get('commAddr.county').setValue('');
      this.dataForm.get('commAddr.addressOther').setValue('');
    }
  }

  prevStep() {
    this.back.emit();
  }

  nextStep() {
    if (this.dataForm.invalid) {
      return;
    }
    const formData = this.dataForm.getRawValue();
    const req: IDCardBackFillDataReq = {
      resAddrZipCode: formData.resAddr.zip,
      resAddrArea: formData.resAddr.county + formData.resAddr.district,
      resAddr: formData.resAddr.addressOther,
      commAddrZipCode: formData.commAddr.zip,
      commAddrArea: formData.commAddr.county + formData.commAddr.district,
      commAddr: formData.commAddr.addressOther,
      marriage: formData.marriage,
      birthPlace: formData.birthPlace,
      checkTerms: formData.checkTerms
    };
    this.applyAccountService
      .setIDCardBackData(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.userDataService.idCardBackFillDataReq = req;
          this.next.emit();
        } else {
          this.modalFactoryService.modalDefault({message: data.message});
        }
      });
  }

  public getDataFormValid() {
    return this.dataForm.valid;
  }

  public getDataFormValue(
    saveToSessionStorage?: boolean
  ): IDCardBackFillDataReq {
    const formData = this.dataForm.getRawValue();
    const req: IDCardBackFillDataReq =  {
      resAddrZipCode: formData.resAddr.zip,
      resAddrArea: formData.resAddr.county + formData.resAddr.district,
      resAddr: formData.resAddr.addressOther,
      commAddrZipCode: formData.commAddr.zip,
      commAddrArea: formData.commAddr.county + formData.commAddr.district,
      commAddr: formData.commAddr.addressOther,
      marriage: formData.marriage,
      checkTerms: formData.checkTerms,
      birthPlace: formData.birthPlace
    };
    if (saveToSessionStorage) {
      this.userDataService.idCardBackFillDataReq = req;
    }
    return req;
  }

  birthPlaceErrorMessageUpdate() {
    const ctl =  this.dataForm.controls['birthPlace'];
    let message = '';
    if (ctl.touched) {
      if (ctl.valid) {
        message = '';
      } else {
        if (ctl.errors.required) {
          message = '請填寫出生地';
        } else if (ctl.errors.pattern) {
          message = '僅能輸入中、英、數字及"-"符號';
        }
      }
    } else {
      message = '';
    }
    this.birthPlaceErrorMessage = message;
    }
}
