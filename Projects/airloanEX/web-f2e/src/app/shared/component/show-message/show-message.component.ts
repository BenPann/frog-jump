import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalSetting } from 'src/app/interface/resultType';

@Component({
  selector: 'app-show-message',
  templateUrl: './show-message.component.html',
  styleUrls: ['./show-message.component.scss']
})
export class ShowMessageComponent implements OnInit {
  public setting: ModalSetting;
  constructor(public modal: NgbActiveModal) {}

  public ngOnInit(): void {
    const defaultSetting: ModalSetting = {
      title: '',
      subTitle: '',
      showConfirm: false,
      showClose: true,
      message: '',
      btnOK: '確認',
      btnCancel: '取消',
      btnForEdda: false,
    };
    this.setting = Object.assign(defaultSetting, this.setting);
  }

  notSubTitle(): string {
    if (this.setting.subTitle) {
      return '';
    } else {
      return 'w-100';
    }
  }
}
