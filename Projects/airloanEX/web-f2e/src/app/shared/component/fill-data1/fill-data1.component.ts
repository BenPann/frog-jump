import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {UserDataService} from 'src/app/service/user-data/user-data.service';
import {CommonDataService} from '../../../shared/service/common/common-data.service';
import {DropdownItem} from 'src/app/interface/resultType';
import {takeWhile} from 'rxjs/operators';
import {ApplyAccountService} from '../../../service/apply-account/apply-account.service';
import {ModalFactoryService} from '../../../shared/service/common/modal-factory.service';
import {IDCardFrontFillDataReq, PhotoView} from '../../../service/apply-account/apply-account.interface';
import {DocumentResult} from '../../../interface/resultType';
import {
  NgbDateParserFormatter,
  NgbDateStruct
} from '@ng-bootstrap/ng-bootstrap';
import {DocumentService} from 'src/app/shared/service/document/document.service';
import {IDCardResponse} from 'src/app/shared/service/document/document.interface';
import {UploadDocument} from '../../../shared/component/upload-id-card/upload-id-card.interface';
import {FormCheckService} from '../../../shared/service/common/form-check.service';
import { EntryService } from 'src/app/service/entry/entry.service';
import { RouterDirectService } from 'src/app/service/router/router-direct.service';

@Component({
  selector: 'app-common-fill-data1',
  templateUrl: './fill-data1.component.html',
  styleUrls: ['./fill-data1.component.scss']
})
export class FillData1Component implements OnInit, OnDestroy {
  @Input() hideButton = false;
  @Input() hideStepBar = false;
  @Output() back = new EventEmitter<void>();
  @Output() next = new EventEmitter<void>();

  stepStr = 3;
  leftBtnStr = '上一步';

  // step步驟bar
  stepbar = 1;

  // 表單
  dataForm: FormGroup;

  // 日期
  maxDate: NgbDateStruct;
  minDate: NgbDateStruct;
  chtNameErrorMessage: string;
  engNameErrorMessage: string;
  today = new Date();
  idCardLocationList: DropdownItem[];
  frontId = '';
  frontIdDoc: DocumentResult;
  private alive = true;
  idCardRecordList: DropdownItem[];
  idCardRecordListOrg: DropdownItem[] = [ {dataKey: '1', dataName: '初領'},
                  {dataKey: '2', dataName: '補領'},
                  {dataKey: '3', dataName: '換發'}];
  recordName = '初領';

  constructor(
    private fb: FormBuilder,
    private userDataService: UserDataService,
    private commonDataService: CommonDataService,
    private entryService: EntryService,
    private applyAccountService: ApplyAccountService,
    private modalFactoryService: ModalFactoryService,
    private routerDirectService: RouterDirectService,
    private dateFormatter: NgbDateParserFormatter,
    private documentService: DocumentService,
    public formCheckService: FormCheckService
  ) {
  }

  ngOnInit() {
    let frontData = this.userDataService.idCardFrontFillDataReq;
    // 此為客戶之前填寫過的  最優先
    if (frontData && frontData.idCardRecord) {
      frontData = {
        chtName: frontData.chtName,
        engName: frontData.engName,
        idCardDate: frontData.idCardDate,
        idCardLocation: frontData.idCardLocation,
        idCardRecord: frontData.idCardRecord
      };
      // 姓名中壽帶入 (需隱碼)
      // tslint:disable-next-line: max-line-length
      if (this.entryService.isCL() && this.routerDirectService.passID === 'Y' && frontData.chtName.length > 0 && this.userDataService.chinaLifeCifInfo &&
      // 未更改的話就隱碼
      this.userDataService.chinaLifeCifInfo.chtName === frontData.chtName) {
        // tslint:disable-next-line: max-line-length
        frontData.chtName = this.userDataService.chinaLifeCifInfo.pyName && this.userDataService.chinaLifeCifInfo.pyName.length > 0 ? this.maskChtName(this.userDataService.chinaLifeCifInfo.chtName) + ' ' + this.userDataService.chinaLifeCifInfo.pyName : this.maskChtName(this.userDataService.chinaLifeCifInfo.chtName);
      }
    } else {
      frontData = {
        chtName: '',
        engName: '',
        idCardDate: '',
        idCardLocation: '',
        idCardRecord: ''
      };

      // 此為CIF資料加上OCR 優先度為 OCR > CIF
      // 姓名中壽帶入 (需隱碼)
      if (this.entryService.isCL() && this.routerDirectService.passID === 'Y') {
        frontData.chtName =
        // tslint:disable-next-line: max-line-length
        this.userDataService.initUserTypeData && this.userDataService.initUserTypeData.chtName.length > 0 ? this.userDataService.initUserTypeData.chtName :
      // tslint:disable-next-line: max-line-length
      this.userDataService.idCardResponse && this.userDataService.idCardResponse.name.length > 0 ?  this.userDataService.idCardResponse.name :  (this.userDataService.chinaLifeCifInfo.chtName && this.userDataService.chinaLifeCifInfo.chtName.length > 0) ?
      // tslint:disable-next-line: max-line-length
      ( this.userDataService.chinaLifeCifInfo.pyName && this.userDataService.chinaLifeCifInfo.pyName.length > 0 ? this.maskChtName(this.userDataService.chinaLifeCifInfo.chtName) + ' ' + this.userDataService.chinaLifeCifInfo.pyName : this.maskChtName(this.userDataService.chinaLifeCifInfo.chtName)) : '';
      } else {
        frontData.chtName =
          this.userDataService.initUserTypeData.chtName ||
          this.userDataService.creditCardCifInfo.chtName ||
          this.userDataService.idCardResponse.name;
      }

      frontData.engName = this.userDataService.creditCardCifInfo.engName;
      let idCardDate = '';
      if (this.userDataService.idCardResponse.idCard_Issue_DT) {
        try {
          idCardDate = (
            +this.userDataService.idCardResponse.idCard_Issue_DT + 19110000
          ).toString(10);
          frontData.idCardDate =
            idCardDate.substring(0, 4) +
            '-' +
            idCardDate.substring(4, 6) +
            '-' +
            idCardDate.substring(6, 8);
        } catch (error) {
          // 有錯誤 就不處理了
        }
      }

      frontData.idCardLocation = this.userDataService.idCardResponse.idCard_Issue_City;
      frontData.idCardRecord = this.userDataService.idCardResponse.idCard_Issue_Type;
    }

    if (this.entryService.isCL() && this.routerDirectService.passID === 'Y') {
      this.dataForm = this.fb.group({
        frontId: ['', Validators.required],
        chtName: [
          frontData.chtName,
          [Validators.required, Validators.minLength(2), Validators.pattern(/^[\u4e00-\u9fa5a-zA-Z,-\s＊]*$/)]
        ],
        engName: [frontData.engName,
          [Validators.required, Validators.pattern(/^[a-zA-Z][a-zA-Z\s,.-]*$/)]],
        idCardDate: [
          this.dateFormatter.parse(frontData.idCardDate),
          Validators.required
        ],
        idCardLocation: [frontData.idCardLocation, Validators.required],
        idCardRecord: [frontData.idCardRecord, Validators.required]
      });
    } else {
        this.dataForm = this.fb.group({
          frontId: ['', Validators.required],
          chtName: [
            frontData.chtName,
            [Validators.required, Validators.minLength(2), Validators.pattern(/^[\u4e00-\u9fa5a-zA-Z\s]*$/)]
          ],
          engName: [frontData.engName,
            [Validators.required, Validators.pattern(/^[a-zA-Z][a-zA-Z\s,.-]*$/)]],
          idCardDate: [
            this.dateFormatter.parse(frontData.idCardDate),
            Validators.required
          ],
          idCardLocation: [frontData.idCardLocation, Validators.required],
          idCardRecord: [frontData.idCardRecord, Validators.required]
        });
    }

    this.formCheckService.markTouchedWhenHaveValue(this.dataForm);

    this.dataForm
      .get('chtName')
      .valueChanges.pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.chtNameErrorMessageUpdate();
      });

      this.dataForm
      .get('engName')
      .valueChanges.pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.engNameErrorMessageUpdate();
      });

    this.initdatepicker();
    this.commonDataService
      .getDropdownData('idcardlocation')
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.idCardLocationList = data;
      });

    // 設定預設值
    const uploadDocument = this.userDataService.uploadDocument;
    if (uploadDocument) {
      this.dataForm.controls['frontId'].setValue(uploadDocument.frontId);
      this.frontId = uploadDocument.frontId
        ? uploadDocument.frontId
        : 'assets/image/id-front-icon.png';
      this.frontIdDoc = {
        title: '',
        image: this.frontId,
        name: 'front',
        serial: uploadDocument.frontIdSerial
      };
    }

      this.dataForm.controls['idCardRecord'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        switch (data) {
          case '1':
              this.recordName = this.idCardRecordListOrg.find(
                (value: { dataKey: string }) => value.dataKey === data
                ).dataName;
            break;
          case '2':
              this.recordName = this.idCardRecordListOrg.find(
                (value: { dataKey: string }) => value.dataKey === data
                ).dataName;
            break;
          case '3':
              this.recordName = this.idCardRecordListOrg.find(
                (value: { dataKey: string }) => value.dataKey === data
                ).dataName;
            break;
        }
    });
    this.idCardRecordList = this.idCardRecordListOrg;
    if (this.dataForm.value.idCardRecord) {
      this.recordName = this.idCardRecordListOrg.find(
        (value: { dataKey: string }) => value.dataKey === this.dataForm.value.idCardRecord
        ).dataName;
    }
  }

  maskChtName(chName: string): string {
    if (!chName) {
      return '';
    }
    // tslint:disable-next-line: max-line-length
    return chName.length < 0 ? '' : chName.length < 3 ? chName.substring(0, 1) + '＊' : chName.substring(0, 1) + '＊'.repeat(chName.length - 2) + chName.substring(chName.length - 1);
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  // 日期設定init
  initdatepicker(): void {
    const y = this.today.getFullYear();
    const m = this.today.getMonth() + 1;
    const d = this.today.getDate();

    this.maxDate = {
      year: y,
      month: m,
      day: d
    };
    this.minDate = {
      year: y - 100,
      month: m,
      day: d
    };
  }

  preview() {
    this.modalFactoryService.modalImage({
      message: this.dataForm.get('frontId').value
    });
  }

  chtNameErrorMessageUpdate() {
    const ctl = this.dataForm.get('chtName');
    let message = '';
    if (ctl.touched) {
      if (ctl.valid) {
        message = '';
      } else {
        if (ctl.errors.required) {
          message = '請輸入身分證姓名，最少為兩個文字';
        } else if (ctl.errors.pattern) {
          message = '僅能輸入中、英文字';
        } else if (ctl.errors.minlength) {
          message = '請輸入身分證姓名，最少為兩個文字';
        }
      }
    } else {
      message = '';
    }
    this.chtNameErrorMessage = message;
  }

  engNameErrorMessageUpdate() {
    const ctl = this.dataForm.get('engName');
    let message = '';
    if (ctl.touched) {
      if (ctl.valid) {
        message = '';
      } else {
        if (ctl.errors.required) {
          message = '請輸入英文姓名，需同護照';
        } else if (ctl.errors.pattern) {
          message = '僅能輸入英文字含空白';
        } else if (ctl.errors.minlength) {
          message = '請輸入英文姓名，最少為兩個文字';
        }
      }
    } else {
      message = '';
    }
    this.engNameErrorMessage = message;
  }

  upperCase(event): void {
    if (!event.target.value) {
      return;
    }
    event.target.value = event.target.value.toUpperCase();
  }


  refreshFrontImage(e: DocumentResult) {
    console.log(this.frontIdDoc);
    console.log(e.serial);
    const photoView: PhotoView = {
      sType: '1',
      base64Image: e.image,
      subSerial: e.serial
    };
    this.documentService
      .upload(photoView)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          const result: IDCardResponse = data.result;
          this.userDataService.idCardResponse = result;
          // this.frontIdDoc.serial = result.subSerial;
          this.frontIdDoc = {...this.frontIdDoc, serial: result.subSerial};
          // 將剛剛取得的OCR結果塞到前端
          if (this.dataForm.get('chtName').value === '' && result.name) {
            this.dataForm.get('chtName').setValue(result.name);
          }

          if (
            this.dataForm.get('idCardLocation').value === '' &&
            result.idCard_Issue_City
          ) {
            this.dataForm
              .get('idCardLocation')
              .setValue(result.idCard_Issue_City);
          }

          if (
            this.dataForm.get('idCardDate').value === '' &&
            result.idCard_Issue_DT
          ) {
            try {
              const idCardDate = (+result.idCard_Issue_DT + 19110000).toString(
                10
              );
              this.dataForm
                .get('idCardDate')
                .setValue(
                  idCardDate.substring(0, 4) +
                  '-' +
                  idCardDate.substring(4, 6) +
                  '-' +
                  idCardDate.substring(6, 8)
                );
            } catch (error) {
              // 有錯誤 就不處理了
            }
          }

          if (
            this.dataForm.get('idCardRecord').value === '' &&
            result.idCard_Issue_Type
          ) {
            this.dataForm
              .get('idCardRecord')
              .setValue(result.idCard_Issue_Type);
          }

          this.frontId = result.waterMarkImage;
          this.dataForm.controls['frontId'].setValue(this.frontId);

          const newUploadDocument: UploadDocument = Object.assign(
            {},
            this.userDataService.uploadDocument
          );
          newUploadDocument.frontId = this.frontId;
          newUploadDocument.frontIdSerial = result.subSerial;
          this.userDataService.uploadDocument = newUploadDocument;
        } else {
          this.modalFactoryService.modalDefault({message: data.message});
        }
      });
  }

  prevStep() {
    this.back.emit();
  }

  nextStep() {
    if (this.dataForm.invalid) {
      return;
    }

    let req: IDCardFrontFillDataReq;
    if (this.entryService.isCL() && this.routerDirectService.passID === 'Y') {
      req = {
      // tslint:disable-next-line: max-line-length
      chtName: !(this.dataForm.value.chtName.indexOf('＊') > -1)
      // && this.dataForm.value.chtName === this.maskChtName(this.userDataService.chinaLifeCifInfo.chtName)
       // tslint:disable-next-line: max-line-length
       ? this.dataForm.value.chtName : this.userDataService.chinaLifeCifInfo.chtName && this.userDataService.chinaLifeCifInfo.chtName.length > 0 ? this.userDataService.chinaLifeCifInfo.chtName : '' ,
      engName: this.dataForm.value.engName,
      idCardDate: this.dateFormatter.format(this.dataForm.value.idCardDate),
      idCardLocation: this.dataForm.value.idCardLocation,
      idCardRecord: this.dataForm.value.idCardRecord
    };
    } else {
        req  = {
          chtName: this.dataForm.value.chtName,
          engName: this.dataForm.value.engName,
          idCardDate: this.dateFormatter.format(this.dataForm.value.idCardDate),
          idCardLocation: this.dataForm.value.idCardLocation,
          idCardRecord: this.dataForm.value.idCardRecord
        };
    }

    this.applyAccountService
      .setIDCardFrontData(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.userDataService.idCardFrontFillDataReq = req;
          this.next.emit();
        } else {
          this.modalFactoryService.modalDefault({message: data.message});
        }
      });
  }

  public getDataFormValid() {
    return this.dataForm.valid;
  }

  public getDataFormValue(
    saveToSessionStorage?: boolean
  ): IDCardFrontFillDataReq {
    const req: IDCardFrontFillDataReq = {
      chtName: this.dataForm.value.chtName,
      engName: this.dataForm.value.engName,
      idCardDate: this.dateFormatter.format(this.dataForm.value.idCardDate),
      idCardLocation: this.dataForm.value.idCardLocation,
      idCardRecord: this.dataForm.value.idCardRecord
    };
    if (saveToSessionStorage) {
      this.userDataService.idCardFrontFillDataReq = req;
    }
    return req;
  }
}
