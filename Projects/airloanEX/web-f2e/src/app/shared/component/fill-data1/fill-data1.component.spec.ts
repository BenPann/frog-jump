import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FillData1Component } from './fill-data1.component';

describe('FillData1Component', () => {
  let component: FillData1Component;
  let fixture: ComponentFixture<FillData1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FillData1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FillData1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
