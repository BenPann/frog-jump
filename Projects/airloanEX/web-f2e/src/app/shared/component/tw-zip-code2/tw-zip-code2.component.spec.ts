import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwZipCode2Component } from './tw-zip-code2.component';

describe('TwZipCode2Component', () => {
  let component: TwZipCode2Component;
  let fixture: ComponentFixture<TwZipCode2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwZipCode2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwZipCode2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
