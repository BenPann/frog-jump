import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  SimpleChanges,
  OnChanges,
  Output,
  EventEmitter
} from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormGroup,
  AbstractControl
} from '@angular/forms';
import { Observable, Subscription, Subject } from 'rxjs';
import { takeUntil, takeWhile } from 'rxjs/operators';
import { FormCheckService } from 'src/app/shared/service/common/form-check.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { zipcodeData } from 'src/app/mock/twzipcode';

@Component({
  selector: 'app-tw-zip-code2',
  templateUrl: './tw-zip-code2.component.html',
  styleUrls: ['./tw-zip-code2.component.scss']
})
export class TwZipCode2Component implements OnInit, OnDestroy, OnChanges {
  zipcodeFull: { [key: string]: any };

  // 傳給父元件的表單值
  @Output() twZipForm = new EventEmitter<FormGroup>();

  // 傳進的值
  @Input() county: string;
  @Input() district: string;
  @Input() zipcode: string;
  @Input() addressOther: string;

  form: FormGroup;

  districtList: any;

  private alive = true;

  constructor(
    private commonDataService: CommonDataService,
    private formCheckService: FormCheckService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    // init tw-zip-form
    this.form = this.fb.group({
      county: ['', Validators.required],
      district: ['', Validators.required],
      zipcode: '',
      addressOther: ['', Validators.required]
    });

    // 取資料
    this.zipcodeFull = zipcodeData;

    if (this.district) {
      this.setZip(this.district);
    }

    // 綁定事件
    this.form.controls['county'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {
        this.districtList = this.zipcodeFull[result];
        this.form.controls['district'].setValue('');
        this.form.controls['zipcode'].setValue('');
        this.form.controls['addressOther'].setValue('');
        this.twZipForm.emit(this.form);
      });

    this.form.controls['district'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {
        this.form.controls['zipcode'].setValue(this.districtList[result]);
        this.twZipForm.emit(this.form);
      });

    this.form.controls['addressOther'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {
        this.twZipForm.emit(this.form);
      });
  }
  /**
   * 當input()異動時觸發的事件
   * @param changes 變動的物件
   */
  ngOnChanges(changes: SimpleChanges): void {
    // if (
    //   changes.zipcode.currentValue &&
    //   changes.zipcode.previousValue !== changes.zipcode.currentValue
    // ) {
    //   this.setZip(changes.zipcode.currentValue, true);
    // }
  }

  public checkIsInValid(formGourp: AbstractControl, controlName: string) {
    return this.formCheckService.checkIsInValid(formGourp, controlName);
  }

  public setZip(districtName: string) {
    if (districtName) {
      for (const county in this.zipcodeFull) {
        if (this.zipcodeFull.hasOwnProperty(county)) {
          const element = this.zipcodeFull[county];
          for (const district in element) {
            if (district === districtName) {
              this.form.controls['county'].setValue(county);
              this.form.controls['district'].setValue(district);
              this.form.controls['zipcode'].setValue(element[district]);
              if (this.addressOther) {
                this.form.controls['addressOther'].setValue(this.addressOther);
              }

              break;
            }
          }
        }
      }
    }
  }

  descOrder(a, b) {
    if (a.key < b.key) {
      return b.key;
    }
  }

  ngOnDestroy(): void {
    this.alive = false;
  }
}
