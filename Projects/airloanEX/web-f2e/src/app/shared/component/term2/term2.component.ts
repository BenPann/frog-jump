import { Component, OnInit, Input } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { TermsService } from 'src/app/service/terms/terms.service';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-term2',
  templateUrl: './term2.component.html',
  styleUrls: ['./term2.component.scss']
})
export class Term2Component implements OnInit {
  isLookdocOpen = false;

  @Input()
  termName: String;

  @Input()
  termTitle: String;

  @Input()
  parentForm: FormGroup;

  constructor(public termService: TermsService) {}

  termText: String;

  ngOnInit() {
    this.termService
      .getTerms(this.termName)
      .pipe(shareReplay(1))
      .subscribe(
        data => {
          try {
            // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
            const json = JSON.parse(data);
            this.termText = '查詢條款失敗';
          } catch (error) {
            this.termText = data;
          }
        },
        error => {
          console.log('條款錯誤', error);
          this.termText = '查詢條款失敗';
        }
      );
  }
}
