import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetOnlineBankComponent } from './set-online-bank.component';

describe('SetOnlineBankComponent', () => {
  let component: SetOnlineBankComponent;
  let fixture: ComponentFixture<SetOnlineBankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetOnlineBankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetOnlineBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
