import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { FormCheckService } from '../../service/common/form-check.service';
import { WebBankService } from '../../../service/web-bank/web-bank.service';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  ValidatorFn
} from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
import { ModalFactoryService } from '../../service/common/modal-factory.service';
import { EncryptService } from '../../service/common/encrypt.service';
import { WebBankResponse } from 'src/app/service/web-bank/web-bank.interface';
import { UserDataService } from '../../../service/user-data/user-data.service';

@Component({
  selector: 'app-set-online-bank',
  templateUrl: './set-online-bank.component.html',
  styleUrls: ['./set-online-bank.component.scss']
})
export class SetOnlineBankComponent implements OnInit, OnDestroy {
  @Output() backStep = new EventEmitter<void>();
  @Output() nextStep = new EventEmitter<boolean>();

  dataForm: FormGroup;
  dataForm2: FormGroup;

  bankUserErrorMessage: string;
  bankPwdErrorMessage: string;
  bankPwdConfirmErrorMessage: string;
  ivrBankPwdErrorMessage: string;
  ivrBankPwdConfirmErrorMessage: string;

  webBankRes: WebBankResponse;
  private alive = true;
  constructor(
    private fb: FormBuilder,
    public formCheckService: FormCheckService,
    private modalFactoryService: ModalFactoryService,
    private webBankService: WebBankService,
    private encryptService: EncryptService,
    private userDataService: UserDataService
  ) {}

  ngOnInit() {
    this.initWebBank();
    this.initIvrBank();
    // 如果已經申請過網銀跟電話語音 就離開
    // this.webBankRes = {
    //   hasWebBank: 'N',
    //   hasIvrBank: 'N'
    // };
    this.webBankService
      .getWebBank()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.webBankRes = data.result;
          if (
            this.webBankRes.hasWebBank === 'Y' &&
            this.webBankRes.hasIvrBank === 'Y'
          ) {
            this.nextStep.emit(false);
          }
        } else {
          this.modalFactoryService.modalDefault({ message: data.message });
        }
      });
  }

  initWebBank() {
    this.dataForm = this.fb.group(
      {
        bankUser: [
          '',
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(12),
            Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{2,}$/),
            this.formCheckService.validatorNotEqual(
              this.userDataService.initRqData.idno
            )
          ]
        ],
        bankpwd: [
          '',
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(16),
            Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{2,}$/),
            this.formCheckService.validatorNotEqual(
              this.userDataService.initRqData.idno
            ),
            this.formCheckService.validatorMatchIncrese('0', 4),
            this.formCheckService.validatorMatchSame('0', 3)
          ]
        ],
        bankpwdConfirm: [
          '',
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(16),
            Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{2,}$/),
            this.formCheckService.validatorNotEqual(
              this.userDataService.initRqData.idno
            ),
            this.formCheckService.validatorMatchIncrese('0', 4),
            this.formCheckService.validatorMatchSame('0', 3)
          ]
        ]
      },
      { validator: this.passwordConfirming }
    );

    this.dataForm
      .get('bankUser')
      .valueChanges.pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.bankUserErrorMessageUpdate();
      });

    this.dataForm
      .get('bankpwd')
      .valueChanges.pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.bankPwdErrorMessageUpdate();
      });
    this.dataForm
      .get('bankpwdConfirm')
      .valueChanges.pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.bankPwdConfirmErrorMessageUpdate();
      });
  }

  initIvrBank() {
    this.dataForm2 = this.fb.group(
      {
        bankpwd: [
          '',
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(8),
            Validators.pattern(/^[\d]{2,}$/),
            this.formCheckService.validatorMatchIncrese('1'),
            this.formCheckService.validatorMatchSame('1')
          ]
        ],
        bankpwdConfirm: [
          '',
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(8),
            Validators.pattern(/^[\d]{2,}$/),
            this.formCheckService.validatorMatchIncrese('1'),
            this.formCheckService.validatorMatchSame('1')
          ]
        ]
      },
      { validator: this.ivrPasswordConfirming }
    );
    this.dataForm2
      .get('bankpwd')
      .valueChanges.pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.ivrBankPwdErrorMessageUpdate();
      });
    this.dataForm2
      .get('bankpwdConfirm')
      .valueChanges.pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.ivrBankPwdConfirmErrorMessageUpdate();
      });
  }

  bankUserErrorMessageUpdate() {
    const ctl = this.dataForm.get('bankUser');
    let message = '';
    if (ctl.touched) {
      if (ctl.valid) {
        message = '';
      } else {
        if (ctl.errors.required) {
          message = '請輸入使用者代號';
        } else if (ctl.errors.minlength) {
          message = '需為6~12位英數混合';
        } else if (ctl.errors.maxlength) {
          message = '需為6~12位英數混合';
        } else if (ctl.errors.pattern) {
          message = '必須包含英數字，英文大小寫視為不同';
        } else if (ctl.errors.notequal) {
          message = '使用者代號不可與身分證字號相同';
        }
      }
    } else {
      message = '';
    }
    this.bankUserErrorMessage = message;
  }

  bankPwdErrorMessageUpdate() {
    const ctl = this.dataForm.get('bankpwd');
    let message = '';
    if (ctl.touched) {
      if (ctl.valid) {
        message = '';
      } else {
        if (ctl.errors.required) {
          message = '請輸入網路銀行密碼';
        } else if (ctl.errors.minlength) {
          message = '需為6~16位英數混合';
        } else if (ctl.errors.maxlength) {
          message = '需為6~16位英數混合';
        } else if (ctl.errors.pattern) {
          message = '必須包含英數字，英文大小寫視為不同';
        } else if (ctl.errors.notequal) {
          message = '網路銀行密碼不可與身分證字號相同';
        } else if (ctl.errors.matchincrese) {
          message =
            '不可含有連續遞增(減)的四碼英文或數字';
        } else if (ctl.errors.matchsame) {
          message =
            '不可含有連續三碼相同的英文或數字';
        }
      }
    } else {
      message = '';
    }
    this.bankPwdErrorMessage = message;
  }

  bankPwdConfirmErrorMessageUpdate() {
    const ctl = this.dataForm.get('bankpwdConfirm');
    let message = '';
    if (ctl.touched) {
      if (ctl.valid) {
        message = '';
      } else {
        if (ctl.errors.required) {
          message = '請輸入確認網路銀行密碼';
        } else if (ctl.errors.minlength) {
          message = '需為6~16位英數混合';
        } else if (ctl.errors.maxlength) {
          message = '需為6~16位英數混合';
        } else if (ctl.errors.pattern) {
          message = '必須包含英數字，英文大小寫視為不同';
        } else if (ctl.errors.notequal) {
          message = '確認網路銀行密碼不可與身分證字號相同';
        } else if (ctl.errors.matchincrese) {
          message =
            '不可含有連續遞增(減)的四碼英文或數字';
        } else if (ctl.errors.matchsame) {
          message =
            '不可含有連續三碼相同的英文或數字';
        }
      }
    } else {
      message = '';
    }
    this.bankPwdConfirmErrorMessage = message;
  }

  ivrBankPwdErrorMessageUpdate() {
    const ctl = this.dataForm2.get('bankpwd');
    let message = '';
    if (ctl.touched) {
      if (ctl.valid) {
        message = '';
      } else {
        if (ctl.errors.required) {
          message = '請輸入電話銀行密碼';
        } else if (ctl.errors.minlength) {
          message = '長度必須為6~8碼的數字';
        } else if (ctl.errors.maxlength) {
          message = '長度必須為6~8碼的數字';
        } else if (ctl.errors.pattern) {
          message = '必須為純數字';
        } else if (ctl.errors.matchincrese) {
          message =
            '不可為連續號碼';
        } else if (ctl.errors.matchsame) {
          message =
            '不可為相同數字';
        }
      }
    } else {
      message = '';
    }
    this.ivrBankPwdErrorMessage = message;
  }

  ivrBankPwdConfirmErrorMessageUpdate() {
    const ctl = this.dataForm2.get('bankpwdConfirm');
    let message = '';
    if (ctl.touched) {
      if (ctl.valid) {
        message = '';
      } else {
        if (ctl.errors.required) {
          message = '請輸入確認電話銀行密碼';
        } else if (ctl.errors.minlength) {
          message = '長度必須為6~8碼的數字';
        } else if (ctl.errors.maxlength) {
          message = '長度必須為6~8碼的數字';
        } else if (ctl.errors.pattern) {
          message = '必須為純數字';
        } else if (ctl.errors.matchincrese) {
          message =
            '不可為連續號碼';
        } else if (ctl.errors.matchsame) {
          message =
            '不可為相同數字';
        }
      }
    } else {
      message = '';
    }
    this.ivrBankPwdConfirmErrorMessage = message;
  }

  ngOnDestroy() {
    this.alive = false;
  }

  passwordConfirming(c: AbstractControl): { invalid: boolean } {
    if (
      c.get('bankpwd').value !== c.get('bankpwdConfirm').value ||
      c.get('bankUser').value === c.get('bankpwd').value
    ) {
      return { invalid: true };
    }
  }

  ivrPasswordConfirming(c: AbstractControl): { invalid: boolean } {
    if (c.get('bankpwd').value !== c.get('bankpwdConfirm').value) {
      return { invalid: true };
    }
  }

  getWebBankPwdConfirmClass() {
    if (this.checkWebBankPwdConfirm()) {
      return 'is-invalid';
    } else {
      return this.formCheckService.validClass(this.dataForm, 'bankpwdConfirm');
    }
  }

  getWebBankPwdClass() {
    if (this.checkWebBankPwd()) {
      return 'is-invalid';
    } else {
      return this.formCheckService.validClass(this.dataForm, 'bankpwd');
    }
  }

  checkWebBankPwdConfirm() {
    return (
      this.dataForm.get('bankpwd').valid &&
      this.dataForm.get('bankpwdConfirm').valid &&
      this.dataForm.get('bankpwdConfirm').value !==
        this.dataForm.get('bankpwd').value
    );
  }

  checkWebBankPwd() {
    return (
      this.dataForm.get('bankpwd').valid &&
      this.dataForm.get('bankUser').valid &&
      this.dataForm.get('bankUser').value === this.dataForm.get('bankpwd').value
    );
  }

  getIvrConfirmClass() {
    if (this.checkIvrConfirm()) {
      return 'is-invalid';
    } else {
      return this.formCheckService.validClass(this.dataForm2, 'bankpwdConfirm');
    }
  }

  checkIvrConfirm() {
    return (
      this.dataForm2.get('bankpwd').valid &&
      this.dataForm2.get('bankpwdConfirm').valid &&
      this.dataForm2.get('bankpwdConfirm').value !==
        this.dataForm2.get('bankpwd').value
    );
  }

  formInValid() {
    if (!this.webBankRes) {
      return true;
    }
    return (
      (this.webBankRes.hasWebBank === 'Y' ? false : this.dataForm.invalid) ||
      (this.webBankRes.hasIvrBank === 'Y' ? false : this.dataForm2.invalid)
    );
  }

  back(): void {
    this.backStep.emit();
  }

  next(): void {
    this.webBankService
      .getWebBankKey()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          const publickey = data.result.webBankKey;
          if (publickey) {
            const bankpwd =
              this.webBankRes.hasWebBank === 'Y'
                ? ''
                : this.encryptService.encrypt(
                    publickey,
                    this.dataForm.value.bankpwd
                  );
            const bankUser =
              this.webBankRes.hasWebBank === 'Y'
                ? ''
                : this.encryptService.encrypt(
                    publickey,
                    this.dataForm.value.bankUser
                  );
            const ivrpwd =
              this.webBankRes.hasIvrBank === 'Y'
                ? ''
                : this.encryptService.encrypt(
                    publickey,
                    this.dataForm2.value.bankpwd
                  );
            this.webBankService
              .setWebBank(bankUser, bankpwd, ivrpwd)
              .pipe(takeWhile(() => this.alive))
              .subscribe(data2 => {
                if (data2.status === 0) {
                  this.nextStep.emit(true);
                } else {
                  this.modalFactoryService.modalDefault({
                    message: data2.message
                  });
                }
              });
          } else {
            this.modalFactoryService.modalDefault({ message: '取得資訊失敗' });
          }
        } else {
          this.modalFactoryService.modalDefault({ message: data.message });
        }
      });
  }
}
