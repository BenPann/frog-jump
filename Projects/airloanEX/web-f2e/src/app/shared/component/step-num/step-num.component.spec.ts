import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepNumComponent } from './step-num.component';

describe('StepNumComponent', () => {
  let component: StepNumComponent;
  let fixture: ComponentFixture<StepNumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepNumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepNumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
