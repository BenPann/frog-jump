import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-step-num',
  templateUrl: './step-num.component.html',
  styleUrls: ['./step-num.component.scss']
})
export class StepNumComponent implements OnInit {
  @Input() step = 0;
  constructor() { }

  ngOnInit() {
  }

}
