import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormGroup} from '@angular/forms';
import {takeWhile} from 'rxjs/operators';
import {FormCheckService} from 'src/app/shared/service/common/form-check.service';
import {CommonDataService} from 'src/app/shared/service/common/common-data.service';
import {ProductService} from '../../../service/product/product.service';
import {BankInfo} from '../../../service/product/product.interface';

@Component({
  selector: 'app-bank-list',
  templateUrl: './bank-list.component.html',
  styleUrls: ['./bank-list.component.scss']
})
export class BankListComponent implements OnInit, OnDestroy {
  // 所有資料列表
  public countyList: string[];
  // 區域列表
  public districtList: string[];
  // 分行列表
  public branchs: string[];

  @Input()
  public parentform: FormGroup;
  @Input()
  public branchId: string;
  @Input()
  public nearBankZipcode: string;

  public bankAddress: string;
  bankInfo: BankInfo[];
  private alive = true;
  private setDefaultData = false;

  constructor(
    private commonDataService: CommonDataService,
    private formCheckService: FormCheckService,
    private productService: ProductService) {
  }

  ngOnInit() {
    this.initEvent();

    const data = {
      status: 0,
      message: '',
      result: [
        {
          bankCity: '臺北市',
          bankDist: '中正區',
          bankAddrZipCode: '10045',
          bankAddr: '中正路XXX號',
          bankName: '城中2',
          branchCode: '101'
        },
        {
          bankCity: '臺北市',
          bankDist: '大安區',
          bankAddrZipCode: '10046',
          bankAddr: '新生南路二段8號',
          bankName: '大安分行',
          branchCode: '102'
        },
        {
          bankCity: '臺北市',
          bankDist: '中山區',
          bankAddrZipCode: '10047',
          bankAddr: '站前廣場',
          bankName: '站前',
          branchCode: '103'
        },
        {
          bankCity: '新北市',
          bankDist: '新店區',
          bankAddrZipCode: '23147',
          bankAddr: '烏來路XX巷OO號',
          bankName: '新店',
          branchCode: '104'
        }
      ]
    };
    if (data.status === 0) {
      this.bankInfo = data.result;
      // 取得清單內 所有的地區

      this.countyList = this.bankInfo
        .map(value => {
          return value.bankCity;
        })
        .filter((value, index, self) => {
          return self.indexOf(value) === index;
        });
    }


    // 取資料
    // this.productService
    //   .getBranchBankInfo()
    //   .pipe(takeWhile(() => this.alive))
    //   .subscribe(data => {
    //     data = {
    //       status: 0,
    //       message: '',
    //       result: [
    //         {
    //           bankCity: '臺北市',
    //           bankDist: '中正區',
    //           bankAddrZipCode: '10045',
    //           bankAddr: '中正路XXX號',
    //           bankName: '城中2',
    //         },
    //         {
    //           bankCity: '臺北市',
    //           bankDist: '大安區',
    //           bankAddrZipCode: '10046',
    //           bankAddr: '新生南路二段8號',
    //           bankName: '大安分行'
    //         },
    //         {
    //           bankCity: '臺北市',
    //           bankDist: '中山區',
    //           bankAddrZipCode: '10047',
    //           bankAddr: '站前廣場',
    //           bankName: '站前'
    //         },
    //         {
    //           bankCity: '新北市',
    //           bankDist: '新店區',
    //           bankAddrZipCode: '23147',
    //           bankAddr: '烏來路XX巷OO號',
    //           bankName: '新店'
    //         }
    //       ]
    //     };
    //     if (data.status === 0) {
    //       this.bankInfo = data.result;
    //       // 取得清單內 所有的地區
    //
    //       this.countyList = this.bankInfo
    //         .map(value => {
    //           return value.bankCity;
    //         })
    //         .filter((value, index, self) => {
    //           return self.indexOf(value) === index;
    //         });
    //     }
    //   });
    if (this.branchId) {
      const branch = this.bankInfo.find(value => value.branchCode === this.branchId);
      this.parentform.controls['bankCity'].setValue(branch.bankCity);
      this.parentform.controls['bankDist'].setValue(branch.bankDist);
      this.parentform.controls['bankName'].setValue(branch.bankName);
    }
  }

  initEvent() {
    // 綁定事件
    this.parentform.controls['bankCity'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {
        if (result) {
          this.districtList = this.bankInfo
            .filter(value => {
              return value.bankCity === result;
            })
            .map(value => {
              return value.bankDist;
            })
            .filter((value, index, self) => {
              return self.indexOf(value) === index;
            });
        } else {
          this.districtList = [];
          this.branchs = [];
        }
        if (!this.setDefaultData) {
          this.parentform.controls['bankDist'].setValue('');
          this.parentform.controls['bankName'].setValue('');
        }
        this.bankAddress = '';
      });

    this.parentform.controls['bankDist'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {
        if (result) {
          this.branchs = this.bankInfo
            .filter(value => {
              return (
                value.bankCity ===
                this.parentform.controls['bankCity'].value &&
                value.bankDist === result
              );
            })
            .map(value => {
              return value.bankName;
            })
            .filter((value, index, self) => {
              return self.indexOf(value) === index;
            });
        } else {
          this.branchs = [];
        }
        this.parentform.controls['bankName'].setValue('');
        this.bankAddress = '';
      });

    this.parentform.controls['bankName'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {
        if (result) {
          const selected = this.bankInfo.find(value => {
            return (
              value.bankCity ===
              this.parentform.controls['bankCity'].value &&
              value.bankDist ===
              this.parentform.controls['bankDist'].value &&
              value.bankName === result
            );
          });
          this.parentform.controls['bankAddrZipCode'].setValue(
            selected.bankAddrZipCode
          );
          this.parentform.controls['bankAddr'].setValue(
            selected.bankAddr
          );
          this.parentform.controls['branchId'].setValue(selected.branchCode);
          this.bankAddress = selected.bankCity + selected.bankDist + selected.bankAddr;
        } else {
          this.parentform.controls['bankAddrZipCode'].setValue('');
          this.parentform.controls['bankAddr'].setValue('');
          this.parentform.controls['branchId'].setValue('');
          this.bankAddress = '';
        }
      });
  }


  /**
   * 依據傳進來的郵遞區號 取得最靠近的分行 並顯示於畫面上的下拉選單
   * @param zipcode 郵遞區號
   */
  // public setBranchBankByZipcode(zipcode: string) {
  //   if (zipcode) {
  //     for (const county in this.bankList) {
  //       if (this.bankList.hasOwnProperty(county)) {
  //         const element = this.bankList[county];
  //         for (const district in element) {
  //           if (element.hasOwnProperty(district)) {
  //             if (!element[district]) { return; }
  //             const address = element[district].address;
  //             const code = element[district].code;
  //             if (address && address.startsWith(zipcode)) {
  //               this.parentform.controls['bankCounty'].setValue(county);
  //               this.parentform.controls['bankName'].setValue(district);
  //               this.parentform.controls['bankAddress'].setValue(address);
  //               this.parentform.controls['bankCode'].setValue(code);
  //               break;
  //             }
  //           }
  //         }
  //       }
  //     }
  //   }
  // }

  /**
   * 依據輸入的分行代號 於畫面上的下拉選單中顯示
   * @param branchId 分行代號
   */
  // public setBranchBank(branchId: string) {
  //   if (branchId) {
  //     for (const county in this.bankList) {
  //       if (this.bankList.hasOwnProperty(county)) {
  //         const element = this.bankList[county];
  //         for (const district in element) {
  //           if (element.hasOwnProperty(district)) {
  //             const address = element[district].address;
  //             const code = element[district].code;
  //             if (district === branchId) {
  //               this.parentform.controls['bankCounty'].setValue(county);
  //               this.parentform.controls['bankName'].setValue(district);
  //               this.parentform.controls['bankAddress'].setValue(address);
  //               this.parentform.controls['bankCode'].setValue(code);
  //               break;
  //             }
  //           }
  //         }
  //       }
  //     }
  //   }
  // }

  // descOrder(a, b) {
  //   if (a.key < b.key) {
  //     return b.key;
  //   }
  // }

  public checkIsInValid(formGourp: AbstractControl, controlName: string) {
    return this.formCheckService.checkIsInValid(formGourp, controlName);
  }

  ngOnDestroy(): void {
    this.alive = false;
  }
}
