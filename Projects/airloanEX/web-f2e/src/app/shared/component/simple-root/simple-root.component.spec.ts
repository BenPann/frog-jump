import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleRootComponent } from './simple-root.component';

describe('SimpleRootComponent', () => {
  let component: SimpleRootComponent;
  let fixture: ComponentFixture<SimpleRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
