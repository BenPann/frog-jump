import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FillData3Component } from './fill-data3.component';

describe('FillData3Component', () => {
  let component: FillData3Component;
  let fixture: ComponentFixture<FillData3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FillData3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FillData3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
