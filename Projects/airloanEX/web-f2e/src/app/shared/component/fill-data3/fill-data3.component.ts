import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserDataService} from 'src/app/service/user-data/user-data.service';
import {CommonDataService} from '../../service/common/common-data.service';
import {filter, takeUntil, takeWhile} from 'rxjs/operators';
import {DropdownItem} from 'src/app/interface/resultType';
import {FormCheckService} from '../../service/common/form-check.service';
import {ApplyAccountService} from '../../../service/apply-account/apply-account.service';
import {AuthInfo, BasicFillDataReq} from 'src/app/service/apply-account/apply-account.interface';
import {ModalFactoryService} from '../../service/common/modal-factory.service';
import {TermsService} from '../../../service/terms/terms.service';
import {Subject} from 'rxjs';
import {BookBranchInfo, BranchBankInfo, AppointmentInfo } from 'src/app/service/product/product.interface';
import { RouterDirectService } from 'src/app/service/router/router-direct.service';
import { OtpService } from 'src/app/service/otp/otp.service';
import { EntryService } from 'src/app/service/entry/entry.service';

@Component({
  selector: 'app-common-fill-data3',
  templateUrl: './fill-data3.component.html',
  styleUrls: ['./fill-data3.component.scss']
})
export class FillData3Component implements OnInit, OnDestroy {
  @Input() hideButton = false;
  @Input() hideStepBar = false;
  @Input() termsUrl;
  hideEstate = false;
  @Output() back = new EventEmitter<void>();
  @Output() next = new EventEmitter<void>();

  stepbar = 3;
  stepStr = 3;
  leftBtnStr = '上一步';

  dataForm: FormGroup;

  private alive = true;

  private unsubscribe: Subject<void> = new Subject();

  authInfoName: string;

  estateTypeList: DropdownItem[];
  educationList: DropdownItem[];
  sendTypeList: DropdownItem[];
  purposeList: DropdownItem[];
  showAuthInfo = true;
  authInfo: AuthInfo;
  branchID: string;
  // emailErrorMsg: string;

  // 選分行的子元件input
  selectedBranchInfo: BranchBankInfo;
  bookBranchInfo: BookBranchInfo;
  errorMsgEmail: string;

  constructor(
    private fb: FormBuilder,
    private userDataService: UserDataService,
    private entryService: EntryService,
    private commonDataService: CommonDataService,
    private routerDirectService: RouterDirectService,
    public formCheckService: FormCheckService,
    public otpService: OtpService,
    private applyAccountService: ApplyAccountService,
    private modalFactoryService: ModalFactoryService,
    private termsService: TermsService
  ) {}

  ngOnInit() {
    // 取得先前填寫的資料
    let basicData = this.userDataService.basicFillDataReq;
    console.log(basicData);
    if (basicData && basicData.emailAddress) {
      console.log(basicData);
      this.branchID = basicData.branchID;
      // 1.email中壽帶入(需 隱碼)
      // tslint:disable-next-line: max-line-length
      if (this.entryService.isCL() && this.routerDirectService.passID === 'Y' && this.userDataService.chinaLifeCifInfo && basicData.emailAddress.length > 0 &&
       // 未更改的話就隱碼
       this.userDataService.chinaLifeCifInfo.email === basicData.emailAddress) {
        basicData.emailAddress = this.maskEmailAddress(basicData.emailAddress);
      }
    } else {
      basicData = {
        emailAddress: '',
        sendType: '',
        resTelArea: '',
        resTel: '',
        homeTelArea: '',
        homeTel: '',
        education: '',
        estateType: '',
        purpose: '',
        authorizeToallCorp: '',
        autVer: '',
        checkTerms: '',
        branchID: ''
      };
      // tslint:disable-next-line: max-line-length
      // 1.email中壽帶入(需 隱碼) 2.居住電話中壽帶入
      if (this.entryService.isCL() && this.routerDirectService.passID === 'Y' && this.userDataService.chinaLifeCifInfo) {
        basicData.emailAddress =
        this.userDataService.chinaLifeCifInfo.email && this.userDataService.chinaLifeCifInfo.email.length > 0 ?
        this.maskEmailAddress(this.userDataService.chinaLifeCifInfo.email)
         : '';
        basicData.resTelArea =
          this.userDataService.chinaLifeCifInfo.homeTelArea || '';
        basicData.resTel = this.userDataService.chinaLifeCifInfo.homeTel || '';
        basicData.homeTelArea =
          this.userDataService.chinaLifeCifInfo.homeTelArea || '';
        basicData.homeTel = this.userDataService.chinaLifeCifInfo.homeTel || '';
      } else {
        basicData.emailAddress =
          this.userDataService.creditCardCifInfo.email || '';
        basicData.resTelArea =
          this.userDataService.creditCardCifInfo.resTelArea || '';
        basicData.resTel = this.userDataService.creditCardCifInfo.resTel || '';
        basicData.homeTelArea =
          this.userDataService.creditCardCifInfo.homeTelArea || '';
        basicData.homeTel = this.userDataService.creditCardCifInfo.homeTel || '';

      }

      basicData.education =
        this.userDataService.creditCardCifInfo.education || '';
      basicData.estateType =
        this.userDataService.creditCardCifInfo.estateType || '';
    }

    // 共銷勾選後就不顯示
    // if (basicData.authorizeToallCorp === 'Y') {
    //   this.showAuthInfo = false;
    // }

    this.dataForm = this.fb.group({
      checkTerms: basicData.checkTerms || '',
      email: [
        basicData.emailAddress,
        [
          Validators.required,
          Validators.email,
          this.formCheckService.checkRepeatEmail(
            this.userDataService.usedPhoneAndEmail.emailAddress
          )
        ]
      ],
      sendType: [basicData.sendType, Validators.required],
      resTelArea: [
        basicData.resTelArea,
        [Validators.required, this.formCheckService.validatorTelAreaCode]
      ],
      resTel: [
        basicData.resTel,
        [Validators.required, Validators.minLength(6), Validators.maxLength(8)]
      ],
      homeTelArea: [
        basicData.homeTelArea,
        [Validators.required, this.formCheckService.validatorTelAreaCode]
      ],
      homeTel: [
        basicData.homeTel,
        [Validators.required, Validators.minLength(6), Validators.maxLength(8)]
      ],
      education: [basicData.education, Validators.required],
      estateType: [basicData.estateType],
      purpose: [basicData.purpose, Validators.required],
      authInfo: basicData.authorizeToallCorp === 'Y' ? 'Y' : '',
      branchInfo: this.fb.group({
        branchAddrCity: ['', Validators.required],
        branchAddrDist: ['', Validators.required],
        branchName: ['', Validators.required],
        branchAddrZipCode: [''],
        branchAddr: [''],
        branchID: ['']
      })
    });

    this.dataForm
      .get('resTelArea')
      .valueChanges.pipe(
        filter(data => !!data),
        takeUntil(this.unsubscribe)
      )
      .subscribe(data => {
        if (this.dataForm.get('checkTerms').value) {
          this.dataForm
            .get('homeTelArea')
            .setValue(this.dataForm.get('resTelArea').value);
        }
      });

    this.dataForm
      .get('resTel')
      .valueChanges.pipe(
        filter(data => !!data),
        takeUntil(this.unsubscribe)
      )
      .subscribe(data => {
        if (this.dataForm.get('checkTerms').value) {
          this.dataForm
            .get('homeTel')
            .setValue(this.dataForm.get('resTel').value);
        }
      });

    this.formCheckService.markTouchedWhenHaveValue(this.dataForm);

    this.commonDataService
      .getDropdownData('estate')
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.estateTypeList = data;
      });

    this.commonDataService
      .getDropdownData('eopeducation')
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.educationList = data;
      });

    this.commonDataService
      .getDropdownData('eopbillType')
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.sendTypeList = data;
      });

    this.commonDataService
      .getDropdownData('eoppurpose')
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.purposeList = data;
      });

    this.termsService
      .getTermList(this.termsUrl)
      .pipe(takeWhile(() => this.alive))
      .subscribe(r => {
        this.authInfoName = r.find(value => value.type === '1').termName;
        this.applyAccountService
          .getAuthInfo(this.authInfoName)
          .pipe(takeWhile(() => this.alive))
          .subscribe(data => {
            if (data.status === 0) {
              this.authInfo = data.result;
              if (this.authInfo && this.authInfo.authorizeToallCorp === 'Y') {
                this.showAuthInfo = false;
                this.authInfo.authorizeToallCorp = '';
                this.authInfo.verNo = '';
              }
            }
          });
      });

      if (this.isCreditCardChoosed()) {
        this.dataForm.get('estateType').setValidators([Validators.required]);
      }

    this.isAddressDisabled();
    // 是否有之前的資料
    const reqData = this.userDataService.appointmentInfo;
    if (reqData && reqData.branchID) {
      this.selectedBranchInfo = {
        branchAddrZipCode: reqData.branchAddrZipCode,
        branchAddrCity: reqData.branchAddrCity,
        branchAddrDist: reqData.branchAddrDist,
        branchAddr: reqData.branchAddr,
        branchID: reqData.branchID,
        branchName: reqData.branchName
      };
    }
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  asResPhone(event) {
    if (event.target.checked) {
      this.dataForm
        .get('homeTelArea')
        .setValue(this.dataForm.get('resTelArea').value);
      this.dataForm.get('homeTel').setValue(this.dataForm.get('resTel').value);
      this.dataForm.get('homeTel').disable();
      this.dataForm.get('homeTelArea').disable();
    } else {
      this.dataForm.get('homeTel').enable();
      this.dataForm.get('homeTelArea').enable();
    }
  }

  // 數三無 不動產所有
  isCreditCardChoosed(): boolean {
    return this.userDataService.newUserChooseProductReq.creditType === '2';
  }

  // BUG #1303 一般帳戶,預約分行，eMail不需檢核唯一性，只需檢核EMail之正確性
  hasAppointment(): boolean {
    return this.routerDirectService.previousStep === '3';
  }

  // 已預約分行，則鎖住欄位
  isAddressDisabled(): void {
    if (
      this.routerDirectService.previousStep === '3' ||
      this.routerDirectService.appointmentPrevStep === 'chooseAccountType'
    ) {
        this.dataForm
          .get('branchInfo')
          .get('branchName')
          .disable();
        this.dataForm
          .get('branchInfo')
          .get('branchAddrDist')
          .disable();
        this.dataForm
          .get('branchInfo')
          .get('branchAddrCity')
          .disable();
    }
  }

  showTerms() {
    this.termsService
      .getTerms(this.authInfoName)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        let termText = '';
        try {
          // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
          const json = JSON.parse(data);
          termText = '查詢條款失敗';
        } catch (error) {
          termText = data;
        }
        this.modalFactoryService.modalHtml({ message: termText });
      });
  }

  clearErrorMsg(): void {
    this.errorMsgEmail = '';
  }

  maskEmailAddress(mailAddr: string): string {
    // tslint:disable-next-line: max-line-length
    return mailAddr.length > 0 ? mailAddr.split('@')[0].replace(mailAddr.split('@')[0].substr(Math.round((mailAddr.split('@')[0].length) / 2)  - 2, 3), '*'.repeat(3)) + '@' +  mailAddr.split('@')[1] : '';
  }

  validateEmail(): void {
    this.otpService
      // tslint:disable-next-line: max-line-length
      .isEmailSuccess(!(this.dataForm.value.email.indexOf('*') > -1)
      // && this.maskEmailAddress(this.userDataService.chinaLifeCifInfo.email) === this.dataForm.value.email
      // tslint:disable-next-line: max-line-length
      ? this.dataForm.value.email : this.userDataService.chinaLifeCifInfo.email && this.userDataService.chinaLifeCifInfo.email.length > 0 ? this.userDataService.chinaLifeCifInfo.email : ''  )
      .pipe(takeWhile(() => this.alive))
      .subscribe(data2 => {
        if (data2.status === 0 && data2.result === 'Y') {
        } else if (data2.status === 0 && data2.result === 'N') {
            this.errorMsgEmail = '電子郵件信箱' + this.dataForm.value.email + '已有其他客戶申請使用，請確認您的電子郵件信箱是否正確或洽各分行辦理開戶，謝謝您！' ;
            this.dataForm.get('email').setErrors({'incorrect': true});
          } else {
            this.errorMsgEmail = data2.message ;
            this.dataForm.get('email').setErrors({'incorrect': true});
        }
      });
  }

  prevStep() {
    this.back.emit();
  }

  nextStep() {
    if (this.dataForm.invalid) {
      return;
    }

    // 預約分行不驗證 email 唯一性
    if (this.hasAppointment()) {
      const req = this.getBasicFillData();

      this.applyAccountService
        .setBasicData(req)
        .pipe(takeWhile(() => this.alive))
        .subscribe(data => {
          if (data.status === 0) {
            // if (this.authInfo && this.authInfo.authorizeToallCorp === '') {
            //   req.authorizeToallCorp = 'Y';  // session
            // }
            this.userDataService.basicFillDataReq = req;
            this.keepAddress();
            this.next.emit();
          } else {
            this.modalFactoryService.modalDefault({
              message: data.message
            });
          }
        });
    } else {
      this.otpService
        .isEmailSuccess(this.dataForm.value.email)
        .pipe(takeWhile(() => this.alive))
        .subscribe(data2 => {
          if (data2.status === 0 && data2.result === 'Y') {
            const req = this.getBasicFillData();

            this.applyAccountService
              .setBasicData(req)
              .pipe(takeWhile(() => this.alive))
              .subscribe(data => {
                if (data.status === 0) {
                  // if (this.authInfo && this.authInfo.authorizeToallCorp === '') {
                  //   req.authorizeToallCorp = 'Y';
                  // }
                  this.userDataService.basicFillDataReq = req;
                  this.keepAddress();
                  this.next.emit();
                } else {
                  this.modalFactoryService.modalDefault({
                    message: data.message
                  });
                }
              });
          } else if (data2.status === 0 && data2.result === 'N') {
            return;
          } else {
            return;
          }
        });
    }
  }

  keepAddress() {
    const formVal = this.dataForm.getRawValue();
    // 是否有之前的資料
    const reqData = this.userDataService.appointmentInfo;
    if (reqData && reqData.branchID) {
      const addressInfo: AppointmentInfo = {
        phone: reqData.phone,
        bookBranchInfo: reqData.bookBranchInfo,
        branchAddrCity: formVal.branchInfo.branchAddrCity,
        branchAddrDist: formVal.branchInfo.branchAddrDist,
        branchName: formVal.branchInfo.branchName,
        branchAddrZipCode: formVal.branchInfo.branchAddrZipCode,
        branchAddr: formVal.branchInfo.branchAddr,
        branchID: formVal.branchInfo.branchID
      };
      this.userDataService.appointmentInfo = addressInfo;
    } else {
      const addressInfo: AppointmentInfo = {
        phone: '',
        bookBranchInfo: this.bookBranchInfo,
        branchAddrCity: formVal.branchInfo.branchAddrCity,
        branchAddrDist: formVal.branchInfo.branchAddrDist,
        branchName: formVal.branchInfo.branchName,
        branchAddrZipCode: formVal.branchInfo.branchAddrZipCode,
        branchAddr: formVal.branchInfo.branchAddr,
        branchID: formVal.branchInfo.branchID
      };
      this.userDataService.appointmentInfo = addressInfo;
    }
  }

  public getDataFormValid() {
    return this.dataForm.valid;
  }

  public getBasicFillData(): BasicFillDataReq {
    const formData = this.dataForm.getRawValue();
    const req: BasicFillDataReq = {
      // tslint:disable-next-line: max-line-length
      emailAddress: !(formData.email.indexOf('*') > -1)
      // && this.maskEmailAddress(this.userDataService.chinaLifeCifInfo.email) === formData.email
      // tslint:disable-next-line: max-line-length
      ? formData.email : this.userDataService.chinaLifeCifInfo.email && this.userDataService.chinaLifeCifInfo.email.length > 0 ? this.userDataService.chinaLifeCifInfo.email : '',
      sendType: formData.sendType,
      estateType: formData.estateType,
      resTelArea: formData.resTelArea,
      resTel: formData.resTel,
      homeTelArea: formData.homeTelArea,
      homeTel: formData.homeTel,
      education: formData.education,
      authorizeToallCorp: '',
      autVer: '',
      purpose: formData.purpose,
      checkTerms: formData.checkTerms,
      branchID: formData.branchInfo.branchID
    };

    if (this.authInfo) {
      req.autVer = this.authInfo.verNo;
      req.authorizeToallCorp = this.authInfo.authorizeToallCorp;
    }

    // 若上次已勾選，皆傳空值
    if (this.authInfo && this.authInfo.authorizeToallCorp === '') {
      req.authorizeToallCorp = '';
      req.autVer = '';
      // 如果有勾選 且有取到共銷 再塞資料進去
    } else if (formData.authInfo && this.authInfo) {
      req.authorizeToallCorp = 'Y';
    }
    return req;
  }

  public getDataFormValue(saveToSessionStorage?: boolean): BasicFillDataReq {
    // const formData = this.dataForm.getRawValue();
    const basicFillData = this.getBasicFillData();
    console.log(basicFillData);
    if (saveToSessionStorage) {
      // if (this.authInfo && this.authInfo.authorizeToallCorp === '') {
      //   basicFillData.authorizeToallCorp = 'Y';
      // }
      this.userDataService.basicFillDataReq = basicFillData;
    }
    return basicFillData;
  }

  public updateEmailErrorMsg(): string {
    const email = this.dataForm.get('email');
    if (email.touched && !email.valid) {
      if (email.errors.repeatEmailCheck) {
        return '電子郵件信箱已有其他客戶申請使用，請確認您的電子郵件信箱是否正確或洽各分行辦理開戶，謝謝您！';
      } else if (email.errors.innerEmailCheck) {
        return '不得以公務信箱申請開戶，請重新輸入';
      } else if (email.value === '') {
        return '請輸入電子郵件信箱';
      } else {
        // return '請輸入正確的Email';
        return '';
      }
    }
    return '';
  }
}
