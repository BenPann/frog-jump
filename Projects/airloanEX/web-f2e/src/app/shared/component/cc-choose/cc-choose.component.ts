import {AfterViewInit, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {takeWhile} from 'rxjs/operators';
import {FormCheckService} from '../../service/common/form-check.service';
import {ApplyAccountService} from 'src/app/service/apply-account/apply-account.service';
import {ModalFactoryService} from '../../service/common/modal-factory.service';
import {CreditCardView} from 'src/app/service/apply-account/apply-account.interface';
import {SwiperConfigInterface} from 'ngx-swiper-wrapper';
import {TermsService} from 'src/app/service/terms/terms.service';
import {TermsInput} from '../../../interface/resultType';
import { RouterDirectService } from '../../../service/router/router-direct.service';
import { UserDataService } from '../../../service/user-data/user-data.service';

@Component({
  selector: 'app-cc-choose',
  templateUrl: './cc-choose.component.html',
  styleUrls: ['./cc-choose.component.scss']
})
export class CcChooseComponent implements OnInit, AfterViewInit, OnDestroy {
  @Output() afterChoose = new EventEmitter<boolean>();

  @Output() backStep = new EventEmitter<void>();

  // 表單
  form: FormGroup;

  // 告知事項區塊開闔
  isLookdocOpen = false;

  termsDiv: TermsInput[];

  creditCardList: CreditCardView[] = [];
  selectedCard: CreditCardView;
  public index: number;
  public config: SwiperConfigInterface = {
    a11y: true,
    direction: 'horizontal',
    slidesPerView: 'auto',
    centeredSlides: true,
    spaceBetween: 30,
    keyboard: true,
    mousewheel: true,
    scrollbar: false,
    navigation: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    },
    effect: 'coverflow',
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: false
    }
  };
  private alive = true;

  constructor(
    private fb: FormBuilder,
    private userDataService: UserDataService,
    private routerDirectService: RouterDirectService,
    public formCheckService: FormCheckService,
    private applyAccountService: ApplyAccountService,
    private modalFactoryService: ModalFactoryService,
    private termsService: TermsService
  ) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      ccType: ['', Validators.required],
      agreement: false
    });
    // if (
    //   this.userDataService.shortUrlData &&
    //   this.userDataService.shortUrlData.creditCardList &&
    //   this.userDataService.shortUrlData.creditCardList.length > 0
    // ) {
    //   const tempList = this.userDataService.shortUrlData.creditCardList;
    //   this.applyAccountService
    //     .getApplyedCreditCardList()
    //     .pipe(takeWhile(() => this.alive))
    //     .subscribe(data => {
    //       if (data.status === 0) {
    //         const applyList: [{ cardType: string }] = data.result;
    //         if (applyList.length > 0) {
    //           this.creditCardList = tempList.filter(
    //             // 濾掉此人已經申請過的信用卡
    //             value => !applyList.find(v => v.cardType === value.cardType)
    //           );
    //         } else {
    //           this.creditCardList = tempList;
    //         }
    //         this.setCreditCardDefault();
    //       } else {
    //         this.modalFactoryService.modalDefault({message: data.message});
    //       }
    //     });
    // } else {
    this.applyAccountService.getCreditCardList()
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {

        if (result.status === 0) {
          this.creditCardList = result.result;
          this.setCreditCardDefault();
        }
      });
    // }
    // this.initTerms();
  }

  initTerms() {
    this.termsService.getTermList().pipe(takeWhile(() => this.alive)).subscribe(data => {
      this.termsDiv = data.filter(terms => terms.type === '2');
    });
  }

  setCreditCardDefault() {
    // 先確定還有沒有卡片可以申請
    if (this.creditCardList.length > 0) {
      if (this.userDataService.creditCardProductId) {
        this.index = this.creditCardList.findIndex((value, index, obj) => {
          return value.productId === this.userDataService.creditCardProductId;
        });
        if (this.index < 0) {
          this.index = 0;
        }
        this.selectedCard = this.creditCardList[this.index];
      } else {
        this.selectedCard = this.creditCardList[0];
      }
    } else {
      this.modalFactoryService
        .modalDefault({message: '尚無您可以申請的信用卡，請選擇帳戶申請'})
        .result.then(v => {
        this.backStep.emit();
      });
    }
  }

  ngAfterViewInit() {
  }

  changeCreditCard(index) {
    if (index > -1) {
      this.selectedCard = this.creditCardList[index];
    }
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  // 下一步按鈕上鎖及解鎖
  isNextDisabled(): boolean {
    // return !this.form.get('agreement').value;
    return false;
  }

  // 上一步按鈕上鎖及解鎖
  isPrevDisabled(): boolean {
    // 前一頁面是存戶和信用卡不可點上一步
    return '123'.indexOf(this.routerDirectService.previousStep) > -1;
  }

  back(): void {
    this.backStep.emit();
  }

  next(): void {
  //   this.termsService
  //     .updateCaseDataTerms(this.termsDiv.map(value => {
  //       return {
  //         termName: value.termName,
  //         check: 'Y'
  //       };
  //     }))
  //     .pipe(takeWhile(() => this.alive))
  //     .subscribe(data => {
  //       if (data.status === 0) {
  //   } else {
  //     this.modalFactoryService.modalDefault({message: data.message});
  //   }
  // });
          this.applyAccountService
            .chooseCardView(this.selectedCard.productId)
            .pipe(takeWhile(() => this.alive))
            .subscribe(data2 => {
              if (data2.status === 0) {
                this.userDataService.creditCardProductId = this.selectedCard.productId;
                this.afterChoose.emit(true);
              } else {
                this.modalFactoryService.modalDefault({
                  message: data2.message
                });
              }
            });
  }
}
