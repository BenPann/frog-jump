import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcChooseComponent } from './cc-choose.component';

describe('CcChooseComponent', () => {
  let component: CcChooseComponent;
  let fixture: ComponentFixture<CcChooseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcChooseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcChooseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
