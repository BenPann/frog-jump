import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FillData5Component } from './fill-data5.component';

describe('FillData5Component', () => {
  let component: FillData5Component;
  let fixture: ComponentFixture<FillData5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FillData5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FillData5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
