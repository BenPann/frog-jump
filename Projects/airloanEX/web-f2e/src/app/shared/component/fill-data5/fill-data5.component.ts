import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserDataService} from 'src/app/service/user-data/user-data.service';
import {FormCheckService} from '../../service/common/form-check.service';
import {ApplyAccountService} from 'src/app/service/apply-account/apply-account.service';
import {ModalFactoryService} from '../../service/common/modal-factory.service';
import {CommonDataService} from '../../service/common/common-data.service';
import {GiftData, GiftFillDataReq, PhotoView} from 'src/app/service/apply-account/apply-account.interface';
import {takeWhile} from 'rxjs/operators';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {DocumentService} from 'src/app/shared/service/document/document.service';
import {OtherDocumentResponse} from '../../service/document/document.interface';
import {DocumentResult, DropdownItem} from '../../../interface/resultType';
import {MappingService} from '../../../service/user-data/mapping.service';
import { EntryService } from 'src/app/service/entry/entry.service';

@Component({
  selector: 'app-common-fill-data5',
  templateUrl: './fill-data5.component.html',
  styleUrls: ['./fill-data5.component.scss']
})
export class FillData5Component implements OnInit, OnDestroy {
  @Input() hideButton = false;
  @Input() hideStepBar = false;

  @Output() back = new EventEmitter<string>();
  @Output() next = new EventEmitter<void>();


  // step步驟bar
  stepbar = 5;
  stepStr = 3;

  giftList: GiftData[] = [];

  showGiftList: boolean = this.userDataService.creditCardCifInfo.sourceType === '1'; // 1:信用卡戶

  selectedGift: GiftData;

  uploadedDocument: any[] = [];
  // 表單
  public dataForm: FormGroup;

  docTypeList: DropdownItem[];
  private alive = true;

  constructor(
    private fb: FormBuilder,
    private userDataService: UserDataService,
    public formCheckService: FormCheckService,
    public entryService: EntryService,
    private applyAccountService: ApplyAccountService,
    private modalFactoryService: ModalFactoryService,
    private dateFormatter: NgbDateParserFormatter,
    private documentService: DocumentService,
    private mappingService: MappingService,
  ) {
  }

  ngOnInit() {
    let giftData = this.userDataService.giftFillDataReq;
    if (!giftData) {
      giftData = {
        firstPresent: '',
      };
    }

    this.docTypeList = this.mappingService.docType;

    // 都會有的欄位
    this.dataForm = this.fb.group({
      docType: ['20']
    });

    // 看使用的人需不需要該欄位
    this.formCheckService.markTouchedWhenHaveValue(this.dataForm);
    this.applyAccountService
      .getGiftList()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.giftList = data.result;
          if (this.giftList.length > 0) {
            giftData.firstPresent = giftData.firstPresent
              ? giftData.firstPresent
              : this.giftList[0].giftCode;
            // 有首刷禮就是必填
            this.dataForm.addControl(
              'firstPresent',
              this.fb.control(giftData.firstPresent, [Validators.required])
            );
            // 設定上次選取的首刷禮
            if (giftData.firstPresent) {
              this.selectedGift = this.giftList.find((value, index, obj) => {
                return value.giftCode === giftData.firstPresent;
              });
            }
          } else {
            this.dataForm.addControl(
              'firstPresent',
              this.fb.control(giftData.firstPresent, [])
            );
          }
          // 設定事件
          this.dataForm
            .get('firstPresent')
            .valueChanges.pipe(takeWhile(() => this.alive))
            .subscribe(data3 => {
              this.selectedGift = this.giftList.find((value, index, obj) => {
                return value.giftCode === data3;
              });
            });
        } else {
          this.modalFactoryService.modalDefault({message: data.message});
        }
      });

    this.documentService
      .getPhotoList()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          const res: OtherDocumentResponse[] = data.result;
          res.forEach(element => {
            const find = this.docTypeList.find(value => {
              return value.dataKey === element.sType;
            });
            if (find) {
              this.uploadedDocument.push({
                subSerial: element.subSerial,
                docName: find.dataName,
                waterMarkImage: element.waterMarkImage
              });
            }
          });
        } else {
          this.modalFactoryService.modalDefault({message: data.message});
        }
      });

  }

  ngOnDestroy(): void {
    this.alive = false;
  }


  uploadFile(dr: DocumentResult) {
    const docType = this.dataForm.get('docType').value;
    const docName = this.docTypeList.find(value => {
      return value.dataKey === docType;
    }).dataName;
    const photoView: PhotoView = {
      sType: docType,
      base64Image: dr.image,
      subSerial: ''
    };
    this.documentService
      .upload(photoView)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          const res: OtherDocumentResponse = data.result;
          this.uploadedDocument.push({
            subSerial: res.subSerial,
            docName: docName,
            waterMarkImage: res.waterMarkImage
          });
        } else {
          this.modalFactoryService.modalDefault({message: data.message});
        }
      });
  }

  deleteUploadFile(serial) {
    this.documentService
      .deletePhoto(serial)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.uploadedDocument = this.uploadedDocument.filter(value => {
            return value.subSerial !== serial;
          });
        } else {
          this.modalFactoryService.modalDefault({message: data.message});
        }
      });
  }


  showPreview(image) {
    this.modalFactoryService.modalImage({
      message: image
    });
  }

  prevStep() {
    this.back.emit('backstring');
  }

  nextStep() {
    if (this.dataForm.invalid) {
      return;
    }
    const formData = this.dataForm.getRawValue();
    let req: GiftFillDataReq;

    req = {
      firstPresent: formData.firstPresent,
    };


    this.applyAccountService
      .setGiftData(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.userDataService.giftFillDataReq = req;
          this.next.emit();
        } else {
          this.modalFactoryService.modalDefault({message: data.message});
        }
      });
  }

  public getDataFormValid() {
    const formData = this.dataForm.value;
    if (!formData) {
      return false;
    }
    if (formData.quickHandle) {
      return this.dataForm.valid && this.uploadedDocument.length > 0;
    } else {
      return this.dataForm.valid;
    }
  }

  public getPrimaryButtonDisabled() {
    return {
      disabled: !this.getDataFormValid()
    };
  }

  public getDataFormValue(saveToSessionStorage?: boolean): GiftFillDataReq {
    const formData = this.dataForm.getRawValue();
    let req: GiftFillDataReq;
    req = {
      firstPresent: formData.firstPresent,
    };
    if (saveToSessionStorage) {
      this.userDataService.giftFillDataReq = req;
    }
    return req;
  }
}
