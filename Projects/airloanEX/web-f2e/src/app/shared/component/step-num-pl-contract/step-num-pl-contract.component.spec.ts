import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepNumPlContractComponent } from './step-num-pl-contract.component';

describe('StepNumPlContractComponent', () => {
  let component: StepNumPlContractComponent;
  let fixture: ComponentFixture<StepNumPlContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepNumPlContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepNumPlContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
