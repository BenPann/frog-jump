import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-step-num-pl-contract',
  templateUrl: './step-num-pl-contract.component.html',
  styleUrls: ['./step-num-pl-contract.component.scss']
})
export class StepNumPlContractComponent implements OnInit {
  @Input() step = 0;
  constructor() { }

  ngOnInit() {
  }

}
