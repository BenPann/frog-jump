import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FillDataStepBarComponent } from './fill-data-step-bar.component';

describe('FillDataStepBarComponent', () => {
  let component: FillDataStepBarComponent;
  let fixture: ComponentFixture<FillDataStepBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FillDataStepBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FillDataStepBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
