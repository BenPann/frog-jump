import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-fill-data-step-bar',
  templateUrl: './fill-data-step-bar.component.html',
  styleUrls: ['./fill-data-step-bar.component.scss']
})
export class FillDataStepBarComponent implements OnInit {
  @Input() stepbar: Number;
  constructor() {}

  ngOnInit() {}
}
