import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FillData4Component } from './fill-data4.component';

describe('FillData4Component', () => {
  let component: FillData4Component;
  let fixture: ComponentFixture<FillData4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FillData4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FillData4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
