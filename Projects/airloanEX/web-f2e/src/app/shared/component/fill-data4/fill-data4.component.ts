import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserDataService} from 'src/app/service/user-data/user-data.service';
import {FormCheckService} from '../../service/common/form-check.service';
import {ApplyAccountService} from '../../../service/apply-account/apply-account.service';
import {BcOcrRes, CorpFillDataReq} from 'src/app/service/apply-account/apply-account.interface';
import {takeWhile} from 'rxjs/operators';
import {ModalFactoryService} from '../../service/common/modal-factory.service';
import {DocumentResult, DropdownItem, TermsInput} from '../../../interface/resultType';
import {CommonDataService} from '../../service/common/common-data.service';
import {NgbDateParserFormatter, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {MappingService} from '../../../service/user-data/mapping.service';
import {TermsService} from '../../../service/terms/terms.service';
import {DocumentService} from '../../service/document/document.service';
import {TwZipCodeComponent} from '../tw-zip-code/tw-zip-code.component';
import { DatepickerService } from 'src/app/shared/service/common/datepicker.service';
import { EntryService } from 'src/app/service/entry/entry.service';

@Component({
  selector: 'app-common-fill-data4',
  templateUrl: './fill-data4.component.html',
  styleUrls: ['./fill-data4.component.scss']
})
export class FillData4Component implements OnInit, OnDestroy {
  @Input() hideButton = false;
  @Input() hideStepBar = false;
  @Input() termsUrl;
  @Output() back = new EventEmitter<void>();
  @Output() next = new EventEmitter<void>();

  @ViewChild('appZipCode', { static: false })
  public appZipCode: TwZipCodeComponent;

  stepbar = 4;
  stepStr = 3;
  leftBtnStr = '上一步';
  occupationList: DropdownItem[];
  jobTitleList: DropdownItem[];
  dataForm: FormGroup;
  // 日期
  maxDate: NgbDateStruct;
  minDate: NgbDateStruct;
  today = new Date();
  private alive = true;

  terms: TermsInput[];

  showCorpDetail = false;
  corpNameErrorMessage: string;

  constructor(
    private fb: FormBuilder,
    private userDataService: UserDataService,
    public formCheckService: FormCheckService,
    private applyAccountService: ApplyAccountService,
    private modalFactoryService: ModalFactoryService,
    private commonDataService: CommonDataService,
    private datepickerService: DatepickerService,
    private dateFormatter: NgbDateParserFormatter,
    private entryService: EntryService,
    private mappingService: MappingService,
    private termsService: TermsService,
    private documentService: DocumentService
  ) {}

  ngOnInit() {
    let corpData = this.userDataService.corpFillDataReq;
    if (corpData) {
      corpData.agreement = corpData.agreement === '' ? '' : corpData.agreement === 'N' ? 'false' : 'true';
    } else {
      corpData = {
        corpName: '',
        corpTelArea: '',
        corpTel: '',
        corpAddrZipCode: '',
        corpAddrArea: '',
        corpAddr: '',
        occupation: '',
        jobTitle: '',
        yearlyIncome: '',
        onBoardDate: '',
        injury: '',
        agreement: ''
      };
      corpData.corpName = this.userDataService.creditCardCifInfo.corpName || '';
      if (!this.entryService.isCL()) {
        corpData.corpTelArea =
          this.userDataService.creditCardCifInfo.corpTelArea || '';
        corpData.corpTel = this.userDataService.creditCardCifInfo.corpTel || '';
        // 公司電話中壽帶入
      } else {
        // corpData.corpTelArea =
        // this.userDataService.chinaLifeCifInfo.corpTelArea || '';
        // corpData.corpTel = this.userDataService.chinaLifeCifInfo.corpTel || '';
      }
      corpData.corpAddrZipCode =
      this.userDataService.creditCardCifInfo.corpAddrZipCode || '';
      corpData.corpAddr = this.userDataService.creditCardCifInfo.corpAddr || '';
      // 由於帶過來的地址會含有縣市區域  所以要把它取代掉
      const corpDis = this.mappingService.findAddressByZipCode(
        corpData.corpAddrZipCode
      );
      corpData.corpAddr = corpData.corpAddr.replace(corpDis, '');

      corpData.occupation = '';
      corpData.yearlyIncome =
        this.userDataService.creditCardCifInfo.yearlyIncome || '';
      corpData.onBoardDate =
        this.userDataService.creditCardCifInfo.onBoardDate || '';
    }

    this.dataForm = this.fb.group({
      corpName: [
        corpData.corpName,
        [
          Validators.required,
          Validators.pattern(/^[\u4e00-\u9fa5a-zA-Z0-9\-]*$/)
        ]
      ],
      corpTelArea: [
        corpData.corpTelArea,
        [Validators.required, this.formCheckService.validatorTelAreaCode]
      ],
      corpTel: [
        corpData.corpTel,
        [Validators.required, Validators.minLength(6), Validators.maxLength(8)]
      ],
      occupation: [corpData.occupation, [Validators.required]],
      jobTitle: [corpData.jobTitle, [Validators.required]],
      yearlyIncome: [
        corpData.yearlyIncome,
        [Validators.required, Validators.min(1)]
      ],
      injury: [corpData.injury],
      agreement: [corpData.agreement],
      onBoardDate: [this.dateFormatter.parse(corpData.onBoardDate)],
      corpAddr: this.fb.group({
        county: [''],
        district: [''],
        zip: [corpData.corpAddrZipCode],
        addressOther: [corpData.corpAddr]
      })
    });

    if (this.isCreditCardChoosed()) {
      this.showCorpDetail = true;
      this.dataForm
        .get('corpAddr')
        .get('county')
        .setValidators([Validators.required]);
      this.dataForm
        .get('corpAddr')
        .get('district')
        .setValidators([Validators.required]);
      this.dataForm
        .get('corpAddr')
        .get('zip')
        .setValidators([
          Validators.required,
          Validators.minLength(3),
          Validators.pattern(/^[0-9]{3}$/)
        ]);
      this.dataForm
        .get('corpAddr')
        .get('addressOther')
        .setValidators([
          Validators.required,
          Validators.minLength(2),
          Validators.pattern(/^[\u4e00-\u9fa5a-zA-Z0-9\-]*$/)
        ]);
    } else {
      this.showCorpDetail = false;
    }

    this.formCheckService.markTouchedWhenHaveValue(this.dataForm);

    this.commonDataService
      .getDropdownData('eopoccupation')
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.occupationList = data;
      });
    this.commonDataService
      .getDropdownData('eopjobtitle')
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.jobTitleList = data;
      });

    this.initdatepicker();
    this.initTerms();

    this.dataForm.controls['corpName'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.corpNameErrorMessageUpdate();
      });

    if (corpData.agreement === 'false') {
      this.dataForm.get('agreement').patchValue(false);
    } else if (corpData.agreement === 'true') {
      this.dataForm.get('agreement').patchValue(true);
    }

    this.dataForm
      .get('injury')
      .valueChanges.pipe(takeWhile(() => this.alive))
      .subscribe(value => {
        if (value === 'Y') {
          this.dataForm.get('agreement').patchValue('');
        } else {
          this.dataForm.get('agreement').patchValue(false);
        }
      });
  }

  corpNameErrorMessageUpdate() {
    const ctl = this.dataForm.controls['corpName'];
    let message = '';
    if (ctl.touched) {
      if (ctl.valid) {
        message = '';
      } else {
        if (ctl.errors.required) {
          message = '請輸入公司名稱';
        } else if (ctl.errors.pattern) {
          message = '僅能輸入中、英、數字及"-"符號';
        }
      }
    } else {
      message = '';
    }
    this.corpNameErrorMessage = message;
  }

  initTerms(): void {
    this.termsService.getTermList(this.termsUrl).subscribe(data => {
      this.terms = data.filter(value => value.type === '2');
    });
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  // 數三無公司地址、 到職日
  isCreditCardChoosed(): boolean {
    return this.userDataService.newUserChooseProductReq.creditType === '2';
  }

  // 日期設定init
  initdatepicker(): void {
    const y = this.today.getFullYear();
    const m = this.today.getMonth() + 1;
    const d = this.today.getDate();

    this.maxDate = {
      year: y,
      month: m,
      day: d
    };
    this.minDate = {
      year: +this.userDataService.initRqData.birthday.substr(0, 4),
      month: +this.userDataService.initRqData.birthday.substr(4, 2),
      day: +this.userDataService.initRqData.birthday.substr(6, 2)
    };
  }

  bcOcr(e: DocumentResult) {
    console.log(e);
    let bcOcr: BcOcrRes;
    this.documentService
      .ocr(e.image)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          console.log(data.result);
          bcOcr = data.result;
          if (this.userDataService.newUserChooseProductReq.creditType === '2') {
            this.appZipCode.setZip(
              bcOcr.ZIPCODE ||
                this.dataForm.get('corpAddr').get('zip').value ||
                '',
              true
            );
          }
          this.dataForm.patchValue({
            corpName:
              bcOcr.CompanyName || this.dataForm.get('corpName').value || '',
            corpTelArea:
              bcOcr.CompanyTelNum1 ||
              this.dataForm.get('corpTelArea').value ||
              '',
            corpTel:
              bcOcr.CompanyTelNum2 || this.dataForm.get('corpTel').value || '',
            corpAddr: {
              // zip: this.dataForm.get('corpAddr').get('zip').value || bcOcr.ZIPCODE || '',
              addressOther:
                bcOcr.CompanyAddr3 ||
                this.dataForm.get('corpAddr').get('addressOther').value ||
                ''
            }
          });
        }
        Object.keys(this.dataForm.controls).forEach(key => {
          const control = this.dataForm.controls[key];
          this.setTouched(control);
        });
      });
  }

  setTouched(control: any) {
    if (control instanceof FormGroup) {
      Object.keys((<FormGroup>control).controls).forEach(key => {
        this.setTouched((<FormGroup>control).controls[key]);
      });
    } else if (control instanceof FormControl) {
      if (control.value) {
        control.markAsTouched();
      }
    }
  }

  prevStep() {
    this.back.emit();
  }

  isRecommanded() {
    // tslint:disable-next-line: max-line-length
    if (
      !this.datepickerService.checkAge(this.minDate, 70) &&
      this.userDataService.basicFillDataReq.education < '6' &&
      this.dataForm.value.injury === 'N'
    ) {
      return true;
    } else {
      return false;
    }
  }

  isQualified() {
    if (this.entryService.isCL()) {
      return false;
    }
    // tslint:disable-next-line: max-line-length
    if (
      !this.datepickerService.checkAge(this.minDate, 70) &&
      this.userDataService.basicFillDataReq.education < '6'
    ) {
      return true;
    } else {
      // default
      this.dataForm.get('injury').patchValue('');
      this.dataForm.get('agreement').patchValue('');
      return false;
    }
  }

  nextStep() {
    if (this.dataForm.invalid) {
      return;
    }
    const formData = this.dataForm.getRawValue();
    const req: CorpFillDataReq = {
      corpName: formData.corpName,
      corpTelArea: formData.corpTelArea,
      corpTel: formData.corpTel,
      corpAddrZipCode: formData.corpAddr.zip,
      corpAddrArea: formData.corpAddr.county + formData.corpAddr.district,
      corpAddr: formData.corpAddr.addressOther,
      occupation: formData.occupation,
      jobTitle: formData.jobTitle,
      yearlyIncome: formData.yearlyIncome,
      onBoardDate: this.dateFormatter.format(formData.onBoardDate),
      injury: formData.injury,
      agreement: formData.agreement === '' ? '' : formData.agreement ? 'Y' : 'N'
    };

      // 先更新條款在更新資料
      this.termsService
        .updateCaseDataTerms(
          this.terms.map(value => {
              return {
                termName: value.termName,
                check: formData.agreement === '' ? '' : formData.agreement ? 'Y' : 'N'
              };
          })
        )
        .pipe(takeWhile(() => this.alive))
        .subscribe(data2 => {
          if (data2.status === 0) {
            this.applyAccountService
              .setCorpData(req)
              .pipe(takeWhile(() => this.alive))
              .subscribe(data => {
                if (data.status === 0) {
                  this.userDataService.corpFillDataReq = req;
                  this.next.emit();
                } else {
                  this.modalFactoryService.modalDefault({
                    message: data.message
                  });
                }
              });
          } else {
            this.modalFactoryService.modalDefault({ message: data2.message });
          }
        });
  }

  public getDataFormValid() {
    return this.dataForm.valid;
  }

  public getDataFormValue(saveToSessionStorage?: boolean): CorpFillDataReq {
    const formData = this.dataForm.getRawValue();
    const req: CorpFillDataReq = {
      corpName: formData.corpName,
      corpTelArea: formData.corpTelArea,
      corpTel: formData.corpTel,
      corpAddrZipCode: formData.corpAddr.zip,
      corpAddrArea: formData.corpAddr.county + formData.corpAddr.district,
      corpAddr: formData.corpAddr.addressOther,
      occupation: formData.occupation,
      jobTitle: formData.jobTitle,
      yearlyIncome: formData.yearlyIncome,
      onBoardDate: this.dateFormatter.format(formData.onBoardDate),
      injury: formData.injury,
      agreement: formData.agreement === '' ? '' : formData.agreement ? 'Y' : 'N'
    };
    if (saveToSessionStorage) {
      this.userDataService.corpFillDataReq = req;
    }
    return req;
  }
}
