import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormGroup} from '@angular/forms';
import {takeWhile} from 'rxjs/operators';
import {FormCheckService} from 'src/app/shared/service/common/form-check.service';
import {BranchBankInfo} from 'src/app/service/product/product.interface';
import { ProductService } from 'src/app/service/product/product.service';

@Component({
  selector: 'app-bank-list3',
  templateUrl: './bank-list3.component.html',
  styleUrls: ['./bank-list3.component.scss']
})
export class BankList3Component implements OnInit, OnDestroy {
  // 所有資料列表
  public countyList: string[];
  // 區域列表
  public districtList: string[];
  // 分行列表
  public branchs: string[];

  // 父元件的表單
  @Input()
  public parentform: FormGroup;

  // 傳進的區域
  @Input()
  public branchInfo: BranchBankInfo;

  // 傳進的分行名
  @Input()
  public bankNameId: string;

  branchAddr: string;
  branchBankInfo: BranchBankInfo[];
  private setDefaultData: boolean;
  private alive = true;

  constructor(
    public formCheckService: FormCheckService,
    private productService: ProductService
  ) {
  }

  ngOnInit() {
    this.initEvent();
    // 取資料
    this.productService
      .getBranchBankInfo()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.branchBankInfo = data.result;
          // 取得清單內 所有的地區

          this.countyList = this.branchBankInfo
            .map(value => {
              return value.branchAddrCity;
            })
            .filter((value, index, self) => {
              return self.indexOf(value) === index;
            });
          if (this.branchInfo && this.branchInfo.branchID) {
            this.setDefaultData = true;

            const security = this.branchBankInfo.find(f => f.branchID === this.branchInfo.branchID);

            this.parentform
              .get('branchAddrCity')
              .setValue(security.branchAddrCity);
            this.parentform
              .get('branchAddrDist')
              .setValue(security.branchAddrDist);
            this.parentform
              .get('branchName')
              .setValue(security.branchName);
            this.setDefaultData = false;
          }
        }
      });
  }

  initEvent() {
    // 綁定事件
    this.parentform.controls['branchAddrCity'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {
        if (result) {
          this.districtList = this.branchBankInfo
            .filter(value => {
              return value.branchAddrCity === result;
            })
            .map(value => {
              return value.branchAddrDist;
            })
            .filter((value, index, self) => {
              return self.indexOf(value) === index;
            });
        } else {
          this.districtList = [];
          this.branchs = [];
        }
        if (!this.setDefaultData) {
          this.parentform.controls['branchAddrDist'].setValue('');
          this.parentform.controls['branchName'].setValue('');
        }
      });

    this.parentform.controls['branchAddrDist'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {
        if (result) {
          this.branchs = this.branchBankInfo
            .filter(value => {
              return (
                value.branchAddrCity ===
                this.parentform.controls['branchAddrCity'].value &&
                value.branchAddrDist === result
              );
            })
            .map(value => {
              return value.branchName;
            })
            .filter((value, index, self) => {
              return self.indexOf(value) === index;
            });
        } else {
          this.branchs = [];
        }
        this.parentform.controls['branchName'].setValue('');
      });

    this.parentform.controls['branchName'].valueChanges
      .pipe(takeWhile(() => this.alive))
      .subscribe(result => {
        if (result) {
          const selected = this.branchBankInfo.find(value => {
            return (
              value.branchAddrCity ===
              this.parentform.controls['branchAddrCity'].value &&
              value.branchAddrDist ===
              this.parentform.controls['branchAddrDist'].value &&
              value.branchName === result
            );
          });
          this.parentform.controls['branchAddrZipCode'].setValue(
            selected.branchAddrZipCode
          );
          this.parentform.controls['branchID'].setValue(
            selected.branchID
          );
          const city =  this.parentform.controls['branchAddrCity'].value;
          const district =  this.parentform.controls['branchAddrDist'].value;
          this.branchAddr = city + district + selected.branchAddr;
        } else {
          this.parentform.controls['branchAddrZipCode'].setValue('');
          this.parentform.controls['branchID'].setValue('');
          this.branchAddr = '';
        }
      });
  }

  public checkIsInValid(formGourp: AbstractControl, controlName: string) {
    return this.formCheckService.checkIsInValid(formGourp, controlName);
  }

  ngOnDestroy(): void {
    this.alive = false;
  }
}
