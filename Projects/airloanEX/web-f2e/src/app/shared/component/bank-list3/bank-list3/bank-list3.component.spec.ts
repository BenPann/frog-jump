import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankList3Component } from './bank-list3.component';

describe('BankList3Component', () => {
  let component: BankList3Component;
  let fixture: ComponentFixture<BankList3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankList3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankList3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
