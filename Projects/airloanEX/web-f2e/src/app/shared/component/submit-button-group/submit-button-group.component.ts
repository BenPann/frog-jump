import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DisableButtonParameter } from './submit-button-group.interface';

@Component({
  selector: 'app-submit-button-group',
  templateUrl: './submit-button-group.component.html',
  styleUrls: ['./submit-button-group.component.scss']
})
export class SubmitButtonGroupComponent implements OnInit {
  @Input() secondaryButtonText = '上一步';
  @Input() primaryButtonText = '下一步';
  @Input() secondaryButtonDisabled: DisableButtonParameter;
  @Input() primaryButtonDisabled: DisableButtonParameter;
  @Input() secondaryButtonDisabledForm: FormGroup;
  @Input() primaryButtonDisabledForm: FormGroup;
  @Input() secondaryButtonHidden = false;
  @Output() primaryButtonClick = new EventEmitter<void>();
  @Output() secondaryButtonClick = new EventEmitter<void>();
  constructor() {}

  ngOnInit() {}

  // 驗證的優先順序為 傳進來的form 再來是傳進來的參數 最後就是預設值開啟
  secondaryButtonisDisabled() {
    if (this.secondaryButtonDisabledForm) {
      return this.secondaryButtonDisabledForm.invalid;
    }

    if (this.secondaryButtonDisabled && this.secondaryButtonDisabled.disabled) {
      return this.secondaryButtonDisabled.disabled;
    }
    return false;
  }

  primaryButtonisDisabled() {
    if (this.primaryButtonDisabledForm) {
      return this.primaryButtonDisabledForm.invalid;
    }
    if (this.primaryButtonDisabled && this.primaryButtonDisabled.disabled) {
      return this.primaryButtonDisabled.disabled;
    }
    return false;
  }
}
