import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitButtonGroupComponent } from './submit-button-group.component';

describe('SubmitButtonGroupComponent', () => {
  let component: SubmitButtonGroupComponent;
  let fixture: ComponentFixture<SubmitButtonGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitButtonGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitButtonGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
