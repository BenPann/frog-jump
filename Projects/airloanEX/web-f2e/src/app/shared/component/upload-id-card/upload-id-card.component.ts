import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {DocumentResult} from 'src/app/interface/resultType';
import {takeWhile} from 'rxjs/operators';
import {ModalFactoryService} from '../../service/common/modal-factory.service';
import {IDCardResponse, OtherDocumentResponse} from 'src/app/shared/service/document/document.interface';
import {DocumentService} from '../../service/document/document.service';
import {UserDataService} from '../../../service/user-data/user-data.service';
import { RouterDirectService } from '../../../service/router/router-direct.service';
import { PhotoView } from 'src/app/service/apply-account/apply-account.interface';
import { ApplyAccountService } from 'src/app/service/apply-account/apply-account.service';
import { EntryService } from 'src/app/service/entry/entry.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';

@Component({
  selector: 'app-upload-id-card',
  templateUrl: './upload-id-card.component.html',
  styleUrls: ['./upload-id-card.component.scss']
})
export class UploadIdCardComponent implements OnInit, OnDestroy {
  // 第二證件是否顯示(舊戶不顯示)
  @Input() isSecCard = true;

  @Output() backStep = new EventEmitter<void>();
  @Output() afterUploadIdCard = new EventEmitter<boolean>();

  // 要傳入app-upload的身分證正面資料
  frontIdDoc: DocumentResult;
  // 要傳入app-upload的身分證反面資料
  backIdDoc: DocumentResult;
  // 要傳入app-upload的第二證件資料
  secCardIdDoc: DocumentResult;
  // 表單
  form: FormGroup;

  private alive = true;
  constructor(
    private fb: FormBuilder,
    private documentService: DocumentService,
    private entryService: EntryService,
    private routerDirectService: RouterDirectService,
    private routerHelperService: RouterHelperService,
    private modalFactoryService: ModalFactoryService,
    private applyAccountService: ApplyAccountService,
    private userDataService: UserDataService
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      frontId: '',
      frontIdSerial: '',
      backId: '',
      backIdSerial: '',
      secCardId: '',
      secCardIdSerial: ''
    });

    // init
    this.frontIdDoc = {
      name: 'frontId',
      image: '',
      title: ''
    };

    this.backIdDoc = {
      name: 'backId',
      image: '',
      title: ''
    };

    this.secCardIdDoc = {
      name: 'secCardId',
      image: '',
      title: ''
    };

    const req = this.userDataService.uploadDocument;
    if (req) {
      this.form.patchValue(req);
      this.frontIdDoc.image = req.frontId;
      this.backIdDoc.image = req.backId;
      this.secCardIdDoc.image = req.secCardId;
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }

  // 取得身分證正面
  getFrontIdImg(e: DocumentResult): void {
    const photoView: PhotoView = {
      sType: '1',
      base64Image: e.image,
      subSerial: this.form.get('frontIdSerial').value
    };
    this.documentService
      .upload(photoView)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          const result: IDCardResponse = data.result;
          this.userDataService.idCardResponse = result;
          this.form.get('frontId').setValue(result.waterMarkImage);
          this.form.get('frontIdSerial').setValue(result.subSerial);
          this.frontIdDoc = {
            name: 'frontId',
            image: result.waterMarkImage,
            title: '身分證正面'
          };
        } else {
          this.modalFactoryService.modalDefault({ message: data.message });
        }
      });
  }

  // 取得身分證反面
  getBackIdImg(e: DocumentResult): void {
    const photoView: PhotoView = {
      sType: '2',
      base64Image: e.image,
      subSerial: this.form.get('backIdSerial').value
    };
    this.documentService
      .upload(photoView)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          const result: IDCardResponse = data.result;
          this.userDataService.idCardResponse = result;
          this.form.get('backId').setValue(result.waterMarkImage);
          this.form.get('backIdSerial').setValue(result.subSerial);
          this.backIdDoc = {
            name: 'backId',
            image: result.waterMarkImage,
            title: '身分證反面'
          };
        } else {
          this.modalFactoryService.modalDefault({ message: data.message });
        }
      });
  }

  // 取得第二證件反面
  getSecCardImg(e: DocumentResult): void {
    const photoView: PhotoView = {
      sType: '3',
      base64Image: e.image,
      subSerial: this.form.get('secCardIdSerial').value
    };
    this.documentService
      .upload(photoView)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          const result: OtherDocumentResponse = data.result;
          this.form.get('secCardId').setValue(result.waterMarkImage);
          this.form.get('secCardIdSerial').setValue(result.subSerial);
          this.secCardIdDoc = {
            name: 'secCardId',
            image: result.waterMarkImage,
            title: '第二證件'
          };
        } else {
          this.modalFactoryService.modalDefault({ message: data.message });
        }
      });
  }

  // 上一步按鈕上鎖及解鎖
  isPrevDisabled(): boolean {
    if (this.userDataService.newUserChooseProductReq.creditType === '2') {
      return false;
    } else {
      return '123'.indexOf(this.routerDirectService.previousStep) > -1;
    }
  }

  // 下一步按鈕是否上鎖
  isNextDisabled(): boolean {
    const val = this.form.getRawValue();
    if (this.isSecCard) {
      return !(val.frontId && val.backId && val.secCardId);
    } else {
      return !(val.frontId && val.backId);
    }
  }

  back(): void {
    this.backStep.emit();
  }

  next(): void {
    if (this.form.valid) {
      this.userDataService.uploadDocument = this.form.value;
      // 通過驗身
      if (this.entryService.isCL() && this.routerDirectService.passID === 'Y') {
        // 只顯示一次中壽回來的訊息
        if (this.userDataService.prevToken === this.entryService.tokenOfChinaLife) {
          this.afterUploadIdCard.emit(true);
        }
        const goNext = [998, 999];
        // 客戶填的為主
        // 驗身過後才取得中壽個資
        this.applyAccountService
          .getCLCif(this.entryService.tokenOfChinaLife)
          .pipe(takeWhile(() => this.alive))
          .subscribe(data => {
            this.userDataService.prevToken = this.entryService.tokenOfChinaLife;
            if (data.status === 0) {
              this.userDataService.chinaLifeCifInfo = data.result;
              this.afterUploadIdCard.emit(true);
              // error
            } else if (
              goNext.indexOf(data.status) > -1
              ) {
                this.modalFactoryService
                .modalDefault({
                  message: data.message,
                  btnOK: '繼續申請'
                })
                .result.then(v2 => {
                  if (v2 === 'ok') {
                this.afterUploadIdCard.emit(true);
                  }
                });
            } else {
              this.modalFactoryService.modalDefault({ message: data.message });
              this.routerHelperService.navigate([
                '',
                'init'
              ]);
            }
          });
        } else {
          this.afterUploadIdCard.emit(true);
      }
    }
  }
}
