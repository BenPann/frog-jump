import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { FormCheckService } from 'src/app/shared/service/common/form-check.service';


@Component({
  selector: 'app-tw-phone',
  templateUrl: './tw-phone.component.html',
  styleUrls: ['./tw-phone.component.scss']
})
export class TwPhoneComponent implements OnInit {
  @Input()
  parentform: FormGroup;

  constructor(public formCheckService: FormCheckService) {}

  ngOnInit() {}

  public showError(formGourp: AbstractControl, controlName: string) {
    return this.formCheckService.checkIsInValid(formGourp, controlName);
  }
}
