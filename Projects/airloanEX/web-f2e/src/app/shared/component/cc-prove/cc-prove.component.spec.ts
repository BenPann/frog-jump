import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcProveComponent } from './cc-prove.component';

describe('CcProveComponent', () => {
  let component: CcProveComponent;
  let fixture: ComponentFixture<CcProveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcProveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcProveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
