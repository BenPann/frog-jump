import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
import { CommonDataService } from '../../service/common/common-data.service';
import { DropdownItem } from '../../../interface/resultType';
import { ModalFactoryService } from '../../service/common/modal-factory.service';
import { ValidateService } from '../../../service/validate/validate.service';
import { FormCheckService } from '../../service/common/form-check.service';
import { UserDataService } from '../../../service/user-data/user-data.service';
import {
  VerifyCountResult,
  NCCCRes,
  PCODE2566Res
} from 'src/app/service/validate/validate.interface';

@Component({
  selector: 'app-cc-prove',
  templateUrl: './cc-prove.component.html',
  styleUrls: ['./cc-prove.component.scss']
})
export class CcProveComponent implements OnInit, OnDestroy {
  @Output() back = new EventEmitter<void>();

  @Output() afterValidate = new EventEmitter<boolean>();

  form: FormGroup;

  dataForm: FormGroup;

  isCreditShow: boolean;
  pcode2566BankList: DropdownItem[];

  verifyCount: VerifyCountResult = {
    errorCount: 0,
    ncccErrorCount: 0,
    pcode2566ErrorCount: 0
  };

  private alive = true;
  constructor(
    private fb: FormBuilder,
    private commonDataService: CommonDataService,
    private modalFactoryService: ModalFactoryService,
    private validateService: ValidateService,
    public formCheckService: FormCheckService,
    private userDataService: UserDataService
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      validateType: ['', [Validators.required]]
    });

    this.form
      .get('validateType')
      .valueChanges.pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.initDataForm(data);
      });

    this.form.get('validateType').setValue('1');
    this.isCreditShow = true;

    this.commonDataService
      .getPcode2566BankList()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.pcode2566BankList = data;
      });

    this.validateService
      .getVerifyCount()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.verifyCount = data.result;
          if (this.verifyCount.errorCount >= Number(this.userDataService.maxVerifyCount)) {
                    this.afterValidate.emit(false);
          }
        } else {
          this.modalFactoryService.modalDefault({ message: data.message });
        }
      });
  }

  initDataForm(data: any) {
    if (data === '2') {
      this.dataForm = this.fb.group({
        account: [
          '',
          [
            Validators.required,
            Validators.minLength(9),
            Validators.pattern(/^[0-9]{10,}$/)
          ]
        ],
        phone: [
          this.userDataService.initUserTypeData.phone,
          [
            Validators.required,
            Validators.minLength(10),
            Validators.pattern(/^[0]{1}[9]{1}[0-9]{8}$/)
          ]
        ],
        bankId: ['', [Validators.required]]
      });
    } else if (data === '1') {
      this.dataForm = this.fb.group({
        cardId1: [
          '',
          [
            Validators.required,
            Validators.minLength(4),
            Validators.pattern(/^[0-9]{4}$/)
          ]
        ],
        cardId2: [
          '',
          [
            Validators.required,
            Validators.minLength(4),
            Validators.pattern(/^[0-9]{4}$/)
          ]
        ],
        cardId3: [
          '',
          [
            Validators.required,
            Validators.minLength(4),
            Validators.pattern(/^[0-9]{4}$/)
          ]
        ],
        cardId4: [
          '',
          [
            Validators.required,
            Validators.minLength(4),
            Validators.pattern(/^[0-9]{4}$/)
          ]
        ],
        year: ['', Validators.required],
        month: [
          '',
          [Validators.required, Validators.pattern(/^(0?[1-9]|1[012])$/)]
        ],
        validateNo: [
          '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.pattern(/^[0-9]{3}$/)
          ]
        ]
      });
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }

  AutoChange(target: string, nextTarget: string, prevTarget: string): void {
    if (this.dataForm.get(target).value.length === 4) {
      document.getElementById(nextTarget).focus();
    } else if (this.dataForm.get(target).value.length === 0) {
      document.getElementById(prevTarget).focus();
    }
  }

  isActive(type: string): string {
    const val = this.form.get('validateType').value;
    return val === type ? 'active' : '';
  }

  backStep(): void {
    this.back.emit();
  }

  nextStep(): void {
    const validateType = this.form.get('validateType').value;
    // 如果使用者沒有填寫正確的資料 就跳出詢問是否要跳過的訊息
    if (this.dataForm.invalid) {
      const message =
        validateType === '1'
          ? '您確定要跳過信用卡驗證'
          : '您確定要跳過帳戶驗證';
      this.modalFactoryService
        .modalDefault({
          showConfirm: true,
          message: message,
          btnCancel: '繼續驗證',
          btnOK: '跳過驗證'
        })
        .result.then(v => {
          if (v === 'ok') {
            this.afterValidate.emit(false);
          }
        });
      return;
    }
    const formData = this.dataForm.value;
    if (validateType === '1') {
      // 信用卡驗證
      this.validateService
        .setNCCC({
          pan:
            formData.cardId1 +
            formData.cardId2 +
            formData.cardId3 +
            formData.cardId4,
          expDate: formData.year + formData.month,
          extNo: formData.validateNo
        })
        .pipe(takeWhile(() => this.alive))
        .subscribe(data => {
          // 呼叫API成功
          if (data.status === 0) {
            const r2: NCCCRes = data.result;
            // 呼叫NCCC是否成功
            if (r2.ncccCheckCode === '0') {
              this.afterValidate.emit(true);
            } else {
              // 失敗 紀錄次數
              this.verifyCount.errorCount = r2.errorCount;
              this.verifyCount.ncccErrorCount = r2.ncccErrorCount;
              this.verifyCount.pcode2566ErrorCount = r2.pcode2566ErrorCount;
              this.modalFactoryService
                .modalDefault({
                  message: r2.ncccMessage
                })
                .result.then(() => {
                  if (this.verifyCount.errorCount >= Number(this.userDataService.maxVerifyCount)) {
                    // 次數已滿 看要去哪
                    this.modalFactoryService
                      .modalDefault({
                        message: '次數已滿，請之後再驗證'
                      })
                      .result.then(v => {
                        this.afterValidate.emit(false);
                      });
                  } else {
                    if (this.verifyCount.ncccErrorCount >= 3) {
                      this.form.get('validateType').setValue('2');
                    }
                  }
                });
            }
          } else {
            this.modalFactoryService.modalDefault({ message: data.message });
          }
        });
    } else if (validateType === '2') {
      this.validateService
        .setPCode2566({
          phone: formData.phone,
          bank: formData.bankId,
          account: formData.account,
          branchId: '',
        })
        .pipe(takeWhile(() => this.alive))
        .subscribe(data => {
          if (data.status === 0) {
            const r2: PCODE2566Res = data.result; // XXX: Charles: Legecy Code is broken. Please Ignore the failure.
            if (r2.pcode2566CheckCode === '0') {
              this.afterValidate.emit(true);
            } else {
              this.verifyCount.errorCount = r2.errorCount; // XXX: Charles: Legecy Code is broken. Please Ignore the failure.
              this.verifyCount.ncccErrorCount = r2.ncccErrorCount; // XXX: Charles: Legecy Code is broken. Please Ignore the failure.
              this.verifyCount.pcode2566ErrorCount = r2.pcode2566ErrorCount; // XXX: Charles: Legecy Code is broken. Please Ignore the failure.
              this.modalFactoryService
                .modalDefault({
                  message: r2.pcode2566Message
                })
                .result.then(() => {
                  if (this.verifyCount.errorCount >= Number(this.userDataService.maxVerifyCount)) {
                    // 次數已滿 看要去哪
                    this.modalFactoryService
                      .modalDefault({
                        message: '次數已滿，請之後再驗證'
                      })
                      .result.then(v => {
                        this.afterValidate.emit(false);
                      });
                  } else {
                    if (this.verifyCount.pcode2566ErrorCount >= 3) {
                      this.form.get('validateType').setValue('1');
                    }
                  }
                });
            }
          } else {
            this.modalFactoryService.modalDefault({ message: data.message });
          }
        });
    }
  }
}
