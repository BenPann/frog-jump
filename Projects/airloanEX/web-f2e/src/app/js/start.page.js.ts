import { Cleave } from 'cleave.js';
// Must declare
declare const Cleave: any;

/***************
 Dialog Popup
 ***************/
// var contactScrollBarWidth=window.innerWidth-contact.clientWidth-side.clientWidth;
let nowOpenedDialog;

/*關閉Dialog：要關閉的組件套上.dialog-close*/
export function closeDialog() {

  (document.querySelector("#BGGray") as HTMLElement).style.display = 'none';

  const bodyWrapper: any = document.body.children[1];
  if (nowOpenedDialog) {
    nowOpenedDialog.classList.remove('dialog-show');
    const otherDialog = document.querySelector('.dialog-show');
    if (otherDialog) {
      nowOpenedDialog = otherDialog;
      return;
    } else {
      document.body.classList.remove('has-dialog');
      bodyWrapper.style.paddingRight = '';
    }
  } // end if
} // end closeDialog

export function showContactUsDialog(){
	closeChatBot();
	jsShowContactUsDialog("#dialogContactMethod");
}

var G_CHATBOT_STATUS = "close";

export function showChatBot(){
	
	if (G_CHATBOT_STATUS=="close"){
		//open it
		document.getElementById("SpiritAI").style.display = "block";
		//document.getElementById("talk-triangle-top").style.display = "block";
		//document.getElementById("talk-triangle-base").style.display = "block";
		G_CHATBOT_STATUS = "open";
	}else{
		
		if (G_CHATBOT_STATUS=="open"){
			//open it
			document.getElementById("SpiritAI").style.display = "none";
			//document.getElementById("talk-triangle-top").style.display = "none";
			//document.getElementById("talk-triangle-base").style.display = "none";
			G_CHATBOT_STATUS = "close";
		}		
		
	}
}


export function closeChatBot(){
	
	document.getElementById("SpiritAI").style.display = "none";
	//document.getElementById("talk-triangle-top").style.display = "none";
	//document.getElementById("talk-triangle-base").style.display = "none";
	G_CHATBOT_STATUS = "close";

}

/*呼叫Dialog：套上onclick="showDialog('#dialog1')"*/
export function jsShowContactUsDialog(target){
  // document.querySelector(target).css('top','200px');
	// document.querySelector(target).css('z-index','100');
  (document.querySelector(target) as any).style = 'z-index:100';
  (document.querySelector("#BGGray") as HTMLElement).style.display = 'block';

  const bodyWrapper: any = document.body.children[1];
  const bodyScrollBarWidth = innerWidth - bodyWrapper.clientWidth; // body scroll bar
  nowOpenedDialog = document.querySelector(target);
  bodyWrapper.style.paddingRight = bodyScrollBarWidth + 'px';
  document.body.classList.add('has-dialog');
  nowOpenedDialog.classList.add('dialog-show');

  window.addEventListener('keydown', (e) => {
    if (e.keyCode === 27) {
      closeDialog();
    }
  });
}

export function jsCloseDialog(target){
	closeDialog();
}
export function closeAllDialog() {
    const bodyWrapper: any = document.body.children[1];
    document.body.classList.remove('has-dialog');
    bodyWrapper.style.paddingRight = '';
    const nowDialogs: any = document.querySelectorAll('.dialog-show');
    for (const nowDialog of nowDialogs) {
        nowDialog.classList.remove('dialog-show');
    }
} // end closeAllDialog

// 關閉全部
export function initAllCloseButtons() {
    const allCloseButtons: any = document.querySelectorAll('.dialog-close-all');
    for (const allCloseButton of allCloseButtons) {
        allCloseButton.addEventListener('click', closeAllDialog);
    }
} // end initAllCloseButtons

/*僅輸入數字 .cleave-number*/
export function initCleaveNumber() {
    const cleaveNumbers: any = document.querySelectorAll('.cleave-number');
    for (const cleaveNumber of cleaveNumbers) {
        const c = new Cleave(cleaveNumber, {
            numericOnly: true,
            blocks: [9999],
        });
    }
} // end initCleaveNumber


export function initCloseButtons() {
    const closeButtons: any = document.querySelectorAll('.dialog-close');
    for (const closeButton of closeButtons) {
        closeButton.addEventListener('click', closeDialog);
    }
} // end initCloseButtons

/*呼叫Dialog：套上onclick="showDialog('#dialog1')"*/
export function showDialog(target) {
    const bodyWrapper: any = document.body.children[1];
    const bodyScrollBarWidth = innerWidth - bodyWrapper.clientWidth; // body scroll bar
    nowOpenedDialog = document.querySelector(target);
    bodyWrapper.style.paddingRight = bodyScrollBarWidth + 'px';
    document.body.classList.add('has-dialog');
    nowOpenedDialog.classList.add('dialog-show');

    window.addEventListener('keydown', (e) => {
        if (e.keyCode === 27) {
            closeDialog();
        }
    });
} // end showDialog
