import { Event } from '@angular/router';
import { ElementRef } from '@angular/core';

/***************
簡訊驗證碼 輸入完畢後自動focus到下一個
// ***************/
export function initRapidForm(elem: ElementRef) {
    const rapidInputs = elem.nativeElement.querySelectorAll('.rapid-form input');
    rapidInputs.forEach((item, idx) => {
        item.addEventListener('input', () => {
            const nextInput = rapidInputs[idx + 1];
            // console.log((item.className as string).indexOf('ng-invalid'))
            if (item.value.length === item.maxLength && (item.className as string).indexOf('ng-invalid') === -1) {
                if (nextInput) {
                    nextInput.focus();
                }
            }
        });
    });
}