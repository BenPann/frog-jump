


export function CalcIRRQuick(inLoadAmt, inLoanYear, inPeriod_1_Month, inPeriod_1_YRate, inPeriod_2_YRate, inOtherFee) {
    //ps: inLoadAmt 貸款額度(單位:萬)
    //ps: inLoanYear 貸款年期
    //ps: inPeriod_1_Month 第一期期數(單位:月)
    //ps: inPeriod_1_YRate 第一期利率(單位:%)
    //ps: inPeriod_2_YRate 第二期利率(單位:%)
    //ps: inOtherFee 其他費用(單位:元)
    //ex:
    //一段式: CalcIRRQuick(300,5,60,3.25,0,5000);
    //二段式: CalcIRRQuick(300,5,12,1.11,3.59,5000);


    console.log("@@@@ 即時運算中......");

    var isPass = true;
    var loadAmt = parseInt(inLoadAmt, 10) * 10000;
    var loanYear = parseInt(inLoanYear, 10);
    var loanMonthTotal = 0;
    var period_1_Month = parseInt(inPeriod_1_Month, 10);
    var period_2_Month = 0;	//用計算的
    var period_1_YRate = inPeriod_1_YRate;
    var period_2_YRate = inPeriod_2_YRate;
    var otherFee = parseInt(inOtherFee, 10);

    loanMonthTotal = loanYear * 12;
    period_2_Month = loanMonthTotal - period_1_Month;

    //[2019.11.07 GaryLiu] ==== START : 白箱 : 修改 : Client Potential Code Injection ====
    otherFee = +XSSFilter(otherFee);
    //[2019.11.07 GaryLiu] ====  END  : 白箱 : 修改 : Client Potential Code Injection ====


    //一段式;
    if (period_2_YRate == 0 || period_2_YRate == null) {

        period_1_Month = loanMonthTotal;
        period_2_Month = 0;
        period_1_YRate = inPeriod_1_YRate;
        period_2_YRate = 0;
        console.log("模式: 一段式利率");
    } else {
        console.log("模式: 兩段式利率");
    }





    var i, I0, IRR;
    var sum = loadAmt;//貸款金額
    var Tn = loanYear * 12;//貸款年限
    //[2019.11.07 GaryLiu] ==== START : 白箱 : 修改 : Client Potential Code Injection ====
    //var a0 = otherFee; 	//各項相關費用總金額 
    var a0 = +reverseXSSFilter(otherFee); 	//各項相關費用總金額

    a0 = +XSSFilter(a0);

    //console.log("#### 各項相關費用總金額="+ a0);
    console.log("各項相關費用總金額=" + reverseXSSFilter(a0));
    //[2019.11.07 GaryLiu] ====  END  : 白箱 : 修改 : Client Potential Code Injection ====

    //document.frm1.n3.value = Tn-eval(document.frm1.n1.value)-eval(document.frm1.n2.value); //第三期月份
    //document.frm1.n3.value = 0; //第三期月份
    var r = new Array();
    var rv = new Array();
    var n = new Array();
    var Yn = new Array();
    var D1, D2;
    IRR = 0;



    //各期年利率
    rv[0] = period_1_YRate;
    rv[1] = period_2_YRate;
    rv[2] = 0;
    console.log("#### 各期利率=" + rv);

    r[0] = period_1_YRate * 0.01;
    r[1] = period_2_YRate * 0.01;
    r[2] = 0 * 0.01;

    //
    n[0] = 0;
    n[1] = period_1_Month;

    if (period_2_YRate == 0) {
        n[2] = 0;
    } else {
        n[2] = period_1_Month + period_2_Month;
    }

    n[3] = 0;

    console.log("#### 各期月數=" + n);

    Yn = y(sum, r, n);



    I0 = 0.0;
    if (!(a0 == 0 && eval(r[0]) == 0 && eval(r[1]) == 0 && eval(r[2]) == 0)) {
        IRR = (0.2 / 12 - I0) / 100;
        while (Math.abs(IRR) > Math.pow(0.1, 20)) {
            D1 = Math.abs(Sn(a0, I0, Yn, n) - sum);
            D2 = Math.abs(Sn(a0, I0 + IRR, Yn, n) - sum);
            if (Math.min(D1, D2) == D1) {
                D1 = Math.abs(Sn(a0, I0 - IRR, Yn, n) - sum);
                if (Math.min(D1, D2) == D1) {
                    I0 = I0 - IRR;
                }
                IRR = IRR / 10;
            }
            I0 = I0 + IRR;
        }
    }




    IRR = (Math.round(I0 * 12 * 10000) / 100).toFixed(2) + "";	//總費用年百分率

    var period_1_MonthPay = ROUNDUP(Yn[0], 0) + "";
    var period_2_MonthPay = ROUNDUP(Yn[1], 0) + "";
    var period_3_MonthPay = ROUNDUP(Yn[2], 0) + "";

    //如要轉MONEY格式(含逗號)請呼叫moneyFormat(value);

    console.log("#### IRR 總費用年百分率 = " + IRR);
    console.log("#### 第一期每月繳款金額 = " + period_1_MonthPay);
    console.log("#### 第二期每月繳款金額 = " + period_2_MonthPay);
    console.log("#### 第三期每月繳款金額 = " + period_3_MonthPay);

    var rtnJson = {
        "IRR": IRR,
        "Period1MonthlyPay": period_1_MonthPay,
        "Period2MonthlyPay": period_2_MonthPay,
        "Period3MonthlyPay": period_3_MonthPay
    };

    return rtnJson;


}


//無條件進位
function ROUNDUP(number, digits) {
    var factor = Math.pow(10, digits);
    return Math.ceil(number * factor) / factor
}



function Sn(a0, IRR, Yn, n) {
    var Gn = new Array();
    var i, j;
    Gn[0] = 0.0;
    Gn[1] = 0.0;
    Gn[2] = 0.0;

    for (j = 0; j <= 2; j++) {
        for (i = eval(n[j]) + 1; i <= n[j + 1]; i++) {
            Gn[j] += Yn[j] * f(IRR, i);
        }
    }
    //[2019.10.04 GaryLiu] ==== START : 修正Client Potential Code Injection ====
    //[2019.11.07 GaryLiu] ==== START : 白箱 : 修改 : Client Potential Code Injection ====
    //return escape(eval(a0)+Gn[0]+Gn[1]+Gn[2]);
    return +escape(eval(reverseXSSFilter(a0)) + Gn[0] + Gn[1] + Gn[2]);
    //[2019.11.07 GaryLiu] ====  END  : 白箱 : 修改 : Client Potential Code Injection ====
    //[2019.10.04 GaryLiu] ====  END  : 修正Client Potential Code Injection ====
}

function y(T, r, n) {
    var i, j;
    var d = new Array();
    var d1 = 0, d2 = T;
    var Tmax = 0;
    for (i = 0; i <= n.length - 1; i++) {
        Tmax = Math.max(n[i], Tmax);
    }
    for (i = 0; i <= 2; i++) {
        if (r[i] == 0) {
            if (Tmax - n[i] > 0) {
                d[i] = d2 / (Tmax - n[i]);
            } else {
                d[i] = 0;
            }
        } else {
            d[i] = (d2 * Math.pow(1.0 + r[i] / 12, Tmax - n[i])) / ((Math.pow(1.0 + r[i] / 12, Tmax - n[i]) - 1.0) / (r[i] / 12));
        }
        for (j = 0; j < eval(n[i + 1]) - eval(n[i]) && d2 > 0.0; j++) {
            d1 += d[i] - d2 * (r[i] / 12);
            d2 = T - d1;
        }
        d1 = 0;
        T = d2;
    }
    return d;
}

function f(x, i) {
    return 1 / Math.pow(1 + x, i);
}



//[2019.11.07 GaryLiu] ==== START : 白箱 : 修改 : Client Potential Code Injection ====
function XSSFilter(str) {
    var returnValue = "";
    returnValue = str.toString().replace(/\&/g, '&amp;').replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace(/\"/g, '&quot;').replace(/\'/g, '&#x27').replace(/\//g, '&#x2F');
    return returnValue;
}

function reverseXSSFilter(str) {
    var returnValue = "";
    returnValue = str.toString().replace('&amp;', /\&/g).replace('&lt;', /\</g).replace('&gt;', /\>/g).replace('&quot;', /\"/g).replace('&#x27', /\'/g).replace('&#x2F', /\//g);
    return returnValue;
}
