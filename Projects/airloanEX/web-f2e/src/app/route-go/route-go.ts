export const RouteGo = {
    // 1. /initPlContract
    initPlContract: {
        path: 'initPlContract',
        navi: ['initPlContract'],
        url: '/initPlContract',
    },
    // 2. /n/chooseIdentificationStylePlContract
    chooseIdentificationStylePlContract: {
        path: 'chooseIdentificationStylePlContract',
        navi: ['n', 'chooseIdentificationStylePlContract'],
        url: '/n/chooseIdentificationStylePlContract',
    },
    // 3. /n/identifyByBankAccountPlContract
    identifyByBankAccountPlContract: {
        path: 'identifyByBankAccountPlContract',
        navi: ['n', 'identifyByBankAccountPlContract'],
        url: '/n/identifyByBankAccountPlContract',
    },
    // 4. /o/otpPlContract
    otpPlContract: {
        path: 'otpPlContract',
        navi: ['o', 'otpPlContract'],
        url: '/o/otpPlContract',
    },
    // 5. /n/choosePlContract
    choosePlContract: {
        path: 'choosePlContract',
        navi: ['n', 'choosePlContract'],
        url: '/n/choosePlContract',
    },
    // 15. /n/contentPlContract
    content: {
        path: 'content',
        navi: ['n', 'content'],
        url: '/n/content',
    },
    // 16. /n/contentrplPlContract
    contentrpl: {
        path: 'contentrpl',
        navi: ['n', 'contentrpl'],
        url: '/n/contentrpl',
    },
    // 6. /n/agreePlContract
    agreePlContract: {
        path: 'agreePlContract',
        navi: ['n', 'agreePlContract'],
        url: '/n/agreePlContract',
    },
    // 7. /n/agreeRplContract
    agreeRplContract: {
        name: 'agreeRplContract',
        navi: ['n', 'agreeRplContract'],
        url: '/n/agreeRplContract',
    },
    // 8. /n/agreeCashcardContract
    agreeCashcardContract: {
        path: 'agreeCashcardContract',
        navi: ['n', 'agreeCashcardContract'],
        url: '/n/agreeCashcardContract',
    },
    // 9. /n/infoPlContract
    infoPlContract: {
        path: 'infoPlContract',
        navi: ['n', 'infoPlContract'],
        url: '/n/infoPlContract',
    },
    // 10. /n/previewPlContract
    previewPlContract: {
        path: 'previewPlContract',
        navi: ['n', 'previewPlContract'],
        url: '/n/previewPlContract',
    },
    // 11. /n/allFinishPlContract
    allFinishPlContract: {
        path: 'allFinishPlContract',
        navi: ['n', 'allFinishPlContract'],
        url: '/n/allFinishPlContract',
    },
    // 13. /n/allFailurePlContract
    allFailurePlContract: {
        path: 'allFailurePlContract',
        navi: ['n', 'allFailurePlContract'],
        url: '/n/allFailurePlContract',
    },
    // 14. /n/updateDgtContract
    updateDgtContract: {
        path: 'updateDgtContract',
        navi: ['n', 'updateDgtContract'],
        url: '/n/updateDgtContract',
    },
    find(url: string) {
        if (!Object.entries) {
            Object.entries = function( obj ) {
                let ownProps = Object.keys( obj ),
                    i = ownProps.length,
                    resArray = new Array(i); // preallocate the Array
                while (i--) {
                resArray[i] = [ownProps[i], obj[ownProps[i]]];
                }

                return resArray;
            };
        }
        for (const [key, value] of Object.entries(this as Record<string, any>)) {
            if (url === value.path) {
                return value;
            }
        }
    }
};
