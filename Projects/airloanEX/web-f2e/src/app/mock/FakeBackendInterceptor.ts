import {Injectable} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {mergeMap} from 'rxjs/operators';
import {environment} from '../../environments/environment';

import cardList from './creditcardList';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  // 回應正常訊息
  regularResponseList = [
    {url: 'api/apply/finish', method: 'GET'},   // 完成頁面
    {url: 'api/credit/revertCreditCaseStatus', method: 'GET'},
    {url: 'api/apply/caseDataFinish', method: 'GET'},
    {url: 'api/apply/webBank', method: 'POST'}, // 儲存網銀頁面內容
    {url: 'api/terms/setCaseDataTerms', method: 'POST'},
    {url: 'api/terms/setContractMainTerms', method: 'POST'}, // airloanEX
    {url: 'api/apply/product', method: 'POST'},
    {url: 'api/apply/accountType', method: 'POST'},
    {url: 'api/apply/setAccount/AccountView', method: 'POST'},
    {url: 'api/apply/setWebBankFlag/WebBankFlagView', method: 'POST'},
    {url: 'api/apply/setLoanType/LoanTypeView', method: 'POST'},
    {url: 'api/apply/setLoan/LoanView', method: 'POST'},
    {url: 'api/apply/setPage', method: 'POST'},
    {url: 'api/browse/setPage', method: 'POST'}, // airloanEX
    {url: 'api/apply/setBreakPointPage', method: 'POST'},
    {url: 'api/apply/setContinue', method: 'POST'},     // 是否接續斷點
    {url: 'api/apply/breakPointPage', method: 'POST'},  // 儲存斷點頁面
    {url: 'api/credit/setCreditCard/ChooseCardView', method: 'POST'},
    {url: 'api/apply/setIDCardFrontData', method: 'POST'}, // 儲存資料填寫第一頁內容
    {url: 'api/apply/setIDCardBackData', method: 'POST'}, // 儲存資料填寫第二頁內容
    {url: 'api/apply/setBasicData', method: 'POST'},  // 儲存資料填寫第三頁內容
    {url: 'api/apply/setCorpData', method: 'POST'},   // 儲存資料填寫第四頁內容
    {url: 'api/apply/creditcard', method: 'POST'},
    {url: 'api/apply/setGiftData', method: 'POST'},   // 儲存資料填寫第五頁內容
    {url: 'api/apply/confirmData', method: 'POST'},   // 儲存資料確認頁的內容
    {url: 'api/extra/createDealAccount', method: 'POST'},
    {url: 'api/extra/updateDealAccount', method: 'POST'},
    {url: 'api/extra/deleteDealAccount', method: 'POST'},
    {url: 'api/extra/saveChtTelData', method: 'POST'},
    {url: 'api/extra/updateChtTelData', method: 'POST'},
    {url: 'api/extra/deleteChtTelData', method: 'POST'},
    {url: 'api/extra/saveElectricityFeeData', method: 'POST'},
    {url: 'api/extra/updateElectricityFeeData', method: 'POST'},
    {url: 'api/extra/deleteElectricityFeeData', method: 'POST'},
    {url: 'api/extra/saveGasFeeData/GasDataView', method: 'POST'},
    {url: 'api/extra/updateGasFeeData/GasDataView', method: 'POST'},
    {url: 'api/extra/deleteGasFeeData/GasDataView', method: 'POST'},
    {url: 'api/extra/saveTaipeiWaterData', method: 'POST'},
    {url: 'api/extra/updateTaipeiWaterData', method: 'POST'},
    {url: 'api/extra/deleteTaipeiWaterData', method: 'POST'},
    {url: 'api/extra/saveTaiwanWaterData', method: 'POST'},
    {url: 'api/extra/updateTaiwanWaterData', method: 'POST'},
    {url: 'api/extra/deleteTaiwanWaterData', method: 'POST'},
    {url: 'api/apply/chooseProduct', method: 'POST'},
    {url: 'api/photo/deletePhoto', method: 'POST'},
    {url: 'api/apply/chooseAccount', method: 'POST'}
  ];

  constructor() {
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return of(null).pipe(
      mergeMap(() => {
        // 產品模式不顯示此段LOG
        if (!environment.production) {
          //  console.log('FakeBackendInterceptor request:', request);
        }

        if (environment.useFakeOtp) {

          if (
            request.url.endsWith('api/validate/nccc') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {
                ncccCheckCode: '0',
                ncccMessage: '',
                errorCount: 0,
                ncccErrorCount: 0,
                pcode2566ErrorCount: 0
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            (
              request.url.endsWith('api/validate/pcode2566') ||
              request.url.endsWith('api/validate/pcode2566/auth') ||
              request.url.endsWith('api/validate/pcode2566/sign')
            ) &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {
                pcode2566CheckCode: '0',
                pcode2566Message: '',
                // ed3
                errorCount: 0,
                ncccErrorCount: 0,
                pcode2566ErrorCount: 0,
                // airloanEX
                authErrCount: 0,
                signErrCount: 0,
                eddaErrCount: 0,
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/validate/sendOTP') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '',
              result: {
                code: '0000',
                message: '',
                sk: 'sk',
                txnID: 'txn',
                txnDate: 'date'
              }
            };

            return of(new HttpResponse({status: 200, body: body}));
          }

          // 驗證OTP
          if (
            request.url.endsWith('api/validate/checkOTP') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '',
              result: {
                code: '0000',
                message: ''
              }
            };

            return of(new HttpResponse({status: 200, body: body}));
          }
          // 驗證Phone
          if (
            request.url.endsWith('api/apply/isPhoneSuccess') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '',
              result: 'Y'
            };

            return of(new HttpResponse({status: 200, body: body}));
          }
        } // end if (environment.useFakeOtp) {

        // 是否使用假的的API
        if (environment.useFakeKgiApi) {
          if (
            this.regularResponseList.find(
              value =>
                value.method === request.method &&
                request.url.endsWith(value.url)
            )
          ) {
            const body = {
              status: 0,
              message: '',
              result: ''
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/stp/initPlContract') &&
            request.method === 'POST'
          ) {
            const body = {
              'status': 0,
              'message': '成功',
              'result': {
                'token': 'sadswqe1we3das',
                'cifinfo': {
                  'sourceType': '1',
                  'chtName': '蔡基',
                  'engName': 'TSAI',
                  'resAddrZipCode': '242',
                  'resAddr': '新北市新莊區XX路XX巷',
                  'commAddrZipCode': '242',
                  'commAddr': '新北市新莊區XX路XX巷',
                  'marriage': '1',
                  'email': 'XXXX@mail',
                  'resTelArea': '02',
                  'resTel': '22222222',
                  'homeTelArea': '02',
                  'homeTel': '222222222',
                  'estateType': 'Hello, world!',
                  'corpName': '景丞公司',
                  'corpTelArea': '02',
                  'corpTel': '22565454',
                  'corpAddrZipCode': '104',
                  'corpAddr': '台北市大安區xxx路',
                  'yearlyIncome': 100,
                  'onBoardDate': '19980222'
                }

              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/credit/getApplyedCreditCardList') &&
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '',
              result: []
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/apply/getToken') && // 取得舊案例的JWTToken
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {
                token: 'sadswqe1we3das' // 選擇接續斷點時，jwttoken須更新
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/credit/revertCreditCaseStatus') &&
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '',
              result: []
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          // TODO: 上線把這段註解掉
          if (
            request.url.endsWith('api/credit/getCreditCardList') &&
            request.method === 'GET'
          ) {
            return of(new HttpResponse({status: 200, body: cardList}));
          }

          if (
            request.url.endsWith('api/apply/creditcard') &&
            request.method === 'GET'
          ) {
            return of(new HttpResponse({status: 200, body: cardList}));
          }

          if (
            request.url.endsWith('publicApi/terms/EOPFatca') &&
            request.method === 'GET'
          ) {
            const body = '美國海外帳戶稅收遵循法(FATCA)相關事宜';
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('publicApi/terms/EOPDepositPromise') &&
            request.method === 'GET'
          ) {
            const body = '存款總約定書';
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('publicApi/terms/CreditPromise') &&
            request.method === 'GET'
          ) {
            const body = '聯名卡同意條款';
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('publicApi/terms/EOPPersonalDataUsing') &&
            request.method === 'GET'
          ) {
            const body = '個人資料運用告知聲明使用者條款及約定';
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('publicApi/terms/EOPRateFee') &&
            request.method === 'GET'
          ) {
            const body = '利率費用告知事項';
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('publicApi/terms/EOPNoAgency') &&
            request.method === 'GET'
          ) {
            const body = '聯名卡同意條款';
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/apply/getRepeatLoginInfo') &&
            request.method === 'GET'
          ) {
            const body =  {
              status: 0,
              message: '',
              result:
              {
                // phone: [],
                // emailAddress: []
                phone: ['09333333'],
                emailAddress: ['a@a']
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/apply/breakPointPage') && // 取得斷點相關資訊
            request.method === 'GET'
          ) {
            const body =  {
              status: 0,
              message: '',
              result:
              {
                // accountType: '1',
                // creditType: '2'
                phone: '0912345678',
                chtName: '蔡基',
                isVerify: 'N',
                accountType: '2',
                creditType: '1',
                previousStep: '3',
                breakPointPage: ''

              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          // 取得是否有開啟網銀
          if (
            request.url.endsWith('api/apply/webBank') &&  // 取得網銀資料
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '',
              result: {
                hasWebBank: 'N',
                hasIvrBank: 'N'
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/apply/getWebBankKey') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {
                webBankKey:
                // tslint:disable-next-line:max-line-length
                  '-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwIEZI5/Sch0JJFaOthBLYb6Yh+OQMezHxP13UG2K/y64/JoHrBYN08fnCTVgx+kMoukR51go3LJRAiRnalwK/2K+psA9E9zVT8nOWpRLjoTs4hNLOJYG7f/QJp6jtBYfwKReZmUMeXo4ISFOEKDOH+4EvoneR91C62NLTSYdwftRnm7MfvveVU4z0vLgGpDTDinTULlIX68XuhMhBHSuhdweAhbqNqeQEg5hNCwxoIavqqyH1N2Tj6E0Gg99qAwrisDbLB3e8DJh9JjDUXsRpv3/LKEQTKSHXpiMMIrv9Xm4LKb3I9+9uVj3eJKOp0ckyeVsjK9BwNQsejW8UFz9IQIDAQAB-----END PUBLIC KEY-----'
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          // 取得銀行帳戶列表
          if (
            request.url.endsWith('api/apply/getAccountList') &&
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '',
              result: [
                {account: '0000026666168168', branchName: '忠孝分行'}
                // { account: '0000026888520168', branchName: '新店分行' }
              ]
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/apply/getSecuritiesInfo') &&
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '',
              result: [
                {
                  securitiesCity: '臺北市',
                  securitiesDist: '中正區',
                  securitiesAddrZipCode: '10044',
                  securitiesCode: '9227',
                  securitiesName: '城中'
                },
                {
                  securitiesCity: '臺北市',
                  securitiesDist: '中正區',
                  securitiesAddrZipCode: '10045',
                  securitiesCode: '9228',
                  securitiesName: '城中2'
                },
                {
                  securitiesCity: '臺北市',
                  securitiesDist: '中山區',
                  securitiesAddrZipCode: '10047',
                  securitiesCode: '920F',
                  securitiesName: '站前'
                },
                {
                  securitiesCity: '新北市',
                  securitiesDist: '新店區',
                  securitiesAddrZipCode: '23147',
                  securitiesCode: '9112',
                  securitiesName: '新店'
                }
              ]
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/apply/getBranchBankInfo') &&
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '',
              result: [
                {
                  bankCity: '臺北市',
                  bankDist: '中正區',
                  bankAddrZipCode: '10045',
                  bankAddr: '中正路XXX號',
                  bankName: '城中2',
                },
                {
                  bankCity: '臺北市',
                  bankDist: '大安區',
                  bankAddrZipCode: '10046',
                  bankAddr: '新生南路二段8號',
                  bankName: '大安分行'
                },
                {
                  bankCity: '臺北市',
                  bankDist: '中山區',
                  bankAddrZipCode: '10047',
                  bankAddr: '站前廣場',
                  bankName: '站前'
                },
                {
                  bankCity: '新北市',
                  bankDist: '新店區',
                  bankAddrZipCode: '23147',
                  bankAddr: '烏來路XX巷OO號',
                  bankName: '新店'
                }
              ]
            };
            return of(new HttpResponse({status: 200, body: body}));
          }


        }

        // 是否使用假的的API
        if (environment.useFakeBackendApi) {
          // 取得下拉選單
          if (
            request.url.endsWith('api/data/getDropDown') &&
            request.method === 'POST'
          ) {
            let body: any[];
            // console.log(request.body);
            switch (request.body.name) {
              case 'estate':
                body = [
                  {dataKey: '1', dataName: '本人所有'},
                  {dataKey: '2', dataName: '配偶所有'},
                  {dataKey: '3', dataName: '家族所有'},
                  {dataKey: '4', dataName: '無'}
                ];
                break;
              case 'eopeducation':
                body = [
                  {dataKey: '1', dataName: '博士'},
                  {dataKey: '2', dataName: '碩士'},
                  {dataKey: '3', dataName: '大學'},
                  {dataKey: '4', dataName: '大專'},
                  {dataKey: '5', dataName: '高中(職)'},
                  {dataKey: '6', dataName: '國中'},
                  {dataKey: '7', dataName: '其他'}
                ];
                break;
              case 'eopbillType':
                body = [
                  {dataKey: '2', dataName: '電子帳單'},
                  {dataKey: '3', dataName: '郵寄帳單'}
                ];
                break;
              case 'eoppurpose':
                body = [
                  {dataKey: '1', dataName: '爽開就開'},
                  {dataKey: '2', dataName: '老媽叫我開'},
                  {dataKey: '3', dataName: '我也不知道為什麼要開'}
                ];
                break;
              case 'eopoccupation':
                body = [
                  {dataKey: '030100', dataName: '軍公教'},
                  {dataKey: '016499', dataName: '金融保險業'},
                  {dataKey: '014290', dataName: '營造/土木工程'},
                  {dataKey: '018610', dataName: '醫療院所'},
                  {dataKey: '013399', dataName: '製造業'},
                  {dataKey: '014990', dataName: '運輸業'},
                  {dataKey: '014832', dataName: '通訊/科技業'},
                  {dataKey: '1_019690', dataName: '服務業'},
                  {dataKey: '2_019690', dataName: '家管'},
                  {dataKey: '016812', dataName: '不動產經紀業'},
                  {dataKey: '1_014745', dataName: '藝術品或骨董等專拍賣'},
                  {dataKey: '016911', dataName: '法律服務業'},
                  {dataKey: '016920', dataName: '會計服務業'},
                  {dataKey: '999690', dataName: '軍火業'},
                  {dataKey: '019200', dataName: '博弈業'},
                  {dataKey: '016419', dataName: '虛擬貨幣'},
                  {dataKey: '016496', dataName: '典當業'},
                  {dataKey: '2_014745', dataName: '珠寶、銀樓業'},
                  {
                    dataKey: '019323',
                    dataName:
                      '特殊行業(視廳歌唱業、理髮業、三溫暖業、舞廳業、酒吧業、特種咖啡茶室及電子遊藝業)'
                  }
                ];
                break;
              case 'eopjobtitle':
                body = [
                  {dataKey: '1', dataName: '董監事/股東'},
                  {dataKey: '2', dataName: '負責人'},
                  {dataKey: '3', dataName: '高階管理職務人員'},
                  {dataKey: '4', dataName: '一般主管'},
                  {dataKey: '5', dataName: '工程師'},
                  {dataKey: '6', dataName: '業務人員'},
                  {dataKey: '7', dataName: '一般職員'},
                  {dataKey: '8', dataName: '約聘人員'},
                  {dataKey: '9', dataName: '老師'},
                  {dataKey: '10', dataName: '其他'}
                ];
                break;
              case 'idcardlocation':
                body = [
                  {dataKey: '北縣', dataName: '北縣'},
                  {dataKey: '宜縣', dataName: '宜縣'},
                  {dataKey: '桃縣', dataName: '桃縣'},
                  {dataKey: '竹縣', dataName: '竹縣'},
                  {dataKey: '苗縣', dataName: '苗縣'},
                  {dataKey: '中縣', dataName: '中縣'},
                  {dataKey: '彰縣', dataName: '彰縣'},
                  {dataKey: '投縣', dataName: '投縣'},
                  {dataKey: '雲縣', dataName: '雲縣'},
                  {dataKey: '嘉縣', dataName: '嘉縣'},
                  {dataKey: '南縣', dataName: '南縣'},
                  {dataKey: '高縣', dataName: '高縣'},
                  {dataKey: '屏縣', dataName: '屏縣'},
                  {dataKey: '東縣', dataName: '東縣'},
                  {dataKey: '花縣', dataName: '花縣'},
                  {dataKey: '澎縣', dataName: '澎縣'},
                  {dataKey: '基市', dataName: '基市'},
                  {dataKey: '竹市', dataName: '竹市'},
                  {dataKey: '嘉市', dataName: '嘉市'},
                  {dataKey: '連江', dataName: '連江'},
                  {dataKey: '金門', dataName: '金門'},
                  {dataKey: '北市', dataName: '北市'},
                  {dataKey: '高市', dataName: '高市'},
                  {dataKey: '新北市', dataName: '新北市'},
                  {dataKey: '中市', dataName: '中市'},
                  {dataKey: '南市', dataName: '南市'},
                  {dataKey: '桃市', dataName: '桃市'}
                ];
                break;
              case 'pCode2566Bank':
                body = [
                  {dataKey: '004', dataName: '台灣銀行'},
                  {dataKey: '009', dataName: '彰化銀行'},
                  {dataKey: '012', dataName: '台北富邦'},
                  {dataKey: '016', dataName: '高雄銀行'},
                  {dataKey: '017', dataName: '兆豐商銀'},
                  {dataKey: '048', dataName: '王道銀行'},
                  {dataKey: '052', dataName: '渣打商銀'},
                  {dataKey: '054', dataName: '京城商銀'},
                  {dataKey: '101', dataName: '瑞興銀行'},
                  {dataKey: '103', dataName: '臺灣新光銀行'},
                  {dataKey: '108', dataName: '陽信銀行'},
                  {dataKey: '118', dataName: '板信銀行'},
                  {dataKey: '162', dataName: '彰化六信'},
                  {dataKey: '216', dataName: '花蓮二信'},
                  {dataKey: '600', dataName: '農金資中心'},
                  {dataKey: '803', dataName: '聯邦銀行'},
                  {dataKey: '805', dataName: '遠東銀行'},
                  {dataKey: '806', dataName: '元大銀行'},
                  {dataKey: '808', dataName: '玉山銀行'},
                  {dataKey: '809', dataName: '凱基銀行'},
                  {dataKey: '812', dataName: '台新銀行'}
                ];
                break;
              default:
                break;
            }
            if (body) {
              return of(
                new HttpResponse({
                  status: 200,
                  body: {
                    status: 0,
                    message: '',
                    result: body
                  }
                })
              );
            }
          }

          if (
            request.url.includes('/publicApi/Terms/') &&
            request.method === 'GET'
          ) {
            return of(new HttpResponse({status: 200, body: '我是條款'}));
          }

          if (
            request.url.endsWith('api/apply/accountType') &&
            request.method === 'GET'
          ) {
            const body = {
              'status': 0,
              'message': '成功',
              'result': {
                'ed3OfflineContent': '非本人帳戶無轉帳限制<br>可使用約定轉帳',
                'ed3OnlineContent': '非本人帳戶無轉帳限制:<br>1萬/單筆、3萬/每日、5萬/每月<br>無約定轉帳'
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/apply/product') &&
            request.method === 'GET'
          ) {
            const body = {
              'status': 0,
              'message': '成功',
              'result': {
                'ed3CCAProdContent': '每月享跨提12次免手續費<br>新戶享首刷禮',
                'ed3AProdContent': '每月享跨提5次免手續費'
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('/publicApi/channel/getChannelList') &&
            request.method === 'GET'
          ) {
            const body = {
              'status': 0,
              'message': '成功',
              'result': [
                {
                  'channelID': 'KB',
                  'channelName': '凱基商銀',
                  'idLength': 'Hello, world!',
                  'idLogic': '0'
                }
              ]
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.includes('/publicApi/terms/getTermsList') &&
            request.method === 'POST'
          ) {
            let body: any[];
            switch (request.body.url) {
              case 'cont-init':
              case 'cont-initPlContract': // add by Charles
                body = [
                  {
                    termTitle: '個人資料運用告知聲明使用者條款及約定',
                    termName: 'EOPPersonalDataUsing',
                    type: '2',
                  },
                  {
                    termTitle: '利率費用告知事項',
                    termName: 'EOPRateFee',
                    type: '2',
                  },
                  {
                    termTitle: '非代辦宣告',
                    termName: 'EOPNoAgency',
                    type: '2',
                  },
                  {
                    termTitle: '電話/網路/行動銀行約定條款',
                    termName: 'EOPBankPromise',
                    type: '1',
                  }
                ];
                break;
              case 'cont-fillData4':
                body = [
                  {
                    termTitle: '美國海外帳戶稅收遵循法(FATCA)相關事宜',
                    termName: 'EOPFatca',
                    type: '2',
                  }
                ];
                break;
              case 'cont-chooseProduct':
                body = [
                  {
                    termTitle: '美國海外帳戶稅收遵循法(FATCA)相關事宜',
                    termName: 'EOPFatca',
                    type: '2'
                  },
                  {
                    termTitle: '存款總約定書',
                    termName: 'EOPDepositPromise',
                    type: '2'
                  },
                  {
                    termTitle: '聯名卡同意條款',
                    termName: 'CreditPromise',
                    type: '2'
                  }
                ];
                break;
                case 'stp-chooseCard':
                body = [
                  {
                    termTitle: '聯名卡同意條款',
                    termName: 'CreditPromise',
                    type: '2'
                  }
                ];
                break;
                case 'cont-product_CCA':
                  body = [
                    {
                      termTitle: '美國海外帳戶稅收遵循法(FATCA)相關事宜',
                      termName: 'EOPFatca',
                      type: '2'
                    },
                    {
                      termTitle: '存款總約定書',
                      termName: 'EOPDepositPromise',
                      type: '2'
                    },
                    {
                      termTitle: '聯名卡同意條款',
                      termName: 'CreditPromise',
                      type: '2'
                    }
                  ];
                  break;
                  case 'cont-product_A':
                  body = [
                    {
                      termTitle: '美國海外帳戶稅收遵循法(FATCA)相關事宜',
                      termName: 'EOPFatca',
                      type: '2'
                    },
                    {
                      termTitle: '存款總約定書',
                      termName: 'EOPDepositPromise',
                      type: '2'
                    },
                  ];
                  break;
            }


            return of(new HttpResponse({status: 200, body: body}));
          }


          if (
            request.url.endsWith('/publicApi/QRCode/check') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {legal: true, batchId: 'EBAT2019080100003'}
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('/apply/getUserType') &&
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '',
              result: {
                userType: '0',
                chtName: '蔡基',
                phone: '0912345678',
                status: 0,
                breakPointPage: ''
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('/api/QRCode/getShortUrlData') &&
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {
                corpName: '觀光局',
                corpTelArea: '02',
                corpTel: '23491500',
                corpAddrZipCode: '106',
                corpAddr: '臺北市大安區忠孝東路四段290號9樓',
                occupation: '030100',
                passBookTw: 'Y',
                shortUrlEndTime: '2019-08-04 23:59:59.0',
                creditCardList: [],
                houseRate: '1.59',
                loanRate: '2.1',
                acString: ['新戶首刷禮介紹1', '新戶首刷禮介紹2'],
                coupon: [
                  '薪轉利率0.29%',
                  '每月享跨提99次免手續費',
                  '每月享跨轉99次免手續費'
                ]
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('/api/apply/getKgiCifInfoCC') &&
            request.method === 'GET'
          ) {
            const body = {status: 0, message: '', result: ''};
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('/api/photo/upload') &&
            request.method === 'POST'
          ) {
            const body = {
              message: '成功',
              result: {
                idCard_Issue_City: '',
                idCard_Issue_DT: '',
                idCard_Issue_Type: '',
                name: '陳筱玲',
                subSerial: '1',
                householdAddr1: '',
                householdAddr2: '',
                householdAddr3: '',
                householdAddr4: '',
                marriage: '',
                householdZipCode: '',
                waterMarkImage: request.body.base64Image
              },
              status: 0
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          // if (
          //   request.url.endsWith('/api/photo/upload') &&
          //   request.method === 'POST'
          // ) {
          //   const body = {
          //     status: 0,
          //     message: '成功',
          //     result: {
          //       waterMarkImage: request.body.base64Image,
          //       idCard_Issue_City: '',
          //       idCard_Issue_DT: '',
          //       idCard_Issue_Type: '',
          //       name: '陳筱玲',
          //       householdAddr1: '',
          //       householdAddr2: '',
          //       householdAddr3: '',
          //       householdAddr4: '',
          //       marriage: '未婚',
          //       subSerial: '2',
          //       householdZipCode: '235'
          //     }
          //   };
          //   return of(new HttpResponse({status: 200, body: body}));
          // }

          // if (
          //   request.url.endsWith('/api/photo/uploadOtherPhoto') &&
          //   request.method === 'POST'
          // ) {
          //   const body = {
          //     status: 0,
          //     message: '成功',
          //     result: {
          //       waterMarkImage: request.body.base64Image,
          //       subSerial: '2'
          //     }
          //   };
          //   return of(new HttpResponse({status: 200, body: body}));
          // }

          if (
            request.url.endsWith('/api/apply/getAuthInfo') &&
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {authorizeToallCorp: 'N', verNo: '1.0'}
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('/api/apply/getGiftList') && // 取得禮物列表
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: []
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('/api/apply/confirmData') && // 取得資料確認頁的內容
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {
                idCardFrontData: {
                  chtName: '陳筱玲',
                  engName: null,
                  idCardDate: '2019-07-31 00:00:00.0',
                  idCardLocation: '東縣',
                  idCardRecord: '2'
                },
                idCardBackData: {
                  resAddrZipCode: '220',
                  resAddr: '新北市板橋區;123123',
                  commAddrZipCode: '220',
                  commAddr: '新北市板橋區;123123',
                  marriage: 'S'
                },
                basicData: {
                  email: 'test@mail.com',
                  sendType: '2',
                  resTelArea: '02',
                  resTel: '12313131',
                  homeTelArea: '02',
                  homeTel: '12313131',
                  education: '1',
                  estateType: '1',
                  passBookTw: 'Y',
                  passBookFore: 'N',
                  authorizeToallCorp: 'N',
                  verNo: ''
                },
                corpData: {
                  corpName: '觀光局',
                  corpTelArea: '02',
                  corpTel: '23491500',
                  corpAddrZipCode: '106',
                  corpAddr: '臺北市大安區;忠孝東路四段290號9樓',
                  occupation: '030100',
                  jobTitle: '1',
                  yearlyIncome: '100',
                  onBoardDate: '2019-07-29'
                },
                giftData: {
                  firstPresent: '',
                  quickHandle: 'N',
                  corpAddrZipCode: '106',
                  corpAddr: '臺北市大安區;忠孝東路四段290號9樓',
                  onBoardDate: '2019-07-29',
                  estateType: '1'
                }
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('/api/photo/getPhotoList') &&
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: []
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('/api/credit/getVerifiyCount') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {
                errorCount: 0,
                ncccErrorCount: 0,
                pcode2566ErrorCount: 0
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/credit/setNCCC/NCCCView') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {
                ncccCheckCode: '0',
                ncccMessage: '',
                errorCount: 0,
                ncccErrorCount: 0,
                pcode2566ErrorCount: 0
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/apply/getEOPProduct') &&
            request.method === 'POST'
          ) {
            const body = {status: 0, message: '成功', result: 'N'};
            return of(new HttpResponse({status: 200, body: body}));
          }

          // 附加服務相關
          if (
            request.url.endsWith('api/extra/getAllBankCodes') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {
                allBank: [
                  {id: '001', name: '中央信託    '},
                  {id: '003', name: '交通銀行    '},
                  {id: '004', name: '台灣銀行    '},
                  {id: '005', name: '土地銀行    '},
                  {id: '006', name: '合庫商銀    '},
                  {id: '007', name: '第一銀行    '},
                  {id: '008', name: '華南銀行    '},
                  {id: '009', name: '彰化銀行    '},
                  {id: '010', name: '華僑銀行    '},
                  {id: '011', name: '上海銀行    '},
                  {id: '012', name: '台北富邦    '},
                  {id: '013', name: '國泰世華    '},
                  {id: '016', name: '高雄銀行    '},
                  {id: '017', name: '兆豐商銀    '},
                  {id: '018', name: '農業金庫    '},
                  {id: '021', name: '花旗銀行    '},
                  {id: '024', name: '運通銀行    '},
                  {id: '025', name: '首都銀行    '},
                  {id: '039', name: '荷蘭銀行    '},
                  {id: '040', name: '中華開發    '},
                  {id: '050', name: '臺灣企銀    '},
                  {id: '052', name: '新竹商銀    '},
                  {id: '053', name: '台中商銀    '},
                  {id: '054', name: '京城商銀    '},
                  {id: '056', name: '花蓮企銀    '},
                  {id: '057', name: '台東企銀    '},
                  {id: '075', name: '東亞銀行    '},
                  {id: '081', name: '匯豐銀行    '},
                  {id: '083', name: '渣打銀行    '},
                  {id: '087', name: '標準銀行    '},
                  {id: '101', name: '瑞興銀行    '},
                  {id: '102', name: '華泰銀行    '},
                  {id: '103', name: '臺灣新光商銀'},
                  {id: '104', name: '台北五信    '},
                  {id: '106', name: '台北九信    '},
                  {id: '108', name: '陽信銀行    '},
                  {id: '114', name: '基隆一信    '},
                  {id: '115', name: '基隆二信    '},
                  {id: '118', name: '板信銀行    '},
                  {id: '119', name: '淡水一信    '},
                  {id: '120', name: '淡水信合社  '},
                  {id: '124', name: '宜蘭信合社  '},
                  {id: '127', name: '桃園信合社  '},
                  {id: '130', name: '新竹一信    '},
                  {id: '132', name: '新竹三信    '},
                  {id: '146', name: '台中二信    '},
                  {id: '147', name: '三信銀行    '},
                  {id: '158', name: '彰化一信    '},
                  {id: '161', name: '彰化五信    '},
                  {id: '162', name: '彰化六信    '},
                  {id: '163', name: '彰化十信    '},
                  {id: '165', name: '鹿港信合社  '},
                  {id: '178', name: '嘉義三信    '},
                  {id: '179', name: '嘉義四信    '},
                  {id: '188', name: '台南三信    '},
                  {id: '203', name: '高雄二信    '},
                  {id: '204', name: '高雄三信    '},
                  {id: '215', name: '花蓮一信    '},
                  {id: '216', name: '花蓮二信    '},
                  {id: '222', name: '澎湖一信    '},
                  {id: '223', name: '澎湖二信    '},
                  {id: '224', name: '金門信合社  '},
                  {id: '512', name: '南農中心    '},
                  {id: '515', name: '南農中心    '},
                  {id: '517', name: '南農中心    '},
                  {id: '518', name: '南農中心    '},
                  {id: '520', name: '南農中心    '},
                  {id: '521', name: '南農中心    '},
                  {id: '523', name: '南農中心    '},
                  {id: '524', name: '南農中心    '},
                  {id: '525', name: '澎湖區漁會  '},
                  {id: '605', name: '高雄市農會  '},
                  {id: '613', name: '南農中心    '},
                  {id: '614', name: '南農中心    '},
                  {id: '616', name: '南農中心    '},
                  {id: '617', name: '南農中心    '},
                  {id: '618', name: '南農中心    '},
                  {id: '619', name: '南農中心    '},
                  {id: '620', name: '南農中心    '},
                  {id: '621', name: '南農中心    '},
                  {id: '622', name: '南農中心    '},
                  {id: '624', name: '南農中心    '},
                  {id: '625', name: '台中市農會  '},
                  {id: '627', name: '南農中心    '},
                  {id: '700', name: '中華郵政    '},
                  {id: '803', name: '聯邦銀行    '},
                  {id: '804', name: '中華銀行    '},
                  {id: '805', name: '遠東銀行    '},
                  {id: '806', name: '復華銀行    '},
                  {id: '807', name: '永豐銀行    '},
                  {id: '808', name: '玉山銀行    '},
                  {id: '809', name: '凱基銀行    '},
                  {id: '810', name: '星展(台灣)銀行'},
                  {id: '812', name: '台新銀行    '},
                  {id: '814', name: '大眾銀行    '},
                  {id: '815', name: '日盛銀行    '},
                  {id: '816', name: '安泰銀行    '},
                  {id: '822', name: '中國信託    '},
                  {id: '825', name: '慶豐銀行    '},
                  {id: '901', name: '大里市農會  '},
                  {id: '903', name: '汐止農會    '},
                  {id: '904', name: '新莊農會    '},
                  {id: '910', name: '聯資中心    '},
                  {id: '912', name: '冬山農會    '},
                  {id: '916', name: '草屯農會    '},
                  {id: '922', name: '台南市農會  '},
                  {id: '928', name: '板橋農會    '},
                  {id: '951', name: '北農中心    '},
                  {id: '954', name: '中農中心    '}
                ],
                hotBanks: [
                  {id: '007', name: '第一銀行'},
                  {id: '809', name: '凱基銀行'},
                  {id: '013', name: '國泰世華'},
                  {id: '004', name: '臺灣銀行'},
                  {id: '005', name: '土地銀行'},
                  {id: '011', name: '上海銀行'}
                ]
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/extra/getGasCompanyList') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: [
                {name: '欣隆公司瓦斯費', id: '00146877'},
                {name: '欣湖瓦斯瓦斯費', id: '04779353'},
                {name: '欣雄瓦斯瓦斯費', id: '07861475'},
                {name: '台北瓦斯瓦斯費', id: '11072304'},
                {name: '欣泰油氣瓦斯費', id: '22000004'},
                {name: '竹建瓦斯瓦斯費', id: '22420667'},
                {name: '欣嘉瓦斯瓦斯費', id: '22658633'},
                {name: '大台南區瓦斯費', id: '22660547'},
                {name: '欣雲瓦斯瓦斯費', id: '22820164'},
                {name: '欣屏瓦斯瓦斯費', id: '23218199'},
                {name: '陽明瓦斯瓦斯費', id: '29705246'},
                {name: '欣欣天然瓦斯費', id: '33665843'},
                {name: '新海瓦斯瓦斯費', id: '35433242'},
                {name: '欣桃瓦斯瓦斯費', id: '43147546'},
                {name: '新竹瓦斯瓦斯費', id: '47032257'},
                {name: '裕苗瓦斯瓦斯費', id: '49650634'},
                {name: '欣中瓦斯瓦斯費', id: '51326774'},
                {name: '欣彰瓦斯瓦斯費', id: '59033674'},
                {name: '欣林瓦斯瓦斯費', id: '61758293'},
                {name: '中油氣費瓦斯費', id: '66845769'},
                {name: '欣南瓦斯瓦斯費', id: '69793483'},
                {name: '欣高瓦斯瓦斯費', id: '79882718'},
                {name: '欣芝瓦斯瓦斯費', id: '84705426'},
                {name: '南鎮瓦斯瓦斯費', id: '86689421'},
                {name: '竹名瓦斯費', id: '97194018'}
              ]
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/extra/fetchAllFeeData') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: [
                {
                  billType: '1',
                  billName: '中華電信費用',
                  displayNumber: '1231-05',
                  serial: '1'
                },
                {
                  billType: '2',
                  billName: '瓦斯費用',
                  displayNumber: '07861475-1213131313',
                  serial: '1'
                },
                {
                  billType: '3',
                  billName: '台北市自來水水費',
                  displayNumber: '1-21-213131-2',
                  serial: '1'
                },
                {
                  billType: '4',
                  billName: '台灣省自來水水費',
                  displayNumber: '11-21-123131-2',
                  serial: '1'
                },
                {
                  billType: '5',
                  billName: '台電公司電費',
                  displayNumber: '02-02-1213-21-1',
                  serial: '1'
                }
              ]
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/extra/fetchChtTelData') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: [{campcode: '1231', userno: '05', serialNo: '1'}]
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/extra/fetchGasFeeData') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {
                gasCompany: '07861475',
                userNo: '12131313',
                serialNo: '1'
              }
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/extra/fetchTaipeiWaterData') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: [
                {
                  areaB: '1',
                  areaM: '21',
                  accountFeeNo: '213131',
                  checkNo: '2',
                  serialNo: '1'
                }
              ]
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/extra/fetchTaiwanWaterData') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: [
                {
                  areaB: '11',
                  areaM: '21',
                  accountFeeNo: '123131',
                  checkNo: '2',
                  serialNo: '1'
                }
              ]
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/extra/fetchElectricityFeeData') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: [
                {
                  areaCode: '02',
                  areaM: '02',
                  branchNo: '21',
                  userno: '1213',
                  checkNo: '1',
                  serialNo: '1'
                }
              ]
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/extra/fetchDealAccounts') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: [
                {
                  BankCode: '004',
                  BankAccount: '1231111111111111',
                  BankName: '台灣銀行',
                  SubSerial: '1'
                }
              ]
            };
            return of(new HttpResponse({status: 200, body: body}));
          }

          if (
            request.url.endsWith('api/apply/verifyPageDesc') &&
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: [
                {
                  ed3VerifyBankAcct: '轉帳交易最便利',
                  ed3VerifyCreditCard: '限轉帳至本人他行帳號',
                  ed3VerifyOffline: '持雙證件至分行臨櫃辦理',
                  ed3VerifyNextTime: '需於30天內完成驗證'
                }
              ]
            };
            return of(new HttpResponse({status: 200, body: body}));
          }
        } // end if (environment.useFakeBackendApi)

        return next.handle(request);
      })
    );
    // .pipe(materialize())
    // .pipe(delay(500))
    // .pipe(dematerialize());
  }
}

export let fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
