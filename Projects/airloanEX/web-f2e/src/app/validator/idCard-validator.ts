import {  AbstractControl, ValidationErrors } from '@angular/forms';

export interface ValidatorFn {
  (control: AbstractControl): ValidationErrors | null
}

export function idCardValidator(): ValidatorFn {

  return (control: AbstractControl): {[key: string]: any} | null => {
    if (!control.value) {
      return;
    }

    const localTitle = /^[A-Z]{1}[1-2,8-9]{1}/;
    const foreignTitle = /^[A-Z]{1}[A-D]{1}/;
    const userIdTitle = control.value.slice(0, 2);

    const tab = 'ABCDEFGHJKLMNPQRSTUVXYWZIO';
    const A1 = new Array (1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3);
    const A2 = new Array (0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5);
    const Mx = new Array (9,8,7,6,5,4,3,2,1,1);

    if (control.value.length !== 10) {
      return { wrongAns: true };
    }

    // 本國人
    if (localTitle.test(userIdTitle)) {
        let i = tab.indexOf( control.value.charAt(0) );
        if ( i === -1 ) {
          return { wrongAns: true };
        }

        let sum = A1[i] + A2[i] * 9;

        for ( i = 1; i < 10; i++ ) {
            const v = parseInt(control.value.charAt(i), 10);
            if ( isNaN(v) ) {
              return { wrongAns: true };
            }
            sum = sum + v * Mx[i];
        }

        if ( sum % 10 !== 0 ) {
          return { wrongAns: true };
        }
        return null;
    }
    // else if (foreignTitle.test(userIdTitle)) {
    //     // 外國人
    //     // 這邊把身分證字號轉換成準備要對應的
    //     let id = control.value;
    //      id = (tab.indexOf(control.value.substring(0, 1)) + 10) + '' +
    //     ((tab.indexOf(control.value.substr(1,1))+10) % 10) + '' + control.value.substr(2, 8);

    //     // 開始進行身分證數字的相乘與累加，依照順序乘上1987654321
    //     const snumber = parseInt(id.substr(0, 1), 10) +
    //     parseInt(id.substr(1, 1), 10) * 9 +
    //     parseInt(id.substr(2, 1), 10) * 8 +
    //     parseInt(id.substr(3, 1), 10) * 7 +
    //     parseInt(id.substr(4, 1), 10) * 6 +
    //     parseInt(id.substr(5, 1), 10) * 5 +
    //     parseInt(id.substr(6, 1), 10) * 4 +
    //     parseInt(id.substr(7, 1), 10) * 3 +
    //     parseInt(id.substr(8, 1), 10) * 2 +
    //     parseInt(id.substr(9, 1), 10);

    //     const checkNum = parseInt(id.substr(10, 1), 10);
    //     // 模數 - 總和/模數(10)之餘數若等於第九碼的檢查碼，則驗證成功
    //     if ((10 - snumber % 10) === checkNum) {
    //       return null;
    //     } else {
    //       return {wrongAns: true};
    //     }

    // }
     else {
      return { wrongAns: true };
    }
  };

}
