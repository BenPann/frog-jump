import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
//
import { ErrorPageComponent } from './component/error-page/error-page.component';
import { InitLayoutComponent } from './component/init-layout/init-layout.component';
import { InitPlContractComponent } from './component/init-pl-contract/init-pl-contract.component';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';

// import { AppComponent } from './app.component';
// import { ApplyLayoutComponent } from './component/apply-layout/apply-layout.component';
// import { HomeComponent } from './component/home/home.component';

import { RouteGo } from './route-go/route-go';

const routes: Routes = [
  { path: '', component: PageNotFoundComponent },
  // apply 申請流程
  /*{ path: 'home', component: HomeComponent },
  {
    path: '',
    component: ApplyLayoutComponent,
    children: [
      { path: 'apply', loadChildren: () => import('./component/apply/apply.module').then(m => m.ApplyModule) },
      { path: 'CAD', loadChildren: () => import('./component/cad/cad.module').then(m => m.CADModule) },
      { path: 'CAL', loadChildren: () => import('./component/cal/cal.module').then(m => m.CALModule) },
      { path: 'SGN', loadChildren: () => import('./component/sgn/sgn.module').then(m => m.SGNModule) },
      { path: 'UPL', loadChildren: () => import('./component/upl/upl.module').then(m => m.UPLModule) },
    ]
  },*/
  // init 立約流程
  {
    path: '',
    component: InitLayoutComponent,
    children: [
      { path: RouteGo.initPlContract.path, component: InitPlContractComponent },
      {
        path: 'o',
        loadChildren: () =>
          import('./o-user/o-user.module').then(m => m.OUserModule)
      },
      {
        path: 'n',
        loadChildren: () =>
          import('./n-user/n-user.module').then(m => m.NUserModule)
      },
    ]
  },
  { path: 'error', component: ErrorPageComponent },
  { path: '**', component: PageNotFoundComponent }
];

//   { path: 'addPhoto', component: AddPhotoComponent },

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'top' }),
    FormsModule
  ],
  exports: [RouterModule, FormsModule]
})
export class AppRoutingModule { }
