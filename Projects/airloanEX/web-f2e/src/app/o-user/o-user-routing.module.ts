import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SimpleRootComponent } from '../shared/component/simple-root/simple-root.component';
import { TokenGuard } from '../guard/token.guard';
import { LeaveGuard } from '../guard/leave.guard';

import { OtpPlContractComponent } from './component/otp-pl-contract/otp-pl-contract.component';

import { RouteGo } from '../route-go/route-go';

const routes: Routes = [
  {
    path: RouteGo.otpPlContract.path,
    component: OtpPlContractComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OUserRoutingModule {}
