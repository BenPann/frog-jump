import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtpPlContractComponent } from './otp-pl-contract.component';

describe('OtpPlContractComponent', () => {
  let component: OtpPlContractComponent;
  let fixture: ComponentFixture<OtpPlContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtpPlContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtpPlContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
