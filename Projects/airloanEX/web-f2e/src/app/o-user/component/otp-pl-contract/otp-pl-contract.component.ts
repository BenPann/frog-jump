import { Component, OnInit, OnDestroy, AfterViewInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { OtpService } from '../../../service/otp/otp.service';
import { ModalFactoryService } from '../../../shared/service/common/modal-factory.service';
import { takeWhile, tap } from 'rxjs/operators';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { interval } from 'rxjs';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { FormCheckService } from 'src/app/shared/service/common/form-check.service';
import { BreakPointService } from 'src/app/service/break-point/break-point.service';
import { ApplyAccountService } from '../../../service/apply-account/apply-account.service';
import { RouterDirectService } from '../../../service/router-direct/router-direct.service';
import { ValidateService } from 'src/app/service/validate/validate.service';
import { VerifyCountResp } from 'src/app/service/validate/validate.interface';
import { ProductService } from 'src/app/service/product/product.service';
import { CleaveService } from '../../../service/cleave/cleave.service';
import { initAllCloseButtons, initCleaveNumber, initCloseButtons, showDialog } from 'src/app/js/start.page.js';

import { initRapidForm } from 'src/app/js/verify.page.js';
@Component({
  selector: 'app-otp-pl-contract',
  templateUrl: './otp-pl-contract.component.html',
  styleUrls: ['./otp-pl-contract.component.scss']
})
export class OtpPlContractComponent implements OnInit, OnDestroy, AfterViewInit {
  private alive = true;
  private resetTimer = true;
  // 上方步驟
  stepStr = 1;

  // 下方按鈕的名字
  leftBtnText = '';
  rightBtnText = '';

  // 表單
  form: FormGroup;

  // 計秒
  secText = 60;

  verifyCount: VerifyCountResp = {
    authErrCount: 0,
    signErrCount: 0,
    eddaErrCount: 0
  };

  // user假資料
  userName = '';
  accountType = '帳戶';
  userPhoneNum = '';
  sendSuccess = false;

  constructor(
    private routerHelperService: RouterHelperService,
    private fb: FormBuilder,
    private userDataService: UserDataService,
    private productService: ProductService,
    private routerDirectService: RouterDirectService,
    private validateService: ValidateService,
    private otpService: OtpService,
    private modalFactoryService: ModalFactoryService,
    public formCheckService: FormCheckService,
    private breakPointService: BreakPointService,
    private applyAccountService: ApplyAccountService,
    public cleave: CleaveService,
    private elem: ElementRef,
  ) {}

  ngAfterViewInit(): void {
    initCleaveNumber();
    initRapidForm(this.elem);
    initCloseButtons();
    initAllCloseButtons();

    setTimeout(() => this.sendOtp());
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  ngOnInit() {
    // this.userName = this.userDataService.initUserTypeData.chtName;
    // 取前一頁面的電話號碼
    this.userPhoneNum = this.routerDirectService.otpPhoneNum;
    // console.log('this.userPhoneNum=' + this.userPhoneNum);

    this.form = this.fb.group({
      // inputcaptcha: [
      //   '',
      //   [
      //     Validators.required,
      //     Validators.minLength(6),
      //     Validators.pattern(/^-?(0|[0-9]\d*)?$/)
      //   ]
      // ],
      tel1: [''],
      tel2: [''],
      tel3: [''],
      tel4: [''],
      tel5: [''],
      tel6: [''],
    });
    this.leftBtnText = '上一步';
    this.rightBtnText = '下一步';
  } // end ngOnInit

  // 重領驗證碼
  sendSMS(): void {

    this.form.get('tel1').setValue('');
    this.form.get('tel2').setValue('');
    this.form.get('tel3').setValue('');
    this.form.get('tel4').setValue('');
    this.form.get('tel5').setValue('');
    this.form.get('tel6').setValue('');
    this.sendOtp();
  }

  sendOtp() {
    this.otpService
      .sendOtp(this.userPhoneNum)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          if (data.result.code === '0000') {
            this.modalFactoryService.modalDefault({
              message: '已發送簡訊至您的手機號碼，請輸入驗證碼進行最後確認'
            });
            this.sendSuccess = true;
            // 計時
            this.setTime(60);
          } else {
            this.modalFactoryService.modalDefault({
              message: data.result.message
            });
            this.secText = 0;
          }
        } else {
          this.modalFactoryService.modalDefault({
            message: data.message
          });
          this.secText = 0;
        }
      });
  }

  // 下一頁的按鈕是否上鎖
  isNextBtnDisabled(): boolean {
    return this.form.invalid || this.sendSuccess === false;
  }

  // 計時
  setTime(tempT: number): void {
    this.resetTimer = true;
    let total = tempT;
    this.secText = total;
    interval(1000)
      .pipe(
        takeWhile(() => total-- !== 0),
        takeWhile(() => this.resetTimer),
        takeWhile(() => this.alive),
        tap(data => (this.secText = total))
      )
      .subscribe();
  }

  // 左邊的按鈕是否上鎖
  isLeftBtnDisabled(): boolean {
    return false;
  }

  showConsume() {
    this.modalFactoryService.modalDefault({
      message: '如有存款相關問題請撥打02-80239088'
    });
  }

  newAccount() {
    this.routerHelperService.navigate(['n', 'otp']);
  }

  // 下一頁
  next(): void {

    const req = this.form.get('tel1').value + this.form.get('tel2').value + this.form.get('tel3').value
        + this.form.get('tel4').value + this.form.get('tel5').value + this.form.get('tel6').value;

    this.otpService
        .checkOtp(this.userPhoneNum, req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          if (data.result.code === '0000') {
            const userData = Object.assign(
              {},
              this.userDataService.initUserTypeData
            );

            // 保存OTP驗證結果並鎖定手機號碼
            this.routerDirectService.passOTP = 'Y';
            userData.phone = this.userPhoneNum;
            this.userDataService.initUserTypeData = userData;

            this.routerHelperService.navigate(this.routerDirectService.otpNextPage);
          } else {
            this.modalFactoryService.modalDefault({
              message: data.result.message
            });
          }
        } else {
          this.modalFactoryService.modalDefault({
            message: data.message
          });
        }
      });
  }

  // 下方左邊按鈕(執行其他動作)
  otherActions(): void {
    this.routerHelperService.navigate(this.routerDirectService.otpPrevPage);
    // // 重領驗證碼
    // this.sendOtp();
    // this.form.get('inputcaptcha').setValue('');
  }

  checkValue(name: string, value: string) {
    if (value) {
      if (!/[0-9]$/.test(value)) {
        this.form.get(name).patchValue('');
       } else if (this.form.valid) {
        this.next();
      }
    }
  }
}
