import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OUserRoutingModule } from './o-user-routing.module';

import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

import { OtpPlContractComponent } from './component/otp-pl-contract/otp-pl-contract.component';

@NgModule({
  declarations: [
    OtpPlContractComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    OUserRoutingModule,
    SharedModule
  ]
})
export class OUserModule {}
