import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TokenGuard } from '../guard/token.guard';
import { LeaveGuard } from '../guard/leave.guard';

import { ChooseIdentificationStylePlContractComponent } from './component/choose-identification-style-pl-contract/choose-identification-style-pl-contract.component';
import { IdentifyByBankAccountPlContractComponent } from './component/identify-by-bank-account-pl-contract/identify-by-bank-account-pl-contract.component';
import { ChoosePlContractComponent } from './component/choose-pl-contract/choose-pl-contract.component';
import { InfoPlContractComponent } from './component/info-pl-contract/info-pl-contract.component';
import { ContentComponent } from './component/content/content.component';
import { ContentrplComponent } from './component/contentrpl/contentrpl.component';
import { AgreePlContractComponent } from './component/agree-pl-contract/agree-pl-contract.component';
import { AgreeRplContractComponent } from './component/agree-rpl-contract/agree-rpl-contract.component';
import { AgreeCashcardContractComponent } from './component/agree-cashcard-contract/agree-cashcard-contract.component';
import { AllFinishPlContractComponent } from './component/all-finish-pl-contract/all-finish-pl-contract.component';
import { AllFailurePlContractComponent } from './component/all-failure-pl-contract/all-failure-pl-contract.component';
import { PreviewPlContractComponent } from './component/preview-pl-contract/preview-pl-contract.component';
import { UpdateDgtContractComponent } from './component/update-dgt-contract/update-dgt-contract.component';
import { RouteGo } from '../route-go/route-go';

const routes: Routes = [
  {
    path: RouteGo.chooseIdentificationStylePlContract.path,
    component: ChooseIdentificationStylePlContractComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { breakPoint: true , browseLog: true }
  },
  {
    path: RouteGo.identifyByBankAccountPlContract.path,
    component: IdentifyByBankAccountPlContractComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { breakPoint: true, browseLog: true }
  },
  {
    path: RouteGo.choosePlContract.path,
    component: ChoosePlContractComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true}
  },
  {
    path: RouteGo.infoPlContract.path,
    component: InfoPlContractComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true}
  },
  {
    path: RouteGo.content.path,
    component: ContentComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true}
  },
  {
    path: RouteGo.contentrpl.path,
    component: ContentrplComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true}
  },
  {
    path: RouteGo.agreePlContract.path,
    component: AgreePlContractComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true}
  },
  {
    path: RouteGo.agreeRplContract.name,
    component: AgreeRplContractComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true}
  },
  {
    path: RouteGo.agreeCashcardContract.path,
    component: AgreeCashcardContractComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true}
  },
  {
    path: RouteGo.allFinishPlContract.path,
    component: AllFinishPlContractComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true}
  },
  {
    path: RouteGo.allFailurePlContract.path,
    component: AllFailurePlContractComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true}
  },
  {
    path: RouteGo.previewPlContract.path,
    component: PreviewPlContractComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true}
  },
  {
    path: RouteGo.updateDgtContract.path,
    component: UpdateDgtContractComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true}
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NUserRoutingModule {}
