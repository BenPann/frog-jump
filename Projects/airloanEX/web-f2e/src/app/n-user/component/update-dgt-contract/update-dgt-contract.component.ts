import { Component, OnInit, OnDestroy } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {takeWhile} from 'rxjs/operators';
import { ModalFactoryService } from '../../../shared/service/common/modal-factory.service';

import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { RouteGo } from 'src/app/route-go/route-go';
import { RouterDirectService } from 'src/app/service/router-direct/router-direct.service';
import {InitApiService} from 'src/app/service/init/init-api.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';

@Component({
  selector: 'app-update-dgt-contract',
  templateUrl: './update-dgt-contract.component.html',
  styleUrls: ['./update-dgt-contract.component.scss']
})
export class UpdateDgtContractComponent implements OnInit, OnDestroy {
  private alive = true;
  stepStr = 2;
  form: FormGroup;
  dataForm: FormGroup;
  previewHtml: string;
  constructor(
    private fb: FormBuilder,
    private routerHelperService: RouterHelperService,
    private initApiService: InitApiService,
    private routerDirectService: RouterDirectService,
    private modalFactoryService: ModalFactoryService,
    private userDataService: UserDataService,
  ) { }

  ngOnDestroy() {
    this.alive = false;
  }

  ngOnInit() {
    const productId = this.userDataService.contractMain.productId;
    
    // 更新立約完成 呼叫立約API UpdateDgt
    this.initApiService.updateDgtConst(productId)
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      // console.log(data);
      if (data.status === 0) {
        this.routerHelperService.navigate(RouteGo.allFinishPlContract.navi);
      } else {
        this.routerHelperService.navigate(RouteGo.allFailurePlContract.navi); // FIXME: Charles: must use RouteGo.allFailurePlContract.navi
        this.modalFactoryService.modalDefault({
          message: data.message
        });
      }
    });

    this.dataForm = this.fb.group({
    });
  }
}
