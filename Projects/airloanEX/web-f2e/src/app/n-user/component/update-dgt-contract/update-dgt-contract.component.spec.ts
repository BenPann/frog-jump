import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDgtContractComponent } from './update-dgt-contract.component';

describe('UpdateDgtContractComponent', () => {
  let component: UpdateDgtContractComponent;
  let fixture: ComponentFixture<UpdateDgtContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDgtContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDgtContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
