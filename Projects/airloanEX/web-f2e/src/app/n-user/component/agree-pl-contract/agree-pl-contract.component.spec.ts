import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgreePlContractComponent } from './agree-pl-contract.component';

describe('AgreePlContractComponent', () => {
  let component: AgreePlContractComponent;
  let fixture: ComponentFixture<AgreePlContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgreePlContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreePlContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
