import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { RouteGo } from 'src/app/route-go/route-go';
import { ContractMain, RateData } from 'src/app/service/airloanex/airloanex.interface';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-contentrpl',
  templateUrl: './contentrpl.component.html',
  styleUrls: ['./contentrpl.component.scss']
})
export class ContentrplComponent implements OnInit {

  main: ContractMain;
  tbA_aprove_amount: string;
  rate: string;
  dailyPay = 0;

  constructor(
    private userDataService: UserDataService,
    private routerHelperService: RouterHelperService,
    private fb: FormBuilder,
    private dateFormatter: NgbDateParserFormatter,
    private router: Router,
  ) { }

  ngOnInit() {
    this.main = this.userDataService.contractMain;
    this.tbA_aprove_amount = this.main.tbA_aprove_amount.replace('0000.0', '萬');
    this.rate = this.main.rate;
    this.dailyPay = this.precisionRoundMod(10000 * (+this.rate/100) / 365, 0); // 1.6是參數，計算方式=10,000*12.99%(依借款利率)/365 (四捨五入整數顯示)
  }

  precisionRoundMod(number, precision) {
    var factor = Math.pow(10, precision);
    var n = precision < 0 ? number : 0.01 / factor + number;
    return Math.round( n * factor) / factor;
  }

  // 下一步
  next(): void {
    //routeUrlName 用於線上立約 完成頁面 Title顯示判斷為額度型貸款契約書/分期型貸款契約書
    sessionStorage.setItem('routeUrlName', this.router.url);
    this.routerHelperService.navigate(RouteGo.agreeRplContract.navi);
  }
}
