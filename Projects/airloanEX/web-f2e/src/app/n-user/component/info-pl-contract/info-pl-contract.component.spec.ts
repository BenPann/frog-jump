import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoPlContractComponent } from './info-pl-contract.component';

describe('InfoPlContractComponent', () => {
  let component: InfoPlContractComponent;
  let fixture: ComponentFixture<InfoPlContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoPlContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoPlContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
