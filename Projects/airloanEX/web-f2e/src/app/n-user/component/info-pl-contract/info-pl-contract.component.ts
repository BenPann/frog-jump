// import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { InitApiService } from 'src/app/service/init/init-api.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ValidateService } from 'src/app/service/validate/validate.service';
import { PCODE2566Req, EddaReq } from './../../../service/validate/validate.interface';
import { Component, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { map, switchMap, shareReplay, tap } from 'rxjs/operators';
import { RouterHelperService } from '../../../shared/service/common/router-helper.service';
import { RouterDirectService } from 'src/app/service/router-direct/router-direct.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ProductService } from 'src/app/service/product/product.service';
import { ModalFactoryService } from 'src/app/shared/service/common/modal-factory.service';

import { FormCheckService} from 'src/app/shared/service/common/form-check.service';
import { DropdownItem } from 'src/app/interface/resultType';
import { VerifyCountResp, PCode2566Resp } from 'src/app/service/validate/validate.interface';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { OtpService } from 'src/app/service/otp/otp.service';

import { KgiBankInfo } from '../../../service/airloanex/airloanex.interface';
import { RouteGo } from 'src/app/route-go/route-go';
import { mainModule } from 'process';
import { WebResult } from 'src/app/interface/resultType';

@Component({
  selector: 'app-info-pl-contract',
  templateUrl: './info-pl-contract.component.html',
  styleUrls: ['./info-pl-contract.component.scss']
})
export class InfoPlContractComponent implements OnInit, OnDestroy {
  private alive = true;
  stepStr = 2;

  // form表單
  dataForm: FormGroup;

  kgiBankList: KgiBankInfo[] = [];

  // 銀行別 dropdownData
  pcode2566BankList: DropdownItem[];

  // 銀行別 dropdownData
  eddaBankList: DropdownItem[];

  // 驗證 verifyCount
  verifyCount: VerifyCountResp = {
    authErrCount: 0,
    signErrCount: 0,
    eddaErrCount: 0
  };

  // 繳款日期設定
  minDate: NgbDateStruct;
  maxDate: NgbDateStruct;
  today = new Date();

  // 手機號碼錯誤
  errorMsgPhone: string;

  // 繳款日期
  payKindDay: string;
  payDayOptions: number[] = Array.from(new Array(31), (x, i) => i + 1); // payDayOptions=[1,...31]

  selectedValue = 0;

  illegalBank = ['農會', '漁會'];

  constructor(
    private fb: FormBuilder,
    private commonDataService: CommonDataService,
    private validateService: ValidateService,
    private routerHelperService: RouterHelperService,
    private routerDirectService: RouterDirectService,
    private userDataService: UserDataService,
    private productService: ProductService,
    private modalFactoryService: ModalFactoryService,
    public formCheckService: FormCheckService,
    public otpService: OtpService,
    private initApiService: InitApiService,
  ) {}

  ngOnDestroy(): void {
  }

  ngOnInit() {
    // init 繳款日期設定
    this.payKindDay = this.initDatePicker();

    // 表單 init
    this.initDataForm();

    // 取得銀行別
    this.commonDataService
    .getPcode2566BankList()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        // data = data.filter(
        //   (value: { dataName: string }) => value.dataName !== '809 凱基銀行'
        // );
        // console.log(data);
        this.pcode2566BankList = data;
      });

    // 取得銀行別
    this.commonDataService
      .getEddaBankList()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.eddaBankList = data;
      });

    // 取得NCCC和PCode2566最大驗身次數 getMaxVerifyCount()
    this.productService
      .getMaxVerifyCount()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.userDataService.maxVerifyCount = data.result;
        } else {
          this.modalFactoryService.modalDefault({ message: data.message });
        }
      });

    // 取得驗身總次數 getVerifyCount()
    this.validateService
    .getVerifyCount()
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      if (data.status === 0) { // 取得
        this.verifyCount = data.result;
        // 超過最大驗身次數maxVerifyCount
        if (
          this.verifyCount.signErrCount >=
          Number(this.userDataService.maxVerifyCount)
        ) {
          this.routerHelperService.navigate(this.routerDirectService.prevPage);
        }
      } else { // 取得失敗
        this.modalFactoryService.modalDefault({ message: data.message });
      }
    });

    this.kgiBankList = this.userDataService.contractMain.kgiBankInfos;

    // 是否有預載的資料 initBankNo, initAccount, initPhone, initSordBankNo, initSordAccNo
    this.selectPaybank();
    this.selectSordBank();
  } // end ngOnInit

  // 初進網頁 取繳款日期設定
  initDatePicker() {
    const main = this.userDataService.contractMain;
    return this.computePayBank(main.initBankNo);
  }

  // 20201117取立約日之撥款日(下一營業日) 有代償銀行 或者 撥款銀行為他行 15點後 改下一營業日 如果撥款銀行為自行 21點後 改下一營業日
  computePayBank(initBankNo: string) {
    const nowToday = this.today.getDate();
    const hour = this.today.getHours();
    const main = this.userDataService.contractMain;

    if (main.payBank && hour > 14) {
      return String(nowToday + 1);
    } else if (initBankNo && initBankNo.substring(0, 3) === '809' && hour > 21) {
      return String(nowToday + 1);
    } else if (initBankNo && initBankNo.substring(0, 3) !== '809' && hour > 14) {
      return String(nowToday + 1);
    }
    return String(nowToday);
  }

  // 20201117改變撥款銀行別 取繳款日期設定
  changePayDate() {
    const formVal = this.dataForm.getRawValue();
    const payMentDay = this.dataForm.get('payMentDay');
    payMentDay.setValue(this.computePayBank(formVal.bankId));
  }

  selectPaybank() {
    const main = this.userDataService.contractMain;
    if (main) {
      // 是否為凱基既有戶
      let hasPayBankOption = main.hasPayBankOption;
      if (main.inputMode === 'N') {
        hasPayBankOption = false;
      }
      // const hasSordBankOption = !main.hasSordBankOption;
      this.dataForm.get('hasPayBankOption').setValue(hasPayBankOption);
      // this.dataForm.get('hasSordBankOption').setValue(hasSordBankOption);
      // dataForm
      const option = this.dataForm.get('selectedOption').value;
      const tmpBankId = this.dataForm.get('tmpBankId');
      const tmpAccount = this.dataForm.get('tmpAccount');
      const tmpBranchId = this.dataForm.get('tmpBranchId');
      const bankId = this.dataForm.get('bankId');
      const account = this.dataForm.get('account');
      const branchId = this.dataForm.get('branchId');
      const phone = this.dataForm.get('phone');
      const eddaBankId = this.dataForm.get('eddaBankId');
      const eddaAccountNo = this.dataForm.get('eddaAccountNo');
      // 0:撥款至本行帳戶 TODO: DELETE ME
      if (option === '0' && hasPayBankOption) {

        // 暫存 tmpBankId, tmpAccount, tmpBranchId
        tmpBankId.setValue(bankId.value);
        tmpAccount.setValue(account.value);
        tmpBranchId.setValue(branchId.value);

        this.isKgiBankNoDisabled();
        this.isKgiAcctNoDisabled();

        const kgiBankInfos: KgiBankInfo[] = main.kgiBankInfos;
        const k = kgiBankInfos[0];

        // bankId
        if (k.kgiBankNo) {
          bankId.setValue(k.kgiBankNo.substring(0, 3));
        } else {
          bankId.setValue('');
        }
        // account
        if (k.kgiAcctNo) {
          account.setValue(k.kgiAcctNo);
        } else {
          account.setValue('');
        }
        // branchId
        if (k.kgiBranchNo) {
          branchId.setValue(k.kgiBranchNo);
        } else {
          branchId.setValue('');
        }
      } else {
        // 1:撥款至他行帳戶
        this.isBankIdDisabled();
        this.isAccountDisabled();
        this.isBranchIdDisabled();
        // bankId
        if (main.initBankNo) {
          bankId.setValue(main.initBankNo.substring(0, 3));
        } else if (main.bank) {
          bankId.setValue(main.bank.substring(0, 3));
        } else {
          bankId.setValue(tmpBankId.value);
        }
        // account
        if (main.initAccount) {
          account.setValue(main.initAccount);
        } else if (main.account) {
          account.setValue(main.account);
        } else {
          account.setValue(tmpAccount.value);
        }
        // branchId
        if (main.initBankNo) {
          if (main.initBankNo.length >= 7) {
            branchId.setValue(main.initBankNo.substring(3, 7));
          }
        } else {
          branchId.setValue(tmpBranchId.value);
        }
      }

      this.isPhoneDisabled();
      this.isEddaBankIdDisabled();
      this.isEddaAccountNoDisabled();
      if (main.phone) {
        phone.setValue(main.phone);
      } else if (main.initPhone) {
        phone.setValue(main.initPhone);
      }

      if (main.initSordBankNo) {
        eddaBankId.setValue(main.initSordBankNo.substring(0, 3));
      }
      if (main.initSordAccNo) {
        eddaAccountNo.setValue(main.initSordAccNo);
      }

      this.isPayMentDayDisabled();
      if (main.initPayKindDay) {
        this.dataForm.get('payMentDay').setValue(main.initPayKindDay);
      } else if (main.payDate) {
        this.dataForm.get('payMentDay').setValue(main.payDate);
      } else if (main.payMentDay) {
        this.dataForm.get('payMentDay').setValue(main.payMentDay);
      }
    }
  } // end selectPaybank

  /**
   * 下拉式切換：撥款至本行帳戶/撥款至他行帳戶
   */
  // selectPayBank(k?: KgiBankInfo) {
  selectPayBank() {

    const main = this.userDataService.contractMain;
    if (main) {
      // 是否為凱基既有戶
      let hasPayBankOption = main.hasPayBankOption;
      if (main.inputMode === 'N') {
        hasPayBankOption = false;
      }
      this.dataForm.get('hasPayBankOption').setValue(hasPayBankOption);
      // dataForm
      const option = this.dataForm.get('selectedOption').value;
      const k: KgiBankInfo|string = option;
      const tmpBankId = this.dataForm.get('tmpBankId');
      const tmpAccount = this.dataForm.get('tmpAccount');
      const tmpBranchId = this.dataForm.get('tmpBranchId');
      const bankId = this.dataForm.get('bankId');
      const account = this.dataForm.get('account');
      const branchId = this.dataForm.get('branchId');

      if (typeof k === 'string') {
        if (k === '1') {
          // 1:撥款至他行帳戶
          this.isBankIdDisabled();
          this.isAccountDisabled();
          this.isBranchIdDisabled();
          // bankId
          if (main.initBankNo) {
            bankId.setValue(main.initBankNo.substring(0, 3));
          } else if (main.bank) {
            bankId.setValue(main.bank.substring(0, 3));
          } else {
            bankId.setValue(tmpBankId.value);
          }
          // account
          if (main.initAccount) {
            account.setValue(main.initAccount);
          } else if (main.account) {
            account.setValue(main.account);
          } else {
            account.setValue(tmpAccount.value);
          }
          // branchId
          if (main.initBankNo) {
            if (main.initBankNo.length >= 7) {
              branchId.setValue(main.initBankNo.substring(3, 7));
            }
          } else {
            branchId.setValue(tmpBranchId.value);
          }
        }
      } else {

        // 0:撥款至本行帳戶
        // 暫存 tmpBankId, tmpAccount, tmpBranchId
        tmpBankId.setValue(bankId.value);
        tmpAccount.setValue(account.value);
        tmpBranchId.setValue(branchId.value);

        this.isKgiBankNoDisabled();
        this.isKgiAcctNoDisabled();

        const kgiBankInfos: KgiBankInfo[] = main.kgiBankInfos;

        // bankId
        if (k.kgiBankNo) {
          bankId.setValue(k.kgiBankNo.substring(0, 3));
        } else {
          bankId.setValue('');
        }
        // account
        if (k.kgiAcctNo) {
          account.setValue(k.kgiAcctNo);
        } else {
          account.setValue('');
        }
        // branchId
        if (k.kgiBranchNo) {
          branchId.setValue(k.kgiBranchNo);
        } else {
          branchId.setValue('');
        }
      }
    }
  } // end selectPayBank

  /**
   * 下拉式切換：使用本行帳戶繳款/使用他行帳戶繳款
   */
  selectSordBank() {

    const main = this.userDataService.contractMain;
    if (main) {
      // 是否為凱基既有戶
      let hasSordBankOption = main.hasSordBankOption;
      if (main.inputMode === 'N') {
        hasSordBankOption = false;
      }
      this.dataForm.get('hasSordBankOption').setValue(hasSordBankOption);
      // dataForm
      const option = this.dataForm.get('selectedEddaOption').value;
      const k: KgiBankInfo|string = option;
      const tmpEddaBankId = this.dataForm.get('tmpEddaBankId');
      const tmpEddaAccountNo = this.dataForm.get('tmpEddaAccountNo');
      const eddaBankId = this.dataForm.get('eddaBankId');
      const eddaAccountNo = this.dataForm.get('eddaAccountNo');

      if (typeof k === 'string') {
        if (k === '1') {
          // 1:使用他行帳戶繳款
          this.isEddaBankIdDisabled();
          this.isEddaAccountNoDisabled();

          if (main.initSordBankNo) {
            eddaBankId.setValue(main.initSordBankNo.substring(0, 3));
          } else if (main.eddaBankId) {
            eddaBankId.setValue(main.eddaBankId.substring(0, 3));
          } else {
            eddaBankId.setValue(tmpEddaBankId.value);
          }

          if (main.initSordAccNo) {
            eddaAccountNo.setValue(main.initSordAccNo);
          } else if (main.account) {
            eddaAccountNo.setValue(main.eddaAccountNo);
          } else {
            eddaAccountNo.setValue(tmpEddaAccountNo.value);
          }
        }
      } else {

        // 0:使用本行帳戶繳款
        // 暫存 tmpBankId, tmpAccount, tmpBranchId
        tmpEddaBankId.setValue(eddaBankId.value);
        tmpEddaAccountNo.setValue(eddaAccountNo.value);

        this.isKgiEddaBankIdDisabled();
        this.isKgiEddaAccountNoDisabled();

        // bankId
        if (k.kgiBankNo) {
          eddaBankId.setValue(k.kgiBankNo.substring(0, 3));
        } else {
          eddaBankId.setValue('');
        }
        // account
        if (k.kgiAcctNo) {
          eddaAccountNo.setValue(k.kgiAcctNo);
        } else {
          eddaAccountNo.setValue('');
        }
      }

    }
  } // end selectSordBank

  // 表單 init
  initDataForm() {
    this.dataForm = this.fb.group({
      bankId: ['', [Validators.required]],
      account: ['', [Validators.required,
                      Validators.minLength(9),
                      Validators.pattern(/^[0-9]{10,}$/)]],
      phone: ['',
        [
          Validators.required,
          Validators.minLength(10),
          Validators.pattern(/^[0]{1}[9]{1}[0-9]{8}$/)
        ]
      ],
      eddaBankId: ['', [Validators.required]],
      eddaAccountNo: ['', [Validators.required,
                        Validators.minLength(9),
                        Validators.pattern(/^[0-9]{10,}$/)]],
      payMentDay: [this.payKindDay, []], // 繳款日
      branchId: ['', [
        Validators.minLength(4),
        Validators.pattern(/^[0-9]{4,5}$/)]],
      selectedOption: ['', []],
      selectedEddaOption: ['', []],
      hasPayBankOption: [false, []],
      tmpBankId: '',
      tmpAccount: '',
      tmpBranchId: '',
      tmpEddaBankId: '',
      tmpEddaAccountNo: '',
      hasSordBankOption: [false, []],
    });
  } // end initDataForm

  isKgiBankNoDisabled(): void {
    const control = this.dataForm.get('bankId');
    this.userDataService.contractMain.kgiBankInfos.length > 0
    ? control.disable()
    : control.enable();
  }

  isKgiAcctNoDisabled(): void {
    const control = this.dataForm.get('account');
    this.userDataService.contractMain.kgiBankInfos.length > 0
    ? control.disable()
    : control.enable();
  }

  isBankIdDisabled(): void {
    const control = this.dataForm.get('bankId');
    const main = this.userDataService.contractMain;
    main.inputMode === 'N' && main.initBankNo
    ? control.disable()
    : control.enable();
  }

  isBranchIdDisabled(): void {
    const control = this.dataForm.get('branchId');
    const main = this.userDataService.contractMain;
    main.inputMode === 'N'
    ? control.disable()
    : control.enable();
  }

  isAccountDisabled(): void {
    const control = this.dataForm.get('account');
    const main = this.userDataService.contractMain;
    main.inputMode === 'N' && main.initAccount
    ? control.disable()
    : control.enable();
  }

  isPhoneDisabled(): void {
    const control = this.dataForm.get('phone');
    const main = this.userDataService.contractMain;
    main.inputMode === 'N' && main.phone
    ? control.disable()
    : control.enable();
  }

  isKgiEddaBankIdDisabled(): void {
    const control = this.dataForm.get('eddaBankId');
    const main = this.userDataService.contractMain;
    this.userDataService.contractMain.kgiBankInfos.length > 0
    ? control.disable()
    : control.enable();
  }

  isKgiEddaAccountNoDisabled(): void {
    const control = this.dataForm.get('eddaAccountNo');
    const main = this.userDataService.contractMain;
    this.userDataService.contractMain.kgiBankInfos.length > 0
    ? control.disable()
    : control.enable();
  }

  isEddaBankIdDisabled(): void {
    const control = this.dataForm.get('eddaBankId');
    const main = this.userDataService.contractMain;
    main.inputMode === 'N' && main.initSordBankNo
    ? control.disable()
    : control.enable();
  }

  isEddaAccountNoDisabled(): void {
    const control = this.dataForm.get('eddaAccountNo');
    const main = this.userDataService.contractMain;
    main.inputMode === 'N' && main.initSordAccNo
    ? control.disable()
    : control.enable();
  }

  isPayMentDayDisabled(): void {
    const control = this.dataForm.get('payMentDay');
    const main = this.userDataService.contractMain;
    main.inputMode === 'N' // && main.initPayKindDay
    ? control.disable()
    : control.enable();
  }

  clearErrorMsg(): void {
    this.errorMsgPhone = '';
  }

  // 手機號碼驗證
  validatePhone(): void {
    /* this.otpService
      .isPhoneSuccess(this.dataForm.get('phone').value)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data2 => {
        if (data2.status === 0 && data2.result === 'Y') {
        } else if (data2.status === 0 && data2.result === 'N') {
          this.errorMsgPhone = '手機號碼' + this.dataForm.get('phone').value + '已有其他客戶申請使用，請確認您的手機號碼是否正確或洽各分行辦理開戶，謝謝您！';
          this.dataForm.get('phone').setErrors({ incorrect: true });
        } else {
          this.errorMsgPhone = data2.message;
          this.dataForm.get('phone').setErrors({ incorrect: true });
        }
      }); */
  }

  // 同意按鈕解鎖
  isNextDisabled(): boolean {
    return !this.dataForm.valid;
  }

  // 上一步
  backStep() {
    this.routerHelperService.navigate(RouteGo.agreePlContract.navi);
  }

  // 下一步
  nextStep(): void {
    const legalBank = this.legalBank();
    if (legalBank) {
      this.saveAndCheckInfo();
    }
  }

  // 是否 為合法銀行(撥款與授扣銀行不為農漁會) 
  legalBank(): boolean {
    const pcode2566Bank = this.pcode2566BankList.find(x => x.dataKey === this.dataForm.get('bankId').value);
    const eddaBank = this.eddaBankList.find(x => x.dataKey === this.dataForm.get('eddaBankId').value);
    const pcode2566IllegalBank = this.illegalBank.some(x => pcode2566Bank.dataName.includes(x));
    const eddaBankIllegalBank = this.illegalBank.some(x => eddaBank.dataName.includes(x));
    const main = this.userDataService.contractMain;
    if (main.inputMode === 'Y' && (pcode2566IllegalBank || eddaBankIllegalBank)) {
      this.modalFactoryService.modalDefault({
        message: '撥款與授扣銀行不可為農漁會，請重新選擇'
      });
      return false;
    }
    return true;
  }

  // 暫存資料 撥款資訊 授扣資訊
  saveAndCheckInfo(): void {
    
    let result = true;
    const infoPlContractReq = this.infoPlContractReq();
    const pcode2566Req = this.pcode2566Req();
    const eddaReq = this.eddaReq();

    // 20201120 Ben 提出 只要input_mode是N 不作PCode2566驗證
    const main = this.userDataService.contractMain;
    const formVal = this.dataForm.getRawValue(); // get disabled controls
    // 無用 this.initApiService.judgeExceedPayDateByExpBank(formVal.bankId, formVal.account)
    const infoPlContract$ = this.initApiService.infoPlContract(infoPlContractReq)
    const pcode2566$ = this.validateService.setPCode2566Sign(pcode2566Req);
    const edda$ = this.validateService.getVerifyEdda(eddaReq);

    infoPlContract$.pipe(
      switchMap(infoResult => {
        // 暫存驗身資料 Start
        // console.log('暫存驗身資料');
        // console.log(infoResult);
        if (infoResult.status === 0) {
          // @save to sessionStorge
          const main = this.userDataService.contractMain;
          main.branchId = formVal.branchId;
          main.payMentDay = infoPlContractReq.payMentDay;
          main.payBankOption = infoPlContractReq.payBankOption;
          this.userDataService.contractMain = main;
          return pcode2566$;
        } else {
          return of(null);
        }
        // 暫存驗身資料 End
      }),
      switchMap(pcode2566Result => {
        // PCode2566驗證他行存戶 Start
        if (pcode2566Result) {
          // console.log('他行存戶驗身');
          // console.log(pcode2566Result);
          if (pcode2566Result.status === 0) {
            const r2: PCode2566Resp = pcode2566Result.result;
            if (r2.pcode2566CheckCode === '0') { // 驗身通過
              // @save to sessionStorge 成功才把req寫入
              main.bank = pcode2566Req.bank;
              main.account = pcode2566Req.account;
              main.phone = pcode2566Req.phone;
              this.userDataService.contractMain = main;
              // 驗身通過
              this.routerDirectService.passID = 'Y';
              // console.log('PCode2566驗身成功');
              return edda$;
            } else { // 驗身失敗
              this.verifyCount.authErrCount = r2.authErrCount;
              this.verifyCount.signErrCount = r2.signErrCount;
              this.verifyCount.eddaErrCount = r2.eddaErrCount;
              this.modalFactoryService
                        .modalDefault({
                          message: r2.pcode2566Message
                        });
              return of(null);
            }
          // data.status !== 0 Model訊息：「撥款資訊驗證有誤，需提供有效本人帳戶以利撥款作業」，並提供「返回驗證」按鈕與「專人聯絡」按扭。
          } else {
            this.modalFactoryService.modalDefault({
              message: '撥款資訊驗證有誤，需提供有效本人帳戶以利撥款作業'
            });
            return of(null);
          }
        }
        // PCode2566驗證他行存戶 End
      })
    ).subscribe(eddaResult => {
      // Edda驗證他行存戶 Start
      // console.log('EDDA的回傳');
      // console.log(eddaResult);
      if (eddaResult && eddaResult.status === 0) {
        // @save to sessionStorge 成功才把req寫入
        const main = this.userDataService.contractMain;
        main.eddaBankId = eddaReq.eddaBankId;
        main.eddaAccountNo = eddaReq.eddaAccountNo;
        main.payMentDay = formVal.payMentDay;
        this.userDataService.contractMain = main;
        // console.log('EDDA驗身成功');
        if (eddaResult.message !== '成功') {
          this.modalFactoryService.modalDefault({message: eddaResult.message});
        } else {
          this.routerHelperService.navigate(RouteGo.find(eddaResult.result).navi);
        }
        // data.status !== 0 Model訊息：「EDDA驗證失敗，是否自動產生EACH申請書並寄到您的信箱，此文件需簽名寄回」，並提供「返回驗證」與「寄送EACH申請書」按鈕。
      } else {
        this.modalFactoryService.modalDefault({
          showConfirm: true,
          showClose: false,
          message: 'EDDA驗證失敗，是否自動產生EACH申請書並寄到您的信箱，此文件需簽名寄回',
          btnOK: '寄送EACH申請書',
          btnCancel: '返回驗證',
          btnForEdda: true,
        })
        .result.then(v => {
          if (v === 'ok') {
             this.routerHelperService.navigate(RouteGo.previewPlContract.navi);
          }
        });
      }
      // Edda驗證他行存戶 End
    });
  }

  // 儲存，分行代號、繳款日期
  infoPlContractReq() {
    const formVal = this.dataForm.getRawValue(); // get disabled controls
    let payBankOption: KgiBankInfo|string = formVal.selectedOption;
    let sordBankOption: KgiBankInfo|string = formVal.selectedEddaOption;
    if (typeof payBankOption !== 'string') {
      payBankOption = '0'; // 0:撥款至本行帳戶
    }
    // console.log(sordBankOption);
    if (typeof sordBankOption !== 'string') {
      sordBankOption = '0';
    }
    const req = {
      // branchId: formVal.branchId, // Charles: branchId 由 this.validateService.setPCode2566Sign(req) 寫入
      payMentDay: formVal.payMentDay,
      payBankOption: payBankOption,
      sordBankOption: sordBankOption,
    };
    
    // console.log(req);

    return req;

  } // end payMentDay

  pcode2566Req() {

    const formVal = this.dataForm.getRawValue(); // get disabled controls

    if (formVal.bankId === '809' && formVal.account.length > 14) {
      if (formVal.account.length === 15) {
        formVal.account = formVal.account.substr(1, 14);
      } else if (formVal.account.length === 16) {
        formVal.account = formVal.account.substr(2, 14);
      }
    }

    // console.log('撥款資訊=> phone : ' + formVal.phone + ' ;bank : ' + formVal.bankId + ' ;account : ' + formVal.account);
    let pcode2566Req: PCODE2566Req = {
      phone: '',
      bank: '',
      account: '',
      branchId: '',
    };
    pcode2566Req = {
      phone: formVal.phone,
      bank: formVal.bankId,
      account: formVal.account,
      branchId: formVal.branchId,
    };

    return pcode2566Req;
  }

  eddaReq() {
    const formVal = this.dataForm.getRawValue(); // get disabled controls
    // Edda驗證他行存戶 Start

    // 20201116 調整 如果為本行 帳號取14碼
    if (formVal.eddaBankId === '809' && formVal.eddaAccountNo.length > 14) {
      if (formVal.eddaAccountNo.length === 15) {
        formVal.eddaAccountNo = formVal.eddaAccountNo.substr(1, 14);
      } else if (formVal.eddaAccountNo.length === 16) {
        formVal.eddaAccountNo = formVal.eddaAccountNo.substr(2, 14);
      }
    }

    // console.log('繳款資訊=> eddaBankId : ' + formVal.eddaBankId + ' ;eddaAccountNo : ' + formVal.eddaAccountNo);
    const edda: EddaReq = {
      eddaBankId: formVal.eddaBankId,
      eddaAccountNo: formVal.eddaAccountNo,
    };
    return edda;
  }
}
