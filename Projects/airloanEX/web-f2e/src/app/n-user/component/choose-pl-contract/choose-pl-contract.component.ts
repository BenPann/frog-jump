import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoanService } from 'src/app/service/loan/loan.service';
import { ModalFactoryService } from 'src/app/shared/service/common/modal-factory.service';
import { takeWhile } from 'rxjs/operators';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { RouterDirectService } from 'src/app/service/router-direct/router-direct.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ValidateService } from 'src/app/service/validate/validate.service';
import { ProductService } from 'src/app/service/product/product.service';
import { TermsService } from 'src/app/service/terms/terms.service';
import { EntryService } from 'src/app/service/entry/entry.service';
import { ApplyAccountService } from 'src/app/service/apply-account/apply-account.service';
import { ChooseAccountReq } from 'src/app/service/product/product.interface';
import {InitApiService} from 'src/app/service/init/init-api.service';
import {GetDgtConstResp} from 'src/app/service/airloanex/airloanex.interface';

import { RouteGo } from 'src/app/route-go/route-go';

@Component({
  selector: 'app-choose-pl-contract',
  templateUrl: './choose-pl-contract.component.html',
  styleUrls: ['./choose-pl-contract.component.scss']
})
export class ChoosePlContractComponent implements OnInit, OnDestroy {
  private alive = true;
  // 表單
  form: FormGroup;

  // 上方步驟
  stepStr = 2;

  // 暫存 pcode2566驗證手機號碼
  tempPhone: string;

  getDgtConstResp1: GetDgtConstResp;
  getDgtConstResp3: GetDgtConstResp;
  getDgtConstResp2: GetDgtConstResp;

  constructor(
    private routerHelperService: RouterHelperService,
    private fb: FormBuilder,
    private routerDirectService: RouterDirectService,
    private userDataService: UserDataService,
    private modalFactoryService: ModalFactoryService,
    private initApiService: InitApiService,
    
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      plContractType: ['', Validators.required]
    });

    this.initApiService
      .prepareDgtConst()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          // console.log(data);
          this.getDgtConstResp1 = data.result.getDgtConstResp1;
          this.getDgtConstResp3 = data.result.getDgtConstResp3;
          this.getDgtConstResp2 = data.result.getDgtConstResp2;

          let cnt = 0;
          let dgtConstResp = null;
          if (this.getDgtConstResp1 === null) {
            cnt += 0;
          } else {
            cnt += 1;
            dgtConstResp = '1';
          }
          if (this.getDgtConstResp3 === null) {
            cnt += 0;
          } else {
            cnt += 1;
            dgtConstResp = '3';
          }
          if (this.getDgtConstResp2 === null) {
            cnt += 0;
          } else {
            cnt += 1;
            dgtConstResp = '2';
          }
          // cnt += (this.getDgtConstResp1 === null) ? 0 : 1;
          // cnt += (this.getDgtConstResp3 === null) ? 0 : 1;
          // cnt += (this.getDgtConstResp2 === null) ? 0 : 1;
          if (cnt === 1) {
            const plContractType = this.form.get('plContractType');
            plContractType.setValue(dgtConstResp);
            this.next();
          }
          // justOne |= ;
        }
      });
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

    // 上一步按鈕上鎖及解鎖
    isPrevDisabled(): boolean {
      return false;
    }

    // 下一步按鈕是否上鎖
    isNextDisabled(): boolean {
      return this.form.get('plContractType').invalid;
    }

    // prevStep()
    otherActions(): void {
      this.routerHelperService.navigate(this.routerDirectService.prevPage);
      /* 2020-07-22 Alen 需要刪除
      this.routerHelperService.navigate(RouteGo.identifyByBankAccountPlContract.navi);
      */
    }

    // 下一步
    next(): void {
      const formVal = this.form.getRawValue();
      // const plContractType = this.form.get('plContractType').value;
      const plContractType = formVal.plContractType;
      // 存值
      this.userDataService.plContractType = plContractType;
      const req = {
        contract : plContractType
      };

      this.initApiService.chosenDgtConst(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        // console.log(data);
        if (data.status === 0) {
          // @save to sessionStorage
          const main = this.userDataService.contractMain;
          main.productId = formVal.plContractType; // 'plContractType'

          this.tempPhone = main.phone; // 20201110 debug 調整

          const getDgtConstResp = data.result.getDgtConstResp;
          const initBankInfo = data.result.initBankInfo;
          const rateDatas = data.result.rateDatas;
          main.inputMode = getDgtConstResp.input_mode;
          this.userDataService.doInitBankInfo(main, initBankInfo);
          main.phone = this.tempPhone;
          this.userDataService.contractMain = main;

          switch (formVal.plContractType) {
            case '1': // 個人信貸(分期攤還)
              // this.routerHelperService.navigate(RouteGo.agreePlContract.navi);
              // TODO: content
              this.routerHelperService.navigate(RouteGo.content.navi);
              break;

            case '3': // 個人信貸(隨借隨還)契約書
              // this.routerHelperService.navigate(RouteGo.agreeRplContract.navi);
              // TODO: contentrpl
              this.routerHelperService.navigate(RouteGo.contentrpl.navi);
              break;

            case '2': // 現金卡契約書
              this.routerHelperService.navigate(RouteGo.contentrpl.navi);
              break;
            default:
              break;
          }
        }
      });
    } // end next

    showDialog() {
      const message =
        '如無法完成選擇待簽署契約項目，需至分行完成申辦';
      this.modalFactoryService
        .modalDefault({
          showConfirm: true,
          showClose: false,
          message: message,
          btnCancel: '返回選擇待簽署契約項目',
          btnOK: '需同意下列事項並填寫撥款及繳款資訊'
        })
        .result.then(v => {
          if (v === 'ok') {
              this.routerHelperService.navigate(['n', 'appointment']);
          } else {
            return;
          }
        });
    } // end showDialog
}
