import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoosePlContractComponent } from './choose-pl-contract.component';

describe('ChoosePlContractComponent', () => {
  let component: ChoosePlContractComponent;
  let fixture: ComponentFixture<ChoosePlContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoosePlContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoosePlContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
