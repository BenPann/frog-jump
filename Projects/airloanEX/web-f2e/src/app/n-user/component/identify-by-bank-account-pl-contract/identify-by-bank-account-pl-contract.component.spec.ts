import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentifyByBankAccountPlContractComponent } from './identify-by-bank-account-pl-contract.component';

describe('IdentifyByBankAccountPlContractComponent', () => {
  let component: IdentifyByBankAccountPlContractComponent;
  let fixture: ComponentFixture<IdentifyByBankAccountPlContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdentifyByBankAccountPlContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentifyByBankAccountPlContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
