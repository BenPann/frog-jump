import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ProductService } from 'src/app/service/product/product.service';
import { ModalFactoryService } from 'src/app/shared/service/common/modal-factory.service';
import { takeWhile } from 'rxjs/operators';
import { FormCheckService } from 'src/app/shared/service/common/form-check.service';
import { DropdownItem, TermsInput } from 'src/app/interface/resultType';
import { PCODE2566Res, NCCCRes, VerifyCountResp, PCODE2566Req, PCode2566Resp } from 'src/app/service/validate/validate.interface';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidateService } from 'src/app/service/validate/validate.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { RouterDirectService } from 'src/app/service/router-direct/router-direct.service';
import { TermsService } from 'src/app/service/terms/terms.service';
import { OtpService } from 'src/app/service/otp/otp.service';
import { RouteGo } from 'src/app/route-go/route-go';

@Component({
  selector: 'app-identify-by-bank-account-pl-contract',
  templateUrl: './identify-by-bank-account-pl-contract.component.html',
  styleUrls: ['./identify-by-bank-account-pl-contract.component.scss']
})
export class IdentifyByBankAccountPlContractComponent implements OnInit, OnDestroy {
  private alive = true;
  stepStr = 1;

  // form表單
  dataForm: FormGroup;
  // dropdownData
  pcode2566BankList: DropdownItem[];
  // verifyCount
  verifyCount: VerifyCountResp = {
    authErrCount: 0,
    signErrCount: 0,
    eddaErrCount: 0
  };
  // termList
  termsLink: TermsInput[];
  //
  errorMsgPhone: string;

  // 若用戶未修改存款帳戶 以電文傳來值為主
  origAccount = '';

  constructor(
    private fb: FormBuilder,
    private routerHelperService: RouterHelperService,
    private routerDirectService: RouterDirectService,
    private userDataService: UserDataService,
    private commonDataService: CommonDataService,
    private validateService: ValidateService,
    private modalFactoryService: ModalFactoryService,
    private otpService: OtpService,
    private termsService: TermsService,
    public formCheckService: FormCheckService,
    private productService: ProductService,
  ) {}

  ngOnDestroy() {
    this.alive = false;
  }

  ngOnInit() {
    // prevPage
    this.routerDirectService.prevPage = RouteGo.chooseIdentificationStylePlContract.navi;

    this.initDataForm();

    // 取得銀行別
    this.commonDataService
      .getPcode2566BankList()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        // data = data.filter(
        //   (value: { dataName: string }) => value.dataName !== '809 凱基銀行'
        // );
        // console.log(data);
        this.pcode2566BankList = data;
      });

      // 取得NCCC和PCode2566最大驗身次數 getMaxVerifyCount()
      this.productService
      .getMaxVerifyCount()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.userDataService.maxVerifyCount = data.result;
          // this.validateService
          //   .getVerifyCount()
          //   .pipe(takeWhile(() => this.alive))
          //   .subscribe(data2 => {
          //     if (data2.status === 0) {
          //       this.userDataService.verifyCount = data2.result;
          //     } else {
          //       this.modalFactoryService.modalDefault({
          //         message: data2.message
          //       });
          //     }
          //   });
        } else {
          this.modalFactoryService.modalDefault({ message: data.message });
        }
      });

      // 取得驗身總次數 getVerifyCount()
      this.validateService
      .getVerifyCount()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) { // 取得
          this.verifyCount = data.result;
          // 超過最大驗身次數maxVerifyCount
          if (
            this.verifyCount.authErrCount >=
            Number(this.userDataService.maxVerifyCount)
          ) {
            this.routerHelperService.navigate(this.routerDirectService.prevPage);
          }
        } else { // 取得失敗
          this.modalFactoryService.modalDefault({ message: data.message });
        }
      });

    this.termsService.getTermList('pcode2566').subscribe(data => {
      if (data) {
        this.termsLink = data.filter(value => value.type === '1');
      }
    });

    // this.isBankIdDisabled();
    // this.isAccountDisabled();
    // this.isPhoneDisabled();

    // 是否有預載的資料 initBankNo, initAccount, initPhone
    const main = this.userDataService.contractMain;
    if (main) {
      if (main.initBankNo) {
        this.dataForm.get('bankId').setValue(main.initBankNo.substring(0, 3));
      } else if (main.bank) {
        this.dataForm.get('bankId').setValue(main.bank.substring(0, 3));
      }
      if (main.initAccount) {
        this.origAccount = main.initAccount;
        this.dataForm.get('account').setValue(main.initAccount.substring(0, 6) + '****' + main.initAccount.substring(10, 14));
      } else if (main.account) {
        this.origAccount = main.account;
        this.dataForm.get('account').setValue(main.account.substring(0, 6) + '****' + main.account.substring(10, 14));
      }
      // 109.10.27 調整不需要帶入電話號碼
      // if (main.initPhone) {
      //   this.dataForm.get('phone').setValue(main.initPhone);
      // } else if (main.phone) {
      //   this.dataForm.get('phone').setValue(main.phone);
      // }
    }
  } // end ngOnInit

  initDataForm() {
    this.dataForm = this.fb.group({
      bankId: ['', [Validators.required]],
      account: [
        '',
        [
          Validators.required,
          Validators.minLength(9),
          Validators.pattern(/^[0-9, .*]{10,}$/)
        ]
      ],
      phone: [
        '',
        [
          Validators.required,
          Validators.minLength(10),
          Validators.pattern(/^[0]{1}[9]{1}[0-9]{8}$/)
        ]
      ]
    });
  } // end initDataForm

  isBankIdDisabled(): void {
    const control = this.dataForm.get('bankId');
    this.userDataService.contractMain.initBankNo
    ? control.disable()
    : control.enable();
  }

  isAccountDisabled(): void {
    const control = this.dataForm.get('account');
    this.userDataService.contractMain.initAccount
    ? control.disable()
    : control.enable();
  }

  isPhoneDisabled(): void {
    const control = this.dataForm.get('phone');
    this.userDataService.contractMain.initPhone
    ? control.disable()
    : control.enable();
  }

  // 下一步按鈕是否上鎖
  isNextDisabled(): boolean {
    return !this.dataForm.valid;
  }

  // showTerms(termsName: string) {
  //   return 'ok';
  // }

  // 當存款帳號值已改變 改用用戶輸入之存款帳號為主
  changed() {
    this.origAccount = '';
  }

  showTerms(termsName: string) {
    this.termsService
      .getTerms(termsName)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        let termText = '';
        try {
          // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
          const json = JSON.parse(data);
          termText = '查詢條款失敗';
        } catch (error) {
          termText = data;
        }
        this.modalFactoryService.modalHtml({ message: termText });
      });
  }

  clearErrorMsg(): void {
    this.errorMsgPhone = '電話格式錯誤';
  }

  validatePhone(): void {
    /*this.otpService
      .isPhoneSuccess(this.dataForm.get('phone').value)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data2 => {
        if (data2.status === 0 && data2.result === 'Y') {
        } else if (data2.status === 0 && data2.result === 'N') {
          this.errorMsgPhone = '手機號碼' + this.dataForm.get('phone').value + '已有其他客戶申請使用，請確認您的手機號碼是否正確或洽各分行辦理開戶，謝謝您！';
          this.dataForm.get('phone').setErrors({ incorrect: true });
        } else {
          this.errorMsgPhone = data2.message;
          this.dataForm.get('phone').setErrors({ incorrect: true });
        }
      });*/
  }

  backStep() {
    this.routerHelperService.navigate(this.routerDirectService.prevPage);
  }

  nextStep(): void {
    /*this.otpService
      .isPhoneSuccess(this.dataForm.get('phone').value)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data2 => {
        if (data2.status === 0 && data2.result === 'Y') {
          // 如果使用者沒有填寫正確的資料 就跳出詢問是否要跳過的訊息
          if (this.dataForm.invalid) {
            const message = '您確定要跳過帳戶驗證';
            this.modalFactoryService
              .modalDefault({
                showConfirm: true,
                message: message,
                btnCancel: '繼續驗證',
                btnOK: '跳過驗證'
              })
              .result.then(v => {
                if (v === 'ok') {
                  this.routerHelperService.navigate(this.routerDirectService.prevPage);
                }
              });
            return;
          }
          const phoneValue = this.dataForm.get('phone').value;
          const formData = this.dataForm.value;
          const req: PCODE2566Req = {
            phone: phoneValue,
            bank: formData.bankId,
            account: formData.account
          };
          // console.log('phoneValue=' + phoneValue);
          // 他行存戶驗身
          this.validateService
            .setPCode2566(req)
            .pipe(takeWhile(() => this.alive))
            .subscribe(data => {
              if (data.status === 0) {
                const r2: PCODE2566Res = data.result;
                if (r2.pcode2566CheckCode === '0') {
                  // @save to sessionStorage 成功才把req寫入
                  this.userDataService.verifiedAccount = req;
                  // 通過驗身
                  this.routerDirectService.passID = 'Y';
                  // 本次已進行過OTP驗證的動作，不再進行OTP驗證 FIXME: Charles
                  if (this.routerDirectService.passOTP === 'Y') {
                    if (
                      this.userDataService.newUserChooseProductReq
                        .creditType === '2'
                    ) {
                      this.routerHelperService.navigate(['n', 'chooseCard']);
                    } else {
                      this.routerHelperService.navigate(['n', 'uploadDocument']);
                    }
                  } else {
                    this.routerHelperService.navigate(RouteGo.otpPlContract.navi);
                  }
                } else {
                  this.verifyCount.authErrCount = r2.errorCount;
                  this.verifyCount.signErrCount = r2.ncccErrorCount;
                  this.verifyCount.eddaErrCount = r2.pcode2566ErrorCount;
                  this.modalFactoryService
                    .modalDefault({
                      message: r2.pcode2566Message
                    })
                    .result.then(() => {
                      if (
                        this.verifyCount.authErrCount >=
                        Number(this.userDataService.maxVerifyCount)
                      ) {
                        // 次數已滿 看要去哪
                        this.modalFactoryService
                          .modalDefault({
                            message: '次數已滿，請以預約方式辦理'
                          })
                          .result.then(v => {
                            this.routerHelperService.navigate(['n', 'appointment']);
                          });
                      } else {
                        // // 未滿6次
                        // this.modalFactoryService
                        //   .modalDefault({
                        //     message: '驗證未通過，請重新驗證或選擇其他驗證方式'
                        //   })
                        //   .result.then(v => {
                        //     this.routerHelperService.navigate([
                        //       'n',
                        //       'identifyByBankAccount'
                        //     ]);
                        //   });
                      }
                    });
                }
              } else {
                this.modalFactoryService.modalDefault({
                  message: data.message
                });
              }
            });
        } else if (data2.status === 0 && data2.result === 'N') {
          // this.modalFactoryService.modalDefault({
          //   message: this.dataForm.get('phone').value + '不可以申請'
          // });
          this.errorMsgPhone = this.dataForm.get('phone').value + '不可以申請';
          this.dataForm.get('phone').setErrors({ incorrect: true });
          return;
        } else {
          // this.modalFactoryService.modalDefault({ message: data2.message });
          this.dataForm.get('phone').setErrors({ incorrect: true });
          return;
        }
      });*/
      // let req: PCODE2566Req = {
      //   phone: '',
      //   bank: '',
      //   account: '',
      // };
         // const phoneValue = this.dataForm.get('phone').value;
         // const formData = this.dataForm.value;

         const formVal = this.dataForm.getRawValue(); // get disabled controls

         if (this.origAccount === '' && formVal.account.includes('*')) {
             this.modalFactoryService
             .modalDefault({
               message: '存款帳號含有*字號'
             });
         } else {
          const req: PCODE2566Req = {
            phone: formVal.phone,
            bank: formVal.bankId,
            account: this.origAccount === '' && !formVal.account.includes('*') ? formVal.account : this.origAccount,
            branchId: '',
          };

           // console.log('phoneValue=' + phoneValue);
           // 他行存戶驗身
           this.validateService
             .setPCode2566Auth(req)
             .pipe(takeWhile(() => this.alive))
             .subscribe(data => {
               // console.log('他行存戶驗身');
               // console.log(data);
               if (data.status === 0) {
                 const r2: PCode2566Resp = data.result;
                 if (r2.pcode2566CheckCode === '0') { // 驗身通過
                   // @save to sessionStorage 成功才把req寫入
                   const main = this.userDataService.contractMain;
                   main.bank = req.bank;
                   main.account = req.account;
                   main.phone = req.phone;
                   this.userDataService.contractMain = main;
                   // 通過驗身
                   this.routerDirectService.passID = 'Y';
                   // 本次已進行過OTP驗證的動作，不再進行OTP驗證
                   if (this.routerDirectService.passOTP === 'Y') {
                     this.routerDirectService.prevPage    = RouteGo.identifyByBankAccountPlContract.navi;
                     this.routerHelperService.navigate(RouteGo.choosePlContract.navi);
                   } else {
                     this.routerDirectService.otpPrevPage = RouteGo.identifyByBankAccountPlContract.navi;
                     this.routerDirectService.otpNextPage = RouteGo.choosePlContract.navi;
                     this.routerDirectService.otpPhoneNum = req.phone;
                     this.routerDirectService.prevPage    = RouteGo.identifyByBankAccountPlContract.navi;
                     this.routerHelperService.navigate(RouteGo.otpPlContract.navi);
                   }
                 } else { // 驗身失敗
                   this.verifyCount.authErrCount = r2.authErrCount;
                   this.verifyCount.signErrCount = r2.signErrCount;
                   this.verifyCount.eddaErrCount = r2.eddaErrCount;
                   this.modalFactoryService
                     .modalDefault({
                       message: r2.pcode2566Message
                     })
                     .result.then(() => {
                       if (
                         this.verifyCount.authErrCount >=
                         Number(this.userDataService.maxVerifyCount)
                       ) {
                         // 次數已滿 看要去哪
                         this.modalFactoryService
                           .modalDefault({
                             message: '次數已滿，請以預約方式辦理'
                           });
                           // .result.then(v => {
                           //   this.routerHelperService.navigate(['n', 'appointment']);
                           // });
                       } else {
                         // 未滿6次
                         this.modalFactoryService
                           .modalDefault({
                             message: '驗證未通過，請重新驗證或選擇其他驗證方式'
                           })
                           .result.then(v => {
                             this.routerHelperService.navigate(
                               RouteGo.identifyByBankAccountPlContract.navi);
                           });
                       }
                     });
                 }
               } else {
                 this.modalFactoryService.modalDefault({
                   message: data.message
                 });
               }
             });
         }
  } // end nextStep
}
