import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbDateParserFormatter, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {takeWhile} from 'rxjs/operators';
// service
import {RouterHelperService} from '../../../shared/service/common/router-helper.service';
import {ModalFactoryService} from '../../../shared/service/common/modal-factory.service';
import {TermsService} from '../../../service/terms/terms.service';

import {FormCheckService} from '../../../shared/service/common/form-check.service';
// import {UserDataService} from 'src/app/service/user-data/user-data.service';
import { RouteGo } from 'src/app/route-go/route-go';
import {AuthInfo} from 'src/app/service/apply-account/apply-account.interface';
import {AirloanexService} from '../../../service/airloanex/airloanex.service';

import {InitApiService} from 'src/app/service/init/init-api.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';

@Component({
  selector: 'app-agree-rpl-contract',
  templateUrl: './agree-rpl-contract.component.html',
  styleUrls: ['./agree-rpl-contract.component.scss']
})
export class AgreeRplContractComponent implements OnInit, OnDestroy {
  private alive = true;
  stepStr = 2;
  pdfUrl: string;

  // form表單
  dataForm: FormGroup;
  // 同意聲明
  showAuthInfo = true;
  authInfo: AuthInfo;
  authInfoName: string;

  // 審閱期設定
  minDate: NgbDateStruct;
  maxDate: NgbDateStruct;
  today = new Date();
  todayMinus5d = new Date();

  resultPayDate: string;

  // initData = false;
  payDayOptions: number[] = Array.from(new Array(31), (x, i) => i + 1); // payDayOptions=[1,...31]

  constructor(
    private routerHelperService: RouterHelperService,
    private fb: FormBuilder,
    private termsService: TermsService,
    private modalFactoryService: ModalFactoryService,
    public formCheckService: FormCheckService,
    private dateFormatter: NgbDateParserFormatter,
    // private userDataService: UserDataService,
    private airloanexService: AirloanexService,
    private initApiService: InitApiService,
    private userDataService: UserDataService,
    ) { }

  ngOnDestroy() {
  }

  ngOnInit() {

    // init 審閱期設定
    this.initdatepicker();

    // 取得先前填寫的資料
    this.initDataForm();

    // 20201120 取撥款日
    this.initApiService.judgeExceedPayDate()
    .pipe(takeWhile(() => this.alive))
    .subscribe(resultData => {
      if (resultData.status === 0) {
            // 取撥款日 => 預設繳款日
            this.resultPayDate = resultData.result;
            //console.log('預設繳款日=' + this.resultPayDate);
            // this.isEmailDisabled();
            // 是否有預載的資料 initEmail
            const main = this.userDataService.contractMain;
            if (main) {
              if (main.initEmail) {
                this.dataForm.get('email').setValue(main.initEmail);
              }
              this.isPayMentDayDisabled();
              if (main.initPayKindDay) {
                this.dataForm.get('payMentDay').setValue(main.initPayKindDay);
              } else if (this.resultPayDate) {
                this.dataForm.get('payMentDay').setValue(this.resultPayDate);
              } else if (main.payMentDay) {
                this.dataForm.get('payMentDay').setValue(main.payMentDay);
              }
            }
       } else {
         this.userDataService.warnMsg = resultData.message;
         this.routerHelperService.navigate(RouteGo.initPlContract.navi);
       }
    });
    
    // 共同行銷客戶個人資料使用同意聲明
    this.termsService
    .getTermList('agreePlContract')
    .pipe(takeWhile(() => this.alive))
    .subscribe(r => {
      this.authInfoName = r.find(value => value.type === '1').termName;
      this.airloanexService
        .getAutInfo(this.authInfoName)
        .pipe(takeWhile(() => this.alive))
        .subscribe(data => {
          // console.log(data);
          if (data.status === 0) {
            this.authInfo = data.result;
            if (this.authInfo && this.authInfo.authorizeToallCorp === 'Y') {
              this.showAuthInfo = false;
              this.authInfo.authorizeToallCorp = '';
              this.authInfo.verNo = '';
            }
          }
        });
    });

    // 個人信貸(分期攤還)契約書
    this.agreeDownloadPdf();

  } // end ngOnInit

  initDataForm() {
    // let basicData = this.userDataService.basicFillDataReq;
    this.dataForm = this.fb.group({
      email: [
        '', // FIXME: Charles: basicData.emailAddress,
        [Validators.required,
         Validators.email,
          //  this.formCheckService.checkRepeatEmail(
          //  this.userDataService.usedPhoneAndEmail.emailAddress)
        ]
      ],
      payMentDay: ['', []], // init 繳款日期設定
      reviewDate: [this.maxDate, [Validators.required]],
      agreeReview: [true, [Validators.requiredTrue]], // FIXME: Charles: basicData.authorizeToallCorp === 'Y' ? 'Y' : ''
      authInfo: true // FIXME: Charles: basicData.authorizeToallCorp === 'Y' ? 'Y' : '',
    });
  }

  isPayMentDayDisabled(): void {
    const control = this.dataForm.get('payMentDay');
    const main = this.userDataService.contractMain;
    // console.log('main.initPayKindDay=' + main.initPayKindDay);
    main.inputMode === 'N' // && main.initPayKindDay
    ? control.disable()
    : control.enable();
  }

  isEmailDisabled(): void {
    const control = this.dataForm.get('email');
    this.userDataService.contractMain.initEmail
    ? control.disable()
    : control.enable();
  }

  // init 審閱期設定
  initdatepicker(): void {
    this.todayMinus5d.setDate(this.todayMinus5d.getDate() - 5); // today - 5d
    const y = this.todayMinus5d.getFullYear();
    const m = this.todayMinus5d.getMonth() + 1;
    const d = this.todayMinus5d.getDate();
    this.minDate = {
      year: y - 1,
      month: m,
      day: d
    };
    this.maxDate = {
      year: y,
      month: m,
      day: d
    };
  }

  // 個人信貸(分期攤還)契約書
  agreeDownloadPdf(): void {
    this.initApiService
    .agreeDownloadPdf('3')
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      // console.log(data);
      this.pdfUrl = data.result;
    });
  }

  showTerms() {
    this.termsService
      .getTerms(this.authInfoName)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        let termText = '';
        try {
          // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
          const json = JSON.parse(data);
          termText = '查詢條款失敗';
        } catch (error) {
          termText = data;
        }
        this.modalFactoryService.modalHtml({ message: termText });
      });
  }

  // 下一步按鈕是否上鎖
  isNextDisabled(): boolean {
    return !this.dataForm.valid;
  }

  // 上一步
  backStep() {
    this.routerHelperService.navigate(RouteGo.choosePlContract.navi);
  }

  // 下一步
  nextStep(): void {
    const formVal = this.dataForm.getRawValue();
    const reviewDate: NgbDateStruct = formVal.reviewDate;
    const req = {
      email: formVal.email,
      reviewDate: this.dateFormatter.format(reviewDate).split('-').join(''),
      authInfo: formVal.authInfo,
      payMentDay: formVal.payMentDay,
    };

    this.initApiService.agreePlContract(req)
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      if (data.status === 0) {
        // @save to sessionStorage
        const main = this.userDataService.contractMain;
        main.email = formVal.email;
        main.readDate = formVal.reviewDate;
        main.authInfo = formVal.authInfo;
        main.payDate = this.resultPayDate; // 計算後的繳款日
        main.payMentDay = formVal.payMentDay; // 自訂繳款日
        this.userDataService.contractMain = main;
        this.routerHelperService.navigate(RouteGo.previewPlContract.navi);
       }
    });
  }
}
