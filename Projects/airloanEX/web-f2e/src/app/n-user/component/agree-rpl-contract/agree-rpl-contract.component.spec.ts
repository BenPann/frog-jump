import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgreeRplContractComponent } from './agree-rpl-contract.component';

describe('AgreeRplContractComponent', () => {
  let component: AgreeRplContractComponent;
  let fixture: ComponentFixture<AgreeRplContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgreeRplContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreeRplContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
