import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgreeCashcardContractComponent } from './agree-cashcard-contract.component';

describe('AgreeCashcardContractComponent', () => {
  let component: AgreeCashcardContractComponent;
  let fixture: ComponentFixture<AgreeCashcardContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgreeCashcardContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreeCashcardContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
