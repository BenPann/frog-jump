import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseIdentificationStylePlContractComponent } from './choose-identification-style-pl-contract.component';

describe('ChooseIdentificationStylePlContractComponent', () => {
  let component: ChooseIdentificationStylePlContractComponent;
  let fixture: ComponentFixture<ChooseIdentificationStylePlContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseIdentificationStylePlContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseIdentificationStylePlContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
