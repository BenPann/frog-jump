import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
// service
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { RouterDirectService } from 'src/app/service/router-direct/router-direct.service';
import { ModalFactoryService } from 'src/app/shared/service/common/modal-factory.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { TermsService } from 'src/app/service/terms/terms.service';

import { RouteGo } from '../../../route-go/route-go';

@Component({
  selector: 'app-choose-identification-style-pl-contract',
  templateUrl: './choose-identification-style-pl-contract.component.html',
  styleUrls: ['./choose-identification-style-pl-contract.component.scss']
})
export class ChooseIdentificationStylePlContractComponent implements OnInit, OnDestroy {
  private alive = true;
  stepStr = 1;

  // 表單
  form: FormGroup;
  // funcDesc
  bankDesc: any;
  creditCardDesc: any;
  offlineDesc: any;
  nextTimeDesc: any;

  constructor(
    private fb: FormBuilder,
    private routerHelperService: RouterHelperService,
    private routerDirectService: RouterDirectService,
    private userDataService: UserDataService,
    private termsService: TermsService,
    private modalFactoryService: ModalFactoryService
  ) {}

  ngOnDestroy(): void {
    this.alive = false;
  }

  ngOnInit() {
    // 表單init
    this.form = this.fb.group({
      applyType: ['', Validators.required]
    });

    // @access
    const main = this.userDataService.contractMain;
    if (main.productId) {
      this.form.get('applyType').setValue(main.productId);
    }

    this.initTerms();
  } // end ngOnInit

  initTerms() {
    // FIXME: Charles: We should develope a funcDesc for STP.
    /*this.termsService
      .getFuncDesc()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.bankDesc = data.result.ed3VerifyBankAcct;
        this.creditCardDesc = data.result.ed3VerifyCreditCard;
        this.offlineDesc = data.result.ed3VerifyOffline;
        this.nextTimeDesc = data.result.ed3VerifyNextTime;
      });*/
  }

  // 下一步按鈕是否上鎖
  isNextDisabled(): boolean {
    return this.form.get('applyType').invalid;
  }

  // 專人與我聯絡
  otherActions(): void {
    // TODO: Charles: Ask Ben for details of appointment.
    this.routerHelperService.navigate(RouteGo.initPlContract.navi);
  }

  // 下一步
  next(): void {
    //
    const formVal = this.form.getRawValue();
    switch (formVal.applyType) {
      case '1':
        // this.routerDirectService.prevPage = RouteGo.chooseIdentificationStylePlContract.navi;
        this.routerHelperService.navigate(RouteGo.identifyByBankAccountPlContract.navi);
        break;
      default:
        break;
    }
  }
  // Charles: The code may come handy in the near future.
/*
  showDialog() {
    const message =
      '如無法完成線上驗證，需至分行完成申辦，且無法設定扣繳中國人壽續期保費';
    this.modalFactoryService
      .modalDefault({
        showConfirm: true,
        showClose: false,
        message: message,
        btnCancel: '返回驗證',
        btnOK: '選擇分行'
      })
      .result.then(v => {
        if (v === 'ok') {
            this.routerHelperService.navigate(['n', 'appointment']);
        } else {
          return;
        }
      });
  }
*/
}
