import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { RouteGo } from 'src/app/route-go/route-go';
import { ContractMain, RateData } from 'src/app/service/airloanex/airloanex.interface';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { CalcIRRQuick } from 'src/app/js/calcIRR.js';
import {Router} from "@angular/router";

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  main: ContractMain;
  tbA_aprove_amount: string;
  aprove_period: number;
  apy_fee: number;
  b_range: number;
  rateDatas = [];
  IRR: number; // 總費用年百分率
  inPeriod:number; // 判斷利率計算模式 用於畫面顯示
  sum_rate:number // 總費用年百分率 用於畫面顯示
  constructor(
    private userDataService: UserDataService,
    private routerHelperService: RouterHelperService,
    private fb: FormBuilder,
    private dateFormatter: NgbDateParserFormatter,
    private router: Router,
  ) { }

  ngOnInit() {
    this.main = this.userDataService.contractMain;
    this.tbA_aprove_amount = this.main.tbA_aprove_amount.replace('0000.0', '萬');
    this.aprove_period = +this.main.aprove_period;
    this.apy_fee = +this.main.apy_fee;
    this.b_range = +this.main.b_range;

    let inLoadAmt = +this.main.tbA_aprove_amount/10000; // 萬元
    let inLoanYear = +this.main.aprove_period;
    let inPeriod_1_Month = 12;
    let inPeriod_1_YRate = 0;
    let inPeriod_2_YRate = null;
    let inOtherFee = +this.main.apy_fee;
    let inPeriod_1_start_Month = 1;
    let inPeriod_2_start_Month = 13;
    for (let i = 0; i < this.main.rateDatas.length; i++) {
      const rateData = this.main.rateDatas[i];
      const r = {
        pay_rate_date: '',
        pay_rate_m: 1,
        pay_rate: 0,
        month_pay: 0
      };
      r.pay_rate_date = `${rateData.pay_rate_date.slice(0,4)}-${rateData.pay_rate_date.slice(4,6)}-${rateData.pay_rate_date.slice(6,8)}`;
      r.pay_rate_date = r.pay_rate_date.split('-').join('/');
      r.pay_rate_m = +rateData.pay_rate_m;
      r.pay_rate = +rateData.pay_rate;

      if (i === 0) {
        inPeriod_1_YRate = r.pay_rate;
        inPeriod_1_start_Month = r.pay_rate_m; // 第一期起月
      } else if (i === 1) {
        inPeriod_2_YRate = r.pay_rate;
        inPeriod_2_start_Month = r.pay_rate_m; // 第二期起月
      }
      if (inPeriod_2_start_Month) {
        inPeriod_1_Month = inPeriod_2_start_Month - inPeriod_1_start_Month;
      } else {
        inPeriod_1_Month = this.aprove_period;
      }
      this.rateDatas.push(r);
    }
    // console.log('inLoadAmt 貸款額度(單位:萬)', inLoadAmt);
    // console.log('inLoanYear 貸款年期', inLoanYear);
    // console.log('inPeriod_1_Month 第一期期數(單位:月)', inPeriod_1_Month);
    // console.log('inPeriod_1_YRate 第一期利率(單位:%)', inPeriod_1_YRate);
    // console.log('inPeriod_2_YRate 第二期利率(單位:%)', inPeriod_2_YRate);
    // console.log('inOtherFee 其他費用(單位:元)', inOtherFee);
    this.inPeriod = inPeriod_2_YRate; // 判斷利率計算模式 用於畫面顯示
    const quick = CalcIRRQuick(inLoadAmt, inLoanYear, inPeriod_1_Month, inPeriod_1_YRate, inPeriod_2_YRate, inOtherFee);
    if (this.rateDatas.length >= 1) {
      this.rateDatas[0].month_pay = quick.Period1MonthlyPay;
    }
    if (this.rateDatas.length >= 2) {
      this.rateDatas[1].month_pay = quick.Period2MonthlyPay;
    }
    if (this.rateDatas.length >= 3) {
      this.rateDatas[2].month_pay = quick.Period3MonthlyPay;
    }
    this.IRR = quick.IRR;
    this.sum_rate = JSON.parse(sessionStorage.getItem('contractMain'))["sum_rate"]; // 總費用年百分率 用於畫面顯示
  }

  // 下一步
  next(): void {
    //routeUrlName 用於線上立約 完成頁面 Title顯示判斷為額度型貸款契約書/分期型貸款契約書
    sessionStorage.setItem('routeUrlName', this.router.url);
    this.routerHelperService.navigate(RouteGo.agreePlContract.navi);
  }
}
