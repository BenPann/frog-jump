import { Component, OnInit, OnDestroy } from '@angular/core';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { RouteGo } from '../../../route-go/route-go';

@Component({
  selector: 'app-all-failure-pl-contract',
  templateUrl: './all-failure-pl-contract.component.html',
  styleUrls: ['./all-failure-pl-contract.component.scss']
})
export class AllFailurePlContractComponent implements OnInit, OnDestroy {
  private alive = true;
  stepStr: number;

  title = '線上簽約失敗，稍後再試！';

  constructor(
    private routerHelperService: RouterHelperService,
  ) { }

  ngOnDestroy(): void {
    this.alive = false;
  } // end ngOnDestroy

  ngOnInit() {
    this.stepStr = 3;
  } // end ngOnInit

  initPlContract() {
    this.routerHelperService.navigate(RouteGo.initPlContract.navi);
  }
}
