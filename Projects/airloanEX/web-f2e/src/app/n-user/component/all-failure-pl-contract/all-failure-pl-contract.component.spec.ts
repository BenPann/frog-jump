import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllFailurePlContractComponent } from './all-failure-pl-contract.component';

describe('AllFailurePlContractComponent', () => {
  let component: AllFailurePlContractComponent;
  let fixture: ComponentFixture<AllFailurePlContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllFailurePlContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllFailurePlContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
