import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {takeWhile} from 'rxjs/operators';

import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { RouteGo } from 'src/app/route-go/route-go';
import { RouterDirectService } from 'src/app/service/router-direct/router-direct.service';
import {InitApiService} from 'src/app/service/init/init-api.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ModalFactoryService } from 'src/app/shared/service/common/modal-factory.service';

@Component({
  selector: 'app-preview-pl-contract',
  templateUrl: './preview-pl-contract.component.html',
  styleUrls: ['./preview-pl-contract.component.scss']
})
export class PreviewPlContractComponent implements OnInit, OnDestroy {
  private alive = true;
  stepStr = 2;
  form: FormGroup;
  dataForm: FormGroup;
  previewHtml = '';
  // 用於線上立約 完成頁面 Title顯示判斷為額度型貸款契約書/分期型貸款契約書
  currentUrl = '';
  productId: string;

  constructor(
    private fb: FormBuilder,
    private routerHelperService: RouterHelperService,
    private initApiService: InitApiService,
    private routerDirectService: RouterDirectService,
    private userDataService: UserDataService,
    private modalFactoryService: ModalFactoryService,
  ) {
  }

  ngOnDestroy() {
    this.alive = false;
  }

  ngOnInit() {
    this.getProductId();
    this.currentUrl = sessionStorage.getItem('routeUrlName');
    // 預覽契約html
    this.initApiService.previewContract()
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      // console.log(data);
      if (data) {
        this.previewHtml = data.result;
      }
    });

    this.dataForm = this.fb.group({

    });
  }

  getProductId() {
    const main = this.userDataService.contractMain;
    if (main && main.productId) {
      this.productId = main.productId;
    } else {
      this.modalFactoryService.modalDefault({
        message: '系統問題，請洽服務人員，謝謝'
      });
    }
  }

  isNextDisabled(): boolean {
    return !this.dataForm.valid;
  }

  backStep() {
    switch (this.productId) {
      case '1': // 個人信貸(分期攤還)
        this.routerHelperService.navigate(RouteGo.infoPlContract.navi);
        break;
      case '3': // 個人信貸(隨借隨還)契約書
        this.routerHelperService.navigate(RouteGo.agreeRplContract.navi);
        break;
      case '2': // 現金卡契約書
        this.routerHelperService.navigate(RouteGo.agreeCashcardContract.navi);
        break;
      default:
        break;
    }
  }

  nextStep() {

    this.initApiService.uploadContract()
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      // ignore;
    });

    this.routerDirectService.prevPage = RouteGo.previewPlContract.navi;
    this.routerDirectService.otpPrevPage = RouteGo.previewPlContract.navi;
    // this.routerDirectService.otpNextPage = RouteGo.updateDgtContract.navi;
    this.routerDirectService.otpNextPage = RouteGo.updateDgtContract.navi;
    this.routerDirectService.otpPhoneNum = this.userDataService.contractMain.phone;
    this.routerHelperService.navigate(RouteGo.otpPlContract.navi);
  }
}
