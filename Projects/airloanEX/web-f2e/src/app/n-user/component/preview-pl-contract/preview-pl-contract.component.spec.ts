import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewPlContractComponent } from './preview-pl-contract.component';

describe('PreviewPlContractComponent', () => {
  let component: PreviewPlContractComponent;
  let fixture: ComponentFixture<PreviewPlContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewPlContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewPlContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
