import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllFinishPlContractComponent } from './all-finish-pl-contract.component';

describe('AllFinishPlContractComponent', () => {
  let component: AllFinishPlContractComponent;
  let fixture: ComponentFixture<AllFinishPlContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllFinishPlContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllFinishPlContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
