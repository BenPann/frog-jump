import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { takeWhile } from 'rxjs/operators';
import { InitApiService } from 'src/app/service/init/init-api.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { ModalFactoryService } from '../../../shared/service/common/modal-factory.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import {ContractMain, ContractMainModel} from '../../../service/airloanex/airloanex.interface';
import { RouteGo } from '../../../route-go/route-go';
import {CommonDataService} from "../../../shared/service/common/common-data.service";
import {DropdownItem} from "../../../interface/resultType";


@Component({
  selector: 'app-all-finish-pl-contract',
  templateUrl: './all-finish-pl-contract.component.html',
  styleUrls: ['./all-finish-pl-contract.component.scss']
})
export class AllFinishPlContractComponent implements OnInit, OnDestroy {
  private alive = true;
  stepStr: number;
  showGreenCheck = true;

  title = '恭喜您，完成簽約！';

  showNext = false;

  // 目的是為了重新選擇其他立約 contractmain不會被清除掉
  otpPhoneNum = '';

  // 為了判別PL還是RPL
  currentUrl = '';
  // 為了顯示銀行撥款及繳款帳號
  bankAcc:any;
  sordAcc:any;
  bankName:any;
  sordBankName:any;
  payMentDay:any;
  bankList:DropdownItem[];

  constructor(
    private routerHelperService: RouterHelperService,
    private activatedRoute: ActivatedRoute,
    private modalService: ModalFactoryService,
    private userDataService: UserDataService,
    private initApiService: InitApiService,
    private commonDataService: CommonDataService,
  ) { }

  ngOnDestroy(): void {
    this.alive = false;
  } // end ngOnDestroy

  ngOnInit() {
    this.stepStr = 3;
    //依據用戶實際內容顯示撥款與繳款帳戶
    this.currentUrl = sessionStorage.getItem('routeUrlName');
    this.bankAcc = JSON.parse(sessionStorage.getItem('contractMain'))["initAccount"];
    this.sordAcc = JSON.parse(sessionStorage.getItem('contractMain'))["initSordAccNo"];
    this.bankName = JSON.parse(sessionStorage.getItem('contractMain'))["initBankNo"].substring(0,3);
    this.sordBankName = JSON.parse(sessionStorage.getItem('contractMain'))["initSordBankNo"].substring(0,3);
    this.commonDataService
        .getPcode2566BankList()
        .pipe(takeWhile(() => this.alive))
        .subscribe(data => {
          this.bankList = data;
          try {
            this.bankName = this.bankList.find(value => value.dataKey === this.bankName).dataName.split(" ")[1];
            this.sordBankName = this.bankList.find(value => value.dataKey === this.sordBankName).dataName.split(" ")[1];
          }catch (e){
            this.bankName = this.bankList.find(value => value.dataKey === this.bankName).dataName;
            this.sordBankName = this.bankList.find(value => value.dataKey === this.sordBankName).dataName;
            alert(e.message);
          }

        });

    this.dgtConstResp();
  } // end ngOnInit

  isAllFinish() {
    this.routerHelperService.navigate(RouteGo.initPlContract.navi);
  }

  continueToNext() {
    this.otpPhoneNum = this.userDataService.contractMain.phone;
    const req = this.userDataService.initRqData;

    this.initApiService
      .doLogin(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        // console.log(data);
        if (data.status === 0) {
          // 登入成功之前 把所有資料先清空
          // sessionStorage.clear();
          // @save to sessionStorage
          this.userDataService.token = data.result.token; // 'kgiToken'
          const main: ContractMain = new ContractMainModel();

          const initBankInfo = data.result.initBankInfo;
          const rateDatas = data.result.rateDatas;

          if (initBankInfo) {
            this.userDataService.doInitBankInfo(main, initBankInfo);
            main.phone = this.otpPhoneNum;
            this.userDataService.contractMain = main;
          } // end if
          this.routerHelperService.navigate(RouteGo.choosePlContract.navi);
        } else {
          this.modalService.modalDefault({message: data.message});
        }
      });
  }

  dgtConstResp() {
    this.initApiService
    .prepareDgtConst()
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      this.payMentDay = data.result.paymentDay;
      // console.log(data);
      // 有沒有約
      // 還有約 要顯示按鈕
      // 0 成功 要根據結果顯示畫面
      // 99 失敗打不到api
      if (data.status !== 0) {
          this.showNext = false;
      }else{
        // 當都有立約資料時就不可以繼續申辦

        if(data.result.getDgtConstResp1  === null ||
        data.result.getDgtConstResp2 === null ||
        data.result.getDgtConstResp3 === null){
          this.showNext = true;
        }
      }
    });
  }
}
