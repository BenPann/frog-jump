import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NUserRoutingModule } from './n-user-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

import { ChooseIdentificationStylePlContractComponent } from './component/choose-identification-style-pl-contract/choose-identification-style-pl-contract.component';
import { IdentifyByBankAccountPlContractComponent } from './component/identify-by-bank-account-pl-contract/identify-by-bank-account-pl-contract.component';
import { ChoosePlContractComponent } from './component/choose-pl-contract/choose-pl-contract.component';
import { InfoPlContractComponent } from './component/info-pl-contract/info-pl-contract.component';
import { AgreePlContractComponent } from './component/agree-pl-contract/agree-pl-contract.component';
import { AgreeRplContractComponent } from './component/agree-rpl-contract/agree-rpl-contract.component';
import { AgreeCashcardContractComponent } from './component/agree-cashcard-contract/agree-cashcard-contract.component';
import { AllFinishPlContractComponent } from './component/all-finish-pl-contract/all-finish-pl-contract.component';
import { AllFailurePlContractComponent } from './component/all-failure-pl-contract/all-failure-pl-contract.component';
import { PreviewPlContractComponent } from './component/preview-pl-contract/preview-pl-contract.component';
import { UpdateDgtContractComponent } from './component/update-dgt-contract/update-dgt-contract.component';
import { ContentComponent } from './component/content/content.component';
import { ContentrplComponent } from './component/contentrpl/contentrpl.component';

@NgModule({
  declarations: [
    ChooseIdentificationStylePlContractComponent,
    IdentifyByBankAccountPlContractComponent,
    ChoosePlContractComponent,
    InfoPlContractComponent,
    AgreePlContractComponent,
    AgreeRplContractComponent,
    AgreeCashcardContractComponent,
    AllFinishPlContractComponent,
    AllFailurePlContractComponent,
    PreviewPlContractComponent,
    UpdateDgtContractComponent,
    ContentComponent,
    ContentrplComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NUserRoutingModule,
    SharedModule
  ]
})
export class NUserModule {}
