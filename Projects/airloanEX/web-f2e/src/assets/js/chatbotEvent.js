

function alertTest(){
	
	alert("test");
	
}


function getLocation(href) {
    var match = href.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/);
    return match && {
        href: href,
        protocol: match[1],
        host: match[2],
        hostname: match[3],
        port: match[4],
        pathname: match[5],
        search: match[6],
        hash: match[7]
    }
}


var G_TIME_SEC_FOR_SHOW_CHATBOT_REMIND = 30;	//30s

var G_TIME_MIN_FOR_LOGOFF = 20;	//20min

var timeoutHandle;

var timeoutHandleForLogOff;

function showChatbotRemind(){
	console.log('>>> 未操作已達 ' + G_TIME_SEC_FOR_SHOW_CHATBOT_REMIND + ' 秒, 顯示提示 ');
	var obj = document.getElementById("help-tip-alert");
	obj.style.display = "block";
}
function hideChatbotRemind(){
	console.log('>>> 偵測到點擊畫面, 隱藏提示 ');
	var obj = document.getElementById("help-tip-alert");
	obj.style.display = "none";
}

function resetChatbotRemind(){
	//restart
	var urlData = getLocation(window.location.href);
	
	if (urlData.pathname.indexOf("CAL/fill")!=-1){
		//為額度利率體驗
		G_TIME_SEC_FOR_SHOW_CHATBOT_REMIND=10;
	}else{
		G_TIME_SEC_FOR_SHOW_CHATBOT_REMIND=30;
	}
	
	console.log('>>> 重置時間, 開始倒數: ' + G_TIME_SEC_FOR_SHOW_CHATBOT_REMIND + ' 秒後顯示小幫手提示....');
	window.clearTimeout(timeoutHandle);
	timeoutHandle = setTimeout(showChatbotRemind, G_TIME_SEC_FOR_SHOW_CHATBOT_REMIND * 1000);
}

var G_LOGOFF_START_URL = "";

function redirectToStart(){
	sessiontimeout(); // @see apply-header.component.ts
	var sPath = G_LOGOFF_START_URL;
	console.log('>>> 未操作已達 ' + G_TIME_MIN_FOR_LOGOFF + ' 分鐘, 導回起始頁: ' + G_LOGOFF_START_URL);
	window.location.replace(G_LOGOFF_START_URL);
}

function startWaitToLogOff(){
	//restart
	var urlData = getLocation(window.location.href);
	var sTargetUrl = urlData.host;
	
	if (urlData.pathname.indexOf("apply/start")==-1) {
		if (urlData.pathname.indexOf("CAL/fill")!=-1){
			//為額度利率體驗
			sTargetUrl = "CAL/fill";
		}else{
			sTargetUrl = "apply/start";
		}
		
		G_LOGOFF_START_URL = sTargetUrl;
		
		console.log('>>> 重置時間, 開始倒數: ' + G_TIME_MIN_FOR_LOGOFF + ' 分鐘後登出....(' + G_LOGOFF_START_URL + ')');
		window.clearTimeout(timeoutHandleForLogOff);
		timeoutHandleForLogOff = setTimeout(redirectToStart, G_TIME_MIN_FOR_LOGOFF * 60 * 1000);	
	}
	
}




function addChatbotOpenCnt(){
	let iCnt = window.localStorage.getItem('ChatbotOpenCnt');
	
	iCnt = parseInt(iCnt, 10) + 1;
	window.localStorage.setItem('ChatbotOpenCnt', iCnt);
	
}

function readChatbotOpenCnt(){
	return window.localStorage.getItem('ChatbotOpenCnt');
}




function resetChatbotOpenCnt(){
	
	var urlData = getLocation(window.location.href);
	
	if (urlData.pathname.indexOf("apply/start")!=-1 || urlData.pathname.indexOf("CAL/fill")!=-1){
		//為開始頁
		window.localStorage.removeItem('ChatbotOpenCnt');
		console.log('>>> 首頁重置 Chatbot 開啟 Count');
	}
	
}

document.onclick = function(event) {
    // // Compensate for IE<9's non-standard event model
    // //
    if (event===undefined) {
		console.log("event-1");
		event = window.event;
	}else{
		console.log("event-2");
	}
	var target= 'target' in event? event.target : event.srcElement;
	
	//reset timeout
	hideChatbotRemind();
	resetChatbotRemind();
	startWaitToLogOff();
	
    //console.log('點擊了 '+target.tagName + '(id=' + target.id + ')');
	
}

//first start
resetChatbotRemind();

resetChatbotOpenCnt();

startWaitToLogOff();

