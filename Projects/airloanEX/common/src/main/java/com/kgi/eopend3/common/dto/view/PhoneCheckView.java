package com.kgi.eopend3.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Setter;

import lombok.Getter;

@Getter
@Setter
@CheckNullAndEmpty
public class PhoneCheckView {

    private String phone ;
}