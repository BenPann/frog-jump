package com.kgi.eopend3.common.dto.db;

import java.util.Date;

import lombok.*;

@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
public class QR_ShortUrl {
    
    @NonNull
	private String ShortUrl	;
	private String ChannelId; 
	private String DepartId;
	private String Member;
	private String PProductType;
	private String PProductId;
	private String Entry;
	private String exp = "";
    private Date CreateTime;
	private Date UpdateTime;
	private String PrjCode;
	private String ParentShortUrl;
	private String ParentMember;
 }