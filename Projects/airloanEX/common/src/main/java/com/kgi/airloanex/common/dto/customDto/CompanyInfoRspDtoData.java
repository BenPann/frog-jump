package com.kgi.airloanex.common.dto.customDto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyInfoRspDtoData {
    private String CompanyNameSource;
    private String CompanyName;
    private String UnifiedBusinessNumber;
    private String HeadOfficeId;
    private String RegisterAddressSource;
    List<CompanyInfoRspDtoDataRegisterAddressNormalize> RegisterAddressNormalize = new ArrayList<>();
    private String KgiIndustryType;
    private String KgiCompanyType;
    private String Government;
    private String Medical;
    private String Nursing;
    CompanyInfoRspDtoDataTax Tax;
    private String School;
    CompanyInfoRspDtoDataStock Stock;
    CompanyInfoRspDtoDataYellowPage YellowPage;
}
