package com.kgi.airloanex.common.dto.db;

public class CaseAuth {
    private String uniqId = "";
    /** 唯一代號類型           | 01:體驗 02:申請 03:立約 04:專人聯絡 05:信用卡 */
    private String uniqType = "";
    /** 驗證類型               | 1:OTP 2:MBC 3:證券跨售 4:其它 */
    private String authType = "";
    /** 異業合作ID */
    private String entType = "";
    /** MBC是否通過            | 0:未通過 1:通過 */
    private String mbcValid = "";
    /** MBC驗證的回應代碼 */
    private String mbcCheckCode = "";
    /** MBC驗證的回傳訊息 */
    private String mbcMessage = "";
    /** 是否同意取中華電信資料 | 0:不同意 1:同意 */
    private String chtAgree = "";
    /** 同意中華電信時間 */
    private String chtAgreeTime;
    /** 新增時間 */
    private String createTime = "";

    public CaseAuth(){}

    public CaseAuth(String uniqId){
        this.uniqId = uniqId;
    }

    public String getUniqId() {
        return uniqId;
    }

    public void setUniqId(String uniqId) {
        this.uniqId = uniqId;
    }

    public String getUniqType() {
        return uniqType;
    }

    public void setUniqType(String uniqType) {
        this.uniqType = uniqType;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getEntType() {
        return entType;
    }

    public void setEntType(String entType) {
        this.entType = entType;
    }

    public String getMbcValid() {
        return mbcValid;
    }

    public void setMbcValid(String mbcValid) {
        this.mbcValid = mbcValid;
    }

    public String getMbcCheckCode() {
        return mbcCheckCode;
    }

    public void setMbcCheckCode(String mbcCheckCode) {
        this.mbcCheckCode = mbcCheckCode;
    }

    public String getMbcMessage() {
        return mbcMessage;
    }

    public void setMbcMessage(String mbcMessage) {
        this.mbcMessage = mbcMessage;
    }

    public String getChtAgree() {
        return chtAgree;
    }

    public void setChtAgree(String chtAgree) {
        this.chtAgree = chtAgree;
    }

    public String getChtAgreeTime() {
        return chtAgreeTime;
    }

    public void setChtAgreeTime(String chtAgreeTime) {
        this.chtAgreeTime = chtAgreeTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
