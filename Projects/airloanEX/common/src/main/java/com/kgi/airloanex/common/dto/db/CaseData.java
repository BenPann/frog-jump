package com.kgi.airloanex.common.dto.db;

import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.PlContractConst.CaseDataSubStatus;
import com.kgi.airloanex.common.PlContractConst.CaseDataJobStatus;
import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * 
 * @see PlContractConst.java CASE_STATUS_*
 */
@Getter
@Setter
public class CaseData {
    
    @NonNull
    @CheckNullAndEmpty
    /** 案件編號(數位信貸)	範例: LOANYYYYMMDD000001 */
    private String CaseNo;

    /** 案件編號(APS)	APS 的案件編號 (ADD_NEW_CASE 產的) */
    private String CaseNoWeb;

    /** APS 真正的案件編號	APS 起案之後會來更新 */
    private String CaseNoAps;

    /** 現金卡系統案件編號	 */
    private String GmCardNo;

    /** 身分證字號或護照號碼	 */
    private String Idno;

    /** 案件狀態	 */
    private String Status;

    /** APS 傳來的案件狀態	 */
    private String ApsStatus;

    /** 是否預核	1:預核 2:非預核 */
    private String IsPreAudit;

    /** 產品名稱	1 PA 預核名單preAudit(快速申辦不查聯徵)  2 PL  3 RPL  4 GM(CASHCARD) */
    private String ProductId;

    /** 職業類別	 */
    private String Occupation;

    /** 姓名	以下欄位皆為對應 WEB API 欄位 */
    private String customer_name;

    /** 生日	yyyyMMdd */
    private String birthday;

    /** 性別	1:男 2:女 */
    private String gender;

    /** 居住地行政區代碼	 */
    private String house_city_code;

    /** 居住地址	 */
    private String house_address;

    /** 住宅電話區碼	 */
    private String house_tel_area;

    /** 住宅電話	 */
    private String house_tel;

    /** 教育程度	 */
    private String education;

    /** 婚姻狀況 1 未婚 2 已婚	 */
    private String marriage;

    /** 公司名稱	 */
    private String corp_name;

    /** 公司地址行政區代碼	 */
    private String corp_city_code;

    /** 公司地址	 */
    private String corp_address;

    /** 公司電話區碼	 */
    private String corp_tel_area;

    /** 公司電話	 */
    private String corp_tel;

    /** 公司分機	畫面加上此欄位 */
    private String corp_tel_exten;

    /** 公司類別	 */
    private String corp_class;

    /** 行業別	帶空字串 */
    private String corp_type;

    /** 職務名稱	 */
    private String title;

    /** 到職日	yyyyMMdd */
    private String on_board_date;

    /** 年收入	(單位：萬元) */
    private String yearly_income;

    /** 不動產狀況	(1.本人所有 2.配偶所有 3.家族所有 4.無) */
    private String estate_type;

    /** 貸款申請金額	(單位：萬元) */
    private String p_apy_amount;

    /** 有效存於行內行動電話	 */
    private String mobile_tel;

    /** 貸款期間	年 */
    private String p_period;

    /** 資金用途	 */
    private String p_purpose;

    /** 資金用途其他	 */
    private String p_purpose_name;

    /** EMAIL 地址	 */
    private String email_address;

    /** 使用者 IP	 */
    private String ip_address;

    /** 預核 專案代號	 */
    private String prj_code;

    /** 預審案件編號	 */
    private String case_no_est;

    /** 客戶勾選與我聯絡	1:不勾選 2:勾選 */
    private String ContactMe;

    /** 是否送給 TM	0:還沒 1:已送出 */
    private String ContactMeFlag;

    /** 圖檔是否上傳影像	0:還沒 1:已送出 */
    private String AddPhotoFlag;

    /** 職業類別 Aps 檢核結果	 */
    private String OccupatioFlag;

    /** 是否為重要客戶	Y:重要客戶 N:非重要客戶 */
    private String ImportantCust;

    /** AML 回傳結果 是否已查詢過AML 0/1	 */
    private String AMLFlag;

    /** 更新時間	 */
    private String UTime;

    /** 使用者類別	0: 新戶 1:信用卡戶 2:存款戶 3:純貸款戶 */
    private String UserType;

    /** 戶藉資料 地址行政區代碼 */
    private String ResAddrZipCode;

    /** 戶藉資料 地址 */
    private String ResAddr;

    /** 戶藉資料 電話區碼 */
    private String ResTelArea;

    /** 戶藉資料 電話 */
    private String ResTel;

    /** 業務員代號 */
    private String AgentNo;

    /** 業務員部門 */
    private String AgentDepart;

    /** CreateTime */
    private String CreateTime;

    /** EndTime */
    private String EndTime;

    /** SendedTime */
    private String SendedTime;

    /** 副狀態 */
    private String SubStatus;

    /** 客戶流程 1: 預約分行流程 2: 數位帳戶流程 */
    private String Process;

    /** 身份證換發日期 */
    private String IdCardDate;

    /** 身份證換發地點 */
    private String IdCardLocation;

    /** 身份證換發紀錄 0:初領 1:補領 2:換發 */
    private String IdCardCRecord;

    /** 1. 發動起案時間(產生 decisionCaseNo 的時間) */
    private String AddNewCaseTime;
    
    /** 2. 送詳細資料時間 */
    private String SendCustInfoTime;
    
    /** 3. 行內是否有舊財力 00:無, 01:有 */ 
    private String HasOldFinaImg;
    
    /** 4. 舊財力影編代號列表 */
    private String OldFinaImgNoList;
    
    /** 5. 舊財力可否使用 00:不可, 01:可 */
    private String CanUseOldFinaImg;
    
    /** 6. 行內是否有舊ID 00:無, 01:有 */
    private String HasOldIdImg;
    
    /** 7. 舊ID影編代號列表 */
    private String OldIdImgNoList;
    
    /** 8. 舊ID可否使用 00:不可, 01:可 */
    private String CanUseOldIdImg;
    
    /** 9. 是否已透過UI上傳ID 00:無, 01:有, 02:無須上傳 */
    private String HasUploadId;
    
    /** 10. 是否已已透過UI上傳財力 00:無, 01:有, 02:無須上傳 */
    private String HasUploadFina;
    
    /** 11. 是否已通過AML 00:否, 01:是 */
    private String HasQryAml;
    
    /** 12. 是否已通過戶役政 00:否, 01:是 */
    private String HasQryIdInfo;
    
    /** 13. 是否已通過Z07 00:否, 01:是 */
    private String HasQryZ07;
    
    /** 14. 是否已產生KYC表 00:否, 01:是 */
    private String HasFinishKyc;

    /** 是否已通過檢核利害關係人 00:否, 01:是 */
    private String HasNoRelp;
    
    /** 15. 案件是否已完整 最後上傳影像XML之cComplete結果(Y/N) */
    private String IsCaseIntegrated;

    /** 16. 是否為免財力證明 00:否, 01:是 */
    private String ApplyWithoutFina;
    
    /** 17. 決策平台編號 透過 api/KGI/ADD_LOAN_CASE 取得的編號，下列決策平台 API都需要用這個編號做後續發查用 */
    private String DecisionCaseNo;

    /** 18. 是否為薪轉戶 判斷客戶是否為薪轉戶(徵審) Y 薪轉戶 N 非薪轉戶 */
    private String CheckSalaryAccount;

    /** 19. 是否為信用卡戶 Y/N */
    private String CheckCreditcardAccount;

    /** 20. 是否為現金卡戶 Y/N */
    private String CheckCashcardAccount;

    /** 21. 是否為存款戶 Y/N */
    private String CheckKgibankAccount;

    /** 22. 是否為貸款戶 Y/N */
    private String CheckLoanAccount;

    /** 23. 客戶填入之行動電話 */
    private String phone;

    // 身份證正面
    /** OCR 身份證字號 */
    private String OCR_FRONT_Idno;
    /** 身份證字號 */
    private String OCR_FRONT_BYPASSID;
    /**  */
    private String OCR_FRONT_RspCode;
    /** 姓名 */
    private String OCR_FRONT_Name;
    /** 發證日期eeeMMdd */
    private String OCR_FRONT_IDCARD_ISSUE_DT;
    /** 發證類型 */
    private String OCR_FRONT_IDCARD_ISSUE_Type;
    /** 發證縣市 */
    private String OCR_FRONT_IDCARD_ISSUE_City;

    // 身份證反面
    /** 縣市區域 */
    private String OCR_BACK_HouseholdAddr1;
    /**  */
    private String OCR_BACK_HouseholdAddr2;
    /**  */
    private String OCR_BACK_HouseholdAddr3;
    /** 完整地址 */
    private String OCR_BACK_HouseholdAddr4;
    /** 婚姻:未婚/已婚 */
    private String OCR_BACK_Marriage;

    /** 發查聯徵成功 Y/N */
    private String JcicOk;

    /** 發查聯徵的時間 */
    private String JcicTime;

    /** 發查Z07 回傳聯徵資料 html */
    private String Z07Html;

    /** 發查Z07 null, Y：是(有通報案件記錄), N：否 */
    private String Z07Result;

    /** 發查Z07的時間 yyyy-MM-dd HH:mm:ss.SSS */
    private String Z07Time;

    /** 發查AML成功:N 身分為正常 Y 身分為黑名單 V 身分為疑似有異常 or 99 */
    private String AmlResult;

    /** 發查AML的時間 yyyy-MM-dd HH:mm:ss.SSS */
    private String AmlTime;

    /** 排程狀態 */
    private String JobStatus;

    /** 風險評級:高 中 低 */
    private String CddRateRisk;

    /** 稍後補件上傳 簡訊傳送時間 yyyy-MM-dd HH:mm:ss.SSS */
    private String LaterAddonSendTime;

    /** 發查利害關係人: Y：利害關係人，N：非利害關係人 */
    private String CheckRelpResult;

    /** 發查利害關係人的時間: yyyy-MM-dd HH:mm:ss.SSS */
    private String CheckRelpTime;

    /** 發查 取得案件申請評分等..資訊 */
    private String DgtcaseResult;

    /** 發查 取得案件申請評分等..資訊 的時間 */
    private String DgtcaseTime;

    /** 發查 影像系統新API(GetCaseComplete)檢核是否後端整件狀態已完整 */
    private String ImgSysCaseComplete;

    /** 發查 影像系統新API(GetCaseComplete)檢核是否後端整件狀態已完整 的時間 */
    private String ImgSysCaseCompleteTime;

    /** 同意使用與貴行往來之資料申辦貸款 Result */
    private String loan_personal_data_Result;

	/** 同意使用與貴行往來之資料申辦貸款 Time */
    private String loan_personal_data_Time;

    /** 理債平台的前一案件編號 */
    private String CaseNoApsPrev;

    /** INFOSOURCE_CDE：得知管道 @see CREATE_NEW_CASE */
    private String INFOSOURCE_CDE;

    /** 專案代號 */
    private String PJ;

    /** 他行帳戶驗身的手機 */
    private String pcode2566_phone;

    /** FTP 上傳影像 XML 欄位 subcaseType: (0/1/2) */
    private String subcaseType;

    /** FTP 上傳影像 XML 欄位 cAbnormalReason: 缺ID、缺戶役政、缺財力、缺同一關係人表、缺風險評級、缺AML、缺Z07、是利關人、缺KYC、APS異常件 */
    private String cAbnormalReason;

    /** 跨售使用 優先核准金額上限 */
    private String creditLimit;

    /** 電文 CHECK_NO_FINPF 的 MEMO 欄位值 */
    private String CHECK_NO_FINPF_MEMO;

    public void setupSubStatus(CaseDataSubStatus subStatus) {
        setStatus(subStatus.getStatus());
        setSubStatus(subStatus.getSubStatus());
    }

    public void setupJobStatus(CaseDataJobStatus jobStatus) {
        setStatus(jobStatus.getStatus());
        setJobStatus(jobStatus.getJobStatus());  
    }

    public CaseData(String CaseNo) {
        this();
        this.CaseNo = CaseNo;
    }

    public CaseData() {

        // this.CaseNoWeb = "";
        // this.CaseNoAps = "";
        // this.GmCardNo = "";
        // this.Idno = "";
        // this.Status = "";
        this.ApsStatus = "";
        this.IsPreAudit = "";
        this.ProductId = "";
        this.Occupation = "";
        this.customer_name = "";
        this.birthday = "";
        this.gender = "";
        this.house_city_code = "";
        this.house_address = "";
        this.house_tel_area = "";
        this.house_tel = "";
        this.education = "";
        this.marriage = "";
        this.corp_name = "";
        this.corp_city_code = "";
        this.corp_address = "";
        this.corp_tel_area = "";
        this.corp_tel = "";
        this.corp_tel_exten = "";
        this.corp_class = "";
        this.corp_type = "";
        this.title = "";
        this.on_board_date = "";
        this.yearly_income = null;
        this.estate_type = "";
        this.p_apy_amount = null;
        this.mobile_tel = "";
        this.p_period = null;
        this.p_purpose = null;
        this.p_purpose_name = "";
        this.email_address = "";
        this.ip_address = "";
        this.prj_code = "";
        this.case_no_est = "";
        this.ContactMe = "";
        this.ContactMeFlag = "0";
        this.AddPhotoFlag = "0";
        this.ImportantCust = "N";
        this.ResAddrZipCode = "";
        this.ResAddr = "";
        
        this.HasOldFinaImg = "00"; // 3. 行內是否有舊財力 00:無, 01:有 *
        this.OldFinaImgNoList = ""; // 4. 舊財力影編代號列表 
        this.CanUseOldFinaImg = "01"; // 5. 舊財力可否使用 00:不可, 01:可  demand by Ben:沒這隻，所以這個欄位一律填入01
        this.HasOldIdImg = "00"; // 6. 行內是否有舊ID 00:無, 01:有 
        this.OldIdImgNoList = ""; // 7. 舊ID影編代號列表 
        this.CanUseOldIdImg = "00"; // 8. 舊ID可否使用 00:不可, 01:可 
        this.HasUploadId = "00"; // 9. 是否已透過UI上傳ID 00:無, 01:有, 02:無須上傳 
        this.HasUploadFina = "00"; // 10. 是否已已透過UI上傳財力 00:無, 01:有, 02:無須上傳 
        this.HasQryAml = "00"; // 11. 是否已通過AML 00:否, 01:是 
        this.HasQryIdInfo = "00"; // 12. 是否已通過戶役政 00:否, 01:是 
        this.HasQryZ07 = "00"; // 13. 是否已通過Z07 00:否, 01:是 
        this.HasFinishKyc = "00"; // 14. 是否已產生KYC表 00:否, 01:是 
        this.HasNoRelp = "00"; // 是否已通過檢核利害關係人 00:否, 01:是
        this.IsCaseIntegrated = "00"; // 15. 案件是否已完整 00:否, 01:是 
        this.ApplyWithoutFina = "00"; // 16. 是否為免財力證明 00:否, 01:是 
    }
}