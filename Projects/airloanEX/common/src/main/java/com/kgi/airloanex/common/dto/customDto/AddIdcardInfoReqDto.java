package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AddIdcardInfoReqDto {

    /** (1)	CUSTOMER_ID：客戶身分證字號，NVARCHAR(10) */
    private String CUSTOMER_ID;

    /** (2)	DGTPlatformNo：數位案件編號，NVARCHAR(20) */
    private String DGTPlatformNo;

    /** (3)	IDCARD_DATE：發證日期(YYYYMMDD)，NVARCHAR(8) */
    private String IDCARD_DATE;

    /** (4)	IDCARD_CITY：發證地點，NVARCHAR(5) */
    private String IDCARD_CITY;

    /** (5)	IDCARD_TYPE：領補換類別，NVARCHAR(5) */
    private String IDCARD_TYPE;

    /** (6)	IDCARD_CHK_DATE:戶役政驗證完成時間(YYYYMMDDHHMMSS)，NVARCHAR(14) */
    private String IDCARD_CHK_DATE;
}
