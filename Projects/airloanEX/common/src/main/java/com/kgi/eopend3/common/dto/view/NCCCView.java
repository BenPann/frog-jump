package com.kgi.eopend3.common.dto.view;

import com.google.gson.GsonBuilder;
import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

public class NCCCView {

	@CheckNullAndEmpty
    private String pan = "";
	@CheckNullAndEmpty
    private String expDate = "";
	@CheckNullAndEmpty
    private String extNo = "";
	
	private String idno = "" ;
	private String birthday = "" ;
	
	private String ncccMerchantId = "" ;
	private String ncccTermId = "" ;
	private String ncccTransMode = "" ;
	private String transAmt = "" ;
	
    private String ESBClientId ;
    private String ESBClientPAZZD;
    private String ESBTimestamp;
    private String ESBChannel ;
    private String ESBQueueReceiveName;
    

	
    public String getNcccMerchantId() {
		return ncccMerchantId;
	}

	public void setNcccMerchantId(String ncccMerchantId) {
		this.ncccMerchantId = ncccMerchantId;
	}

	public String getNcccTermId() {
		return ncccTermId;
	}

	public void setNcccTermId(String ncccTermId) {
		this.ncccTermId = ncccTermId;
	}

	public String getNcccTransMode() {
		return ncccTransMode;
	}

	public void setNcccTransMode(String ncccTransMode) {
		this.ncccTransMode = ncccTransMode;
	}

	public String getTransAmt() {
		return transAmt;
	}

	public void setTransAmt(String transAmt) {
		this.transAmt = transAmt;
	}

	public String getIdno() {
		return idno;
	}

	public void setIdno(String idno) {
		this.idno = idno;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getExtNo() {
        return extNo;
    }

    public void setExtNo(String extNo) {
        this.extNo = extNo;
    }

	public String getESBClientId() {
		return ESBClientId;
	}

	public void setESBClientId(String eSBClientId) {
		ESBClientId = eSBClientId;
	}

	public String getESBClientPAZZD() {
		return ESBClientPAZZD;
	}

	public void setESBClientPAZZD(String eSBClientPAZZD) {
		ESBClientPAZZD = eSBClientPAZZD;
	}

	public String getESBTimestamp() {
		return ESBTimestamp;
	}

	public void setESBTimestamp(String eSBTimestamp) {
		ESBTimestamp = eSBTimestamp;
	}

	public String getESBChannel() {
		return ESBChannel;
	}

	public void setESBChannel(String eSBChannel) {
		ESBChannel = eSBChannel;
	}

	public String getESBQueueReceiveName() {
		return ESBQueueReceiveName;
	}

	public void setESBQueueReceiveName(String eSBQueueReceiveName) {
		ESBQueueReceiveName = eSBQueueReceiveName;
	}

	public String toJsonString() {
		return new GsonBuilder().disableHtmlEscaping().create().toJson(this) ;
	}

}