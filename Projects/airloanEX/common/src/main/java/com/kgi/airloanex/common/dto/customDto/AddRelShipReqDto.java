package com.kgi.airloanex.common.dto.customDto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddRelShipReqDto { 
    
    /** 客戶身分證字號 */
    private String CUSTOMER_ID;

    /** 數位案件編號 */
    private String DGTPlatformNo;

    /** 同一關係人親屬資料 */
    private List<AddRelShipReqDtoDataList> DATA_LIST = new ArrayList<>();

    /** 同一關係人企業資料 */
    private List<AddRelShipReqDtoDataCompList> DATA_COMP_LIST = new ArrayList<>();
    
}
