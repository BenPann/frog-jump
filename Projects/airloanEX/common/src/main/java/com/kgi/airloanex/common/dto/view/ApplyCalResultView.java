package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplyCalResultView {
    String phone;
    String CASE_NO;
}
