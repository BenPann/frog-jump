package com.kgi.eopend3.common.dto.respone;

public class LogFromValidation {

	private String req = "";
    private String res = "";

    public String getReq() {
 		return req;
 	}

 	public void setReq(String req) {
 		this.req = req;
 	}

 	public String getRes() {
 		return res;
 	}

 	public void setRes(String res) {
 		this.res = res;
 	}
        
}