package com.kgi.eopend3.common.dto.db;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ED3_AOResult {

    private String UniqId = "";
    private String Status = "";
    private String OpenfailMsg = "";
    private String StatusTw = "";
    private String SalaryAccountTw = "";    
    @SerializedName(value = "passBookTw",alternate = {"PassBookTw"})
    private String PBT = "";
    private String StatusFore = "";
    private String SalaryAccountFore = "";    
    @SerializedName(value = "passBookFore",alternate = {"PassBookFore"})
    private String PBF = "";
    private String TrustStatus = "";
    private String D3Message = "";   
    private byte[] D3Base64PDF = new byte[]{};
    private String FailedImage;
    private String RegNo ;
    private String RegDate ;
    
}