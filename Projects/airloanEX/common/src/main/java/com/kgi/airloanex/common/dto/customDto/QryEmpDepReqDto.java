package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class QryEmpDepReqDto {
    
    /** (1)	EMP_ID：員工編號，NVARCHAR (6) */
    private String EMP_ID;
}
