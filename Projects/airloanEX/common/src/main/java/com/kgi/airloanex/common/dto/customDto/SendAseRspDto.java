package com.kgi.airloanex.common.dto.customDto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SendAseRspDto {

    // (1) CHECK_CODE：
    // A. 檢核資料錯誤(空白代表沒有錯誤)
    // B. 如果有多個錯誤用『;』區隔,例如01;02
    // C. 『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
    // D. 錯誤代碼對照表:
    // 代碼 說明
    // ============================
    // 01 案件編號格式錯誤
    // 02 問題代號清單格式錯誤
    // 03 問題答案清單格式錯誤
    // 04 系統異常
    // 99 非預期性系統異常
    private String CHECK_CODE;
    // (2) ERR_MSG：
    // A. 當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
    private String ERR_MSG;
    // (3) RESULT_CODE：
    // A. 回應代碼:CHAR(1)，Y：有額度，N：無額度
    private String RESULT_CODE;
    // (4) PROJECT_CODE：
    // A. 專案代號:VARCHAR(4)
    private String PROJECT_CODE;
    // (5) CREDIT_LINE：
    // A. 建議額度:INT
    private String CREDIT_LINE;
    // (6) INTEREST_RATE：
    // A. 建議利率: decimal(6,3)
    private String INTEREST_RATE;
    // (7) RANK：
    // A. RANK：INT
    private String RANK;
    // (8) SCORE：
    // A. 分數：decimal(8,3)
    private String SCORE;

    private String CASE_NO;
}