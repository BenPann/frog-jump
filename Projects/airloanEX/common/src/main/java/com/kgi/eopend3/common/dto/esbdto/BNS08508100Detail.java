package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "MembCustAc", "Currency", "AcctType", "IntCat", "CurrStatus" , "CloseDt" })
public class BNS08508100Detail {
    @XmlElement(name = "MembCustAc")
    private String MembCustAc;

    @XmlElement(name = "Currency")
    private String Currency;

    @XmlElement(name = "AcctType")
    private String AcctType;

    @XmlElement(name = "IntCat")
    private String IntCat;

    @XmlElement(name = "CurrStatus")
    private String CurrStatus;

    @XmlElement(name = "CloseDt")
    private String CloseDt;

    public String getMembCustAc() {
        return MembCustAc;
    }

    public void setMembCustAc(String membCustAc) {
        MembCustAc = membCustAc;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    public String getAcctType() {
        return AcctType;
    }

    public void setAcctType(String acctType) {
        AcctType = acctType;
    }

    public String getIntCat() {
        return IntCat;
    }

    public void setIntCat(String intCat) {
        IntCat = intCat;
    }

    public String getCurrStatus() {
        return CurrStatus;
    }

    public void setCurrStatus(String currStatus) {
        CurrStatus = currStatus;
    }

    public String getCloseDt() {
        return CloseDt;
    }

    public void setCloseDt(String closeDt) {
        CloseDt = closeDt;
    }

    @Override
    public String toString() {
        return "BNS08508100Detail [AcctType=" + AcctType + ", CloseDt=" + CloseDt + ", CurrStatus=" + CurrStatus
                + ", Currency=" + Currency + ", IntCat=" + IntCat + ", MembCustAc=" + MembCustAc + "]";
    }

}