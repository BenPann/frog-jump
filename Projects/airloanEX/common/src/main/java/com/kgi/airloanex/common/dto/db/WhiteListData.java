package com.kgi.airloanex.common.dto.db;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class WhiteListData {
    @NonNull
    @CheckNullAndEmpty
    private String ID;
    // @NonNull
    private int PType;
    @NonNull
    @CheckNullAndEmpty
    private String ApiName;
	private String Response;
}