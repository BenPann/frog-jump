package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class NewCaseChkReqDto {
    /** (1)	CUSTOMER_ID：客戶身分證字號，NVARCHAR(10) */
    private String CUSTOMER_ID;
    /** (2)	PROD_TYPE：產品別(1-GM/RPL、2-PLOAN)，NCHAR(1) */
    private String PROD_TYPE;
}
