package com.kgi.crosssell.common.dto;

public class CSCommonData {
    private String type = "";
    private String chtName = "";
    private String engName = "";
    private String idno = "";
    private String idCardDate = "";
    private String idCardLocation = "";
    private String idCardCRecord = "";
    private String gender = "";
    private String birthday = "";
    private String mobile = "";
    private String productType= "";
    private String productID= "";
    private String applyAmount= "";
    private String period= "";
    private String purpose = "";
    private String purposeOther = "";
    private String email = "";
    private String education = "";
    private String marriage = "";
    private String resAddrZipCode = "";
    private String resAddr = "";
    private String commAddrZipCode = "";
    private String commAddr = "";
    private String homeAddrZipCode = "";
    private String homeAddr = "";
    private String resTelArea = "";
    private String resTel = "";
    private String homeTelArea = "";
    private String homeTel = "";
    private String estateType = "";
    private String occupation = "";
    private String corpName  = "";
    private String corpTelArea  = "";
    private String corpTel  = "";
    private String corpTelExten  = "";
    private String corpAddrZipCode  = "";
    private String corpAddr  = "";
    private String corpDepart  = "";
    private String jobTitle  = "";
    private String onBoardDate  = "";
    private String yearlyIncome  = "";
    private String taxIdno  = "";
    private String projectCode = "";
    private String additionalData = "";
    private String score = "";
    private String channelID  = "";
    private String promoDepart  = "";
    private String promoMember = "";
    private CSCommonImage image;



    public String getIdCardCRecord() {
        return idCardCRecord;
    }

    public void setIdCardCRecord(String idCardCRecord) {
        this.idCardCRecord = idCardCRecord;
    }

    public String getIdCardLocation() {
        return idCardLocation;
    }

    public void setIdCardLocation(String idCardLocation) {
        this.idCardLocation = idCardLocation;
    }

    public String getIdCardDate() {
        return idCardDate;
    }

    public void setIdCardDate(String idCardDate) {
        this.idCardDate = idCardDate;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getChtName() {
        return chtName;
    }

    public void setChtName(String value) {
        this.chtName = value;
    }

    public String getEngName() {
        return engName;
    }

    public void setEngName(String value) {
        this.engName = value;
    }

    public String getIdno() {
        return idno;
    }

    public void setIdno(String value) {
        this.idno = value;
    }



    public String getGender() {
        return gender;
    }

    public void setGender(String value) {
        this.gender = value;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String value) {
        this.birthday = value;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String value) {
        this.mobile = value;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String value) {
        this.productType = value;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String value) {
        this.productID = value;
    }

    public String getApplyAmount() {
        return applyAmount;
    }

    public void setApplyAmount(String value) {
        this.applyAmount = value;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String value) {
        this.period = value;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String value) {
        this.purpose = value;
    }

    public String getPurposeOther() {
        return purposeOther;
    }

    public void setPurposeOther(String value) {
        this.purposeOther = value;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String value) {
        this.email = value;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String value) {
        this.education = value;
    }

    public String getMarriage() {
        return marriage;
    }

    public void setMarriage(String value) {
        this.marriage = value;
    }

    public String getResAddrZipCode() {
        return resAddrZipCode;
    }

    public void setResAddrZipCode(String value) {
        this.resAddrZipCode = value;
    }

    public String getResAddr() {
        return resAddr;
    }

    public void setResAddr(String value) {
        this.resAddr = value;
    }

    public String getCommAddrZipCode() {
        return commAddrZipCode;
    }

    public void setCommAddrZipCode(String value) {
        this.commAddrZipCode = value;
    }

    public String getCommAddr() {
        return commAddr;
    }

    public void setCommAddr(String value) {
        this.commAddr = value;
    }

    public String getHomeAddrZipCode() {
        return homeAddrZipCode;
    }

    public void setHomeAddrZipCode(String value) {
        this.homeAddrZipCode = value;
    }

    public String getHomeAddr() {
        return homeAddr;
    }

    public void setHomeAddr(String value) {
        this.homeAddr = value;
    }

    public String getResTelArea() {
        return resTelArea;
    }

    public void setResTelArea(String value) {
        this.resTelArea = value;
    }

    public String getResTel() {
        return resTel;
    }

    public void setResTel(String value) {
        this.resTel = value;
    }

    public String getHomeTelArea() {
        return homeTelArea;
    }

    public void setHomeTelArea(String value) {
        this.homeTelArea = value;
    }

    public String getHomeTel() {
        return homeTel;
    }

    public void setHomeTel(String value) {
        this.homeTel = value;
    }

    public String getEstateType() {
        return estateType;
    }

    public void setEstateType(String value) {
        this.estateType = value;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String value) {
        this.occupation = value;
    }

    public String getCorpName() {
        return corpName;
    }

    public void setCorpName(String value) {
        this.corpName = value;
    }

    public String getCorpTelArea() {
        return corpTelArea;
    }

    public void setCorpTelArea(String value) {
        this.corpTelArea = value;
    }

    public String getCorpTel() {
        return corpTel;
    }

    public void setCorpTel(String value) {
        this.corpTel = value;
    }

    public String getCorpTelExten() {
        return corpTelExten;
    }

    public void setCorpTelExten(String value) {
        this.corpTelExten = value;
    }

    public String getCorpAddrZipCode() {
        return corpAddrZipCode;
    }

    public void setCorpAddrZipCode(String value) {
        this.corpAddrZipCode = value;
    }

    public String getCorpAddr() {
        return corpAddr;
    }

    public void setCorpAddr(String value) {
        this.corpAddr = value;
    }

    public String getCorpDepart() {
        return corpDepart;
    }

    public void setCorpDepart(String value) {
        this.corpDepart = value;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String value) {
        this.jobTitle = value;
    }

    public String getOnBoardDate() {
        return onBoardDate;
    }

    public void setOnBoardDate(String value) {
        this.onBoardDate = value;
    }

    public String getYearlyIncome() {
        return yearlyIncome;
    }

    public void setYearlyIncome(String value) {
        this.yearlyIncome = value;
    }

    public String getTaxIdno() {
        return taxIdno;
    }

    public void setTaxIdno(String value) {
        this.taxIdno = value;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String value) {
        this.projectCode = value;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String value) {
        this.additionalData = value;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String value) {
        this.score = value;
    }

    public String getChannelID() {
        return channelID;
    }

    public void setChannelID(String value) {
        this.channelID = value;
    }

    public String getPromoDepart() {
        return promoDepart;
    }

    public void setPromoDepart(String value) {
        this.promoDepart = value;
    }

    public String getPromoMember() {
        return promoMember;
    }

    public void setPromoMember(String value) {
        this.promoMember = value;
    }

    public CSCommonImage getImage() {
        return image;
    }

    public void setImage(CSCommonImage value) {
        this.image = value;
    }
}
