package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyInfoRspDtoDataYellowPage {
    private String Category1;
    private String Category2;
    private String Category3;
    private String Category4;
}
