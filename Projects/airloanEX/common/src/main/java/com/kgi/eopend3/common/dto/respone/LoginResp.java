package com.kgi.eopend3.common.dto.respone;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResp {

    private String uniqId = "";    
    private String uniqType = "";
    private String idno = "";
    private String ipAddress = "";
    
}