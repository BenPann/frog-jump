package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AddRelShipReqDtoDataList {
    
    /** 關係人姓名 */
    private String relship_name;
    
    /** 關係人羅馬拼音姓名 */
    private String relship_roman_pinyin_name;

    /** 身分證統一編號 */
    private String relship_id;

    /** 
     * 關係別代號
     * 08-配偶
     * 01-父母
     * 02-子女
     * 03-兄弟姊妹
     * 04-祖父母
     * 05-外祖父母
     * 06-孫子女
     * 07-外孫子女
     * 09-配偶之父母
     * 10-配偶之兄弟姊妹
     * 11-其他親屬
     * 12-其他非親屬自然人
     */
    private String relship_code;
}
