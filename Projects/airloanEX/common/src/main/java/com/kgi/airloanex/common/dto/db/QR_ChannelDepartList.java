package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class QR_ChannelDepartList {

    /** 所屬ChannelId */
    @NonNull
    private String ChannelId;
    
    /** 推廣單位ID或活動代號 */
    @NonNull
    private String DepartId;

    /** 推廣單位名稱 */
    private String DepartName = "";

    /** 此Depart啟用狀態 1:啟用、0:停用 */
    private String Enable = "";

    /** 申辦未完成 通知的EMAIL 多個EMAIL使用;分隔 */
    private String EMail = "";
    // ------------------------------------------------------
    // 貸款用
    /** 轉介單位 */
    private String TransDepart = "";
    
    /** 轉介員工 */
    private String TransMember = "";
    
    /** 推廣單位 */
    private String PromoDepart = "";
    
    /** 推廣員工 */
    private String PromoMember = "";
    
    /** 貸款分派單位 */
    private String AssignDepart = "";
    // ------------------------------------------------------
    // 信用卡用
    /** 預設推廣單位 */
    private String PromoDepart1 = "";

    /** 預設推廣人員 */
    private String PromoMember1 = "";

    /** 預設推廣單位2 */
    private String PromoDepart2 = "";

    /** 預設推廣人員2 */
    private String PromoMember2 = "";
    
    /** 信用卡既有戶分派單位 */
    private String AssignDepartExist = "";
    
    /** 信用卡新戶分派單位 */
    private String AssignDepartNew = "";
    // ------------------------------------------------------
    private String SendAddress = "";

    
}