package com.kgi.eopend3.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.*;


@Getter
@Setter
@CheckNullAndEmpty
public class ProductListView {

	private String userType;
	private String productType;
	private String caseSource;
	
}
