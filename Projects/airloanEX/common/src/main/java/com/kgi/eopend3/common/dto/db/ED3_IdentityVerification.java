package com.kgi.eopend3.common.dto.db;

public class ED3_IdentityVerification {

    private String uniqId = "";
    private String verifyStatus = "";
    private String verifyType = "";
    private String cardNo = "";
    private String cardTime = "";
    private String bankId = "";
    private String accountNo = "";
    private String bookingBranchId = "";
    private String bookingDate = "";
    private String bookingTime = "";
    private Integer errorCount;
    private Integer ncccErrorCount;
    private Integer pcode2566ErrorCount;
    private String errorMsg = "";
    private String myCard = "N";

    public ED3_IdentityVerification() {
    }

    public ED3_IdentityVerification(String uniqId) {
        this.uniqId = uniqId;
    }

    public String getUniqId() {
        return uniqId;
    }

    public void setUniqId(String uniqId) {
        this.uniqId = uniqId;
    }

    public String getVerifyStatus() {
        return verifyStatus;
    }

    public void setVerifyStatus(String verifyStatus) {
        this.verifyStatus = verifyStatus;
    }

    public String getVerifyType() {
        return verifyType;
    }

    public void setVerifyType(String verifyType) {
        this.verifyType = verifyType;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardTime() {
        return cardTime;
    }

    public void setCardTime(String cardTime) {
        this.cardTime = cardTime;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }
   
    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Integer getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(Integer errorCount) {
        this.errorCount = errorCount;
    }

    public Integer getNCCCErrorCount() {
        return ncccErrorCount;
    }

    public void setNCCCErrorCount(Integer ncccErrorCount) {
        this.ncccErrorCount = ncccErrorCount;
    }

    public Integer getPcode2566ErrorCount() {
        return pcode2566ErrorCount;
    }

    public void setPcode2566ErrorCount(Integer pcode2566ErrorCount) {
        this.pcode2566ErrorCount = pcode2566ErrorCount;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccoutNo(String accountNo) {
        this.accountNo = accountNo;
    }

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getBookingTime() {
		return bookingTime;
	}

	public void setBookingTime(String bookingTime) {
		this.bookingTime = bookingTime;
	}

    public String getBookingBranchId() {
        return bookingBranchId;
    }

    public void setBookingBranchId(String bookingBranchId) {
        this.bookingBranchId = bookingBranchId;
    }

	public String getMyCard() {
		return myCard;
	}

	public void setMyCard(String myCard) {
		this.myCard = myCard;
	}
  
}
