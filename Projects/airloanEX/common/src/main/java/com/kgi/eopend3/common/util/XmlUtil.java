package com.kgi.eopend3.common.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XmlUtil {

	/**
	 * 回傳 XML 中的節點
	 */
	public static NodeList getXmlNodes(String xml, String nodeName) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes("UTF-8"))));
//			Document document = builder.parse(new InputSource(new StringReader(xml)));

			NodeList nodes = document.getElementsByTagName(nodeName);
//System.out.println(nodes.getLength());
			/*
			 * for (int cnt=0; cnt< nodes.getLength(); cnt++) { Node node = nodes.item(cnt)
			 * ; if (node.getNodeType() == Node.ELEMENT_NODE) { Element detail = (Element)
			 * node;
			 * System.out.println(detail.getElementsByTagName("RM").item(0).getTextContent()
			 * ); System.out.println(detail.getElementsByTagName("DocumentNo").item(0).
			 * getTextContent()); }
			 * 
			 * }
			 */
			return nodes;
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static String getNodeValue(Element element, String tagName) {
		try {
			return element.getElementsByTagName(tagName).item(0).getTextContent();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
