package com.kgi.airloanex.common.dto.db;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class ContractMainPdf {
    @NonNull
    @CheckNullAndEmpty
    private String UniqId;
    private String Version;
    private byte[] PdfContent;
}