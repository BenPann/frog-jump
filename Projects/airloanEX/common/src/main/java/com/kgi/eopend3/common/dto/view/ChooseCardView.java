package com.kgi.eopend3.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;

@CheckNullAndEmpty
@Setter
@Getter
public class ChooseCardView  {

    private String creditProductId = "";
      
}