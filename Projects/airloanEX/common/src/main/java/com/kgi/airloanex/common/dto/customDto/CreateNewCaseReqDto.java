package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateNewCaseReqDto {

   /** (1)	CASE_NO_WEB：網路案件編號，(string) */
   private String CASE_NO_WEB;

   /** (2)	CUSTOMER_ID：客戶ID，(string) */
   private String CUSTOMER_ID;

   /** (3)	CUSTOMER_NAME：客戶姓名，(string) */
   private String CUSTOMER_NAME;

   /** (4)	DATA_TYPE：資料類型，1.預審2.一般信貸3.一般e貸寶，(string) */
   // CaseData >> ProductId
   private String DATA_TYPE;

   /** (5)	BIRTHDAY：生日(YYYYMMDD)，(string) */
   private String BIRTHDAY;

   /** (6)	MOBILE_TEL：手機號碼，(string) */
   private String MOBILE_TEL;

   /** (7)	P_APY_AMOUNT：貸款申請金額，(string) */
   private String P_APY_AMOUNT;

   /** (8)	HOUSE_CITY_CODE：居住地行政區代碼，(string) */
   private String HOUSE_CITY_CODE;

   /** (9)	HOUSE_ADDRESS：居住地址，(string) */
   private String HOUSE_ADDRESS;

   /** (10)	AUTH_TYPE：驗證方式1.OTP 2.MBC 4.PCode2566，(string) */
   // 計有戶 ＯＴＰ 
   private String AUTH_TYPE;

   /** (11)	ENT_TYPE：異業來源，CHT(中華電信)，(string) */
   //  第一順位:ENTRYDATA中OTHERS資料中如有CH參數，則以CH參數前兩碼為優先
   // 第二順位:ENTRYDATA中的 ENTRY 欄位前兩碼
   private String ENT_TYPE;

   /** (12)	STATUS_CODE：狀態代碼，(string) */
   // 先留空白
   private String STATUS_CODE;

   /** (13)	MESSAGE：狀態訊息，(string) */
   // 先留空白
   private String MESSAGE;

   /** (14)	PLOAN_CC_FLG：是否是卡加貸類型(Y/N)，(string) */
   // 新戶 或 從信用卡申請頁帶入為Ｙ
   private String PLOAN_CC_FLG;

   /** (15)	PLOAN_CC_JCIC_DATE：卡加貸聯徵發查日(YYYYMMDD)，(string) */
   // PLOAN_CC_FLG 為 Ｙ 時要填
   private String PLOAN_CC_JCIC_DATE;

   /** (16)	PLOAN_CC_JCIC_HTML：卡加貸聯徵發查HTML，(string) */
   // PLOAN_CC_FLG 為 Ｙ 時要填
   private String PLOAN_CC_JCIC_HTML;

   /** (17)	ORG_PROJECT_NO：初審貸款專案代號（卡加貸前半核卡後信用卡徵審會回傳給WEB，(string) */
   // 卡加貸 才會有
   private String ORG_PROJECT_NO;

   /** (18)	ORG_CREDIT_LINE：初審金額（卡加貸前半核卡後信用卡徵審會回傳給WEB），(string) */
   // 卡加貸 才會有
   private String ORG_CREDIT_LINE;

   /** (19)	ORG_INTEREST_RATE：初審利率（卡加貸前半核卡後信用卡徵審會回傳給WEB），(string) */
   // 卡加貸 才會有
   private String ORG_INTEREST_RATE;

   /** (20)	ORG_RANK：初審RANK（卡加貸前半核卡後信用卡徵審會回傳給WEB），(string) */
   // 卡加貸 才會有
   private String ORG_RANK;

   /** (21)	ORG_SCORE：初審分數（卡加貸前半核卡後信用卡徵審會回傳給WEB），(string) */
   // 卡加貸 才會有
   private String ORG_SCORE;

   /** (22)	IS_CONSENT：是否勾選異業同意書(1-是 2-否 0-請選擇)，(string) */
   // 預設 2 
   private String IS_CONSENT;

   /** (23)	IS_CONSENT_DATETIME：勾選異業同意書時間(yyyymmddhhmmss)，(string) */
   // 無值帶空
   private String IS_CONSENT_DATETIME;

   /** (24)	EXTERNAL_CHECK_TYPE：驗身方式 (01-OTP / 02-MBC / 03-他行信用卡 / 04-他行存戶) (帶入名稱非代碼)，(string) */
   // AUTH_TYPE 帶入名稱
   private String EXTERNAL_CHECK_TYPE;

   /** (25)	CASE_NO_SCORE_CC：卡加貸評分編號(信用卡取評分用的編號)，(string) */
   private String CASE_NO_SCORE_CC;

   /** (26)	EXTERNAL_SOURCE：異業來源代碼(公司種別代碼)，(string) */
   // channel id ,同(11)
   private String EXTERNAL_SOURCE;

   /** (27)	EXTERNAL_SOURCE_CH：異業來源中文名稱(公司種別名稱)，(string) */
   //  用(26)兩碼查詢 QR_CHANNELLIST 後取 CHANNEL_NAME 欄位
   //   (ex: select ChannelName from QR_CHANNELLIST WHERE CHANNELID=@channelid; )
   private String EXTERNAL_SOURCE_CH;

   /** (28)	ASSIGN_BRANCH：派件單位，(string) */
   // QR_ChannelDepartList > AssignDepart
   // 1.登入時填入之員編透過API查得之單位代號D1，用(26)之ChannelId與D1前三碼查詢 QR_CHANNELDEPARTLIST
   // (ex: select * from QR_CHANNELDEPARTLIST WHERE CHANNELID=@channelid and LEFT(Depart,3)=LEFT(@D1,3); )
   // 結果只會有一列資料，派件單位=AssignDepart欄位
   // 2.如登入時填入之員編透過API查不到單位代號，則預設單位代號為'0000'，用(26)之ChannelId與0000查詢  QR_CHANNELDEPARTLIST
   // (ex: select * from QR_CHANNELDEPARTLIST WHERE CHANNELID=@channelid and Depart='0000'; )
   // 結果只會有一列資料，派件單位=AssignDepart欄位
   // 3.如登入時未填員編，則預設單位代號為'9743'，用(26)之ChannelId與9743查詢  QR_CHANNELDEPARTLIST
   // (ex: select * from QR_CHANNELDEPARTLIST WHERE CHANNELID=@channelid and Depart='9743'; )
   // 結果只會有一列資料，派件單位=AssignDepart欄位
   private String ASSIGN_BRANCH;

   /** (29)	B_S_UNIT：轉介單位，(string) */
   // 登入頁輸入的員編 查詢後的 員編單位  QR_ChannelDepartList > DepartId
   // 同(28)，B_S_UNIT為取結果列之TransDepart欄位
   private String B_S_UNIT;

   /** (30)	B_S_CODE：轉介人員，(string) */
   // start.html 以登入頁輸入之業務人員代號為優先，
   // 若無，則取同(28)結果列之TransMember欄位
   private String B_S_CODE;

   /** (31)	SYMNO：推廣單位，(string) */
   // QR_ChannelDepartList >> PromoDepart
   // 同(28)，B_S_UNIT為取結果列之PromoDepart欄位
   private String SYMNO;

   /** (32)	SYKNO：推廣人員，(string) */
   // QR_ChannelDepartList >> PromoMember
   // 同(28)，B_S_UNIT為取結果列之PromoMember欄位
   private String SYKNO;

   /** (33)	COMP_CASE：是否為完整件(Y/N)，(string) */
   // 戶役政正常 免財力 金額小於100萬
   // CaseData >> IsCaseIntegrated
   private String COMP_CASE;

   /** (34)	NO_FINPF：是否為免財力證明客戶(Y/N)，(string) */
   // CaseData >> ApplyWithoutFina
   private String NO_FINPF;

   /** (35)	HAVE_FINPF：是否有舊有財力(Y/N)，(string) */
   // CaseData >> HasOldFinaImg
   private String HAVE_FINPF;

   /** (36) SALARY_ACCOUNT：是否為薪轉戶(Y/N)，(string) */
   // CaseData >> CheckSalaryAccount
   private String SALARY_ACCOUNT;

   /** (37) EDUCATION：教育程度(1.高中以下2.大專3.大學4.研究所以上)，(string) */
   private String EDUCATION;
   /** (38) CORP_NAME：公司名稱，(string) */
   private String CORP_NAME;
   /** (39) YEARLY_INCOME：年收入(單位 萬元)，(string) */
   private String YEARLY_INCOME;
   /** (40) ON_BOARD_DATE：到職日(YYYYMMDD)，(string) */
   private String ON_BOARD_DATE;
   /** (41) CORP_CLASS：公司種別(1.政府機關2.教育機關3.上市/上櫃/知名大企業4.一般企業5.小公司/獨資行號)，(string) */
    //  帶空
   private String CORP_CLASS;
    /** (42) CORP_TYPE：行業別，(string) */
    //  帶空
   private String CORP_TYPE;
   /** (43) TITLE：職務名稱，(string) */
   private String TITLE;

   /** (44)	AML_FLG：洗錢職業類別比對結果(Y/N)，(string) */
   private String AML_FLG;
   
   /** (45)	CORP_AML：洗錢職業類別代碼，(string) */
   private String CORP_AML;
   
   /** (46)	CORP_AML_NAME：洗錢職業類別中文名稱，(string) */
   private String CORP_AML_NAME;
   
   /** (47)	AML_RESULT：洗錢AML比對結果(Y/V/N/O/P) Y-ID命中禁制名單、V-疑似高 風險名單、N-非高風險名單、新增兩個狀態code"O-系統異常無法比對、P-非當日即時查詢，(string) */
   private String AML_RESULT;
   
   /** (48)	QRY_AML_REQUEST：洗錢AML發查電文request-content，(string) */
   private String QRY_AML_REQUEST;
   
   /** (49)	QRY_AML_RESULT：洗錢AML發查電文response-content，(string) */
   private String QRY_AML_RESULT;
   
   /** (50)	MARRIAGE：婚姻狀況(1.未婚2.已婚)，(string) */
   private String MARRIAGE;
   
   /** (51)	CHT_FLAG：是否為中華電信客戶註記，1是,0不是，(string) */
   private String CHT_FLAG;
   
   /** (52)	CREDIT_VALUE：電信評等分數，(string) */
   private String CREDIT_VALUE;

   /** (53)	CORP_CITY_CODE：公司地址行政區代碼，(string) */
   private String CORP_CITY_CODE;

   /** (54)	CORP_ADDRESS：公司地址，(string) */
   private String CORP_ADDRESS;

   /** (55)	CORP_TEL_AREA：公司電話區碼，(string) */
   private String CORP_TEL_AREA;

   /** (56)	CORP_TEL：公司電話，(string) */
   private String CORP_TEL;

   /** (57)	CORP_TEL_EXTEN：公司分機，(string) */
   private String CORP_TEL_EXTEN;

   /** (58)	EMAIL_ADDRESS：Email，(string) */
   private String EMAIL_ADDRESS;

   /** (59)	HOUSE_TEL_AREA：住宅電話區碼，(string) */
   private String HOUSE_TEL_AREA;

   /** (60)	HOUSE_TEL：住宅電話，(string) */
   private String HOUSE_TEL;

   /** (61)	ESTATE_TYPE：不動產狀況(1.本人所有2.配偶所有3.家族所有4.無)，(string) */
   private String ESTATE_TYPE;

   /** (62)	IP_ADDRESS：使用者IP，(string) */
   private String IP_ADDRESS;

   /** (63)	FIN_STATE：財力證明1.薪資證明2.扣繳憑單3.所得清單4.其他，(string) */
   private String FIN_STATE;

   /** (64)	P_PURPOSE：資金用途，(string) */
   private String P_PURPOSE;

   /** (65)	P_PURPOSE_NAME：資金用途其他說明，資金用途選『其他』時此欄為必填，(string) */
   private String P_PURPOSE_NAME;

   /** (66)	TAX_ID：公司統編，(string) */
   private String TAX_ID;

   /** (67)	HAVE_FINPF_TYPE：舊有財力類型1.影像系統舊財力、2.本行薪轉戶、3.無既有財力，(string) */
   private String HAVE_FINPF_TYPE;

   /** (68)	SPONSOR：介紹人(有多筆時用;區隔)，(string) */
   private String SPONSOR;

   /** (69)	INFOSOURCE_CDE：得知管道，(string) */
   private String INFOSOURCE_CDE;

   /** (70)	PROJECT_CODE：專案代號，(string) */
   private String PROJECT_CODE;

   /** (71)	P_PERIOD：貸款期間，(string) */
   private String P_PERIOD;

   /** (72)	CONSENT_FINANCIAL：同意使用既有資料條款(Y/N)，(string) */
   private String CONSENT_FINANCIAL;

   /** (73)	NO_FINPF_MEMO：是否為免財力證明客戶備註，(string) */
   private String NO_FINPF_MEMO;

   // 補充: 影像上傳單位(XML中BranchType)
   // 同(28)，影像上傳單位為取結果列之AssignDepart欄位

}
