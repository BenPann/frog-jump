package com.kgi.airloanex.common.dto.db;

public class QR_PromoCaseUserInput {
    private String UniqId = "";
    private String UniqType = "";
    private String PromoDepart = "";
    private String PromoMember = "";
    private String channelId = "";

    public QR_PromoCaseUserInput() {
    }

    public QR_PromoCaseUserInput(String uniqId) {
        this.setUniqId(uniqId);
    }

	public String getUniqId() {
		return UniqId;
	}

	public void setUniqId(String uniqId) {
		UniqId = uniqId;
	}

	public String getUniqType() {
		return UniqType;
	}

	public void setUniqType(String uniqType) {
		UniqType = uniqType;
	}

	public String getPromoDepart() {
		return PromoDepart;
	}

	public void setPromoDepart(String promoDepart) {
		PromoDepart = promoDepart;
	}

	public String getPromoMember() {
		return PromoMember;
	}

	public void setPromoMember(String promoMember) {
		PromoMember = promoMember;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
}