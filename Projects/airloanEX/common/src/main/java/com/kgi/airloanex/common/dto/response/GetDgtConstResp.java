package com.kgi.airloanex.common.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * 
 * @see com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoConstData
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetDgtConstResp {
    
    // (3)	CONST_DATA：
    
    /** A.	case_no：案件編號 */
    private String case_no;

    // /** B.	p_loan_no：批覆書號 */
    // private String p_loan_no;

    // /** C.	aprove_amount：額度(核准額度) */
    // private String aprove_amount;

    // /** D.	rate：利率 */
    // private String rate;

    // /** E.	aprove_period：期數(年) */
    // private String aprove_period;

    // /** F.	apy_fee：手續費 */
    // private String apy_fee;

    // /** G.	b_range：綁約期間(期) */
    // private String b_range;

    // /** H.	rate_desc：利率種類(契約條款) */
    // private String rate_desc;

    // /** I.	pay_desc：撥款方式(契約條款) */
    // private String pay_desc;

    // /** J.	pay_desc_type：撥款方式 (1-(一)撥入立約人、2-(二)撥入立約人開立於、3-(三)撥付代償立約人) */
    // private String pay_desc_type;

    // /** K.	pay_bank：代償銀行名稱 */
    // private String pay_bank;

    // /** L.	exp_bank_no：撥款銀行代號 */
    // private String exp_bank_no;

    // /** M.	exp_account_no：撥款銀行帳號 */
    // private String exp_account_no;

    // /** N.	pay_account：代償銀行帳號 */
    // private String pay_account;

    // /** O.	pay_amount：撥款金額 */
    // private String pay_amount;

    // /** P.	pay_kind：還款方式(契約條款) */
    // private String pay_kind;

    // /** Q.	pay_kind_type：還款方式(1-(一)自撥款日起、2- (二)自撥款日起、3-(三)寬限期自撥款日起算、4-(五)其他約定方式：、5-(四)員工貸款約定：) */
    // private String pay_kind_type;

    // /** R.	pay_kind_day：繳款日 */
    // private String pay_kind_day;

    // /** S.	pay_way：還款授權扣款方式(契約條款) */
    // private String pay_way;

    // /** T.	pay_way_type：還款授權扣款方式(1-本行、2-eDDA、3-ACH) 
    //  * pay_way_type = 1 : 自行，沒帶帳號資訊下來，勾1，不驗EDDA
    //  * pay_way_type = 4 : 自行，有帶帳號資訊下來，勾1，不驗EDDA，帳號資訊帶到第一項內變數
    //  * pay_way_type = 2 : 他行，有帶帳號資訊下來，如果驗證EDDA成功，勾2，帳號資訊帶到第二項內變數
    //  * pay_way_type = 2 : 他行，有帶帳號資訊下來，如果驗證EDDA失敗，勾3，有ACH申請資訊三張
    //  * pay_way_type = 3 : ACH已由業務自取，勾3，不顯示ACH申請資訊三張
    //  */
    // private String pay_way_type;

    // /** U.	sord_bank_no：授扣銀行代號 */
    // private String sord_bank_no;

    // /** V.	sord_bank_name：授扣銀行名稱 */
    // private String sord_bank_name;

    // /** W.	sord_acct_no：授扣帳號 */
    // private String sord_acct_no;

    // /** X.	apy_fee_kind：手續費收取方式(契約條款) */
    // private String apy_fee_kind;

    // /** Y.	pen_fee_kind：提前清償違約金(契約條款) */
    // private String pen_fee_kind;

    // /** Z.	project_code：專案別代號 */
    // private String project_code;

    // /** AA.	email_address：EMAIL */
    // private String email_address;

    // /** BB.	apply_credit_line：申請額度 */
    // private String apply_credit_line;

    // /** CC.	branch_id：簽收分行 */
    // private String branch_id;

    // /** DD.	member_no：會員號 */
    // private String member_no;

    // /** EE.	sum_rate：年費用百分率 */
    // private String sum_rate;

    // /** FF.	pay_date：撥款日期(YYYYMMDD) */
    // private String pay_date;

    // /** GG.	pay_cashcard_num：放款連結帳戶 */
    // private String pay_cashcard_num;

    // /** HH.	sign_flg：是否親簽註記 */
    // private String sign_flg;

    // /** II.	house_city_code：住宅區域 */
    // private String house_city_code;

    // /** JJ.	house_address：住宅地址 */
    // private String house_address;

    // /** KK.	corp_city_code：公司區域 */
    // private String corp_city_code;

    // /** LL.	corp_address：公司地址 */
    // private String corp_address;

    // /** MM.	mobile_tel：手機號碼 */
    // private String mobile_tel;

    // /** NN.	case_no_dgt：數位案件編號 */
    // private String case_no_dgt;

    // /** OO.	gmcard_no：案件序號 */
    // private String gmcard_no;

    // /** PP.	MBC_flg：是否為mbc案件 */
    // private String MBC_flg;

    // /** QQ.	customer_name：客戶姓名(戶名+羅馬拼音) */
    // private String customer_name;

    // /** RR.	APSSeqNo：契約書流水編號(YYYYMMDD + 5碼流水號) */
    // private String APSSeqNo;

    // /** SS.	rate_desc_type：利率種類代號 */
    // private String rate_desc_type;

    // /** TT.	rpl_pay_desc：借款之動用及交付方式(契約條款) */
    // private String rpl_pay_desc;

    // /** UU.	fast_flg：快速件註記(Y/N) */
    // private String fast_flg;

    /** VV.	input_mode：自動立約鍵機註記(Y/N)
     * N 時撥款資訊頁僅顯示電文中撥款/繳款資訊內容(並鎖定)
     * Y 時撥款資訊頁讓使用者自行填寫
     */
    private String input_mode;
}