package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CheckHaveFinpfReqDto {

    /** (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string) */
    private String CASE_NO;

    /** (2)	CUST_TYPE：客戶類型(01-PL、02-GM、03-RPL、04-CC)，(string) */
    private String CUST_TYPE;

    /** (3)	IMG_TYPE：影像類別,1.申請書 2.身份證 3.財力證明 4.其他，(string) */
    private String IMG_TYPE;


}
