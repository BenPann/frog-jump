package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EOP_ApplyItem {

    private String UniqId;   
    private String ItemId;  
    private String Comment;    
}