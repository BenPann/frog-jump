package com.kgi.eopend3.common.dto.respone;

public class NCCCResp {
	
	int result = -1 ;
	String message = "";
	String statusCode = "";
	String approveCode = "";

    
    private String ncccCheckCode = "";    
    private String ncccMessage = "";
    private Integer errorCount ;
    private Integer ncccErrorCount ;
    private Integer pcode2566ErrorCount ;

    
    
    public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getApproveCode() {
		return approveCode;
	}

	public void setApproveCode(String approveCode) {
		this.approveCode = approveCode;
	}

	public String getNcccCheckCode() {
        return ncccCheckCode;
    }

    public void setNcccCheckCode(String ncccCheckCode) {
        this.ncccCheckCode = ncccCheckCode;
    }

    public String getNcccMessage() {
        return ncccMessage;
    }

    public void setNcccMessage(String ncccMessage) {
        this.ncccMessage = ncccMessage;
    }

    public Integer getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(Integer errorCount) {
        this.errorCount = errorCount;
    }

    public Integer getNcccErrorCount() {
        return ncccErrorCount;
    }

    public void setNcccErrorCount(Integer ncccErrorCount) {
        this.ncccErrorCount = ncccErrorCount;
    }

    public Integer getPcode2566ErrorCount() {
        return pcode2566ErrorCount;
    }

    public void setPcode2566ErrorCount(Integer pcode2566ErrorCount) {
        this.pcode2566ErrorCount = pcode2566ErrorCount;
    }

}