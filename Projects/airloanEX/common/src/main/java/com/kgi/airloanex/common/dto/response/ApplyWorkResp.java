package com.kgi.airloanex.common.dto.response;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApplyWorkResp {
    /** Y/N */
    private String hasOldIdImg;
    /** Y/N */
    private String hasOldFinaImg;
}
