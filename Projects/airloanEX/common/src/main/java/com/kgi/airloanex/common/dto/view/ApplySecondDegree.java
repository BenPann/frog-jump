package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplySecondDegree {
    private String relationship;
    private String relationshipName;
    private String relationshipId;
}
