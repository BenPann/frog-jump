package com.kgi.airloanex.common.dto.db;

public class QR_PromoCase {
    private String UniqId = "";
    private String UniqType = "";
    private String TransDepart = "";
    private String TransMember = "";
    private String PromoDepart = "";
    private String PromoMember = "";
    private String PromoDepart1 = "";
    private String PromoMember1 = "";
    private String PromoDepart2 = "";
    private String PromoMember2 = "";

    public QR_PromoCase() {
    }

    public QR_PromoCase(String uniqId) {
        this.setUniqId(uniqId);
    }

	public String getUniqId() {
		return UniqId;
	}

	public void setUniqId(String uniqId) {
		UniqId = uniqId;
	}

	public String getUniqType() {
		return UniqType;
	}

	public void setUniqType(String uniqType) {
		UniqType = uniqType;
	}

	public String getTransDepart() {
		return TransDepart;
	}

	public void setTransDepart(String transDepart) {
		TransDepart = transDepart;
	}

	public String getTransMember() {
		return TransMember;
	}

	public void setTransMember(String transMember) {
		TransMember = transMember;
	}

	public String getPromoDepart() {
		return PromoDepart;
	}

	public void setPromoDepart(String promoDepart) {
		PromoDepart = promoDepart;
	}

	public String getPromoMember() {
		return PromoMember;
	}

	public void setPromoMember(String promoMember) {
		PromoMember = promoMember;
	}

	public String getPromoDepart1() {
		return PromoDepart1;
	}

	public void setPromoDepart1(String promoDepart1) {
		PromoDepart1 = promoDepart1;
	}

	public String getPromoMember1() {
		return PromoMember1;
	}

	public void setPromoMember1(String promoMember1) {
		PromoMember1 = promoMember1;
	}

	public String getPromoDepart2() {
		return PromoDepart2;
	}

	public void setPromoDepart2(String promoDepart2) {
		PromoDepart2 = promoDepart2;
	}

	public String getPromoMember2() {
		return PromoMember2;
	}

	public void setPromoMember2(String promoMember2) {
		PromoMember2 = promoMember2;
	}
}