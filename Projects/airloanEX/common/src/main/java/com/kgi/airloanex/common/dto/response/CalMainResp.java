package com.kgi.airloanex.common.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CalMainResp {
    private String CASE_NO;
    private String EXP_NO;
    private String CREDIT_LINE;
    private String INTEREST_RATE;
}
