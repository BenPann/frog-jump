package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "StatusCode" })
public class CRD00EC5500Status {
    @XmlElement(name = "StatusCode")
    private String StatusCode;

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String statusCode) {
        StatusCode = statusCode;
    }

    @Override
    public String toString() {
        return "CRD00EC5500Status [StatusCode=" + StatusCode + "]";
    }
   
}