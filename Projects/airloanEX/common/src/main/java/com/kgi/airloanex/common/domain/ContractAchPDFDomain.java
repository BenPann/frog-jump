package com.kgi.airloanex.common.domain;

import java.util.LinkedHashMap;
import java.util.Map;

import com.kgi.airloanex.common.dto.db.ContractMainActInfo;

import org.apache.commons.lang3.StringUtils;

public class ContractAchPDFDomain {
    
    private Map<String, String> map = new LinkedHashMap<String, String>();

    public void make(
        ContractMainActInfo contractMainActInfo,
        String sendAddress) {

        // Charles:僅留"銀行"前的字串，例如："台灣銀行" 轉為 "台灣" 2020-12-29 demand by Ben
        String strEddaBankName = StringUtils.trimToEmpty(contractMainActInfo.getEddaBankName());
        if (StringUtils.containsAny(strEddaBankName, "銀行")) {
            strEddaBankName = StringUtils.substringBefore(strEddaBankName, "銀行");
        } else if (StringUtils.containsAny(strEddaBankName, "商銀")) {
            strEddaBankName = StringUtils.substringBefore(strEddaBankName, "商銀");
        }

        String _square = "<span style='font-size:2.4em; line-height:";
        String _solid = "px;'>■</span>";
        String _Hollow = "px;'>□</span>";

        // ACH_HIDE_[Y/N]
        if (StringUtils.equals("1", contractMainActInfo.getEddaStatus())) {
            map.put("%edda_result%", "Y");
        } else {
            map.put("%edda_result%", "N");
        }

        // 新申請自動扣繳
        map.put("%new_app_auto%", _square + "10" + _solid);
        // 身分證上面已帶入
        // 行動電話 authPhone
        map.put("%auth_phone%", StringUtils.trimToEmpty(contractMainActInfo.getAuthPhone()));

        char[] eddaBankIdAry = new char[0];
        if (contractMainActInfo.getEddaBankId() != null) {
            eddaBankIdAry = contractMainActInfo.getEddaBankId().toCharArray();
        }

        char[] eddaAccountNo = new char[0];
        if (contractMainActInfo.getEddaAccountNo() != null) {
            eddaAccountNo = contractMainActInfo.getEddaAccountNo().toCharArray();
        }

        final int eddaBankIdLength = 3; // 銀行代碼長度 = 3
        final int eddaAccountNoLength = 16; // 銀行帳號長度=16

        if(StringUtils.equals("700", contractMainActInfo.getEddaBankId())) {
            map.put("%edda_bank%", _square + "12" + _Hollow);
            map.put("%edda_post%", _square + "12" + _solid);
            map.put("%edda_result_Edda_Bank_Name%",  "");// EDDA銀行名稱

            for (int i = 0; i < eddaBankIdLength; i++) {
                if (i < eddaBankIdAry.length) {
                    map.put("%eddaBankId_".concat(String.valueOf(i+1)).concat("%"), "");
                }
            }

            for (int i = 0; i < eddaAccountNoLength; i++) {
                if (i < eddaAccountNo.length) {
                    map.put("%eddaAccountNo_postOffice_".concat(String.valueOf(i+1)).concat("%"), String.valueOf(eddaAccountNo[i]));
                    map.put("%eddaAccountNo_".concat(String.valueOf(i+1)).concat("%"), "");
                } else {
                    map.put("%eddaAccountNo_postOffice_".concat(String.valueOf(i+1)).concat("%"), "");
                    map.put("%eddaAccountNo_".concat(String.valueOf(i+1)).concat("%"), "");
                }
            }
        } else {
            map.put("%edda_bank%", _square + "12" + _solid);
            map.put("%edda_post%", _square + "12" + _Hollow);
            map.put("%edda_result_Edda_Bank_Name%",  strEddaBankName);// EDDA銀行名稱
            
            for (int i = 0; i < eddaBankIdLength; i++) {
                if (i < eddaBankIdAry.length) {
                    map.put("%eddaBankId_".concat(String.valueOf(i+1)).concat("%"), String.valueOf(eddaBankIdAry[i]));
                } else {
                    map.put("%eddaBankId_".concat(String.valueOf(i+1)).concat("%"), "");
                }
            }

            for (int i = 0; i < eddaAccountNoLength; i++) {
                if (i < eddaAccountNo.length) {
                    map.put("%eddaAccountNo_".concat(String.valueOf(i+1)).concat("%"), String.valueOf(eddaAccountNo[i]));
                    map.put("%eddaAccountNo_postOffice_".concat(String.valueOf(i+1)).concat("%"), "");
                } else {
                    map.put("%eddaAccountNo_".concat(String.valueOf(i+1)).concat("%"), "");
                    map.put("%eddaAccountNo_postOffice_".concat(String.valueOf(i+1)).concat("%"), "");
                }
            }
        }

        // %send_address%   從ContractPDF取得
        map.put("%send_address%", sendAddress);
    }

    public Map<String, String> getMap() {
        return map;
    }
    public void setMap(Map<String, String> map) {
        this.map = map;
    }
}
