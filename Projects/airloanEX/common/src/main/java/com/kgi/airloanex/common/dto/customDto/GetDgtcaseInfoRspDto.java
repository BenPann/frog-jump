package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetDgtcaseInfoRspDto {

    /**
     * (1)	CHECK_CODE：
     * A.	檢核資料錯誤(空白代表沒有錯誤)
     * B.	如果有多個錯誤用『;』區隔,例如01;02
     * C.	『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
     * D.	錯誤代碼對照表:
     * 代碼  說明
     * ============================
     * 01	   案件編號格式錯誤
     * 02	   案件編號不存在
     * 99	  非預期性系統異常
     */
    private String CHECK_CODE;

    /**
     * (2)	ERR_MSG：
     * 當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
     */
    private String ERR_MSG;

    /** (3)	RESULT：檢核結果1.拒貸2.線上3.線下4.系統發生錯誤，CHAR(1) */
    private String RESULT;
    
    /** (4)	RESULT_1：拒貸代碼，VARCHAR(100) */
    private String RESULT_1;
    
    /** (5)	RESULT_2：線下代碼，VARCHAR(100) */
    private String RESULT_2;
    
    /** (6)	IS_REJECT_IN：行內拒貸(Y/N)，CHAR(1) */
    private String IS_REJECT_IN;
    
    /** (7)	IS_REJECT_OUT：行外拒貸(Y/N)，CHAR(1) */
    private String IS_REJECT_OUT;
    
    /** (8)	IS_IMPORT_CUST：是否為重要客戶(Y/N)，CHAR(1) */
    private String IS_IMPORT_CUST;
    
    /** (9)	PROJECT_CODE：專案代號，VARCHAR(10) */
    private String PROJECT_CODE;
    
    /** (10)	CREDIT_LINE：建議額度(單位萬)，INT */
    private String CREDIT_LINE;
    
    /** (11)	INTEREST_RATE：建議利率，float */
    private String INTEREST_RATE;
    
    /** (12)	RANK：RANK，VARCHAR(2) */
    private String RANK;
    
    /** (13)	SCORE：分數，float */
    private String SCORE;
    
    /** (14)	CASE_STATUS：案件狀態(1.正常執行完畢2.系統發生錯誤,空字串:系統逾時)，CHAR(1) */
    private String CASE_STATUS;
    
    /** (15)	APS_STATUS：案件審核狀態(1.審核中2.已核准3.人工拒貸4.可立約5.已立約6.30天系統拒貸) ，CHAR(1) */
    private String APS_STATUS;
    
    /** (16)	SHOW_SCORE：是否顯示額度利率(Y/N) ，CHAR(1) */
    private String SHOW_SCORE;

    /**  */
    private String SHOW_SCORE_RES;
    
    /** (17)	GMCARD_NO：APS系統案件編號，INT */
    private String GMCARD_NO;
    
    /** (18)	DGT_TYPE：數位身分別(1.數位小額2.數位優質3.數位一般)，CHAR(1) */
    private String DGT_TYPE;

    /** (19)	CASE_FLAG_STEP2：第2步驟是否已完成(Y/N) */
    private String CASE_FLAG_STEP2;

    /** (20)	CASE_FLAG_STEP3：第3步驟是否已完成(Y/N) */
    private String CASE_FLAG_STEP3;
}
