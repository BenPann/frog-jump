package com.kgi.airloanex.common.dto.db;

public class MailHistory {

    private String Serial = "";
    private String UniqId = "";
    private String MailType = "";
    private String Status = "";
    private String ErrorMessage = "";
    private String Title = "";
    private String content = "";
    private String TemplateId = "";
    private byte[] Attatchment = new byte[]{};
    private String EMailAddress = "";
    private String CreateTime = "";
    private String UpdateTime = "";


    public static MailHistory getInstance(){
        return new MailHistory();
    }

    public String getSerial() {
        return Serial;
    }

    public MailHistory setSerial(String serial) {
        Serial = serial;
        return this;
    }

    public String getUniqId() {
        return UniqId;
    }

    public MailHistory setUniqId(String uniqId) {
        UniqId = uniqId;
        return this;
    }

    public String getMailType() {
        return MailType;
    }

    public MailHistory setMailType(String mailType) {
        MailType = mailType;
        return this;
    }

    public String getStatus() {
        return Status;
    }

    public MailHistory setStatus(String status) {
        Status = status;
        return this;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public MailHistory setErrorMessage(String errorMessage) {
        ErrorMessage = errorMessage;
        return this;
    }

    public String getTitle() {
        return Title;
    }

    public MailHistory setTitle(String title) {
        Title = title;
        return this;
    }

    public String getContent() {
        return content;
    }

    public MailHistory setContent(String content) {
        this.content = content;
        return this;
    }

    public String getTemplateId() {
        return TemplateId;
    }

    public MailHistory setTemplateId(String templateId) {
        TemplateId = templateId;
        return this;
    }

    public byte[] getAttatchment() {
        return Attatchment;
    }

    public MailHistory setAttatchment(byte[] attatchment) {
        Attatchment = attatchment;
        return this;
    }

    public String getEMailAddress() {
        return EMailAddress;
    }

    public MailHistory setEMailAddress(String EMailAddress) {
        this.EMailAddress = EMailAddress;
        return this;
    }

    public String getCreateTime() {
        return CreateTime;
    }

    public MailHistory setCreateTime(String createTime) {
        CreateTime = createTime;
        return this;
    }

    public String getUpdateTime() {
        return UpdateTime;
    }





}
