package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.Setter;

/**
 * 簡訊及email待發送資料(由其他服務發送)
 */
@Getter
@Setter
public class JobAlert {
    
    /** SMS/MAILHUNTER */
    private String AlertType;

	/**  */
    private String Clazz;

	/**  */
    private String UniqId;

	/**  */
    private String BillDepart;

	/**  */
    private String Phone;

	/**  */
    private String MsgContent;

	/**  */
    private String Content001;

	/**  */
    private String Content002;

	/**  */
    private String CreateTime;

	/**  */
    private String OrderTime;

	/**  */
    private String SendTime;

	/**  */
    private String SendStatus;

}
