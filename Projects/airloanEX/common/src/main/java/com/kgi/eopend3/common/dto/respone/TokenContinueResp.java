package com.kgi.eopend3.common.dto.respone;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenContinueResp extends TokenResp {

    private String nexttime;
    private String clToken;

}