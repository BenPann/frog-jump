package com.kgi.airloanex.common.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CaseDataResp {
    
        /** 案件編號(數位信貸)	範例: CASEYYYYMMDD000001 */
        private String CaseNo;

        /** 案件編號(APS)	APS 的案件編號 (ADD_NEW_CASE 產的) */
        private String CaseNoWeb;
    
        /** APS 真正的案件編號	APS 起案之後會來更新 */
        private String CaseNoAps;
    
        /** 現金卡系統案件編號	 */
        private String GmCardNo;
    
        /** 身分證字號或護照號碼	 */
        private String Idno;
    
        /** 案件狀態	 */
        private String Status;
    
        /** APS 傳來的案件狀態	 */
        private String ApsStatus;
    
        /** 是否預核	1:預核 2:非預核 */
        private String IsPreAudit;
    
        /** 產品名稱	 */
        private String ProductId;
    
        /** 職業類別	 */
        private String Occupation;
    
        /** 姓名	以下欄位皆為對應 WEB API 欄位 */
        private String customer_name;
    
        /** 生日	 */
        private String birthday;
    
        /** 性別	1:男 2:女 */
        private String gender;
    
        /** 居住地行政區代碼	 */
        private String house_city_code;
    
        /** 居住地址	 */
        private String house_address;
    
        /** 住宅電話區碼	 */
        private String house_tel_area;
    
        /** 住宅電話	 */
        private String house_tel;
    
        /** 教育程度	 */
        private String education;
    
        /** 婚姻狀況	 */
        private String marriage;
    
        /** 公司名稱	 */
        private String corp_name;
    
        /** 公司地址行政區代碼	 */
        private String corp_city_code;
    
        /** 公司地址	 */
        private String corp_address;
    
        /** 公司電話區碼	 */
        private String corp_tel_area;
    
        /** 公司電話	 */
        private String corp_tel;
    
        /** 公司分機	畫面加上此欄位 */
        private String corp_tel_exten;
    
        /** 公司類別	 */
        private String corp_class;
    
        /** 行業別	帶空字串 */
        private String corp_type;
    
        /** 職務名稱	 */
        private String title;
    
        /** 到職日	 */
        private String on_board_date;
    
        /** 年收入	(單位：萬元) */
        private String yearly_income;
    
        /** 不動產狀況	(1.本人所有 2.配偶所有 3.家族所有 4.無) */
        private String estate_type;
    
        /** 貸款申請金額	(單位：萬元) */
        private String p_apy_amount;
    
        /** 有效存於行內行動電話	 */
        private String mobile_tel;
    
        /** 貸款期間	 */
        private String p_period;
    
        /** 資金用途	 */
        private String p_purpose;
    
        /** 資金用途其他	 */
        private String p_purpose_name;
    
        /** EMAIL 地址	 */
        private String email_address;
    
        /** 使用者 IP	 */
        private String ip_address;
    
        /** 專案代號	 */
        private String prj_code;
    
        /** 預審案件編號	 */
        private String case_no_est;
    
        /** 客戶勾選與我聯絡	1:不勾選 2:勾選 */
        private String ContactMe;
    
        /** 是否送給 TM	0:還沒 1:已送出 */
        private String ContactMeFlag;
    
        /** 圖檔是否上傳影像	0:還沒 1:已送出 */
        private String AddPhotoFlag;
    
        /** 職業類別 Aps 檢核結果	 */
        private String OccupatioFlag;
    
        /** 是否為重要客戶	Y:重要客戶 N:非重要客戶 */
        private String ImportantCust;
    
        /** AML 回傳結果	 */
        private String AMLFlag;
    
        /** 更新時間	 */
        private String UTime;
    
        /** 使用者類別	0: 新戶 1:信用卡戶 2:存款戶 3:純貸款戶 */
        private String UserType;
    
        /** 戶藉資料 */
        private String ResAddrZipCode;
    
        /** 戶藉資料 */
        private String ResAddr;
    
        /** 戶藉資料 */
        private String ResTelArea;
    
        /** 戶藉資料 */
        private String ResTel;

        /** 23. 客戶填入之行動電話 */
        private String phone;

        /** 24. DATA_TYPE 客戶類型(1.信用卡2.現金卡3.存款4.貸款) 目的是給apply verify 簡訊驗證顯示用戶於本行有效行動電話之服務名稱 */
        private String stempDataType;

        /** 他行帳戶驗身的手機 */
        private String pcode2566_phone;
}
