package com.kgi.eopend3.common.dto.db;

public class ED3_CLPayerLog {

	private int PolicySerial ;
	private String PayerStatus;
	private String PayerFailMsg = "";
	private String CreateTime ;
	public int getPolicySerial() {
		return PolicySerial;
	}
	public void setPolicySerial(int policySerial) {
		PolicySerial = policySerial;
	}
	public String getPayerStatus() {
		return PayerStatus;
	}
	public void setPayerStatus(String payersStatus) {
		PayerStatus = payersStatus;
	}
	public String getPayerFailMsg() {
		return PayerFailMsg;
	}
	public void setPayerFailMsg(String payerFailMsg) {
		PayerFailMsg = payerFailMsg;
	}
	public String getCreateTime() {
		return CreateTime;
	}
	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}
	

	
	
	
}