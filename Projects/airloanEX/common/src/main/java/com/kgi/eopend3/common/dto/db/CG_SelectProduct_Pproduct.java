package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CG_SelectProduct_Pproduct {

    String CGProductID = "";
    String PProductType= "";
    String ChannelId= "";
    String Select_Default= "";
    String Required= "";
    String SORT= "";
    
}