package com.kgi.airloanex.common.dto.db;

public class MemoData {
    private String uniqId = "";
    private String uniqType = "";
    private String memoName = "";
    private String memo = "";

    public MemoData(){

    }

    public MemoData(String uniqId, String uniqType,String memoName) {
        this.uniqId = uniqId;
        this.uniqType = uniqType;
        this.memoName = memoName;
    }

	public String getUniqId() {
		return uniqId;
	}

	public void setUniqId(String uniqId) {
		this.uniqId = uniqId;
	}

	public String getUniqType() {
		return uniqType;
	}

	public void setUniqType(String uniqType) {
		this.uniqType = uniqType;
	}

	public String getMemoName() {
		return memoName;
	}

	public void setMemoName(String memoName) {
		this.memoName = memoName;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}
}
