package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement(name = "MsgRs", namespace = "http://www.cosmos.com.tw/esb")
@XmlAccessorType(XmlAccessType.FIELD)
public class BNS08508100MsgRs {

    @XmlElement(name="Header")
    private BNS08508100Header bNS08508100Header;

    @XmlElement(name="SvcRs")
    private BNS08508100SvcRs bNS08508100SvcRs;

    public BNS08508100SvcRs getbNS08508100SvcRs() {
        return bNS08508100SvcRs;
    }

    public void setbNS08508100SvcRs(BNS08508100SvcRs bNS08508100SvcRs) {
        this.bNS08508100SvcRs = bNS08508100SvcRs;
    }

    public BNS08508100Header getbNS08508100Header() {
        return bNS08508100Header;
    }

    public void setbNS08508100Header(BNS08508100Header bNS08508100Header) {
        this.bNS08508100Header = bNS08508100Header;
    }

    @Override
    public String toString() {
        return "BNS08508100MsgRs [bNS08508100Header=" + bNS08508100Header + ", bNS08508100SvcRs=" + bNS08508100SvcRs + "]";
    }
   
}