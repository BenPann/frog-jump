package com.kgi.airloanex.common.dto.customDto;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CheckNullAndEmpty
@AllArgsConstructor
public class QryQaReqDto {
    
    /** (1)	CASE_NO_WEB：網路案件編號，CHAR(11) */
    private String CASE_NO_WEB;

    /** (2)	IP_ADDRESS：IPAddress，VARCHAR(15) */
    private String IP_ADDRESS;
}