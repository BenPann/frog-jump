package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class APIMInfoReqDto {

    private String personId;

    private String applyYyymmdd;

    private String applyCode;
    
    private String issueSiteId;
}
