package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QryQaRspDtoQaData {

    // A.	題目資料
// 欄位名稱		欄位說明
// =======================================================
// var_code      版本編號
// quest_no		問題編號
// quest_desc		問題說明
// quest_kind		問題類型(1.選擇題2.問答題)
// quest_kind2	問題型態(1.文字2.數字)
// quest_length	資料最大長度
// quest_type    問題分類(1.借款人信用2.借款人財力3.借款人負債)
private String var_code;
private String quest_no;
private String quest_desc;
private String quest_kind;
private String quest_kind2;
private String quest_length;
private String quest_type;
}
