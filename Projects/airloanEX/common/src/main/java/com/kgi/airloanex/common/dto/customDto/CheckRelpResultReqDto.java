package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CheckRelpResultReqDto {
    
    /** (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string) */
    private String CASE_NO;
    
    /** (2)	USER：查詢者，(string) */
    private String USER;
    

}
