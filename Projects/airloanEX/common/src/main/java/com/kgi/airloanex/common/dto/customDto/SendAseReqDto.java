package com.kgi.airloanex.common.dto.customDto;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CheckNullAndEmpty
@AllArgsConstructor
public class SendAseReqDto {
    
    // (1)	CASE_NO：案件編號(功能2.1取得的編號)，CHAR(11)
    // (2)	QUEST_LIST：問題代號清單，問題編號用『;』區隔，例如1;2;3
    // (3)	ANSWER_LIST：問題答案清單，答案用『;』區隔，例如1;2;1
    private String CASE_NO;
    private String QUEST_LIST;
    private String ANSWER_LIST;
}