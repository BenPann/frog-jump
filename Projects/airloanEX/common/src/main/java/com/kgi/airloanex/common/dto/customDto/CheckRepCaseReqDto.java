package com.kgi.airloanex.common.dto.customDto;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CheckNullAndEmpty
@AllArgsConstructor
public class CheckRepCaseReqDto {

    // CUSTOMER_ID：客戶ID，VARCHAR(11)
    private String CUSTOMER_ID;
    // DATA_TYPE：資料類型，1.預審2.一般信貸3.一般e貸寶，CHAR(1)
    private String DATA_TYPE;
}
