package com.kgi.eopend3.common.dto.customDto;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class ChinalifeCaseStsRq {

	private String msgId = "";
	private String token = "";
	private String msgSendDt = "";


	@SerializedName(value="caseStsInfo")
	private ChinalifeCaseStsInfo caseStsInfo ;


	public String getMsgId() {
		return msgId;
	}


	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}


	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public String getMsgSendDt() {
		return msgSendDt;
	}


	public void setMsgSendDt(String msgSendDt) {
		this.msgSendDt = msgSendDt;
	}


	public ChinalifeCaseStsInfo getCaseStsInfo() {
		return caseStsInfo;
	}


	public void setCaseStsInfo(ChinalifeCaseStsInfo caseStsInfo) {
		this.caseStsInfo = caseStsInfo;
	}
	
	
	public String toJsonString() {
		return new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(this) ;
	}
	
	
}
