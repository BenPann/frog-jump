package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UpdateApsFastCaseReqDto {
    
    /** (1)	CASE_NO：案件編號(2.23功能取得的案件編號)，(string) */
    private String CASE_NO;
    
    /** (2)	ERR_FLAG：異常件註記(Y/N)，(string) */
    private String ERR_FLAG;
    
    /** (3)	IMG_READY_FLAG：影像系統已索引註記(Y/N)，(string) */
    private String IMG_READY_FLAG;
    
    /** (4)	DOC_FLAG：申請書/ID/財力完整者(Y/N)，(string) */
    private String DOC_FLAG;
    

}
