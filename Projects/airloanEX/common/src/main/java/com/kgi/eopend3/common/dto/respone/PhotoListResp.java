package com.kgi.eopend3.common.dto.respone;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhotoListResp {

    private String waterMarkImage = "";
    private String subSerial = "";
    private String sType = "";
    private String pType = "";

}