package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeTriggerView {
    
    private String productType;
    private String flowType;
    private String pageName;
    private String subPageName;
}
