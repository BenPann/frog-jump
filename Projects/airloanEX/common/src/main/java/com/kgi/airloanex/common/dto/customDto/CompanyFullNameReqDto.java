package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CompanyFullNameReqDto {
    
    /** 名稱 */
    private String ShortName;

    /** 查詢回傳筆數 */
    private String Take;
}
