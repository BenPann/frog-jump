package com.kgi.airloanex.common.dto.response;

import java.util.ArrayList;
import java.util.List;

import com.kgi.airloanex.common.PlContractConst.RouteGo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CaseInitResp {

    private String token;
    private RouteGo routeGo;
    private InitBankInfo initBankInfo;
    private List<RateData> rateDatas = new ArrayList<>();
}