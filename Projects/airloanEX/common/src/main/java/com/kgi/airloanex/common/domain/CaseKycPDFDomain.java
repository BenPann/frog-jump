package com.kgi.airloanex.common.domain;

/**
 * 產製PDF之前需要先打完下列電文，
 * 1. SEND_JCIC
 * 2. CHECK_Z07_RESULT
 * 3. GET_KYC_DEBT_INFO
 * 4. GET_AML_DATA
 * 
 * @see CasePDF.java
 */
public class CaseKycPDFDomain {

    /** 數位申辦平台案件編號：[UniqId] */
    private String UniqId;

    /** 一、KYC 檢核作業：區分 1.基本資料／2.收入狀況／3.負債狀況 查詢日期 */
    private String Query_Date;
    
    /** (1)姓名 */
    private String Apply_user_name;
    
    /** (2)ID */
    private String Apply_user_idno; // FIXME loan_kyc.html
    
    /** (3)年齡 */
    private String Apply_user_age;
    
    /** (4)工作年資 */
    private String Apply_user_job_tenure; // FIXME loan_kyc.html
    
    /** (5)工作地點 */
    private String Apply_user_company_address;
    
    /** (6)通訊地址 */
    private String Apply_user_house_address; // FIXME loan_kyc.html
    
    /** (7)推廣單位 */
    private String Promo_Depart;
    
    /** (8)跨區案件 */
    private String Cross_zone_case; // FIXME loan_kyc.html
    
    /** (1)財力證明 */
    private String Finacial_type; // finpf
    
    /** (2)年收入 */
    private String Apply_user_annual_income;
    
    /** (3)平均月收入 */
    private String Apply_user_avg_month_income;
    
    /** 二、防制洗錢及打擊資恐檢核作業(AML) 查詢日期 */
    private String AML_Query_Date; // FIXME loan_kyc.html
    
    /** (1)身分認證方式 */
    private String Auth_Type;
    
    /** (2)來源 IP */
    private String ip_address;
    
    /** (3)時間 */
    private String Apply_date; // FIXME loan_kyc.html

    /** (1)房貸 */
    private String Apply_user_MORTAGAGE;

    /** (2)車貸 */
    private String Apply_user_CAR_LOAN;

    /** (3)信貸 */
    private String Apply_user_PLOAN;

    /** (4)信用卡 */
    private String Apply_user_CREDIT_CARD;

    /** (5)現金卡 */
    private String Apply_user_CASH_CARD;
    
}
