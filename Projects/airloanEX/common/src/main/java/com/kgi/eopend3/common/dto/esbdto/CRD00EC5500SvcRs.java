package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SvcRs", propOrder = {"detail"})
public class CRD00EC5500SvcRs {


    @XmlElement(name = "DETAIL")
    private List<CRD00EC5500Detail> detail;

    public List<CRD00EC5500Detail> getDetail() {
        return detail;
    }

    public void setDetail(List<CRD00EC5500Detail> detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "CRD00EC5500SvcRs [detail=" + detail + "]";
    }

       
}