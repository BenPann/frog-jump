package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CG_SelectProductList {

    String CGProductID= "";
    String CGProductName= "";
    String CGProductTitle= "";
    String CGProductSubTitle= "";
    String CGProductDesc= "";
    
}