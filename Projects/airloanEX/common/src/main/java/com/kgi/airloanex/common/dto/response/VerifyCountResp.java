package com.kgi.airloanex.common.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VerifyCountResp {

    private Integer authErrCount;
    private Integer signErrCount;
    private Integer eddaErrCount;
}