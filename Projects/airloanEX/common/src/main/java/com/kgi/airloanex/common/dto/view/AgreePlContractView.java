package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgreePlContractView {
    private String email;
    private String reviewDate;
    private String authInfo;
    private String payMentDay;
}