package com.kgi.eopend3.common.dto.respone;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DropDownResp {

    private String dataKey = "";
    private String dataName = "";
    
}