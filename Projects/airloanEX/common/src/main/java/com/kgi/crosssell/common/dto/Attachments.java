package com.kgi.crosssell.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Attachments {
    /** 附件類型 */
    private String fileType;

    /** 附件副檔名 */
    private String fileExtension;

    /** 附件資料 */
    private String fileData;
    
}
