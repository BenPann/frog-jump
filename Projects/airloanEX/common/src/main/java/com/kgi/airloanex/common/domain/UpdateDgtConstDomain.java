package com.kgi.airloanex.common.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDto;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoConstData;
import com.kgi.airloanex.common.dto.db.ContractMain;
import com.kgi.airloanex.common.dto.db.ContractMainActInfo;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.lang3.StringUtils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateDgtConstDomain {

    /** (1) CASE_NO：案件編號 */
    private String CASE_NO;

    /** (2) CITY_CODE：郵遞區號，NVARCHAR(3) */
    private String CITY_CODE;

    /** (3) ADDRESS：地址，NVARCHAR(150) */
    private String ADDRESS;

    /** (4) pay_date：撥款日期(YYYYMMDD) */
    private String pay_date;

    /** (5) pay_desc_type：撥款方式 (1-(一)撥入立約人、2-(二)撥入立約人開立於、3-(三)撥付代償立約人) */
    private String pay_desc_type;

    /** (6) pay_bank：撥款銀行名稱 */
    private String pay_bank;

    /** (7) pay_bank_no：撥款銀行代號 */
    private String pay_bank_no;

    /** (8) pay_bank_no：撥款銀行分行代號 */
    private String pay_bank_branchId;

    /** (9) pay_account：撥款帳號 */
    private String pay_account;

    /**
     * (10) pay_kind_type：還款方式(1-(一)自撥款日起、2-
     * (二)自撥款日起、3-(三)寬限期自撥款日起算、4-(五)其他約定方式：、5-(四)員工貸款約定：)
     */
    private String pay_kind_type;

    /** (11) pay_kind_day：繳款日(D) */
    private String pay_kind_day;

    /** (12) pay_kind：還款方式(契約條款) */
    private String pay_kind;

    /** (13) pay_way_type：還款授權扣款方式(1-本行、2-eDDA、3-ACH) */
    private String pay_way_type;

    /** (14) sord_bank_no：授扣銀行代號 */
    private String sord_bank_no;

    /** (15) sord_bank_name：授扣銀行名稱 */
    private String sord_bank_name;

    /** (16) sord_acct_no：授扣帳號 */
    private String sord_acct_no;

    /** (17) edda_check_time：EDDA認證時間(YYYYMMDDHHMMSS) */
    private String edda_check_time;

    /** (18) const_sign_no：契約簽署版本(YYYYMMDD) */
    private String const_sign_no;

    /** (19) APSSeqNo：契約書流水編號(YYYYMMDD + 5碼流水號) */
    private String APSSeqNo;

    /** (18)	exceed_flg：熔斷註記(N/E1/E2/E3/E4/E5/E6/E7/E8)，N-非熔斷 */
    private String exceed_flg;

    boolean input_mode_N = false;
    boolean input_mode_Y = false;
    boolean input_mode_N_pay_desc_type = false;
    boolean input_mode_Y_pay_desc_type = false;
    boolean input_mode_Y_pay_desc_type_else = false;
    boolean input_mode_N_pay_kind_type_1 = false;
    boolean input_mode_N_pay_kind_type_2 = false;
    boolean input_mode_N_productId_3 = false; // 20201118 debug 因RPL 少pay_kind_type參數可判斷 pay_kind_day帶入錯誤值至updateDGT
    boolean input_mode_Y_eq_pay_kind_day = false;
    boolean input_mode_Y_ne_pay_kind_day = false;
    boolean input_mode_N_or_Y_pay_kind_type_3_or_4_or_5 = false;  // 20201102 調整邏輯 pay_kind_type如果是3或者4或5 顯示 pay_kind資訊
    boolean isEddaPass = false;
    boolean isEddaFail = false;
    boolean input_mode_N_pay_way_type_1_isKgibank_eq_account = false;
    boolean input_mode_N_pay_way_type_4_isKgibank_ne_account = false;
    boolean input_mode_N_pay_way_type_2_isEddaPass = false;
    boolean input_mode_N_pay_way_type_2_isEddaFail = false;
    boolean input_mode_N_pay_way_type_3 = false;
    boolean input_mode_Y_isKgibank_eq_account = false;
    boolean input_mode_Y_isKgibank_ne_account = false;
    boolean input_mode_Y_notKgibank_isEddaPass = false;
    boolean input_mode_Y_notKgibank_isEddaFail = false;
    boolean input_mode_Y_isKgibank = false;
    
    /**
     * 
     * @param contractMain
     * @param actInfe
     * @param const_sign_no
     * @param finalPayDayDate yyyy-MM-dd HH:mm:ss.SSS
     */
    public void make(ContractMain contractMain, ContractMainActInfo actInfe, String const_sign_no, String finalPayDayDate) {

        GetDgtConstRspDto apsConstData = new Gson().fromJson(contractMain.getApsConstData(), GetDgtConstRspDto.class);
        GetDgtConstRspDtoConstData constData = apsConstData.getConstDataList().get(0);

        // 1. CASE_NO 案件編號
        this.setCASE_NO(contractMain.getCaseNoWeb());

        // 2. CITY_CODE 郵遞區號
        this.setCITY_CODE(constData.getHouse_city_code());

        // 3. ADDRESS 地址
        this.setADDRESS(constData.getHouse_address());

        // 4. pay_date 撥款日期(yyyyMMdd)7碼
        SimpleDateFormat bb = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date finalPayDay = null;
        try {
            finalPayDay = bb.parse(finalPayDayDate);
            this.setPay_date(DateUtil.GetDateFormatString(finalPayDay, "yyyyMMdd"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        // 5. pay_desc_type 撥款方式(1-(一)撥入立約人、2-(二)撥入立約人開立於、3-(三)撥付代償立約人)
        // 2020-09-03 Ben demands:
        // input_mode=N(客戶不能變更)時:
        // 依據立約查詢電文裡面的pay_desc_type，帶入立約完成電文的pay_desc_type
        // input_mode=Y(客戶自行輸入)時:
        // 依據畫面客戶選擇，如為自行帳號，則pay_desc_type=1，立約完成電文裡面的pay_desc_type帶入1
        // 依據畫面客戶選擇，如為他行帳號，則pay_desc_type=2，立約完成電文裡面的pay_desc_type帶入2
        this.input_mode_N = StringUtils.equals("N", constData.getInput_mode());
        this.input_mode_Y = StringUtils.equals("Y", constData.getInput_mode());
        this.input_mode_N_pay_desc_type = input_mode_N;
        this.input_mode_Y_pay_desc_type = input_mode_Y && (StringUtils.equals("0", contractMain.getPayBankOption()));
        this.input_mode_Y_pay_desc_type_else = input_mode_Y && !(StringUtils.equals("0", contractMain.getPayBankOption()));
        String pay_desc_type = "";
        if (input_mode_N_pay_desc_type) {
            pay_desc_type = constData.getPay_desc_type();
        }
        // 0:本行  1:它行  null:它行   
        if (input_mode_Y_pay_desc_type) { // 0:本行
            pay_desc_type = "1";
        }
        if (input_mode_Y_pay_desc_type_else) { // 它行
            pay_desc_type = "2";
        }
        this.setPay_desc_type(pay_desc_type);

        // 6. pay_bank 撥款銀行名稱
        this.setPay_bank(StringUtils.trimToEmpty(actInfe.getSignBankName()));

        // 7. pay_bank_no 撥款銀行代號(有值給7碼 無值給空字串)
        String pay_bank_no = "";
        if (StringUtils.isNotBlank(actInfe.getSignBankId())) {
            pay_bank_no = StringUtils.rightPad(StringUtils.trimToEmpty(actInfe.getSignBankId()) + StringUtils.trimToEmpty(actInfe.getSignBranchId()), 7, "0");
        } else {
            pay_bank_no = "";
        }
        this.setPay_bank_no(StringUtils.trimToEmpty(pay_bank_no));

        // 8. pay_bank_branchId 撥款銀行分行代號
        String pay_bank_branchId = "";
        if (StringUtils.isNotBlank(actInfe.getSignBranchId())) {
            pay_bank_branchId = StringUtils.rightPad(StringUtils.trimToEmpty(actInfe.getSignBranchId()), 4, "0");
        } else {
            pay_bank_branchId = "";
        }
        this.setPay_bank_branchId(pay_bank_branchId);

        // 9. pay_account 撥款帳號
        String pay_account = StringUtils.trimToEmpty(actInfe.getSignAccountNo());
        this.setPay_account(pay_account);
        
        // 10. pay_kind_type 還款方式(1-(一)自撥款日起、2- (二)自撥款日起、3-(三)寬限期自撥款日起算、4-(五)其他約定方式：、5-(四)員工貸款約定：)
        String finalPayDayString = DateUtil.GetDateFormatString(finalPayDay, "d"); // 撥款日1-31

        String pay_kind_type = constData.getPay_kind_type();
        String pay_kind_day = finalPayDayString; // 1-31

        this.input_mode_N_pay_kind_type_1 = input_mode_N && (StringUtils.equals("1", pay_kind_type));
        this.input_mode_N_pay_kind_type_2 = input_mode_N && (StringUtils.equals("2", pay_kind_type));
        this.input_mode_N_productId_3 = input_mode_N && (StringUtils.equals("3", contractMain.getProductId())); // 20201118 新增判斷 解決RPL沒有pay_kind_type
        this.input_mode_Y_eq_pay_kind_day = input_mode_Y && (StringUtils.equals(contractMain.getPaymentDay(), finalPayDayString));
        this.input_mode_Y_ne_pay_kind_day = input_mode_Y && !(StringUtils.equals(contractMain.getPaymentDay(), finalPayDayString));
        this.input_mode_N_or_Y_pay_kind_type_3_or_4_or_5 = StringUtils.equals("3", pay_kind_type) || StringUtils.equals("4", pay_kind_type) || StringUtils.equals("5", pay_kind_type);

        if (input_mode_N_pay_kind_type_1) {
            pay_kind_type = constData.getPay_kind_type();
            pay_kind_day = finalPayDayString;
        } 
        if (input_mode_N_pay_kind_type_2) {
            pay_kind_type = constData.getPay_kind_type();
            pay_kind_day = constData.getPay_kind_day();
        }
        if (input_mode_N_productId_3) {
            // pay_kind_type = constData.getPay_kind_type();
            pay_kind_day = constData.getPay_kind_day();
        }
        
        if (input_mode_Y_eq_pay_kind_day) { // 同今天日期：is same month-day as today
            pay_kind_day = contractMain.getPaymentDay(); // 1-31;
            pay_kind_type = "1";
        } 
        if (input_mode_Y_ne_pay_kind_day) {
            pay_kind_day = contractMain.getPaymentDay(); // 1-31;
            pay_kind_type = "2";
        }

        if (this.input_mode_N_or_Y_pay_kind_type_3_or_4_or_5) {
            pay_kind = constData.getPay_kind();
        } else {
            pay_kind = "";
        }

        // 2020-09-04 demand by Ben 69.現金卡立約電文值有誤，現金卡請將pay_kind_type、pay_kind_day帶成空白即可，撥款日也帶入錯誤，今天 9/4 卻帶成 9/7
        if (StringUtils.equals("2", contractMain.getProductId())) {
            pay_kind_type = "";
            pay_kind_day = "";
        }

        pay_kind_type = StringUtils.trimToEmpty(pay_kind_type);
        pay_kind_day = StringUtils.trimToEmpty(pay_kind_day);
        
        this.setPay_kind_type(pay_kind_type);

        // 11. pay_kind_day 繳款日
        this.setPay_kind_day(pay_kind_day); // 1-31
        
        // 13. sord_bank_no 授扣銀行代號(有值給7碼 無值給空字串)
        String sord_bank_no = "";
        if (StringUtils.isNotBlank(actInfe.getEddaBankId())) {
            sord_bank_no = StringUtils.rightPad(StringUtils.trimToEmpty(actInfe.getEddaBankId()), 7, "0");
        } else {
            sord_bank_no = "";
        }
        this.setSord_bank_no(sord_bank_no);
        
        // 14. sord_bank_name 授扣銀行名稱
        this.setSord_bank_name(StringUtils.trimToEmpty(actInfe.getEddaBankName()));

        // 15.  sord_acct_no 授扣帳號
        String sord_acct_no = StringUtils.trimToEmpty(actInfe.getEddaAccountNo());
        this.setSord_acct_no(sord_acct_no);
        
        // 16. edda_check_time：EDDA認證成功時間(YYYYMMDDHHMMSS)
        if (actInfe.getEddaTime() != null) {
            SimpleDateFormat sdfLong = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date date = null;
            try {
                date = sdfLong.parse(actInfe.getEddaTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat sdfShort = new SimpleDateFormat("yyyyMMddHHmmss");
            this.setEdda_check_time(sdfShort.format(date)); // yyyyMMddHHmmss
        } else {
            this.setEdda_check_time("");
        }

        // 17. const_sign_no：契約簽署版本(YYYYMMDD)
        this.setConst_sign_no(const_sign_no);

        // 18. APSSeqNo：契約書流水編號(YYYYMMDD + 5碼流水號)
        this.setAPSSeqNo(constData.getAPSSeqNo());

        // 18. exceed_flg：熔斷註記(N/E1/E2/E3/E4/E5/E6/E7/E8)，N-非熔斷
        this.setExceed_flg(constData.getExceed_flg());

        // 12. pay_way_type 還款授權扣款方式(1-本行、2-eDDA、3-ACH)
        String pay_way_type = constData.getPay_way_type();
        this.isEddaPass = StringUtils.equals(actInfe.getEddaStatus(), "1");
        this.isEddaFail = StringUtils.equals(actInfe.getEddaStatus(), "0");

        this.input_mode_N_pay_way_type_1_isKgibank_eq_account = 
            input_mode_N 
            && (StringUtils.equals("1", pay_way_type) 
            && isKgibank(pay_bank_no) 
            && isKgibank(sord_bank_no)
            && StringUtils.equals(pay_account, sord_acct_no));
        this.input_mode_N_pay_way_type_4_isKgibank_ne_account = 
            input_mode_N 
            && (StringUtils.equals("4", pay_way_type)); // 2020-10-12 若 pay_way_type=4 送出UPDATE_DGT_CONST時一律改為 pay_way_type=1 demand by Ben
        this.input_mode_N_pay_way_type_2_isEddaPass = 
            input_mode_N 
            && (StringUtils.equals("2", pay_way_type) && isEddaPass);
        this.input_mode_N_pay_way_type_2_isEddaFail = 
            input_mode_N
            && (StringUtils.equals("2", pay_way_type) && isEddaFail);
        this.input_mode_N_pay_way_type_3 = 
            input_mode_N
            && (StringUtils.equals("3", pay_way_type));
        this.input_mode_Y_isKgibank_eq_account = 
            input_mode_Y 
            && (isKgibank(pay_bank_no) 
            && isKgibank(sord_bank_no)
            && StringUtils.equals(pay_account, sord_acct_no));
        this.input_mode_Y_isKgibank_ne_account = 
            input_mode_Y 
            && (isKgibank(pay_bank_no) 
            && isKgibank(sord_bank_no)
            && !StringUtils.equals(pay_account, sord_acct_no));
        this.input_mode_Y_notKgibank_isEddaPass = 
            input_mode_Y 
            && (!isKgibank(sord_bank_no) && isEddaPass);
        this.input_mode_Y_notKgibank_isEddaFail = 
            input_mode_Y 
            && (!isKgibank(sord_bank_no) && isEddaFail);
        this.input_mode_Y_isKgibank = 
            input_mode_Y 
            && isKgibank(sord_bank_no);
        if (input_mode_N_pay_way_type_1_isKgibank_eq_account) {
            pay_way_type = "1";
        }
        if (input_mode_N_pay_way_type_4_isKgibank_ne_account) {
            pay_way_type = "1";
        }
        if (input_mode_N_pay_way_type_2_isEddaPass) {
            pay_way_type = "2";
        }
        if (input_mode_N_pay_way_type_2_isEddaFail) {
            pay_way_type = "3";
        }
        if (input_mode_N_pay_way_type_3) {
            pay_way_type = "3";
        }

        if (input_mode_Y_isKgibank_eq_account) {
            pay_way_type = "1";
        }
        if (input_mode_Y_isKgibank_ne_account) {
            pay_way_type = "1";
        }
        if (input_mode_Y_notKgibank_isEddaPass) {
            pay_way_type = "2";
        }
        if (input_mode_Y_notKgibank_isEddaFail) {
            pay_way_type = "3";
        }
        if (input_mode_Y_isKgibank) {
            pay_way_type = "1";
        }

        this.setPay_way_type(pay_way_type);
    }
    /**
     * 是否為凱基銀行 809
     * @param bank_no
     * @return
     */
    private boolean isKgibank(String bank_no) {
        String bank_id = StringUtils.substring(bank_no, 0, 3); // 3碼銀行別
        return StringUtils.equals("809", bank_id);
    }
}
