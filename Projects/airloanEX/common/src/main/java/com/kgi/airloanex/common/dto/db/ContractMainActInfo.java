package com.kgi.airloanex.common.dto.db;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class ContractMainActInfo {
    @NonNull
    @CheckNullAndEmpty
    private String UniqId;
    private String AuthPhone;
    private String AuthBankId;
    private String AuthBankName;
    private String AuthAccountNo;
    private String AuthStatus;
    private int AuthErrCount;
    private String AuthTime;
    private String SignPhone;
    private String SignBankId;
    private String SignBankName;
    private String SignBranchId;
    private String SignAccountNo;
    private String SignStatus;
    private int SignErrCount;
    private String SignTime;
    private String EddaBankId;
    private String EddaBankName;
    private String EddaAccountNo;
    private String EddaStatus;
    private int EddaErrCount;
    private String EddaTime;
    private String CreateTime;
    private String UpdateTime;
}