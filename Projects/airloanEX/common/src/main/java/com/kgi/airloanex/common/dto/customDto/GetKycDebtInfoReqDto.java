package com.kgi.airloanex.common.dto.customDto;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CheckNullAndEmpty
@AllArgsConstructor
public class GetKycDebtInfoReqDto {
    /** (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string) */
    private String CASE_NO;
    /** (2)	APY_TYPE：產品別,1.現金卡(e貸寶) 2.PLOAN，(string) */
    private String APY_TYPE;
    /** (3)	IS_DGT：是否為數位案件(Y/N)，(string) */
    private String IS_DGT;
    /** (4)	IS_IMM：是否為秒貸案件(Y/N)(預設為N)，(string) */
    private String IS_IMM;
    /** (5)	JCIC_DATE：聯徵日(YYYYMMDD)，(string) */
    private String JCIC_DATE;

}
