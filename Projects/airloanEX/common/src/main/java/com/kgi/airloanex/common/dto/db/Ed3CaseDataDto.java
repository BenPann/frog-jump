package com.kgi.airloanex.common.dto.db;

public class Ed3CaseDataDto {

	private String UniqId ;
	private String BranchID ;
	private String Process ;
	private String PromoDepart ;
	private String PromoChannelID ;
	private String UserType ;
	

	public String getUniqId() {
		return UniqId;
	}
	public void setUniqId(String uniqId) {
		UniqId = uniqId;
	}
	public String getBranchID() {
		return BranchID;
	}
	public void setBranchID(String branchID) {
		BranchID = branchID;
	}
	public String getProcess() {
		return Process;
	}
	public void setProcess(String process) {
		Process = process;
	}
	public String getPromoDepart() {
		return PromoDepart;
	}
	public void setPromoDepart(String promoDepart) {
		PromoDepart = promoDepart;
	}
	public String getPromoChannelID() {
		return PromoChannelID;
	}
	public void setPromoChannelID(String promoChannelID) {
		PromoChannelID = promoChannelID;
	}
	public String getUserType() {
		return UserType;
	}
	public void setUserType(String userType) {
		UserType = userType;
	}
	
}
