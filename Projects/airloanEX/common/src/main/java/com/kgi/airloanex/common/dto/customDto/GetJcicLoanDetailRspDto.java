package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetJcicLoanDetailRspDto {
    /*
     * (2)	CHECK_CODE：
     * A.	檢核資料錯誤(空白代表沒有錯誤)
     * B.	如果有多個錯誤用『;』區隔,例如01;02
     * C.	『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
     * D.	錯誤代碼對照表:
     * 代碼  說明
     * ============================
     * 01	   案件編號格式錯誤
     * 99	  非預期性系統異常
     */
    private String CHECK_CODE;

    /*
     * (3)	ERR_MSG：
     * A.	當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
     */
    private String ERR_MSG;

    /*
     * (4)	LOAN_DETAIL：
     * A.	貸款明細
     * 欄位名稱			欄位說明
     * =======================================================
     * DATA_TYPE		資料類型(S:短期無擔保放款,SH:短期擔保放款,M:中期無擔保放款,MH:中期擔保放款,L:長期無擔保放款,LH:長期擔保放款,GMC:現金卡,CARD:信用卡
     * BANK_CODE   		銀行代號
     * BANK_NAME			銀行名稱
     * CONTRACT_AMT		訂約(萬)
     * LOAN_AMT 			餘額(萬)
     * SYS_PAY 		系統月付款(元)
     * REAL_PAY 		實際月付款(元)
     * J_COUNT			是否為代償
     */
    private String LOAN_DETAIL;
}
