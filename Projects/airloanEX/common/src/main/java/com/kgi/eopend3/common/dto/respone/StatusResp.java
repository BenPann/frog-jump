package com.kgi.eopend3.common.dto.respone;

import com.google.gson.GsonBuilder;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatusResp {
    private String status = "";
    
    
	public String toJsonString() {
		return new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(this) ;
	}
	
}