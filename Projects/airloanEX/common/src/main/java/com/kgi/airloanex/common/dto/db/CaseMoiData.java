package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class CaseMoiData {

    @NonNull
    private String UniqId;
    private String IdNo;
    private String CustomerName;
    private String IssueLocation;
    private String IssueType;
    private String IssueDate;
    private String Request;
    private String Response;
    private String ResponseCode;
    private String Memo;
    private String CreateTime;
    private String UpdateTime;

}
