package com.kgi.eopend3.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CheckNullAndEmpty
public class IDCardBackDataView {

    private String resAddrZipCode = "";    
    private String resAddrArea = "";   
    private String resAddr = "";
    private String commAddrZipCode = "";    
    private String commAddrArea = "";
    private String commAddr = "";
    private String marriage = "";
    private String birthPlace = "";

}