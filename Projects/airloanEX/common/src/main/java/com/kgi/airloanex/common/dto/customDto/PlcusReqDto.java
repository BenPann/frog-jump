package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlcusReqDto {

    // UniqId
    // UniqType

    // Name
    private String Name;
    // ProductId
    private String ProductId;
    // Phone
    private String Phone;
    // EMail
    private String EMail;
    // DepartId
    private String DepartId;
    // Member
    private String Member;
    // ContactTime
    private String ContactTime;
    // Note
    private String Note;

    // PrevPage
    // ContactFlag

}
