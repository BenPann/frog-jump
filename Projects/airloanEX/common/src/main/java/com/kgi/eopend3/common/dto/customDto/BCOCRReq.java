package com.kgi.eopend3.common.dto.customDto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class BCOCRReq {

    private String BYPASSID = "";
    private String BC_CONTENT = "";

}