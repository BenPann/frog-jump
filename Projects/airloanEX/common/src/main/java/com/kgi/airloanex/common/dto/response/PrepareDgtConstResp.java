package com.kgi.airloanex.common.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PrepareDgtConstResp {
    // 1. GET_DGT_CONST（ID,1）	是否有個人信貸（PL）簽約資訊
    private GetDgtConstResp getDgtConstResp1;
    // 2. GET_DGT_CONST（ID,3）	是否有循環信貸（RPL）簽約資訊
    private GetDgtConstResp getDgtConstResp3;
    // 3. GET_DGT_CONST（ID,2）	是否有現金卡簽約資訊
    private GetDgtConstResp getDgtConstResp2;
    // 繳款日期
    private String paymentDay ;

}