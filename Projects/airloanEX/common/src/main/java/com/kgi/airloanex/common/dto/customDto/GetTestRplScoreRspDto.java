package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetTestRplScoreRspDto {
    /**
     * (24)	CHECK_CODE：
     * A.	檢核資料錯誤(空白代表沒有錯誤)
     * B.	如果有多個錯誤用『;』區隔,例如01;02
     * C.	『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
     * D.	錯誤代碼對照表:
     * 代碼  說明
     * ============================
     * 01	   案件編號格式錯誤
     * 02	   案件編號不存在
     * 03	   婚姻狀況格式錯誤
     * 04	   工作年資格式錯誤
     * 05	   年齡格式錯誤
     * 06	   性別格式錯誤
     * 07	   教育程度格式錯誤
     * 08	   年收入格式錯誤
     * 09	   行業別代號格式錯誤
     * 10	   職務名稱代碼格式錯誤
     * 99	  非預期性系統異常
     */
    private String CHECK_CODE;
    /**
     * (25)	ERR_MSG：
     * 當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
     */
    private String ERR_MSG;
    /**
     * (26)	SCORE_INFO：
     * B.	評分資訊
     * 欄位名稱			欄位說明
     * =======================================================
     * PROJECT_NO			專案代號
     * CARD_TYPE 		    評分模型類型 1.未婚 2.已婚
     * TOTAL_SCORE		分數
     * SCORE_RANK			RANK
     * INTEREST_RATE     利率
     * CREDIT_LINE		建議額度
     */
    private GetTestRplScoreRspDtoScoreInfo SCORE_INFO;
}
