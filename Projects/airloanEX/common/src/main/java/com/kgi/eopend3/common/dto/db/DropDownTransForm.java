package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DropDownTransForm {

    String DropDownDataKey = "";
    String Credit = "";
    String Loan = "";
    String Eop = "";
    String Description = "";
        
}
