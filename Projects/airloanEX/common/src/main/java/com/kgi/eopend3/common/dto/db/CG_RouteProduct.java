package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CG_RouteProduct {

    String ChooseProductId = "";
	String ChannelId = "";
	String RouteID = "";
	String RouteType = "";
	String RouteURL = "";
    
}