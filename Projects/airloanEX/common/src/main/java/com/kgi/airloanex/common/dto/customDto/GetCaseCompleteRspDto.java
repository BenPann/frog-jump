package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetCaseCompleteRspDto {
    
    private String CHECK_CODE;

    private String RTN_CODE;
    
    private String RTN_MSG;
}
