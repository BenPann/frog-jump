package com.kgi.eopend3.common.dto.respone;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ChannelDepartListResp {

    private String departId;
    private String departName;
    
}