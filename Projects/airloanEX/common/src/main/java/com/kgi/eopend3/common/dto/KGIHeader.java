package com.kgi.eopend3.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KGIHeader {

    private String uniqId = "";    
    private String uniqType = "";
    private String idno = "";
    private String ipAddress = "";

}