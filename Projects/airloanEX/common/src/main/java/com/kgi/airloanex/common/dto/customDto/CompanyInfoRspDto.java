package com.kgi.airloanex.common.dto.customDto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyInfoRspDto {
    
    private String Code;

    private String Message;

    private CompanyInfoRspDtoData Data;
}
