package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
public class ED3_EX_CreditCaseData {
    
    @NonNull
    private String UniqId ;
    private String AirloanUniqId ;
    private String UserType ;
    private String CreditProductId ;
    private String IdCardCRecord ;
    private String Occupation ;
    private String Education ;
    private String Marriage ;
    private String SendType ;    
    private String JobTitle ;
    private String FirstPresent ;
    private String DataUse ;   
   
}
