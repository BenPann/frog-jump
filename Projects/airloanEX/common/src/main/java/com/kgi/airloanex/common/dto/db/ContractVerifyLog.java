package com.kgi.airloanex.common.dto.db;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class ContractVerifyLog {
    @NonNull
    @CheckNullAndEmpty
    private String UniqId;
    @NonNull
    @CheckNullAndEmpty
    private String UniqType;
    private String VerifyType;
    // @NonNull
    private int Count;
    private String Request;
    private String Response;
    private String CheckCode;
    private String AuthCode;
    private String Other;
    private String Status;
    private String CreateTime;
}