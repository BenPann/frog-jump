package com.kgi.crosssell.common.util;

public class ValidateObj {
    private boolean ok;
    private String value;
    private String oriValue;

    public ValidateObj(String str) {
        this.oriValue = str;
        this.setFail();
    }

    public void setFail() {
        this.ok = false;
        this.value = "ValidateFail";
    }

    public void setSuccess(String str) {
        this.ok = true;
        this.value = str;
    }

    public boolean isOK() {
        return this.ok;
    }

    public String getValue() {
        return this.value;
    }

    public String getOriValue() {
        return this.oriValue;
    }

    public String toString() {
        return "ok:" + this.ok + " value:" + this.value + " oriValue:" + this.oriValue;
    }
}
