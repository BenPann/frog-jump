package com.kgi.airloanex.common.dto.view;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplyRelationDegreeView {
    private String idno;
    private String customer_name;

    // private String relationship;
    // private String relationshipName;
    // private String relationshipId;

    private List<ApplySecondDegree> addSecondDegreeData = new ArrayList<>();
    private List<ApplyRelationDegreeCorp> addData = new ArrayList<>();
}
