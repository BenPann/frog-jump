package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreditCaseData {
    // [UniqId] [nvarchar](50) NOT NULL 
    // 序號                 | CRED 開頭或 CASE 開頭                                                     |
    private String UniqId;

    // [UniqType] [char](2) NOT NULL 
    // 唯一代號類型         | 01 : 體驗 02: 申請 03:立約 04:專人聯絡 05:信用卡 12:薪轉 13:數三 14: 中壽 |
    private String UniqType;

    // [CreditNoAps] [nvarchar](50) NULL 
    // APS 的信用卡編號     |                                                                           |
    private String CreditNoAps;

    // [Status] [char](2) NOT NULL 
    // 案件狀態             |                                                                           |
    private String Status;

    // [ApsStatus] [varchar](1) NOT NULL 
    // APS 的案件狀態       | 1:不核卡 2:核卡                                                           |
    private String ApsStatus;

    // [Idno] [nvarchar](50) NOT NULL 
    // 身分證字號或護照號碼 |                                                                           |
    private String Idno;

    // [UserType] [char](1) NOT NULL 
    // 使用者類別           | 0: 新戶 1:信用卡戶 2:存款戶 3:純貸款戶                                    |
    private String UserType;

    // [Phone] [nvarchar](50) NOT NULL 
    // 手機號碼             |                                                                           |
    private String Phone;

    // [ProductId] [nvarchar](50) NOT NULL 
    // 產品名稱             | 信用卡代號                                                                |
    private String ProductId;

    // [ChtName] [nvarchar](50) NOT NULL 
    // 中文姓名             |                                                                           |
    private String ChtName;

    // [EngName] [nvarchar](100) NULL 
    // 英文姓名             |                                                                           |
    private String EngName;

    // [Birthday] [nvarchar](50) NOT NULL 
    // 生日                 |                                                                           |
    private String Birthday;

    // [Gender] [nvarchar](50) NOT NULL 
    // 性別                 | 1:男 2:女                                                                 |
    private String Gender;

    // [IdCardDate] [datetime] NOT NULL 
    // 身份證換發日期       |                                                                           |
    private String IdCardDate;

    // [IdCardLocation] [nvarchar](50) NOT NULL 
    // 身份證換發地點       |                                                                           |
    private String IdCardLocation;

    // [IdCardCRecord] [char](1) NOT NULL 
    // 身份證換發紀錄       | 0:初領 1:補領 2:換發                                                      |
    private String IdCardCRecord;

    // [ResAddrZipCode] [nvarchar](6) NOT NULL 
    // 戶籍地址區碼         |                                                                           |
    private String ResAddrZipCode;

    // [ResAddr] [nvarchar](200) NOT NULL 
    // 戶籍地址             |                                                                           |
    private String ResAddr;

    // [CommAddrZipCode] [nvarchar](6) NOT NULL 
    // 通訊地址區碼         |                                                                           |
    private String CommAddrZipCode;

    // [CommAddr] [nvarchar](200) NOT NULL 
    // 通訊地址             |                                                                           |
    private String CommAddr;

    // [HomeAddrZipCode] [nvarchar](6) NOT NULL 
    // 居住地址區碼         |                                                                           |
    private String HomeAddrZipCode;

    // [HomeAddr] [nvarchar](200) NOT NULL 
    // 居住地址             |                                                                           |
    private String HomeAddr;

    // [BillAddrZipCode] [nvarchar](6) NOT NULL 
    // 帳單地址區碼         |                                                                           |
    private String BillAddrZipCode;

    // [BillAddr] [nvarchar](200) NOT NULL 
    // 帳單地址             |                                                                           |
    private String BillAddr;

    // [SendCardAddrZipCode] [nvarchar](6) NOT NULL 
    // 寄卡地址區碼         |                                                                           |
    private String SendCardAddrZipCode;

    // [SendCardAddr] [nvarchar](200) NOT NULL 
    // 寄卡地址             |                                                                           |
    private String SendCardAddr;

    // [Education] [char](1) NOT NULL 
    // 教育程度             |                                                                           |
    private String Education;

    // [Marriage] [char](1) NOT NULL 
    // 婚姻狀態             |                                                                           |
    private String Marriage;

    // [EmailAddress] [varchar](120) NOT NULL 
    // EMAIL 地址           |                                                                           |
    private String EmailAddress;

    // [ResTelArea] [nvarchar](4) NOT NULL 
    // 戶籍電話區碼         |                                                                           |
    private String ResTelArea;

    // [ResTel] [nvarchar](30) NOT NULL 
    // 戶籍電話             |                                                                           |
    private String ResTel;

    // [HomeTelArea] [nvarchar](4) NOT NULL 
    // 居住電話區碼         |                                                                           |
    private String HomeTelArea;

    // [HomeTel] [nvarchar](30) NOT NULL 
    // 居住電話             |                                                                           |
    private String HomeTel;

    // [HomeType] [char](1) NOT NULL 
    // 居住房屋類型         | 0:自有宅 1:租房                                                           |
    private String HomeType;

    // [HomeOwner] [char](1) NOT NULL 
    // 不動產所有           | 0:自有 1:國宅 2:租賃                                                      |
    private String HomeOwner;

    // [CorpName] [nvarchar](100) NOT NULL 
    // 公司名稱             |                                                                           |
    private String CorpName;

    // [CorpTelArea] [nvarchar](4) NOT NULL 
    // 公司電話區碼         |                                                                           |
    private String CorpTelArea;

    // [CorpTel] [nvarchar](30) NOT NULL 
    // 公司電話             |                                                                           |
    private String CorpTel;

    // [CorpTelExten] [nvarchar](6) NOT NULL 
    // 公司電話分機         |                                                                           |
    private String CorpTelExten;

    // [CorpAddrZipCode] [nvarchar](6) NULL 
    // 公司地址區碼         |                                                                           |
    private String CorpAddrZipCode;

    // [CorpAddr] [nvarchar](200) NOT NULL 
    // 公司地址             |                                                                           |
    private String CorpAddr;

    // [Occupation] [char](3) NOT NULL 
    // 職業類別             |                                                                           |
    private String Occupation;

    // [CorpDepart] [nvarchar](50) NOT NULL 
    // 公司部門             |                                                                           |
    private String CorpDepart;

    // [JobTitle] [nvarchar](100) NOT NULL 
    // 職稱                 |                                                                           |
    private String JobTitle;

    // [YearlyIncome] [int] NOT NULL 
    // 年收入               | (單位：萬元)                                                              |
    private String YearlyIncome;

    // [OnBoardDate] [char](6) NOT NULL 
    // 到職日               |                                                                           |
    private String OnBoardDate;

    // [SendType] [char](1) NOT NULL 
    // 信用卡帳單型態       | 1:實體帳單 2:電子帳單 4:行動帳單                                          |
    private String SendType;

    // [FirstPresent] [nvarchar](50) NOT NULL 
    // 首刷禮               | 跟另一個 Table 對應                                                       |
    private String FirstPresent;

    // [IpAddress] [varchar](20) NULL 
    // IP                   |                                                                           |
    private String IpAddress;

    // [DataUse] [char](2) NULL 
    // 同意資料使用         | 01:同意 02:不同意                                                         |
    private String DataUse;

    // [CreateTime] [datetime] NOT NULL 
    // 新增時間             |                                                                           |
    private String CreateTime;

    // [UpdateTime] [datetime] NOT NULL 
    // 更新時間             |                                                                           |
    private String UpdateTime;

}
