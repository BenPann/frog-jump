package com.kgi.crosssell.common.dto;

import lombok.Getter;
import lombok.Setter;
import net.minidev.json.JSONObject;

@Getter
@Setter
public class CSCommonCifData {
    private String chtName;
    private String engName;
    private String idno;
    private String idCardDate;
    private String idCardLocation;
    private String idCardCRecord;
    private String gender;
    private String birthday;
    private String mobile;
    private String productType;
    private String productId;
    private String applyAmount;
    private String period;
    private String purpose;
    private String purposeOther;
    private String email;
    private String education;
    private String marriage;
    private String resAddrZipCode;
    private String resAddr;
    private String commAddrZipCode;
    private String commAddr;
    private String homeAddrZipCode;
    private String homeAddr;
    private String resTelArea;
    private String resTel;
    private String homeTelArea;
    private String homeTel;
    private String estateType;
    private String occupation;
    private String corpName;
    private String corpTelArea;
    private String corpTel;
    private String corpTelExten;
    private String corpAddrZipCode;
    private String corpAddr;
    private String corpDepart;
    private String jobTitle;
    private String onBoardDate;
    private String yearlyIncome;
    private String taxIdno;
    private String projectCode;
    private JSONObject additionalData;
    private String score;
    private String channelId;
    private String promoDepart;
    private String promoMember;
    private String image;
}
