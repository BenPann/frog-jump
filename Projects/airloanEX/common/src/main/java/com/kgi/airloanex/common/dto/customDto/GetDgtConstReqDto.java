package com.kgi.airloanex.common.dto.customDto;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CheckNullAndEmpty
@AllArgsConstructor
public class GetDgtConstReqDto {

    /** (1)	CUSTOMER_ID：客戶ID，VARCHAR (10) */
    private String CUSTOMER_ID;
    
    /** (2)	DATA_TYPE：產品類型(1-信用貸款;2-現金卡;3-小額循環信用貸款)，CHAR(1) */
    private String DATA_TYPE;
}