package com.kgi.airloanex.common.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApplyPreAuditResp {

    private String ProjectCode;

    /** 貸款金額(萬元) */
    private String CreditLine;
    
    /** 貸款利率(%) */
    private String IntrestRate;
    
    /** 本行指數利率 */
    private String Twdrate;
}
