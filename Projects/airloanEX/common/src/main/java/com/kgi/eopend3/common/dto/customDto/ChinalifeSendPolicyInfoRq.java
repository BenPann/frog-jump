package com.kgi.eopend3.common.dto.customDto;

import com.google.gson.GsonBuilder;
import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

public class ChinalifeSendPolicyInfoRq {
	@CheckNullAndEmpty
	private String msgId = "" ;
	@CheckNullAndEmpty
	private String actionType = "" ;
	@CheckNullAndEmpty
	private String applNo = "" ;
	@CheckNullAndEmpty
	private String caseId = "" ;
	@CheckNullAndEmpty
	private String msgDate = "" ;

	// for actionType = 1
	private String effDt = "" ;
	private int cashValue = 0 ;
	private int loanAmt = 0 ;
	private int rePremiu = 0 ;
	private String rePmtMtd = "" ;
	private String oriPmtMtd = "" ;
	private String policyImage = "" ;
	private String calDate = "" ;
	private String qryDate = "" ;
	private String applSts = "" ;
	private String applStsUpdDate = "" ;
	
	// for actionType = 2
	private String policySts = "" ;
	private String policyStsUpdDate = "" ;
	
	
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public String getApplNo() {
		return applNo;
	}
	public void setApplNo(String applNo) {
		this.applNo = applNo;
	}
	public String getCaseId() {
		return caseId;
	}
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	public String getEffDt() {
		return effDt;
	}
	public void setEffDt(String effDt) {
		this.effDt = effDt;
	}
	public int getCashValue() {
		return cashValue;
	}
	public void setCashValue(int cashValue) {
		this.cashValue = cashValue;
	}
	public int getLoanAmt() {
		return loanAmt;
	}
	public void setLoanAmt(int loanAmt) {
		this.loanAmt = loanAmt;
	}
	public int getRePremiu() {
		return rePremiu;
	}
	public void setRePremiu(int rePremiu) {
		this.rePremiu = rePremiu;
	}
	public String getRePmtMtd() {
		return rePmtMtd;
	}
	public void setRePmtMtd(String rePmtMtd) {
		this.rePmtMtd = rePmtMtd;
	}
	public String getOriPmtMtd() {
		return oriPmtMtd;
	}
	public void setOriPmtMtd(String oriPmtMtd) {
		this.oriPmtMtd = oriPmtMtd;
	}
	public String getPolicyImage() {
		return policyImage;
	}
	public void setPolicyImage(String policyImage) {
		this.policyImage = policyImage;
	}
	public String getCalDate() {
		return calDate;
	}
	public void setCalDate(String calDate) {
		this.calDate = calDate;
	}
	public String getQryDate() {
		return qryDate;
	}
	public void setQryDate(String qryDate) {
		this.qryDate = qryDate;
	}
	public String getApplSts() {
		return applSts;
	}
	public void setApplSts(String applSts) {
		this.applSts = applSts;
	}
	public String getApplStsUpdDate() {
		return applStsUpdDate;
	}
	public void setApplStsUpdDate(String applStsUpdDate) {
		this.applStsUpdDate = applStsUpdDate;
	}
	public String getMsgDate() {
		return msgDate;
	}
	public void setMsgDate(String msgDate) {
		this.msgDate = msgDate;
	}
	public String getPolicySts() {
		return policySts;
	}
	public void setPolicySts(String policySts) {
		this.policySts = policySts;
	}
	public String getPolicyStsUpdDate() {
		return policyStsUpdDate;
	}
	public void setPolicyStsUpdDate(String policyStsUpdDate) {
		this.policyStsUpdDate = policyStsUpdDate;
	}
	
	public String toJsonString() {
		return new GsonBuilder().disableHtmlEscaping().create().toJson(this) ;
	}	
	
}
