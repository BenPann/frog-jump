package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class APIMInfoRspDto {

    private String httpCode; // http 碼

    private String httpMessage; // http 訊息

    private String rdCode; // rdCode:rdMessage => RS2007:資料不存在; RS3811:連結機關作業管理資料未建; RS7007:網路傳送錯誤; RS7009:查詢作業完成

    private String rdMessage;

    private APIMInfoRspDtoData responseData; // 查驗總結果
    
}
