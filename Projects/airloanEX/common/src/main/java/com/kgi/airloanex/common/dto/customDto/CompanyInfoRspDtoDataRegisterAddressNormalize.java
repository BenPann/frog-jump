package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyInfoRspDtoDataRegisterAddressNormalize {
    
    private String Address;
    
    private String City;
    
    private String District;
    
    private String Road;
    
    private String Number;
    
    private String Other;
}
