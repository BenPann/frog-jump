package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CheckNoFinpfReqDto {
    
    /** (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string) */
    private String CASE_NO;

    /** (2)	APY_TYPE：產品別,1.現金卡(e貸寶) 2.PLOAN 3.信用卡，(string) */
    private String APY_TYPE;

    /** (3)	IS_DGT：是否為數位案件(Y/N)，(string) */
    private String IS_DGT;

    /** (4)	IS_IMM：是否為秒貸案件(Y/N)(預設為N)，(string) */
    private String IS_IMM;

    /** (5)	CORP_TYPE：行業別，(string) */
    private String CORP_TYPE;

    /** (6)	COMPANY_NAME：公司名稱，(string) */
    private String COMPANY_NAME;

    /** (7)	TITLE：職稱，(string) */
    private String TITLE;

    /** (8)	CORP_TITLE：職務名稱代碼(textmining)，(string) */
    private String CORP_TITLE;

    /** (9)	CUST_KIND：客戶類型(1.貸款2.現金卡3.信用卡4.薪轉戶，如果有多個類型用『;』區隔,例如01;02)，(string) */
    private String CUST_KIND;

    /** (10)	AML_RESULT：洗錢AML比對結果(Y/V/N/O/P)，(string) */
    private String AML_RESULT;

    /** (11)	APPLY_AMT：申請金額(單位:萬元)，(string) */
    private String APPLY_AMT;

    
    private String YEARLY_INCOME;

    
}
