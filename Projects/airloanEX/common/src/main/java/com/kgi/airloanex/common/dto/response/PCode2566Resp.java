package com.kgi.airloanex.common.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PCode2566Resp {
    
    private String pcode2566CheckCode = "";    
    private String pcode2566Message = "";

    private Integer authErrCount;
    private Integer signErrCount;
    private Integer eddaErrCount;
   
}