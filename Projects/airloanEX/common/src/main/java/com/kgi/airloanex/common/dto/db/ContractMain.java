package com.kgi.airloanex.common.dto.db;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class ContractMain {
    @NonNull
    @CheckNullAndEmpty
    /** 立約編號               | 範例: CONTYYYYMMDD00001                                      | */
    private String UniqId;
    
    /** 戶況(使用者使用者類別) | 0: 新戶 1:信用卡戶 2:存款戶 3:純貸款戶 (從 GET_CIF_INO 取得) | */
    private String UserType;
    
    /** 案件編號(APS)          | GET_DGT_CONST 來                                           | */
    private String CaseNoWeb;
    
    /** 立約回傳編號           | GET_DGT_CONST 來                                          | */
    private String GmCardNo;
    
    /** 案件狀態               | [詳見列表](#Status案件主狀態)                              | */
    private String Status;
    
    /** 產品代號               | 1、2、3 GET_DGT_CONST 代碼                                 | */
    private String ProductId;
    
    /** 身分證字號             |                                                            | */
    private String Idno;
    
    /** 客戶姓名               | 新戶: GET_DGT_CONST 既有戶:GET_CIF_INFO                     | */
    private String ChtName;
    
    /** 生日                   | YYYYMMDD                                                   | */
    private String Birthday;
    
    /** 電子信箱               | GET_CIF_INFO 來                                             | */
    private String Email;
    
    /** APS 撥款日期           | GET_DGT_CONST 來                                            | */
    private String DealPayDay;
    
    /** 最終撥款日期           | 計算過後的 PayDay                                            | */
    private String FinalPayDay;

    /** 撥款為本行他行         | 0:本行  1:它行  null:它行                                    | */
    private String PayBankOption;

    /** 繳款為本行他行         | 0:本行  1:它行  null:它行                                    | */
    private String SordBankOption;

    /** 立約日期               | 立約當日                                                     | */
    private String ContractDay;
    
    /** 繳款日期               | 01~31                                                        | */
    private String PaymentDay;
    
    /** 文件寄送地址           | 暫無                                                         | */
    private String DocAddr;
    
    /** 審閱日期               |                                                              | */
    private String ReadDate;
    
    /** 告知方式               | 01:簡訊 02:書面                                              | */
    private String Notify;
    
    /** 法院名稱簡寫           |                                                              | */
    private String Court;
    
    /** 同意資料使用(共銷)     | 01:同意 02:不同意                                            | */
    private String DataUse;
    
    /** APS 的立約資料         | GET_DGT_CONST                                                | */
    private String ApsConstData;
    
    /** nvarchar(max)          | 預覽用                                                       | */
    private String ResultHtml;
    
    /** 簽名時間(對保時間)               |                                                              | */
    private String SignTime;
    
    /** 送影像                 |                                                              | */
    private String SendTime;
    
    /** IP                     |                                                              | */
    private String IpAddress;
    
    /** 建立時間               |                                                              | */
    private String CreateTime;
    
    /** 更新時間               |                                                              | */
    private String UpdateTime;
    
}