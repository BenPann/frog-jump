package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplyNewCreditcardView {

    /** 信用卡新戶首刷禮 CreditCardGift.GiftCode */
    private String nitoCard;

    /** 英文姓名 */
    private String englishName;

    /** 帳單類別 */
    private String billingType;

    /** 通訊地址，此為信用卡、實體帳單及郵寄寄送地址：ra1 同住家地址, ra2 同公司地址, ra3 自行填寫 */
    private String mailingAddress;

    /** 縣市 */
    private String counties;

    /** 區 */
    private String township;

    /** 地址 */
    private String address;

    /** 郵遞區號 */
    private String city_code;

    /** 完整地址[縣市][區][地址] */
    private String fullAddress;

    /** 公司電話[市話區碼]-[市話號碼] */
    private String workPhone;

    /** 分機 */
    private String extension;

    /** 市話區碼 */
    private String tel_area;

    /** 市話號碼 */
    private String tel;

    /** 我已閱讀並同意聯名機構之個資利用同意事項 */
    private String ch1;

    /** 我已閱讀並同意上列條款及告知事項 */
    private String ch2;

}
