package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Terms {

    private int serial;
    private String name = "";
    private String version = "";
    private String status= "";
    private String content = "";
    private String addedTime = "";  
    
}