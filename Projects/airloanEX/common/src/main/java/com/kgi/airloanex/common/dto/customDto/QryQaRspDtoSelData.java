package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QryQaRspDtoSelData {

    // A.	選項資料
// 欄位名稱		欄位說明
// =======================================================
// quest_no		問題編號
// sel_no			選項編號
// sel_desc		選項說明
// sel_value		選項值
private String quest_no;
private String sel_no;
private String sel_desc;
private String sel_value;
}
