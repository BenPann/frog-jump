package com.kgi.eopend3.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KGIProductListDto {

	private String ed3CommonProductId = "";
	private String ed3CommProdTitle = "";
	private String ed3CommSubTitle = "";
	private String ed3CommDesc = "";
	private String ed3CommDisplayDefault = "";
	private String ed3CommDisplayOption = "";
	private String ed3CommSort = "";
	
}
