package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class CaseProperties {
    
    @NonNull
    private String UniqId;

	private String PropertyName;

	private String PropertyDesc;

	private String PropertyValue;

}
