package com.kgi.airloanex.common.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDto;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoConstData;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoDepData;
import com.kgi.airloanex.common.dto.db.CaseAuth;
import com.kgi.airloanex.common.dto.db.ContractMain;
import com.kgi.airloanex.common.dto.db.ContractMainActInfo;

import org.apache.commons.lang3.StringUtils;

public class ContractPDFDomain {
// .replace("%agrDateYY%",
// .replace("%agrDateMM%",
// .replace("%agrDateDD%",
// .replace("%aprove_amount%",
// .replace("%aprove_period%",
// .replace("%LoanBeginYear%",
// .replace("%LoanBeginMonth%",
// .replace("%LoanBeginDay%",
// .replace("%LoanEndYear%",
// .replace("%LoanEndMonth%",
// .replace("%LoanEndDay%",
// .replace("%"+newdata
// .replace("%rate_desc%",
// .replace("%rpl_pay_desc%",
// .replace("%pay_desc%",
// .replace("%chk_kgi_account%",
// .replace("%chk_other_account%",
// .replace("%dep_account%",
// .replace("%other_bank_no%",
// .replace("%other_bank_name%",
// .replace("%other_account%",
// .replace("%chk_kgi_account%",
// .replace("%chk_other_account%",
// .replace("%dep_account%",
// .replace("%other_bank_no%",
// .replace("%other_bank_name%",
// .replace("%other_account%",
// .replace("%chk_pay_kind_type_1%",
// .replace("%chk_pay_kind_type_2%",
// .replace("%pay_kind_day%",
// .replace("%pay_kind_day%",
// .replace("%chk_use_ach%",
// .replace("%pay_kind%",
// .replace("%pay_way%",
// .replace("%notify%",
// .replace("%apy_fee_kind%",
// .replace("%sum_rate%",
// .replace("%pen_fee_kind%",
// .replace("%agrService%",
// .replace("%districtCourt%",
// .replace("%idNo%",
// .replace("%pay_cashcard_num%",
// .replace("%apply_credit_line%",
// .replace("%p_loan_no%",
// .replace("%apy_fee%",
// .replace("%pay_kind_day%",
// .replace("%payWayFlag%",
// .replace("%agrService2%",
// .replace("%member_no%",
// .replace("%ROCyear%",
// .replace("%year%",
// .replace("%month%",
// .replace("%day%",
// .replace("%hour%",
// .replace("%minute%",
// .replace("%ROCyear%",
// .replace("%year%",
// .replace("%month%",
// .replace("%day%",
// .replace("%hour%",
// .replace("%minute%",
// .replace("%idPass%",
// .replace("%idPass%",
// .replace("%idPass%",
// .replace("%idPass%",
// .replace("%IPAddress%",
// .replace("%sTable%",
// .replace("%sTable%",
// .replace("%user_cname%",
// .replace("%edda_result%",
// .replace("%edda_result%",
// .replace("%eddaBankId_".
// .replace("%eddaBankId_".
// .replace("%eddaAccountNo_".
// .replace("%eddaAccountNo_".

public static final String _CHK = "<span class='fs22'>■</span>";
public static final String _UNCHK = "<span class='fs22'>□</span>";

    private Map<String, String> map = new LinkedHashMap<String, String>();

    public void make(
        String _uniqId,
        ContractMain contractMain, 
        ContractMainActInfo contractMainActInfo,
        String verifyType,
        String contractdate,
        String dealPaydate,
        String payDate,
        CaseAuth read,
        boolean isReplaceBankSeal,
		String contentBase64,
		String sendAddress,
		UpdateDgtConstDomain dgt) {
		
		String productId = contractMain.getProductId();

		// JSONObject apsConstData = JSONObject.fromObject(contractMain.getApsConstData());
		GetDgtConstRspDto apsConstData = new Gson().fromJson(contractMain.getApsConstData(), GetDgtConstRspDto.class);

		List<Map<String, String>> payjarr = new ArrayList<>();
		List<GetDgtConstRspDtoConstData> constDataList = apsConstData.getConstDataList();
		// 
		for (int i = 0; i < constDataList.size(); i++) {
			GetDgtConstRspDtoConstData o = constDataList.get(i);
			Map<String, String> r = new HashMap<>();
			r.put("pay_account", o.getPay_account());
			r.put("pay_bank", o.getPay_bank());
			payjarr.add(r);
		}

			GetDgtConstRspDtoConstData constData = apsConstData.findOneConstData();
			@SuppressWarnings("unused")
			GetDgtConstRspDtoDepData constDepData = apsConstData.findOneDepData();

			// 要確認一下
			String readDate = contractMain.getReadDate();
			// readDate = "2019-12-05 00:00:00.000"; // FIXME: Charles
			map.put("%agrDateYY%", String.valueOf((Integer.parseInt(readDate.substring(0, 4)) - 1911)));
			map.put("%agrDateMM%", readDate.substring(5, 7));
			map.put("%agrDateDD%", readDate.substring(8, 10));

			String name = contractMain.getChtName();
			if(name.length() <=0)
			{
				name = constData.getCustomer_name();
			}
			map.put("%user_cname%", name);

			// 第一條(借款金額)			
			map.put("%aprove_amount%", constData.getAprove_amount());

			// 第二條(借款期間)
			map.put("%aprove_period%", String.valueOf(new Double(constData.getAprove_period()).intValue()));
			int LoanEndYear;
			// 當數位信貸時撥貸期間為paydate與aprove_period相加，其餘產品為paydate加一年
			if (StringUtils.equals(productId, PlContractConst.CONTRACT_PRODUCT_ID_TYPE_LOAN)) {
				LoanEndYear = Integer.valueOf(payDate.substring(0, 4)) + new Double(constData.getAprove_period()).intValue();
			} else {
				LoanEndYear = Integer.valueOf(payDate.substring(0, 4)) + 1;
			}

			map.put("%LoanBeginYear%", String.valueOf((Integer.parseInt(payDate.substring(0, 4)) - 1911)));
			map.put("%LoanBeginMonth%", payDate.substring(4, 6));

			// if (StringUtils.isNotBlank(constData.getPay_kind_day()) 
			//     && !StringUtils.equals("0", constData.getPay_kind_day().trim())
			// 	&& StringUtils.isNotBlank(constData.getPay_date())
			// 	&& StringUtils.equals("N", constData.getInput_mode())) {
			// 		map.put("%LoanBeginDay%", constData.getPay_date().substring(6, 8));
			// 		map.put("%LoanEndDay%", constData.getPay_date().substring(6, 8));
			// } else {
				map.put("%LoanBeginDay%", payDate.substring(6, 8));
				map.put("%LoanEndDay%", payDate.substring(6, 8));
			// }

			
			map.put("%LoanEndYear%", String.valueOf((LoanEndYear - 1911)));
			map.put("%LoanEndMonth%", payDate.substring(4, 6));
			
			
			// 第三條(借款利率及利息計付方式)
			map.put("%rate_desc%", constData.getRate_desc());

			map.put("%rpl_pay_desc%", StringUtils.trimToEmpty(constData.getRpl_pay_desc()));

			// 第四條(契約類型及借款之交付)
			// 立約電文中pay_desc_type=3時，直接顯示pay_desc
			if (dgt.input_mode_N_pay_desc_type) {
				map.put("%pay_desc_show%", "3");
				map.put("%pay_desc%", StringUtils.trimToEmpty(constData.getPay_desc()));// 立約查詢電文的 pay_desc				
			}
			
		    // 立約電文中pay_desc_type!=3時，由系統依據畫面產生下段 或 客戶畫面自行輸入
			if (dgt.input_mode_Y_pay_desc_type) {
				map.put("%pay_desc_show%", "1");
				map.put("%chk_kgi_account%", _CHK);
				map.put("%chk_other_account%", _UNCHK);
				map.put("%809_account%", StringUtils.trimToEmpty(contractMainActInfo.getSignAccountNo()));// 本行
				map.put("%other_bank_no%", "___");
				map.put("%other_bank_name%", "______");
				map.put("%other_account%", "________________");
			}
			if (dgt.input_mode_Y_pay_desc_type_else) {
				map.put("%pay_desc_show%", "1");
				map.put("%chk_kgi_account%", _UNCHK);
				map.put("%chk_other_account%", _CHK);
				map.put("%809_account%", "________________");
				map.put("%other_bank_no%", StringUtils.trimToEmpty(contractMainActInfo.getSignBankId()));// 他行
                // Charles:僅留"銀行"前的字串，例如："台灣銀行" 轉為 "台灣" 2020-12-29 demand by Ben
				String strSignBankName = StringUtils.trimToEmpty(contractMainActInfo.getSignBankName());
				if (StringUtils.containsAny(strSignBankName, "銀行")) {
					strSignBankName = StringUtils.substringBefore(strSignBankName, "銀行");
				} else if (StringUtils.containsAny(strSignBankName, "商銀")) {
					strSignBankName = StringUtils.substringBefore(strSignBankName, "商銀");
				}
				map.put("%other_bank_name%", strSignBankName);// 他行銀行名稱
				map.put("%other_account%", StringUtils.trimToEmpty(contractMainActInfo.getSignAccountNo()));// 他行帳號
			};

            // 第五條(委託代償指定金融機構借款切結條款)
            // 指定銀行名單
            if (payjarr.size() == 0) {
            	map.put("%sTable%", "");
            } else {
            	String table = "";
            	for (int i = 0; i < payjarr.size(); i++) {
            		Map<String, String> bank = payjarr.get(i);
            		table = table.concat("<tr>");
            		table = table.concat(
            				"<td>" + bank.getOrDefault("pay_bank", "").replace("NULL", "&nbsp;")
            						+ "</td>");
            
            		table = table.concat("<td>"
            				+ bank.getOrDefault("pay_account", "").replace("NULL", "&nbsp;")
            				+ "</td>");
            		table = table.concat("</tr>");
            	}
            	map.put("%sTable%", table);
            }
			
            // 第六條(還款方式)
			// map.put("%pay_kind%", "StringUtils.trimToEmpty(constData.getPay_kind())");		
			map.put("%pay_kind%", dgt.getPay_kind());

			if (dgt.input_mode_N_pay_kind_type_1) {
				map.put("%chk_pay_kind_type_1%", _CHK);
				map.put("%chk_pay_kind_type_2%", _UNCHK);
				map.put("%pay_kind_day%", "");
			}
			if (dgt.input_mode_N_pay_kind_type_2) {
				map.put("%chk_pay_kind_type_1%", _UNCHK);
				map.put("%chk_pay_kind_type_2%", _CHK);
				map.put("%pay_kind_day%", StringUtils.trimToEmpty(contractMain.getPaymentDay()));
			}
			if (dgt.input_mode_Y_eq_pay_kind_day) {
				map.put("%chk_pay_kind_type_1%", _UNCHK);
				map.put("%chk_pay_kind_type_2%", _CHK);
				map.put("%pay_kind_day%", StringUtils.trimToEmpty(contractMain.getPaymentDay()));
			}
			if (dgt.input_mode_Y_ne_pay_kind_day) {
				map.put("%chk_pay_kind_type_1%", _UNCHK);
				map.put("%chk_pay_kind_type_2%", _CHK);
				map.put("%pay_kind_day%", StringUtils.trimToEmpty(contractMain.getPaymentDay()));
			}
			if (dgt.input_mode_N_or_Y_pay_kind_type_3_or_4_or_5) {
				map.put("%pay_kind_status%", "no");
			} else {
				map.put("%pay_kind_status%", "yes");
			}
 
			// 針對RPL及現金寫入
			if (!StringUtils.equals("1", contractMain.getProductId())) {
				map.put("%pay_kind_day%", StringUtils.trimToEmpty(contractMain.getPaymentDay()));
			}
			// 第七條(還款授權扣款方式)

			// Charles:僅留"銀行"前的字串，例如："台灣銀行" 轉為 "台灣" 2020-12-29 demand by Ben
			String strEddaBankName = StringUtils.trimToEmpty(contractMainActInfo.getEddaBankName());
			if (StringUtils.containsAny(strEddaBankName, "銀行")) {
				strEddaBankName = StringUtils.substringBefore(strEddaBankName, "銀行");
			} else if (StringUtils.containsAny(strEddaBankName, "商銀")) {
				strEddaBankName = StringUtils.substringBefore(strEddaBankName, "商銀");
			}

			if (dgt.input_mode_N_pay_way_type_1_isKgibank_eq_account) {
				map.put("%chk_pay_way_type_1%", _CHK);
				map.put("%chk_pay_way_type_1_1%", _CHK);
				map.put("%pay_desc_type%", StringUtils.trimToEmpty(constData.getPay_desc_type()));
				map.put("%chk_pay_way_type_1_2%", _UNCHK);
				map.put("%pay_way_type_1_2_SordAct%", "______");
				map.put("%chk_pay_way_type_2%", _UNCHK);
				map.put("%chk_pay_way_type_2_Edda_service%", _UNCHK);
				map.put("%chk_pay_way_type_2_Edda_Bank_Name%", "________");
				map.put("%chk_pay_way_type_2_Edda_Bank_Act_No%", "________________");
				map.put("%chk_pay_way_type_3%", _UNCHK);
				map.put("%edda_result%", "N");
			}
			if (dgt.input_mode_N_pay_way_type_4_isKgibank_ne_account) {
				map.put("%chk_pay_way_type_1%", _CHK);
				map.put("%chk_pay_way_type_1_1%", _UNCHK);
				map.put("%pay_desc_type%", "");
				map.put("%chk_pay_way_type_1_2%", _CHK);
				map.put("%pay_way_type_1_2_SordAct%", StringUtils.trimToEmpty(contractMainActInfo.getEddaAccountNo()));//SordAccount
				map.put("%chk_pay_way_type_2%", _UNCHK);
				map.put("%chk_pay_way_type_2_Edda_service%", _UNCHK);
				map.put("%chk_pay_way_type_2_Edda_Bank_Name%", "________");
				map.put("%chk_pay_way_type_2_Edda_Bank_Act_No%", "________________");
				map.put("%chk_pay_way_type_3%", _UNCHK);
				map.put("%edda_result%", "N");
			}
			if (dgt.input_mode_N_pay_way_type_2_isEddaPass) {
				map.put("%chk_pay_way_type_1%", _UNCHK);
				map.put("%chk_pay_way_type_1_1%", _UNCHK);
				map.put("%pay_desc_type%", "");
				map.put("%chk_pay_way_type_1_2%", _UNCHK);
				map.put("%pay_way_type_1_2_SordAct%", "______");
				map.put("%chk_pay_way_type_2%", _CHK);
				map.put("%chk_pay_way_type_2_Edda_service%", _CHK);
				map.put("%chk_pay_way_type_2_Edda_Bank_Name%", strEddaBankName);// EDDA銀行名稱
				map.put("%chk_pay_way_type_2_Edda_Bank_Act_No%", StringUtils.trimToEmpty(contractMainActInfo.getEddaAccountNo()));// EDDA帳號
				map.put("%chk_pay_way_type_3%", _UNCHK);
				map.put("%edda_result%", "N");
			}
			if (dgt.input_mode_N_pay_way_type_2_isEddaFail) {
				map.put("%chk_pay_way_type_1%", _UNCHK);
				map.put("%chk_pay_way_type_1_1%", _UNCHK);
				map.put("%pay_desc_type%", "");
				map.put("%chk_pay_way_type_1_2%", _UNCHK);
				map.put("%pay_way_type_1_2_SordAct%", "______");
				map.put("%chk_pay_way_type_2%", _CHK);
				map.put("%chk_pay_way_type_2_Edda_service%", _UNCHK);
				map.put("%chk_pay_way_type_2_Edda_Bank_Name%", "________");
				map.put("%chk_pay_way_type_2_Edda_Bank_Act_No%", "________________");
				map.put("%chk_pay_way_type_3%", _CHK);
				map.put("%edda_result%", "Y");
			}
			if (dgt.input_mode_N_pay_way_type_3) {
				map.put("%chk_pay_way_type_1%", _UNCHK);
				map.put("%chk_pay_way_type_1_1%", _UNCHK);
				map.put("%pay_desc_type%", "");
				map.put("%chk_pay_way_type_1_2%", _UNCHK);
				map.put("%pay_way_type_1_2_SordAct%", "______");
				map.put("%chk_pay_way_type_2%", _CHK);
				map.put("%chk_pay_way_type_2_Edda_service%", _UNCHK);
				map.put("%chk_pay_way_type_2_Edda_Bank_Name%", "________");
				map.put("%chk_pay_way_type_2_Edda_Bank_Act_No%", "________________");
				map.put("%chk_pay_way_type_3%", _CHK);
				map.put("%edda_result%", "N");
			}
			if (dgt.input_mode_Y_isKgibank_eq_account) {
				map.put("%chk_pay_way_type_1%", _CHK);
				map.put("%chk_pay_way_type_1_1%", _CHK);
				map.put("%pay_desc_type%", StringUtils.trimToEmpty(constData.getPay_desc_type()));
				map.put("%chk_pay_way_type_1_2%", _UNCHK);
				map.put("%pay_way_type_1_2_SordAct%", "______");
				map.put("%chk_pay_way_type_2%", _UNCHK);
				map.put("%chk_pay_way_type_2_Edda_service%", _UNCHK);
				map.put("%chk_pay_way_type_2_Edda_Bank_Name%", "________");
				map.put("%chk_pay_way_type_2_Edda_Bank_Act_No%", "________________");
				map.put("%chk_pay_way_type_3%", _UNCHK);
				map.put("%edda_result%", "N");
			}
			if (dgt.input_mode_Y_isKgibank_ne_account) {
				map.put("%chk_pay_way_type_1%", _CHK);
				map.put("%chk_pay_way_type_1_1%", _UNCHK);
				map.put("%pay_desc_type%", "");
				map.put("%chk_pay_way_type_1_2%", _CHK);
				map.put("%pay_way_type_1_2_SordAct%", StringUtils.trimToEmpty(contractMainActInfo.getEddaAccountNo())); // SordAccount
				map.put("%chk_pay_way_type_2%", _UNCHK);
				map.put("%chk_pay_way_type_2_Edda_service%", _UNCHK);
				map.put("%chk_pay_way_type_2_Edda_Bank_Name%", "________");
				map.put("%chk_pay_way_type_2_Edda_Bank_Act_No%", "________________");
				map.put("%chk_pay_way_type_3%", _UNCHK);
				map.put("%edda_result%", "N");
			}
			if (dgt.input_mode_Y_notKgibank_isEddaPass) {
				map.put("%chk_pay_way_type_1%", _UNCHK);
				map.put("%chk_pay_way_type_1_1%", _UNCHK);
				map.put("%pay_desc_type%", "");
				map.put("%chk_pay_way_type_1_2%", _UNCHK);
				map.put("%pay_way_type_1_2_SordAct%", "______");
				map.put("%chk_pay_way_type_2%", _CHK);
				map.put("%chk_pay_way_type_2_Edda_service%", _CHK);
				map.put("%chk_pay_way_type_2_Edda_Bank_Name%", strEddaBankName);// EDDA銀行名稱
				map.put("%chk_pay_way_type_2_Edda_Bank_Act_No%", StringUtils.trimToEmpty(contractMainActInfo.getEddaAccountNo()));// EDDA帳號
				map.put("%chk_pay_way_type_3%", _UNCHK);
				map.put("%edda_result%", "N");
			}
			if (dgt.input_mode_Y_notKgibank_isEddaFail) {
				map.put("%chk_pay_way_type_1%", _UNCHK);
				map.put("%chk_pay_way_type_1_1%", _UNCHK);
				map.put("%pay_desc_type%", "");
				map.put("%chk_pay_way_type_1_2%", _UNCHK);
				map.put("%pay_way_type_1_2_SordAct%", "______");
				map.put("%chk_pay_way_type_2%", _CHK);
				map.put("%chk_pay_way_type_2_Edda_service%", _UNCHK);
				map.put("%chk_pay_way_type_2_Edda_Bank_Name%", "________");
				map.put("%chk_pay_way_type_2_Edda_Bank_Act_No%", "________________");
				map.put("%chk_pay_way_type_3%", _CHK);
				map.put("%edda_result%", "Y");
			}

			if (dgt.input_mode_Y_isKgibank) {
				map.put("%chk_pay_way_type_1%", _CHK);
				map.put("%chk_pay_way_type_1_1%", _UNCHK);
				map.put("%pay_desc_type%", "");
				map.put("%chk_pay_way_type_1_2%", _CHK);
				map.put("%pay_way_type_1_2_SordAct%", StringUtils.trimToEmpty(contractMainActInfo.getEddaAccountNo())); // SordAccount
				map.put("%chk_pay_way_type_2%", _UNCHK);
				map.put("%chk_pay_way_type_2_Edda_service%", _UNCHK);
				map.put("%chk_pay_way_type_2_Edda_Bank_Name%", "________");
				map.put("%chk_pay_way_type_2_Edda_Bank_Act_No%", "________________");
				map.put("%chk_pay_way_type_3%", _UNCHK);
				map.put("%edda_result%", "Y");
			  }

			// 第八條(利率調整通知)
			map.put("%notify%", StringUtils.equals(contractMain.getNotify(), "01") ? "簡訊" : "書面");

            // 第九條(費用之收取與總費用年百分率)
			map.put("%apy_fee_kind%", StringUtils.trimToEmpty(constData.getApy_fee_kind()));
			map.put("%sum_rate%", StringUtils.trimToEmpty(constData.getSum_rate()));

			// 第十條（提前清償違約金）
			map.put("%pen_fee_kind%", StringUtils.trimToEmpty(constData.getPen_fee_kind()));

			// 第十一條（資訊之利用）
			map.put("%agrService%", "同意");

			// 第十二條
			map.put("%districtCourt%", StringUtils.trimToEmpty(contractMain.getCourt()));

			// 第十三條
			map.put("%const_other%", StringUtils.trimToEmpty(constData.getConst_other()));

            // 第十七條
			map.put("%idNo%", StringUtils.trimToEmpty(contractMain.getIdno()));
            // String contractdate = r.optString("contractDate", "");
            if (contractdate.length() > 0) {
            	map.put("%ROCyear%", String.valueOf((Integer.parseInt(contractdate.substring(0, 4)) - 1911)));
            	map.put("%year%", String.valueOf((Integer.parseInt(contractdate.substring(0, 4)) - 1911)));
            	map.put("%month%", contractdate.substring(4, 6));
            	map.put("%day%", contractdate.substring(6, 8));
            	map.put("%hour%", contractdate.substring(8, 10));
            	map.put("%minute%", contractdate.substring(10, 12));
            } else {
            	map.put("%ROCyear%", " ");
            	map.put("%year%", " ");
            	map.put("%month%", " ");
            	map.put("%day%", " ");
            	map.put("%hour%", " ");
            	map.put("%minute%", " ");
            }


			// FIXME: Alen: 以下不確定還需不需要
            // edda 1:驗過空心 2:沒驗過實心 
            map.put("%chk_use_ach%", StringUtils.equals(contractMainActInfo.getEddaStatus(), "1") ? _UNCHK : _CHK);
        	map.put("%pay_way%", StringUtils.trimToEmpty(constData.getPay_way()));
            // map.put("%pay_kind_day%", contractMain.getPaymentDay()); 
			

			map.put("%pay_cashcard_num%", StringUtils.trimToEmpty(constData.getPay_cashcard_num()));
			map.put("%apply_credit_line%", StringUtils.trimToEmpty(constData.getApply_credit_line()));
			map.put("%p_loan_no%", StringUtils.trimToEmpty(constData.getP_loan_no()));
			map.put("%apy_fee%", StringUtils.trimToEmpty(constData.getApy_fee()));
			
			map.put("%payWayFlag%", ""); // FIXME: Charles: 我也不知道為什麼會有這個欄位
			map.put("%agrService2%", "同意");
			map.put("%member_no%", StringUtils.trimToEmpty(constData.getMember_no()));
			

			map.put("%idPass%", verifyType);

			map.put("%IPAddress%", StringUtils.trimToEmpty(contractMain.getIpAddress()));

			// 專案者親簽flag
			String signflg = StringUtils.trimToEmpty(constData.getSign_flg());
			if (StringUtils.equals(signflg, "N")) {
				// 則不印上user簽名
				map.put("<div style=\"display:none;\">[usersign]</div>(親簽)<b>「如選擇本條第一項第(六)款述專案者請記得親簽」</b>",
						"(親簽)<b>「如選擇本條第一項第(六)款述專案者請記得親簽」</b>");
			}


	        // 如果是中華電信的 用中華電信的時間
	        boolean useCht = false;
	        if (null != read) {
	            useCht = StringUtils.equals(read.getChtAgree(), "1");
	            if (useCht) {
	            	String chtAgreeDate ="";
	            	String chtAgreeTime ="";
	            	// ChtAgreeTime
	                if(read.getChtAgreeTime() != null && !"".contentEquals(read.getChtAgreeTime()) ) {
	                	chtAgreeDate = read.getChtAgreeTime().split(" ")[0];
	                	chtAgreeTime = read.getChtAgreeTime().split(" ")[1];
	                }
	                if(StringUtils.isNotBlank(chtAgreeDate) && chtAgreeDate.split("-").length == 3)
	                map.put("[Apply_date]","中華民國".concat(String.valueOf((Integer.parseInt(chtAgreeDate.split("-")[0]) - 1911))).concat("年").concat(chtAgreeDate.split("-")[1]).concat("月").concat(chtAgreeDate.split("-")[2]).concat("日"));
	                if(StringUtils.isNotBlank(chtAgreeTime) )
	                {
                		map.put("[get_cht_rating_time]", chtAgreeTime.split(":")[0].concat(":").concat(chtAgreeTime.split(":")[1]));
	                }
	            }
	        }

	        // 申請人ip address
	        map.put("[ip_address]", StringUtils.trimToEmpty(contractMain.getIpAddress()));
			
			String _square = "<span style='font-size:2.4em; line-height:";
			String _solid = "px;'>■</span>";
			String _Hollow = "px;'>□</span>";

			// ACH_HIDE_[Y/N]
			if (StringUtils.equals("1", contractMainActInfo.getEddaStatus())) {
				map.put("%edda_result%", "Y");
			} else {
				map.put("%edda_result%", "N");
			}

			// 新申請自動扣繳
			map.put("%new_app_auto%", _square + "10" + _solid);
			// 身分證上面已帶入
			// 行動電話 authPhone
			map.put("%auth_phone%", StringUtils.trimToEmpty(contractMainActInfo.getAuthPhone()));

			char[] eddaBankIdAry = new char[0];
			if (contractMainActInfo.getEddaBankId() != null) {
				eddaBankIdAry = contractMainActInfo.getEddaBankId().toCharArray();
			}

			char[] eddaAccountNo = new char[0];
			if (contractMainActInfo.getEddaAccountNo() != null) {
				eddaAccountNo = contractMainActInfo.getEddaAccountNo().toCharArray();
			}

			final int eddaBankIdLength = 3; // 銀行代碼長度 = 3
			final int eddaAccountNoLength = 16; // 銀行帳號長度=16

			if(StringUtils.equals("700", contractMainActInfo.getEddaBankId())) {
				map.put("%edda_bank%", _square + "12" + _Hollow);
				map.put("%edda_post%", _square + "12" + _solid);
				map.put("%edda_result_Edda_Bank_Name%",  "");// EDDA銀行名稱

				for (int i = 0; i < eddaBankIdLength; i++) {
					if (i < eddaBankIdAry.length) {
						map.put("%eddaBankId_".concat(String.valueOf(i+1)).concat("%"), "");
					}
				}

				for (int i = 0; i < eddaAccountNoLength; i++) {
					if (i < eddaAccountNo.length) {
						map.put("%eddaAccountNo_postOffice_".concat(String.valueOf(i+1)).concat("%"), String.valueOf(eddaAccountNo[i]));
						map.put("%eddaAccountNo_".concat(String.valueOf(i+1)).concat("%"), "");
					} else {
						map.put("%eddaAccountNo_postOffice_".concat(String.valueOf(i+1)).concat("%"), "");
						map.put("%eddaAccountNo_".concat(String.valueOf(i+1)).concat("%"), "");
					}
				}
			} else {
				map.put("%edda_bank%", _square + "12" + _solid);
				map.put("%edda_post%", _square + "12" + _Hollow);
				map.put("%edda_result_Edda_Bank_Name%",  strEddaBankName);// EDDA銀行名稱
				
				for (int i = 0; i < eddaBankIdLength; i++) {
					if (i < eddaBankIdAry.length) {
						map.put("%eddaBankId_".concat(String.valueOf(i+1)).concat("%"), String.valueOf(eddaBankIdAry[i]));
					} else {
						map.put("%eddaBankId_".concat(String.valueOf(i+1)).concat("%"), "");
					}
				}

				for (int i = 0; i < eddaAccountNoLength; i++) {
					if (i < eddaAccountNo.length) {
						map.put("%eddaAccountNo_".concat(String.valueOf(i+1)).concat("%"), String.valueOf(eddaAccountNo[i]));
						map.put("%eddaAccountNo_postOffice_".concat(String.valueOf(i+1)).concat("%"), "");
					} else {
						map.put("%eddaAccountNo_".concat(String.valueOf(i+1)).concat("%"), "");
						map.put("%eddaAccountNo_postOffice_".concat(String.valueOf(i+1)).concat("%"), "");
					}
				}
			}

			// %send_address%   從ContractPDF取得
			map.put("%send_address%", sendAddress);

		if (isReplaceBankSeal) {
			replaceBankSeal(map, contentBase64);
		} else {
			replaceBankSealToBlank(map);
		}

    }

    public void replaceBankSeal(Map<String, String> map, String contentBase64) {
		map.put("<div style=\"display: none;\">[Bank_seal]</div>",
				"<img src=\"data:image/jpeg;base64," + contentBase64 + "\" " + "alt=\"\" style=\"width:120px;\" />");
	}

	public void replaceBankSealToBlank(Map<String, String> map) {
		map.put("<div style=\"display: none;\">[Bank_seal]</div>",
							"<div style=\"display: none;\">&nbsp;</div>");
    }

    public Map<String, String> getMap() {
        return map;
    }
    public void setMap(Map<String, String> map) {
        this.map = map;
    }
}
