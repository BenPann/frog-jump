package com.kgi.eopend3.common.dto.view;

public class IDCardFrontDataView {

    private String chtName = "";
    private String engName = "";
    private String idCardDate  = "";
    private String idCardLocation = "";
    private String idCardRecord = "";

    public String getChtName() {
        return chtName;
    }

    public void setChtName(String chtName) {
        this.chtName = chtName;
    }

    public String getEngName() {
        return engName;
    }

    public void setEngName(String engName) {
        this.engName = engName;
    }

    public String getIdCardDate() {
        return idCardDate;
    }

    public void setIdCardDate(String idCardDate) {
        this.idCardDate = idCardDate;
    }

    public String getIdCardLocation() {
        return idCardLocation;
    }

    public void setIdCardLocation(String idCardLocation) {
        this.idCardLocation = idCardLocation;
    }

    public String getIdCardRecord() {
        return idCardRecord;
    }

    public void setIdCardRecord(String idCardRecord) {
        this.idCardRecord = idCardRecord;
    }
    
}