package com.kgi.eopend3.common.dto.customDto;

import com.google.gson.GsonBuilder;

public class BNS00040007Dto {
	private int result = -1 ;
	private String status = "";
	private String pbStatus = "";
	private String chopStatus = "";
	private String noOfHold = "";
	private String bnsIdno = "";
	private String message = "";

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPbStatus() {
		return pbStatus;
	}

	public void setPbStatus(String pbStatus) {
		this.pbStatus = pbStatus;
	}

	public String getChopStatus() {
		return chopStatus;
	}

	public void setChopStatus(String chopStatus) {
		this.chopStatus = chopStatus;
	}

	public String getNoOfHold() {
		return noOfHold;
	}

	public void setNoOfHold(String noOfHold) {
		this.noOfHold = noOfHold;
	}

	public String getBnsIdno() {
		return bnsIdno;
	}

	public void setBnsIdno(String bnsIdno) {
		this.bnsIdno = bnsIdno;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String toString() {
		return new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(this);
	}

}
