package com.kgi.eopend3.common.dto.db;

public class ED3_VerifyLog {

	public ED3_VerifyLog(){};

	public ED3_VerifyLog(String uniqId, String verifyType, int count){
		this.UniqId = uniqId;
		this.VerifyType = verifyType;
		this.Count = count;
	}

    private String UniqId;
//    private String UniqType= "";
    private String VerifyType;
    private int Count;
    private String Request="";
    private String Response="";
    private String CheckCode="";
    private String AuthCode="";
    private String Other="";
    private String CreateTime;
    
	public String getUniqId() {
		return UniqId;
	}
	public void setUniqId(String uniqId) {
		UniqId = uniqId;
	}
//	public String getUniqType() {
//		return UniqType;
//	}
//	public void setUniqType(String uniqType) {
//		UniqType = uniqType;
//	}
	public String getVerifyType() {
		return VerifyType;
	}
	public void setVerifyType(String verifyType) {
		VerifyType = verifyType;
	}
	public int getCount() {
		return Count;
	}
	public void setCount(int count) {
		Count = count;
	}
	public String getRequest() {
		return Request;
	}
	public void setRequest(String request) {
		Request = request;
	}
	public String getResponse() {
		return Response;
	}
	public void setResponse(String response) {
		Response = response;
	}
	public String getCheckCode() {
		return CheckCode;
	}
	public void setCheckCode(String checkCode) {
		CheckCode = checkCode;
	}
	public String getAuthCode() {
		return AuthCode;
	}
	public void setAuthCode(String authCode) {
		AuthCode = authCode;
	}
	public String getOther() {
		return Other;
	}
	public void setOther(String other) {
		Other = other;
	}
	public String getCreateTime() {
		return CreateTime;
	}
	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}

}
