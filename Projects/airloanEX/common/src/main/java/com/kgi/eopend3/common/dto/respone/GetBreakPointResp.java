package com.kgi.eopend3.common.dto.respone;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetBreakPointResp {

    private String breakPointPage = "";
    private String phone = "";
    private String chtName = "";
    private String isVerify = "";
    private String nexttime = "";
    private String url = "";
    private String verifyType = "";
    
}