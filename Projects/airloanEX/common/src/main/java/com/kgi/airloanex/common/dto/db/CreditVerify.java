package com.kgi.airloanex.common.dto.db;

public class CreditVerify {
	
	private String UniqId="";
	/** 唯一代號類型   | 01:體驗 02:申請 03:立約 04:專人聯絡 05:信用卡 */
	private String UniqType="";
	/** 驗證狀態       | 0:驗證失敗 1:驗證成功 */
	private String VerifyStatus="";
	/** 驗證類型       | 0:信用卡 1:存款帳戶 2:行動門號 4:線下驗身 */
	private String VerifyType="";
	/** 信用卡卡號     | 卡號範例 1111********2222 */
	private String CardNo="";
	/** 信用卡到期年月 | 到期範例 : MMYY 月份 西元年後兩碼 */
	private String CardTime="";
	/** 存款銀行 */
	private String BankId="";
	/** 存款帳號 */
	private String AccountNo="";
	/** 錯誤次數       | 只能錯三次 所以最多到3 */
	private Integer ErrorCount;
	/** 新增時間 */
	private String CreateTime="";
	/** 更新時間 */
	private String UpdateTime ="";

	private Integer NCCCErrorCount = 0;
	private Integer Pcode2566ErrorCount = 0;

	// private String MyCard;
	
	public CreditVerify() {}	
	
	public CreditVerify(String UniqId){
		this.UniqId = UniqId;
	}
	
	public String getUniqId() {
		return UniqId;
	}
	public String getUniqType() {
		return UniqType;
	}
	public String getVerifyStatus() {
		return VerifyStatus;
	}
	public String getVerifyType() {
		return VerifyType;
	}
	public String getCardNo() {
		return CardNo;
	}
	public String getCardTime() {
		return CardTime;
	}
	public String getBankId() {
		return BankId;
	}
	public String getAccountNo() {
		return AccountNo;
	}
	
	public String getCreateTime() {
		return CreateTime;
	}
	public String getUpdateTime() {
		return UpdateTime;
	}
	public void setUniqId(String uniqId) {
		UniqId = uniqId;
	}
	public void setUniqType(String uniqType) {
		UniqType = uniqType;
	}
	public void setVerifyStatus(String verifyStatus) {
		VerifyStatus = verifyStatus;
	}
	public void setVerifyType(String verifyType) {
		VerifyType = verifyType;
	}
	public void setCardNo(String cardNo) {
		CardNo = cardNo;
	}
	public void setCardTime(String cardTime) {
		CardTime = cardTime;
	}
	public void setBankId(String bankId) {
		BankId = bankId;
	}
	public void setAccountNo(String accountNo) {
		AccountNo = accountNo;
	}
	
	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}
	public void setUpdateTime(String updateTime) {
		UpdateTime = updateTime;
	}
	public Integer getErrorCount() {
		return ErrorCount;
	}
	public Integer getNCCCErrorCount() {
		return NCCCErrorCount;
	}
	public Integer getPcode2566ErrorCount() {
		return Pcode2566ErrorCount;
	}
	public void setErrorCount(Integer errorCount) {
		ErrorCount = errorCount;
	}
	public void setNCCCErrorCount(Integer nCCCErrorCount) {
		NCCCErrorCount = nCCCErrorCount;
	}
	public void setPcode2566ErrorCount(Integer pcode2566ErrorCount) {
		Pcode2566ErrorCount = pcode2566ErrorCount;
	}
}
