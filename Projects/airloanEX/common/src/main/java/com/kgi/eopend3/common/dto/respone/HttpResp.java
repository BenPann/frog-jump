package com.kgi.eopend3.common.dto.respone;

public class HttpResp {

	private int statusCode = 0 ;
	private String response = "" ;
	
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	
	
	
	
	
}
