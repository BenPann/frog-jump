package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class CaseDocument {
    
    String UniqId = "";
    
    /** 01:體驗 02:申請 03:立約 04:專人聯絡 05:信用卡 */
    String UniqType = "";

    /*
    * 拒貸PDF: loan.rejectpdf
    * KYC表PDF: loan.kyc.pdf
    * 貸款PDF: loan.applypdf
    * 立約PDF: contract.pdf
    * 信用卡PDF: credit.applypdf
    */
    String DocumentType = "";
    
    byte[] Content = null;
    
    String Version = "";
    
    String Other = "";

    public CaseDocument() {
    }

    public CaseDocument(String uniqId, String uniqType, String documentType) {
        this.setUniqId(uniqId);
        this.setUniqType(uniqType);
        this.setDocumentType(documentType);
    }
}
