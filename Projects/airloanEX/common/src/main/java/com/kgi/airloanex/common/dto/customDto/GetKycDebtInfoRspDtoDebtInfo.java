package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetKycDebtInfoRspDtoDebtInfo {

   /** 房貸註記(1.本行2.他行3.皆有) */
   private String MORTAGAGE_FLAG;

   /** 房貸餘額 */
   private String MORTAGAGE_AMT;

   /** 房貸每月應繳金額 */
   private String MORTAGAGE_PAY;

   /** 車貸註記(1.本行2.他行3.皆有) */
   private String CAR_LOAN_FLAG;

   /** 車貸餘額 */
   private String CAR_LOAN_AMT;

   /** 車貸每月應繳金額 */
   private String CAR_LOAN_PAY;

   /** 信貸註記(1.本行2.他行3.皆有) */
   private String PLOAN_FLAG;

   /** 信貸餘額 */
   private String PLOAN_AMT;

   /** 信貸每月應繳金額 */
   private String PLOAN_PAY;

   /** 信用卡註記(1.本行2.他行3.皆有) */
   private String CREDIT_CARD_FLAG;

   /** 信用卡餘額 */
   private String CREDIT_CARD_AMT;

   /** 信用卡付款方式(1.全清2.循環) */
   private String CREDIT_CARD_PAY_KIND;

   /** 信用卡循環金額 */
   private String CREDIT_CARD_CYCLE_AMT;

   /** 現金卡註記(1.本行2.他行3.皆有) */
   private String CASH_CARD_FLAG;

   /** 現金卡動用餘額  */
   private String CASH_CARD_AMT;

}
