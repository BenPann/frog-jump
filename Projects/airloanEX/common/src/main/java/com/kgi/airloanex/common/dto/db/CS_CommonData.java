package com.kgi.airloanex.common.dto.db;

public class CS_CommonData {
	private String UniqId = "";
	private String UniqType = "";
	private String CSType = "";
	private String ChannelId = "";
	private String ResultData = "";
	private String UpdateTime = "";
	private String CreateTime = "";
	
	public CS_CommonData() {
    }

    public CS_CommonData(String uniqId) {
        this.setUniqId(uniqId);
    }
	
	public String getUniqId() {
		return UniqId;
	}
	public String getUniqType() {
		return UniqType;
	}
	public String getCSType() {
		return CSType;
	}
	public String getChannelId() {
		return ChannelId;
	}
	public String getResultData() {
		return ResultData;
	}
	public String getUpdateTime() {
		return UpdateTime;
	}
	public String getCreateTime() {
		return CreateTime;
	}
	public void setUniqId(String uniqId) {
		UniqId = uniqId;
	}
	public void setUniqType(String uniqType) {
		UniqType = uniqType;
	}
	public void setCSType(String cSType) {
		CSType = cSType;
	}
	public void setChannelId(String channelId) {
		ChannelId = channelId;
	}
	public void setResultData(String resultData) {
		ResultData = resultData;
	}
	public void setUpdateTime(String updateTime) {
		UpdateTime = updateTime;
	}
	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}
}
