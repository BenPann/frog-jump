package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChatBotPageTeTrigger {
	// private String serial;
	private String FlowType;
	private String ProductType;
	private String PageName;
	private String SubPageName;
	private String TeTrigger;
	private String Enable;
	private String Sort;
	private String Welcome;
}
