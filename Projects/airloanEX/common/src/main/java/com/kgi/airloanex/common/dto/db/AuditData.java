package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuditData {
    private String UniqId;
	private String UniqType;
	private String ProjectCode;
	private String CreditLine;
	private String IntrestRate;
	private String Rank;
	private String Score;
	private String ShowScore;
	private String UTime;
}
