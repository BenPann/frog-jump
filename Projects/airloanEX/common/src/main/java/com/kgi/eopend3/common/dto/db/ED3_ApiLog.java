package com.kgi.eopend3.common.dto.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class ED3_ApiLog {

    private String Type;   
    private String KeyInfo;  
    private String ApiName;  
    private String Request;  
    private String Response;  
    private String StartTime; 
    private String EndTime;

}