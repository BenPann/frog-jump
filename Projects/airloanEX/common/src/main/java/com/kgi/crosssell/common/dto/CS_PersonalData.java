package com.kgi.crosssell.common.dto;

public class CS_PersonalData {
	
	private String UniqId= "";
    private String UniqType= "";
    private String HasAum="";
    private String UUId="";
    private String UpdateAum="";
    private String Idno="";
    private String Name="";
    private String Birthday="";
    private String Mobile="";
    private String EMail="";
    private String CommAddrZipCode = "";
    private String CommAddress="";
    private String ResTelArea="";
    private String ResPhone="";
    private String CreateTime="";
    
    private String AumStatus ;
    
    
    public CS_PersonalData(){};

	public CS_PersonalData(String UniqId){
		this.UniqId = UniqId;
	}
    
    
	public String getUniqId() {
		return UniqId;
	}
	public void setUniqId(String uniqId) {
		UniqId = uniqId;
	}
	public String getUniqType() {
		return UniqType;
	}
	public void setUniqType(String uniqType) {
		UniqType = uniqType;
	}
	public String getHasAum() {
		return HasAum;
	}
	public void setHasAum(String hasAum) {
		HasAum = hasAum;
	}
	public String getUpdateAum() {
		return UpdateAum;
	}
	public void setUpdateAum(String updateAum) {
		UpdateAum = updateAum;
	}
	public String getIdno() {
		return Idno;
	}
	public void setIdno(String idno) {
		Idno = idno;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getBirthday() {
		return Birthday;
	}
	public void setBirthday(String birthday) {
		Birthday = birthday;
	}
	public String getMobile() {
		return Mobile;
	}
	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	public String getEMail() {
		return EMail;
	}
	public void setEMail(String eMail) {
		EMail = eMail;
	}
	public String getCommAddress() {
		return CommAddress;
	}
	public void setCommAddress(String commAddress) {
		CommAddress = commAddress;
	}
	public String getResPhone() {
		return ResPhone;
	}
	public void setResPhone(String resPhone) {
		ResPhone = resPhone;
	}
	public String getCreateTime() {
		return CreateTime;
	}
	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}
	public String getUUId() {
		return UUId;
	}
	public String getCommAddrZipCode() {
		return CommAddrZipCode;
	}
	public String getResTelArea() {
		return ResTelArea;
	}
	public void setUUId(String uUId) {
		UUId = uUId;
	}
	public void setCommAddrZipCode(String commAddrZipCode) {
		CommAddrZipCode = commAddrZipCode;
	}
	public void setResTelArea(String resTelArea) {
		ResTelArea = resTelArea;
	}

	public String getAumStatus() {
		return AumStatus;
	}

	public void setAumStatus(String aumStatus) {
		AumStatus = aumStatus;
	}
	
	
}
