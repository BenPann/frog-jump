package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class GetDgtcaseInfoReqDto {
    
    /** (1)	CASE_NO：案件編號(2.4功能取得的案件編號)，CHAR(12) */
    private String CASE_NO;
}
