package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class UspGetCddRate {
    private String SystemType;
    private String PARTY_NUMBER;
    private String PARTY_NAME;
    private String Risk_Classification;
    private String Risk;
    private String QueryDateTime;
}
