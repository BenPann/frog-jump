package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CheckJcicResultReqDto {

    /** (1)	JCIC_HTML：聯徵判讀結果(string) */
    private String JCIC_HTML;
}
