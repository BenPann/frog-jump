package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpBankView {
    private String expBankCode;
    private String expBankAcctNo;
}
