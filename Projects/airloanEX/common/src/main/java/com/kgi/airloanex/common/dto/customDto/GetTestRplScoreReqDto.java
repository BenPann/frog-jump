package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class GetTestRplScoreReqDto {
    /** (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string) */
    private String CASE_NO;
    /** (2)	MARRIAGE：婚姻狀況(1.未婚2.已婚)，(string) */
    private String MARRIAGE;
    /** (3)	JOBYEAR：工作年資，(string) */
    private String JOBYEAR;
    /** (4)	AGE：年齡，(string) */
    private String AGE;
    /** (5)	SEX：性別(1.男2.女)，(string) */
    private String SEX;
    /** (6)	EDUCATION：教育程度(1.高中以下2.大專3.大學4.研究所以上)，(string) */
    private String EDUCATION;
    /** (7)	YEARLY_INCOME：年收入(單位:萬元)，(string) */
    private String YEARLY_INCOME;
    /** (8)	CORP_TYPE：行業別代號，(string) */
    private String CORP_TYPE;
    /** (9)	CORP_TITLE：職務名稱代碼，(string) */
    private String CORP_TITLE;
}
