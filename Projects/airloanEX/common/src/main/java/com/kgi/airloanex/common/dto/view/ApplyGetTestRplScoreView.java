package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplyGetTestRplScoreView {
   
    private String ProductType;

    private String MARRIAGE;
    private String JOBYEAR;
    private String AGE;
    private String SEX;
    private String EDUCATION;
    private String YEARLY_INCOME;
    private String CORP_TYPE;
    private String CORP_TITLE;
}
