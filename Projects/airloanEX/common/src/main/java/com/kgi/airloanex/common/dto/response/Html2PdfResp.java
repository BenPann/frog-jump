package com.kgi.airloanex.common.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Html2PdfResp {
    private String RTN_CODE;
    private String MESSAGE;
    private String FILE_PATH;
    private String FILE_NAME;
    private String PAGE;
    private String TIME;

}