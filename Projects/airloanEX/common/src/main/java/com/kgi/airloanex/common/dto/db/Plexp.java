package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class Plexp {
    /** 體驗編號 */
    @NonNull
    private String ExpNo;

    /** 手機號碼 */
    private String Phone;

    /** yyyyMMdd 西元年月日 */
    private String Date;

    /** 姓名 */
    private String Name;

    /** 案件編號(AIRLOAN 產的) */
    private String CaseNoWeb;

    /** 案件編號(KGI 產的) */
    private String CaseNo;

    /** 問題版本 */
    private String QVer;

    /** 問卷答案列表 */
    private String AnsList;

    /** 專案代號  */
    private String ProjectCode;

    /** 建議額度 */
    private String CreditLine;

    /** 建議利率 */
    private String IntrestRate;

    /** RANK */
    private String Rank;
    
    /** 分數 */
    private String Score;

    /** 更新時間 */
    private String UTime;
    
    private String ExpProductType;
    private String Sex;
    private String age;
    private String edu;
    private String marriage;
    private String jobType;
    private String workyear;
    private String yearincome;
    private String IPAddress;
}
