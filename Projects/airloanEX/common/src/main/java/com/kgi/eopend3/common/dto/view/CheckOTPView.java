package com.kgi.eopend3.common.dto.view;


import com.google.gson.annotations.SerializedName;
import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

public class CheckOTPView {

    private String idno = "";
    @SerializedName(value="opId", alternate={"opID"})
    private String opId = "";
    private String billDep = "";

    @CheckNullAndEmpty
    private String otp = "";
    @CheckNullAndEmpty
    private String sk = "";
    @CheckNullAndEmpty
    @SerializedName(value="txnId", alternate={"txnID"})
    private String txnId = "";
    @CheckNullAndEmpty
    private String txnDate = "";
    @CheckNullAndEmpty
    private String phone = "";


    
    public String getIdno() {
		return idno;
	}

	public void setIdno(String idno) {
		this.idno = idno;
	}

	public String getOpId() {
		return opId;
	}

	public void setOpId(String opId) {
		this.opId = opId;
	}

	public String getBillDep() {
		return billDep;
	}

	public void setBillDep(String billDep) {
		this.billDep = billDep;
	}

	public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getSk() {
        return sk;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


}