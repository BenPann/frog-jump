package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CheckNoFinpfRspDto {
    
    /*    
     * (1)	CHECK_CODE：
     * A.	檢核資料錯誤(空白代表沒有錯誤)
     * B.	如果有多個錯誤用『;』區隔,例如01;02
     * C.	『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
     * D.	錯誤代碼對照表:
     * 代碼  說明
     * ============================
     * 01	   案件編號格式錯誤
     * 02	   案件編號不存在
     * 03	   產品別格式錯誤
     * 04	   是否為數位案件格式錯誤
     * 05	   是否為秒貸案件格式錯誤
     * 06	   行業別格式錯誤
     * 07	   公司名稱格式錯誤
     * 08	   職稱格式錯誤
     * 09	   職務名稱代碼格式錯誤
     * 10	   客戶類型格式錯誤
     * 11	   洗錢AML比對結果格式錯誤
     * 12	   申請金額格式錯誤
     * 99	  非預期性系統異常
     */
    private String CHECK_CODE;

    /*
     * (2)	ERR_MSG：
     * A.	當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
     */
    private String ERR_MSG;

    /*
     * (3)	RESULT_BEFOR：
     * A.	 回應代碼:CHAR(1)，Y：免財力證明，N：需要財力證明
     */
    private String RESULT_BEFOR;

    /*
     * (4)	RESULT_AFTER：
     * A.	回應代碼:CHAR(1)，Y：免財力證明，N：需要財力證明
     */
    private String RESULT_AFTER;

    /** (5)	YEARLY_INCOME：年收入(單位:萬元)，(string) */
    private String YEARLY_INCOME;

    /** (6)	MEMO：備註 */
    private String MEMO;

    /** (7)	RESULT_CODE：結果代碼 */
    private String RESULT_CODE;

}
