package com.kgi.crosssell.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Portfolios{
    /** 財力基準日 */
    private String baseDate;

    /** 幣別 */
    private String currency;

    /** 市場項目 */
    private String marketItem;

    /** 財力價值合計 */
    private String valueSum;

}