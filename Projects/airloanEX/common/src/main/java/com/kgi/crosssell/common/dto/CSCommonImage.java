package com.kgi.crosssell.common.dto;

public class CSCommonImage {
  private String idCardFront = "";
  private String idCardBack = "";
  private String businessCard = "";
  private String[] financialProof;

  public String getIdCardFront() {
    return idCardFront;
  }

  public void setIdCardFront(String value) {
    this.idCardFront = value;
  }

  public String getIdCardBack() {
    return idCardBack;
  }

  public void setIdCardBack(String value) {
    this.idCardBack = value;
  }

  public String getBusinessCard() {
    return businessCard;
  }

  public void setBusinessCard(String value) {
    this.businessCard = value;
  }

  public String[] getFinancialProof() {
    return financialProof;
  }

  public void setFinancialProof(String[] value) {
    this.financialProof = value;
  }
}