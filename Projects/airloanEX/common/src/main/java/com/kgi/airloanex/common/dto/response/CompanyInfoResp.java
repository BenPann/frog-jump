package com.kgi.airloanex.common.dto.response;

import java.util.List;

import com.kgi.airloanex.common.dto.customDto.CompanyInfoRspDtoData;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyInfoResp {
    
    private String Code;

    private String Message;

    private CompanyInfoRspDtoData Data;
}
