package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class UserPhoto {
    /**
     * 序號
     */
    private String UniqId = "";

    /**
     * 線上還是線下件
     * 1 線上件
     * 2 線下件
     */
    private String Online = "";

    /**
     * 流水號
     */
    private int SubSerial;

    /**
     * 圖片狀態
     * | 代碼 | 中文                                    |
     * | ---- | --------------------------------------- |
     * | 0    | 暫存用(使用完之後在轉處理中或轉別TABLE) |
     * | 1    | 使用者處理中                            |
     * | 2    | 等待上傳中                              |
     * | 3    | 上傳成功待刪除                          |
     * | 9    | 上傳失敗                                |
     * | 10   | 暫存信用卡案件不上傳影像的圖片          |
     */
    private String Status = "";

    /**
     * 產品別
     * | 代碼 | 中文               |
     * | ---- | ------------------ |
     * | 1    | 信用卡線上申辦     |
     * | 2    | 信貸線上申辦       |
     * | 3    | 銀行線上開戶(薪轉) |
     * | 4    | 證券線上開戶       |
     * | 5    | 期貨線上開戶       |
     */
    private String ProdType = "";


    /**
     * 主分類
     * | 代碼 | 中文         |
     * | ---- | ------------ |
     * | 0    | 無主分類     |
     * | 1    | 身分證明文件 |
     * | 2    | 財力證明文件 |
     */
    private int PType;

    /**
     * 次分類
     * | 代碼 | 中文         |
     * | ---- | ------------ |
     * | 0    | 無次分類     |
     * | 1    | 身分證正面   |
     * | 2    | 身分正反面   |
     * | 3    | 健保卡正面   |
     * | 4    | 健保卡反面   |
     * | 5    | 駕照正面     |
     * | 6    | 駕照反面     |
     * | 20   | 薪資轉帳證明 |
     * | 21   | 土地謄本     |
     * | 22   | 存款證明     |
     * | 23   | 扣繳憑單     |
     * | 24   | 所得清單     |
     * | 25   | AUM          |
     */
    private int SType;

    private String ProductId = "";

    private String UnitId = "";

    /**
     * 使用者上傳的圖片(大)
     */
    private byte[] ImageBig = new byte[] {};

    /**
     * 使用者上傳的圖片(小)
     */
    private byte[] ImageSmall = new byte[] {};

    /**
     * 新增時間
     */
    private String CreatTime = "";

    public UserPhoto() {}

    public UserPhoto(String uniqId, String online, int subSerial) {
        this.UniqId = uniqId;
        this.Online = online;
        this.SubSerial = subSerial;
    }
}