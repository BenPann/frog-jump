package com.kgi.airloanex.common.dto.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyFullNameResp {
    
    private String Code;

    private String Message;

    private List<String> Data;
}
