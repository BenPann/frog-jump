package com.kgi.eopend3.common.dto.view;

import com.google.gson.annotations.SerializedName;
import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

public class SendOTPView {
	
	@CheckNullAndEmpty
    private String phone = "";

    private String idno = "";
    private String opId = "";
    private String billDep = "";
    @SerializedName(value="txnId", alternate={"txnID"})
    private String txnId = "";


    
    public String getIdno() {
		return idno;
	}

	public void setIdno(String idno) {
		this.idno = idno;
	}

	public SendOTPView(String phone) {
        this.phone = phone;
    }
	
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    
	public String getOpId() {
		return opId;
	}

	public void setOpId(String opId) {
		this.opId = opId;
	}

	public String getBillDep() {
		return billDep;
	}

	public void setBillDep(String billDep) {
		this.billDep = billDep;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

}