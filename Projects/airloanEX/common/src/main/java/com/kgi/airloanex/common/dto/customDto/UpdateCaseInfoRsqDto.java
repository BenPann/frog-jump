package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateCaseInfoRsqDto {

    // CHECK_CODE：
    // 檢核資料錯誤(空白代表沒有錯誤)
    // 如果有多個錯誤用『;』區隔,例如01;02
    // 『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
    // 錯誤代碼對照表:
    // 代碼 說明
    // ============================
    // 案件編號格式錯誤
    // 案件編號不存在
    // 99 非預期性系統異常
    private String CHECK_CODE;
    // ERR_MSG：
    // 當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
    private String ERR_MSG;

}
