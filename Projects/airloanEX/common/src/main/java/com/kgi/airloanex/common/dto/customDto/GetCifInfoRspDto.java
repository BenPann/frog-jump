package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetCifInfoRspDto {

    /* (5)	CHECK_CODE：
     * A.	『0』檢核資料錯誤(空白代表沒有錯誤)
     * B.	『10』代表查無資料
     * C.	『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
     * (6)	ERR_MSG：
     * 當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
     */
    private String CHECK_CODE;

    /** (7)	CUST_ID：客戶ID，VARCHAR(11) */
    private String CUST_ID;

    /** (8)	NAME：戶名，VARCHAR(40) */
    private String NAME;

    /** (9)	EML_ADDR：電子郵件，VARCHAR(40) */
    private String EML_ADDR;

    /** (10)	MOBILE_TEL：行動電話號碼:VARCHAR(20) */
    private String MOBILE_TEL;

    /** (11)	ADR_ZIP：現居地址郵遞區號，VARCHAR(5) */
    private String ADR_ZIP;

    /** (12)	ADR_ZIP_1: 戶籍地址郵遞區號，VARCHAR(5) */
    private String ADR_ZIP_1;

    /** (13)	ADR：現居地址，VARCHAR(64) */
    private String ADR;

    /** (14)	ADR_1：戶籍地址，VARCHAR(64) */
    private String ADR_1;

    /** (15)	TEL_NO_AREACODE：現居地址電話區碼，VARCHAR(3) */
    private String TEL_NO_AREACODE;

    /** (16)	TEL_NO_AREACODE_1：戶籍地址電話區碼，VARCHAR(3) */
    private String TEL_NO_AREACODE_1;

    /** (17)	TEL_NO：現居地址電話，VARCHAR(8) */
    private String TEL_NO;

    /**  */
    private String TEL_NO_EXT;

    /** (18)	TEL_NO_1: 戶籍地址電話，VARCHAR(8) */
    private String TEL_NO_1;

    /**  */
    private String TEL_NO_EXT_1;

    /** (19)	EDU：教育程度(1.高中以下2.大專3.大學4.研究所以上)，CHAR(1) */
    private String EDU;

    /** (20)	MARRIAGE：婚姻狀況(1.未婚2.已婚)，CHAR(1) */
    private String MARRIAGE;

    /** (21)	COMPANY_NAME：公司名稱，VARCHAR(60) */
    private String COMPANY_NAME;

    /** (22)	COMPANY_ADDR_ZIP：公司地址郵遞區號，VARCHAR(5) */
    private String COMPANY_ADDR_ZIP;

    /** (23)	COMPANY_ADDR：公司地址，VARCHAR(150) */
    private String COMPANY_ADDR;

    /** (24)	COMPANY_TEL_AREACODE：公司電話區碼，VARCHAR(3) */
    private String COMPANY_TEL_AREACODE;

    /** (25)	COMPANY_TEL：公司電話，VARCHAR(30) */
    private String COMPANY_TEL;

    /**  */
    private String COMPANY_TEL_EXT;

    /** (26)	OCCUPATION：職稱，VARCHAR(60) */
    private String OCCUPATION;

    /** (27)	ON_BOARD_DATE：到職年月日，CHAR(8) */
    private String ON_BOARD_DATE;

    /** (28)	INCOME：年收入，INT */
    private String INCOME;

    /* (29)	HOUSE_OWNER：不動產狀況，VARCHAR(1)
     * 代碼		說明
     * ============================
     * 1	 	本人所有
     * 2	 	家族所有
     * 3		配偶所有
     * 4		無    
     */
    private String HOUSE_OWNER;
}