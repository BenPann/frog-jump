package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetTestRplScoreRspDtoScoreInfo {
    
    /** 專案代號 */
    private String PROJECT_NO;
    /** 評分模型類型 1.未婚 2.已婚 */
    private String CARD_TYPE;
    /** 分數 */
    private String TOTAL_SCORE;
    /** RANK */
    private String SCORE_RANK;
    /** 利率 */
    private String INTEREST_RATE;
    /** 建議額度 */
    private String CREDIT_LINE;
}
