package com.kgi.airloanex.common.dto.soap;

public class SendMsgSoapParameter {
	public String cTobranch = "";		//分行代號
    public String cTitle = "";			//標頭資訊
    public String iMessageColor = "";	//0:黑 1:紅 2:綠 3:藍
    public String cMessage = "";		//訊息內容
    
    public SendMsgSoapParameter(String cTobranch, String cTitle, String iMessageColor, String cMessage) {
        this.cTobranch = cTobranch;
        this.cTitle = cTitle;
        this.iMessageColor = iMessageColor;
        this.cMessage = cMessage;
    }
}
