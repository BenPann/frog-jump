package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
public class UploadFileDto {

    /** I001：申請書  */
    public static final String I001 = "I001";
    /** I002：ID */
    public static final String I002 = "I002";
    /** I003：財力證明 */
    public static final String I003 = "I003";
    /** I999：其他 */
    public static final String I999 = "I999";

    /** 
     * I001：申請書 
     * I002：ID
     * I003：財力證明
     * I999：其他
     */
    @NonNull
    private String imgType = "";
    
    @NonNull
    private String filename = "";
    
}
