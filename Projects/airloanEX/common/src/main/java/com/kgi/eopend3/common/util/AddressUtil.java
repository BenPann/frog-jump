package com.kgi.eopend3.common.util;

public class AddressUtil {
	private static final String SPILTER = ";" ;
	
	
    public static String[] addressHandler(String address){
        int times = 0;
        StringBuilder sb = new StringBuilder();
        String[] strings = address.split("");

        for(String s : strings){
            if(times < 2) {
                switch (s) {
                    case "縣":
                    case "市":
                    case "區":
                    case "鄉":
                    case "鎮":
                    	if (times == 1) { 
                    		sb.append(s).append(SPILTER);
                    	} else {
                    		sb.append(s);
                    	}
                        times++;
                        break;
                    default:
                        sb.append(s);
                }
            } else {
                sb.append(s);
            }
        }
        return sb.toString().split(SPILTER);
    }

    
    public static void main(String argc[]) {
    	System.out.println(AddressUtil.addressHandler("新竹市東區中山路二段200號3樓之3")[0]);
    	System.out.println(AddressUtil.addressHandler("新竹市東區中山路二段200號3樓之3")[1]);
    	
    	
    }
    
    
}
