package com.kgi.eopend3.common.dto.customDto;

import com.google.gson.GsonBuilder;

public class ChinalifeSendPolicyInfoRs {
	private String rtncode = "" ;
	private String message = "" ;
	public String getRtncode() {
		return rtncode;
	}
	public void setRtncode(String rtncode) {
		this.rtncode = rtncode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	
	public String toJsonString() {
		return new GsonBuilder().disableHtmlEscaping().create().toJson(this) ;
	}
	
}
