package com.kgi.eopend3.common.dto.respone;

public class PCode2566Resp {
    
    private String pcode2566CheckCode = "";    
    private String pcode2566Message = "";
    private Integer errorCount ;
    private Integer ncccErrorCount ;
    private Integer pcode2566ErrorCount ;

    public String getPcode2566CheckCode() {
        return pcode2566CheckCode;
    }

    public void setPcode2566CheckCode(String pcode2566CheckCode) {
        this.pcode2566CheckCode = pcode2566CheckCode;
    }

    public String getPcode2566Message() {
        return pcode2566Message;
    }

    public void setPcode2566Message(String pcode2566Message) {
        this.pcode2566Message = pcode2566Message;
    }

    public Integer getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(Integer errorCount) {
        this.errorCount = errorCount;
    }

    public Integer getNcccErrorCount() {
        return ncccErrorCount;
    }

    public void setNcccErrorCount(Integer ncccErrorCount) {
        this.ncccErrorCount = ncccErrorCount;
    }

    public Integer getPcode2566ErrorCount() {
        return pcode2566ErrorCount;
    }

    public void setPcode2566ErrorCount(Integer pcode2566ErrorCount) {
        this.pcode2566ErrorCount = pcode2566ErrorCount;
    }
   

}