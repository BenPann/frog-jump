package com.kgi.eopend3.common.dto.view;

public class DropDownView  {

    private String name = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}