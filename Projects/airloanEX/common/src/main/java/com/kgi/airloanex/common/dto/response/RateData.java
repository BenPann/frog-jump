package com.kgi.airloanex.common.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RateData {
    
    // (5)	RATE_DATA：

    /** A.	pay_rate_m：期間(月) */
    private String pay_rate_m;

    /** B.	pay_rate_date：生效日(YYYYMMDD) */
    private String pay_rate_date;

    /** C.	pay_rate：帳戶利率 */
    private String pay_rate;
    
}
