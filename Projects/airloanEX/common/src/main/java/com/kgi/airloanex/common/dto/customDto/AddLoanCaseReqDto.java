package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AddLoanCaseReqDto {
    
    /** (1)	CUSTOMER_ID：客戶ID，(string) */
    private String CUSTOMER_ID;
    
    /** (2)	DATA_TYPE：介接系統(AA.信用卡系統,BB.PLOAN徵審系統,CC.數位申辦平台)，(string) */
    private String DATA_TYPE;
    
    /** (3)	CASE_NO_REF：介接系統案件編號(卡加貸串接WEB端UUID)，CHAR(10) */
    private String CASE_NO_REF;

}
