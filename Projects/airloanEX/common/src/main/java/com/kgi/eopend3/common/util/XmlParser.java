package com.kgi.eopend3.common.util;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class XmlParser {
    @SuppressWarnings("all")
    public static Map<String,String> parse(String xml){
        Map<String, String> rsMap = new HashMap<>();
        Iterator iterator = null;
        try {
            SOAPMessage msg = formatSoapString(xml);
            SOAPBody body = msg.getSOAPBody();
            iterator = body.getChildElements();

        } catch (Exception e) {
            e.printStackTrace();
        }
        convertBodyToMap(iterator,rsMap);
        return rsMap;
    }

    public static String parseByTagName(String xml,String tagName){
        Map<String, String> parse = parse(xml);
        String rs = parse.get(tagName);
        return rs;
    }




    private static SOAPMessage formatSoapString(String soapString) {
        MessageFactory msgFactory;
        try {
            msgFactory = MessageFactory.newInstance();
            SOAPMessage reqMsg = msgFactory.createMessage(new MimeHeaders(),
                    new ByteArrayInputStream(soapString.getBytes("UTF-8")));
            reqMsg.saveChanges();
            return reqMsg;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    @SuppressWarnings("all")
    private static Map<String, String> convertBodyToMap(Iterator iterator,Map<String, String>rsMap) {

        if(null == iterator)
            return rsMap;


        while (iterator.hasNext()) {
            SOAPElement element = null;
            try {
                element = (SOAPElement) iterator.next();
            } catch (Exception e) {
                continue;
            }
            if ((null == element.getValue() || element.getValue().trim().equals("")) && element.getChildElements().hasNext()) {
                convertBodyToMap(element.getChildElements(),rsMap);
            }
            rsMap.put(element.getTagName(),element.getValue());
        }
        return rsMap;
    }

    public static Document parseToDoc(String xml) throws IOException, ParserConfigurationException, SAXException {
        InputStream inputStream = IOUtils.toInputStream(xml, "UTF-8");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputStream);
        doc.getDocumentElement().normalize();
        return doc;
    }
}
