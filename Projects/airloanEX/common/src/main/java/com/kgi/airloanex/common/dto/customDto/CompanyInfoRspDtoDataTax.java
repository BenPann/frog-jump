package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyInfoRspDtoDataTax {
    
    private String Capital;

    private String RegisterDate;

    private String CompanyType;

    private String IndustryTypeCode;

    private String IndustryType;

}
