package com.kgi.crosssell.common.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CSCrossBusinessApply{

    /** 訊息代碼 */
    private String rtnCode;

    /** 訊息內容 */
    private String msg;

    /** 序號 */
    private String rtnSeq;

    /** 業務種類 */
    private String businessKind;

    /** 財力證明種類 */
    private String financialProofKind;

    /** 身分證字號 */
    private String idno;

    /** 申請日期時間 */
    private String applyDateTime;

    /** CA Sert */
    private String caCert;

    /** 契約版號 */
    private String contractVer;

    /** IP 位址 */
    private String ip;
    
    private List<Portfolios> portfolios;

    private List<Attachments> attachments;
}