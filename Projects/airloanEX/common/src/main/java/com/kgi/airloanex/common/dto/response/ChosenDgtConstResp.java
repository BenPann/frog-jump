package com.kgi.airloanex.common.dto.response;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ChosenDgtConstResp {
    
    private GetDgtConstResp getDgtConstResp;
    private InitBankInfo initBankInfo;
    private List<RateData> rateDatas = new ArrayList<>();
}