package com.kgi.eopend3.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CorpDataView {

    @CheckNullAndEmpty
    private String corpName = "";
    @CheckNullAndEmpty
    private String corpTelArea = "";
    @CheckNullAndEmpty
    private String corpTel = "";
    @CheckNullAndEmpty(checkEmpty = "N")
    private String corpAddrZipCode = "";
    @CheckNullAndEmpty(checkEmpty = "N")
    private String corpAddrArea = "";
    @CheckNullAndEmpty(checkEmpty = "N")
    private String corpAddr = "";
    @CheckNullAndEmpty
    private String occupation = "";
    @CheckNullAndEmpty
    private String jobTitle = "";
    @CheckNullAndEmpty
    private String yearlyIncome = "";
    @CheckNullAndEmpty(checkEmpty = "N")
    private String onBoardDate = "";
    @CheckNullAndEmpty(checkEmpty = "N")
    private String injury = "";


}