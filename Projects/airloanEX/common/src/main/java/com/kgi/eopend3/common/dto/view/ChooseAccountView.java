package com.kgi.eopend3.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChooseAccountView {

    @CheckNullAndEmpty
    private String accountType;
    @CheckNullAndEmpty
    private String deliveryType;
    private String securitiesAddrZipCode;    
    private String securitiesAddr;
    private String securitiesCode;
    private String securitiesName;
}