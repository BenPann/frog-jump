package com.kgi.airloanex.common.dto.customDto;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@CheckNullAndEmpty
@AllArgsConstructor
@RequiredArgsConstructor
public class UpdateCaseInfoReqDto {

    /** (1)	CASE_NO：案件編號(2.23功能取得的案件編號)，(string) */
    @NonNull
    private String CASE_NO;

    /** (2)	HOUSE_REG_CITY_CODE：戶籍地址行政區代碼，(string) */
    private String HOUSE_REG_CITY_CODE;

    /** (3)	HOUSE_REG_ADDRESS：戶籍地址，(string) */
    private String HOUSE_REG_ADDRESS;

    /** (4)	HOUSE_REG_AREA：戶籍電話區碼，(string) */
    private String HOUSE_REG_AREA;

    /** (5)	HOUSE_REG_TEL：戶籍電話，(string) */
    private String HOUSE_REG_TEL;

    /** (6)	AML_FLG：洗錢職業類別比對結果(Y/N)，(string) */
    private String AML_FLG;

    /** (7)	AML_RESULT：洗錢AML比對結果(Y/V/N/O/P) Y-ID命中禁制名單、V-疑似高 風險名單、N-非高風險名單、新增兩個狀態code"O-系統異常無法比對、P-非當日即時查詢，(string) */
    private String AML_RESULT;

    /** (8)	COMP_CASE：是否為完整件(Y/N)，(string) */
    private String COMP_CASE;

    /** (9)	FAST_ID_FLG：免ID註記，不須上傳ID客戶標記為Y(Y/N)，(string) */
    private String FAST_ID_FLG;

}
