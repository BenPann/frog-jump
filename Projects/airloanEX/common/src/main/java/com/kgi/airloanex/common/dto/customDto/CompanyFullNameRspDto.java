package com.kgi.airloanex.common.dto.customDto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyFullNameRspDto {
    
    private String Code;

    private String Message;

    private List<String> Data;
}
