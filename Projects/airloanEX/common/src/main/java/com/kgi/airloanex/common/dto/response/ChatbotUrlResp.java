package com.kgi.airloanex.common.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChatbotUrlResp {
    
    private String productType;

    private String flowType;

    private String pageName;

    private String trigger;

    private String chatbotUrl;

    private String welcome;
}
