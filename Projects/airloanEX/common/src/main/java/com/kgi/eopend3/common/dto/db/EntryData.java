package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class EntryData {

    @NonNull
    private String UniqId;
    @NonNull
    private String UniqType;
    @NonNull
    private String Entry;

    private String Member = "";
    @NonNull
    private String Process;
    @NonNull
    private String Browser;
    @NonNull
    private String Platform;
    @NonNull
    private String OS;

    private String Other = "";
    
    private String UTime;

    public EntryData(String UniqId) {
        this.UniqId = UniqId;
    }
}
