package com.kgi.airloanex.common.dto.response;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @see LogonResp
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LogonInitResp {
    
    private String token;
}
