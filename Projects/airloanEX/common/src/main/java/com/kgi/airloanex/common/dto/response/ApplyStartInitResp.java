package com.kgi.airloanex.common.dto.response;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApplyStartInitResp {
    private String token;
    private CaseDataResp caseDataResp;
    private String breakPoint;

    private boolean caseCreated;
}
