package com.kgi.airloanex.common.dto.response;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * JWT驗證機制 - 
 * 將會 LogonResp.loginResp 
 * 轉換成 LogonInitResp.token
 * 
 * @see LogonInitResp
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LogonResp {
    
    private LoginResp loginResp;
}
