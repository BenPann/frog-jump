package com.kgi.crosssell.common.dto;

import com.kgi.airloanex.common.dto.db.CreditCaseDataExtend;

import net.sf.json.JSONObject;

public class UserObject {
    /**
     * 唯一ID
     */
    private String uniqId;
    /**
     * 唯一ID的類別
     */
    private String uniqType;
    /**
     * OTP是否檢核過的狀態
     */
    private Boolean otpStatus = false;
    /**
     * 上一頁從哪邊來(專人與我聯絡專用)
     */
    private String prevPage;
    /**
     * 客戶手機號碼(發OTP用)
     */
    private String phoneNum;
    private String txnId;
    private String txnDate;
    private String sk;
    /**
     * 客戶EMAIL(發專人與我聯絡用)
     */
    private String email;
    /**
     * 客戶資料(APS來的)
     */
    private JSONObject cifData;
    /**
     * 客戶申請案件的資料
     */
    private ApplyData applyData;

    private CreditCaseDataExtend creditCaseDataExtend;

    /**
     * 客戶姓名
     */
    private String name;
    /**
     * 身分證字號
     */
    private String idno;
    /**
     * 生日
     */
    private String birthday;
    /**
     * 共銷同意註記版本
     */
    private String autVerNo = "";
    /**
     * 共銷同意是否同意所有公司
     */
    private String autAuthorizeToAllCorp = "";
    /**
     * OTP_Source對應的名稱
     */
    private String otpSourceName = "";
    /**
     * IP
     */
    private String ipAddress = "";

    /**
     * 有無跨售AUM資料
     */
    private boolean hasCSAUMData = false;

    /**
     * 隱碼復原用ResTel
     */
    private String phoneNumber;

    public boolean getHasCSAUMData() {
        return hasCSAUMData;
    }

    public void setHasCSAUMData(boolean hasCSAUMData) {
        this.hasCSAUMData = hasCSAUMData;
    }

    public String getTxnId() {
        return txnId;
    }

    /**
     * @return the ipAddress
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * @param ipAddress the ipAddress to set
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * @return the birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * @param birthday the birthday to set
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     * @return the idno
     */
    public String getIdno() {
        return idno;
    }

    /**
     * @param idno the idno to set
     */
    public void setIdno(String idno) {
        this.idno = idno;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getSk() {
        return sk;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }

    public String getUniqId() {
        return uniqId;
    }

    public void setUniqId(String uniqId) {
        this.uniqId = uniqId;
    }

    public String getUniqType() {
        return uniqType;
    }

    public void setUniqType(String uniqType) {
        this.uniqType = uniqType;
    }

    public Boolean getOtpStatus() {
        return otpStatus;
    }

    public void setOtpStatus(Boolean otpStatus) {
        this.otpStatus = otpStatus;
    }

    public String getPrevPage() {
        return prevPage;
    }

    public void setPrevPage(String prevPage) {
        this.prevPage = prevPage;
    }

    public JSONObject getCifData() {
        return cifData;
    }

    public void setCifData(JSONObject cifData) {
        this.cifData = cifData;
    }

    public ApplyData getApplyData() {
        return applyData;
    }

    public void setApplyData(ApplyData applyData) {
        this.applyData = applyData;
    }

    public CreditCaseDataExtend getCreditCaseDataExtend() {
        return creditCaseDataExtend;
    }

    public void setCreditCaseDataExtend(CreditCaseDataExtend creditCaseDataExtend) {
        this.creditCaseDataExtend = creditCaseDataExtend;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getOtpSourceName() {
        return otpSourceName;
    }

    public void setOtpSourceName(String otpSourceName) {
        this.otpSourceName = otpSourceName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 共銷同意註記版本
     */
    public String getAutVerNo() {
        return autVerNo;
    }

    /**
     * 共銷同意註記版本
     */
    public void setAutVerNo(String autVerNo) {
        this.autVerNo = autVerNo;
    }

    public String getAutAuthorizeToAllCorp() {
        return autAuthorizeToAllCorp;
    }

    public void setAutAuthorizeToAllCorp(String autAuthorizeToAllCorp) {
        this.autAuthorizeToAllCorp = autAuthorizeToAllCorp;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
