package com.kgi.eopend3.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IDOCRRes {
    
    // 身份證正面
    // 範例電文:{"BYPASSID":"F224316254","IDCARD_ISSUE_City":"北市","Sex":"2","Birthday":"0570605","RspCode":"0","IDCARD_ISSUE_Type":"3","IDCARD_ISSUE_DT":"0940101","Idno":"A234567890","Name":"陳筱玲"}
    private String BYPASSID = "";
    private String IDCARD_ISSUE_City = "";
    private String Sex = "";
    private String Birthday = "";
    private String RspCode = "";
    private String IDCARD_ISSUE_Type = "";
    private String IDCARD_ISSUE_DT = "";
    private String Idno = "";
    private String Name = "";
    // 身份證反面
    // 範例電文:{"BYPASSID":"","Father":"陳德明","Mother":"吳春美","Military":"","Partner":"金大昇","Marriage":"已婚","Birthplace":"臺北市","HouseholdAddr1":"臺北市內湖區","HouseholdAddr2":"葫洲里01鄰","HouseholdAddr3":"民權東路六段","HouseholdAddr4":"283巷165弄218號"}
    private String HouseholdAddr1 = "";
    private String HouseholdAddr2 = "";
    private String HouseholdAddr3 = "";
    private String HouseholdAddr4 = "";
    private String Marriage = "";
}