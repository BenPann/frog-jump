package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CSGetDataView {
	private String prodid;
    private String channelId;
    private String token;
    private String idno;
    private String ip;
}