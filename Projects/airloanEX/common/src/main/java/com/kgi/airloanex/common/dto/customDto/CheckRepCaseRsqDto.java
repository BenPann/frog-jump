package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CheckRepCaseRsqDto {
    // CHECK_CODE：
    // 檢核資料錯誤(空白代表沒有錯誤)
    // 如果有多個錯誤用『;』區隔,例如01;02
    // 『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
    // 錯誤代碼對照表:
    // 代碼 說明
    // ============================
    // 客戶ID格式錯誤
    // 資料類型格式錯誤
    // 99 非預期性系統異常
    private String CHECK_CODE;
    // ERR_MSG：
    // 當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
    private String ERR_MSG;
    // RESULT：檢核結果(Y:檢核通過/N:檢核不通過)，CHAR(1)
    private String RESULT;

}
