package com.kgi.eopend3.common.dto.customDto;


public class EOpenFirstPostDto {

    private String count;
    private String uniqIdList;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getUniqIdList() {
        return uniqIdList;
    }

    public void setUniqIdList(String uniqIdList) {
        this.uniqIdList = uniqIdList;
    }
}
