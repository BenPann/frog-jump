package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcessTime {

	private String Serial;
	private String UniqId;
	private String UniqType;
	private String StartTime;
	private String EndTime;
	private String UpdateTime;
}