package com.kgi.eopend3.common.util;

import javax.servlet.http.HttpServletRequest;

public class RequestUtil {
    public static synchronized String getIpAddr(HttpServletRequest request) {
        
        String ip = request.getHeader("X-FORWARDED-FOR");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        // logger.info("Orinin IP" + ip);

        // IPV4的情況 把PORT號濾掉
        if (ip.split(":").length == 2) {
            ip = ip.split(":")[0];
        } else if (ip.split(":").length > 6) {
            // 如果是IP V6 先用此IP
            ip = "172.31.1.116";
        }
        // logger.info("Last IP" + ip);

        return ip;
    }
}
