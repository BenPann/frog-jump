package com.kgi.eopend3.common.dto.respone;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RepeatLoginResp {

    private List<String> phone;
    private List<String> emailAddress;

}