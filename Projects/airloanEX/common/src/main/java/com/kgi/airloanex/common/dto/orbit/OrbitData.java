package com.kgi.airloanex.common.dto.orbit;

public abstract class OrbitData implements OrbitDataObj{
	public static StringBuilder putXML(StringBuilder sb, String fieldName, String fieldValue){
		sb.append(' ');
		sb.append(fieldName);
		sb.append('=');
		sb.append('"');
		sb.append(fieldValue);
		sb.append('"');
		return sb;
	}
	
	public static StringBuilder putXML(StringBuilder sb, String fieldName, int fieldValue){
		return putXML(sb, fieldName, String.valueOf(fieldValue));
	}
}
