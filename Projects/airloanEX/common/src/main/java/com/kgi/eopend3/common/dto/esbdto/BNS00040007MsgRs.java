package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement(name = "MsgRs", namespace = "http://www.cosmos.com.tw/esb")
@XmlAccessorType(XmlAccessType.FIELD)
public class BNS00040007MsgRs {

    @XmlElement(name="SvcRs")
    private BNS00040007SvcRs bNS00040007SvcRs;

    public BNS00040007SvcRs getbNS00040007SvcRs() {
        return bNS00040007SvcRs;
    }

    public void setbNS00040007SvcRs(BNS00040007SvcRs bNS00040007SvcRs) {
        this.bNS00040007SvcRs = bNS00040007SvcRs;
    }

    @Override
    public String toString() {
        return "BNS00040007MsgRs [bNS00040007SvcRs=" + bNS00040007SvcRs + "]";
    }

   
}