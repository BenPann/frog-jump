package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ED3_UserPhoto {

    private String UniqId = "";
    private String SubSerial = "";
    private String Status = "";
    private String PType = "";
    private String SType = "";
    private byte[] ImageBig = new byte[] {};
    private byte[] ImageSmall = new byte[] {};
        
}