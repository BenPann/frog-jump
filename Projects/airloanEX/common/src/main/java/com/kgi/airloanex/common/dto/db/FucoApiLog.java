package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FucoApiLog {

    private String Serial;
    private String ApiName;
    private String Request;
    private String Response;
    private String UTime;
}
