package com.kgi.crosssell.common.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CreditApprovalLog {

    // public CreditApprovalLog(String UUID) {
    // this.UniqId = UUID;
    // }

    private String uniqId = "";

    String idno = "";

    String productId = "";

    String creditApsNo = "";

    String productName = "";

    String supNo = "";

    String supChtName = "";

    String status = "";

    String statusDesc = "";

    String prodFlg = "";

    String salesMarket = "";

    String chtName = "";

    String apooTime = "";

    String approvedTime = "";

    String crediteLimit = "";

    String createTime = "";

    String updateTime = "";

    public String getUniqId() {
        return uniqId;
    }

    @JsonSetter("UUID")
    public void setUniqId(String uniqId) {
        this.uniqId = uniqId;
    }

    public String getIdno() {
        return idno;
    }

    @JsonSetter("P_ID")
    public void setIdno(String idno) {
        this.idno = idno;
    }

    public String getProductId() {
        return productId;
    }

    @JsonSetter("PCARD_TYPE")
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCreditApsNo() {
        return creditApsNo;
    }

    @JsonSetter("CASE_ID")
    public void setCreditApsNo(String creditApsNo) {
        this.creditApsNo = creditApsNo;
    }

    public String getProductName() {
        return productName;
    }

    @JsonSetter("PCARD_NAME")
    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSupNo() {
        return supNo;
    }

    @JsonSetter("SUP_NO")
    public void setSupNo(String supNo) {
        this.supNo = supNo;
    }

    public String getSupChtName() {
        return supChtName;
    }

    @JsonSetter("S_CHI_NAME")
    public void setSupChtName(String supChtName) {
        this.supChtName = supChtName;
    }

    public String getStatus() {
        return status;
    }

    @JsonSetter("STATUS_ID")
    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    @JsonSetter("STATUS_DESC")
    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getProdFlg() {
        return prodFlg;
    }

    @JsonSetter("PROD_FLG")
    public void setProdFlg(String prodFlg) {
        this.prodFlg = prodFlg;
    }

    public String getSalesMarket() {
        return salesMarket;
    }

    @JsonSetter("SALES_MARKET")
    public void setSalesMarket(String salesMarket) {
        this.salesMarket = salesMarket;
    }

    public String getChtName() {
        return chtName;
    }

    @JsonSetter("CH_NAME")
    public void setChtName(String chtName) {
        this.chtName = chtName;
    }

    public String getApooTime() {
        return apooTime;
    }

    @JsonSetter("APOO_TIME")
    public void setApooTime(String apooTime) {
        this.apooTime = apooTime;
    }

    public String getApprovedTime() {
        return approvedTime;
    }

    @JsonSetter("APPROVED_TIME")
    public void setApprovedTime(String approvedTime) {
        this.approvedTime = approvedTime;
    }

    public String getCrediteLimit() {
        return crediteLimit;
    }

    @JsonSetter("CREDIT_LIMIT")
    public void setCrediteLimit(String crediteLimit) {
        this.crediteLimit = crediteLimit;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @JsonIgnore
    public Date getApprovedTimeFormat() {
        return this.getApprovedTimeFormat("yyyy-MM-dd hh:mm:ss");
    }

    @JsonIgnore
    public Date getApprovedTimeFormat(String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Date approvedTime = null;
        try {
            approvedTime = simpleDateFormat.parse(this.approvedTime);
        } catch (ParseException e) {
        }
        return approvedTime;
    }

    @JsonIgnore
    public Date getAp00TimeFormat() {
        return this.getAp00TimeFormat("yyyy-MM-dd hh:mm:ss");
    }

    @JsonIgnore
    public Date getAp00TimeFormat(String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Date ap00Time = null;
        try {
            ap00Time = simpleDateFormat.parse(this.apooTime);
        } catch (ParseException e) {
        }
        return ap00Time;
    }

    @JsonIgnore
    public String getApproveTimeStringFormat() {
        return this.getApproveTimeStringFormat("yyyyMMdd");
    }

    @JsonIgnore
    public String getApproveTimeStringFormat(String format) {
        Date approvedTimeFormat = this.getApprovedTimeFormat();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        String result = "";
        try {
            result = simpleDateFormat.format(approvedTimeFormat);
        } catch (Exception e) {
        }
        return result;
    }

}
