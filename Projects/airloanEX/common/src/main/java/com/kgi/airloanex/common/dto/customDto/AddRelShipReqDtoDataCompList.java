package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AddRelShipReqDtoDataCompList {
    
    /** 企業名稱 */
    private String relship_name;
    
    /** 營利事業統一編號 */
    private String relship_id;

    /** 稱謂 */
    private String relship_code;

    /** 關係別代號 */
    private String relship_comp_code;

    /** 關係別備註(關係別為 99-其他時為必輸入欄位) */
    private String relship_comp_desc;
}
