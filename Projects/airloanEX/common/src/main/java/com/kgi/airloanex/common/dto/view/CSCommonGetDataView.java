package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CSCommonGetDataView {
	private String prodId;
    private String channelId;
    private String prodType;
    private String token;
}