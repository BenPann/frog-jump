package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewCaseChkRspDto {
    /**
     * (1)	CHECK_CODE：
     * A.	檢核資料錯誤(空白代表沒有錯誤)
     * B.	如果有多個錯誤用『;』區隔,例如01;02
     * C.	『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
     * D.	錯誤代碼對照表:
     * 代碼  說明
     * ============================
     * 01	產品別錯誤
     * 99	  非預期性系統異常
     */
    private String CHECK_CODE;

    /**
     * (2)	ERR_MSG：
     * A.	當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
     */
    private String ERR_MSG;

    /** 
     * (3)	RESULT_CODE：回覆結果代碼，(00-檢核成功、01-已重複申請，案件處理中、02-已核未撥75天內的PL案件、03-拒貸30天內案件、04-已持有本行GM卡、05-GM案件處理中)
     */
    private String RESULT_CODE;

}
