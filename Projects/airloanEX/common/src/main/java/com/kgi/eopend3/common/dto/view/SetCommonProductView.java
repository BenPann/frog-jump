package com.kgi.eopend3.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.*;


@Getter
@Setter

public class SetCommonProductView {

	private String[] productId;
	private String twTaxpayer;
	@CheckNullAndEmpty
	private String channelId;
	
}
