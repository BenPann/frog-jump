package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class APIMInfoRspDtoData {

    /**
     * 1=國民身分證資料與檔存資料相符。
     * 2=身分證字號 目前驗證資料錯誤次數已達1次，今日錯誤累積達 3 次後，此身分證字號將無法查詢。
     * 3=身分證字號 目前驗證資料錯誤次數已達2次，今日錯誤累積達 3 次後，此身分證字號將無法查詢。
     * 4=身分證字號 目前驗證資料錯誤次數已達3次，今日錯誤累積達 3 次後，此身分證字號將無法查詢。
     * 5=身分證字號 驗證資料錯誤次數已達3次。今日無法查詢，請明日再查！！
     * 6=您所查詢的國民身分證字號 已停止使用。
     * 7=您所查詢的國民身分證，業依當事人申請登錄掛失。
     * 8=單一使用者出現異常使用情形，暫停使用者權限。
     */
    private String checkIdCardApply;
    
}
