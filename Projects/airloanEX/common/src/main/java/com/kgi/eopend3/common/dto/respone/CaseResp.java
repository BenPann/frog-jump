package com.kgi.eopend3.common.dto.respone;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CaseResp {

    private LoginResp loginResp;    
    private CIFInfoCCResp cifInfoCCResp;
}