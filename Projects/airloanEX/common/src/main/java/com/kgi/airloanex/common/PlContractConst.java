package com.kgi.airloanex.common;

import java.util.HashMap;
import java.util.Map;

public class PlContractConst {

	/** 作為讓外部使用者呼叫 apim 戶役政 的識別 */
	public final static String ExternalCallPostMan = "ExternalCallPostMan";

	/** 預設 ChannelId=KB */
	public final static String KB_9743_CHANNEL_ID = "KB";

	/** 預設 DepartId=9743 */
	public final static String KB_9743_DEPART_ID = "9743";

	public static final String QLOG = "QLOG";

    /** 立約流程 uniqId 的 prefix */
    public static final String CONT = "CONT";

    /** 客服聯絡 uniqId 的 prefix */
    public static final String CTAC = "CTAC";

	/** 申請流程 uniqId 的 prefix */
	public static final String LOAN = "LOAN";

	public static final String LOAN0000000000000 = "LOAN0000000000000";

	/** [信貸]申請流程 - 額度體驗題目編號 - EXPyyyyMMdd12345 */
	public static final String EXP = "EXP";

    public static final String EDDA = "EDDA";

    public static final String API_HTML2PDF = "api/v1/htmlToPdf" ;

	public static final String APILOG_TYPE_CALLOUT = "1";
    public static final String APILOG_TYPE_CALLIN = "2";

	/**婚姻狀態 - 未婚 single*/
	public static final String CIF_MARRIAGE_NO = "S";
	/**婚姻狀態 - 已婚 married */
	public final static String CIF_MARRIAGE_YES = "M";

	/** 未婚 single */
	public static final String CASEDATA_marriage_single = "1";
	/** 已婚 married */
    public static final String CASEDATA_marriage_married = "2";

    /**IdentityVerification 驗證成功*/
	public static final String VERIFY_STATUS_SUCCESS = "1";
	/**IdentityVerification 驗證失敗*/
	public static final String VERIFY_STATUS_FAIL = "0";

	/**立約產品代號 - 信貸 PL */
	public final static String CONTRACT_PRODUCT_ID_TYPE_LOAN = "1";
	/**立約產品代號 - 現金卡 CASHCARD */
	public final static String CONTRACT_PRODUCT_ID_TYPE_CASH = "2";
	/**立約產品代號 - 一般e貸寶 RPL */
	public final static String CONTRACT_PRODUCT_ID_TYPE_ELOAN = "3";

	// TABLE: CaseDocument
	// 01:體驗 02:申請 03:立約 04:專人聯絡 05:信用卡
	public final static String UNIQ_TYPE_01 = "01"; // 01:體驗
	public final static String UNIQ_TYPE_02 = "02"; // 02:申請
	public final static String UNIQ_TYPE_03 = "03"; // 03:立約
	public final static String UNIQ_TYPE_04 = "04"; // 04:專人聯絡
	public final static String UNIQ_TYPE_05 = "05"; // 05:信用卡

	// TABLE: CaseDocument
	public final static String DOCUMENT_TYPE_LOAN_APPLYPDF   = "loan.applypdf"; // 貸款PDF
	public final static String DOCUMENT_TYPE_LOAN_APPLYPDF_FINALE   = "loan.applypdf.finale"; // 貸款PDF
	public final static String DOCUMENT_TYPE_LOAN_REJECTPDF  = "loan.rejectpdf"; // 拒貸PDF
	public final static String DOCUMENT_TYPE_LOAN_KYC_PDF    = "loan.kyc.pdf"; // KYC表PDF
	public final static String DOCUMENT_TYPE_CREDIT_APPLYPDF = "credit.applypdf"; // 信用卡PDF
    public final static String DOCUMENT_TYPE_CONTRACT_PDF    = "contract.pdf"; // 立約PDF
	public final static String DOCUMENT_TYPE_CONTRACT_ACH_PDF    = "contract.ach.pdf"; // 立約PDF
    
    /** OTP驗證 */
	public final static String CASEAUTH_AUTH_TYPE_OTP = "1";
	/** MBC驗證 */
	public final static String CASEAUTH_AUTH_TYPE_MBC = "2";
	/** 證券異業 */
	public final static String CASEAUTH_AUTH_TYPE_SECURITY = "3";

	/**數位信貸各種驗證-驗證類型*/
	public static final String CREDITVERIFY_TYPE_NCCC = "0";
	public static final String CREDITVERIFY_TYPE_BANKACOUNT = "1";
	public static final String CREDITVERIFY_TYPE_CHTMBC = "2";
	public static final String CREDITVERIFY_TYPE_CROSSSELL = "3";
	public static final String CREDITVERIFY_TYPE_NOTVERIFY = "4";
	public static final String CREDITVERIFY_TYPE_9 = "9"; // 既有戶驗身

	public static final String CREDITVERIFY_NCCC = "他行卡驗身";
	public static final String CREDITVERIFY_BANKACOUNT = "他行帳戶驗身";
	public static final String CREDITVERIFY_CHTMBC = "MBC";
	public static final String CREDITVERIFY_OTP = "OTP";
//	public static final String CREDITVERIFY_CROSSSELL = "3";
    public static final String CREDITVERIFY_NOTVERIFY = "線下驗身";
	public static final String CREDITVERIFY_9 = "OTP"; // 既有戶驗身

    /**預約分行流程*/
	public final static String CASEDATA_PROCESS_REV_BRANCH = "1" ;
	/**數位帳戶流程*/
	public final static String CASEDATA_PROCESS_ED3 = "2" ;

	/** 是否預核 - 1:預核 */
	public final static String IsPreAudit_1 = "1";
	/** 是否預核 - 2:非預核 */
	public final static String IsPreAudit_2 = "2";
	
	// @see select Name, DataKey, DataName, Enable, Sort from DropdownData where Name = 'airloanStatus';
	/**案件狀態 -init*/
	public final static String CASE_STATUS_INIT = "00";
	/**案件狀態 -填寫中*/
	public final static String CASE_STATUS_WRITING = "01";
	/**案件狀態 -填寫完成*/
	public final static String CASE_STATUS_WRITE_SUCCESS = "02";
	/**案件狀態 -改紙本進件 */
	public final static String CASE_STATUS_APPLYPAPER = "04";
	/**案件狀態 -30天自動結案*/
	public final static String CASE_STATUS_SUSPENDBY30DAYS = "06";
	/**案件狀態 -斷點取消*/
	public final static String CASE_STATUS_CANCEL = "07";
	/**案件狀態 -拒貸*/
	public final static String CASE_STATUS_REJECT = "08";
	/**案件狀態 -拒貸已送件*/
	public final static String CASE_STATUS_REJECT_SENDED = "09";
	/**案件狀態 -送件中*/
	public final static String CASE_STATUS_SUBMITTING = "11";
	/**案件狀態 -送件完成*/
	public final static String CASE_STATUS_SUBMIT_SUCCESS = "12";
	/**案件狀態 -無對應之信用卡產品*/
	public final static String CASE_STATUS_PRODUCT_ERROR = "17";
	/**案件狀態 -送件失敗*/
	public final static String CASE_STATUS_SUBMIT_FAIL = "18";
	/**案件狀態 -查無申請書無法送件*/
	public final static String CASE_STATUS_SUBMIT_ERROR = "19";
	/**案件狀態 - 中壽投保逾時撤件 */
	public final static String CASE_STATUS_POLICYTIMEOUT = "20";
	/**案件狀態 -APS作業中*/
	public final static String CASE_STATUS_APS_PROCESSING = "21";
	/**案件狀態 -APS作業完成*/
	public final static String CASE_STATUS_APS_COMPLETE = "22";
	/**案件狀態-等待立約*/
	public final static String CASE_STATUS_CONTRACT_WAIT = "91";
	/**案件狀態-立約送出*/
	public final static String CASE_STATUS_CONTRACT_SUBMIT = "92";
	/**案件狀態-立約成功*/
	public final static String CASE_STATUS_CONTRACT_SUCCESS = "93";

	public enum CaseDataSubStatus {

		/** 客戶初進入，尚未OTP */
		CASE_STATUS_00_SubStatus_000("00 000"), 
		/** 客戶初進入，已OTP */
		CASE_STATUS_00_SubStatus_010("00 010"), 
		/** 客戶資料填寫中，尚未驗身 */
		CASE_STATUS_01_SubStatus_000("01 000"), 
		/** **客戶資料填寫中，已驗身 */
		CASE_STATUS_01_SubStatus_010("01 010"), 
		/** NEW_CASE_CHK 成功 */
		CASE_STATUS_01_SubStatus_020("01 020"), 
		/** GET_AML_DATA 成功 */
		CASE_STATUS_01_SubStatus_030("01 030"), 
		/** CHECK_NO_FINPF 成功 */
		CASE_STATUS_01_SubStatus_040("01 040"), 
		/** CHECK_HAVE_FINPF2 成功 */
		CASE_STATUS_01_SubStatus_050("01 050"), 
		/** CHECK_HAVE_FINPF3 成功 */
		CASE_STATUS_01_SubStatus_060("01 060"), 
		/** CREATE_NEW_CASE 已呼叫新增案件(STP)成功 */
		CASE_STATUS_01_SubStatus_070("01 070"), 
		/** 完成新增案件，含以上所有電文。 */
		CASE_STATUS_01_SubStatus_090("01 090"), 
		/** **產生申請書HTML(previewCasePDFHtml)**成功 */
		CASE_STATUS_01_SubStatus_100("01 100"), 
		/** **產生申請書HTML(previewCasePDFHtml)**失敗 */
		CASE_STATUS_01_SubStatus_101("01 101"), 

		/** 確認上傳身份證正反面 */
		CASE_STATUS_01_SubStatus_400("01 400"), 
		/** 戶役政查詢成功 */
		CASE_STATUS_01_SubStatus_410("01 410"), 
		/** 戶役政查詢失敗 */
		CASE_STATUS_01_SubStatus_411("01 411"), 

		/** 風險評級成功 */
		CASE_STATUS_01_SubStatus_420("01 420"), 
		/** 風險評級失敗 */
		CASE_STATUS_01_SubStatus_421("01 421"), 

		/** 呼叫Z07查詢成功 */
		CASE_STATUS_01_SubStatus_430("01 430"), 
		/** 呼叫Z07查詢失敗 */
		CASE_STATUS_01_SubStatus_431("01 431"), 
		/** 呼叫外部負債資料查詢成功 */
		CASE_STATUS_01_SubStatus_440("01 440"), 
		/** 呼叫外部負債資料查詢失敗 */
		CASE_STATUS_01_SubStatus_441("01 441"), 
		/** 產生KYC表成功 */
		CASE_STATUS_01_SubStatus_450("01 450"), 
		/** 產生KYC表失敗 */
		CASE_STATUS_01_SubStatus_451("01 451"), 

		/** 確認上傳財力證明 */
		CASE_STATUS_01_SubStatus_600("01 600"), 
		/** 確認上傳同一關係人表 */
		CASE_STATUS_01_SubStatus_700("01 700"), 
		/** 客戶資料上傳完成 */
		CASE_STATUS_01_SubStatus_800("01 800"),
		
		/** UPDATE_CASE_INFO 發查聯徵中 */
		CASE_STATUS_01_SubStatus_900("01 900"), 
		/** UPDATE_CASE_INFO 發查聯徵成功 */
		CASE_STATUS_01_SubStatus_910("01 910"), 
		/** UPDATE_CASE_INFO 發查聯徵失敗 */
		CASE_STATUS_01_SubStatus_911("01 911"),

		/** 卡加貸信用卡已送件 */
		CASE_STATUS_03_SubStatus_000("03 000"),

		/** 紙本送出 */
		CASE_STATUS_04_SubStatus_000("04 000"),

		/** 送件完成 */
		CASE_STATUS_12_SubStatus_000("12 000"), 
		/** GET_CASE_COMPLETE 後端整件狀態已完整 */
		CASE_STATUS_12_SubStatus_100("12 100"), 

		/** APSStatus狀態已有資料 */
		CASE_STATUS_22_SubStatus_000("22 000"), 
		/** 30天到期取消 */
		CASE_STATUS_06_SubStatus_000("06 000"), 
		/** 案件取消 */
		CASE_STATUS_07_SubStatus_000("07 000"), 
		/** 系統拒貸，申請書尚未上傳 */
		CASE_STATUS_09_SubStatus_000("09 000"), 
		/** 系統拒貸，申請書已上傳影像系統 */
		CASE_STATUS_09_SubStatus_010("09 010");

		public final String code;
		
		CaseDataSubStatus(String code) {
			this.code = code;
		}

		public String getStatus() {
			return this.code.split(" ")[0];
		}
		public String getSubStatus() {
			return this.code.split(" ")[1];
		}
		
	} // end CaseDataSubStatus

	public enum CaseDataJobStatus {

		/** 等待 - 第一次開始產生申請書PDF */
		CASE_STATUS_01_JobStatus_010("01 010"),
		/** 第一次開始產生申請書PDF */
		CASE_STATUS_01_JobStatus_200("01 200"), 
		/** 執行送件成功 */
		CASE_STATUS_01_JobStatus_210("01 210"), 
		/** 執行送件失敗(連線失敗 */
		CASE_STATUS_01_JobStatus_211("01 211"), 
		/** 圖片上傳ftp成功 */
		CASE_STATUS_01_JobStatus_220("01 220"), 
		/** 圖片上傳ftp失敗 */
		CASE_STATUS_01_JobStatus_221("01 221"), 
		/** XML上傳成功 */
		CASE_STATUS_01_JobStatus_230("01 230"), 
		/** XML上傳失敗 */
		CASE_STATUS_01_JobStatus_231("01 231"), 
		/** 呼叫影像WebService成功 */
		CASE_STATUS_01_JobStatus_240("01 240"), 
		/** 呼叫影像WebService失敗 */
		CASE_STATUS_01_JobStatus_241("01 241"), 
		/** 更新送件完成成功 */
		CASE_STATUS_01_JobStatus_250("01 250"), 
		/** 更新送件完成失敗 */
		CASE_STATUS_01_JobStatus_251("01 251"), 
		/** 第一次完成產生申請書PDF成功 */
		CASE_STATUS_01_JobStatus_260("01 260"), 
		/** 第一次完成產生申請書PDF失敗 */
		CASE_STATUS_01_JobStatus_261("01 261"),

		/** 等待 - 開始產生KYC表PDF */
		CASE_STATUS_01_JobStatus_020("01 020"),
		/**  */
		CASE_STATUS_01_JobStatus_500("01 500"), 
		/**  */
		CASE_STATUS_01_JobStatus_520("01 520"), 
		/**  */
		CASE_STATUS_01_JobStatus_521("01 521"), 
		/**  */
		CASE_STATUS_01_JobStatus_530("01 530"), 
		/**  */
		CASE_STATUS_01_JobStatus_531("01 531"), 
		/** 開始產生KYC表PDF成功 */
		CASE_STATUS_01_JobStatus_560("01 560"), 
		/** 開始產生KYC表PDF失敗 */
		CASE_STATUS_01_JobStatus_561("01 561"),

		/** 等待 - UPDATE_CASE_INFO 發查聯徵中 */
		CASE_STATUS_01_JobStatus_030("01 030"),
		/** UPDATE_CASE_INFO 發查聯徵中 */
		CASE_STATUS_01_JobStatus_900("01 900"), 

		/** 等待 - 第二次開始產生申請書PDF(含ID財力圖檔) */
		CASE_STATUS_01_JobStatus_040("01 040"),
		/** 第二次開始產生申請書PDF(含ID財力圖檔 */
		CASE_STATUS_02_JobStatus_200("02 200"), 
		/** 執行送件成功 */
		CASE_STATUS_02_JobStatus_210("02 210"), 
		/** 執行送件失敗(連線失敗 */
		CASE_STATUS_02_JobStatus_211("02 211"), 
		/** 圖片上傳ftp成功 */
		CASE_STATUS_02_JobStatus_220("02 220"), 
		/** 圖片上傳ftp失敗 */
		CASE_STATUS_02_JobStatus_221("02 221"), 
		/** XML上傳成功 */
		CASE_STATUS_02_JobStatus_230("02 230"), 
		/** XML上傳失敗 */
		CASE_STATUS_02_JobStatus_231("02 231"), 
		/** 呼叫影像WebService成功 */
		CASE_STATUS_02_JobStatus_240("02 240"), 
		/** 呼叫影像WebService失敗 */
		CASE_STATUS_02_JobStatus_241("02 241"), 
		/** 更新送件完成成功 */
		CASE_STATUS_02_JobStatus_250("02 250"), 
		/** 更新送件完成失敗 */
		CASE_STATUS_02_JobStatus_251("02 251"), 
		/** 第二次完成產生申請書PDF(含ID財力圖檔)成功 */
		CASE_STATUS_02_JobStatus_260("02 260"), 
		/** 第二次完成產生申請書PDF(含ID財力圖檔)失敗 */
		CASE_STATUS_02_JobStatus_261("02 261");

		public final String code;
		
		CaseDataJobStatus(String code) {
			this.code = code;
		}

		public String getStatus() {
			return this.code.split(" ")[0];
		}
		public String getJobStatus() {
			return this.code.split(" ")[1];
		}
		
	} // end CaseDataJobStatus
	
	/**圖片上傳狀態-待處理(圖片上傳完成才會到此狀態)*/
	public final static Integer PHOTO_STATUS_WAIT = 1;
	/**圖片上傳狀態-客戶補件中*/
	public final static Integer PHOTO_STATUS_ADDING = 2;
	/**圖片上傳狀態-已送影像系統*/
	public final static Integer PHOTO_STATUS_SUBMIT = 3;
	
	/**利率體驗30天狀態 -可體驗*/
	public final static String QUOTACAL_CHKDATE_SUCCESS = "1";
	
	/**利率體驗30天狀態 -不可體驗(30天內已有體驗)*/
	public final static String QUOTACAL_CHKDATE_ERROR = "99";
	
	/**上傳圖片線上線下狀態 -線上*/
	public final static Integer PHOTO_ONLINE_STATUS_ONLINE = 1;
	/**上傳圖片線上線下狀態 -線下*/
	public final static Integer PHOTO_ONLINE_STATUS_OFFLINE = 2;
	/**上傳圖片信用卡貸款種類 -信用卡*/
	public final static String PHOTO_PRODUCTTYPE_CREDIT = "1";
	/**上傳圖片信用卡貸款種類 -貸款*/
	public final static String PHOTO_PRODUCTTYPE_LOAN = "2";

	/** 使用者圖片狀態 - 暫存 */
	public static final String USERPHOTO_STATUS_TEMP = "0";
	/** 使用者圖片狀態 - 使用者處理中(上傳中還未送出) */
	public static final String USERPHOTO_STATUS_USER_PROCESS = "1";
	/** 使用者圖片狀態 - 使用者上傳完成 等待JOB處裡 */
	public static final String USERPHOTO_STATUS_WAIT_UPLOAD = "2";
	/** 使用者圖片狀態 - 圖片上傳成功 */
	public static final String USERPHOTO_STATUS_UPLOAD_SUCCESS = "3";
	/** 2021-05-04 等待JOB處理，UserPhoto 的 Status 須採用 4 因為 2 會被別的排程搶圖片 demand by Ben */
	public static final String USERPHOTO_STATUS_WAIT_UPLOAD_4 = "4";
	/** 使用者圖片狀態 - 圖片上傳失敗 */
	public static final String USERPHOTO_STATUS_UPLOAD_FAIL = "9";
	/** 使用者圖片狀態 - 特殊圖片暫存 */
	public static final String USERPHOTO_STATUS_SPECIAL_TEMP = "10";
	/** 使用者圖片狀態 - 從Addphoto尚未上傳Copy至userPhoto */
	public static final String USERPHOTO_STATUS_COPY_FROM_ADDPHOTO = "11";

	public static final int IMAGE_SIZE_LIMIT = 6*1024*1366 ; // 上傳圖片大小限制 (base64 會大一點  1024/3*4 ==> 1366)
	
	/** 1 線上件 */
	public static final String USERPHOTO_ONLINE_1 = "1";

	/** 2 線下件 */
	public static final String USERPHOTO_ONLINE_2 = "2";

	// 申辦產品
	/**  1 信用卡線上申辦(CREDITCARD) */
	public static final String USERPHOTO_ProdType_1 = "1";
	/**  2 信貸線上申辦(LOAN) */
	public static final String USERPHOTO_ProdType_2 = "2";
	/**  3 銀行線上開戶(薪轉EOP) */
	public static final String USERPHOTO_ProdType_3 = "3";
	/**  4 證券線上開戶(ED3) */
	public static final String USERPHOTO_ProdType_4 = "4";
	/**  5 期貨線上開戶 */
	public static final String USERPHOTO_ProdType_5 = "5";

	/** 無主分類 */
	public static final String USERPHOTO_PTYPE_0 = "0";
	/** 身分證明文件 */
	public static final String USERPHOTO_PTYPE_1 = "1";
	/** 財力證明文件 */
	public static final String USERPHOTO_PTYPE_2 = "2";

	/** 身份證正面 front */
	public static final String USERPHOTO_SUBTYPE_IDCARDFRONT = "1";
	/** 身份證反面 back */
	public static final String USERPHOTO_SUBTYPE_IDCARDBANK = "2";
	/** 健保卡正面 */
	public static final String USERPHOTO_SUBTYPE_HEALTHIDCARD = "3";
	/** 其它財力證明文件 */
	public static final String USERPHOTO_SUBTYPE_99 = "99";
	
	/**縣市鄉鎮區代碼*/
	public final static String DROPDOWNDATA_NAME_ZIPCODEMAPPING = "zipcodemapping";

	/** CreditCaseData UserType 信用卡-使用者類型-新戶*/
	public static final String CREDITCASEDATA_USERTYPE_NEW = "0";
	/** CreditCaseData UserType 信用卡-使用者類型-信用卡戶*/
	public static final String CREDITCASEDATA_USERTYPE_CREDIT = "1";
	/** CreditCaseData UserType 信用卡-使用者類型-存款戶*/
	public static final String CREDITCASEDATA_USERTYPE_ACCOUNT = "2";
	/** CreditCaseData UserType 信用卡-使用者類型-純貸款戶*/
	public static final String CREDITCASEDATA_USERTYPE_LOAN = "3";
	/** CreditCaseData UserType 信用卡-使用者類型-其他*/
	public static final String CREDITCASEDATA_USERTYPE_OTHER = "4";

	/** CaseData.UserType=3 既有戶 */
	public static final String USER_TYPE_3_OLD_ONE = "3";
	/** CaseData.UserType=0 新戶 */
	public static final String USER_TYPE_0_NEW_ONE = "0";

	// TABLE Config 錯誤訊息
	/** '您無任何可供線上簽約之案件或申請案件尚未達立約階段', '沒有任何可以簽屬的立約案件' */
	public static final String CONT_Err_GET_DGT_CONST = "CONT_Err_GET_DGT_CONST";
	/** '系統問題，如有任何需要協助，請洽客服人員', '連接電文回傳 CHECK_CODE=99 系統錯誤' */
	public static final String CONT_Err_GETAPI = "CONT_Err_GETAPI";
	/** '系統問題，如有任何需要協助，請洽客服人員', '非預期的錯誤' */
	public static final String CONT_Err_Unexpected = "CONT_Err_Unexpected";
	/** '您有貸款申請案件尚未結案', '申請人在APS系統有案件尚未結案' */
	public static final String LOAN_Err_NEW_CASE_CHK = "LOAN_Err_NEW_CASE_CHK";

	// CONT 立約流程
	/** CONT 立約流程 1 PL  */
	public static final String CONT_ProductId_1 = "1";
	/** CONT 立約流程 3 RPL  */	
	public static final String CONT_ProductId_3 = "3";
	/** CONT 立約流程 2 CASHCARD 現金卡  */
	public static final String CONT_ProductId_2 = "2";

	// LOAN 申請流程
	/** LOAN 申請流程 1 PA 預核名單preAudit(快速申辦不查聯徵) */
	public static final String LOAN_ProductId_1_PA = "1";
	/** LOAN 申請流程 2 PL */
	public static final String LOAN_ProductId_2_PL = "2";
	/** LOAN 申請流程 3 RPL */
	public static final String LOAN_ProductId_3_RPL = "3";
	/** LOAN 申請流程 4 GM(CASHCARD) 現金卡 */
	public static final String LOAN_ProductId_4_GM = "4";

	/** 8220 北一區 */
	public static final String[] CROSS_ZONE_8220 = {"基隆市", "台北市", "新北市", "桃園", "宜蘭"};
	/** 8230 北二區 */
	public static final String[] CROSS_ZONE_8230 = {"基隆市", "台北市", "新北市", "桃園", "宜蘭", "花蓮"};
	/** 8240 桃竹區 */
	public static final String[] CROSS_ZONE_8240 = {"新北市", "桃園", "新竹", "苗栗"};
	/** 8250 台中區 */
	public static final String[] CROSS_ZONE_8250 = {"苗栗", "台中", "彰化", "南投"};
	/** 8260 台南區 */
	public static final String[] CROSS_ZONE_8260 = {"雲林", "嘉義", "台南", "高雄"};
	/** 8270 高雄區 */
	public static final String[] CROSS_ZONE_8270 = {"台南", "高雄", "屏東", "台東"};

	public static final String Get_CDD_Rate_SystemType_G08 = "G08";

	public static final Map<String, String[]> CROSS_ZONES;
	static {
		CROSS_ZONES = new HashMap<>();
		CROSS_ZONES.put("8220", CROSS_ZONE_8220);
		CROSS_ZONES.put("8230", CROSS_ZONE_8230);
		CROSS_ZONES.put("8240", CROSS_ZONE_8240);
		CROSS_ZONES.put("8250", CROSS_ZONE_8250);
		CROSS_ZONES.put("8260", CROSS_ZONE_8260);
		CROSS_ZONES.put("8270", CROSS_ZONE_8270);
	}

	/** 
	 * DATA_TYPE 客戶類型(1.信用卡 2.現金卡(靈活卡) 3.存款 4.貸款) 目的是給apply verify 簡訊驗證顯示用戶於本行有效行動電話之服務名稱 
	 * @see verify.component.ts
	 */
	public enum StempDataType {

		/** 
		 * 信用卡
		 * 提示：申辦本行信用卡服務之
		 */
		Creditcard("1"),
		/** 
		 * 現金卡
		 * 提示：申辦本行現金卡服務之 
		 */
		Cashcard("2"),
		/** 
		 * 存款
		 * 提示：申辦本行存款服務之 
		 */
		Kgibank("3"),
		/** 
		 * 貸款
		 * 提示：申辦本行貸款服務之 
		 */
		Loan("4"),
		/** 
		 * VIP
		 * 提示：VIP 
		 */
		Vip("5");

		public final String code;
		
		StempDataType(String code) {
			this.code = code;
		}
	}

	// TODO: enum CaseDocument

    public enum RouteGo {
        // 1. /initPlContract
        initPlContract,
        // 2. /n/chooseIdentificationStylePlContract
        chooseIdentificationStylePlContract,
        // 3. /n/identifyByBankAccountPlContract
        identifyByBankAccountPlContract,
        // 4. /o/otpPlContract
        otpPlContract,
        // 5. /n/choosePlContract
        choosePlContract,
        // 6. /n/agreePlContract
        agreePlContract,
        // 7. /n/agreeRplContract
        agreeRplContract,
        // 8. /n/agreeCashcardContract
        agreeCashcardContract,
        // 9. /n/infoPlContract
        infoPlContract,
        // 10. /n/previewPlContract
        previewPlContract,
        // 11. /n/allFinishPlContract
        allFinishPlContract,
        // 13. /n/allFailurePlContract
        allFailurePlContract
    }

    public enum SituationPCode2566 {
        auth, 
        sign
	}
	
	public static boolean isPL(String productId) {
		return PlContractConst.CONTRACT_PRODUCT_ID_TYPE_LOAN.equals(productId);
	}
	public static boolean isRPL(String productId) {
		return PlContractConst.CONTRACT_PRODUCT_ID_TYPE_ELOAN.equals(productId);
	}
	public static boolean isCASHCARD(String productId) {
		return PlContractConst.CONTRACT_PRODUCT_ID_TYPE_CASH.equals(productId);
	}
}
