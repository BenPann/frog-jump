package com.kgi.airloanex.common.dto.view;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CompanyInfoView {
    
    private String CompanyId;

    private String CompanyName;
}
