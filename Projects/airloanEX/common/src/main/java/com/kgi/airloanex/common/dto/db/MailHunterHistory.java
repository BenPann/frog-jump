package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MailHunterHistory {

    private String Serial = "";
    private String UniqId = "";
    private String Type = "";    
    private String TemplateId = "";
    private String ChtName = "";
    private String Idno = "";    
    private String EMailAddress = "";    
    private String Status = "";    
    private String ErrorMessage = "";    
    private String Title = "";    
    private String Content = "";
    private String ProjectCode = "";    
    private String OwnerID = "";    
    private String ProductCode = "";
    private byte[] Attatchment = new byte[]{};
    private String CreateTime = "";
    private String UpdateTime = "";

}
