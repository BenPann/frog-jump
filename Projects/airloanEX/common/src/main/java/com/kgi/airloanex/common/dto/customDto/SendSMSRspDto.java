package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SendSMSRspDto {
    
    private String checkCode;
    private String errMsg;
}
