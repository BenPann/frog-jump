package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.Setter;

/**
 * 下拉選單資料表
 */
@Getter
@Setter
public class DropdownData {
    private String Name;
	private String DataKey;
	private String DataName;
	private String Enable;
	private String Sort;
}
