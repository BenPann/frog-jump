package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "sendType" })
public class CRD00EC5500Detail {
    @XmlElement(name = "SEND-TYPE")
    private String sendType;

    public String getSendType() {
        return sendType;
    }

    public void setSendType(String sendType) {
        this.sendType = sendType;
    }

	@Override
	public String toString() {
		return "CRD00EC5500Detail [sendType=" + sendType + "]";
	}
   
}