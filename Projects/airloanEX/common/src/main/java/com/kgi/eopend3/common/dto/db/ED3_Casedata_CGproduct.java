package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ED3_Casedata_CGproduct {

    private String UniqId = "";
    private String MainProduct = "";    
    private String SubProduct = "";
}