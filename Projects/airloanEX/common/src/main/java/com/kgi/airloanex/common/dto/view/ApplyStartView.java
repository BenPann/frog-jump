package com.kgi.airloanex.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplyStartView {

    private String productId;
    private String uniqId;
    private String entry;
    private String entryOther;

    private String uniqType;

    @CheckNullAndEmpty
    private String idno ;
    @CheckNullAndEmpty
    private String birthday ;

    private String phone;

    private String agentNo;
    
    /** 我已閱讀並同意下列條款及告知事項 */
    private boolean ch1 = false;
    
    /** 同意使用與貴行往來之資料申辦貸款 */
    private boolean ch2 = false;

    private String forceNewApply;

    private String expCaseNo;

    private String ipAddress ;
    private String browser ;
    private String platform ;
    private String os ;
}
