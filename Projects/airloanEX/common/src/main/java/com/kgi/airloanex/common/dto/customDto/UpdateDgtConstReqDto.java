package com.kgi.airloanex.common.dto.customDto;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CheckNullAndEmpty
public class UpdateDgtConstReqDto {
    
    /** (1)	CASE_NO：案件編號 */
    private String CASE_NO;

    /** (2)	CITY_CODE：郵遞區號，NVARCHAR(3) */
    private String CITY_CODE;

    /** (3)	ADDRESS：地址，NVARCHAR(150) */
    private String ADDRESS;

    /** (4)	pay_date：撥款日期(YYYYMMDD) */
    private String pay_date;

    /** (5)	pay_desc_type：撥款方式 (1-(一)撥入立約人、2-(二)撥入立約人開立於、3-(三)撥付代償立約人) */
    private String pay_desc_type;

    /** (6)	pay_bank：撥款銀行名稱 */
    private String pay_bank;

    /** (7)	pay_bank_no：撥款銀行代號 */
    private String pay_bank_no;

    /** (8)	pay_bank_no：撥款銀行分行代號 */
    private String pay_bank_branchId;

    /** (9)	pay_account：撥款帳號 */
    private String pay_account;

    /** (10)	pay_kind_type：還款方式(1-(一)自撥款日起、2- (二)自撥款日起、3-(三)寬限期自撥款日起算、4-(五)其他約定方式：、5-(四)員工貸款約定：) */
    private String pay_kind_type;

    /** (11)	pay_kind_day：繳款日(D) */
    private String pay_kind_day;

    /** (12)	pay_way_type：還款授權扣款方式(1-本行、2-eDDA、3-ACH) */
    private String pay_way_type;

    /** (13)	sord_bank_no：授扣銀行代號 */
    private String sord_bank_no;

    /** (14)	sord_bank_name：授扣銀行名稱 */
    private String sord_bank_name;

    /** (15)	sord_acct_no：授扣帳號 */
    private String sord_acct_no;

    /** (16)	edda_check_time：EDDA認證時間(YYYYMMDDHHMMSS) */
    private String edda_check_time;

    /** (17)	const_sign_no：契約簽署版本(YYYYMMDD) */
    private String const_sign_no;

    /** (18)	APSSeqNo：契約書流水編號(YYYYMMDD + 5碼流水號) */
    private String APSSeqNo;

    /** (18)	exceed_flg：熔斷註記(N/E1/E2/E3/E4/E5/E6/E7/E8)，N-非熔斷 */
    private String exceed_flg;

}