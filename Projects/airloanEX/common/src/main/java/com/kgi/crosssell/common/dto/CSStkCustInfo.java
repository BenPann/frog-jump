package com.kgi.crosssell.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CSStkCustInfo{

    /** 回應狀態碼 */
    private String rtncode;

    /** 回應錯誤訊息 */
    private String message;

    /**   戶名(中文) */
    private String custName1;

    /**   戶名(原住民) */
    private String custName2;

    /** 戶名(英文) */
    private String custEngName;

    /**    身份證字號 */
    private String idno;

    /**  證件類別 */
    private String idkind;

    /**    出生日期 */
    private String birthday;

    /** 戶籍地址 */
    private String resAddr;

    /**    通訊地址 */
    private String commAddr;

    /** 聯絡電話(H) */
    private String homeTel;

    /**  聯絡電話(H)分機 */
    private String homeTelExt;

    /** 聯絡電話(O) */
    private String corpTel;

    /**  聯絡電話(O)分機 */
    private String corpTelExt;

    /**   聯絡電話(M) */
    private String phone;

    /**   E-MAIL */
    private String email;

    /**    服務單位名稱 */
    private String corpName;

    /** 職業(行業別) */
    private String jobType;

    /**    職稱 */
    private String jobTitle;

    /**   學歷 */
    private String education;

    /**   婚姻狀況 */
    private String maritalStatus;

    /**  子女數 */
    private String childrenStatus;

    /**    公司地址 */
    private String corpAddr;

    /**  戶籍地址郵遞區號 */
    private String resAddrZip;

    /** 通訊地址郵遞區號 */
    private String commAddrZip;

    /** 公司地址郵遞區號 */
    private String corpAddrZip;

}