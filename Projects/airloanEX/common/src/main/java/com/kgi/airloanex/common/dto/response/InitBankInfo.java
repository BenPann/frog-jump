package com.kgi.airloanex.common.dto.response;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/** 立約預載資料，其中包含
 * 新戶：GET_DGT_CONST.CONST_DATA
 * 既有戶：GET_DGT_CONST.DEP_DATA
 */
@Getter
@Setter
public class InitBankInfo {
    
    /** 手機 */
    String phone;
    /** email */
    String email;

    /** 是否有下拉式選單[撥款至本行帳戶/撥款至他行帳戶] */
    boolean hasPayBankOption = false;
    
    /** 撥款為本行他行  0:本行  1:它行  null:它行 */
    String PayBankOption;
    /** 撥款銀行 */
    String expBankNo;
    /** 撥款帳號 */
    String expAccount;

    /** 凱基銀行帳戶資訊 */
    List<KgiBankInfo> kgiBankInfos = new ArrayList<>();

    /** 是否有下拉式選單[授扣至本行帳戶/授扣至他行帳戶] */
    boolean hasSordBankOption = false;

    /** 繳款為本行他行  0:本行  1:它行  null:它行 */
    String SordBankOption;

    /** 授扣銀行 */
    String sordBankNo;
    /** 授扣帳號 */
    String sordAcctNo;
    /** 繳款日 */
    String payKindDay;

    /** 代償銀行 目的是為了初步判斷繳款日 */
    String payBank;
    // --------------------------------------------------------------------
    /** D.	rate：利率 */
    private String rate;

    /** E.	aprove_period：期數(年) */
    private String aprove_period;

    /** F.	apy_fee：手續費 */
    private String apy_fee;

    /** G.	b_range：綁約期間(期) */
    private String b_range;

    /** H.	rate_desc：利率種類(契約條款) */
    private String rate_desc;
    private String p_prj_code;
    private String int_type;
    private String p_now_rate;
    private String int_pre_1;
    private String fix_rate1;
    private String tbA_aprove_amount; // 核准金額
    private String sum_rate; // 總費用年百分率 用於畫面顯示
    private String session_id;
    // --------------------------------------------------------------------
}