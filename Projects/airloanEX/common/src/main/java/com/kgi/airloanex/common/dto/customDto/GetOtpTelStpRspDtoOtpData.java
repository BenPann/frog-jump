package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetOtpTelStpRspDtoOtpData {
    /*
     * (5)	OTP_DATA：
     * A.	OTP明細
     * 欄位名稱			欄位說明
     * =======================================================
     * DATA_TYPE		客戶類型(1:信用卡, 2:現金卡(靈活卡), 3:存款, 4:貸款)
     * OTP_TEL	電話號碼
     */
    private String DATA_TYPE;
    private String OTP_TEL;
    private String FORMAT;
}
