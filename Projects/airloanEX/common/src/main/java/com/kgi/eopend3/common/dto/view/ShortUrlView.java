package com.kgi.eopend3.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShortUrlView {

    private String channelID= "";  
    private String departID= "";  
    private String shortUrl= "";  
    private String member= "";  
    private String entry= "";  
    private String exp= "";
    private String token= ""; 
    
}