package com.kgi.airloanex.common.dto.customDto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QryQaRspDto {
   
// (1)	CHECK_CODE：
// A.	檢核資料錯誤(空白代表沒有錯誤)
// B.	如果有多個錯誤用『;』區隔,例如01;02
// C.	『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
// D.	錯誤代碼對照表:
// 代碼  說明
// ============================
// 01	網路案件編號格式錯誤
// 02	IPAddress格式錯誤
// 	03	  系統異常
// 	99	  非預期性系統異常
private String CHECK_CODE;
// (2)	ERR_MSG：
// A.	當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
private String ERR_MSG;

// (3)	CASE_NO：
// A.	案件編號:CHAR(11) ，此案件資料鍵值
// B.	日後用來檢核後續步驟使用
private String CASE_NO;

// (4)	QA_DATA：
// A.	題目資料
// 欄位名稱		欄位說明
// =======================================================
// var_code      版本編號
// quest_no		問題編號
// quest_desc		問題說明
// quest_kind		問題類型(1.選擇題2.問答題)
// quest_kind2	問題型態(1.文字2.數字)
// quest_length	資料最大長度
// quest_type    問題分類(1.借款人信用2.借款人財力3.借款人負債)
private String QA_DATA ;

// (5)	SEL_DATA：
// A.	選項資料
// 欄位名稱		欄位說明
// =======================================================
// quest_no		問題編號
// sel_no			選項編號
// sel_desc		選項說明
// sel_value		選項值
private String SEL_DATA ;

}