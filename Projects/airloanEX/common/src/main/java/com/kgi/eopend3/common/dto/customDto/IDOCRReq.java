package com.kgi.eopend3.common.dto.customDto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class IDOCRReq {

    private String BYPASSID = "";
    private String IDFront_CONTENT = "";
    private String IDBack_CONTENT = "";

}