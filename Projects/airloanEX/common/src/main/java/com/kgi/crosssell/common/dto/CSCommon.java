package com.kgi.crosssell.common.dto;

public class CSCommon {
  private String checkCode;
  private String errMsg;
  private CSCommonData data;

  public String getCheckCode() {
    return checkCode;
  }

  public void setCheckCode(String value) {
    this.checkCode = value;
  }

  public String getErrMsg() {
    return errMsg;
  }

  public void setErrMsg(String value) {
    this.errMsg = value;
  }

  public CSCommonData getData() {
    return data;
  }

  public void setData(CSCommonData value) {
    this.data = value;
  }
}
