package com.kgi.airloanex.common.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResp {

    private String uniqId = "";    
    private String uniqType = "";
    private String idno = "";
    private String ipAddress = "";
    
}