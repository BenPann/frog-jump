package com.kgi.airloanex.common.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ExportUtil {
   
    public synchronized static void export(String filepath, String str) {
        try {
            export(filepath, str.getBytes("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void export(String filepath, byte[] bytes) {
        try {
            Path path = Paths.get(filepath);
            Files.write(path, bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
