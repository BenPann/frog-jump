package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetDgtConstRspDtoDepData {
    
    /** A.	MembCustAc：帳號 */
    private String MembCustAc;

    /** B.	AcctType：產品大類 */
    private String AcctType;

    /** C.	IntCat：產品子類 */
    private String IntCat;

    /** D.	ParmDesc：產品名稱 */
    private String ParmDesc;

    /** E.	MCurrAcctType：帳戶類別 */
    private String MCurrAcctType;

    /** F.	CurrStatus：帳號狀態/戶況 */
    private String CurrStatus;

    /** G.	Currency：幣別 */
    private String Currency;

    /** H.	INVMBranchNo：帳務行/帳戶所屬行 */
    private String INVMBranchNo;

    /** I.	ShortName：中文戶名 */
    private String ShortName;

}