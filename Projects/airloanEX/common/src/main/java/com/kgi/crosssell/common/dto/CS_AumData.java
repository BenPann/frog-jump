package com.kgi.crosssell.common.dto;

public class CS_AumData {
	
	private String UniqId="";
	private String UniqType="";
	private String Currency="";
	private String CurrentStock="";
	private String FinancingBalance="";
	private String BorrowStockBalance="";
	private String TotalFinancial="";
	private String ExchangeRate="";
	private String UpdateTime="";
	private String CreateTime="";
	private String StockBaseDate ="";
	private String StockPriceDate ="";
	
	
	public CS_AumData(){};

	public CS_AumData(String UniqId, String UniqType, String Currency, String CurrentStock, String FinancingBalance, String BorrowStockBalance, String TotalFinancial){
		this.UniqId = UniqId;
		this.UniqType = UniqType;
		this.Currency = Currency;
		this.CurrentStock = CurrentStock;
		this.FinancingBalance = FinancingBalance;
		this.BorrowStockBalance = BorrowStockBalance;
		this.TotalFinancial = TotalFinancial;
	}
	
	
	public String getUniqId() {
		return UniqId;
	}
	public void setUniqId(String uniqId) {
		UniqId = uniqId;
	}
	public String getUniqType() {
		return UniqType;
	}
	public void setUniqType(String uniqType) {
		UniqType = uniqType;
	}
	public String getCurrency() {
		return Currency;
	}
	public void setCurrency(String currency) {
		Currency = currency;
	}
	public String getCurrentStock() {
		return CurrentStock;
	}
	public void setCurrentStock(String currentStock) {
		CurrentStock = currentStock;
	}
	public String getFinancingBalance() {
		return FinancingBalance;
	}
	public void setFinancingBalance(String financingBalance) {
		FinancingBalance = financingBalance;
	}
	public String getBorrowStockBalance() {
		return BorrowStockBalance;
	}
	public void setBorrowStockBalance(String borrowStockBalance) {
		BorrowStockBalance = borrowStockBalance;
	}
	public String getTotalFinancial() {
		return TotalFinancial;
	}
	public void setTotalFinancial(String totalFinancial) {
		TotalFinancial = totalFinancial;
	}
	public String getExchangeRate() {
		return ExchangeRate;
	}
	public void setExchangeRate(String exchangeRate) {
		ExchangeRate = exchangeRate;
	}
	public String getUpdateTime() {
		return UpdateTime;
	}
	public void setUpdateTime(String updateTime) {
		UpdateTime = updateTime;
	}
	public String getCreateTime() {
		return CreateTime;
	}
	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}

	public String getStockBaseDate() {
		return StockBaseDate;
	}

	public String getStockPriceDate() {
		return StockPriceDate;
	}

	public void setStockBaseDate(String stockBaseDate) {
		StockBaseDate = stockBaseDate;
	}

	public void setStockPriceDate(String stockPriceDate) {
		StockPriceDate = stockPriceDate;
	}
	
	
	
	
}
