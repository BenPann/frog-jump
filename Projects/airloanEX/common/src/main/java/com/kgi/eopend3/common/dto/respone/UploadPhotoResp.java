package com.kgi.eopend3.common.dto.respone;

import lombok.*;
/**
 * 上傳圖片 Resp
 */
@Getter
@Setter
public class UploadPhotoResp {
    /** data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAg... (string) - 加上浮水印後的圖檔(Base64) */
    private String waterMarkImage = "";

    /** 蔡基 (string) - 姓名 */
    private String name = "";

    /** 0940701(string) - 發證日期，格式為民國年月 (YYYMMDD，年份不滿100左補0) */
    private String idCard_Issue_DT = "";

    /** 1 (string) - 發證類型, 1 初發 2 補發 3 換發 */
    private String idCard_Issue_Type = "";

    /** 北市 (string) - 發證地點，從下拉選單取得的value，下拉選單(idcardlocation) */
    private String idCard_Issue_City = "";     

    /** 101 (string) - 戶籍地區的區碼，從下拉選單取得，下拉選單(zipcodemapping) */
    private String householdZipCode="";

    /** (string) - 縣市鄉鎮地區 */
    private String householdAddr1 = "";

    /** 葫洲里1鄰(string) - 戶藉地村里鄰名 */
    private String householdAddr2 = "";

    /** 民權東路六段(string) - 戶藉地路名 */
    private String householdAddr3 = "";

    /** 283巷18弄2號(string) - 戶藉地巷弄號 */
    private String householdAddr4 = "";

    /** 未婚 (string) - 婚姻，只會給 未婚/已婚 */
    private String marriage = "";

    /** 1 (string) - 圖檔序號，後端提供 */
    private String subSerial="";

}