package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddLoanCaseRspDto {
    /*
     * (1)	CHECK_CODE：
     * A.	檢核資料錯誤(空白代表沒有錯誤)
     * B.	如果有多個錯誤用『;』區隔,例如01;02
     * C.	『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
     * D.	錯誤代碼對照表:
     * 代碼  說明
     * ============================
     * 01	客戶ID格式錯誤
     * 02	介接系統格式錯誤
     * 03	介接系統案件編號格式錯誤(介接系統為信用卡時才需檢核)
     * 99	非預期性系統異常
     */
    private String CHECK_CODE;

    /*
     * (2)	ERR_MSG：
     * A.	當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
     */
    private String ERR_MSG;

    /*
     * (3)	CASE_NO：
     * A.	案件編號:CHAR(11) ，此案件資料鍵值
     * B.	日後用來檢核後續步驟使用
     */
    private String CASE_NO;

}
