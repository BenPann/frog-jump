package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class QryIdcardInfoReqDto {
    
    /** (1)	CUSTOMER_ID：客戶身分證字號，NVARCHAR(10) */
    private String CUSTOMER_ID;
}
