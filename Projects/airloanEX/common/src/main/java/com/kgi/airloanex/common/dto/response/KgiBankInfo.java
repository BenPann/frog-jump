package com.kgi.airloanex.common.dto.response;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KgiBankInfo {
    
    /** 凱基 - 銀行 */
    String kgiBankNo;
    /** 凱基 - 帳號 */
    String kgiAcctNo;
    /** 凱基 - 中文戶名 */
    String kgiAcctName;
    /** 凱基 - 帳務行/帳戶所屬行 */
    String kgiBranchNo;
}
