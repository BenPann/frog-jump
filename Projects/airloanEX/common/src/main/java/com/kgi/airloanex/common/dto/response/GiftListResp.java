package com.kgi.airloanex.common.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GiftListResp {

    private String giftTitle = "";
    private String giftCode = "";
    private String giftImage = "";
    private String giftNote = "";

}