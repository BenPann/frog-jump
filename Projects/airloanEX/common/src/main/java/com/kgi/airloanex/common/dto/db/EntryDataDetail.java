package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class EntryDataDetail {
    @NonNull
    private String UniqId;
    
	private String UserAgent;
}