package com.kgi.airloanex.common.dto.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * 同一關係人表
 */
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class CaseDataRelationDegree {
    @NonNull
    private String UniqId;
    /** 00-本人, 11-二等親配偶, 12-二等親子女, 13-二等親父親, 14-二等親母親, 21-本人任職公司資訊, 22-配偶任職公司資訊 */
	private String RelationDegree;
	private String Idno;
	private String Name;
	private String CorpName;
	private String CorpNumber;
	private String CorpJobTitle;
	private String Memo;

	public CaseDataRelationDegree(@NonNull String uniqId, String relationDegree, String idno, String name,
			String corpName, String corpNumber, String corpJobTitle, String memo) {
		UniqId = uniqId;
		RelationDegree = relationDegree;
		Idno = idno;
		Name = name;
		CorpName = corpName;
		CorpNumber = corpNumber;
		CorpJobTitle = corpJobTitle;
		Memo = memo;
	}
}
