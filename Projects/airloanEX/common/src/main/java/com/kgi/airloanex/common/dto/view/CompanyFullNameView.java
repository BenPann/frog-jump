package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyFullNameView {
    
    /** 名稱 */
    private String ShortName;
}
