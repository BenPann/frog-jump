package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InfoPlContractView {
    private String payMentDay;
    private String payBankOption;
    private String sordBankOption;
}