package com.kgi.airloanex.common.dto.customDto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetDgtConstRspDto {

    /**
     *  (1)	CHECK_CODE：
     *  A.	檢核資料錯誤(空白代表沒有錯誤)
     *  B.	如果有多個錯誤用『;』區隔,例如01;02
     *  C.	『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
     *  D.	錯誤代碼對照表:
     *  代碼  說明
     *  ============================
     *  99    非預期性系統異常
     *  01    客戶ID錯誤
     *  02    產品類型錯誤
     *  03    查無待立約資料
     */
    private String CHECK_CODE = "";

    /**
     * (2)	ERR_MSG：
     * A.	當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
     */
    private String ERR_MSG = "";

    /**
     * (3)	CONST_DATA： 
     * @see com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoConstData;
     */
    private String CONST_DATA;

    /**
     * (4)	DEP_DATA：
     * @see com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoDepData;
     */
    private String DEP_DATA;

    /**
     * (5)	RATE_DATA： 
     * A.	pay_rate_m：期間(月)
     * B.	pay_rate_date：生效日(YYYYMMDD)
     * C.	pay_rate：帳戶利率
     * @see com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoRateData;
     */
    private String RATE_DATA;

    // CONST_DATA
    // @Transient
    // Parse "[{},{}]" to [{},{}]
    private List<GetDgtConstRspDtoConstData> constDataList = new ArrayList<>();

    // DEP_DATA
    // @Transient
    // Parse "[{},{}]" to [{},{}]
    private List<GetDgtConstRspDtoDepData> depDataList = new ArrayList<>();

    // RATE_DATA
    // @Transient
    // Parse "[{},{}]" to [{},{}]
    private List<GetDgtConstRspDtoRateData> rateDataList = new ArrayList<>();

    public void setConstDataList(List<GetDgtConstRspDtoConstData> constDataList) {
        this.constDataList = constDataList;
    }

    // CONST_DATA
    public List<GetDgtConstRspDtoConstData> getConstDataList() {
        List<GetDgtConstRspDtoConstData> constDataList = new Gson().fromJson(this.CONST_DATA, new TypeToken<List<GetDgtConstRspDtoConstData>>() {}.getType());
        if (constDataList == null) {
            constDataList = new ArrayList<>();
        }
        this.constDataList = constDataList;
        return this.constDataList;
    }

    public GetDgtConstRspDtoConstData findOneConstData() {
        List<GetDgtConstRspDtoConstData> constDataList = getConstDataList();
        if (constDataList.size() > 0) {
            return constDataList.get(0);
        } else {
            return null;
        }
    }

    // DEP_DATA
    public List<GetDgtConstRspDtoDepData> getDepDataList() {
        List<GetDgtConstRspDtoDepData> depDataList = new Gson().fromJson(this.DEP_DATA, new TypeToken<List<GetDgtConstRspDtoDepData>>() {}.getType());
        if (depDataList == null) {
            depDataList = new ArrayList<>();
        }
        this.depDataList = depDataList;
        return this.depDataList;
    }

    public GetDgtConstRspDtoDepData findOneDepData() {
        List<GetDgtConstRspDtoDepData> depDataList = getDepDataList();
        if (depDataList.size() > 0) {
            return depDataList.get(0);
        } else {
            return null;
        }
    }

    // RATE_DATA
    public List<GetDgtConstRspDtoRateData> getRateDataList() {
        List<GetDgtConstRspDtoRateData> rateDataList = new Gson().fromJson(this.RATE_DATA, new TypeToken<List<GetDgtConstRspDtoRateData>>() {}.getType());
        if (rateDataList == null) {
            rateDataList = new ArrayList<>();
        }
        this.rateDataList = rateDataList;
        return this.rateDataList;
    }

    public GetDgtConstRspDtoRateData findOneRateData() {
        List<GetDgtConstRspDtoRateData> rateDataList = getRateDataList();
        if (rateDataList.size() > 0) {
            return rateDataList.get(0);
        } else {
            return null;
        }
    }

}