package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BNS085105", propOrder = { "CustName", "phone","birthday" , "detail"})
public class BNS08508100 { 

    @XmlElement(name="MobileNo")
    private String phone;

    @XmlElement(name="CustName")
    private String CustName;

    @XmlElement(name="BirthDate1")
    private String birthday;

    @XmlElement(name = "Detail")
    private List<BNS08508100Detail> detail;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCustName() {
        return CustName;
    }

    public void setCustName(String custName) {
        CustName = custName;
    }

    public List<BNS08508100Detail> getDetail() {
        return detail;
    }

    public void setDetail(List<BNS08508100Detail> detail) {
        this.detail = detail;
    }
    
    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "BNS08508100 [CustName=" + CustName + ", birthday=" + birthday + ", detail=" + detail + ", phone="
                + phone + "]";
    }
    
}
