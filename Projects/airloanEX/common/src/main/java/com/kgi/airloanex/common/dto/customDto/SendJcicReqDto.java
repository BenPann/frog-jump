package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SendJcicReqDto {
    
    /** (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string) */
    private String CASE_NO;

    /** (2)	APY_TYPE：產品別,1.現金卡(G+C)2.PLOAN(e貸寶)(P+G) (P+E) (P+C) (E+C) (P+G+C) (P+E+C)，(string) */
    private String APY_TYPE;

    /** (3)	PRODUCT: 發查產品,1.$04 2.$05(J10) 3.N30 4.票信 5.B72 6.Z07 7.Z13 8.B29($73) 9.A98，(string) */
    private String PRODUCT;

    /** (4)	UNIT_ID：單位代號，非必填，預設值為消金徵審，(string) */
    private String UNIT_ID;

}
