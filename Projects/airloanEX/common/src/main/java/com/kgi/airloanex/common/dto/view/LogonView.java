package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//@AllArgsConstructor
public class LogonView {
  
    private String uniqType;
    private String entry;
    // private String shortUrl;

    private String idno ;
    private String birthday ;

    private String ipAddress ;
    private String browser ;
    private String platform ;
    private String os ;
}
