package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplyRelationDegreeCorp {
    private String corpRelationDegree;
    private String corpName;
    private String corpnumber;
    private String corpJobTitle;
    private String memo;
}
