package com.kgi.eopend3.common.dto.view;

public class EntryView {

    private String qrcode = "";    
    
    public EntryView(){

    }
    public EntryView(String qrcode){
        this.qrcode = qrcode;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }
    

}