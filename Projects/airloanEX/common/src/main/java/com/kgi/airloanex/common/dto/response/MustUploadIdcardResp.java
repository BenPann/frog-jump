package com.kgi.airloanex.common.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MustUploadIdcardResp {
    
    private boolean mustUploadIdcard = true;
    private boolean mustUploadFinpf = true;
}
