package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PreAudit {

    /** idno */
    private String customerId;
    
    /** 貸款金額(萬元) */
    private String creditLine;
    
    /** 貸款利率(%) */
    private String interestRate;
    
    /**  */
    private String createTime;
    
    /**  */
    private String projectCode;
    
    private String caseNo;
    
    /** 1 男 2 女 */
    private String gender;
    
    /**  */
    private String lastscoreday;
}
