package com.kgi.airloanex.common.dto.db;

public class CreditCaseDataExtend extends CreditCaseData {


    private String UniqId = "";
    private String UniqType = "";
    
    private String Status = "";
    private String ApsStatus = "";
    //卡加貸 貸款用
    private String TransDepart = "";
    private String TransMember = "";
    private String CardSerial = "";
    /**MBC驗證是否通過 */
	private Boolean isMbcValid = false;
	/**是否同意使用中華電信資料 */
	private Boolean isChtAgree = false;
	
	private String MBCCheckCode="";
	private String MBCMessage="";
	private String ChtAgreeTime="";
	private String UpdateTime = "";


    public String getUniqId() {
        return UniqId;
    }

    public void setUniqId(String uniqId) {
        UniqId = uniqId;
    }

    public String getUniqType() {
        return UniqType;
    }

    public void setUniqType(String uniqType) {
        UniqType = uniqType;
    }

   

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getApsStatus() {
        return ApsStatus;
    }

    public void setApsStatus(String apsStatus) {
        ApsStatus = apsStatus;
    }

    public String getTransDepart() {
        return TransDepart;
    }

    public void setTransDepart(String transDepart) {
        TransDepart = transDepart;
    }

    public String getTransMember() {
        return TransMember;
    }

    public void setTransMember(String transMember) {
        TransMember = transMember;
    }

    public String getCardSerial() {
        return CardSerial;
    }

    public void setCardSerial(String cardSerial) {
        CardSerial = cardSerial;
    }

    public Boolean getMbcValid() {
        return isMbcValid;
    }

    public void setMbcValid(Boolean mbcValid) {
        isMbcValid = mbcValid;
    }

    public Boolean getChtAgree() {
        return isChtAgree;
    }

    public void setChtAgree(Boolean chtAgree) {
        isChtAgree = chtAgree;
    }

    public String getMBCCheckCode() {
        return MBCCheckCode;
    }

    public void setMBCCheckCode(String MBCCheckCode) {
        this.MBCCheckCode = MBCCheckCode;
    }

    public String getMBCMessage() {
        return MBCMessage;
    }

    public void setMBCMessage(String MBCMessage) {
        this.MBCMessage = MBCMessage;
    }

    public String getChtAgreeTime() {
        return ChtAgreeTime;
    }

    public void setChtAgreeTime(String chtAgreeTime) {
        ChtAgreeTime = chtAgreeTime;
    }

    public String getUpdateTime() {
        return UpdateTime;
    }

    public void setUpdateTime(String updateTime) {
        UpdateTime = updateTime;
    }


}
