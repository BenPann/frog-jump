package com.kgi.airloanex.common.dto.db;

public class AmlInfo {

    private String ID_NO = "";
    private String NAME = "";
    private String CHECK_DEPT = "";

    public String getID_NO() {
        return ID_NO;
    }

    public void setID_NO(String iD_NO) {
        ID_NO = iD_NO;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String nAME) {
        NAME = nAME;
    }

    public String getCHECK_DEPT() {
        return CHECK_DEPT;
    }

    public void setCHECK_DEPT(String cHECK_DEPT) {
        CHECK_DEPT = cHECK_DEPT;
    }
}
