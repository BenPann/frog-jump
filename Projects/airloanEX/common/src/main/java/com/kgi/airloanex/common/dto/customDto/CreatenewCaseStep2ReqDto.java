package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class CreatenewCaseStep2ReqDto {
    
    /** (1)	CASE_NO：案件編號(2.23功能取得的案件編號)，(string) */
    @NonNull
    private String CASE_NO;
}
