package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CheckGuarshipResultReqDto {
    
    /** (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string) */
    private String CASE_NO;

    /** (2)	CUSTOMER_NAME：客戶姓名，(string) */
    private String CUSTOMER_NAME;

}
