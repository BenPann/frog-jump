package com.kgi.crosssell.common.dto;

public class LoginFreeCif {
	private Integer status;
	private String message;
	private CSCommonData result;
	public Integer getStatus() {
		return status;
	}
	public String getMessage() {
		return message;
	}
	public CSCommonData getResult() {
		return result;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public void setResult(CSCommonData result) {
		this.result = result;
	}
	
	
}
