package com.kgi.eopend3.common.dto.view;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;

import lombok.*;

@Getter
@Setter
@CheckNullAndEmpty
public class TermPageView  {

    private String url;

}