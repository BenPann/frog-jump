package com.kgi.airloanex.common.dto.db;

import java.util.ArrayList;
import java.util.List;

public class ChannelData {
    private String ShortUrl = "";
    private String ChannelId = "";
    private String ChannelType = "";
    private String ThemePath = "";
    private String DomainUrl = "";
    private String MBCFlag = "";
    private String DepartId = "";
    private String TransDepart = "";
    private String TransMember = "";
    private String PromoDepart = "";
    private String PromoMember = "";
    private String PromoDepart1 = "";
    private String PromoMember1 = "";
    private String PromoDepart2 = "";
    private String PromoMember2 = "";
    private String Member = "";
    private String PProductType = "";
    private String Url = "";
    private String PProductId = "";
    private List<String> PProductIdList =  new ArrayList<String>();
    
    private String ChannelName = "";
    private String Entry = "";
    
    public String getEntry() {
		return Entry;
	}

	public void setEntry(String entry) {
		Entry = entry;
	}

	/**
     * @return the shortUrl
     */
    public String getShortUrl() {
        return ShortUrl;
    }

    /**
     * @return the pProductId
     */
    public String getPProductId() {
        return PProductId;
    }

    /**
     * @param pProductId the pProductId to set
     */
    public void setPProductId(String pProductId) {
        this.PProductId = pProductId;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return Url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.Url = url;
    }

    /**
     * @return the pProductType
     */
    public String getPProductType() {
        return PProductType;
    }

    /**
     * @param pProductType the pProductType to set
     */
    public void setPProductType(String pProductType) {
        this.PProductType = pProductType;
    }

    /**
     * @return the member
     */
    public String getMember() {
        return Member;
    }

    /**
     * @param member the member to set
     */
    public void setMember(String member) {
        this.Member = member;
    }

    /**
     * @return the promoMember2
     */
    public String getPromoMember2() {
        return PromoMember2;
    }

    /**
     * @param promoMember2 the promoMember2 to set
     */
    public void setPromoMember2(String promoMember2) {
        this.PromoMember2 = promoMember2;
    }

    /**
     * @return the promoDepart2
     */
    public String getPromoDepart2() {
        return PromoDepart2;
    }

    /**
     * @param promoDepart2 the promoDepart2 to set
     */
    public void setPromoDepart2(String promoDepart2) {
        this.PromoDepart2 = promoDepart2;
    }

    /**
     * @return the promoMember1
     */
    public String getPromoMember1() {
        return PromoMember1;
    }

    /**
     * @param promoMember1 the promoMember1 to set
     */
    public void setPromoMember1(String promoMember1) {
        this.PromoMember1 = promoMember1;
    }

    /**
     * @return the promoDepart1
     */
    public String getPromoDepart1() {
        return PromoDepart1;
    }

    /**
     * @param promoDepart1 the promoDepart1 to set
     */
    public void setPromoDepart1(String promoDepart1) {
        this.PromoDepart1 = promoDepart1;
    }

    /**
     * @return the promoMember
     */
    public String getPromoMember() {
        return PromoMember;
    }

    /**
     * @param promoMember the promoMember to set
     */
    public void setPromoMember(String promoMember) {
        this.PromoMember = promoMember;
    }

    /**
     * @return the promoDepart
     */
    public String getPromoDepart() {
        return PromoDepart;
    }

    /**
     * @param promoDepart the promoDepart to set
     */
    public void setPromoDepart(String promoDepart) {
        this.PromoDepart = promoDepart;
    }

    /**
     * @return the transMember
     */
    public String getTransMember() {
        return TransMember;
    }

    /**
     * @param transMember the transMember to set
     */
    public void setTransMember(String transMember) {
        this.TransMember = transMember;
    }

    /**
     * @return the transDepart
     */
    public String getTransDepart() {
        return TransDepart;
    }

    /**
     * @param transDepart the transDepart to set
     */
    public void setTransDepart(String transDepart) {
        this.TransDepart = transDepart;
    }

    /**
     * @return the departId
     */
    public String getDepartId() {
        return DepartId;
    }

    /**
     * @param departId the departId to set
     */
    public void setDepartId(String departId) {
        this.DepartId = departId;
    }

    /**
     * @return the mBCFlag
     */
    public String getMBCFlag() {
        return MBCFlag;
    }

    /**
     * @param mBCFlag the mBCFlag to set
     */
    public void setMBCFlag(String mBCFlag) {
        this.MBCFlag = mBCFlag;
    }

    /**
     * @return the domainUrl
     */
    public String getDomainUrl() {
        return DomainUrl;
    }

    /**
     * @param domainUrl the domainUrl to set
     */
    public void setDomainUrl(String domainUrl) {
        this.DomainUrl = domainUrl;
    }

    /**
     * @return the themePath
     */
    public String getThemePath() {
        return ThemePath;
    }

    /**
     * @param themePath the themePath to set
     */
    public void setThemePath(String themePath) {
        this.ThemePath = themePath;
    }

    /**
     * @return the channelType
     */
    public String getChannelType() {
        return ChannelType;
    }

    /**
     * @param channelType the channelType to set
     */
    public void setChannelType(String channelType) {
        this.ChannelType = channelType;
    }

    /**
     * @return the channelId
     */
    public String getChannelId() {
        return ChannelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(String channelId) {
        this.ChannelId = channelId;
    }

    /**
     * @param shortUrl the shortUrl to set
     */
    public void setShortUrl(String shortUrl) {
        this.ShortUrl = shortUrl;
    }

	public List<String> getPProductIdList() {
		return PProductIdList;
	}

	public void setPProductIdList(List<String> pProductIdList) {
		PProductIdList = pProductIdList;
	}

	public String getChannelName() {
		return ChannelName;
	}

	public void setChannelName(String channelName) {
		ChannelName = channelName;
	}
    
}