package com.kgi.eopend3.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CG_UserType {

    String UserTypeID = "";
    String UserTypeName = "";
    String UserTypeDef = "";
    
}