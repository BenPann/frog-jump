package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class ContractMainTerms {

    @NonNull
    private String UniqId; 
    @NonNull
    private int TermSerial;
    private String IsAgree;
}