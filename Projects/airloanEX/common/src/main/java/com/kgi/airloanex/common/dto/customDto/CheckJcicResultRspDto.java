package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CheckJcicResultRspDto {

    /*
     * (1)	CHECK_CODE：
     * A.	檢核資料錯誤(空白代表沒有錯誤)
     * B.	如果有多個錯誤用『;』區隔,例如01;02
     * C.	『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
     * D.	錯誤代碼對照表:
     * 代碼  說明
     * ============================
     * 01	   聯徵判讀結果格式錯誤
     * 99	  非預期性系統異常
     */
    private String CHECK_CODE;

    /*
     * (2)	ERR_MSG：
     * A.	當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
     */
    private String ERR_MSG;

    /*
     * (3)	RESULT：
     * A.	聯徵發查狀態：CHAR(1)，Y：正常，N：異常，E：Status不存在
     */
    private String RESULT;
}
