package com.kgi.eopend3.common.dto.db;

import com.kgi.eopend3.common.annotation.CheckNullAndEmpty;
import com.kgi.eopend3.common.annotation.MergeAlias;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class ED3_CaseData {
    @NonNull
    @CheckNullAndEmpty
    private String UniqId;
    private String UniqType;
    private String UserType;
    private String Status;
    @MergeAlias({"accountType"})
    private String Process;
    private String Idno;
    private String Birthday;
    private String Phone;
    @MergeAlias({"twTaxpayer"})
    private String IsTwTaxResident;
    private String PromoChannelID;
    private String PromoDepart;
    private String PromoMember;
    @MergeAlias({"deliveryType"})
    private String SetSecuritiesDelivery;
    private String SecuritiesAddr;
    private String SecuritiesAddrZipCode;
    private String SecuritiesCode;
    private String SecuritiesName;
    @MergeAlias({"creditType"})
    private String ProductId;
    private String ChtName;
    private String EngName;
    private String Gender;
    private String IdCardDate;
    private String IdCardLocation;
    private String IdCardRecord;
    private String ResAddrZipCode;
    private String ResAddrArea;
    private String ResAddr;
    private String CommAddrZipCode;
    private String CommAddrArea;
    private String CommAddr;
    private String BirthPlace;
    private String Education;
    private String Marriage;
    private String EmailAddress;
    private String SendType;
    private String ResTelArea;
    private String ResTel;
    private String HomeTelArea;
    private String HomeTel;
    private String EstateType;
    private String Purpose;
    private String BranchID;
    private String AuthorizeToallCorp;
    private String AutVer;
    private String Occupation;
    private String CorpName;
    private String CorpTelArea;
    private String CorpTel;
    private String CorpAddrZipCode;
    private String CorpAddrArea;
    private String CorpAddr;
    private String JobTitle;
    private String YearlyIncome;
    private String OnBoardDate;
    private String Injury;
    private String BreakPointPage;
    private String AccountType;
    private String SubCategory;
    private String AMLResult;
    private String IpAddress;
    private String CreateTime;
    private String UpdateTime;
    private String SendedTime;
    private String EndTime;
    // 中壽
    private String RouteID;
    private String ChannelStartPoint;
    private String CLToken;
    private String IsPayer;
    
}