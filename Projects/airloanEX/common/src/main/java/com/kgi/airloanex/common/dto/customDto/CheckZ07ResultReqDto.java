package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CheckZ07ResultReqDto {
    
    /** (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string) */
    private String CASE_NO;

    /** (2)	APY_TYPE：產品別,1.現金卡(e貸寶) 2.PLOAN，(string) */
    private String APY_TYPE;

    /** (3)	IS_DGT：是否為數位案件(Y/N)，(string) */
    private String IS_DGT;

    /** (4)	JCIC_DATE：聯徵查詢日期(YYYYMMDD)，(string) */
    private String JCIC_DATE;


}
