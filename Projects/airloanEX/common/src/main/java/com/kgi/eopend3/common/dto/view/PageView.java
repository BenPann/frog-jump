package com.kgi.eopend3.common.dto.view;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class PageView {

    private String pageName = "";    
    private String action = "GET";
}