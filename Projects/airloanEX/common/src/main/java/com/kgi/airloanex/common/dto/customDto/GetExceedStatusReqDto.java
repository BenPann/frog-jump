package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class GetExceedStatusReqDto {
    
    /** (1)	CASE_NO：徵審案件編號，NVARCHAR(10) */
    private String CASE_NO;
    /** (2)	EXP_BANK_CODE：撥款銀行代號，NVARCHAR(7) */
    private String EXP_BANK_CODE;
    /** (3)	EXP_BANK_ACCT_NO：撥款銀行帳號，NVARCHAR(20) */
    private String EXP_BANK_ACCT_NO;

}
