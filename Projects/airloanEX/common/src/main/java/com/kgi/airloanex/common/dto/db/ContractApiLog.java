package com.kgi.airloanex.common.dto.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Builder
public class ContractApiLog {

    private int Serial;
    @NonNull
    private String Type;
    @NonNull
    private String KeyInfo;
    @NonNull
    private String ApiName;
    @NonNull
    private String Request;
    @NonNull
    private String Response;
    @NonNull
    private String StartTime;
    @NonNull
    private String EndTime;
    private boolean Used;

}