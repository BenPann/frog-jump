package com.kgi.eopend3.common.dto.esbdto;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BNS032041", propOrder = { "status", "pbStatus" , "chopStatus", "noOfHold", "branchID", "idno" , "acctType" , "acctCat" })
public class BNS00040007 { 

    @XmlElement(name="Status1")
    private String status;

    @XmlElement(name="PBStatus1")
    private String pbStatus;

    @XmlElement(name = "ChopStatus1")
    private String chopStatus;

    @XmlElement(name = "NoOfHold1")
    private String noOfHold;

    @XmlElement(name = "BranchID1")
    private String branchID;

    @XmlElement(name = "IDNo")
    private String idno;

    @XmlElement(name = "AcctTyp1")
    private String acctType;

    @XmlElement(name = "AcctCat1")
    private String acctCat;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPbStatus() {
        return pbStatus;
    }

    public void setPbStatus(String pbStatus) {
        this.pbStatus = pbStatus;
    }

    public String getChopStatus() {
        return chopStatus;
    }

    public void setChopStatus(String chopStatus) {
        this.chopStatus = chopStatus;
    }

    public String getNoOfHold() {
        return noOfHold;
    }

    public void setNoOfHold(String noOfHold) {
        this.noOfHold = noOfHold;
    }

    public String getBranchID() {
        return branchID;
    }

    public void setBranchID(String branchID) {
        this.branchID = branchID;
    }

    public String getIdno() {
        return idno;
    }

    public void setIdno(String idno) {
        this.idno = idno;
    }

    @Override
    public String toString() {
        return "BNS00040007 [branchID=" + branchID + ", chopStatus=" + chopStatus + ", idno=" + idno + ", noOfHold="
                + noOfHold + ", pbStatus=" + pbStatus + ", status=" + status + "]";
    }

    public String getAcctType() {
        return acctType;
    }

    public void setAcctType(String acctType) {
        this.acctType = acctType;
    }

    public String getAcctCat() {
        return acctCat;
    }

    public void setAcctCat(String acctCat) {
        this.acctCat = acctCat;
    }
           
}
