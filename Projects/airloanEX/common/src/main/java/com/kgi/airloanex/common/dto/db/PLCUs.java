package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PLCUs {

    // private String Serial;
    private String UniqId;
    private String UniqType;
    private String Name;
    private String Phone;
    private String EMail;
    private String ContactTime;
    private String Note;
    private String ProductId;
    private String DepartId;
    private String Member;
    private String PrevPage;
    private String ContactFlag;
    private String UTime;
    private String Idno;
    private String Entry;
}
