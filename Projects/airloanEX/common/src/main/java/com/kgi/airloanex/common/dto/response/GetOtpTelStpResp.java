package com.kgi.airloanex.common.dto.response;

import java.util.ArrayList;
import java.util.List;

import com.kgi.airloanex.common.dto.customDto.GetOtpTelStpRspDtoOtpData;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetOtpTelStpResp {
    
    private List<GetOtpTelStpRspDtoOtpData> OTP_DATA = new ArrayList<>();
}
