package com.kgi.airloanex.common.dto.db;

import java.util.Date;

public class FastPass {

    private String CaseNo = "";
    private String AddPhoto = "";
    private String AML = "";
    private String AMLRequest = "";
    private String AMLResponse = "";
    private Date EndTime;
    private String DGTType = "";
    private Date CreateTime;
    private Date UpdateTime;
    
    public FastPass(){

    }

    public FastPass(String caseNo){
        this.setCaseNo(caseNo);
    }


	public String getCaseNo() {
		return CaseNo;
	}
	public void setCaseNo(String caseNo) {
		CaseNo = caseNo;
	}
	public String getAddPhoto() {
		return AddPhoto;
	}
	public void setAddPhoto(String addPhoto) {
		AddPhoto = addPhoto;
	}
	public String getAML() {
		return AML;
	}
	public void setAML(String aML) {
		AML = aML;
	}
	public String getAMLRequest() {
		return AMLRequest;
	}
	public void setAMLRequest(String aMLRequest) {
		AMLRequest = aMLRequest;
	}
	public String getAMLResponse() {
		return AMLResponse;
	}
	public void setAMLResponse(String aMLResponse) {
		AMLResponse = aMLResponse;
	}
	public Date getEndTime() {
		return EndTime;
	}
	public void setEndTime(Date endTime) {
		EndTime = endTime;
	}
	public String getDGTType() {
		return DGTType;
	}
	public void setDGTType(String dGTType) {
		DGTType = dGTType;
	}
	public Date getCreateTime() {
		return CreateTime;
	}
	public void setCreateTime(Date createTime) {
		CreateTime = createTime;
	}
	public Date getUpdateTime() {
		return UpdateTime;
	}
	public void setUpdateTime(Date updateTime) {
		UpdateTime = updateTime;
	}
}