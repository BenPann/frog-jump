package com.kgi.crosssell.common;

public class SystemConst {
	/** 中壽代碼 */
	public final static String UNIQ_TYPE_CL = "14";
	/** 數三代碼 */
	public final static String UNIQ_TYPE_D3 = "13";

	public final static String DEFAULT_CHANNEL_ID = "KB";

	/** QR_ChannelDepartListEOP 預設值 */
	public static final String QR_CHANNELDEPARTLISTEOP_DEFAULT = "-";

	/** 呼叫API共用常數 */

	/** 回傳代碼的KEY */
	public final static String API_RSPCODE = "RspCode";
	/** 回傳訊息的KEY */
	public final static String API_RSPMSG = "RspMsg";
	/** 回傳的子回應 */
	public final static String API_SUBRSP = "SubRsp";

	/** CaseAuth.AuthType */
	/** OTP驗證 */
	public final static String CASEAUTH_AUTH_TYPE_OTP = "1";
	/** MBC驗證 */
	public final static String CASEAUTH_AUTH_TYPE_MBC = "2";
	/** 證券異業 */
	public final static String CASEAUTH_AUTH_TYPE_SECURITY = "3";
	/** 新戶其他 */
	public final static String CASEAUTH_AUTH_TYPE_NCCC_OR_PC2566 = "4";

	/** 呼叫API預設值 */
	public final static int API_DEFAULT = -1;
	/** 呼叫API成功 */
	public final static int API_SUCCESS = 0;
	/** 呼叫API逾時 */
	public final static int API_TIMEOUT = 9000;
	/** 呼叫API其他錯誤(不明原因) */
	public final static int API_OTHER_ERROR = 9999;

	/** 預約分行流程 */
	public final static String CASEDATA_PROCESS_REV_BRANCH = "1";
	/** 數位帳戶流程 */
	public final static String CASEDATA_PROCESS_ED3 = "2";

	/** ED3_EX_CreditCaseData UserType 信用卡-使用者類型-新戶 */
	public static final String CREDITCASEDATA_USERTYPE_NEW = "0";
	/** ED3_EX_CreditCaseData UserType 信用卡-使用者類型-信用卡戶 */
	public static final String CREDITCASEDATA_USERTYPE_CREDIT = "1";
	/** ED3_EX_CreditCaseData UserType 信用卡-使用者類型-存款戶 */
	public static final String CREDITCASEDATA_USERTYPE_ACCOUNT = "2";
	/** ED3_EX_CreditCaseData UserType 信用卡-使用者類型-純貸款戶 */
	public static final String CREDITCASEDATA_USERTYPE_LOAN = "3";
	/** ED3_EX_CreditCaseData UserType 信用卡-使用者類型-其他 */
	public static final String CREDITCASEDATA_USERTYPE_OTHER = "4";

	/** OTP錯誤代碼 */
	/** OTP驗證碼 驗證成功 */
	public final static String OTP_SUCCESS = "0000";
	/** OTP驗證碼 輸入錯誤 */
	public final static String OTP_VERIFY_ERROR = "E001";
	/** OTP驗證碼 密碼驗證失敗，輸入密碼逾有效時限，請重新取得 */
	public final static String OTP_VERIFY_TIMEOUT = "E002";
	/** OTP驗證碼 驗證失敗 輸入密碼已使用 */
	public final static String OTP_VERIFY_USED = "E003";
	/** OTP驗證碼 要求簡訊傳送交易密碼超過次數上限 */
	public final static String OTP_SEND_OVER = "E004";
	/** OTP驗證碼 驗證失敗次數過多，進入延遲模式 */
	public final static String OTP_VERIFY_DELAY = "E005";
	/** OTP驗證碼 無此帳號，請確認您輸入的帳號正確或聯絡本行客服 */
	public final static String OTP_ERROR_ACCOUNT = "E006";
	/** OTP驗證碼 您未於本行登錄手機號碼，請聯絡本行客服 */
	public final static String OTP_ERROR_PHONE = "E007";
	/** 系統錯誤 */
	public final static String OTP_SYSTEM_ERROR = "F999";

	// 0000 取得：成功取得動態密碼，請檢視您的簡訊並輸入動態密碼
	// 密碼驗證成功：驗證OTP正確
	// E001 密碼輸入錯誤
	// E002 密碼驗證失敗，輸入密碼逾有效時限，請重新取得
	// E003 密碼驗證失敗，輸入密碼已使用
	// E004 要求簡訊傳送交易密碼超過次數上限
	// E005 OTP驗證失敗次數過多，進入延遲模式
	// E006 無此帳號，請確認您輸入的帳號正確或聯絡本行客服
	// E007 您未於本行登錄手機號碼，請聯絡本行客服
	// F999 系統錯誤

	/** 案件狀態 -init */
	public final static String CASE_STATUS_INIT = "00";
	/** 案件狀態 -填寫中 */
	public final static String CASE_STATUS_WRITING = "01";
	/** 案件狀態 -填寫完成 */
	public final static String CASE_STATUS_WRITE_SUCCESS = "02";
	/** 案件狀態 -30天自動結案 */
	public final static String CASE_STATUS_SUSPENDBY30DAYS = "06";
	/** 案件狀態 -斷點取消 */
	public final static String CASE_STATUS_CANCEL = "07";
	/** 案件狀態 -拒貸 */
	public final static String CASE_STATUS_REJECT = "08";
	/** 案件狀態 -拒貸已送件 */
	public final static String CASE_STATUS_REJECT_SENDED = "09";
	/** 案件狀態 -送件中 */
	public final static String CASE_STATUS_SUBMITTING = "11";
	/** 案件狀態 -送件完成 */
	public final static String CASE_STATUS_SUBMIT_SUCCESS = "12";
	/** 案件狀態 -無對應之信用卡產品 */
	public final static String CASE_STATUS_PRODUCT_ERROR = "17";
	/** 案件狀態 -送件失敗 */
	public final static String CASE_STATUS_SUBMIT_FAIL = "18";
	/** 案件狀態 -查無申請書無法送件 */
	public final static String CASE_STATUS_SUBMIT_ERROR = "19";
	/** 案件狀態 - 中壽投保逾時撤件 */
	public final static String CASE_STATUS_POLICYTIMEOUT = "20";
	/** 案件狀態 -APS作業中 */
	public final static String CASE_STATUS_APS_PROCESSING = "21";
	/** 案件狀態 -APS作業完成 */
	public final static String CASE_STATUS_APS_COMPLETE = "22";
	/** 案件狀態-等待立約 */
	public final static String CASE_STATUS_CONTRACT_WAIT = "91";
	/** 案件狀態-立約送出 */
	public final static String CASE_STATUS_CONTRACT_SUBMIT = "92";
	/** 案件狀態-立約成功 */
	public final static String CASE_STATUS_CONTRACT_SUCCESS = "93";

	/** 圖片上傳狀態-待處理(圖片上傳完成才會到此狀態) */
	public final static Integer PHOTO_STATUS_WAIT = 1;
	/** 圖片上傳狀態-客戶補件中 */
	public final static Integer PHOTO_STATUS_ADDING = 2;
	/** 圖片上傳狀態-已送影像系統 */
	public final static Integer PHOTO_STATUS_SUBMIT = 3;

	/** 利率體驗30天狀態 -可體驗 */
	public final static String QUOTACAL_CHKDATE_SUCCESS = "1";

	/** 利率體驗30天狀態 -不可體驗(30天內已有體驗) */
	public final static String QUOTACAL_CHKDATE_ERROR = "99";

	/** 上傳圖片線上線下狀態 -線上 */
	public final static Integer PHOTO_ONLINE_STATUS_ONLINE = 1;
	/** 上傳圖片線上線下狀態 -線下 */
	public final static Integer PHOTO_ONLINE_STATUS_OFFLINE = 2;
	/** 上傳圖片信用卡貸款種類 -信用卡 */
	public final static String PHOTO_PRODUCTTYPE_CREDIT = "1";
	/** 上傳圖片信用卡貸款種類 -貸款 */
	public final static String PHOTO_PRODUCTTYPE_LOAN = "2";

	/** 紀錄使用者的瀏覽歷程記錄的群組代碼 */
	public final static String BROWSE_STEP = "BrowseStep";

	/** 檢核申請人在APS系統是否有案件尚未結案 - 預審 */
	public final static String REP_CASE_DATA_TYPE_PREAUDIT = "1";
	/** 檢核申請人在APS系統是否有案件尚未結案 - 一般信貸 */
	public final static String REP_CASE_DATA_TYPE_LOAN = "2";
	/** 檢核申請人在APS系統是否有案件尚未結案 - 一般e貸寶 */
	public final static String REP_CASE_DATA_TYPE_ELOAN = "3";

	/** 立約產品代號 - 信貸 */
	public final static String CONTRACT_PRODUCT_ID_TYPE_LOAN = "1";
	/** 立約產品代號 - 現金卡 */
	public final static String CONTRACT_PRODUCT_ID_TYPE_CASH = "2";
	/** 立約產品代號 - 一般e貸寶 */
	public final static String CONTRACT_PRODUCT_ID_TYPE_ELOAN = "3";

	/** 貸款產品代號 - 個人信貸/循環信貸/分期攤還 */
	public final static String LOAN_PRODUCT_ID_TYPE_LOAN = "2";
	/** 貸款產品代號 - 一般e貸寶/隨借隨還 */
	public final static String LOAN_PRODUCT_ID_TYPE_ELOAN = "3";

	/** 信用卡產品上下架狀態-待上架 */
	public static final String CREDIT_PRODUCT_STATUS_WAITTOSUBMIT = "03";
	/** 信用卡產品上下架狀態-上架 */
	public static final String CREDIT_PRODUCT_STATUS_SUBMIT = "04";
	/** 信用卡產品上下架狀態-待下架 */
	public static final String CREDIT_PRODUCT_STATUS_WAITTOSUSPEND = "05";
	/** 信用卡產品上下架狀態-下架 */
	public static final String CREDIT_PRODUCT_STATUS_SUSPEND = "06";

	// public static final String AES_ENCRYPT_IV =
	// "oyU3JncP1DPa7Ul215_FC_K51l6QayBeh-hnMU37lh8";
	public static final String AES_ENCRYPT_IV = "fedcba9876543210";
	// public static final String AES_ENCRYPT_SECRETKEY =
	// "E_XKByfONVbCTF6X4OPOAD9__B4bv3gqY_TeTa6quFM";
	public static final String AES_ENCRYPT_SECRETKEY = "0123456789abcdef";

	/** VerifyLog 驗證別-中華電信 */
	public static final String VERIFYLOG_TYPE_CHT = "Cht";
	/** VerifyLog 驗證別-信用卡信用驗證 */
	public static final String VERIFYLOG_TYPE_NCCC = "NCCC";
	/** VerifyLog 驗證別-銀行信用驗證 */
	public static final String VERIFYLOG_TYPE_PCODE = "PCode2566";

	/** PrevPage 參數 */
	public static final String PREV_NONPREAUDITLOAN = "1";
	public static final String PREV_EXPERENCE = "2";
	public static final String PREV_PREAUDITLOAN = "3";
	public static final String PREV_CONTRACT = "4";
	public static final String PREV_NEWAPPLYLOAN = "5";
	public static final String PREV_INDEX = "6";
	public static final String PREV_LOANWITHCHANNEL = "7";
	public static final String PREV_REJECTMBC = "8"; // 首頁轉專人 || 驗證頁尚未驗證轉專人
	public static final String PREV_MBCBLACKLIST = "9"; // MBC黑名單
	public static final String PREV_MBCVALIDFALSE = "10";// MBC驗證失敗
	public static final String PREV_CHOOSEANOTHERLOANRATE = "11"; // MBC showloanrate後轉專人

	/** CSCommon的MEMO紀錄位置 */
	public static final String MEMO_CSCOMMON_MEMO = "cscommon.memo";
	/** CSCommon的CIF紀錄位置 */
	public static final String MEMO_CSCOMMON_CIF = "cscommon.cif";
	/** OCCredit的CIF紀錄位置 */
	public static final String MEMO_OCCREDIT_CIF = "occredit.cif";
	/** LoginFree的CIF紀錄位置 */
	public static final String MEMO_LOGINFREE_CIF = "loginfree.cif";

	/** 使用者圖片狀態 - 暫存 */
	public static final String USERPHOTO_STATUS_TEMP = "0";
	/** 使用者圖片狀態 - 使用者處理中(上傳中還未送出) */
	public static final String USERPHOTO_STATUS_USER_PROCESS = "1";
	/** 使用者圖片狀態 - 使用者上傳完成 等待JOB處裡 */
	public static final String USERPHOTO_STATUS_WAIT_UPLOAD = "2";
	/** 使用者圖片狀態 - 圖片上傳成功 */
	public static final String USERPHOTO_STATUS_UPLOAD_SUCCESS = "3";
	/** 使用者圖片狀態 - 圖片上傳失敗 */
	public static final String USERPHOTO_STATUS_UPLOAD_FAIL = "9";
	/** 使用者圖片狀態 - 特殊圖片暫存 */
	public static final String USERPHOTO_STATUS_SPECIAL_TEMP = "10";
	/** 使用者圖片狀態 - 從Addphoto尚未上傳Copy至userPhoto */
	public static final String USERPHOTO_STATUS_COPY_FROM_ADDPHOTO = "11";

	/** 數位信貸各種驗證-驗證類型 */
	public static final String CREDITVERIFY_TYPE_NCCC = "0";
	public static final String CREDITVERIFY_TYPE_BANKACOUNT = "1";
	public static final String CREDITVERIFY_TYPE_CHTMBC = "2";
	public static final String CREDITVERIFY_TYPE_CROSSSELL = "3"; // @2021-02-04備註: CC新戶證券驗身 or CC既有戶 OTP 驗身這裡都會標成 3
	public static final String CREDITVERIFY_TYPE_NOTVERIFY = "4";

	public static final String CREDITVERIFY_NCCC = "他行卡驗身";
	public static final String CREDITVERIFY_BANKACOUNT = "他行帳戶驗身";
	public static final String CREDITVERIFY_CHTMBC = "MBC";
	public static final String CREDITVERIFY_OTP = "OTP";
	public static final String CREDITVERIFY_CROSSSELL = "跨售證券驗身";
	public static final String CREDITVERIFY_NOTVERIFY = "線下驗身";

	/** PHOTO-TYPE */
	public static final Integer ADDPHOTO_PTYPE_IDENTITY = 1; // 身分證明
	public static final Integer ADDPHOTO_PTYPE_FINANCIAL = 2; // 財力證明
	public static final String ADDPHOTO_STYPE_IDENTITYFRONT = "1"; // 身分證正面
	public static final String ADDPHOTO_STYPE_IDENTITYBACK = "2";// 身分證反面
	public static final String ADDPHOTO_STYPE_HEALTHCARDFRONT = "3";// 健保卡正面
	public static final String ADDPHOTO_STYPE_HEALTHCARDBACK = "4";// 健保卡反面
	public static final String ADDPHOTO_STYPE_DRIVELICENSEFRONT = "5";// 駕照正面
	public static final String ADDPHOTO_STYPE_DRIVELICENSEBACK = "6";// 駕照反面
	public static final String ADDPHOTO_STYPE_SALARY = "20";// 薪資轉帳證明
	public static final String ADDPHOTO_STYPE_LANDOWNER = "21";// 土地謄本
	public static final String ADDPHOTO_STYPE_BANKSAVE = "22";// 存款證明
	public static final String ADDPHOTO_STYPE_PAYMENT = "23";// 扣繳憑單
	public static final String ADDPHOTO_STYPE_INCOMELIST = "24";// 所得清單
	public static final String ADDPHOTO_STYPE_SECURITYSTOCK = "25";// 證券庫存
	public static final String ADDPHOTO_STYPE_OTHER = "98";
	public static final String ADDPHOTO_STYPE_OTHER2 = "99";// 不知道為什麼會有兩個OTHER 但ADDPHOTODAO>parseDest有存，就先留。

	/**
	 * ADDPHOTOSTATUS-STATUS
	 */
	public static final String ADDPHOTOSTATUS_STATUS_WAIT = "1";
	public static final String ADDPHOTOSTATUS_STATUS_CUSTOMERUPLOAD = "2";
	public static final String ADDPHOTOSTATUS_STATUS_SENDTOIMAGECENTER = "3";
	public static final String ADDPHOTOSTATUS_STATUS_UPLOADTOUSERPHOTO = "4";

	/**
	 * 申辦產品
	 */
	/** 信用卡 */
	public static final String ADDPHOTO_PROD_TYPE_CREDIT = "1";
	/** 貸款 */
	public static final String ADDPHOTO_PROD_TYPE_LOAN = "2";
	/** 薪轉開戶 */
	public static final String ADDPHOTO_PROD_TYPE_EOP = "3";
	/** 數位帳戶 */
	public static final String ADDPHOTO_PROD_TYPE_ED3 = "4";

	/** 白名單種類 -信用卡 */
	public final static String WhiteList_CREDIT = "0";
	/** 白名單種類 -貸款 */
	public final static String WhiteList_LOAN = "1";
	/** 白名單種類 -立約 */
	public final static String WhiteList_Contract = "2";

	public final static String DOCUMENT_TYPE_LOAN_APPLYPDF = "loan.applypdf";
	public final static String DOCUMENT_TYPE_LOAN_REJECTPDF = "loan.rejectpdf";
	public final static String DOCUMENT_TYPE_CREDIT_APPLYPDF = "credit.applypdf";
	public final static String DOCUMENT_TYPE_CONTRACT_PDF = "contract.pdf";

	public static final String KGI_BANK_ID = "809";

	public static final String API_SEND_OTP = "sendOTP";
	public static final String API_CHECK_OTP = "checkOTP";
	public static final String API_KGI_PCODE2566 = "kgiPcode2566";
	public static final String API_OTHER_PCDOE2566 = "otherPCode2566";
	public static final String API_KGI_NCCC = "kgiNCCC";
	public static final String API_OTHER_NCCC = "otherNCCC";

	/** IdentityVerification 驗證成功 */
	public static final String VERIFY_STATUS_SUCCESS = "1";
	/** IdentityVerification 驗證失敗 */
	public static final String VERIFY_STATUS_FAIL = "0";

	/** Product_Type_List PProduct_Type 貸款 */
	public static final String PRODUCTTYPELIST_PPRODUCTTYPE_LOAN = "0";
	/** Product_Type_List PProduct_Type 信用卡 */
	public static final String PRODUCTTYPELIST_PPRODUCTTYPE_CREDITCARD = "1";

	public static final String[][] HTTP_HEADERS = { { "Accept", "application/json,application/text,text/plain" },
			{ "Accept-Charset", "utf-8" }, { "Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7" },
			{ "@ConnectTimeout", "90000" }, { "@ReadTimeout", "90000" }
			 };
}
