package com.kgi.eopend3.common.dto.customDto;

public class PCode2566Dto {
	private int result = -1 ;
	private String statusCode = "" ;
	private String message = "" ;
	
	private String verifyCodeByVerifyType ="" ;
	private String verifyCodeByAccountState = "" ;
	private String verifyCodeByOpenAccountState = "" ;

	
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getVerifyCodeByVerifyType() {
		return verifyCodeByVerifyType;
	}
	public void setVerifyCodeByVerifyType(String verifyCodeByVerifyType) {
		this.verifyCodeByVerifyType = verifyCodeByVerifyType;
	}
	public String getVerifyCodeByAccountState() {
		return verifyCodeByAccountState;
	}
	public void setVerifyCodeByAccountState(String verifyCodeByAccountState) {
		this.verifyCodeByAccountState = verifyCodeByAccountState;
	}
	public String getVerifyCodeByOpenAccountState() {
		return verifyCodeByOpenAccountState;
	}
	public void setVerifyCodeByOpenAccountState(String verifyCodeByOpenAccountState) {
		this.verifyCodeByOpenAccountState = verifyCodeByOpenAccountState;
	}

}
