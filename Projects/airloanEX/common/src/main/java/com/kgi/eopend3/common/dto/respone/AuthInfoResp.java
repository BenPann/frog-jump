package com.kgi.eopend3.common.dto.respone;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthInfoResp {

    private String authorizeToallCorp = "";    
    private String verNo = "";
}