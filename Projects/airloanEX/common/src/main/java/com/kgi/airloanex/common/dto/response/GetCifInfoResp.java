package com.kgi.airloanex.common.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetCifInfoResp {
    
    private String email;
}