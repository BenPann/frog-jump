package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CSInsertFucoApiLog {
	private String prodid;
    private String channelId;
    private String token;
    private String idno;
    private String ip;
    private String shortUrl;
    private	String depart;
    private String member;
    private String entry;
}