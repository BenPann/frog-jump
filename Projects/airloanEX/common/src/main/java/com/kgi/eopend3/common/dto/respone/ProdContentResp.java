package com.kgi.eopend3.common.dto.respone;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ProdContentResp {

    private String ed3CCAProdContent;
    private String ed3AProdContent;

}