package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplyUploadIdcardView {

    // {
    // idcardCRecord: "3"
    // idcardDate: "0940701"
    // idcardLocation: "北市"
    // countiesName: "台北市"
    // townshipName: "大同區"
    // township: 114
    // address: "葫洲里1民權東路六段283巷15弄218號"
    // }

    /** 發證紀錄 */
    private String idcardCRecord;
    /** 發證日期 eeeMMdd(民國年月日7碼) */
    private String idcardDate;
    /** 發證地點 */
    private String idcardLocation;
    /** 縣市名稱 */
    private String countiesName;
    /** 區域名稱 */
    private String townshipName;
    /** zipcode */
    private String township;
    /** 地址 */
    private String address;
    /** 婚姻:未婚/已婚 */
    private String marriage;
}
