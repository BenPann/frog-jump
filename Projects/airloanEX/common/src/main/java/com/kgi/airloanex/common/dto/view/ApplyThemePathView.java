package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplyThemePathView {
    private String channelId;
}
