package com.kgi.airloanex.common.dto.db;

public class CreditEopDataDto {
    private String PayDate1="";
    private String PayDate2="";
    private String CorpID="";

    public String getPayDate1() {
        return PayDate1;
    }

    public void setPayDate1(String payDate1) {
        PayDate1 = payDate1;
    }

    public String getPayDate2() {
        return PayDate2;
    }

    public void setPayDate2(String payDate2) {
        PayDate2 = payDate2;
    }

    public String getCorpID() {
        return CorpID;
    }

    public void setCorpID(String corpID) {
        CorpID = corpID;
    }
}
