package com.kgi.airloanex.common.dto.orbit;

import static com.kgi.airloanex.common.PlContractConst.IsPreAudit_1;

import com.kgi.airloanex.common.PlContractConst;

import org.apache.commons.lang3.StringUtils;

public class OrbitImage extends OrbitData {

	public String fileName;	    			
	public String caseId = "";					
	public String FMID = "";				
	public String covertPage = "";			
	public String productType = "";			
	public String branchType = "";			
	public String casePriority = "";					
	public String documentType = "";
	public String cardApplyType = "";
	public String cardType = "";
	public String idNumber = "";
	public String cardNumber = "";
	public String detailId = "";
	public String imgType = "";	
	
	//MainRow  web進件
	public static OrbitImage from(String fileName, String caseId, String documentType, String idNumber, String branchType, 
			 					  String productId, boolean isApplyLoanCC, String IsPreAudit, String casePriority, String imgType) {
		OrbitImage orbitImage = new OrbitImage();
		orbitImage.fileName = fileName;
		orbitImage.caseId = caseId;
		orbitImage.FMID = "A001"; // FMID 固定A001
		orbitImage.covertPage = "A001"; // CovertPage 固定A001
		
		//如果確認後是卡加貸產品 將P035 改成 P037
		orbitImage.productType = productType(isApplyLoanCC, IsPreAudit);
		orbitImage.branchType = "B".concat(branchType); // branchType
		//orbitImage.casePriority = "F001";	 // 固定F001	
		
		//4/23 因應中壽認股案 修改成
		if (StringUtils.isNotBlank(casePriority)) {
			orbitImage.casePriority = casePriority;
		} else {
			orbitImage.casePriority = casePriority(productId);
		}
		
		
		orbitImage.documentType = documentType; // 合約C003
		orbitImage.idNumber = idNumber;
		orbitImage.imgType = imgType;
		return orbitImage;
	}

	//MainRow  退件專用
	/** 此為退件專用 */
	public static OrbitImage rejectFrom(String fileName, String caseId, String documentType, String idNumber,
			String productId, String branchId) {
		OrbitImage orbitImage = new OrbitImage();
		orbitImage.fileName = fileName;
		orbitImage.caseId = caseId;
		orbitImage.FMID = "A001"; // FMID 固定A001
		orbitImage.covertPage = "A001"; // CovertPage 固定A001

		// TODO 之後因應QRCode產生器 會再做調整
		// E貸寶不支援自動簽收 所以帶P002 其餘帶P035
		// 產品別
		if (productId.equals("3")) {
			// E貸寶 P002
			orbitImage.productType = "P002";
		} else if (productId.equals("2")) {
			// PLOAN P003
			orbitImage.productType = "P035";
		} else {
			// 預設值
			orbitImage.productType = "P035";
		}

		orbitImage.branchType = "B".concat(branchId); // branchType
		orbitImage.casePriority = "F001"; // 固定F001
		orbitImage.documentType = documentType; // 合約C003
		orbitImage.idNumber = idNumber;

		return orbitImage;
	}

	//SubRow
	public static OrbitImage from(String fileName, String imgType) {
		OrbitImage orbitImage = new OrbitImage();
		orbitImage.fileName = fileName;
		orbitImage.imgType = imgType;
		return orbitImage;
	}
	//MainRow 契約書
		public static OrbitImage contractfrom(String fileName, String caseId, String idNumber, String product, String branchId, String casePriority, String imgType) {
			OrbitImage orbitImage = new OrbitImage();
			orbitImage.fileName = fileName;
			orbitImage.caseId = caseId;
			orbitImage.FMID = "A001";				// FMID       固定A001
			orbitImage.covertPage = "A001";			// CovertPage	固定A001
			
			// 產品別 
			switch(product) {
			case PlContractConst.CONTRACT_PRODUCT_ID_TYPE_CASH:
			case PlContractConst.CONTRACT_PRODUCT_ID_TYPE_ELOAN:
				orbitImage.productType = "P002";
				break;
			case PlContractConst.CONTRACT_PRODUCT_ID_TYPE_LOAN:
				orbitImage.productType = "P003";
				break;
			default:
				orbitImage.productType = "P003";//預設時暫時給P003
				break;
			}
					
			// orbitImage.branchType = "B9743";		// branchType
			orbitImage.branchType = "B".concat(branchId);
			if(!casePriority.equals("")) {
				orbitImage.casePriority = casePriority;
			}else {
				orbitImage.casePriority = "F032";		// 改合約F0320
			}
			orbitImage.documentType = "C003"; 		// 合約C003
			orbitImage.idNumber = idNumber;
			orbitImage.imgType = imgType;
			return orbitImage;
	}
	
	@Override
	public void toXML(StringBuilder sb) {
		sb.append("<row");
		putXML(sb, "fileName", fileName);
		putXML(sb, "caseId", caseId);
		putXML(sb, "FMID", FMID);
		putXML(sb, "covertPage", covertPage);
		putXML(sb, "productType", productType);
		putXML(sb, "branchType", branchType);
		putXML(sb, "casePriority", casePriority);
		putXML(sb, "documentType", documentType);
		putXML(sb, "cardApplyType", cardApplyType);
		putXML(sb, "cardType", cardType);
		putXML(sb, "idNumber", idNumber);
		putXML(sb, "cardNumber", cardNumber);
		putXML(sb, "detailId", detailId);
		putXML(sb, "imgType", imgType);
		
		sb.append(" />");
	}

	/**
     * &lt;productTypeID&gt;
     */
    public static String productTypeId(boolean isApplyLoanCC) {
        // 卡加貸 - productTypeID=P037
        // PA - productTypeID=P035 casePriority=F001
        // PL - productTypeID=P035 casePriority=F037
        // RPL - productTypeID=P035 casePriority=F036
        String productTypeId;
        if (isApplyLoanCC) {
            productTypeId = "P037";
        } else {
            productTypeId = "P035";
        }
        return productTypeId;
    }

	/** &lt;productType&gt; */
	private static String productType(boolean isApplyLoanCC, String IsPreAudit) {
		String productType = productTypeId(isApplyLoanCC);
		if (isPreAudit(IsPreAudit)) { // 預核件XML中，ROW1的ProductType改為P003(與非預核PL P035不同)，上方ProductTypeId維持P035
			productType = "P003";
		}
		return productType;
	}

	/** 判斷是否為預核名單 CaseData.IsPreAudit = 1 */
	private static boolean isPreAudit(String IsPreAudit) {
		return StringUtils.equals(IsPreAudit_1, IsPreAudit);
	}

    /**
     * &lt;row casePriority="F037"&gt;
     */
    public static String casePriority(String productId) {
        // 卡加貸 - productTypeID=P037
        // PA - productTypeID=P035 casePriority=F001
        // PL - productTypeID=P035 casePriority=F037
        // RPL - productTypeID=P035 casePriority=F036
        String casePriority;
        if (StringUtils.equals("1", productId)) { // PA(預核名單)
            casePriority = "F001";
        } else if (StringUtils.equals("2", productId)) { // PL
            casePriority = "F037";
        } else if (StringUtils.equals("3", productId)) { // RPL
            casePriority = "F036";
        } else { // Others
            casePriority = "F001";
        }
        return casePriority;
    }
}
