package com.kgi.airloanex.common.dto.db;

public class CS_VerifyLog {
	
	private String UniqId="";
	private String UniqType	="";
	private String Idno	="";
	private String IpAddress="";
	private String Time="";
	private String MessageSerial="";
	private String ContractVersion="";
	private String CreateTime="";
	public String getUniqId() {
		return UniqId;
	}
	public void setUniqId(String uniqId) {
		UniqId = uniqId;
	}
	public String getUniqType() {
		return UniqType;
	}
	public void setUniqType(String uniqType) {
		UniqType = uniqType;
	}
	public String getIdno() {
		return Idno;
	}
	public void setIdno(String idno) {
		Idno = idno;
	}
	public String getIpAddress() {
		return IpAddress;
	}
	public void setIpAddress(String ipAddress) {
		IpAddress = ipAddress;
	}
	public String getTime() {
		return Time;
	}
	public void setTime(String time) {
		Time = time;
	}
	public String getMessageSerial() {
		return MessageSerial;
	}
	public void setMessageSerial(String messageSerial) {
		MessageSerial = messageSerial;
	}
	public String getContractVersion() {
		return ContractVersion;
	}
	public void setContractVersion(String contractVersion) {
		ContractVersion = contractVersion;
	}
	public String getCreateTime() {
		return CreateTime;
	}
	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}
}
