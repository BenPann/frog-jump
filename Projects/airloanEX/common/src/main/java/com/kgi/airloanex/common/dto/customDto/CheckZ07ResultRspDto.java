package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CheckZ07ResultRspDto {

    /*
     * (1)	CHECK_CODE：
     * A.	檢核資料錯誤(空白代表沒有錯誤)
     * B.	如果有多個錯誤用『;』區隔,例如01;02
     * C.	『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
     * D.	錯誤代碼對照表:
     * 代碼  說明
     * ============================
     * 01	   案件編號格式錯誤
     * 02	   案件編號不存在
     * 03	   產品別格式錯誤
     * 04	   是否為數位案件格式錯誤
     * 05	   聯徵查詢日期格式錯誤
     * 99	  非預期性系統異常
     */
    private String CHECK_CODE;

    /*
     * (2)	ERR_MSG：
     * A.	當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
     */
    private String ERR_MSG;
    
    /*
     * (3)	RESULT：
     * A.	回應代碼:CHAR(1)，Y：是(有通報案件記錄)，N：否
     */
    private String RESULT;

}
