package com.kgi.airloanex.common.dto.customDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SendSMSReqDto {
    
    private String phoneNumber; 
    
    private String billDepart; 
    
    private String message;
}
