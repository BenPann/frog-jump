package com.kgi.eopend3.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BCOCRRes {

    private String CompanyName = "";
    private String CompanyTelNum1 = "";
    private String CompanyTelNum2 = "";
    private String CompanyAddr1 = "";
    private String CompanyAddr2 = "";
    private String CompanyAddr3 = "";
    private String ZIPCODE = "";

}