package com.kgi.eopend3.common.dto.customDto;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class ChinalifeCifRs {

	private String msgId = "";
	private String stsCode = "";
	private String stsDesc = "";
	private String shortUrl = "";
	private String entry = "";
	private String caseId = "";
	private String applNo = "";
	private String applyPdc = "";
	private String applyPdcRePmt = "";
	private String applyPmtEndDt = "";
	private String msgSendDt = "";
	
	@SerializedName(value="tokenInfo")
	private ChinaTokenInfo tokenInfo = null;
	
	@SerializedName(value="cifInfo")
	private ChinaCifInfo cifInfo = null;
	
	@SerializedName(value="addInfo")
	private ChinaAddInfo addInfo = null;

	
	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getStsCode() {
		return stsCode;
	}

	public void setStsCode(String stsCode) {
		this.stsCode = stsCode;
	}

	public String getStsDesc() {
		return stsDesc;
	}

	public void setStsDesc(String stsDesc) {
		this.stsDesc = stsDesc;
	}

	public String getShortUrl() {
		return shortUrl;
	}

	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}

	public String getEntry() {
		return entry;
	}

	public void setEntry(String entry) {
		this.entry = entry;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public String getApplNo() {
		return applNo;
	}

	public void setApplNo(String applNo) {
		this.applNo = applNo;
	}

	public String getApplyPdc() {
		return applyPdc;
	}

	public void setApplyPdc(String applyPdc) {
		this.applyPdc = applyPdc;
	}

	public String getApplyPdcRePmt() {
		return applyPdcRePmt;
	}

	public void setApplyPdcRePmt(String applyPdcRePmt) {
		this.applyPdcRePmt = applyPdcRePmt;
	}

	public String getApplyPmtEndDt() {
		return applyPmtEndDt;
	}

	public void setApplyPmtEndDt(String applyPmtEndDt) {
		this.applyPmtEndDt = applyPmtEndDt;
	}

	public String getMsgSendDt() {
		return msgSendDt;
	}

	public void setMsgSendDt(String msgSendDt) {
		this.msgSendDt = msgSendDt;
	}

	public ChinaTokenInfo getTokenInfo() {
		return tokenInfo;
	}

	public void setTokenInfo(ChinaTokenInfo tokenInfo) {
		this.tokenInfo = tokenInfo;
	}

	public ChinaCifInfo getCifInfo() {
		return cifInfo;
	}

	public void setCifInfo(ChinaCifInfo cifInfo) {
		this.cifInfo = cifInfo;
	}

	public ChinaAddInfo getAddInfo() {
		return addInfo;
	}

	public void setAddInfo(ChinaAddInfo addInfo) {
		this.addInfo = addInfo;
	}


	public String toJsonString() {
		return new GsonBuilder().disableHtmlEscaping().create().toJson(this) ;
	}

}
