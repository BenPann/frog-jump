package com.kgi.crosssell.common.dto;

import com.google.gson.annotations.SerializedName;
import com.kgi.crosssell.common.util.CommonFunctionUtil;

import net.sf.json.JSONObject;

public class ApplyData {
	private final String AddrSplitStr = ";";

	/** 此為數位信貸產生的CaseNo */
	@SerializedName("CaseNo")
	private String caseNo = "";
	/** 此為APS系統產生的暫時CaseNo */
	@SerializedName("CaseNoWeb")
	private String caseNoWeb = "";
	/** 此為APS系統產生的真實CaseNo */
	@SerializedName("CaseNoAps")
	private String caseNoAps = "";
	@SerializedName("GmCardNo")
	private String gmCardNo = "";
	@SerializedName("IsPreAudit")
	private String isPreAudit = "";
	@SerializedName("ProductId")
	private String productId = "";
	@SerializedName("Occupation")
	private String occupation = "";
	@SerializedName("Idno")
	private String idno = "";
	private String gender = "";
	@SerializedName("customer_name")
	private String customerName = "";
	private String birthday = "";
	// 2018/10/30 MBC新戶貸款用 戶藉地址、電話
	private String addressCode = ""; // CaseData.ResAddrZipCode
	private String address = ""; // CaseData.ResAddr
	private String telArea = ""; // CaseData.ResTelArea
	private String tel = ""; // CaseData.ResTel

	private String houseCityCode = "";
	private String houseAddress = "";
	private String houseTelArea = "";
	private String houseTel = "";
	private String education = "";
	private String marriage = "";
	private String corpName = "";
	private String corpCityCode = "";
	private String corpAddress = "";
	private String corpTelArea = "";
	private String corpTel = "";
	private String corpTelExten = "";
	private String corpClass = "";
	private String corpType = "";
	private String title = "";
	private String onBoardDate = "";
	private String yearlyIncome = "";
	private String estateType = "";
	@SerializedName("p_apy_amount")
	private String pApyAmount = "";
	private String mobileTel = "";
	@SerializedName("p_period")
	private String pPeriod = "";
	@SerializedName("p_purpose")
	private String pPurpose = "";
	private String emailAddress = "";
	private String ipAddress = "";
	private String prjCode = "";
	private String caseNoEst = "";
	private String contactMe = "";
	@SerializedName("p_purpose_name")
	private String pPurposeName = "";
	@SerializedName("Status")
	private String caseStatus = "";
	/** 是否是斷點接續案件 */
	private Boolean isBreakPoint = false;
	/** MBC驗證是否通過 */
	private Boolean isMbcValid = false;
	/** 是否同意使用中華電信資料 */
	private Boolean isChtAgree = false;
	/** 使用的ChnanelId */
	private String channelId = "";

	private String MBCCheckCode = "";
	private String MBCMessage = "";
	private String ChtAgreeTime = "";

	private String uniqType = "";

	// 沙盒案新增存中華電信評
	private JSONObject ChtCreditRating = new JSONObject();

	private String AUM_UUID = "";
	private String HasAum = "";
	private String UpdateAum = "";

	private String UserType;

	public String getCaseNoWeb() {
		return caseNoWeb;
	}

	/**
	 * @return the isChtAgree
	 */
	public Boolean getIsChtAgree() {
		return isChtAgree;
	}

	/**
	 * @param isChtAgree the isChtAgree to set
	 */
	public void setIsChtAgree(Boolean isChtAgree) {
		this.isChtAgree = isChtAgree;
	}

	/**
	 * @return the channelId
	 */
	public String getChannelId() {
		return channelId;
	}

	/**
	 * @param channelId the channelId to set
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public void setCaseNoWeb(String caseNoWeb) {
		this.caseNoWeb = caseNoWeb;
	}

	/**
	 * @return the isMbcValid
	 */
	public Boolean getIsMbcValid() {
		return isMbcValid;
	}

	/**
	 * @param isMbcValid the isMbcValid to set
	 */
	public void setIsMbcValid(Boolean isMbcValid) {
		this.isMbcValid = isMbcValid;
	}

	public String getCaseNoAps() {
		return caseNoAps;
	}

	public void setCaseNoAps(String caseNoAps) {
		this.caseNoAps = caseNoAps;
	}

	public String getGmCardNo() {
		return gmCardNo;
	}

	public void setGmCardNo(String gmCardNo) {
		this.gmCardNo = gmCardNo;
	}

	public String getIsPreAudit() {
		return isPreAudit;
	}

	public void setIsPreAudit(String isPreAudit) {
		this.isPreAudit = isPreAudit;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getCaseNo() {
		return caseNo;
	}

	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getHouseCityCode() {
		return houseCityCode;
	}

	public void setHouseCityCode(String houseCityCode) {
		this.houseCityCode = houseCityCode;
	}

	/** 取得沒有地區的戶籍地址 */
	public String getRegAddressNoArea() {
		String[] arr = address.split(AddrSplitStr);
		if (arr.length >= 2) {
			return arr[1];
		} else {
			// 以防萬一，再用程式檢查一次
			String addressHandler = CommonFunctionUtil.addressHandler(address, AddrSplitStr);
			String[] split = addressHandler.split(AddrSplitStr);
			if (split != null && split.length > 0) {
				return split[split.length - 1];
			} else {
				return "";
			}
		}
	}

	public void setRegAddressNoArea() {
		// skipped
	}

	/** 取得沒有地區的地址 */
	public String getHouseAddressNoArea() {
		String[] arr = houseAddress.split(AddrSplitStr);
		if (arr.length >= 2) {
			return arr[1];
		} else {
			// 以防萬一，再用程式檢查一次
			String addressHandler = CommonFunctionUtil.addressHandler(houseAddress, AddrSplitStr);
			String[] split = addressHandler.split(AddrSplitStr);
			if (split != null && split.length > 0) {
				return split[split.length - 1];
			} else {
				return "";
			}
		}
	}

	public void setHouseAddressNoArea() {
		// skipped
	}

	/** 取得沒有區隔地區跟地址的字串 */
	public String getHouseAddressFull() {
		return houseAddress.replace(AddrSplitStr, "");
	}

	public void setHouseAddressFull() {
		// skipped
	}

	/** 取得用分號區隔地區跟地址的字串 */
	public String getHouseAddress() {
		return houseAddress;
	}

	public void setHouseAddress(String houseArea, String houseAddress) {
		this.houseAddress = houseArea.concat(AddrSplitStr).concat(houseAddress);
	}

	public void setHouseAddress(String houseAddress) {
		this.houseAddress = houseAddress;
	}

	public String getHouseTelArea() {
		return houseTelArea;
	}

	public void setHouseTelArea(String houseTelArea) {
		this.houseTelArea = houseTelArea;
	}

	public String getHouseTel() {
		return houseTel;
	}

	public void setHouseTel(String houseTel) {
		this.houseTel = houseTel;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getMarriage() {
		return marriage;
	}

	public void setMarriage(String marriage) {
		this.marriage = marriage;
	}

	public String getCorpName() {
		return corpName;
	}

	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}

	public String getCorpCityCode() {
		return corpCityCode;
	}

	public void setCorpCityCode(String corpCityCode) {
		this.corpCityCode = corpCityCode;
	}

	/** 取得沒有地區的地址 */
	public String getCorpAddressNoArea() {
		String[] arr = corpAddress.split(AddrSplitStr);
		if (arr.length >= 2) {
			return arr[1];
		} else {
			// 以防萬一，再用程式檢查一次
			String addressHandler = CommonFunctionUtil.addressHandler(corpAddress, AddrSplitStr);
			String[] split = addressHandler.split(AddrSplitStr);
			if (split != null && split.length > 0) {
				return split[split.length - 1];
			} else {
				return "";
			}
		}
	}

	public void setCorpAddressNoArea() {
		// skipped
	}

	/** 取得沒有區隔地區跟地址的字串 */
	public String getCorpAddressFull() {
		return corpAddress.replace(AddrSplitStr, "");
	}

	public void setCorpAddressFull() {
		// skipped
	}

	/** 取得用分號區隔地區跟地址的字串 */
	public String getCorpAddress() {
		return corpAddress;
	}

	public void setCorpAddress(String corpArea, String corpAddress) {
		if (!corpArea.equals("") && !corpAddress.equals("")) {
			this.corpAddress = corpArea.concat(AddrSplitStr).concat(corpAddress);
		} else {
			this.corpAddress = "";
		}
	}

	public void setCorpAddress(String corpAddress) {
		this.corpAddress = corpAddress;
	}

	public String getCorpTelArea() {
		return corpTelArea;
	}

	public void setCorpTelArea(String corpTelArea) {
		this.corpTelArea = corpTelArea;
	}

	public String getCorpTel() {
		return corpTel;
	}

	public void setCorpTel(String corpTel) {
		this.corpTel = corpTel;
	}

	public String getCorpTelExten() {
		return corpTelExten;
	}

	public void setCorpTelExten(String corpTelExten) {
		this.corpTelExten = corpTelExten;
	}

	public String getCorpClass() {
		return corpClass;
	}

	public void setCorpClass(String corpClass) {
		this.corpClass = corpClass;
	}

	public String getCorpType() {
		return corpType;
	}

	public void setCorpType(String corpType) {
		this.corpType = corpType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getOnBoardDate() {
		return onBoardDate;
	}

	public void setOnBoardDate(String onBoardDate) {
		this.onBoardDate = onBoardDate;
	}

	public String getYearlyIncome() {
		return yearlyIncome;
	}

	public void setYearlyIncome(String yearlyIncome) {
		this.yearlyIncome = yearlyIncome;
	}

	public String getEstateType() {
		return estateType;
	}

	public void setEstateType(String estateType) {
		this.estateType = estateType;
	}

	public String getpApyAmount() {
		return pApyAmount;
	}

	public void setpApyAmount(String pApyAmount) {
		this.pApyAmount = pApyAmount;
	}

	public String getMobileTel() {
		return mobileTel;
	}

	public void setMobileTel(String mobileTel) {
		this.mobileTel = mobileTel;
	}

	public String getpPeriod() {
		return pPeriod;
	}

	public void setpPeriod(String pPeriod) {
		this.pPeriod = pPeriod;
	}

	public String getpPurpose() {
		return pPurpose;
	}

	public void setpPurpose(String pPurpose) {
		this.pPurpose = pPurpose;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getPrjCode() {
		return prjCode;
	}

	public void setPrjCode(String prjCode) {
		this.prjCode = prjCode;
	}

	public String getCaseNoEst() {
		return caseNoEst;
	}

	public void setCaseNoEst(String caseNoEst) {
		this.caseNoEst = caseNoEst;
	}

	public String getIdno() {
		return idno;
	}

	public void setIdno(String idno) {
		this.idno = idno;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getContactMe() {
		return contactMe;
	}

	public void setContactMe(String contactMe) {
		this.contactMe = contactMe;
	}

	public String getpPurposeName() {
		return pPurposeName;
	}

	public void setpPurposeName(String pPurposeName) {
		this.pPurposeName = pPurposeName;
	}

	public String getCaseStatus() {
		return caseStatus;
	}

	public void setCaseStatus(String caseStatus) {
		this.caseStatus = caseStatus;
	}

	public Boolean getIsBreakPoint() {
		return isBreakPoint;
	}

	public void setIsBreakPoint(Boolean isBreakPoint) {
		this.isBreakPoint = isBreakPoint;
	}

	public String getMBCCheckCode() {
		return MBCCheckCode;
	}

	public void setMBCCheckCode(String mBCCheckCode) {
		MBCCheckCode = mBCCheckCode;
	}

	public String getMBCMessage() {
		return MBCMessage;
	}

	public void setMBCMessage(String mBCMessage) {
		MBCMessage = mBCMessage;
	}

	public String getChtAgreeTime() {
		return ChtAgreeTime;
	}

	public void setChtAgreeTime(String chtAgreeTime) {
		ChtAgreeTime = chtAgreeTime;
	}

	public String getUniqType() {
		return uniqType;
	}

	public void setUniqType(String uniqType) {
		this.uniqType = uniqType;
	}

	public String getHasAum() {
		return HasAum;
	}

	public String getUpdateAum() {
		return UpdateAum;
	}

	public void setHasAum(String hasAum) {
		HasAum = hasAum;
	}

	public void setUpdateAum(String updateAum) {
		UpdateAum = updateAum;
	}

	public String getAUM_UUID() {
		return AUM_UUID;
	}

	public void setAUM_UUID(String aUM_UUID) {
		AUM_UUID = aUM_UUID;
	}

	public JSONObject getChtCreditRating() {
		return ChtCreditRating;
	}

	public void setChtCreditRating(JSONObject chtCreditRating) {
		ChtCreditRating = chtCreditRating;
	}

	public String getAddressCode() {
		return addressCode;
	}

	public void setAddressCode(String addressCode) {
		this.addressCode = addressCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String area, String address) {
		this.address = area.concat(AddrSplitStr).concat(address);
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelArea() {
		return telArea;
	}

	public void setTelArea(String telArea) {
		this.telArea = telArea;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getUserType() {
		return UserType;
	}

	public void setUserType(String userType) {
		UserType = userType;
	}
}
