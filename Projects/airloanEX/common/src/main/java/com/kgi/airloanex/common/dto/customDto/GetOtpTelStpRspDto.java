package com.kgi.airloanex.common.dto.customDto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetOtpTelStpRspDto {
    /* 
     * (1)	CHECK_CODE：
     * A.	檢核資料錯誤(空白代表沒有錯誤)
     * B.	如果有多個錯誤用『;』區隔,例如01;02
     * C.	『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
     * D.	錯誤代碼對照表:
     * 代碼  說明
     * ============================
     * 01	   客戶ID格式錯誤
     * 02	   生日格式錯誤
     * 03	   新戶(查無資料)
     * 04	   ID與生日不符
     * 99	  非預期性系統異常
     */
    private String CHECK_CODE = ""; // "0":驗證失敗，"":驗證成功(舊戶)，"10":新戶

    /*
     * (2)	ERR_MSG：
     * A.	當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤)
     */
    private String ERR_MSG = "";

    /*
     * (3)	BANK_BIRTHDAY：生日
     */
    private String BANK_BIRTHDAY = "";

    /*
     * (4)	BIRTHDAY_SOURCE：生日客戶類型來源 1:信用卡, 2:靈活卡, 3:存款, 4:貸款
     */
    private String BIRTHDAY_SOURCE = "";

    /*
     * (5)	OTP_DATA：
     * A.	OTP明細
     * 欄位名稱			欄位說明
     * =======================================================
     * DATA_TYPE		客戶類型(1:信用卡, 2:靈活卡, 3:存款, 4:貸款)
     * OTP_TEL	電話號碼
     */
    private List<GetOtpTelStpRspDtoOtpData> OTP_DATA = new ArrayList<>();
}
