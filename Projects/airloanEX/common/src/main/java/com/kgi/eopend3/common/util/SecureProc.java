package com.kgi.eopend3.common.util;

public class SecureProc {

	public static String hiddenUserData(String str, int beginIndex, int length) {
		int endIndex = beginIndex + length;
		String newStr = "";
		if (str.length() >= endIndex && beginIndex >= 0) {
			String star = "";
			for (int i = 0; i < length; i++) {
				star += "*";
			}
			newStr = str.substring(0, beginIndex) + star + str.substring(endIndex, str.length());
		}
		// logger.info("hidden str="+newStr);
		return newStr;
	}
	
	/**
	 * 中文姓名隱碼
	 * */
	public static String hiddenChtName(String name) {
		
		if (name == null || name.length() <= 1) {
			return name ;
		}
		
		// 二碼隱1碼
		if (name.length() == 2) {
			return hiddenUserData(name, 1, 1) ;
		} else { // 三碼以上隱中間
			return hiddenUserData(name, 1, name.length()-2) ;
		}
	}

	/**
	 * Email 隱碼
	 * */
	public static String hiddenEmail(String email) {
		
		if (email == null || email.indexOf("@") <= 1) {
			return email ;
		}
		
		String brforeAt = email.substring(0, email.indexOf("@")) ;
		String restpart = email.substring(email.indexOf("@")) ;
		
		
		// 二碼隱1碼
		int len = brforeAt.length(); 
		if (len == 2) {
			return hiddenUserData(brforeAt, 1, 1) + restpart ;
		} else if (len >= 9) { // 九碼以上前後各顯示3碼
			return hiddenUserData(brforeAt, 3, len-6) + restpart ;
		} else if (len >= 6) { // 六碼以上前後各顯示2碼
			return hiddenUserData(brforeAt, 2, len-4) + restpart ;
		} else { // 三碼以上隱中間
			return hiddenUserData(brforeAt, 1, brforeAt.length()-2) + restpart ;
		}
	}

	public static String substring(String str, int beginIndex, int endIndex) {
		if (endIndex < beginIndex) {
			return str;
		}

		if (str.length() >= (endIndex - beginIndex)) {
			return str.substring(beginIndex, endIndex);
		} else {
			return str;
		}
	}


	public static void main(String argc[]) {
		System.out.println(hiddenChtName("中"));
		System.out.println(hiddenChtName("中昌"));
		System.out.println(hiddenChtName("中昌火"));
		System.out.println(hiddenChtName("中昌火竹"));
		System.out.println(hiddenChtName("a"));
		System.out.println(hiddenChtName("ab"));
		System.out.println(hiddenChtName("abc"));
		System.out.println(hiddenChtName("abcd"));
		
		System.out.println(hiddenEmail("a@a.a.a"));
		System.out.println(hiddenEmail("ab@a.a.a"));
		System.out.println(hiddenEmail("abc@a.a.a"));
		System.out.println(hiddenEmail("abcd@a.a.a"));
		System.out.println(hiddenEmail("abcde@a.a.a"));
		System.out.println(hiddenEmail("abcdef@a.a.a"));
		System.out.println(hiddenEmail("abcdefghi@a.a.a"));
	}

}
