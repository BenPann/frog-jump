package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CaseDoc {
    private String CaseNo;
	private String idno;
	private String PdfContent;
	private String Image;
}