package com.kgi.airloanex.common.dto.customDto;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class IdentityVerify {

    private String uniqId;

    // PCode2566
    private String pcode2566CheckCode = "";    
    private String pcode2566Message = "";

    private String verifyType = ""; // PCode2566/eDDA
    private String verifyStatus = "";
    
    private Integer errorCount;
    private Integer ncccErrorCount;
    private Integer pcode2566ErrorCount;
    
    private String errorMsg = "";

    private String phone = "";
    private String bankId = "";
    private String accountNo = "";

    private String cardNo = "";
    private String cardTime = "";
    private String myCard = "N";

}