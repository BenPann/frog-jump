package com.kgi.airloanex.common.dto.customDto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetKycDebtInfoRspDto {
    /** 
     * (1)	CHECK_CODE：
     * A.	檢核資料錯誤(空白代表沒有錯誤)
     * B.	如果有多個錯誤用『;』區隔,例如01;02
     * C.	『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
     * D.	錯誤代碼對照表:
     * 代碼  說明
     * ============================
     * 01	   案件編號格式錯誤
     * 02	   案件編號不存在
     * 03	   產品別格式錯誤
     * 04	   是否為數位案件格式錯誤
     * 05	   是否為秒貸案件格式錯誤
     * 06	   聯徵日格式錯誤
     * 99	  非預期性系統異常 
     */
    private String CHECK_CODE;

    /** 
     * (2)	ERR_MSG：
     * 當系統發生非預期性異常時，CHECK_CODE=99，此參數存放錯誤說明(空白代表沒有錯誤) 
     */
    private String ERR_MSG;

    /** 
     * (3)	DEBT_INFO：
     * A.	負債資訊
     * 欄位名稱					欄位說明
     * =======================================================
     * MORTAGAGE_FLAG				房貸註記(1.本行2.他行3.皆有)
     * MORTAGAGE_AMT				房貸餘額
     * MORTAGAGE_PAY    			房貸每月應繳金額
     * CAR_LOAN_FLAG				車貸註記(1.本行2.他行3.皆有)
     * CAR_LOAN_AMT				車貸餘額
     * CAR_LOAN_PAY    			車貸每月應繳金額
     * PLOAN_FLAG					信貸註記(1.本行2.他行3.皆有)
     * PLOAN_AMT					信貸餘額
     * PLOAN_PAY					信貸每月應繳金額
     * CREDIT_CARD_FLAG	信用卡註記(1.本行2.他行3.皆有)
     * CREDIT_CARD_AMT			信用卡餘額
     * CREDIT_CARD_PAY_KIND		信用卡付款方式(1.全清2.循環)
     * CREDIT_CARD_CYCLE_AMT		信用卡循環金額
     * CASH_CARD_FLAG	現金卡註記(1.本行2.他行3.皆有)
     * CASH_CARD_AMT	現金卡動用餘額 
     */
    private GetKycDebtInfoRspDtoDebtInfo DEBT_INFO;
}
