package com.kgi.airloanex.common.dto.view;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * 
 * @see CreditCardProduct.md
 */
@Getter
@Setter
public class CreditCardProductView {
    
    private String productId;
}
