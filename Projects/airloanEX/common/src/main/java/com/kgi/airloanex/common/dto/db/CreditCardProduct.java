package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreditCardProduct {
    private int CardSerial;
	private String ProductId;
	private String CardType;
	private String CardTitle;
	private String CardContent;
	private String CardImage;
	private String StartDate;
	private String EndDate;
	private String CardStatus;
	private String CreateUser;
	private String CreateTime;
	private String UpdateUser;
	private String UpdateTime;
	private String ApproveUser;
	private String ApproveTime;
}
