package com.kgi.airloanex.common.dto.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiLog {

    private int Serial;
    private String ApiName;
    private String Request;
    private String Response;
    private String StartTime;
    private String EndTime;
    private String UTime;
}
