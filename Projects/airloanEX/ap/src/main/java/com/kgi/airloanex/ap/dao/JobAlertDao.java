package com.kgi.airloanex.ap.dao;

import javax.sql.DataSource;

import com.kgi.airloanex.common.dto.db.JobAlert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class JobAlertDao {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    DataSource dataSource;

    public static final String NAME = "JobAlert" ;

    protected NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(this.dataSource);
    }

    public int Create(JobAlert fullItem) {
        String sql = "INSERT INTO " + NAME 
                + " ( AlertType, Clazz, UniqId, BillDepart, Phone, MsgContent, Content001, Content002, CreateTime, OrderTime, SendTime, SendStatus)" 
        + " VALUES ( :AlertType,:Clazz,:UniqId,:BillDepart,:Phone,:MsgContent,:Content001,:Content002,  getdate(),:OrderTime,:SendTime,:SendStatus)";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }    
}
