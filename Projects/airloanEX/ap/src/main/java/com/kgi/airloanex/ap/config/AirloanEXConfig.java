package com.kgi.airloanex.ap.config;

import java.nio.charset.Charset;

import com.kgi.airloanex.common.PlContractConst;
import com.kgi.eopend3.ap.config.GlobalConfig;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.common.util.crypt.FucoAESUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * 存取 AIRLOANDB 的 TABLE Config
 */
@Service
public class AirloanEXConfig {

    @Autowired
    private GlobalConfig config;

    public String makePazzd(String pazzd) {
        FucoAESUtil instance = FucoAESUtil.getInstance(config.AbbeyRye, config.RayakRoe);

        try {
            return new String(instance.decrypt(pazzd), Charset.forName("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public boolean isPDF_Export() {
        return "Y".equals(PDF_Export());
    }

    public String findContract_TemplateFilePath(String productId) {
        if (PlContractConst.isPL(productId)) {
            return PDF_Contract_PL_TemplateFilePath();
        } else if (PlContractConst.isRPL(productId)) {
            return PDF_Contract_RPL_TemplateFilePath();
        } else if (PlContractConst.isCASHCARD(productId)) {
            return PDF_Contract_Cashcard_TemplateFilePath();
        } else {
            throw new RuntimeException("airloanEX.PDF.Contract.**.TemplateFilePath not found.");
        }
    }

    public String findContract_VerNo(String productId) {
        if (PlContractConst.isPL(productId)) {
            return PDF_Contract_PL_VerNo();
        } else if (PlContractConst.isRPL(productId)) {
            return PDF_Contract_RPL_VerNo();
        } else if (PlContractConst.isCASHCARD(productId)) {
            return PDF_Contract_Cashcard_VerNo();
        } else {
            System.err.println("airloanEX.PDF.Contract.**.VerNo not found.");
            return "";
        }
    }

    public String findContract_VerDate(String productId) {
        if (PlContractConst.isPL(productId)) {
            return PDF_Contract_PL_VerDate();
        } else if (PlContractConst.isRPL(productId)) {
            return PDF_Contract_RPL_VerDate();
        } else if (PlContractConst.isCASHCARD(productId)) {
            return PDF_Contract_Cashcard_VerDate();
        } else {
            System.err.println("airloanEX.PDF.Contract.**.VerDate not found.");
            return "";
        }
    }

    public String findContract_Footer(String productId) {
        if (PlContractConst.isPL(productId)) {
            return PDF_Contract_PL_Footer();
        } else if (PlContractConst.isRPL(productId)) {
            return PDF_Contract_RPL_Footer();
        } else if (PlContractConst.isCASHCARD(productId)) {
            return PDF_Contract_Cashcard_Footer();
        } else {
            System.err.println("airloanEX.PDF.Contract.**.Footer not found.");
            return "";
        }
    }

    @Autowired
    ConfigDao configDao;

    public static final String PREFIX_airloanEX = "airloanEX";

    private String airloanEXConfigValue(String keyName, String defaultValue) {
        return configDao.ReadConfigValue(PREFIX_airloanEX + "." + keyName, defaultValue);
    }

    /** # */
    public String upload_image_alpha() {
        return airloanEXConfigValue("upload.image.alpha", "0.4f");
    }

    /** # aps server */
    public String ALN_ServerHost() {
        return airloanEXConfigValue("ALN.ServerHost", ""); // fix Improper Restriction of Stored XXE Ref -
                                                           // 新興科技科_STP_立約_白箱報告_0831.pdf
    }

    /** # 額度利率體驗PL計算用 API 呼叫用的HOST */
    public String ALN_ServerHost_EXP() {
        return airloanEXConfigValue("ALN.ServerHost.EXP", "");
    }

    /** # dgt server 立約用 GET_DGT_CONST, GET_DGT_UPDATE */
    public String APS_ServerHost() {
        return airloanEXConfigValue("APS.ServerHost", ""); // fix Improper Restriction of Stored XXE Ref -
                                                           // 新興科技科_STP_立約_白箱報告_0831.pdf
    }

    // /** #共銷同意註記 WS */
    // public String AutInfo_Url_ip() {
    //     return airloanEXConfigValue("AutInfo.Url.ip", ""); // fix Improper Restriction of Stored XXE Ref -
    //                                                        // 新興科技科_STP_立約_白箱報告_0831.pdf
    // }

    /** #共銷同意註記 WS */
    public String AutInfo_Url() {
        return airloanEXConfigValue("AutInfo.Url", "172.17.131.11:80"); // fix Improper Restriction of Stored XXE Ref -
                                                                        // 新興科技科_STP_立約_白箱報告_0831.pdf
    }

    // /** #MAIL */
    // public String Mail_Host() {
    //     return airloanEXConfigValue("Mail.Host", "mail.kgibank.com");
    // }

    // /** #MAIL */
    // public String ContactMe_Mail_To() {
    //     return airloanEXConfigValue("ContactMe.Mail.To", "Ben.Pann@kgi.com");
    // }

    // /** #MAIL */
    // public String ContactMe_Mail_From() {
    //     return airloanEXConfigValue("ContactMe.Mail.From", "ebanking@kgi.com");
    // }

    /** #FTP */
    public String Img_FTPServer_Address() {
        return airloanEXConfigValue("Img.FTPServer.Address", ""); // fix Improper Restriction of Stored XXE Ref -
                                                                  // 新興科技科_STP_立約_白箱報告_0831.pdf
    }

    /** #FTP */
    public String Img_FTPServer_Path() {
        return airloanEXConfigValue("Img.FTPServer.Path", "PoolingDir");
    }

    /** #FTP */
    public String Img_FTPServer_Userid() {
        return airloanEXConfigValue("Img.FTPServer.Userid", "phoneapp");
    }

    /** #FTP */
    public String Img_FTPServer_PWD() {
        return airloanEXConfigValue("Img.FTPServer.PWD", ""); // fix Improper Restriction of Stored XXE Ref -
                                                              // 新興科技科_STP_立約_白箱報告_0831.pdf
    }

    /** #MailHunter設定 */
    public String MailHunter_FTPServer_Address() {
        return airloanEXConfigValue("MailHunter.FTPServer.Address", ""); // fix Improper Restriction of Stored XXE Ref -
                                                                         // 新興科技科_STP_立約_白箱報告_0831.pdf
    }

    /** #MailHunter設定 */
    public String MailHunter_FTPServer_Userid() {
        return airloanEXConfigValue("MailHunter.FTPServer.Userid", "mhu_ftp");
    }

    /** #MailHunter設定 #MailHunterFTPServerPAZZD=1qaz@WSX */
    public String MailHunter_FTPServer_PAZZD() {
        String pazzd = airloanEXConfigValue("MailHunter.FTPServer.PAZZD", ""); // fix Use of Hard coded Cryptographic
                                                                               // Key - 新興科技科_STP_立約_白箱報告_0825_v2.pdf
        pazzd = this.makePazzd(pazzd);
        return pazzd;
    }

    // /** #MailHunter設定 */
    // public String MailHunter_Url() {
    //     return airloanEXConfigValue("MailHunter.Url", ""); // fix Improper Restriction of Stored XXE Ref -
    //                                                        // 新興科技科_STP_立約_白箱報告_0831.pdf
    // }

    // /** #斷點通知信天數(用半形逗號分隔) */
    // public String BreakPointRemainDays() {
    //     return airloanEXConfigValue("BreakPointRemainDays", "7,14,21");
    // }

    /** #圖片浮水印字 */
    public String IMG_WATER_FONT() {
        return airloanEXConfigValue("IMG.WATER.FONT", "限凱基商業銀行申辦使用");
    }

    /** #圖片浮水印字 KYC */
    public String IMG_WATER_FONT_KYC() {
        return airloanEXConfigValue("IMG.WATER.FONT.KYC", "聯徵Z07查詢");
    }

    /** #NCCC設定 */
    public String NCCC_MerchantID() {
        return airloanEXConfigValue("NCCC.MerchantID", "1300816679");
    }

    /** #NCCC設定 */
    public String NCCC_TID() {
        return airloanEXConfigValue("NCCC.TID", "A2100352");
    }

    // /** #NCCC設定 */
    // public String NCCC_UserID() {
    //     return airloanEXConfigValue("NCCC.UserID", "W122217457");
    // }

    /** #NCCC設定 */
    public String NCCC_TransMode() {
        return airloanEXConfigValue("NCCC.TransMode", "0");
    }

    /** #NCCC設定 */
    public String NCCC_TransAmount() {
        return airloanEXConfigValue("NCCC.TransAmount", "0");
    }

    /** #ESB設定 */
    public String ESB_ClientId() {
        return airloanEXConfigValue("ESB.ClientId", "EOPSYS");
    }

    /** #ESB設定 #ESBClientPAZZD=eOpenSystem@2019 */
    public String ESB_ClientPAZZD() {
        String pazzd = airloanEXConfigValue("ESB.ClientPAZZD", ""); // fix Use of Hard coded Cryptographic Key -
                                                                    // 新興科技科_STP_立約_白箱報告_0825_v2.pdf
        pazzd = this.makePazzd(pazzd);
        return pazzd;
    }

    // /** #ESB設定 */
    // public String ESB_HostName() {
    //     return airloanEXConfigValue("ESB.HostName", ""); // fix Improper Restriction of Stored XXE Ref -
    //                                                      // 新興科技科_STP_立約_白箱報告_0831.pdf
    // }

    /** #ESB設定 */
    public String ESB_Channel() {
        return airloanEXConfigValue("ESB.Channel", "ESBMQSVR.EOPSYS");
    }

    // /** #ESB設定 */
    // public String ESB_Port() {
    //     return airloanEXConfigValue("ESB.Port", "1421");
    // }

    // /** #ESB設定 */
    // public String ESB_QueueManagerName() {
    //     return airloanEXConfigValue("ESB.QueueManagerName", " ");
    // }

    // /** #ESB設定 */
    // public String ESB_QueueSendName() {
    //     return airloanEXConfigValue("ESB.QueueSendName", "MQ.EA");
    // }

    /** #ESB設定 */
    public String ESB_QueueReceiveName() {
        return airloanEXConfigValue("ESB.QueueReceiveName", "EA.EOPSYS");
    }

    /** #ALNWEB設定 */
    public String ALN_ClientId() {
        return airloanEXConfigValue("ALN.ClientId", "ALNWEB");
    }

    /** #ALNWEB設定 #ALNClientPAZZD=AirLoan8&Test */
    public String ALN_ClientPAZZD() {
        String pazzd = airloanEXConfigValue("ALN.ClientPAZZD", ""); // fix Use of Hard coded Cryptographic Key -
                                                                    // 新興科技科_STP_立約_白箱報告_0825_v2.pdf
        pazzd = this.makePazzd(pazzd);
        return pazzd;
    }

    /** #ALNWEB設定 */
    public String ALN_HostName() {
        return airloanEXConfigValue("ALN.HostName", ""); // fix Improper Restriction of Stored XXE Ref -
                                                         // 新興科技科_STP_立約_白箱報告_0831.pdf
    }

    /** #ALNWEB設定 */
    public String ALN_Channel() {
        return airloanEXConfigValue("ALN.Channel", "ESBMQSVR.MB");
    }

    /** #ALNWEB設定 */
    public String ALN_Port() {
        return airloanEXConfigValue("ALN.Port", "1421");
    }

    /** #ALNWEB設定 */
    public String ALN_QueueManagerName() {
        return airloanEXConfigValue("ALN.QueueManagerName", " ");
    }

    /** #ALNWEB設定 */
    public String ALN_QueueSendName() {
        return airloanEXConfigValue("ALN.QueueSendName", "MQ.EA");
    }

    /** #ALNWEB設定 */
    public String ALN_QueueReceiveName() {
        return airloanEXConfigValue("ALN.QueueReceiveName", "EA.ALNWEB");
    }

    /** #OCR */
    public String OCR_ServerHost() {
        return airloanEXConfigValue("OCR.ServerHost", ""); // fix Improper Restriction of Stored XXE Ref -
                                                           // 新興科技科_STP_立約_白箱報告_0831.pdf
    }

    // /** #額度利率體驗天數 */
    // public String chkQuotalDays() {
    //     return airloanEXConfigValue("chkQuotalDays", "7");
    // }

    // /** #契約書是否直接寄送EMAIL開關(1:開啟直接寄送至客戶email 0:關閉) */
    // public String SendContractEmailFlag() {
    //     return airloanEXConfigValue("SendContractEmailFlag", "0");
    // }

    /** #補件上傳完成之後 立約上傳完成之後 是否刪除圖檔 (1:刪除,0:不刪除) 預設為不刪除 */
    public String AdditionalPhotoDelete() {
        return airloanEXConfigValue("AdditionalPhotoDelete", "0");
    }

    // /** #立約完成之後 是否更新APS的立約狀態 (1:更新,0:不更新) 預設為更新 */
    // public String UpdateApsContractStatus() {
    //     return airloanEXConfigValue("UpdateApsContractStatus", "0");
    // }

    // /** #線下件補件流程 是否刪除圖檔 (1:刪除,0:不刪除) 預設為刪除 */
    // public String OfflineAddPhotoDelete() {
    //     return airloanEXConfigValue("OfflineAddPhotoDelete", "0");
    // }

    /** # */
    public String PCODE2566_TMNL_TYPE() {
        return airloanEXConfigValue("PCODE2566.TMNL_TYPE", "6614");
    }

    /** # */
    public String PCODE2566_TMNL_ID() {
        return airloanEXConfigValue("PCODE2566.TMNL_ID", "EOPS");
    }

    // /** # */
    // public String SecuritiesHost() {
    //     return airloanEXConfigValue("SecuritiesHost", ""); // fix Improper Restriction of Stored XXE Ref -
    //                                                        // 新興科技科_STP_立約_白箱報告_0831.pdf
    // }

    /** # */
    public String PCode2566_KGIErrorMsg() {
        return airloanEXConfigValue("PCode2566.KGIErrorMsg", "資料有誤，請聯繫客服");
    }

    // /** # */
    // public String EopenImageServerHost() {
    //     return airloanEXConfigValue("EopenImageServerHost", ""); // fix Improper Restriction of Stored XXE Ref -
    //                                                              // 新興科技科_STP_立約_白箱報告_0831.pdf
    // }

    // /** # */
    // public String PortalUrl() {
    //     return airloanEXConfigValue("PortalUrl", ""); // fix Improper Restriction of Stored XXE Ref -
    //                                                   // 新興科技科_STP_立約_白箱報告_0831.pdf
    // }

    /** #Validation Server */
    public String ValidationServerUrl() {
        return airloanEXConfigValue("ValidationServerUrl", "http://localhost:8090/Validation/validate/");
    }

    /** #for OTP */
    public String OTP_OpID() {
        return airloanEXConfigValue("OTP.OpID", "AP08");
    }

    /** #for OTP */
    public String OTP_BillDep() {
        return airloanEXConfigValue("OTP.BillDep", "6660");
    }

    /** #Nccc驗證錯誤上限 */
    public String NcccTotalCount() {
        return airloanEXConfigValue("NcccTotalCount", "6");
    }

    /** #Pcode2566驗證錯誤上限 */
    public String Pcode2566TotalCount() {
        return airloanEXConfigValue("Pcode2566TotalCount", "6");
    }

    // /** #可以往後預約幾個月(不含當月) */
    // public String BookingDay() {
    //     return airloanEXConfigValue("BookingDay", "30");
    // }

    // /** #可以往後預約幾個月(不含當月) */
    // public String BookingMonth() {
    //     return airloanEXConfigValue("BookingMonth", "0");
    // }

    // /** #AUM 徵審系統 */
    // public String AUM_Host() {
    //     return airloanEXConfigValue("AUM.Host", ""); // fix Improper Restriction of Stored XXE Ref -
    //                                                  // 新興科技科_STP_立約_白箱報告_0831.pdf
    // }

    // /** #AUM 徵審系統 */
    // public String AUM_Cmd_AddCLAUMInfo() {
    //     return airloanEXConfigValue("AUM.Cmd_AddCLAUMInfo", "KGI/ADD_CL_AUMINFO");
    // }

    // /** #代收付平台 */
    // public String PayerSysId() {
    //     return airloanEXConfigValue("PayerSysId", "PAY");
    // }

    // /** #代收付平台 */
    // public String PayerApKey() {
    //     return airloanEXConfigValue("PayerApKey", ""); // fix Use of Hard coded Cryptographic Key -
    //                                                    // 新興科技科_STP_立約_白箱報告_0825_v2.pdf
    // }

    // /** #代收付平台 */
    // public String PayerApplyCode() {
    //     return airloanEXConfigValue("PayerApplyCode", "ACH02");
    // }

    // /** #代收付平台 */
    // public String PayerTransType() {
    //     return airloanEXConfigValue("PayerTransType", "704");
    // }

    // /** #代收付平台 */
    // public String PayerLaunchUniformId() {
    //     return airloanEXConfigValue("PayerLaunchUniformId", "03434016");
    // }

    // /** #代收付平台 */
    // public String PayerActionType() {
    //     return airloanEXConfigValue("PayerActionType", "A");
    // }

    // /** #代收付平台 */
    // public String PayerTransBankCode() {
    //     return airloanEXConfigValue("PayerTransBankCode", "8090407");
    // }

    // /** #代收付平台 */
    // public String PayerAccountType() {
    //     return airloanEXConfigValue("PayerAccountType", "1");
    // }

    // /** #測試用收件信箱 */
    // public String FAKE_EMAIL() {
    //     return airloanEXConfigValue("FAKE_EMAIL", "vicky.tsai@kgi.com");
    // }

    /** # */
    public String CMBSendMsg_SOAP_URL() {
        return airloanEXConfigValue("CMBSendMsg.SOAP.URL", ""); // fix Improper Restriction of Stored XXE Ref -
                                                                // 新興科技科_STP_立約_白箱報告_0831.pdf
    }

    /** # 浮水印字型路徑 */
    public String PDF_FONT_WATER_TTF() {
        return airloanEXConfigValue("PDF.FONT_WATER_TTF", "C:/Windows/Fonts/mingliu.ttc,0");
    }

    /** # 立約 html2pdf */
    public String PDF_PdfServerUrl() {
        return airloanEXConfigValue("PDF.PdfServerUrl", ""); // fix Improper Restriction of Stored XXE Ref -
                                                             // 新興科技科_STP_立約_白箱報告_0831.pdf
    }

    /** # 現金卡立約書樣板路徑 */
    public String PDF_Contract_Cashcard_TemplateFilePath() {
        return airloanEXConfigValue("PDF.Contract.Cashcard.TemplateFilePath", "/html/contract_CashCard.html");
    }

    /** # 現金卡立約書樣板版本VerNo */
    public String PDF_Contract_Cashcard_VerNo() {
        return airloanEXConfigValue("PDF.Contract.Cashcard.VerNo", "2019/05");
    }

    /** # 現金卡立約書樣板版本VerDate */
    public String PDF_Contract_Cashcard_VerDate() {
        return airloanEXConfigValue("PDF.Contract.Cashcard.VerDate", "20190501");
    }

    /** # 現金卡立約書樣板版本Footer */
    public String PDF_Contract_Cashcard_Footer() {
        return airloanEXConfigValue("PDF.Contract.Cashcard.Footer", "");
    }

    /** # 小額信貸立約書MBC驗身樣板路徑 */
    public String PDF_Contract_RPL_MBC_TemplateFilePath() {
        return airloanEXConfigValue("PDF.Contract.RPL.MBC.TemplateFilePath", "/html/contract_E_Loan_MBC.html");
    }

    /** # 小額信貸立約書MBC驗身樣板版本號VerNo */
    public String PDF_Contract_RPL_MBC_VerNo() {
        return airloanEXConfigValue("PDF.Contract.RPL.MBC.VerNo", "2019/09");
    }

    /** # 小額信貸立約書MBC驗身樣板版本號VerDate */
    public String PDF_Contract_RPL_MBC_VerDate() {
        return airloanEXConfigValue("PDF.Contract.RPL.MBC.VerDate", "20190901");
    }

    /** # 小額信貸立約書MBC驗身樣板版本號Footer */
    public String PDF_Contract_RPL_MBC_Footer() {
        return airloanEXConfigValue("PDF.Contract.RPL.MBC.Footer", "");
    }

    /** # 小額信貸立約書樣板路徑 */
    public String PDF_Contract_RPL_TemplateFilePath() {
        return airloanEXConfigValue("PDF.Contract.RPL.TemplateFilePath", "/html/contract_E_Loan.html");
    }

    /** # 小額信貸立約書樣板版本VerNo */
    public String PDF_Contract_RPL_VerNo() {
        return airloanEXConfigValue("PDF.Contract.RPL.VerNo", "2019/09");
    }

    /** # 小額信貸立約書樣板版本VerDate */
    public String PDF_Contract_RPL_VerDate() {
        return airloanEXConfigValue("PDF.Contract.RPL.VerDate", "20190901");
    }

    /** # 小額信貸立約書樣板版本Footer */
    public String PDF_Contract_RPL_Footer() {
        return airloanEXConfigValue("PDF.Contract.RPL.Footer", "RPL002-1 (Web立約適用)(2019/09)");
    }

    /** # 一般貸款立約書MBC驗身樣板路徑 */
    public String PDF_Contract_PL_MBC_TemplateFilePath() {
        return airloanEXConfigValue("PDF.Contract.PL.MBC.TemplateFilePath", "/html/contract_onlineLoan_MBC.html");
    }

    /** # 一般貸款立約書MBC驗身樣板版本號VerNo */
    public String PDF_Contract_PL_MBC_VerNo() {
        return airloanEXConfigValue("PDF.Contract.PL.MBC.VerNo", "2019/09");
    }

    /** # 一般貸款立約書MBC驗身樣板版本號VerDate */
    public String PDF_Contract_PL_MBC_VerDate() {
        return airloanEXConfigValue("PDF.Contract.PL.MBC.VerDate", "20190901");
    }

    /** # 一般貸款立約書MBC驗身樣板版本號Footer */
    public String PDF_Contract_PL_MBC_Footer() {
        return airloanEXConfigValue("PDF.Contract.PL.MBC.Footer", "");
    }

    /** # 一般貸款立約書樣板路徑 */
    public String PDF_Contract_PL_TemplateFilePath() {
        return airloanEXConfigValue("PDF.Contract.PL.TemplateFilePath", "/html/contract_onlineLoan.html");
    }

    /** # 一般貸款立約書樣板路徑 ACH 申請書 */
    public String PDF_Contract_PL_ACH_TemplateFilePath() {
        return airloanEXConfigValue("PDF.Contract.PL.ACH.TemplateFilePath", "/html/contract_onlineLoan_ach.html");
    }

    /** # 一般貸款立約書樣板版本VerNo */
    public String PDF_Contract_PL_VerNo() {
        return airloanEXConfigValue("PDF.Contract.PL.VerNo", "2019/09");
    }

    /** # 一般貸款立約書樣板版本VerDate */
    public String PDF_Contract_PL_VerDate() {
        return airloanEXConfigValue("PDF.Contract.PL.VerDate", "20190901");
    }

    /** # 一般貸款立約書樣板版本Footer */
    public String PDF_Contract_PL_Footer() {
        return airloanEXConfigValue("PDF.Contract.PL.Footer", "PL002-1 一般個人信貸(Web 立約版)(2019/09)");
    }

    /** # 信用卡MBC驗身樣板路徑 */
    public String PDF_Credit_MBC_TemplateFilePath() {
        return airloanEXConfigValue("PDF.Credit.MBC.TemplateFilePath", "/html/creditcard/apply_CreditCard_MBC.html");
    }

    /** # 信用卡MBC驗身樣板版本號VerNo */
    public String PDF_Credit_MBC_VerNo() {
        return airloanEXConfigValue("PDF.Credit.MBC.VerNo", "2020/01");
    }

    /** # 信用卡MBC驗身樣板版本號VerDate */
    public String PDF_Credit_MBC_VerDate() {
        return airloanEXConfigValue("PDF.Credit.MBC.VerDate", "20190901");
    }

    /** # 信用卡MBC驗身樣板版本號Footer */
    public String PDF_Credit_MBC_Footer() {
        return airloanEXConfigValue("PDF.Credit.MBC.Footer", "");
    }

    /** # 信用卡樣板路徑 */
    public String PDF_Credit_TemplateFilePath() {
        return airloanEXConfigValue("PDF.Credit.TemplateFilePath", "/html/creditcard/apply_CreditCard.html");
    }

    /** # 信用卡樣板版本號VerNo */
    public String PDF_Credit_VerNo() {
        return airloanEXConfigValue("PDF.Credit.VerNo", "2019/04");
    }

    /** # 信用卡樣板版本號VerDate */
    public String PDF_Credit_VerDate() {
        return airloanEXConfigValue("PDF.Credit.VerDate", "20190901");
    }

    /** # 信用卡樣板版本號Footer */
    public String PDF_Credit_Footer() {
        return airloanEXConfigValue("PDF.Credit.Footer", "");
    }

    /** # 中華電信貸款樣板路徑 */
    public String PDF_PL_Cht_TemplateFilePath() {
        return airloanEXConfigValue("PDF.PL.Cht.TemplateFilePath", "/html/loan_application_cht.html");
    }

    /** # 中華電信貸款樣板版本號VerNo */
    public String PDF_PL_Cht_VerNo() {
        return airloanEXConfigValue("PDF.PL.Cht.VerNo", "2018/12");
    }

    /** # 中華電信貸款樣板版本號VerDate */
    public String PDF_PL_Cht_VerDate() {
        return airloanEXConfigValue("PDF.PL.Cht.VerDate", "20181201");
    }

    /** # 中華電信貸款樣板版本號Footer */
    public String PDF_PL_Cht_Footer() {
        return airloanEXConfigValue("PDF.PL.Cht.Footer", "");
    }

    /** # 貸款MBC驗身樣板路徑 */
    public String PDF_PL_MBC_TemplateFilePath() {
        return airloanEXConfigValue("PDF.PL.MBC.TemplateFilePath", "/html/loan_application_mbc.html");
    }

    /** # 貸款MBC驗身樣板版本號VerNo */
    public String PDF_PL_MBC_VerNo() {
        return airloanEXConfigValue("PDF.PL.MBC.VerNo", "2020/02");
    }

    /** # 貸款MBC驗身樣板版本號VerDate */
    public String PDF_PL_MBC_VerDate() {
        return airloanEXConfigValue("PDF.PL.MBC.VerDate", "20200201");
    }

    /** # 貸款MBC驗身樣板版本號Footer */
    public String PDF_PL_MBC_Footer() {
        return airloanEXConfigValue("PDF.PL.MBC.Footer", "");
    }

    /** # 貸款拒件樣板路徑 */
    public String PDF_PL_Reject_TemplateFilePath() {
        return airloanEXConfigValue("PDF.PL.Reject.TemplateFilePath", "/html/loan_application_reject.html");
    }

    /** # 貸款拒件樣板版本號VerNo */
    public String PDF_PL_Reject_VerNo() {
        return airloanEXConfigValue("PDF.PL.Reject.VerNo", "2020/02");
    }

    /** # 貸款拒件樣板版本號VerDate */
    public String PDF_PL_Reject_VerDate() {
        return airloanEXConfigValue("PDF.PL.Reject.VerDate", "20200201");
    }

    /** # 貸款拒件樣板版本號VerFooter */
    public String PDF_PL_Reject_Footer() {
        return airloanEXConfigValue("PDF.PL.Reject.Footer", "");
    }

    /** # 貸款樣板路徑 */
    public String PDF_PL_TemplateFilePath() {
        return airloanEXConfigValue("PDF.PL.TemplateFilePath", "/html/loan_application.html");
    }

    /** # 貸款樣板版本號VerNo */
    public String PDF_PL_VerNo() {
        return airloanEXConfigValue("PDF.PL.VerNo", "2020/02");
    }

    /** # 貸款樣板版本號VerDate */
    public String PDF_PL_VerDate() {
        return airloanEXConfigValue("PDF.PL.VerDate", "20200201");
    }

    /** # KYC與AML檢核表 樣板路徑 */
    public String PDF_KYC_TemplateFilePath() {
        return airloanEXConfigValue("PDF.KYC.TemplateFilePath", "/html/loan_kyc.html");
    }

    /** # KYC與AML檢核表 樣板版本號VerNo */
    public String PDF_KYC_VerNo() {
        return airloanEXConfigValue("PDF.KYC.VerNo", "2020/02");
    }

    /** # KYC與AML檢核表 樣板版本號VerDate */
    public String PDF_KYC_VerDate() {
        return airloanEXConfigValue("PDF.KYC.VerDate", "20200201");
    }

    /** # 同一關係人表樣板路徑 */
    public String PDF_RelationDegree_TemplateFilePath() {
        return airloanEXConfigValue("PDF.RelationDegree.TemplateFilePath", "/html/loan_relation_degree.html");
    }

    /** # 同一關係人表 樣板版本號VerNo */
    public String PDF_RelationDegree_VerNo() {
        return airloanEXConfigValue("PDF.RelationDegree.VerNo", "2020/02");
    }

    /** # 同一關係人表 樣板版本號VerDate */
    public String PDF_RelationDegree_VerDate() {
        return airloanEXConfigValue("PDF.RelationDegree.VerDate", "20200201");
    }

    /** # 貸款樣板版本號VerFooter */
    public String PDF_PL_Footer() {
        return airloanEXConfigValue("PDF.PL.Footer", "");
    }

    /** # qrcode版本 */
    public String PDF_QRCode_TemplateFilePath() {
        return airloanEXConfigValue("PDF.QRCode.TemplateFilePath", "/html/eopen_qrcode.html");
    }

    /** # dec server */
    public String DEC_ServerHost() {
        return airloanEXConfigValue("DEC.ServerHost", "");
    }

    /** # 申請流程 卡加貸預設信用卡別 */
    public String Apply_Creditcard_ProductId_Default() {
        return airloanEXConfigValue("Apply.Creditcard.ProductId.Default", "519001");
    }

    /** # 申請流程 登入頁 */
    public String Apply_Start() {
        return airloanEXConfigValue("Apply.Start", "");
    }

    /** # 是否匯出檔案至 localhost */
    public String PDF_Export() {
        return airloanEXConfigValue("PDF.Export", "N");
    }

    /** #Socket Timeout Millisec */
    public String SocketTimeoutMillisec() {
        return airloanEXConfigValue("Socket.Timeout.Millisec", "90000");
    }

    /** # 黃頁 /api/company/full-name, /api/company/info */
    public String CRP_ServerHost() {
        return airloanEXConfigValue("CRP.ServerHost", "");
    }

    /** # sms server 報表簡訊平台(SMS) power by Ben */
    public String SMS_ServerHost() {
        return airloanEXConfigValue("SMS.ServerHost", "");
    }

    /** #APIM相關 */
    public String APIM_ServerHost() {
        return airloanEXConfigValue("APIMServerHost", "");
    }
    public String APIM_CrossSell_ApiKey() {
        return airloanEXConfigValue("APIM.CrossSell.ApiKey", "");
    }
    public String APIM_CrossSell_SecretKey() {
        return airloanEXConfigValue("APIM.CrossSell.SecretKey", "");
    }
    public String CrossSellCCUrl() {
        return airloanEXConfigValue("CrossSell.Entry.CC", "");
    }
    public String CrossSellLoanPLUrl() {
        return airloanEXConfigValue("CrossSell.Entry.LOAN.PL", "");
    }
    public String CrossSellLoanRPLUrl() {
        return airloanEXConfigValue("CrossSell.Entry.LOAN.RPL", "");
    }

    /** #智能客服(Chatbot) */
    public String ChatbotUrL() {
        return airloanEXConfigValue("Chatbot.UrL", "");
    }

    /** # sms 簡訊發送 稍後補件 的內容 */
    public String SMS_applyLaterAddon() {
        return airloanEXConfigValue("SMS.applyLaterAddon", "您的線上申請貸款尚未完成，稍後可至 %ApplyStart% 接續完成申請流程。");
    }

    /** # sms 簡訊發送 額度利率體驗傳送方案 的內容 */
    public String SMS_applyCalResult() {
        return airloanEXConfigValue("SMS.applyCalResult", "試算額度%CreditLine%萬元，利率%IntrestRate%%，稍後可至 %ApplyStart% 接續完成申請流程。");
    }

    /** # sms 簡訊發送 額度利率體驗傳送方案 BillDep */
    public String SMS_applyCalResult_BillDep() {
        return airloanEXConfigValue("SMS.applyCalResult.BillDep", "9722");
    }

    /** # sms 簡訊發送 操作逾時 的內容 */
    public String SMS_applySessiontimeout() {
        return airloanEXConfigValue("SMS.applySessiontimeout", "操作逾時");
    }

    public String SMS_on() {
        return airloanEXConfigValue("SMS.on", "Y");
    }

    /** 
     * 調整立約時間,是PL(1)exp_bank本行 時間小於21點前 格式hhmmss: 205959
     */
    public String payDateTime_pl_exp_bank_809() {
        return airloanEXConfigValue("payDateTime.pl.exp_bank_809", "205959");
    }
    /** 
     * 調整立約時間,是PL(1)exp_bank它行 時間小於15點前 格式hhmmss: 205959
     */
    public String payDateTime_pl_exp_bank_other() {
        return airloanEXConfigValue("payDateTime.pl.exp_bank_other", "145959");
    }
    /** 
     * 調整立約時間,是PL(1)pay_bank有代償 時間小於15點前 格式hhmmss: 145959
     * 
     */
    public String payDateTime_pl_pay_bank() {
        return airloanEXConfigValue("payDateTime.pl.pay_bank", "145959");
    }
    /** 
     * 調整立約時間,是RPL(3) 時間小於24點前 格式hhmmss: 235959
     */
    public String payDateTime_rpl() {
        return airloanEXConfigValue("payDateTime.rpl", "235959");
    }
    /** 
     * 調整立約時間,是現金卡(2) 時間小於24點前 格式hhmmss: 235959
     */
    public String payDateTime_cash() {
        return airloanEXConfigValue("payDateTime.cash", "235959");
    }
    /** 
     * 調整立約時間,是熔斷 營業時間檢核點 格式hhmmss: 155959
     */
    public String payDateTime_exceed_flg() {
        return airloanEXConfigValue("payDateTime.exceed_flg", "155959");
    }

    /** # GET_DGTCASE_INFO 嘗試次數 */
    public String loop_GET_DGTCASE_INFO() {
        return airloanEXConfigValue("loop.GET_DGTCASE_INFO", "6");
    }

    /** # GET_DGTCASE_INFO 等待時間 */
    public String wait_GET_DGTCASE_INFO() {
        return airloanEXConfigValue("wait.GET_DGTCASE_INFO", "30000");
    }
}