package com.kgi.eopend3.ap.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

// @Component
@PropertySource(value = {"airloandbConfig"}, encoding = "UTF-8")
public class AirloandbConfig {
    
    @Value("${AML.AMLEnd}")
    public String AML_AMLEnd;

    public AirloandbConfig(ApplicationContext appContext) {
        AutowireCapableBeanFactory factory = appContext.getAutowireCapableBeanFactory();
		factory.autowireBean(this);
    }
}