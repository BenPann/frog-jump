package com.kgi.airloanex.ap.service;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.CaseDataDao;
import com.kgi.airloanex.ap.dao.UserPhotoDao;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.db.CaseData;
import com.kgi.airloanex.common.dto.db.UserPhoto;
import com.kgi.eopend3.ap.dao.DropdownDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.ap.service.APSService;
import com.kgi.eopend3.ap.service.AsyncService;
import com.kgi.eopend3.ap.service.ImageService;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.customDto.BCOCRReq;
import com.kgi.eopend3.common.dto.customDto.BCOCRRes;
import com.kgi.eopend3.common.dto.customDto.IDOCRReq;
import com.kgi.eopend3.common.dto.customDto.IDOCRRes;
import com.kgi.airloanex.common.dto.db.DropdownData;
import com.kgi.eopend3.common.dto.respone.PhotoListResp;
import com.kgi.eopend3.common.dto.respone.UploadPhotoResp;
import com.kgi.eopend3.common.dto.view.DeletPhotoView;
import com.kgi.eopend3.common.dto.view.OCRView;
import com.kgi.eopend3.common.dto.view.UserPhotoView;
import com.kgi.eopend3.common.util.CheckUtil;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.kgi.airloanex.common.PlContractConst.*;

@Service
public class UserPhotoService {

	@Autowired
	private UserPhotoDao userPhotoDao;

	@Autowired
	private AirloanEXConfig globalConfig;

	@Autowired
	APSService apsService;

	@Autowired
	DropdownDao dropdownDao;

	@Autowired
	AsyncService asyncService;

	@Autowired
	private ImageService imageService;

	@Autowired
	private CaseDataDao caseDataDao;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public String uploadPhoto(String reqJson, String uniqId, String idno) {
		try {
			// logger.info("### uploadPhoto=" + reqJson.substring(0,100));
			Gson gson = new Gson();
			UserPhotoView view = gson.fromJson(reqJson, UserPhotoView.class);

			String stype = view.getSType();
			boolean isIdcardFront = StringUtils.equals(PlContractConst.USERPHOTO_SUBTYPE_IDCARDFRONT, stype);
			boolean isIdcardBack = StringUtils.equals(PlContractConst.USERPHOTO_SUBTYPE_IDCARDBANK, stype);
			boolean isHealthidcard = StringUtils.equals(PlContractConst.USERPHOTO_SUBTYPE_HEALTHIDCARD, stype);

			if (!CheckUtil.check(view)) {
				throw new ErrorResultException(1, "參數錯誤", null);
			} else {
				String base64Image = "";
				String[] imageArr = view.getBase64Image().split("base64,");// 1.去掉頭部的data:image/png;base64,（注意有逗号）去掉。
				base64Image = imageArr[1];
				if (base64Image.length() > PlContractConst.IMAGE_SIZE_LIMIT) {
					throw new ErrorResultException(2, "圖片超過6MB", null);
				} else {
					// 先做OCR，同步存入DB
					byte[] bigImg = null;
					byte[] smallImg = null;
					String waterMark = globalConfig.IMG_WATER_FONT();
					boolean drawWaterMark = false;
					if (isDocument(view.getSType())) {
						drawWaterMark = true;
					}
					bigImg = Base64.decodeBase64(base64Image);
					bigImg = imageService.resizeImageFile(bigImg, waterMark, drawWaterMark);
					smallImg = imageService.resizeSmallImageFile(bigImg);
					String ocrResp = "";
					IDOCRRes idocrRes = null;
					// Charles: 我也不知道他為什麼(吃飽了)要用非同步，總之會異常。
					// Future<String> future = asyncService.outputAfterCreate(uniqId, view, bigImg, smallImg, drawWaterMark);
					String subSerial = asyncService.outputAfterCreate(uniqId, view, bigImg, smallImg, drawWaterMark);

					String waterMarkImage = "data:image/jpeg;base64," + Base64.encodeBase64String(bigImg);
					UploadPhotoResp uploadPhoto = new UploadPhotoResp();
					if (PlContractConst.USERPHOTO_SUBTYPE_IDCARDFRONT.equals(view.getSType())) {
						ocrResp = doIdOCRBase64(idno, base64Image, "", uniqId);
						System.out.println("#### 正面 OCR....完成");
					} else if (PlContractConst.USERPHOTO_SUBTYPE_IDCARDBANK.equals(view.getSType())) {
						ocrResp = doIdOCRBase64(idno, "", base64Image, uniqId);
						System.out.println("#### 被面 OCR....完成");
					} else { // 其它圖檔類型

					}
					idocrRes = gson.fromJson(ocrResp, IDOCRRes.class);
					if(idocrRes!=null){
						String ocrIdno = idocrRes.getIdno();
						String bypassId = idocrRes.getBYPASSID();
						String rspCode = idocrRes.getRspCode();
						String name = idocrRes.getName();
						String idCard_Issue_City = idocrRes.getIDCARD_ISSUE_City();
						String idCard_Issue_DT = idocrRes.getIDCARD_ISSUE_DT();
						String idCard_Issue_Type = idocrRes.getIDCARD_ISSUE_Type();
						String householdZipCode = getOCRZipCode(idocrRes.getHouseholdAddr1());
						String householdAddr1 = idocrRes.getHouseholdAddr1();
						String householdAddr2 = idocrRes.getHouseholdAddr2();
						String householdAddr3 = idocrRes.getHouseholdAddr3();
						String householdAddr4 = idocrRes.getHouseholdAddr4();
						String marriage = idocrRes.getMarriage();

						uploadPhoto.setName(name);
						uploadPhoto.setIdCard_Issue_City(idCard_Issue_City);
						uploadPhoto.setIdCard_Issue_DT(idCard_Issue_DT);
						uploadPhoto.setIdCard_Issue_Type(idCard_Issue_Type);	
						uploadPhoto.setHouseholdZipCode(householdZipCode);						
						uploadPhoto.setHouseholdAddr1(householdAddr1);
						uploadPhoto.setHouseholdAddr2(householdAddr2);
						uploadPhoto.setHouseholdAddr3(householdAddr3);
						uploadPhoto.setHouseholdAddr4(householdAddr4);
						uploadPhoto.setMarriage(marriage);

						// 將 OCR 的結果寫入 IdCardDate, IdCardLocation, IdCardCRecord
						try {
							
							if (isIdcardFront) {
								// @call 16. uploadIdcard
								CaseData caseData = caseDataDao.ReadByCaseNo(uniqId);
								caseData.setOCR_FRONT_Idno(ocrIdno);
								caseData.setOCR_FRONT_BYPASSID(bypassId);
								caseData.setOCR_FRONT_RspCode(rspCode);
								caseData.setOCR_FRONT_Name(name);
								caseData.setOCR_FRONT_IDCARD_ISSUE_DT(idCard_Issue_DT);
								caseData.setOCR_FRONT_IDCARD_ISSUE_Type(idCard_Issue_Type);
								caseData.setOCR_FRONT_IDCARD_ISSUE_City(idCard_Issue_City);
								caseDataDao.Update(caseData);
							} else if (isIdcardBack) {
								CaseData caseData = caseDataDao.ReadByCaseNo(uniqId);
								caseData.setOCR_BACK_HouseholdAddr1(householdAddr1);
								caseData.setOCR_BACK_HouseholdAddr2(householdAddr2);
								caseData.setOCR_BACK_HouseholdAddr3(householdAddr3);
								caseData.setOCR_BACK_HouseholdAddr4(householdAddr4);
								caseData.setOCR_BACK_Marriage(marriage);
								caseDataDao.Update(caseData);
							} else if (isHealthidcard) {
								// @call 22. uploadFinpf
							}
							
						} catch(Exception e) {
							logger.error("CaseData not found.", e);
						}
					}
					uploadPhoto.setWaterMarkImage(waterMarkImage);
					// Charles: 我也不知道他為什麼(吃飽了)要用非同步，總之會異常。
					// uploadPhoto.setSubSerial(future.get());
					uploadPhoto.setSubSerial(subSerial);
					return WebResult.GetResultString(0, "成功", uploadPhoto);
				}
			}
		} catch (ErrorResultException e) {
			e.printStackTrace();
			System.out.println("#### 被面 OCR....XXXXXXX-A");
			return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("未處理的錯誤", e);
			System.out.println("#### 被面 OCR....XXXXXXX-B");
			return WebResult.GetResultString(99, "系統錯誤", null);
		}
	}

	public boolean isDocument(String stype) {
		if (PlContractConst.USERPHOTO_SUBTYPE_IDCARDFRONT.equals(stype)
				|| PlContractConst.USERPHOTO_SUBTYPE_IDCARDBANK.equals(stype)
				|| PlContractConst.USERPHOTO_SUBTYPE_HEALTHIDCARD.equals(stype)) {
			return true;
		} else {
			return false;
		}
	}

	public String getOCRZipCode(String address) {
		DropdownData dropdown = new DropdownData();
		dropdown.setName(PlContractConst.DROPDOWNDATA_NAME_ZIPCODEMAPPING);
		dropdown.setDataKey(address);
		dropdown = dropdownDao.Read(dropdown);
		if (dropdown != null) {
			return dropdown.getDataName();
		} else {
			return "";
		}
	}

	public String delete(String reqJson, String uniqId) {
		try {
			Gson gson = new Gson();
			DeletPhotoView view = gson.fromJson(reqJson, DeletPhotoView.class);
			if (!CheckUtil.check(view)) {
				throw new ErrorResultException(1, "參數錯誤", null);
			} else {
				UserPhoto photo = new UserPhoto();
				photo.setUniqId(uniqId);
				photo.setSubSerial(Integer.valueOf(view.getSubSerial()));
				userPhotoDao.Delete(photo);
				return WebResult.GetResultString(0, "成功", "");
			}
		} catch (ErrorResultException e) {
			return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
		} catch (Exception e) {
			logger.error("未處理的錯誤", e);
			return WebResult.GetResultString(99, "系統錯誤", null);
		}
	}

	public String getPhotoList(String uniqId) {
		try {
			List<PhotoListResp> photoListResp = new ArrayList<PhotoListResp>();
			UserPhoto photo = new UserPhoto();
			photo.setUniqId(uniqId);
			photo.setStatus(PlContractConst.USERPHOTO_STATUS_USER_PROCESS);
			List<UserPhoto> list = userPhotoDao.Query(photo);
			for (UserPhoto userPhoto : list) {
				PhotoListResp photoResp = new PhotoListResp();
				String waterMarkImage = "data:image/jpeg;base64," + Base64.encodeBase64String(userPhoto.getImageBig());
				photoResp.setWaterMarkImage(waterMarkImage);
				photoResp.setSubSerial(String.valueOf(userPhoto.getSubSerial()));
				photoResp.setSType(String.valueOf(userPhoto.getSType()));
				photoResp.setPType(String.valueOf(userPhoto.getPType()));
				photoListResp.add(photoResp);
			}
			return WebResult.GetResultString(0, "成功", photoListResp);
		} catch (ErrorResultException e) {
			return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
		} catch (Exception e) {
			logger.error("未處理的錯誤", e);
			return WebResult.GetResultString(99, "系統錯誤", null);
		}
	}

	public String doIdOCRBase64(String userid, String frontId, String backId,String uniqId) throws Exception {
        IDOCRReq idocrReq = new IDOCRReq(userid,frontId,backId);
        Gson gson = new Gson();
        return apsService.queryOCR("/OCR/getIDOCR", gson.toJson(idocrReq),uniqId);
    }

    public String doBcOCRBase64(String userid, String image64,String uniqId) throws Exception {        
        BCOCRReq bcocrReq = new BCOCRReq(userid,image64);
        Gson gson = new Gson();
        // 直接呼叫ocr方法
        return apsService.queryOCR("/OCR/getBCOCR",  gson.toJson(bcocrReq),uniqId);
    }

	public String uploadBCard(String action,String reqJson, String uniqId,String idno) {
		try {
			Gson gson = new Gson();
			OCRView view = gson.fromJson(reqJson, OCRView.class);
			if (!CheckUtil.check(view)) {
				throw new ErrorResultException(1, "參數錯誤", null);
			} else {
				String base64Image = "";
				String[] imageArr = view.getBase64Image().split("base64,");// 1.去掉頭部的data:image/png;base64,（注意有逗号）去掉。
				base64Image = imageArr[1];
				if (base64Image.length() > PlContractConst.IMAGE_SIZE_LIMIT) {
					throw new ErrorResultException(2, "圖片超過6MB", null);
				} else {
					//做OCR
					String ocrResp = "";
					if (action.equals("uploadBCard")) {
						ocrResp = doBcOCRBase64(idno, base64Image, uniqId);
						BCOCRRes bcocrRes = gson.fromJson(ocrResp, BCOCRRes.class);	
						bcocrRes.setZIPCODE(getOCRZipCode(bcocrRes.getCompanyAddr1()+bcocrRes.getCompanyAddr2()));
						return WebResult.GetResultString(0, "成功", bcocrRes);
					} else {
						//信用卡				
						return WebResult.GetResultString(0, "成功", "");
					}
				}
			}
		} catch (ErrorResultException e) {
			return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
		} catch (Exception e) {
			logger.error("未處理的錯誤", e);
			return WebResult.GetResultString(99, "系統錯誤", null);
		}
	}

	public List<UserPhoto> getWaitSendOrbitPhotoList(String uniqId){
		UserPhoto dto = new UserPhoto();
		dto.setUniqId(uniqId);
		dto.setPType(1);
		dto.setStatus("2");
		return  userPhotoDao.Query(dto);
	}


	// public void updateStatus(String uniqId,String subSerial,String status){
	// 	UserPhoto dto = new UserPhoto();
	// 	dto.setUniqId(uniqId);
	// 	dto.setSubSerial(Integer.valueOf(subSerial));
	// 	dto.setStatus(status);
	// 	userPhotoDao.updateStatus(dto);
	// }

	/**
	 * 等待JOB處理
	 * Update status = '4'
	 */
	public int updateStatusToWaitUpload(String uniqId, String prodType, String ptype, String stype) {
        return userPhotoDao.updateStatus(USERPHOTO_STATUS_WAIT_UPLOAD_4, uniqId, USERPHOTO_ONLINE_1, prodType, ptype, stype);
    }

}
