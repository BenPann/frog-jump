package com.kgi.crosssell.ap.dao;

import java.util.List;

import com.kgi.crosssell.common.dto.CS_PersonalData;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class CS_PersonalDataDao extends CRUDQDao<CS_PersonalData> {

	@Override
	public int Create(CS_PersonalData fullItem) {
		String sql = "INSERT INTO CS_PersonalData VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,getdate())";
		return this.getJdbcTemplate().update(sql,
				new Object[] { fullItem.getUniqId(), fullItem.getUniqType(), fullItem.getHasAum(),
						fullItem.getUpdateAum(), fullItem.getUUId(), fullItem.getIdno(), fullItem.getName(),
						fullItem.getBirthday(), fullItem.getMobile(), fullItem.getEMail(),
						fullItem.getCommAddrZipCode(), fullItem.getCommAddress(), fullItem.getResTelArea(),
						fullItem.getResPhone() });

	}

	@Override
	public CS_PersonalData Read(CS_PersonalData keyItem) {
		String sql = "SELECT * FROM CS_PersonalData WHERE UniqId = ? AND 1=1";
		try {
			return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CS_PersonalData.class),
					new Object[] { keyItem.getUniqId() });
		} catch (DataAccessException ex) {

			System.out.println("#### " + keyItem.getUniqId() + " 在 CS_PersonalData 查無對應資料.");
			return null;
		}
	}

	@Override
	public int Update(CS_PersonalData fullItem) {
		String sql = "UPDATE CS_PersonalData SET UpdateAum = ? , UUId = ? WHERE UniqId = ?";
		return this.getJdbcTemplate().update(sql,
				new Object[] { fullItem.getUpdateAum(), fullItem.getUUId(), fullItem.getUniqId() });
	}

	@Override
	public int Delete(CS_PersonalData keyItem) {
		return 0;
	}

	@Override
	public List<CS_PersonalData> Query(CS_PersonalData keyItem) {
		return null;
	}

}
