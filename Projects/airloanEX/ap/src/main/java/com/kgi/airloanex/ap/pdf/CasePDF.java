package com.kgi.airloanex.ap.pdf;

import static com.kgi.airloanex.common.PlContractConst.UNIQ_TYPE_02;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.CaseAuthDao;
import com.kgi.airloanex.ap.dao.CaseDocumentDao;
import com.kgi.airloanex.ap.dao.PDFDao;
import com.kgi.airloanex.ap.dao.QR_ChannelListDao;
import com.kgi.airloanex.ap.pdf.dto.PDFCaseData;
import com.kgi.airloanex.ap.service.APSPlContractService;
import com.kgi.airloanex.ap.service.ChannelDepartService;
import com.kgi.airloanex.ap.service.SendMailHunterService;
import com.kgi.airloanex.common.ImageConst;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.db.CaseAuth;
import com.kgi.airloanex.common.dto.db.CaseDocument;
import com.kgi.airloanex.common.dto.db.DropdownData;
import com.kgi.airloanex.common.dto.db.QR_ChannelDepartList;
import com.kgi.airloanex.common.dto.db.QR_ChannelList;
import com.kgi.airloanex.common.util.ExportUtil;
import com.kgi.eopend3.ap.config.GlobalConfig;
import com.kgi.eopend3.ap.dao.DropdownDao;
import com.kgi.eopend3.common.util.CommonFunctionUtil;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class CasePDF extends BasePDFDocument {

    private static final Logger logger = LogManager.getLogger(CasePDF.class.getName());

    private String _uniqId = "";

    @Autowired
    private GlobalConfig global;
    
	@Autowired
	private AirloanEXConfig globalConfig;

    @Autowired
    private PDFDao pdfDao;

    @Autowired
    private DropdownDao dropdownDao;

    @Autowired
    private CaseAuthDao caseAuthDao;

    @Autowired
    private APSPlContractService apsService;

    @Autowired
    private SendMailHunterService sendMailHunterService;
    
    @Autowired
    private ChannelDepartService channelDepartService;
    
    @Autowired
    private CaseDocumentDao caseDocumentDao;
    
    @Autowired
    private QR_ChannelListDao channelListDao;

    @Autowired
    private PageFooter footer;
    
    private PDFCaseData pdfCaseData = null;
    private List<DropdownData> dropdownDatas = null;
    private QR_ChannelDepartList channelDepart;
    private QR_ChannelList channel;
    
    public String templateVersion = "";
    
    public CasePDF(ApplicationContext appContext) {
        super(appContext);
    }

    public void init(String... params) throws IOException {
        this._uniqId = params[0];
        // 是否加密
        this.setUseEncrepyt(true);
        // this.setEncryptPassword(idno);
        // 是否使用浮水印
        this.setUseWaterMark(true);
		this.setWaterMarkText(globalConfig.IMG_WATER_FONT());
		this.setWaterMarkFont(globalConfig.PDF_FONT_WATER_TTF());
        // 是否要PDF轉成圖片
        this.setPdfToImage(false);
        // 是否存DB
        this.setUseSaveToDB(true);
        // 是否發信
        this.setUseSendEMail(true);
        // 是否設定每頁的事件
        this.setPageEvent(footer);
    }

    @Override
    public void beforePdfProcess() {
        try {
            Boolean useCht = false;
            CaseAuth caseAuth = new CaseAuth(this._uniqId);
            CaseAuth read = caseAuthDao.Read(caseAuth);
            logger.info("### " + new Gson().toJson(caseAuth));
            if (null != read) {
                useCht = read.getChtAgree().equals("1");
            }

            // 取得Html的方法
            String htmlUrl = "";
            String StaticFilePath = global.StaticFilePath;
            if (useCht) {
                htmlUrl = StaticFilePath + globalConfig.PDF_PL_MBC_TemplateFilePath();
                templateVersion = globalConfig.PDF_PL_MBC_VerNo();
            } else {
                htmlUrl = StaticFilePath + globalConfig.PDF_PL_TemplateFilePath();
                templateVersion = globalConfig.PDF_PL_VerNo();
            }
            logger.info("### htmlUrl => " + htmlUrl);
            InputStream input = new FileInputStream(new File(htmlUrl));
            this.setRawHtml(this.getContentFromFilePath(input));

            pdfCaseData = pdfDao.loadCaseData(this._uniqId);
            dropdownDatas = dropdownDao.loadDropdownData();

            channelDepart = this.channelDepartService.getLoanChannelDepart(this._uniqId);
            channel = this.channelListDao.Read(new QR_ChannelList(channelDepart.getChannelId()));
            this.setEncryptPassword(pdfCaseData.idno);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("取得PDF檔案失敗! CaseNo = " + this._uniqId);
        }

    }

    @Override
    public String replaceField(String raw) {
        logger.info("### Begin - replaceField");
        boolean isPersonCredit = false;
        boolean isRecycleCredit = false;
        String str = raw;
        str = str.replace("[MemberNo]", "");
        str = str.replace("[AccountNo]", "");
        str = str.replace("[ISBN]", "");
        if (pdfCaseData.productId.equalsIgnoreCase("3")) {
            isRecycleCredit = true;
            isPersonCredit = false;
        } else {
            isRecycleCredit = false;
            isPersonCredit = true;
        }
        // 備份一下
        // " <img alt=\"\" style=\"width:20px;\"
        // src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJzSURBVGiB7Zo9SBxBGIafCImKKNyhgpIqpEshasDqRBsLG5GEYIqEVCEEQkQUKyOIRYoUdjaWgoXiIdgIFrFLJATEQoRgYwQlBk08JUT8KfY7VmW9m5n9G2VfGGa5/X7e9/ZmZ76ZgwQJEvhBLTAMLAM7wFnEbUdyDwE1piKeA7kYyF/XDoBnuiKeAqcSYBrIACndIAEgBbQCM8LlFOhWda4G/ojj+zDYGaIPh9MekFZxGMF9ErYhi8NtWMV4RYwzYTIyRBsOt+8qxgdiXBEiIVNU4nD7q2Kcf0vYCk9+JTEQCQWJENtgo5DSoALFOdhfAD+BxwVslPnFJaQFOJLcbwvYWS3kIbAreT8VsbVWSApYk5xZio9bK4XcAz5LvmWgXMHHSiETkmsTqFf0sU5Ir+TJAU0aflYJ6QCOcQqlHk3fQIWUAgvAa00SAA24K+xBA/9AhTzBLT3faZCoATbEdxK4o+Grw0/L8A1wIrYfFUhVAN/EfhG4q0LGBz+tMdID/Bf7ca6fA0pwNxDWUay5/fLTHeydwKH4TOH9TY/K/V/AA43YvviZvLUywL74zXN5Ynspn/8jmH2A0F+/jbi7kUtAFdCO+9N7ZRDTFz8/88gjYEv8vwDbcj1mGM8LkU2IdcDqhTizBFvARTqzp3GeyFfUFoI6iHyJkgLuBxDnKqxaa/lBsq91I3CrheSkt3UTG5wy4BK8hGxIr1O1RYVm6X9cveElZE76/tDomGNA+rmCVoKLR299YTEywACaR2/gfRhaFga7IijHx2FoHt3AbwofGUfZdoEuXRF5pIEPOJtn+eIpynaI+4eBOI7HEyS4NTgH3tZKOTc9lX8AAAAASUVORK5CYII=\"/>"
        // " <img alt=\"\" style=\"width:20px;\"
        // src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFFSURBVGiB7ZqxSgNBFEUPVopYJKgfonXEP5CACH6FEEypAX9HIeQ3LAR7sQ8ooknaaDFvicqosxPHfcg9MLxi3+zew8IW8xaEEMuwDQyAa2AMvP7xGtuzz4CtXIljYNpA+K/WBDiqK3EIzO0Gl0AHaNW9yS/QAvaAK8syB7qpmzeBZ9t4UiJdJj1CpiegnbLhgsWb8MaQkG2Q0nxrzZ2SiTLZJ2S7SWmeWPN6wUC5bBCyvaQ0V18Jr0TzrTQQpAgS8YZEvCERb0jEGxLxhkS8IRFvSMQbEvGGRLwhEW9IxBsS8UZMZGrV6yE2hIP2D8RE7q3uFIuTz67Vu88XYiIjq6fF4uTTtzr6tst4P3rrlUqUQZ+aozeID0NXS6T7gTWWGIZWdIFHmh9LV+sBOKgrUdEGzglD+1kD4WcsfhhoYjwuxL/hDTwNt7HmQsC4AAAAAElFTkSuQmCC\"/>"
        String checkedHtml = "<img alt=\"\" style=\"width:20px;\" src=\"" + ImageConst.CHECKED_IMAGE + "\"/>";
        String uncheckedHtml = "<img alt=\"\" style=\"width:20px;\" src=\"" + ImageConst.UNCHECKED_IMAGE + "\"/>";
        if (isPersonCredit) { // checked
            // 個人信用貸款checkbox
            str = str.replace("[Personal_credit_checkbox]", checkedHtml);
            // 申請個人信用貸款checkbox
            str = str.replace("[Apply_personal_credit_checkbox]", checkedHtml);
            // 申請個人信用貸款金額
            str = str.replace("[Apply_personal_credit_money]", pdfCaseData.p_apy_amount + "萬元");
        } else { // unchecked
            // 個人信用貸款checkbox
            str = str.replace("[Personal_credit_checkbox]", uncheckedHtml);
            // 申請個人信用貸款checkbox
            str = str.replace("[Apply_personal_credit_checkbox]", uncheckedHtml);
            // 申請個人信用貸款金額
            str = str.replace("[Apply_personal_credit_money]", "____________萬元");
        }

        if (isRecycleCredit) { // checked
            // 循環信用貸款checkbox
            str = str.replace("[Personal_recycle_checkbox]", checkedHtml);
            // 申請循環信用貸款checkbox
            str = str.replace("[Apply_personal_recycle_checkbox]", checkedHtml);
            // 申請循環信用貸款金額
            str = str.replace("[Apply_personal_recycle_money]", pdfCaseData.p_apy_amount + "萬元");
        } else { // unchecked
            // 循環信用貸款checkbox
            str = str.replace("[Personal_recycle_checkbox]", uncheckedHtml);
            // 申請循環信用貸款checkbox
            str = str.replace("[Apply_personal_recycle_checkbox]", uncheckedHtml);
            // 申請循環信用貸款金額
            str = str.replace("[Apply_personal_recycle_money]", "____________萬元");
        }

        str = str.replace("[Loan_period]",
                (pdfCaseData.p_period == 0) ? "" : String.valueOf(pdfCaseData.p_period) + "年"); // 貸款期間(年)
        boolean noCapitaluse = true;
        for (DropdownData dropdownData : dropdownDatas) {

            if (dropdownData.getName().equalsIgnoreCase("moneypurpose")
                    && dropdownData.getDataKey().equalsIgnoreCase(String.valueOf(pdfCaseData.p_purpose))) {
                // 資金用途
                // 選其他時，套上其他說明欄位
                if (dropdownData.getDataKey().equalsIgnoreCase("16")) {
                    str = str.replace("[Capital_use]", pdfCaseData.p_purpose_name);
                } else {
                    str = str.replace("[Capital_use]", dropdownData.getDataName());
                }
                noCapitaluse = false;
                break;
            }
        }
        if (noCapitaluse) {
            str = str.replace("[Capital_use]", "");
        }
        str = str.replace("[Apply_user_name]", pdfCaseData.customer_name); // 申請人姓名
        String gender = "";
        if("1".equals(pdfCaseData.gender)) {
        	gender="男";
        }else if("2".equals(pdfCaseData.gender)) {
        	gender="女";
        } else if(pdfCaseData.idno !=null &&pdfCaseData.idno.length()==10) {
            //判別用戶身分證第二碼 為1、8:男生 2、9:女生
            String genderNumber = pdfCaseData.idno.substring(1, 2);
        	if("1".equals(genderNumber) || "8".equals(genderNumber)){
        		gender="男";
        	} else if("2".equals(genderNumber) || "9".equals(genderNumber)){
        		gender="女";
        	} else {
        		gender="女";
        	}
        }else {
        	gender="女";
        }
        str = str.replace("[Apply_user_gender]", gender); // 性別
        
        str = str.replace("[Apply_user_company]", pdfCaseData.corp_name); // 公司名稱
        // 出生日期
        if (!pdfCaseData.birthday.isEmpty()) {
            if (pdfCaseData.birthday.length() == 8)
                str = str.replace("[Apply_user_birth]",
                        "民國" + String.valueOf((Integer.parseInt(pdfCaseData.birthday.substring(0, 4)) - 1911)) + "年"
                                + pdfCaseData.birthday.substring(4, 6) + "月" + pdfCaseData.birthday.substring(6, 8)
                                + "日");
            else
                str = str.replace("[Apply_user_birth]", "民國____年____月____日");
        }

        for (DropdownData dropdownData : dropdownDatas) {
            if (dropdownData.getName().equalsIgnoreCase("marriage")
                    && dropdownData.getDataKey().equalsIgnoreCase(String.valueOf(pdfCaseData.marriage))) {
                str = str.replace("[Apply_user_marriage]", dropdownData.getDataName()); // 婚姻
                break;
            }
        }
        // 公司電話
        str = str.replace("[Apply_user_company_phone]", "(" + pdfCaseData.corp_tel_area.trim() + ")"
                + pdfCaseData.corp_tel.trim() + " 分機:" + pdfCaseData.corp_tel_exten);
        str = str.replace("[Apply_user_identity_code]", pdfCaseData.idno); // 身分證統一編號
        str = str.replace("[Apply_user_company_address]", pdfCaseData.corp_city_code.concat(pdfCaseData.corp_address)); // 公司地址(含郵遞區碼)
        for (DropdownData dropdownData : dropdownDatas) {
            if (dropdownData.getName().equalsIgnoreCase("education")
                    && dropdownData.getDataKey().equalsIgnoreCase(String.valueOf(pdfCaseData.education))) {
                str = str.replace("[Apply_user_education]", dropdownData.getDataName()); // 教育程度
                break;
            }
        }
        str = str.replace("[Apply_user_annual_income]", String.valueOf(pdfCaseData.yearly_income) + "萬元"); // 年收入
        // 到職年月
        if (!pdfCaseData.on_board_date.isEmpty()) {
            if (pdfCaseData.on_board_date.length() == 8)
                str = str.replace("[Apply_user_first_day_of_work]",
                        "民國" + String.valueOf((Integer.parseInt(pdfCaseData.on_board_date.substring(0, 4)) - 1911))
                                + "年" + pdfCaseData.on_board_date.substring(4, 6) + "月");
            else
                str = str.replace("[Apply_user_first_day_of_work]", "民國____年____月");
        }
        str = str.replace("[Apply_user_email]", pdfCaseData.email_address); // 電子信箱
        str = str.replace("[Apply_user_job_title]", pdfCaseData.title); // 職稱
        for (DropdownData dropdownData : dropdownDatas) {
            if (dropdownData.getName().equalsIgnoreCase("estate")
                    && dropdownData.getDataKey().equalsIgnoreCase(String.valueOf(pdfCaseData.estate_type))) {
                str = str.replace("[Apply_user_real_estate]", dropdownData.getDataName()); // 不動產狀況
                break;
            }
        }
        str = str.replace("[Apply_user_mobile_number]", pdfCaseData.mobile_tel); // 行動電話
        str = str.replace("[Apply_user_address]", pdfCaseData.house_city_code.concat(pdfCaseData.house_address)); // 現居地址
        str = str.replace("[Apply_user_home_phone]",
                "(" + pdfCaseData.house_tel_area.trim() + ")" + pdfCaseData.house_tel); // 居住電話(含郵遞區碼)

        String project_code = "";
        if (StringUtils.isNotBlank(pdfCaseData.prj_code)) {
            project_code = pdfCaseData.prj_code;
        } else if (StringUtils.isNotBlank(pdfCaseData.PJ)) {
            project_code = pdfCaseData.PJ;
        }
        str = str.replace("[Apply_user_project_code]", project_code); // 專案代碼
        // 申請日期
        // String applydate = DateUtil.GetDateFormatString();

        // 如果是中華電信的 用中華電信的時間
        CaseAuth caseAuth = new CaseAuth(this._uniqId);
        CaseAuth read = caseAuthDao.Read(caseAuth);
        boolean useCht = false;
        if (null != read) {
            useCht = read.getChtAgree().equals("1");
            if (useCht) {
            	String chtAgreeTime ="";
            	String chtAgreeHour="";
            	String chtAgreeMinute="";
            	// ChtAgreeTime
                if(read.getChtAgreeTime() != null && !"".contentEquals(read.getChtAgreeTime()) ) {
                	chtAgreeTime = read.getChtAgreeTime().toString().split(" ")[1];
                	
                }
                if(chtAgreeTime != null && !"".equals(chtAgreeTime) )
                {	
                	chtAgreeHour = chtAgreeTime.split(":")[0];
                	chtAgreeMinute = chtAgreeTime.split(":")[1];
                	String agree = chtAgreeHour.concat(":").concat(chtAgreeMinute);
                	str = str.replace("[get_cht_rating_time]",agree);
                	str = str.replace("[get_cht_rating_time]", agree);
                }	
            }
        }
        String applydate = "";
        String verifyType = "";
        
        boolean hasNoCreditVerify = !pdfCaseData.applycc;
        logger.info("### hasNoCreditVerify=" + hasNoCreditVerify);
        if(hasNoCreditVerify) { // hasNoCreditVerify
        	applydate = pdfCaseData.applyccUtime;
        	verifyType = PlContractConst.CREDITVERIFY_OTP;
        } else { // hasCreditVerify
            applydate = pdfCaseData.CaseNo.substring(4, 12);
            String authType = apsService.externalCheckTypeForAddNewCase(this._uniqId);
            verifyType = apsService.transAuthType(authType);
            logger.info("### authType=" + authType);
            logger.info("### verifyType=" + verifyType);
        }        

        str = str.replace("[Apply_date]",
                "中華民國".concat(String.valueOf(Integer.parseInt(applydate.substring(0, 4))-1911)) + "年" + applydate.substring(4, 6) + "月" + applydate.substring(6, 8) + "日");
        // str = str.replace("[Apply_date]", "____年____月____日");

        // 申請人ip address
        str = str.replace("[ip_address]", pdfCaseData.ip_address);
		boolean promoMemberIsID = false;
		String channelName = "";
        if (this.channel != null) {
        	promoMemberIsID = this.channel.getIdLogic().equals("0");
			//2019/09/24 增加 通路來源 channel
			channelName = this.channel.getChannelName();
        }
        String transMember = this.channelDepart.getTransMember();
		if(promoMemberIsID) {
			transMember=CommonFunctionUtil.hiddenUserDataWithStar(transMember,3,3);			
        }
        str = str.replace("[Trans_Depart]", this.channelDepart.getTransDepart());
        str = str.replace("[Trans_Employee]", transMember);
        str = str.replace("[Promo_Depart]", this.channelDepart.getPromoDepart());
        str = str.replace("[Promo_Employee]", this.channelDepart.getPromoMember());
		//2019/09/24 增加 增加 通路來源 channel and UniqId
        str = str.replace("[Channel_info]", channelName);
        str = str.replace("[UniqId]", this._uniqId);

        
        
        /*
        CreditVerify cv =new CreditVerify(this._uniqId);
        cv= creditVerifyDao.Read(cv);
        String verifyType =  "";
        if(cv != null ) {
	        switch (cv.getVerifyType()) {
			case "0":
				verifyType="他行卡驗身";
				break;
			case "1":
				verifyType="他行帳戶驗身";
				break;
			case "2":
				verifyType="MBC";
				break;
			default:
				verifyType="";
				break;
			}
        }*/
        str = str.replace("[Auth_Type]", verifyType);
        //20200130 戶籍地址與電話
        str = str.replace("[Apply_user_res_address]", pdfCaseData.resAddrZipCode.concat(pdfCaseData.resAddr));
        str = str.replace("[Apply_user_res_tel]", "(".concat(pdfCaseData.resTelArea).concat(")").concat(pdfCaseData.resTel));
        logger.info("### End - replaceField");
        logger.info("### previewHtml.length()=" + str.length());
        
        // HTML檔 - @export 儲存至檔案系統
        if (globalConfig.isPDF_Export()) {
            ExportUtil.export(global.PdfDirectoryPath + "/" + "CasePDF.previewHtml.html", str);
        }

        return str;
    }

    @Override
    public void afterPdfProcess() {

    }

    @Override
    public void saveToDB(byte[] pdfByte) {
        
        // Save bytes to TABLE: CaseDocument
        CaseDocument cd = new CaseDocument(this._uniqId, UNIQ_TYPE_02, PlContractConst.DOCUMENT_TYPE_LOAN_APPLYPDF);
        cd.setContent(pdfByte);
        cd.setVersion(templateVersion);
        caseDocumentDao.CreateOrUpdate(cd);
        
        // Save string base64 to TABLE: CaseDoc
        String pdfbase64 = Base64.encodeBase64String(pdfByte);
        pdfDao.storePDF(this._uniqId, this.pdfCaseData.idno, pdfbase64, "");
        System.out.println("Save Pdf To DB");

        // PDF檔 - @export 儲存至檔案系統
        if (globalConfig.isPDF_Export()) {
            ExportUtil.export(global.PdfDirectoryPath + "/" + _uniqId + ".pdf", pdfByte);
        }
    }

    @Override
    public void pdfToImage(List<byte[]> byteList) {

    }

    @Override
    public void sendEmail(byte[] pdfByte) {
        // String pdfBase64Str = Base64.encodeBase64String(pdfByte);
        // mailHunterService.sendCaseMailHunter(this._uniqId, pdfCaseData.idno,
        // pdfCaseData.email_address, pdfBase64Str);
        sendMailHunterService.sendCaseMailHunter(pdfCaseData, pdfByte);
    }

    public PDFCaseData getPdfCaseData() {
        return pdfCaseData;
    }

    public void setPdfCaseData(PDFCaseData pdfCaseData) {
        this.pdfCaseData = pdfCaseData;
    }
}
