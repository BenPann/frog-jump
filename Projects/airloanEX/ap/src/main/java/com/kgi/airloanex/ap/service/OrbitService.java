package com.kgi.airloanex.ap.service;

import static com.kgi.airloanex.ap.service.CaseDataService.CHECK_finpf;
import static com.kgi.airloanex.ap.service.CaseDataService.CHECK_idcard;
import static com.kgi.airloanex.common.PlContractConst.DOCUMENT_TYPE_CONTRACT_PDF;
import static com.kgi.airloanex.common.PlContractConst.DOCUMENT_TYPE_CONTRACT_ACH_PDF;
import static com.kgi.airloanex.common.PlContractConst.KB_9743_DEPART_ID;
import static com.kgi.airloanex.common.PlContractConst.USERPHOTO_ProdType_2;
import static com.kgi.airloanex.common.PlContractConst.USERPHOTO_STATUS_UPLOAD_FAIL;
import static com.kgi.airloanex.common.PlContractConst.USERPHOTO_STATUS_UPLOAD_SUCCESS;
import static com.kgi.airloanex.common.PlContractConst.USERPHOTO_STATUS_WAIT_UPLOAD_4;
import static com.kgi.airloanex.common.PlContractConst.CaseDataJobStatus.CASE_STATUS_02_JobStatus_200;
import static com.kgi.airloanex.common.PlContractConst.CaseDataSubStatus.CASE_STATUS_12_SubStatus_000;
import static com.kgi.airloanex.common.dto.customDto.UploadFileDto.I001;
import static com.kgi.airloanex.common.dto.customDto.UploadFileDto.I002;
import static com.kgi.airloanex.common.dto.customDto.UploadFileDto.I003;
import static com.kgi.airloanex.common.dto.customDto.UploadFileDto.I999;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JLabel;

import com.google.gson.Gson;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.AuditDataDao;
import com.kgi.airloanex.ap.dao.CaseDataDao;
import com.kgi.airloanex.ap.dao.CaseDocumentDao;
import com.kgi.airloanex.ap.dao.ChtDao;
import com.kgi.airloanex.ap.dao.ContractDocDao;
import com.kgi.airloanex.ap.dao.ContractMainActInfoDao;
import com.kgi.airloanex.ap.dao.ContractMainDao;
import com.kgi.airloanex.ap.dao.FastPassDao;
import com.kgi.airloanex.ap.dao.MailHistoryDao;
import com.kgi.airloanex.ap.dao.UserPhotoDao;
import com.kgi.airloanex.ap.job.SendPDFJob;
import com.kgi.airloanex.ap.pdf.ContractFinalePDF;
import com.kgi.airloanex.ap.pdf.PDFHelper;
import com.kgi.airloanex.ap.pdf.dto.PDFCaseDataShoot;
import com.kgi.airloanex.ap.pdf.dto.PDFCaseDataSmall;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDto;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoConstData;
import com.kgi.airloanex.common.dto.customDto.UploadFileDto;
import com.kgi.airloanex.common.dto.db.CaseData;
import com.kgi.airloanex.common.dto.db.CaseDocument;
import com.kgi.airloanex.common.dto.db.ContractMain;
import com.kgi.airloanex.common.dto.db.ContractMainActInfo;
import com.kgi.airloanex.common.dto.db.DropdownData;
import com.kgi.airloanex.common.dto.db.FastPass;
import com.kgi.airloanex.common.dto.db.MailHistory;
import com.kgi.airloanex.common.dto.db.QR_ChannelDepartList;
import com.kgi.airloanex.common.dto.db.UserPhoto;
import com.kgi.airloanex.common.dto.orbit.OrbitImage;
import com.kgi.airloanex.common.dto.orbit.OrbitRoot;
import com.kgi.airloanex.common.util.ExportUtil;
import com.kgi.eopend3.ap.config.GlobalConfig;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.ap.dao.DropdownDao;
import com.kgi.eopend3.ap.service.DropDownService;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.json.JSONObject;

/**
 * PDF IMAGE XML 檔上傳 FTP
 * <ol>
 * <li>uploadCaseDataToOrbit() 信貸申請書(申請 apply)上傳</li>
 * <li>uploadCreditCaseDataToOrbit() 信用卡申請書上傳</li>
 * <li>uploadCaseKycToOrbit() KYC表上傳</li>
 * <li>uploadAddPhotoOrbitData() 身證及財力證明圖片上傳</li>
 * <li>uploadContractDataToOrbit() 信貸契約書(立約 contract)上傳</li>
 * </ol>
 * 
 * @see SendPDFJob
 */
@Service
public class OrbitService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
	private GlobalConfig global;

    @Autowired
	private AirloanEXConfig globalConfig;

    @Autowired
    private CaseDataDao caseDataDao;

    @Autowired
    private ContractMainDao contractMainDao;

    @Autowired
    private ContractMainActInfoDao contractMainActInfoDao;

    @Autowired
    private DropdownDao dropdownDataDao;

    @Autowired
    private ContractDocDao contractDocDao;

    @Autowired
    private FastPassDao fastPassDao;

    @Autowired
    private UserPhotoDao userPhotoDao;

    @Autowired
	private ConfigDao configDao;

    @Autowired
    private MailHistoryDao mailHistoryDao;

    @Autowired
    private ChtDao chtDao;

    @Autowired
    private AuditDataDao auditDataDao;

    @Autowired
    private FTPService ftpservice;

    @Autowired
    private ChannelDepartService channelDepartService;

    @Autowired
    private AMLService amlService;

    @Autowired
    private SOAPService soapService;

    @Autowired
    private JobLogService jobLogService;

    @Autowired
    private PdfService pdfService;

    @Autowired
    private CaseDataService caseDataService;

    @Autowired
    private KgiService kgiService;

    @Autowired
    private ContractDocService contractDocService;
	
    @Autowired
    private DropDownService dropdownService;
    
    @Autowired
    private CaseDocumentDao caseDocumentDao;

    private String createAMLImageBase64(String idno, String chtName, String amlQueryTime, String amlStauts, String occupation, String jobTitle,String idCardDate, String idCardLocation, String idCardCRecord, String IDCardResult,String idCardQryDate) {
		String sTdHtml = "</font></td><td><font style=\"font-size:20px;\">";
        String html = "<html>" +
                "<table border=1>" +
                "<tr valign=\"top\">" +
                "<td colspan=\"2\"><font style=\"font-size:24px;\">[線上申請-洗錢防制高風險系統查詢資料]</font></td>" +
                "</tr>" +
                "<tr valign=\"top\">" +
                "<td><font style=\"font-size:20px;\">  身分證字號: " + sTdHtml + idno + "</font></td>" +
                "</tr>" +
                "<tr valign=\"top\">" +
                "<td><font style=\"font-size:20px;\">  姓　　　名: " + sTdHtml  + chtName + "</font></td>" +
                "</tr>" +
                "<tr valign=\"top\">" +
                "<td><font style=\"font-size:20px;\">  AML系統查詢日期: " + sTdHtml  + amlQueryTime + "</font></td>" +
                "</tr>" +
                "<tr valign=\"top\">" +
                "<td><font style=\"font-size:20px;\">  AML系統查詢結果: " + sTdHtml  + amlStauts + "</font></td>" +
                "</tr>" +
                "<tr valign=\"top\">" +
                "<td><font style=\"font-size:20px;\">  網頁填寫職業類別: " + sTdHtml  + occupation + "</font></td>" +
                "</tr>" +
                "<tr valign=\"top\">" +
                "<td><font style=\"font-size:20px;\">  網頁填寫職稱: " + sTdHtml  + jobTitle + "</font></td>" +
                "</tr>" +
                "</table><br/>-----------------------------------------------<br/>" +
                "<table border=1>" +
                "<tr valign=\"top\">" +
                "<td colspan=\"2\"><font style=\"font-size:24px;\">[線上申請-戶役政查詢結果]</font></td>" +
                "</tr>" +
                "<tr valign=\"top\">" +
                "<td><font style=\"font-size:20px;\">  統一編號　: " + sTdHtml  + idno + "</font></td>" +
                "</tr>" +
                "<tr valign=\"top\">" +
                "<td><font style=\"font-size:20px;\">  發證日期　: " + sTdHtml  + idCardDate + "</font></td>" +
                "</tr>" +
                "<tr valign=\"top\">" +
                "<td><font style=\"font-size:20px;\">  發證地點　: " + sTdHtml  + idCardLocation + "</font></td>" +
                "</tr>" +
                "<tr valign=\"top\">" +
                "<td><font style=\"font-size:20px;\">  補領換類別: " + sTdHtml  + idCardCRecord + "</font></td>" +
                "</tr>" +
                "<tr valign=\"top\">" +
                "<td><font style=\"font-size:20px;\">  查詢日期  : " + sTdHtml  + idCardQryDate + "</font></td>" +
                "</tr>" +
                "<tr valign=\"top\">" +
                "<td><font style=\"font-size:20px;\">  查詢結果　: " + sTdHtml  + IDCardResult + "</font></td>" +
                "</tr>" +
                "</table>" +				
                "</html>";

        try {
            JLabel label = new JLabel(html);
            label.setSize(1024, 768);
            label.setForeground(Color.BLACK);
            label.setVerticalAlignment(JLabel.TOP);

            BufferedImage image = new BufferedImage(
                    label.getWidth(), label.getHeight(),
                    BufferedImage.TYPE_INT_RGB);

            // paint the html to an image
            Graphics g = image.getGraphics();
            //背景設成白色
            Color bg = new Color(255, 255, 255);
            g.setColor(bg);
            g.fillRect(0, 0, label.getWidth(), label.getHeight());
            label.paint(g);
            g.dispose();

            // get the byte array of the image (as jpeg)
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", baos);
            byte[] imageData = baos.toByteArray();
            baos.close();
            return Base64.encodeBase64String(imageData);
        } catch (Exception e) {
//            e.printStackTrace();
            return "";
        }

    }

    public String buildOrbitData(String caseno, String idno, List<UploadFileDto> pdfFileList, String dgtType, QR_ChannelDepartList channelDepart, String amlStatus, String productId) throws Exception {

        // 取出須同步至ORBIT的主檔資料
        List<CaseData> map = caseDataDao.queryDatabyCaseNo(caseno);
        String branchId = this.channelDepartService.getAssignDepart(channelDepart, "02", "");
        if (null == map) {
            jobLogService.AddSendDataChkfailLog("Caseno:".concat(caseno).concat(" buildOrbitData no Data"));
            return branchId;
        }

        // add check method
        boolean isApplyLoanCC = caseDataDao.isApplyLoanCC(caseno);

        OrbitRoot orbitRoot = new OrbitRoot();
        processMainData(map, orbitRoot, "C001", pdfFileList, dgtType, isApplyLoanCC, channelDepart, amlStatus);

        String fileName = DateUtil.GetDateFormatString("yyyyMMddHHmmssS").concat(map.get(0).getCaseNo()).concat(idno) + ".xml";

        StringBuilder sb = new StringBuilder();
        orbitRoot.toXML(sb);
        jobLogService.AddSendDataInfoLog("信貸一般進件案件CaseNo:" + caseno + "打包後送XML檔名:" + fileName + " XML資訊：" + sb.toString());
        InputStream in = IOUtils.toInputStream(sb.toString(), "UTF-8");

        // XML檔 - @export 儲存至檔案系統
        if (globalConfig.isPDF_Export()) {
            ExportUtil.export(global.PdfDirectoryPath + "/" + fileName, sb.toString().getBytes("UTF-8"));
        }

        if (!ftpservice.uploadImgXml(fileName, in)) {
            throw new RuntimeException("上傳ftp失敗!!");
        }
        return branchId;
    } // end buildOrbitData

    /**
     *
     */
    private void processMainData(List<CaseData> lscasedata, OrbitRoot orbitRoot, String documentType, List<UploadFileDto> fileNameArray, String dgtType, boolean isApplyLoanCC, QR_ChannelDepartList channelDepart, String amlStatus) throws Exception {
    	InetAddress ip = null;
        ip = InetAddress.getLocalHost();
        //type 預核或非預核帶值不同
        //2019/11/12 以後預核改成非預核相同
        String xmltype = "0";  //預設為預核件的值   @2020-06-04 , 淑敏需求: 0--->1 by Ben
        //做是否為非預核判斷
        if (StringUtils.equals(lscasedata.get(0).getIsPreAudit(), "2")) {
            //非預核(一般)
            xmltype = "1";
            //開始判斷是否為非預核快速件
            if (StringUtils.equals(lscasedata.get(0).getImportantCust(), "Y")) {
                //第2階段不會有快速件，第3階段有aml時，將xmltype改為2 送件
                xmltype = "1";
                //xmltype = "2";
            }
        }
        CaseData caseData = lscasedata.get(0);
        String uniqId = caseData.getCaseNo();

        String idno = caseData.getIdno();
        String IsPreAudit = caseData.getIsPreAudit();

        // 卡加貸的時候才要值
        String parentUUID = (isApplyLoanCC) ? uniqId : "";

        String productId = caseData.getProductId();

        // 卡加貸 - productTypeID=P037
        // PA - productTypeID=P035 casePriority=F001
        // PL - productTypeID=P035 casePriority=F037
        // RPL - productTypeID=P035 casePriority=F036
        String productTypeId = OrbitImage.productTypeId(isApplyLoanCC);
        String casePriority = OrbitImage.casePriority(productId);
        
        String prjCode = StringUtils.isBlank(caseData.getPrj_code()) ? "-" : caseData.getPrj_code();
        String applyAmount = caseData.getP_apy_amount();
        String assignDepart = this.channelDepartService.getAssignDepart(channelDepart, "02", "");
        String customer_name = StringUtils.trimToEmpty(caseData.getCustomer_name());
        String mobile_tel = StringUtils.trimToEmpty(caseData.getMobile_tel());
        String occupation = StringUtils.trimToEmpty(caseData.getOccupation());
        String title = StringUtils.trimToEmpty(caseData.getTitle());
        String hasOldIdImg = StringUtils.trimToEmpty(caseData.getHasOldIdImg());
        String hasOldFinaImg = StringUtils.trimToEmpty(caseData.getHasOldFinaImg());
        String oldIdImgNoList = StringUtils.trimToEmpty(caseData.getOldIdImgNoList());
        String oldFinaImgNoList = StringUtils.trimToEmpty(caseData.getOldFinaImgNoList());


        List<String> mCaseIds = new ArrayList<>();
        List<String> mMergeImageTypes = new ArrayList<>();
        // 
        if (StringUtils.isNotBlank(hasOldIdImg) && StringUtils.isNotBlank(oldIdImgNoList)) {
            String[] oldIdImgNoLists = StringUtils.split(oldIdImgNoList, ";");
            for (int i = 0; oldIdImgNoList != null && i < oldIdImgNoLists.length; i++) {
                mCaseIds.add(oldIdImgNoLists[i]);
                mMergeImageTypes.add("I002");
            }
        }
        // 
        if (StringUtils.isNotBlank(hasOldFinaImg) && StringUtils.isNotBlank(oldFinaImgNoList)) {
            String[] oldFinaImgNoLists = StringUtils.split(oldFinaImgNoList, ";");
            for (int i = 0; oldFinaImgNoLists != null && i < oldFinaImgNoLists.length; i++) {
                mCaseIds.add(oldFinaImgNoLists[i]);
                mMergeImageTypes.add("I003");
            }
        }

        Map<String, Boolean> completeMap = caseDataService.makeCompleteMap(uniqId);

        // mCaseId=影編1|影編2|影編3
        String mCaseId = StringUtils.join(mCaseIds, "|");
        // cMergeImageType=I002|I002|I003
        String cMergeImageType = StringUtils.join(mMergeImageTypes, "|"); // I001-申請書, I002-ID, I003-財力, ALL-全部
        // cComplete=N
        String cComplete = caseDataService.cComplete(completeMap); // Y-完整, N-不完整
        // cAbnormalReason=缺ID、缺戶役政、缺財力、缺同一關係人表、缺風險評級、缺AML、缺Z07、是利關人、缺KYC、APS異常件
        String cAbnormalReason = caseDataService.cAbnormalReason(completeMap);

        caseDataDao.updateIsCaseIntegrated(uniqId, cComplete);
        caseDataDao.updatecAbnormalReason(uniqId, cAbnormalReason);

		// logger.info("occupation:" + occupation + ", title=" + title) ;

        if (amlStatus == null) {
            FastPass fp = fastPassDao.Read(new FastPass(uniqId));
            if (fp != null) {
            	if (fp.getAML().equals("V")) {
            		amlStatus = "O" ;
            	} else {
            		amlStatus = fp.getAML() ;
            	}
            } else {
        		amlStatus = "" ;
            }
        }
        
        // 增加判斷職業別
		if(amlService.isHighRiskOccupation(occupation, title)){ // 高風險
			amlStatus = "Y" ;
		}
        
        // 0413-001.當CHECK_NO_FINPF為Y，且CHECK_HAVE_FINPF有APS案編時，上傳影像XML的<notes>內容要標[免財力信用卡或信貸或循貸之案件編號%APS案編%]
        String notes = "";
        String applyWithoutFina = caseData.getApplyWithoutFina(); // CHECK_NO_FINPF=Y
        String caseNoApsPrev = caseData.getCaseNoApsPrev();
        if (StringUtils.equals("01", applyWithoutFina) && StringUtils.isNotBlank(caseNoApsPrev)) {
            notes = "免財力信用卡或信貸或循貸之案件編號" + caseNoApsPrev;
        }
        
        //當C001:申請件時，uuid才塞值
        if (documentType.equals("C001")) {
            // 如果數位身分為2 就走快速
            if (dgtType.equals("2")) {
                xmltype = "2";
            }
            if (StringUtils.equals("Y", cComplete)) {
                xmltype = "2";
            }
            // 得知管道 KnowChannelType (INFOSOURCE_CDE)判斷調整如紅字，當最後為14/15時，影像XML SubCaseType=1 demand by Ben
            if (StringUtils.containsAny(caseData.getINFOSOURCE_CDE(), "14", "15", "85")) {
                xmltype = "1";
            }
            orbitRoot.orbitHeader.from(ip.getHostAddress(), xmltype, caseData.getCaseNoWeb(), "9999", "webuser", notes, parentUUID, prjCode, 
                    channelDepart.getPromoDepart(), channelDepart.getPromoMember(), applyAmount, productTypeId, 
                    channelDepart.getTransDepart(), channelDepart.getTransMember(), 
                    customer_name, mobile_tel, amlStatus, occupation, 
                    mCaseId, cMergeImageType, cComplete, cAbnormalReason);
        } else {
			//當C002:補件時
			//依據佳欣要求改subcasetype為0
			String subcaseTypeForC002 = "0";
            orbitRoot.orbitHeader.from(ip.getHostAddress(), subcaseTypeForC002, "", "9999", "webuser", notes, parentUUID, prjCode,
                    channelDepart.getPromoDepart(), channelDepart.getPromoMember(), applyAmount, productTypeId, 
                    channelDepart.getTransDepart(), channelDepart.getTransMember(), 
                    customer_name, mobile_tel, amlStatus, occupation, 
                    mCaseId, cMergeImageType, cComplete, cAbnormalReason);
        }

        String subcaseType = orbitRoot.orbitHeader.subcaseType;
        caseDataDao.updateSubcaseType(uniqId, subcaseType);
        
        if (null != fileNameArray && fileNameArray.size() > 0) {

            List<UploadFileDto> I001s = new ArrayList<>();
            List<UploadFileDto> I002s = new ArrayList<>();
            List<UploadFileDto> I003s = new ArrayList<>();
            List<UploadFileDto> I999s = new ArrayList<>();
            Map<String, List<UploadFileDto>> fileGroups = new LinkedHashMap<>();
            fileGroups.put(I001, I001s);
            fileGroups.put(I002, I002s);
            fileGroups.put(I003, I003s);
            fileGroups.put(I999, I999s);
            
            for (int j = 0; j < fileNameArray.size(); j++) {

                UploadFileDto uploadFile = fileNameArray.get(j);
                String filename = uploadFile.getFilename();
                String imgType = uploadFile.getImgType();
                //第一筆row時 塞主要 XML data
                // if (j== 0 && StringUtils.equals(I001, imgType)) {
                //     orbitRoot.addMain(filename, documentType, idno, assignDepart, productId, isApplyLoanCC, IsPreAudit, casePriority, imgType);
                // } else {
                //     if (StringUtils.equals("C001", documentType)) {
                //         orbitRoot.addSub(filename, imgType);
                //     } else {
                //         orbitRoot.addSub(filename, documentType, idno, imgType);
                //     }
                // }

                fileGroups.get(imgType).add(uploadFile);
            } // end for

            // 6.影像XML中圖檔類型排序順序:I001、I002、I003、I999
            List<UploadFileDto> list = new ArrayList<>();
            list.addAll(fileGroups.get(I001));
            list.addAll(fileGroups.get(I002));
            list.addAll(fileGroups.get(I003));
            list.addAll(fileGroups.get(I999));
            for (int i = 0; i < list.size(); i++) { // loop list
                UploadFileDto uploadFile = list.get(i);
                String filename = uploadFile.getFilename();
                String imgType = uploadFile.getImgType();
                if (StringUtils.equals(I001, imgType)) {
                    orbitRoot.addMain(filename, documentType, idno, assignDepart, productId, isApplyLoanCC, IsPreAudit, casePriority, imgType);
                } else {
                    if (StringUtils.equals("C001", documentType)) {
                        orbitRoot.addSub(filename, imgType);
                    } else {
                        orbitRoot.addSub(filename, documentType, idno, imgType);
                    }
                }
            } // end loop list
        }
    } // end processMainData

    /**
     * 第二次開始產生申請書PDF(含ID財力圖檔)
     */
    public void uploadCaseDataAndKycAndPhotoAddon() throws Exception {

        // [Status JobStatus]=01 040
        List<CaseData> caselist = caseDataDao.queryWaitPhotoCaseAddon();
        if (caselist.size() > 0) {
					
			logger.info("######## [排程] uploadCaseDataAndKycAndPhotoAddon(UPL-4) 有 " + caselist.size() + "  筆處理中.....");

			int cntOK = 0;
            // loop caselist
            for (int i = 0; i < caselist.size(); i++) {

                String caseNo = caselist.get(i).getCaseNo();

                // @setup caseData.JobStatus 02 200
                caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_02_JobStatus_200);

                // @call 15. previewCasePDFFinaleHtml
                pdfService.pdfGeneratorCaseFinalePDF(caseNo);
                
                List<PDFCaseDataSmall> sendcaselist = caseDataDao.queryWaitPhotoCaseByUniqId(caseNo);
                PDFCaseDataSmall small  = (sendcaselist != null && sendcaselist.size() > 0 ? sendcaselist.get(0) : null);
                if (small != null) {
                    // TODO: 狀態修正 02 系列
                    uploadCaseDataToOrbit(sendcaselist);
                    // @setup caseData.SubStatus 12 000
                    caseDataDao.updateSubStatusByCaseNo(caseNo, CASE_STATUS_12_SubStatus_000);
                }

                updateApsFastCase(caseNo);
            }
        }
    } // end uploadCaseDataAndKycAndPhotoAddon

    public void updateApsFastCase(String uniqId) throws Exception {
        CaseData caseData = caseDataDao.findByCaseNo(uniqId);
        Map<String, Boolean> completeMap = caseDataService.makeCompleteMap(uniqId);
        String cComplete = caseDataService.cComplete(completeMap); // Y-完整, N-不完整

        boolean checkIdcard = completeMap.get(CHECK_idcard); // 檢核ID
        boolean checkFinpf = completeMap.get(CHECK_finpf); // 檢核財力
        String errFlag = StringUtils.equals("N", cComplete) ? "Y" : "N"; // 異常=Y 正常=N
        String mgReadyFlag = StringUtils.equals("2", caseData.getSubcaseType()) ? "Y" : "N"; // 根據主表subcaseType欄位，值=2帶入Y，其他帶入N
        String docFlag = checkIdcard && checkFinpf ? "Y" : "N"; // 申請書/ID/財力完整者(Y/N)
        try {
            kgiService.call_UPDATE_APS_FAST_CASE(uniqId, caseData.getCaseNoWeb(), errFlag, mgReadyFlag, docFlag);
        } catch(Exception e) {
            logger.error("Fail to UPDATE_APS_FAST_CASE.");
        }
    }
    
    /**
     * 第一次開始產生申請書PDF
     * 1. find CaseData
     * 2. find CaseDoc
     * 
     */
    public void uploadCaseDataToOrbit() throws Exception {
        // 撈取尚未進件的案件資料
		// 找 CASE_STATUS_01_JobStatus_010 案件
        List<PDFCaseDataSmall> sendcaselist = caseDataDao.queryWaitPhotoCase();
		if (sendcaselist.size() > 0) {
			logger.info("######## [排程] uploadCaseDataToOrbit(UPL-1) 有 " + sendcaselist.size() + "  筆處理中.....");
		}			
        uploadCaseDataToOrbit(sendcaselist);
    }

    public void uploadIdcardOrFinpfToOrbit() throws Exception {
        List<PDFCaseDataSmall> sendcaselist = caseDataDao.queryUploadIdcardOrFinpf();
        for (PDFCaseDataSmall small : sendcaselist) {
            String caseNo = small.getCaseNo();
            // @call 15. previewCasePDFFinaleHtml
            pdfService.pdfGeneratorCaseFinalePDF(caseNo);	//gen PDF and save to Table
        }
		if (sendcaselist.size() > 0) {
			logger.info("######## [排程] uploadIdcardOrFinpfToOrbit(UPL-2/3) 有 " + sendcaselist.size() + "  筆處理中.....");
		}		
        uploadCaseDataToOrbit(sendcaselist);
    }

    /**
     * 開始產生申請書PDF
     */
    public void uploadCaseDataToOrbit(List<PDFCaseDataSmall> sendcaselist) throws Exception {

        if (sendcaselist.size() > 0) {
			
            PDFCaseDataShoot shoot = new PDFCaseDataShoot();
            PDFHelper pdfHelper = new PDFHelper();
            shoot.setCntOK(0);
            for (int i = 0; i < sendcaselist.size(); i++) {
                shoot.setSendimageOK(false); // 測試開true
                shoot.setFileList(new ArrayList<>());
                shoot.setErr(0);
	
                String idno = sendcaselist.get(i).getIdno();
                String caseNo = sendcaselist.get(i).getCaseNo();
                String name = sendcaselist.get(i).getCustomer_name();
                String Occupation = sendcaselist.get(i).getOccupation();
                String title = sendcaselist.get(i).getTitle();
                String IsPreAudit = sendcaselist.get(i).getIsPreAudit();
                String productId = sendcaselist.get(i).getProductId();
				
				logger.info("######## [排程] uploadCaseDataToOrbit(Step-1) 處理 " + caseNo + " 中.....");
				
				
				//01-010--->01-200-->01-210-->01-220

                try {

                    // @setup caseData.JobStatus 01 200
                    //caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_01_JobStatus_200);
					caseDataDao.updateJobStatusOnly(caseNo,"200");
                    // err = 1;
                    // 1. 檢核是否為快速通關(FastPass)
                    makeFastPass(shoot, caseNo);
                    if (shoot.isFastPass()) {
                        continue;
                    }
                    // @setup caseData.JobStatus 01 210
                    //caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_01_JobStatus_210);
					caseDataDao.updateJobStatusOnly(caseNo,"210");
                    // err = 2;
                    // 2. 上傳申請書到FTP,申請書檔案放在第一個
                    uploadCaseDoc(shoot, pdfHelper, caseNo, idno);
                    if (shoot.isGoReturn()) {
                        // @setup caseData.JobStatus 01 261
                        //caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_01_JobStatus_261);
						caseDataDao.updateJobStatusOnly(caseNo,"261");
						logger.info("!!!!!!!! " + caseNo + " 上傳申請書到FTP發生異常! (01-261)");
                        return;
                    }

                    // 3. 新增aml圖檔
                    uploadAMLImage(shoot, caseNo, idno, name, Occupation, title);

                    // @setup caseData.JobStatus 01 220
                    //caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_01_JobStatus_220);
					caseDataDao.updateJobStatusOnly(caseNo,"220");
					// 4-1.把此案所有USERPHOTO中圖檔，狀態改為4，下方兩步驟才會缺檔
					int photoCnt = userPhotoDao.setAllPhotoToWaitUpload(caseNo, USERPHOTO_STATUS_WAIT_UPLOAD_4);
					logger.info("######## " + caseNo + " 共有圖檔數 : " + photoCnt);

                    // 4-2. 案件上傳[ID]圖檔到FTP，上傳身份證正反
                    uploadIdcard(shoot, caseNo, idno);

                    // 4-3. 案件上傳[財力]圖檔到FTP，財力證明
                    uploadFinancial(shoot, caseNo, idno);
                    
                    if (shoot.isSendimageOK()) {

                        // err = 3;
                        // 5. 上傳 XML 到影像系統
                        uploadXmlFile(shoot, caseNo, idno, productId); // generate branchId

                        // @setup caseData.JobStatus 01 230
                        //caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_01_JobStatus_230);
						caseDataDao.updateJobStatusOnly(caseNo,"230");
                        // err = 4;
                        // 6. 檢核 於信用卡以及貸款送件之後 判斷此案件是否黑名單(AML)或是否高風險職業類別,如果是 在發送給TM時 增加註記
                        makeApplyLoanCMBProgress(shoot, caseNo, idno, name, IsPreAudit);

                        // @setup caseData.JobStatus 01 240
                        //caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_01_JobStatus_240);
						caseDataDao.updateJobStatusOnly(caseNo,"240");
                        shoot.setErr(5);
                        // 6.將案件狀態標註為  送件完成
                        shoot.setCntOK(shoot.getCntOK() + 1);
                        // 2020-11-11 Charles 註解掉的
                        // caseDataDao.updateStatusbyCaseNo(caseNo, PlContractConst.CASE_STATUS_SUBMIT_SUCCESS);
                        // @setup caseData.JobStatus 01 250
                        //caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_01_JobStatus_250);
						caseDataDao.updateJobStatusOnly(caseNo,"250");
                        // @setup caseData.JobStatus 01 260
                        //caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_01_JobStatus_260);
						caseDataDao.updateJobStatusOnly(caseNo,"260");
                    }
                } catch (Exception e) {
                    logger.error("!!!!!!!! " + caseNo + " Fail to uploadCaseDataToOrbit " , e);
                    switch (shoot.getErr()) {
                        case 1:
                            // @setup caseData.JobStatus 01 211
                            //caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_01_JobStatus_211);
							caseDataDao.updateJobStatusOnly(caseNo,"211");
                            jobLogService.AddSendDataConnOutLog("一般進件案件".concat(caseNo).concat("執行送件失敗(連線失敗)"));
                            break;
                        case 2:
                            // @setup caseData.JobStatus 01 221
                            //caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_01_JobStatus_221);
							caseDataDao.updateJobStatusOnly(caseNo,"221");
                            jobLogService.AddSendDataConnOutLog("一般進件案件".concat(caseNo).concat("圖片上傳ftp失敗"));
                            break;
                        case 3:
                            // @setup caseData.JobStatus 01 231
                            //caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_01_JobStatus_231);
							caseDataDao.updateJobStatusOnly(caseNo,"231");
                            jobLogService.AddSendDataConnOutLog("一般進件案件".concat(caseNo).concat("XML上傳失敗"));
                            break;
                        case 4:
                            // @setup caseData.JobStatus 01 241
                            //caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_01_JobStatus_241);
							caseDataDao.updateJobStatusOnly(caseNo,"241");
                            jobLogService.AddSendDataConnOutLog("一般進件案件".concat(caseNo).concat("呼叫影像WebService失敗"));
                            break;
                        case 5:
                            // @setup caseData.JobStatus 01 251
                            //caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_01_JobStatus_251);
							caseDataDao.updateJobStatusOnly(caseNo,"251");
                            jobLogService.AddSendDataConnOutLog("一般進件案件".concat(caseNo).concat("更新送件完成失敗"));
                            break;
                    }
                    //jobLogService.AddSendDataErrorLog("一般進件案件".concat(e.toString()));
                    // 當連不到ws時，讓狀態直接完成，不影響後面流程。
                    if (shoot.getErr() == 4) {
                        // 2020-11-11 Charles 註解掉的
                        // caseDataDao.updateStatusbyCaseNo(caseNo, PlContractConst.CASE_STATUS_SUBMIT_SUCCESS);
                        // @setup caseData.JobStatus 01 260
                        //caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_01_JobStatus_260);
						caseDataDao.updateJobStatusOnly(caseNo,"260");
                    } else {
                        // 2020-11-11 Charles 註解掉的
                        // caseDataDao.updateStatusbyCaseNo(caseNo, PlContractConst.CASE_STATUS_SUBMIT_FAIL);
                    }
                }
            } // end for loop sendcase(申請信用貸款一般進件案件)
            jobLogService.AddSendDataInfoLog("信貸一般進件案件打包後送本次處理總共：" + sendcaselist.size() + "筆，處理成功計有：" + shoot.getCntOK()
                    + "筆，處理失敗計有" + (sendcaselist.size() - shoot.getCntOK()) + "筆。");
        }

    } // end uploadCaseDataToOrbit

    /** 
     * 1. 檢核是否為快速通關(FastPass)
     */
    private void makeFastPass(PDFCaseDataShoot shoot, String caseNo) {
        shoot.setErr(1);

        //判斷快速通關條件是否完成
        FastPass fp = fastPassDao.Read(new FastPass(caseNo));
        if (fp == null || fp.getAML().equals("0") || fp.getAML().equals("1")) {
            //需做過AML才能進影像
            //取不到快速通關資料，或是AML為零(尚未做過AML)，跳到下一個貸款案件
//          System.out.println("AML尚未完成，CaseNO : " + caseNo);
            shoot.setFastPass(true);;
        }

        shoot.setDgtType(fp.getDGTType());
        shoot.setAmlQueryTime(fp.getUpdateTime().toString().substring(0, 10));
        shoot.setAmlStatus(fp.getAML());
    }

    /** 
     * 2. 上傳申請書到FTP,申請書檔案放在第一個
     */
    private void uploadCaseDoc(PDFCaseDataShoot shoot, PDFHelper pdfHelper, String caseNo, String idno)
            throws Exception {
        List<Map<String, Object>> caseDocs = caseDataDao.queryCaseDoc(caseNo, idno);
        List<Integer> uploadList = new ArrayList<>();
        if (caseDocs.size() > 0) {
            shoot.setErr(2);

            Map<String, Object> caseDoc = caseDocs.get(0);
            byte[] photobyte = pdfHelper.DecryptPDF(
                    java.util.Base64.getDecoder().decode(caseDoc.get("PdfContent").toString()), idno);
            List<byte[]> listpdfphoto = pdfHelper.getImageList(photobyte);

            for (int j = 0; j < listpdfphoto.size(); j++) {
                // 申請書--組檔名
                String filename = caseNo.concat(idno).concat("_" + String.format("%03d", j + 1))
                        .concat(".jpg");
                uploadList.add(j + 1);
                
                byte[] image = listpdfphoto.get(j);

                // IMAGE檔 - @export 儲存至檔案系統
                if (globalConfig.isPDF_Export()) {
                    ExportUtil.export(global.PdfDirectoryPath + "/" + filename, image);
                }

                // 上傳到ftp
                shoot.setSendimageOK(ftpservice.uploadImgBase64File(filename, image));

                if (j == 0 ) { // I001 貸款申請書
                    shoot.getFileList().add(new UploadFileDto(I001, filename));
                } else { // I999 KYC表、同一關係人表
                    shoot.getFileList().add(new UploadFileDto(I999, filename));
                }

                // 上傳ftp失敗，拋出例外
                if (!shoot.isSendimageOK()) {
                    throw new RuntimeException("上傳ftp失敗!!");
                }
            }
        } else {
            jobLogService.AddSendDataChkfailLog("Casno:" + caseNo + " 查無申請書無法上傳");
            shoot.setGoReturn(true);
        }
    }

    /**
     * 3. 新增aml圖檔
     */
    private void uploadAMLImage(PDFCaseDataShoot shoot, String caseNo, String idno, String name, String Occupation, String title)
            throws Exception {
        String occupation = dropdownDataDao.getDataNameByNameAndDataKey("occupation", Occupation);


        CaseData caseData = caseDataDao.findByCaseNo(caseNo);
        
		String idCardDate = "";
		String idCardLocationName = "";
		String idCardLocationKey = "";
		String idCardLocation = "";
        String idCardCRecord = "";
		String idCardQryResult = "";
		String idCardQryDate = "";
		
		idCardDate = caseData.getIdCardDate();

		if (idCardDate==null){
			idCardDate = "-尚未上傳身分證-";
			idCardLocationName = "";
			idCardLocationKey = "";
			idCardLocation = "-尚未上傳身分證-";
			idCardCRecord = "-尚未上傳身分證-";
			idCardQryResult = "-尚未查詢-";
			idCardQryDate = "-尚未查詢-";

			
		}else if (idCardDate.equals("")){
			idCardDate = "-尚未上傳身分證-";
			idCardLocationName = "";
			idCardLocationKey = "";
			idCardLocation = "-尚未上傳身分證-";
			idCardCRecord = "-尚未上傳身分證-";
			idCardQryResult = "-尚未查詢-";
			idCardQryDate = "-尚未查詢-";
			
		}else{
			idCardDate = StringUtils.substring(idCardDate, 0, 3) + "/" + StringUtils.substring(idCardDate, 3, 5) + "/" + StringUtils.substring(idCardDate, 5, 7);
			idCardLocationName = caseData.getIdCardLocation();
			idCardLocationKey = dropdownService.findDataKey_idlocation_new(idCardLocationName);
			idCardLocation = idCardLocationKey + "-" + idCardLocationName;
			idCardQryDate = caseData.getSendCustInfoTime();
			
			idCardCRecord = caseData.getIdCardCRecord();
			if (StringUtils.equals("1", idCardCRecord)) {
				idCardCRecord = "新發";
			} else if (StringUtils.equals("2", idCardCRecord)) {
				idCardCRecord = "補發";
			} else if (StringUtils.equals("3", idCardCRecord)) {
				idCardCRecord = "換發";
			}	
			
			idCardQryResult = caseData.getHasQryIdInfo();
			logger.info("######## getHasQryIdInfo=" + idCardQryResult);
			if (StringUtils.equals("01", idCardQryResult)) {
				idCardQryResult = "(O)資料相符";
			} else {
				idCardQryResult = "(X)資料不符";
			}
			
		}
        
		
        String AMLImgbase64 = createAMLImageBase64(idno, name, shoot.getAmlQueryTime(), shoot.getAmlStatus(), occupation, title, idCardDate, idCardLocation, idCardCRecord, idCardQryResult,idCardQryDate);
		
		if (!AMLImgbase64.equals("")) {
            // 1轉檔
            ftpservice.Base64ToInputStream(AMLImgbase64);
            // 2先組檔名
            String filename = caseNo;
            filename = filename.concat("_AML_" + String.format("%03d", shoot.getFileList().size() + 1));
            filename += ".jpg";

            byte[] image = IOUtils.toByteArray(ftpservice.inputStream);

            // IMAGE檔 - @export 儲存至檔案系統
            if (globalConfig.isPDF_Export()) {
                ExportUtil.export(global.PdfDirectoryPath + "/" + filename, image);
            }

            // 4.3上傳到ftp
            shoot.setSendimageOK(ftpservice.uploadImgBase64File(filename, image));
            // I999 AML表
            shoot.getFileList().add(new UploadFileDto(I999, filename));
            //orbitRoot.addSub(filename);
			
			logger.info("######## " + caseNo + " 產生AMLL圖檔成功!");
			
        } else {
            jobLogService.AddSendDataChkfailLog("一般進件案件".concat(caseNo).concat("無法產生AML圖檔"));
			logger.info("XXXXXXXX " + caseNo + " 產生AMLL圖檔失敗!!!");
        }
    }

    /**
     * 4. 案件上傳圖檔到FTP，上傳身份證正反
     */
    private void uploadIdcard(PDFCaseDataShoot shoot, String caseNo, String idno) throws Exception {
        List<UserPhoto> idcards = userPhotoDao.findIdcardOnline(caseNo, USERPHOTO_STATUS_WAIT_UPLOAD_4, USERPHOTO_ProdType_2); // FIXME:
		
		logger.info("######## " + caseNo + " 有ID圖檔數 : " + idcards.size());
		
        for (int j = 0; j < idcards.size(); j++) {
            UserPhoto up = idcards.get(j);
            //3.1轉檔
            ftpservice.fileToInputStream(up.getImageBig());
            //3.2先組檔名
            String filename = caseNo;
            filename = caseNo.concat(idno).concat("_IDCARD_" + String.format("%03d", j + 1)).concat(".jpg");

            // IMAGE檔 - @export 儲存至檔案系統
            if (globalConfig.isPDF_Export()) {
                ExportUtil.export(global.PdfDirectoryPath + "/" + filename, up.getImageBig());
            }

            //上傳到ftp
            shoot.setSendimageOK(ftpservice.uploadImgBase64File(filename));

            if (!shoot.isSendimageOK()) { // failed
                up.setStatus(USERPHOTO_STATUS_UPLOAD_FAIL);
                userPhotoDao.Update(up);
            } else { // ok
                up.setStatus(USERPHOTO_STATUS_UPLOAD_SUCCESS);
                userPhotoDao.Update(up);
                shoot.getFileList().add(new UploadFileDto(I002, filename));
            }
        } // for
    } // end uploadIdcard

    /**
     * 4. 案件上傳圖檔到FTP，財力證明
     */
    private void uploadFinancial(PDFCaseDataShoot shoot, String caseNo, String idno) throws Exception {
        List<UserPhoto> fins = userPhotoDao.findFinancialOnline(caseNo, USERPHOTO_STATUS_WAIT_UPLOAD_4, USERPHOTO_ProdType_2);
		
		logger.info("######## " + caseNo + " 有財力圖檔數 : " + fins.size());
		
        for (int j = 0; j < fins.size(); j++) {
            UserPhoto up = fins.get(j);
            //3.1轉檔
            ftpservice.fileToInputStream(up.getImageBig());
            //3.2先組檔名
            String filename = caseNo;
            filename = caseNo.concat(idno).concat("_FIN_" + String.format("%03d", j + 1)).concat(".jpg");

            // IMAGE檔 - @export 儲存至檔案系統
            if (globalConfig.isPDF_Export()) {
                ExportUtil.export(global.PdfDirectoryPath + "/" + filename, up.getImageBig());
            }

            //上傳到ftp
            shoot.setSendimageOK(ftpservice.uploadImgBase64File(filename));

            if (!shoot.isSendimageOK()) { // failed
                up.setStatus(USERPHOTO_STATUS_UPLOAD_FAIL);
                userPhotoDao.Update(up);
            } else { // ok
                up.setStatus(USERPHOTO_STATUS_UPLOAD_SUCCESS);
                userPhotoDao.Update(up);
                shoot.getFileList().add(new UploadFileDto(I003, filename));
            }
        } // for
    } // end uploadFinancial

    /**
     * 5. 上傳 XML 到影像系統
     */
    private void uploadXmlFile(PDFCaseDataShoot shoot, String caseNo, String idno, String productId) throws Exception {
        shoot.setErr(3);

        QR_ChannelDepartList channelDepart = channelDepartService.getLoanChannelDepart(caseNo);

        // 產圖檔的XML檔將XML上傳到FTP
        String branchId = buildOrbitData(caseNo, idno, shoot.getFileList(), shoot.getDgtType(), channelDepart, shoot.getAmlStatus(), productId);
        shoot.setBranchId(branchId);
    }

    /** 
     * 6. 檢核 於信用卡以及貸款送件之後 判斷此案件是否黑名單(AML)或是否高風險職業類別,如果是 在發送給TM時 增加註記
     */
    private void makeApplyLoanCMBProgress(PDFCaseDataShoot shoot, String caseNo, String idno, String name, String IsPreAudit)
            throws Exception {
        shoot.setErr(4);
        JSONObject audit = new JSONObject();
        String snedCMD = "0";
        // 組通知時，非預核件檢查有無APS額度
        String hasdata = chtDao.hasdata(caseNo);
        int CreditLine = 0;
        if (IsPreAudit.equals("2")) {
            List<Map<String, Object>> result = auditDataDao.getAuditData(caseNo);
            if (result.size() > 0) {
                audit = JSONObject.fromObject(result.get(0));
            }
            CreditLine = audit.optInt("CreditLine", 0);
            // 預核進件--呼叫訊息平台

        }
        //於信用卡以及貸款送件之後 判斷此案件是否黑名單(AML)或是否高風險職業類別,如果是 在發送給TM時 增加註記
        boolean isApplyLoanCC = caseDataDao.isApplyLoanCC(caseNo);
        String plusMessage = amlService.checkIsHighRisk(caseNo, (isApplyLoanCC) ? "applyccLoan-loan" : "applyLoan");
        MailHistory mailHistory = soapService.getApplyLoanCMBProgress(idno, name, hasdata, CreditLine, plusMessage, shoot.getBranchId());
        mailHistory.setUniqId(caseNo);
        mailHistoryDao.Create(mailHistory);
    }

    // /**
    //  * 舊系統的圖檔補件程式，Charles: 2021-01-19 棄用。
    //  * 
    //  * @deprecated Charles:舊系統程式碼，不再使用。
    //  */
    /*public void uploadAddPhotoOrbitData() throws Exception {
        //取得狀態為待處理的addPhotoStatus
//        List<Map<String, Object>> caselist = caseDataDao.queryPhotoDataByStatus(PlContractConst.PHOTO_STATUS_WAIT);
        //1227 改成取USERPHOTO
        List<UserPhoto> userPhotos = userPhotoDao.findByStatusAndProdType(USERPHOTO_STATUS_WAIT_UPLOAD_4, USERPHOTO_ProdType_2);
        if (userPhotos == null || userPhotos.size() == 0) {
//            System.out.println("buildAddPhotoOrbitData no Data");
            return;
        }
        // logger.info("### userPhoto.size()=" + userPhotos.size());
        for (int i = 0; i < userPhotos.size(); i++) {
            // int noAddPhotocnt = 0;
            // logger.info("### userPhoto=" + new Gson().toJson(userPhotos.get(i)));
            updateLoanAddPhotoStatus(userPhotos.get(i));
        }
    }*/

    // /**
    //  * @deprecated Charles:舊系統程式碼，不再使用。
    //  */
    /*public void updateLoanAddPhotoStatus(UserPhoto userPhoto) {

        String caseno = userPhoto.getUniqId();

        // 先判斷是線上還線下
        if (userPhoto.getOnline().equals("1")) {
            // 線上件
            try {
                // 檢查casedata status
                // 取得最新的一筆CaseData且是送出申請出成功的
    //            List<Map<String, Object>> casebyidlist = caseDataDao.query1CaseDatabyIdnoAndStatus(idno,
    //                    PlContractConst.CASE_STATUS_SUBMIT_SUCCESS);
                //改成取userphoto , 帶的是uniqId, 直接取得案件
                // logger.info("### caseno=" + caseno);
                CaseData caseData = caseDataDao.findByCaseNo(caseno);
                String idno = caseData.getIdno();
                String cname = caseData.getCustomer_name();
                uploadonlineCase(idno, caseno, PlContractConst.PHOTO_PRODUCTTYPE_LOAN, userPhoto, cname);
                // 補件案件 呼叫kgi訊息平台 1:成功
                // QR_ChannelDepartList channelDepart = this.channelDepartService.getLoanChannelDepart(caseno);
                // String branchID = this.channelDepartService.getAssignDepart(channelDepart, "02", "");
                // MailHistory addPhotoCMBProgress = soapService.getAddPhotoCMBProgress(idno, cname, branchID);
                // addPhotoCMBProgress.setUniqId(caseno);
                // mailHistoryDao.Create(addPhotoCMBProgress);

                // 將送上ftp的檔案註記為已送到影像中心
//                    caseDataDao.updatePhotoStatus(idno, PlContractConst.PHOTO_STATUS_SUBMIT, PlContractConst.PHOTO_ONLINE_STATUS_ONLINE, PlContractConst.PHOTO_PRODUCTTYPE_LOAN);
            } catch (Exception e) {
                jobLogService.AddSendDataErrorLog("補件案件(E) ".concat(caseno).concat(" " + e.toString()));
                // caseDataDao.updatePhotoStatus(idno, PlContractConst.PHOTO_STATUS_SUBMIT);
            }
            // for (int j=0; j < casebyidlist.size();j++)
            // {
            // //為未進件不能當補件案送件
            // if (casebyidlist.get(j).get("Status").equals(PlContractConst.CASE_STATUS_WRITING)
            // ||
            // casebyidlist.get(j).get("Status").equals(PlContractConst.CASE_STATUS_WRITE_SUCCESS)
            // ||
            // casebyidlist.get(j).get("Status").equals(PlContractConst.CASE_STATUS_SUBMITTING)
            // ||
            // casebyidlist.get(j).get("Status").equals(PlContractConst.CASE_STATUS_SUBMIT_ERROR))
            // {
            // noAddPhotocnt++;
            // }else {
            // cname = casebyidlist.get(j).get("customer_name").toString();
            // caseno = casebyidlist.get(j).get("CaseNo").toString();
            // }
            // }
            // if(noAddPhotocnt==0) {
            // try {
            // uploadonlineCase(idno,caseno);
            // //補件案件 呼叫kgi訊息平台 1:成功
            // String snedCMD = soapService.getAddPhotoCMBProgress(idno,cname);
            // if(snedCMD.equals("1")) {
            // //將送上ftp的檔案註記為已送到影像中心
            // caseDataDao.updatePhotoStatus(idno, PlContractConst.PHOTO_STATUS_SUBMIT);
            // }else {
            // jobLogService.AddSendDataConnOutLog("補件案件".concat(idno).concat("呼叫訊息平台
            // LOG:").concat(snedCMD));
            // caseDataDao.updatePhotoStatus(idno, PlContractConst.PHOTO_STATUS_SUBMIT);
            // }
            // } catch (Exception e) {
            // jobLogService.AddSendDataErrorLog("補件案件".concat(idno).concat("
            // "+e.toString()));
            // //caseDataDao.updatePhotoStatus(idno, PlContractConst.PHOTO_STATUS_SUBMIT);
            // }
            // }
        } else {
            String unitId = userPhoto.getUnitId();
            String idno = userPhoto.getUniqId();
            try {
                // 線下件
                uploadofflineCase(userPhoto);
                // 補件案件 呼叫kgi訊息平台 1:成功
                // MailHistory addPhotoCMBProgress = soapService.getAddPhotoCMBProgress(idno, "", unitId);
                // addPhotoCMBProgress.setUniqId(unitId);
                // mailHistoryDao.Create(addPhotoCMBProgress);

//                caseDataDao.updatePhotoStatus(idno, PlContractConst.PHOTO_STATUS_SUBMIT, PlContractConst.PHOTO_ONLINE_STATUS_OFFLINE, PlContractConst.PHOTO_PRODUCTTYPE_LOAN);
            } catch (Exception e) {
                jobLogService.AddSendDataErrorLog("補件案件(B) ".concat(idno).concat(e.toString()));
//                 caseDataDao.updatePhotoStatus(idno, PlContractConst.PHOTO_STATUS_SUBMIT);
            }
        }
    }*/

    // /** 
    //  * 處理線上件補件
    //  * 
    //  * @deprecated Charles:舊系統程式碼，不再使用。
    //  */
    /*public void uploadonlineCase(String idno, String caseNo, String prodid, UserPhoto up, String cname) throws Exception {
        
        // List<Map<String, Object>> caselist = caseDataDao.queryDatabyCaseNo(caseNo);
        List<Map<String, Object>> caselist = caseDataDao.queryWaitPhotoCaseAddon(caseNo);
        if (caselist.size() > 0) { // Charles: 理論上用caseNo查詢CaseData只會有1筆，事實上也只會有一筆，唉…舊code真是…
            int cntOK = 0;
            //UserPhoto up = new UserPhoto();
            for (int i = 0; i < caselist.size(); i++) {

                // @setup caseData.JobStatus 02 200
                caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_02_JobStatus_200);

                boolean sendimageOK = false;
                try {
                    //補件流程改為不上傳申請書
                    //3.上傳申請書到FTP
                    // List<Map<String, Object>> caseDocs = caseDataDao.queryCaseDoc(caselist.get(i).get("CaseNo").toString(), idno);
                    
                    // if(caseDocs.size()>0) {
                        
                    //         //轉檔
                    //         ftpservice.Base64ToInputStream(caseDocs.get(0).get("Image").toString());
                    //         //先組檔名
                    //         String filename = caselist.get(i).get("case_no_est").toString();
                    //         filename = filename.concat(idno);
                    //         filename = filename.concat("_"+String.format("%03d", 1));
                    //         //上傳到ftp
                    //         sendimageOK = ftpservice.uploadImgBase64File(filename + ".jpg");
                    // }
                    //4.上傳上傳圖檔到FTP
                    //改成取USERPHOTO
                    // List<Map<String, Object>> casephotolist = caseDataDao.queryPhotoDataByCase(idno, PlContractConst.PHOTO_ONLINE_STATUS_ONLINE, prodid);

                    //up.setUniqId(caseNo);
                    // up.setOnline(String.valueOf(PlContractConst.PHOTO_ONLINE_STATUS_ONLINE));
                    List<String> fileNameArray = new ArrayList<>();
                    List<UserPhoto> query = userPhotoDao.Query(up);
                    String phototime = DateUtil.GetDateFormatString("HHmmssS");
                    for (int j = 0; j < query.size(); j++) {
                        up = query.get(j);
                        //3.1轉檔
                        ftpservice.fileToInputStream(up.getImageBig());
                        //3.2先組檔名
                        String filename = caselist.get(i).get("CaseNo").toString();
                        filename = filename.concat(idno).concat(phototime).concat("_P" + String.format("%03d", j + 1)).concat(".jpg");

                        // IMAGE檔 - @export 儲存至檔案系統
                        if (globalConfig.isPDF_Export()) {
                            ExportUtil.export(global.PdfDirectoryPath + "/" + filename, up.getImageBig());
                        }

                        //上傳到ftp
                        sendimageOK = ftpservice.uploadImgBase64File(filename);

                        //上傳ftp失敗，拋出例外
                        //1227 上傳FTP失敗，改成繼續傳下一張圖，失敗圖狀態改成9
                        if (!sendimageOK) {
                            
                            // throw new RuntimeException("圖檔上傳ftp失敗!!");
                            up.setStatus(USERPHOTO_STATUS_UPLOAD_FAIL);
                            userPhotoDao.Update(up);

                            // @setup caseData.JobStatus 02 221
                            caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_02_JobStatus_221);
                        } else {
                            
                            fileNameArray.add(filename);
                            up.setStatus(USERPHOTO_STATUS_UPLOAD_SUCCESS);
                            userPhotoDao.Update(up);

                            // @setup caseData.JobStatus 02 220
                            caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_02_JobStatus_220);
                        }
                    } // end if

                    if (fileNameArray.size() > 0) {
                        //5. 產圖檔的XML檔將XML上傳到FTP
                        OrbitRoot orbitRoot = new OrbitRoot();
                        //補件流程改為不上傳申請書 這段拿掉
                        //orbitRoot.add(caselist.get(i).get("case_no_est").toString().concat(idno).concat("_001").concat(".jpg"), "C002", idno);

                        // check is applyLoanCC
                        boolean isApplyLoanCC = caseDataDao.isApplyLoanCC(caseNo);
                        QR_ChannelDepartList channelDepart = channelDepartService.getLoanChannelDepart(caseNo);
                        processMainData(caselist, orbitRoot, "C002", fileNameArray, "", isApplyLoanCC, channelDepart, null);
                        String fileName = DateUtil.GetDateFormatString("yyyyMMddHHmmssS").concat(caselist.get(i).get("CaseNo").toString()).concat(idno) + ".xml";
                        //將資料上傳到影像系統
                        StringBuilder sb = new StringBuilder();
                        orbitRoot.toXML(sb);
                        InputStream in = IOUtils.toInputStream(sb.toString(), "UTF-8");

                        // XML檔 - @export 儲存至檔案系統
                        if (globalConfig.isPDF_Export()) {
                            ExportUtil.export(global.PdfDirectoryPath + "/" + fileName, sb.toString().getBytes("UTF-8"));
                        }

                        if (!ftpservice.uploadImgXml(fileName, in)) {

                            // @setup caseData.JobStatus 02 231
                            caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_02_JobStatus_231);

                            throw new RuntimeException("XML檔上傳ftp失敗!!");
                        } else {

                            // @setup caseData.JobStatus 02 230
                            caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_02_JobStatus_230);

                        }
                        // 完成，更新 CaseData 的 AddPhotoFlag=1 已補件
                        caseDataDao.isAddPhoto(caseNo);

                        jobLogService.AddSendDataInfoLog("信貸線上件補件成功 CaseNo:" + caseNo + ", XML-Name:" + fileName + ", XML: " + sb.toString());

                        // @setup caseData.JobStatus 02 240
                        caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_02_JobStatus_240);

                        cntOK++;
                    } // end if
                } catch (Exception e) {
                    jobLogService.AddSendDataErrorLog("信貸線上件補件失敗 CaseNo:" + caseNo + ", ftp error: ".concat(e.toString()));
                    throw new RuntimeException("線上補件失敗");
                }

            } // end for
            
			if (cntOK > 0) {
                //jobLogService.AddSendDataInfoLog("信貸線上補件案件IDNO:" + idno + "打包後送程序本次處理成功");
                try {
                    String delete = globalConfig.AdditionalPhotoDelete();
                    if (delete.equals("1")) {
//                        caseDataDao.deletePhotoDataByCase(idno, PlContractConst.PHOTO_ONLINE_STATUS_ONLINE, prodid);
                        up = new UserPhoto();
                        up.setUniqId(caseNo);
                        up.setOnline(String.valueOf(PlContractConst.PHOTO_ONLINE_STATUS_ONLINE));
                        up.setStatus(PlContractConst.USERPHOTO_STATUS_UPLOAD_SUCCESS);
                        userPhotoDao.deleteUserPhotoByAllValue(up);
                    }
                    // @setup caseData.JobStatus 02 250
                    caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_02_JobStatus_250);
                    //jobLogService.AddSendDataInfoLog("信貸線上補件案件 IDNO:" + idno + "完成送件，刪除相片資料完成");
                } catch (Exception e) {
                    // @setup caseData.JobStatus 02 251
                    caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_02_JobStatus_251);
                    jobLogService.AddSendDataErrorLog("信貸線上補件案件 IDNO:" + idno + " 完成送件，刪除相片資料失敗");
                }
                // 
                QR_ChannelDepartList channelDepart = this.channelDepartService.getLoanChannelDepart(caseNo);
                String branchID = this.channelDepartService.getAssignDepart(channelDepart, "02", "");
                MailHistory addPhotoCMBProgress = soapService.getAddPhotoCMBProgress(idno, cname, branchID);
                addPhotoCMBProgress.setUniqId(caseNo);
                mailHistoryDao.Create(addPhotoCMBProgress);

                // @setup caseData.JobStatus 02 260
                caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_02_JobStatus_260);

                // @setup caseData.SubStatus 12 000
                caseDataDao.updateSubStatusByCaseNo(caseNo, CASE_STATUS_12_SubStatus_000);

            } else {
                // @setup caseData.JobStatus 02 261
                caseDataDao.updateJobStatusByCaseNo(caseNo, CASE_STATUS_02_JobStatus_261);
                jobLogService.AddSendDataInfoLog("信貸線上補件案件 Uniqid:" + caseNo + ", IDNO:" + idno + " 打包後送程序本次處理失敗");
            }
        }

    }*/

    // /**
    //  * 處理線下件補件
    //  * 
    //  * @deprecated Charles:舊系統程式碼，不再使用。
    //  */
    /*public void uploadofflineCase(UserPhoto userphoto) throws Exception {

//        List<Map<String, Object>> photolist = caseDataDao.queryPhotoDataByCase(idno, PlContractConst.PHOTO_ONLINE_STATUS_OFFLINE, prodid);
        List<UserPhoto> photolist = userPhotoDao.Query(userphoto);
        String idno = userphoto.getUniqId();
        String unitId = userphoto.getUnitId();
//        List<Integer> uploadList = new ArrayList<>();
        if (photolist.size() > 0) {
            int cntOK = 0;
            boolean sendimageOK = false;
            //日期先產好
//            String datestr = DateUtil.GetDateFormatString();
            List<String> fileNameList = new ArrayList<>();
            for (int j = 0; j < photolist.size(); j++) {
                userphoto = photolist.get(j);
                // 3.1轉檔
                ftpservice.fileToInputStream(userphoto.getImageBig());
                // 3.2先組檔名
                String filename = idno.concat(DateUtil.GetDateFormatString());
                filename = filename.concat("_" + String.format("%03d", j + 1)).concat(".jpg");

                // IMAGE檔 - @export 儲存至檔案系統
                if (globalConfig.isPDF_Export()) {
                    ExportUtil.export(global.PdfDirectoryPath + "/" + filename, userphoto.getImageBig());
                }

                // 上傳到ftp
                sendimageOK = ftpservice.uploadImgBase64File(filename);
                // 上傳ftp失敗，拋出例外
                if (!sendimageOK) {
                    userphoto.setStatus(PlContractConst.USERPHOTO_STATUS_UPLOAD_FAIL);
                    jobLogService.AddSendDataErrorLog("信貸線下件補件IDNO:" + idno + "圖片上傳ftp error:".concat("圖檔上傳ftp失敗!!"));
                } else {
//                    uploadList.add(j + 1);
                    fileNameList.add(filename);
                    userphoto.setStatus(PlContractConst.USERPHOTO_STATUS_UPLOAD_SUCCESS);
                }
                userPhotoDao.Update(userphoto);

            }

            if (fileNameList.size() > 0) {
                // 產圖檔的XML檔將XML上傳到FTP
                OrbitRoot orbitRoot = new OrbitRoot();

                InetAddress ip = null;
                try {
                    ip = InetAddress.getLocalHost();

                    // 補件改不塞uuid值
                    // orbitRoot.orbitHeader.from(ip.getHostAddress(), "0", idno, "9999", "webuser");
                    String subcaseTypeForC002OffLine = "0";
                    orbitRoot.orbitHeader.from(ip.getHostAddress(), subcaseTypeForC002OffLine, "", "9999", "webuser", "", "", "", "", "", "", "", "", "", "", "", "");
                } catch (UnknownHostException e) {
                    jobLogService.AddSendDataErrorLog("信貸線下件補件 error: ".concat(e.toString()));
                }
                // String datestr = DateUtil.GetDateFormatString();
                try {
                    for (int j = 0; j < fileNameList.size(); j++) {
//                        String row = idno.concat(datestr);
//                        row = row.concat("_" + String.format("%03d", j + 1));
                        // 判斷ROw第一筆 塞主要 XML Data
                        if (j == 0) {
                            //將產品編號預設值放入P035
                            orbitRoot.addMain(fileNameList.get(j), "C002", idno, unitId, "", false, "");
                        } else {
                            orbitRoot.addSub(fileNameList.get(j), "C002", idno);
                        }
                    }

                    String fileName = DateUtil.GetDateFormatString().concat(idno) + ".xml";
                    // 將資料上傳到影像系統
                    StringBuilder sb = new StringBuilder();
                    orbitRoot.toXML(sb);

                    jobLogService.AddSendDataInfoLog("信貸線下補件案件 IDNO:" + idno + ", XML-Name:" + fileName + ", XML: " + sb.toString());
                    InputStream in = IOUtils.toInputStream(sb.toString(), "UTF-8");

                    // XML檔 - @export 儲存至檔案系統
                    if (globalConfig.isPDF_Export()) {
                        ExportUtil.export(global.PdfDirectoryPath + "/" + fileName, sb.toString().getBytes("UTF-8"));
                    }

                    // 上傳ftp失敗，拋出例外
                    if (!ftpservice.uploadImgXml(fileName, in)) {
                        throw new RuntimeException("xml檔上傳ftp失敗!!");
                    }
                    cntOK++;
                } catch (Exception e) {
                    jobLogService.AddSendDataConnOutLog("信貸線下件補件案件 IDNO:" + idno + ", ftp err:".concat(e.toString()));
                }
            }
            if (cntOK > 0) {

                jobLogService.AddSendDataInfoLog("信貸線下補件案件 IDNO:" + idno + " 本次處理成功");
                try {
                    String delete = globalConfig.AdditionalPhotoDelete();
                    if (delete.equals("1")) {
//                        caseDataDao.deletePhotoDataByCase(idno, PlContractConst.PHOTO_ONLINE_STATUS_OFFLINE, userphoto.getProdType());
                        UserPhoto up = new UserPhoto();
                        up.setUniqId(idno);
                        up.setOnline(String.valueOf(PlContractConst.PHOTO_ONLINE_STATUS_OFFLINE));
                        up.setProdType(userphoto.getProdType());
                        userPhotoDao.deleteUserPhotoByAllValue(up);
                    }
                    //jobLogService.AddSendDataInfoLog("信貸線下補件案件 IDNO:" + idno + " 完成送件，刪除相片資料完成");
                } catch (Exception e) {
                    jobLogService.AddSendDataErrorLog("信貸線下補件案件 IDNO:" + idno + " 完成送件，刪除相片資料失敗");
                }
                MailHistory addPhotoCMBProgress = soapService.getAddPhotoCMBProgress(idno, "", unitId);
                addPhotoCMBProgress.setUniqId(unitId);
                mailHistoryDao.Create(addPhotoCMBProgress);
            } else {
                jobLogService.AddSendDataInfoLog("信貸線下補件案件 IDNO:" + idno + " 本次處理失敗");
            }
        }
    }*/

    /*public void uploadRejectCase() throws Exception {
        List<Map<String, Object>> Recaselist = caseDataDao.queryRejectCase();
        if (Recaselist.size() > 0) {
            System.out.println("上傳退件Job開始");
            PDFHelper pdfHelper = new PDFHelper();
            int cntOK = 0;
            for (int i = 0; i < Recaselist.size(); i++) {
                int err = 1;
                try {
                    boolean sendimageOK = false; //測試開true
                    String caseNo = Recaselist.get(i).get("CaseNo").toString();
                    String idno = Recaselist.get(i).get("idno").toString();
                    //1.拒貸件申請書檔案上傳影像系統
                    //產拒貸申請書
                    CaseRejectPDF pdf = new CaseRejectPDF(appContext);
                    pdf.init(caseNo);
                    pdfHelper.setPDFDocument(pdf);
                    byte[] pdfByte = pdfHelper.PdfProcess();

                    if (pdfByte.length > 0) {
                        err = 2;
                        // 將資料庫裡64base字串轉檔

                        // 申請書--組檔名
                        String filename = caseNo;
                        filename = filename.concat(idno);
                        filename = filename.concat("_" + String.format("%03d", 1));
                        filename = filename.concat(".jpg");
                        List<byte[]> imageList = pdfHelper.getImageList(pdfByte);
                        // 上傳到ftp
                        if (imageList.size() == 0) {
                            throw new RuntimeException(caseNo + "PDF轉圖片失敗");
                        }
                        sendimageOK = ftpservice.uploadImgBase64File(filename, imageList.get(0));
                        // 上傳ftp失敗，拋出例外
                        if (!sendimageOK) {
                            throw new RuntimeException("上傳ftp失敗!!");
                        }
                    } else {
                        jobLogService.AddSendDataChkfailLog(
                                "Casno:" + caseNo + " 拒貸件無法產出申請書上傳");
                        // 沒產好pdf需查問題狀態
                        // caseDataDao.updateStatusbyCaseNo(Recaselist.get(i).get("CaseNo").toString(),
                        // PlContractConst.CASE_STATUS_SUBMIT_ERROR);
                    }

                    if (sendimageOK) {
                        //拒貸件XML上傳到影像系統
                        err = 3;
                        //產圖檔的XML檔將XML上傳到FTP
                        OrbitRoot orbitRoot = new OrbitRoot();
                        InetAddress ip = null;
                        ip = InetAddress.getLocalHost();
                        String productId = Recaselist.get(i).get("ProductId").toString();
                        QR_ChannelDepartList channelDepart = channelDepartService.getLoanChannelDepart(caseNo);
                        orbitRoot.addRejectMain(caseNo.concat(idno).concat("_001").concat(".jpg"),
                                "C001", idno, productId, channelDepart.getAssignDepart());

                        Map<String, Object> caseData = Recaselist.get(i);
                        // 卡加貸的時候才要值
                        String parentUUID = "";
                        String productTypeID = "";
                        if (productId.equals("3")) {
                            // E貸寶 P002
                            productTypeID = "P002";
                        } else if (productId.equals("2")) {
                            // PLOAN P003
                            productTypeID = "P035";
                        } else {
                            // 預設值
                            productTypeID = "P035";
                        }
                        String prjCode = caseData.get("prj_code") == null ? "" : caseData.get("prj_code").toString();
                        String applyAmount = caseData.get("p_apy_amount").toString();
                         String customer_name = caseData.get("customer_name")!=null ? caseData.get("customer_name").toString():"";
                        String mobile_tel = caseData.get("mobile_tel")!=null?caseData.get("mobile_tel").toString():"";
                        String occupation = caseData.get("Occupation") != null ? caseData.get("Occupation").toString() : "";
                        FastPass fp = fastPassDao.Read(new FastPass(caseNo));
                        String amlStatus = fp != null ? fp.getAML() : "";
                        amlStatus = amlStatus.equals("V") ? "O" : amlStatus;
                        orbitRoot.orbitHeader.from(ip.getHostAddress(), "2",
                                Recaselist.get(i).get("CaseNoWeb").toString(), "9999", "webuser", parentUUID, prjCode,
                                channelDepart.getPromoDepart(), channelDepart.getPromoMember(), applyAmount,
                                productTypeID, channelDepart.getTransDepart(), channelDepart.getTransMember(), customer_name, mobile_tel, amlStatus, occupation);
                        String fileName = DateUtil.GetDateFormatString("yyyyMMddHHmmssS")
                                .concat(Recaselist.get(i).get("CaseNo").toString())
                                .concat(Recaselist.get(i).get("idno").toString()) + ".xml";

                        StringBuilder sb = new StringBuilder();
                        orbitRoot.toXML(sb);
                        jobLogService.AddSendDataInfoLog("信貸拒貸案件CaseNo:" + Recaselist.get(i).get("CaseNo").toString() + "打包後送XML檔名:" + fileName + " XML資訊：" + sb.toString());
                        InputStream in = IOUtils.toInputStream(sb.toString(), "UTF-8");

                        //上傳ftp失敗，拋出例外
                        if (!ftpservice.uploadImgXml(fileName, in)) {
                            throw new RuntimeException("上傳ftp失敗!!");
                        }
                        err = 4;
                        MailHistory rejectCaseMailHistory = soapService.getRejectCaseCMBProgress(Recaselist.get(i).get("idno").toString(), Recaselist.get(i).get("customer_name").toString(), channelDepart.getAssignDepart());
                        rejectCaseMailHistory.setUniqId(caseNo);
                        mailHistoryDao.Create(rejectCaseMailHistory);

                        err = 5;
                        //將案件狀態標註為09 拒貸送件完成
                        caseDataDao.updateStatusbyCaseNo(Recaselist.get(i).get("CaseNo").toString(), PlContractConst.CASE_STATUS_REJECT_SENDED);
                        cntOK++;
                    }
                } catch (Exception e) {
                    switch (err) {
                        case 1:
                            jobLogService.AddSendDataConnOutLog("拒貸案件".concat(Recaselist.get(i).get("CaseNo").toString()).concat("執行送件失敗(連線失敗)"));
                            break;
                        case 2:
                            jobLogService.AddSendDataConnOutLog("拒貸案件".concat(Recaselist.get(i).get("CaseNo").toString()).concat("申請書上傳影像失敗"));
                            break;
                        case 3:
                            jobLogService.AddSendDataConnOutLog("拒貸案件".concat(Recaselist.get(i).get("CaseNo").toString()).concat("拒貸案xml上傳影像失敗"));
                            break;
                        case 4:
                            jobLogService.AddSendDataConnOutLog("拒貸案件".concat(Recaselist.get(i).get("CaseNo").toString()).concat("呼叫影像WebService失敗"));
                            break;
                        case 5:
                            jobLogService.AddSendDataConnOutLog("拒貸案件".concat(Recaselist.get(i).get("CaseNo").toString()).concat("更新送件完成失敗"));
                            break;
                    }
                    jobLogService.AddSendDataErrorLog("拒貸案件 err Message:".concat(e.toString()));
                }
            }
//            System.out.println("上傳退件Job結束 共" + Recaselist.size() + "筆. 成功" + cntOK + "筆");
        }
    }*/

    public void uploadContractDataToOrbit() throws Exception {
        // 撈取尚未進件的案件契約資料
        List<ContractMain> sendlist = contractMainDao.queryWaitSendContract();
        int allCase = sendlist.size();
        
        if (sendlist.size() > 0) {
			
			System.out.println("立約上傳案件 總計: " + allCase + " 件上傳中...");
			
            // 確定有送出的資料之後 取得分行對應表以及初始化
            PDFHelper pdfHelper = new PDFHelper();
            List<DropdownData> branchList = dropdownDataDao.findByName("branchIdMapping");
            Map<String, String> branchMap = new HashMap<String, String>();
            for (int i = 0; i < branchList.size(); i++) {
                branchMap.put(branchList.get(i).getDataKey(),
                        branchList.get(i).getDataName());
            }

            int cntOK = 0;
            // System.out.println("上傳簽約案件資料Job開始 此批數量 : " + sendlist.size());
            for (ContractMain sendData : sendlist) {

                boolean sendimageOK = false; // 測試開true
                int err = 0;
                String conNo = sendData.getUniqId();
                // System.out.println("案件處理中 : ".concat(conNo));
                
                try {
                    // 由於線下件的BranchId會不一樣 所以要從constData去取得
                    String branchId4 = KB_9743_DEPART_ID;
                    String idno = sendData.getIdno();
                    String productId = sendData.getProductId();
                    String casePriority = "";
                    /*try {
                        if (sendData.get("prj_code") != null && !sendData.get("prj_code").equals("")) {
                            casePriority = getCasePriority(sendData.get("prj_code").toString());
                        }
                    } catch (Exception e) {
                    //    System.out.println("取得專案代號失敗" + e.getMessage());
                    }*/


                    try {
                        // 此段失敗就直接使用預設值
                        GetDgtConstRspDto getDgtConstRspDto = new Gson().fromJson(sendData.getApsConstData(), GetDgtConstRspDto.class);
                        GetDgtConstRspDtoConstData constData = getDgtConstRspDto.findOneConstData();
                        String branchId3 = constData.getBranch_id();

                        if (branchMap.containsKey(branchId3)) {
                            branchId4 = branchMap.get(branchId3);
                        }
                    } catch (Exception ex) {
                       System.out.println("取得BranchId失敗，原因:" + ex.getMessage());
                    }

                    final String prefix = "0" + DateUtil.GetDateFormatString("yyyyMMddHHmmssS");

                    err = 1;
                   // System.out.println("更新案件狀態為處理中 : ".concat(conNo));
                    // 2.將案件狀態標註為11送件中
                    contractMainDao.updateContractStatus(conNo, PlContractConst.CASE_STATUS_SUBMITTING);
                    
                    ContractMainActInfo actInfe = contractMainActInfoDao.ReadByUniqId(conNo);
                    boolean isACH = StringUtils.equals(actInfe.getEddaStatus(), "0"); // ACH

                    // 產生契約書PDF寄送EMAIL給客戶並儲存到DB
                    // contract.pdf 個人信用貸款契約書
                    contractDocService.generateContractPDF(conNo);
                    // contract.ach.pdf ACH申請書
                    contractDocService.generateContractAchPDF(conNo);
                    
                    // contract.pdf 包含 ACH 申請書
                    CaseDocument caseDocumentCont = caseDocumentDao.findByUniqIdAndDocumentType(conNo, DOCUMENT_TYPE_CONTRACT_PDF);
                    byte[] contByte = caseDocumentCont.getContent();
                    contByte = pdfHelper.DecryptPDF(contByte, idno); // 解密PDF

                    byte[] achByte = null;
                    if (isACH) {
                        CaseDocument caseDocumentAch = caseDocumentDao.findByUniqIdAndDocumentType(conNo, DOCUMENT_TYPE_CONTRACT_ACH_PDF);
                        achByte = caseDocumentAch.getContent();
                               achByte = pdfHelper.DecryptPDF(achByte, idno); // 解密PDF
                    }

                    // 3.上傳契約書到FTP,申請書檔案放在第一個
                    //List<Map<String, Object>> condoc = contractDocDao.getContractDoc(conNo);
                    
                    OrbitRoot orbitRoot = new OrbitRoot();
                    // if (condoc.size() == 0) {
                    //     // System.out.println("查無申請書 : ".concat(conNo));
                    //     jobLogService.AddSendDataChkfailLog("案件契約ConNo:" + conNo + " 查無申請書無法上傳");
                    //     // 沒產好pdf需查問題狀態
                    //     contractMainDao.updateContractStatus(conNo, PlContractConst.CASE_STATUS_SUBMIT_ERROR);
                    //     continue;
                    // }
                    // String contractPdfContent = condoc.get(0).get("PdfContent").toString();

                    err = 2;
                    // System.out.println("上傳FTP : ".concat(conNo));
                    // 契約書--組檔名
                    String filenamePdf = conNo + ".pdf";
                    // 將資料庫裡64base字串轉檔
                    // byte[] pdfbyte = pdfHelper.DecryptPDF(java.util.Base64.getDecoder().decode(contractPdfContent), idno);
                    logger.info("### filenamePdf=" + filenamePdf);
                    logger.info("### contByte.length=" + contByte.length);
                    uploadPdf(filenamePdf, contByte);

                    String filenameAch = conNo + ".ach.pdf";
                    if (isACH) {
                        logger.info("### filenameAch=" + filenameAch);
                        logger.info("### achByte.length=" + achByte.length);
                        uploadPdf(filenameAch, achByte);
                    }

                    // 4. 案件上傳圖檔到FTP
                    err = 6;

                    //byte[] photobyte = pdfHelper.DecryptPDF(java.util.Base64.getDecoder().decode(contractPdfContent), idno);
                    List<byte[]> imageList = new ArrayList<>();
                    imageList.addAll(pdfHelper.getImageList(contByte));
                    if (isACH) {
                        imageList.addAll(pdfHelper.getImageList(achByte));
                    }
                    String filename;
                    int j = 0;
                    for (byte[] image : imageList) {
                        // 4.1轉檔
                        // 4.2先組檔名 檔案從第二個檔案開始放
                        filename = prefix.concat(conNo);
                        filename = filename.concat("_" + String.format("%03d", j + 1));
                        filename = filename.concat(".jpg");

                        // IMAGE檔 - @export 儲存至檔案系統
                        if (globalConfig.isPDF_Export()) {
                            ExportUtil.export(global.PdfDirectoryPath + "/" + filename, image);
                        }

                        // 4.3上傳到ftp
                        sendimageOK = ftpservice.uploadImgBase64File(filename, image);
                        // 上傳ftp失敗，拋出例外
                        if (!sendimageOK) {
                            throw new RuntimeException("上傳ftp失敗!!");
                        }
                        // 開始組xml row檔
                        String imgType = "";
                        if (j == 0) { // XML檔 - 新增scanFiles第一列
                            orbitRoot.addConRow(filename, idno, productId, branchId4, casePriority, imgType);
                        } else {
                            orbitRoot.addSub(filename, imgType);
                        }
                        j++;
                    }

                    // 5.上傳到影像系統
                    err = 3;
                    // 產圖檔的XML檔將XML上傳到FTP

                    InetAddress ip = InetAddress.getLocalHost();

                    // APSSeqNo
                    GetDgtConstRspDto dgt = new Gson().fromJson(sendData.getApsConstData(), GetDgtConstRspDto.class);
                    final String APSSeqNo = dgt.findOneConstData().getAPSSeqNo();

                    // 例如：CONT2021050700020.pdf,CONT2021050700020.ach.pdf
                    String attachments = filenamePdf;
                    if (isACH) {
                        attachments = filenamePdf.concat(",").concat(filenameAch);
                    }
                    orbitRoot.orbitHeader.attachmentfrom(ip.getHostAddress(), "0", "", branchId4, "9999", "webuser",
                            sendData.getEmail(), "", APSSeqNo, attachments);
                    String fileName = prefix.concat(conNo).concat(".xml");

                    StringBuilder sb = new StringBuilder();
                    orbitRoot.toXML(sb);
                    
                    logger.info("案件契約 xml");
                    logger.info(sb.toString());
                    // XML檔 - @export 儲存至檔案系統
                    if (globalConfig.isPDF_Export()) {
                        ExportUtil.export(global.PdfDirectoryPath + "/" + fileName, sb.toString().getBytes("UTF-8"));
                    }

                    jobLogService.AddSendDataInfoLog(
                            "案件契約ConNo:" + conNo + "打包後送XML檔名:" + fileName + " XML資訊：" + sb.toString());
                    InputStream in = IOUtils.toInputStream(sb.toString(), "UTF-8");
                    // System.out.println("上傳XML至FTP : ".concat(conNo));
                    // 上傳ftp失敗，拋出例外
                    if (!ftpservice.uploadImgXml(fileName, in)) {
                        throw new RuntimeException("上傳ftp失敗!!");
                    }
                    // 契約書送件--呼叫訊息平台
                    MailHistory contractMailHistory = soapService.getContractCMBProgress(idno,
                            sendData.getChtName().toString(), branchId4);
                    contractMailHistory.setUniqId(conNo);
                    mailHistoryDao.Create(contractMailHistory);

                    err = 4;
                    // System.out.println("更新案件狀態為完成 : ".concat(conNo));
                    // 6.將案件狀態標註為12送件完成
                    contractMainDao.updateContractStatus(conNo, PlContractConst.CASE_STATUS_SUBMIT_SUCCESS);
                    contractMainDao.updateSendTime(conNo);

                    // 案件契約書後送完成，進行送件圖檔清檔
                    err = 5;
                    String delete = globalConfig.AdditionalPhotoDelete();
                    if (delete.equals("1")) {
                       // System.out.println("刪除立約圖檔 : ".concat(conNo));
                        contractDocDao.deleteContractDocPhoto(conNo);
                        jobLogService.AddSendDataInfoLog("案件契約ConNo：".concat(conNo).concat("刪除契約書後送影像系統用圖檔成功"));
                    }
                    cntOK++;
                } catch (Exception e) {
                    logger.error("Fail to uploadContractDataToOrbit.", e);
                    switch (err) {
                        case 1:
                            jobLogService.AddSendDataConnOutLog(
                                    "案件契約ConNo：".concat(conNo).concat("執行送件失敗(連線失敗),原因:").concat(e.getMessage()));
                            break;
                        case 2:
                            jobLogService.AddSendDataConnOutLog(
                                    "案件契約ConNo：".concat(conNo).concat("圖片上傳ftp失敗,原因:").concat(e.getMessage()));
                            break;
                        case 3:
                            jobLogService.AddSendDataConnOutLog(
                                    "案件契約ConNo：".concat(conNo).concat("呼叫影像WebService失敗,原因:").concat(e.getMessage()));
                            break;
                        case 4:
                            jobLogService.AddSendDataConnOutLog(
                                    "案件契約ConNo：".concat(conNo).concat("更新送件完成失敗,原因:").concat(e.getMessage()));
                            break;
                        case 5:
                            jobLogService.AddSendDataConnOutLog(
                                    "案件契約ConNo：".concat(conNo).concat("刪除契約書後送影像系統用圖檔失敗,原因:").concat(e.getMessage()));
                            break;
                        case 6:
                            jobLogService.AddSendDataConnOutLog(
                                    "案件契約ConNo：".concat(conNo).concat("產生立約圖檔失敗,原因:").concat(e.getMessage()));
                            break;
                        default:
                            jobLogService.AddSendDataErrorLog("案件契約ConNo：".concat(e.getMessage()));
                            break;
                    }

                    // 當連不到ws時，讓狀態直接完成，不影響後面流程。
                    if (err == 3) {
                        contractMainDao.updateContractStatus(conNo, PlContractConst.CASE_STATUS_SUBMIT_SUCCESS);
                    } else {
                        contractMainDao.updateContractStatus(conNo, PlContractConst.CASE_STATUS_SUBMIT_FAIL);
                    }
                }
            }
            // System.out.println("上傳簽約案件資料Job結束 此批數量 : " + sendlist.size() + " 成功數量 : " + cntOK);
            jobLogService.AddSendDataInfoLog("案件契約打包後送本次處理總共：" + sendlist.size() + "筆，處理成功計有：" + cntOK + "筆，處理失敗計有"
                    + (sendlist.size() - cntOK) + "筆。");
        }
    }

    private boolean uploadPdf(String filename, byte[] pdfbyte) {
        // System.out.println("上傳FTP : ".concat(conNo));
        // 契約書--組檔名
        // 將資料庫裡64base字串轉檔
        // byte[] pdfbyte = pdfHelper.DecryptPDF(java.util.Base64.getDecoder().decode(contractPdfContent), idno);
        ftpservice.toInputStream(pdfbyte);
        // PDF檔 - @export 儲存至檔案系統
        if (globalConfig.isPDF_Export()) {
            ExportUtil.export(global.PdfDirectoryPath + "/" + filename, pdfbyte);
        }
        // 上傳到ftp
        boolean sendimageOK = ftpservice.uploadImgBase64File(filename);
        // 上傳ftp失敗，拋出例外
        if (!sendimageOK) {
            throw new RuntimeException("上傳ftp失敗!!");
        }
        return sendimageOK;
    }
    
    // Ben說：這是中壽的，不適用。
    // public String getCasePriority(String projectCode) {
    //     return dropdownDataDao.getDataNameByNameAndDataKey("CL.CasePriority", projectCode);
    // }

    @SuppressWarnings("unused")
    private String createDocAddrImageBase64(String docAddr) {

        String html = "";
        if (docAddr.equals("")) {
            html = "<html>" +
                    "<table><tr valign=\"top\">" +
                    "<td><font style=\"font-size:20px;\">客戶無指定撥貸通知書寄送地址</font></td>" +
                    "</tr></table>" +
                    "</html>";
        } else {
            html = "<html>" +
                    "<table><tr>" +
                    "<td><font style=\"font-size:20px;\">撥貸通知書寄送地址：</font></td>" +
                    "<td><font style=\"font-size:20px;\">" + docAddr + "</font></td>" +
                    "</tr></table>" +
                    "</html>";

        }
        try {
            JLabel label = new JLabel(html);
            label.setSize(1024, 768);
            label.setForeground(Color.BLACK);
            label.setVerticalAlignment(JLabel.TOP);

            BufferedImage image = new BufferedImage(
                    label.getWidth(), label.getHeight(),
                    BufferedImage.TYPE_INT_RGB);

            // paint the html to an image
            Graphics g = image.getGraphics();
            //背景設成白色
            Color bg = new Color(255, 255, 255);
            g.setColor(bg);
            g.fillRect(0, 0, label.getWidth(), label.getHeight());
            label.paint(g);
            g.dispose();

            // get the byte array of the image (as jpeg)
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", baos);
            byte[] imageData = baos.toByteArray();
            baos.close();
            return Base64.encodeBase64String(imageData);
        } catch (Exception e) {
//            e.printStackTrace();
            return "";
        }

    }


}
