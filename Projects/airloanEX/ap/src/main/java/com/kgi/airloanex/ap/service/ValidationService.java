package com.kgi.airloanex.ap.service;

import static com.kgi.airloanex.common.PlContractConst.CONT;
import static com.kgi.airloanex.common.PlContractConst.SituationPCode2566.auth;
import static com.kgi.airloanex.common.PlContractConst.SituationPCode2566.sign;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.CaseAuthDao;
import com.kgi.airloanex.ap.dao.ContractApiLogDao;
import com.kgi.airloanex.ap.dao.ContractMainActInfoDao;
import com.kgi.airloanex.ap.dao.ContractMainDao;
import com.kgi.airloanex.ap.dao.ContractVerifyLogDao;
import com.kgi.airloanex.ap.dao.CreditVerifyDao;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.PlContractConst.RouteGo;
import com.kgi.airloanex.common.PlContractConst.SituationPCode2566;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDto;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoConstData;
import com.kgi.airloanex.common.dto.customDto.IdentityVerify;
import com.kgi.airloanex.common.dto.db.CaseAuth;
import com.kgi.airloanex.common.dto.db.ContractMain;
import com.kgi.airloanex.common.dto.db.ContractMainActInfo;
import com.kgi.airloanex.common.dto.db.ContractVerifyLog;
import com.kgi.airloanex.common.dto.db.CreditVerify;
import com.kgi.airloanex.common.dto.response.PCode2566Resp;
import com.kgi.airloanex.common.dto.response.VerifyCountResp;
import com.kgi.eopend3.ap.dao.DropdownDao;
import com.kgi.eopend3.ap.dao.HttpDao;
import com.kgi.eopend3.ap.dao.SNDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.customDto.BNS00040007Dto;
import com.kgi.eopend3.common.dto.customDto.PCode2566Dto;
import com.kgi.airloanex.common.dto.db.DropdownData;
import com.kgi.eopend3.common.dto.respone.CheckOTPResp;
import com.kgi.eopend3.common.dto.respone.HttpResp;
import com.kgi.eopend3.common.dto.respone.LogFromValidation;
import com.kgi.eopend3.common.dto.respone.SendOTPResp;
import com.kgi.eopend3.common.dto.view.CheckOTPView;
import com.kgi.eopend3.common.dto.view.EddaView;
import com.kgi.eopend3.common.dto.view.PCode2566View;
import com.kgi.eopend3.common.dto.view.SendOTPView;
import com.kgi.eopend3.common.util.CheckUtil;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidationService {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AirloanEXConfig globalConfig;

	@Autowired
	private DropdownDao dropdownDao;

	@Autowired
	private SNDao sndao;

	@Autowired
	private SrnoService snSer;

	@Autowired
	private ContractMainDao contractMainDao;

	@Autowired
	private ContractApiLogDao contractApiLogDao;

	@Autowired
	private ContractVerifyLogDao verifyLogDao;
	
	@Autowired
	private HttpDao httpDao ;

	@Autowired
	private ContractMainActInfoDao contractMainActInfoDao;

	@Autowired
	private CaseAuthDao caseAuthDao;

	@Autowired
	private CreditVerifyDao creditVerifyDao;

	@Autowired
	private AccountInfoService accountInfoService;

	/**
	 * 取得 OTP
	 * */
	public SendOTPResp getSessionKey(SendOTPView view, String uniqId, String idno, SituationPCode2566 situation) {
		logger.info("--> getSessionKey()");
//		String response = "";
		String startTime = "";
		String endTime = "";
		String logReq = "", logRes = "" ;

		try {
			view.setIdno(idno);
			view.setBillDep(globalConfig.OTP_BillDep());
			view.setOpId(globalConfig.OTP_OpID());

			// 交易代號 格式(AIRLOAN + 年月日(8) + 流水號(4))
			String date = DateUtil.GetDateFormatString("yyyyMMdd");
			// 從DN取流水號 並將其串成四位字串
			Integer sni = sndao.getSerialNumber("OTP", date);
			String sn = StringUtils.leftPad(sni.toString(), 4, "0");
			String txnId = CONT + date + sn;
			view.setTxnId(txnId);
			String jsonStr = new Gson().toJson(view);
			logReq = jsonStr ;

			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			HttpResp resp = httpDao.postJson(globalConfig.ValidationServerUrl() + SystemConst.API_SEND_OTP, jsonStr);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			logRes = resp.getResponse() ;
			logger.info("getSessionKey - resp.getResponse()");
			logger.info(resp.getResponse());
			LogFromValidation log = new Gson().fromJson(resp.getResponse(), LogFromValidation.class) ;
			if (log != null) {
				if (!CheckUtil.isEmpty(log.getReq())) {
					logReq = log.getReq() ;
				}
				
				if (!CheckUtil.isEmpty(log.getRes())) {
					logRes = log.getRes() ;
				}
			}
			SendOTPResp sendOpt = new Gson().fromJson(resp.getResponse(), SendOTPResp.class) ;

			// 2020-08-11 demand by Ben
			logReq = jsonStr;
			logRes = resp.getResponse();

			// 寫入apilog記錄
			contractApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "/sendOTP", logReq, logRes, startTime, endTime);

			// 寫入ContractMainActInfo
			ContractMainActInfo verify = contractMainActInfoDao.touch(new ContractMainActInfo(uniqId));
			if (auth == situation) {
				verify.setAuthPhone(view.getPhone());
			} else if (sign == situation) {
				verify.setSignPhone(view.getPhone());
			}
			contractMainActInfoDao.Update(verify);

			return sendOpt;
		} catch (Exception e) {
			logger.error("未處理的錯誤", e);
			SendOTPResp sendOpt = new SendOTPResp() ; 
			sendOpt.setCode("F999");
			sendOpt.setMessage("系統錯誤");
			return sendOpt;
		} finally {
			logger.info("<-- getSessionKey()");
		}
	} // end getSessionKey

	/**
	 * 檢核 OTP
	 */
	public CheckOTPResp checkSessionKeyOtp(CheckOTPView view, String uniqId, String idno, String ipAddr, SituationPCode2566 situation) {
		logger.info("--> checkSessionKeyOtp()");
//		String response = "";
		String startTime = "";
		String endTime = "";
		String logReq = "", logRes = "" ;

		try {
			view.setIdno(idno);
			view.setBillDep(globalConfig.OTP_BillDep());
			view.setOpId(globalConfig.OTP_OpID());

			String jsonStr = new Gson().toJson(view);
			logReq = jsonStr ;

			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			HttpResp response = httpDao.postJson(globalConfig.ValidationServerUrl() + SystemConst.API_CHECK_OTP, jsonStr);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			logRes = response.getResponse() ;
			logger.info("checkSessionKeyOtp - response.getResponse()");
			logger.info(response.getResponse());
			LogFromValidation log = new Gson().fromJson(response.getResponse(), LogFromValidation.class) ;
			if (log != null) {
				if (!CheckUtil.isEmpty(log.getReq())) {
					logReq = log.getReq() ;
				}
				
				if (!CheckUtil.isEmpty(log.getRes())) {
					logRes = log.getRes() ;
				}
			}
			CheckOTPResp checkOpt = new Gson().fromJson(response.getResponse(), CheckOTPResp.class) ;

			// 2020-08-11 demand by Ben
			logReq = jsonStr;
			logRes = response.getResponse();

			// 寫入apilog記錄
			contractApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "/checkOTP", logReq, logRes, startTime, endTime);

			ContractMain contractMain = contractMainDao.Read(new ContractMain(uniqId));
			contractMain.setIpAddress(ipAddr);
			// 更新立約狀態
			if (contractMain.getStatus().equals(PlContractConst.CASE_STATUS_INIT)) {
				// 填寫中
				contractMain.setStatus(PlContractConst.CASE_STATUS_WRITING);
			}

			contractMainDao.Update(contractMain);

			// 寫入ContractMainActInfo
			ContractMainActInfo verify = contractMainActInfoDao.Read(new ContractMainActInfo(uniqId));
			boolean isCheckOtpSuccess = "0000".equals(checkOpt.getCode());
			if (auth == situation) {
				verify.setAuthPhone(view.getPhone());
				verify.setAuthStatus(isCheckOtpSuccess?"1":"0");
				verify.setAuthTime(DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS"));
				verify.setAuthErrCount(isCheckOtpSuccess?verify.getAuthErrCount():verify.getAuthErrCount()+1);

				CaseAuth caseAuth = new CaseAuth(uniqId);
				// CreditVerify creditVerify = new CreditVerify(uniqId); // 既有戶無需 insert creditVerify
				caseAuth.setUniqType("03");
				caseAuth.setAuthType("1"); // 1:OTP
				caseAuth.setMbcValid("0");
				caseAuth.setChtAgree("0");
				caseAuthDao.CreateOrUpdate(caseAuth);

			} else if (sign == situation) {
				final String signTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
				verify.setSignPhone(view.getPhone());
				verify.setSignStatus(isCheckOtpSuccess?"1":"0");
				verify.setSignTime(signTime);
				verify.setSignErrCount(isCheckOtpSuccess?verify.getSignErrCount():verify.getSignErrCount()+1);
			}
			contractMainActInfoDao.Update(verify);

			return checkOpt;
		} catch (ErrorResultException e) {
			CheckOTPResp checkOpt = new CheckOTPResp() ;
			checkOpt.setCode(String.valueOf(e.getStatus()));
			checkOpt.setMessage("查無資料");
			return checkOpt;
		} catch (Exception e) {
			CheckOTPResp checkOpt = new CheckOTPResp() ;
			checkOpt.setCode("F099");
			checkOpt.setMessage( "系統錯誤");
			logger.error("系統錯誤", e);

			return checkOpt;
		} finally {
			logger.info("<-- checkSessionKeyOtp()");
		}
	} // end checkSessionKeyOtp

	/**
	 * 讀取案件的驗證失敗次數
	 * 如果案件不存在 ContractMainActInfo ，建立之
	 * */
	public String getVerifyCount(String ulid) {
		try {
			ContractMainActInfo verify = contractMainActInfoDao.touch(new ContractMainActInfo(ulid));
			VerifyCountResp resp = new VerifyCountResp();
			resp.setAuthErrCount(verify.getAuthErrCount());
			resp.setSignErrCount(verify.getSignErrCount());
			resp.setEddaErrCount(verify.getEddaErrCount());

			return WebResult.GetResultString(0, "成功", resp);
		} catch (Exception e) {
			logger.error("未處理的錯誤", e);
			return WebResult.GetResultString(99, "系統錯誤", null);
		}
	} // end getVerifyCount

	public String checkPCode2566(String uniqId, String idno, PCode2566View view, SituationPCode2566 situation) throws Exception {
		logger.info("--> checkPCode2566()" );
		ContractVerifyLog verifyLog = new ContractVerifyLog();
		verifyLog.setUniqId(uniqId);
		verifyLog.setVerifyType(SystemConst.VERIFYLOG_TYPE_PCODE);
		verifyLog.setCount(verifyLogDao.Query(verifyLog).size() + 1);
		ContractMainActInfo verify = contractMainActInfoDao.touch(new ContractMainActInfo(uniqId));

		if (verify == null) {
			return WebResult.GetResultString(19, "查無資料", null);
		}

		String accountNo = view.getAccount();
		String accountNo16 = String.format("%016d", Long.parseLong(accountNo));
		view.setAccount(accountNo16);
		view.setIdno(idno);

		// PCode2566 IdentityVerify
		IdentityVerify identityVerify = new IdentityVerify();
		identityVerify.setUniqId(verify.getUniqId());
		identityVerify.setVerifyType("PCode2566");
		identityVerify.setBankId(view.getBank());
		identityVerify.setAccountNo(view.getAccount());
		
		// Auth/Sign/Edda - (DB to DTO) ContractMainActInfo -> IdentityVerify
		if (auth == situation) {
			identityVerify.setErrorCount(verify.getAuthErrCount());
		} else if (sign == situation) {
			identityVerify.setErrorCount(verify.getSignErrCount());
		}

		// sign撥款資訊頁，當input mode=N時，不驗證pcode2566，只驗證edda
		if (sign == situation) {
			ContractMain contractMain = contractMainDao.ReadByUniqId(uniqId); 
            GetDgtConstRspDto apsConstData = new Gson().fromJson(contractMain.getApsConstData(), GetDgtConstRspDto.class);
			GetDgtConstRspDtoConstData one = apsConstData.findOneConstData();
			if (StringUtils.equals("N", one.getInput_mode())) {
				identityVerify.setPcode2566CheckCode("0");
				identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_SUCCESS);
			} else {
                // 主要驗證邏輯 - 將 PCode2566 驗證結果回填至 identityVerify
                if (view.getBank().equals(SystemConst.KGI_BANK_ID)) {
                	kgiPCode2566(uniqId, view, identityVerify, verifyLog);
                } else {
                	otherPCode2566(uniqId, view, identityVerify, verifyLog);
                }
			}
		} else {
            // 主要驗證邏輯 - 將 PCode2566 驗證結果回填至 identityVerify
            if (view.getBank().equals(SystemConst.KGI_BANK_ID)) {
            	kgiPCode2566(uniqId, view, identityVerify, verifyLog);
            } else {
            	otherPCode2566(uniqId, view, identityVerify, verifyLog);
            }
		}

		// Auth/Sign/Edda - (DTO to DB)IdentityVerify -> ContractMainActInfo
		PCode2566Resp resp = new PCode2566Resp();
		if (auth == situation) {

			verify.setAuthBankId(view.getBank());
			verify.setAuthBankName(view.getBankName());
			verify.setAuthAccountNo(accountNo);
			verify.setAuthStatus(identityVerify.getVerifyStatus());
			verify.setAuthTime(DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS"));
			contractMainActInfoDao.Update(verify);

			resp.setPcode2566CheckCode(identityVerify.getPcode2566CheckCode());
			resp.setPcode2566Message(identityVerify.getPcode2566Message());
			resp.setAuthErrCount(identityVerify.getErrorCount());

			CaseAuth caseAuth = new CaseAuth(uniqId);
			caseAuth.setUniqType("03");
			caseAuth.setAuthType("4"); // 4:其它
			caseAuth.setMbcValid("0");
			caseAuth.setChtAgree("0");
			caseAuthDao.CreateOrUpdate(caseAuth);
			CreditVerify creditVerify = new CreditVerify(uniqId);
			creditVerify.setUniqType("03");
			creditVerify.setVerifyStatus(PlContractConst.VERIFY_STATUS_SUCCESS);
			creditVerify.setVerifyType(PlContractConst.CREDITVERIFY_TYPE_BANKACOUNT);
			creditVerify.setBankId(identityVerify.getBankId());
			creditVerify.setAccountNo(identityVerify.getAccountNo());
			creditVerify.setErrorCount(identityVerify.getErrorCount());
			creditVerify.setPcode2566ErrorCount(identityVerify.getErrorCount());
			creditVerifyDao.CreateOrUpdate(creditVerify);
			
		} else if (sign == situation) {

			verify.setSignBankId(view.getBank());
			verify.setSignBranchId(view.getBranchId());;
			verify.setSignBankName(view.getBankName());;
			verify.setSignAccountNo(accountNo);
			verify.setSignStatus(identityVerify.getVerifyStatus());
			verify.setSignTime(DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS"));
			contractMainActInfoDao.Update(verify);

			resp.setPcode2566CheckCode(identityVerify.getPcode2566CheckCode());
			resp.setPcode2566Message(identityVerify.getPcode2566Message());
			resp.setSignErrCount(identityVerify.getErrorCount());
		}

		return WebResult.GetResultString(0, "成功", resp);
	} // end checkPCode2566

	/**
	 * 檢核他行 Pcode2566
	 * */
	private void otherPCode2566(String uniqId, PCode2566View view, IdentityVerify identityVerify, ContractVerifyLog verifyLog) {
		logger.info("--> otherPCode2566() uniqId=[" + uniqId + "]" );
		String startTime = "";
		String endTime = "";
		String reqJson = "";
//		String response = "";
		String noteCode = "";
		String statusCode = "";
		DropdownData dropDown = new DropdownData();
		String logRes = "", logReq = "" ;

		try {
			view.setTmnlId(globalConfig.PCODE2566_TMNL_ID());
			view.setTmnlType(globalConfig.PCODE2566_TMNL_TYPE());


			ContractMain ContractMain = contractMainDao.Read(new ContractMain(uniqId));
			view.setBirthday(ContractMain.getBirthday());
			view.setPcodeUid(snSer.getpCode2566UniqId());

			// ESB Setting
			view.setESBChannel(globalConfig.ESB_Channel());
			view.setESBClientId(globalConfig.ESB_ClientId());
			view.setESBQueueReceiveName(globalConfig.ESB_QueueReceiveName());
			String esbClientPAZZD = globalConfig.ESB_ClientPAZZD();
			String timeNow = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
			esbClientPAZZD = DigestUtils.md5Hex(esbClientPAZZD + timeNow);
			view.setESBTimestamp(timeNow);
			view.setESBClientPAZZD(esbClientPAZZD);

			reqJson = new Gson().toJson(view);
			logReq = reqJson ;

			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			HttpResp response = httpDao.postJson(globalConfig.ValidationServerUrl() + SystemConst.API_OTHER_PCDOE2566, reqJson);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			logger.info(SystemConst.API_OTHER_PCDOE2566 + " response: [" + response + "]");
			LogFromValidation log = new Gson().fromJson(response.getResponse(), LogFromValidation.class) ;
			logRes = response.getResponse() ;
			if (log != null) {
				if (!CheckUtil.isEmpty(log.getReq())) {
					logReq = log.getReq() ;
				}
				
				if (!CheckUtil.isEmpty(log.getRes())) {
					logRes = log.getRes() ;
				}
			}

			PCode2566Dto pcode2566Resp = new Gson().fromJson(response.getResponse(), PCode2566Dto.class);

			// 2020-08-11 demand by Ben
			logReq = reqJson;
			logRes = response.getResponse();

			if (pcode2566Resp.getResult() == 0) {
				// 檢驗結果
				String verifyCodeByVerifyType = pcode2566Resp.getVerifyCodeByVerifyType();
				// 帳號檢驗結果
				String verifyCodeByAccountState = pcode2566Resp.getVerifyCodeByAccountState();
				// 開戶狀態
				String verifyCodeByOpenAccountState = pcode2566Resp.getVerifyCodeByOpenAccountState();
				noteCode = verifyCodeByVerifyType + verifyCodeByAccountState + verifyCodeByOpenAccountState;

				if (verifyCodeByVerifyType.equals("00")) {
					if (verifyCodeByAccountState.equals("00")) {
						// 2019/01/14 新增加檢核第5 6碼
						if (!verifyCodeByOpenAccountState.equals("01")) {
							setErrorRespAndErrorCountPlus("PCode2566NoteCodeOpenAccountState",
									verifyCodeByOpenAccountState, "資料錯誤，請與開戶行聯繫", identityVerify);
						} else {
							identityVerify.setPcode2566CheckCode("0");
							identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_SUCCESS);
						}
					} else { // verifyCodeByAccountState != "00"
						setErrorRespAndErrorCountPlus("PCode2566NoteCodeAccountState", verifyCodeByAccountState,
								"資料錯誤，請與開戶行聯繫", identityVerify);
					}
				} else { // verifyCodeByVerifyType != "00"
					setErrorRespAndErrorCountPlus("PCode2566NoteCodeVerifyType", verifyCodeByVerifyType,
							"資料錯誤，請與開戶行聯繫", identityVerify);
				}
			} else if (pcode2566Resp.getResult() < 0) { // 小於0的錯誤號碼代表系統錯誤，不需增加錯誤次數
				if (pcode2566Resp.getResult() == -2538) {
					dropDown.setName("PCode2566ConnectFailCode");
					dropDown.setDataKey(pcode2566Resp.getStatusCode());
					dropDown = dropdownDao.Read(dropDown);
					identityVerify.setPcode2566CheckCode(String.valueOf(pcode2566Resp.getStatusCode()));
					if (dropDown != null) {
						identityVerify.setPcode2566Message(dropDown.getDataName());
					}
				} else {
					identityVerify.setPcode2566Message("系統錯誤" + pcode2566Resp.getMessage());
				}
				identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_FAIL);
				identityVerify.setErrorMsg(identityVerify.getPcode2566Message());

			} else {// 小於0的錯誤號碼代表商務邏輯錯誤，需增加錯誤次數
				identityVerify.setPcode2566CheckCode(pcode2566Resp.getStatusCode());
				identityVerify.setPcode2566Message("您本次填寫的驗證資料不正確");
				setErrorRespAndErrorCountPlus("PCode2566StatusCode", statusCode, "資料錯誤，請與開戶行聯繫", identityVerify);
				identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_FAIL);
				identityVerify.setErrorMsg(identityVerify.getPcode2566Message());
			}
		} catch (Exception e) {
			identityVerify.setPcode2566CheckCode("-999");
			identityVerify.setPcode2566Message("您本次填寫的驗證資料不正確");
			
			
			identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_FAIL);
			identityVerify.setErrorMsg(identityVerify.getPcode2566Message());
		} finally {
			contractApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "/pCode2566/other", logReq, logRes, startTime, endTime);

			verifyLog.setRequest(logReq);
			verifyLog.setResponse(logRes);
			// 晚點再補這兩個欄位
			verifyLog.setCheckCode(statusCode);
			verifyLog.setAuthCode(noteCode);
			verifyLogDao.Create(verifyLog);
		}
	} // end otherPCode2566

	/**
	 * 檢核凱基 Pcode2566
	 * */
	private void kgiPCode2566(String uniqId, PCode2566View view, IdentityVerify identityVerify,
			ContractVerifyLog verifyLog) throws Exception {
		logger.info("--> kgiPCode2566() uniqId=[" + uniqId + "]" );
		String startTime = "";
		String endTime = "";
		String reqJson = "";
//		String res = "";
		
		String logReq = "", logRes = "" ;
		try {
			// ESB Setting
			view.setESBChannel(globalConfig.ESB_Channel());
			view.setESBClientId(globalConfig.ESB_ClientId());
			view.setESBQueueReceiveName(globalConfig.ESB_QueueReceiveName());
			String esbClientPAZZD = globalConfig.ESB_ClientPAZZD() ;
			String timeNow = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
			esbClientPAZZD = DigestUtils.md5Hex(esbClientPAZZD + timeNow);
			view.setESBTimestamp(timeNow);
			view.setESBClientPAZZD(esbClientPAZZD);


			reqJson = new Gson().toJson(view);
			logReq = reqJson ;

			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			HttpResp res = httpDao.postJson(globalConfig.ValidationServerUrl() + SystemConst.API_KGI_PCODE2566, reqJson);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
//			logger.info(res);
			logRes = res.getResponse() ;
			LogFromValidation log = new Gson().fromJson(res.getResponse(), LogFromValidation.class) ;
			if (log != null) {
				if (!CheckUtil.isEmpty(log.getReq())) {
					logReq = log.getReq() ;
				}
				
				if (!CheckUtil.isEmpty(log.getRes())) {
					logRes = log.getRes() ;
				}
			}
			BNS00040007Dto bns00040007 = new Gson().fromJson(res.getResponse(), BNS00040007Dto.class);

			// 2020-08-11 demand by Ben
			logReq = reqJson;
			logRes = res.getResponse();

			// 舊戶正常帳戶:
			// 存摺掛失<PBStatus1> => 1: 正常 空值是無摺
			// 印鑑掛失<ChopStatus1> => 1:正常
			// Status1: 正常
			// NOOFHOLDS1: 00000
			// IDNo
			if (bns00040007.getResult() == 0) {
				String status = bns00040007.getStatus();
				String pbStatus = bns00040007.getPbStatus();
				String chopStatus = bns00040007.getChopStatus();
				String noOfHold = bns00040007.getNoOfHold();
				String bnsIdno = bns00040007.getBnsIdno();
				if ("正常".equals(status) 
					&& ("1".equals(pbStatus) || "".equals(pbStatus)) 
					&& "1".equals(chopStatus)
					&& "00000".equals(noOfHold) 
					&& view.getIdno().equals(bnsIdno)) {
					identityVerify.setPcode2566CheckCode("0");
					identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_SUCCESS);
				}
			}

			// 如果是業務面的失敗，驗身錯誤次數要 +1
			if (bns00040007.getResult() > 0 || !"0".equals(identityVerify.getPcode2566CheckCode())) {
				identityVerify.setPcode2566CheckCode("1");
				identityVerify.setPcode2566Message(bns00040007.getMessage());
				identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_FAIL);
				identityVerify.setErrorCount(identityVerify.getErrorCount() + 1);
				identityVerify.setErrorMsg(identityVerify.getPcode2566Message());
			}

		} catch (Exception e) {
			logger.error("未知錯誤", e);
			throw e;
		} finally {
			contractApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "/pCode2566/KGI", logReq, logRes, startTime, endTime);
			verifyLog.setRequest(logReq);
			verifyLog.setResponse(logRes);
			verifyLog.setCheckCode("");
			verifyLog.setAuthCode("");
			verifyLogDao.Create(verifyLog);
		}
	} // end kgiPCode2566

	private void setErrorRespAndErrorCountPlus(String name, String dataKey, String defaultMsg, IdentityVerify identityVerify) {
		DropdownData dropDown = new DropdownData();
		dropDown.setName(name);
		dropDown.setDataKey(dataKey);
		dropDown = dropdownDao.Read(dropDown);
		identityVerify.setPcode2566CheckCode(dataKey);
		if (dropDown != null) {
			identityVerify.setPcode2566Message(dropDown.getDataName());
		} else {
			identityVerify.setPcode2566Message(defaultMsg);
		}
		identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_FAIL);
		identityVerify.setErrorCount(identityVerify.getErrorCount() + 1);
	} // end setErrorRespAndErrorCountPlus

    /**
     * 是否為凱基銀行 809
     * @param bank_no
     * @return
     */
    private boolean isKgibank(String bank_no) {
        String bank_id = StringUtils.substring(bank_no, 0, 3); // 3碼銀行別
        return StringUtils.equals("809", bank_id);
    }

	public String checkEdda(String uniqId, String idno, EddaView view) {

		ContractMain contractMain = contractMainDao.ReadByUniqId(uniqId);
		GetDgtConstRspDto apsConstData = new Gson().fromJson(contractMain.getApsConstData(), GetDgtConstRspDto.class);
		GetDgtConstRspDtoConstData one = apsConstData.findOneConstData();

		String eddaBankId = view.getEddaBankId();
		String eddaAccountNo = view.getEddaAccountNo();
		String eddaBankName = view.getEddaBankName();

		String rtn = null; // 0:edda未驗過 1:edda驗過 2: 不須驗證
		// 2020-09-04 demand by Ben 68.加上一邏輯，RP繳款(受扣)如果為809，則無需打EDDA驗證
		// System.out.println("### contractMain.getProductId() = " + contractMain.getProductId());
		// System.out.println("### isKgibank(view.getEddaBankId()) = " + isKgibank(view.getEddaBankId()));
		if (StringUtils.equals("1", contractMain.getProductId()) && isKgibank(view.getEddaBankId())) {
			rtn = "2";
		} else if (StringUtils.equals("1", one.getPay_way_type())) { // 1-本行
			rtn = "2";
		} else {
			rtn =  accountInfoService.getEDDA0000100MDetail(uniqId, idno, eddaBankId, eddaAccountNo);		
		}
		logger.info("edda -->" + rtn);
		
		Integer status;
		String message;
		Object result;

		boolean isSuccess = !StringUtils.equals("0", rtn);
		// System.out.println("### isSuccess => " + isSuccess);
		if(isSuccess) {
			// goto previewPlContract 頁籤
			status = 0;
			message = "成功";
			result = RouteGo.previewPlContract;
		} else {
			status = 99;
			message = "繳款資訊錯誤";
			result = RouteGo.infoPlContract;
		}

		ContractMainActInfo verify = contractMainActInfoDao.ReadByUniqId(uniqId);
		verify.setEddaBankId(eddaBankId);
		verify.setEddaBankName(eddaBankName);
		verify.setEddaAccountNo(eddaAccountNo);

		if (isSuccess) {
			verify.setEddaStatus("1");  // FIXME:  0:edda未驗過 1:edda驗過 2: 不須驗證 新增 不須驗證的狀態參數值 調整後面相關對應
			verify.setEddaErrCount(verify.getEddaErrCount());
			if (StringUtils.equals(rtn, "1")) {
				verify.setEddaTime(DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS"));
			} else if (StringUtils.equals(rtn, "2")){
				verify.setEddaTime(null);
			}
		} else {
			verify.setEddaStatus("0");
			verify.setEddaErrCount(verify.getEddaErrCount()+1);
			verify.setEddaTime(null);
		}
		
		contractMainActInfoDao.Update(verify);

		return WebResult.GetResultString(status, message, result);

	}
}
