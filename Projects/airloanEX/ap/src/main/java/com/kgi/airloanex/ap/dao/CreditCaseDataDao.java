package com.kgi.airloanex.ap.dao;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.db.CaseData;
import com.kgi.airloanex.common.dto.db.CreditCaseData;
import com.kgi.airloanex.common.dto.db.CreditCaseDataExtend;
import com.kgi.airloanex.common.dto.db.CreditEopDataDto;
import com.kgi.airloanex.common.dto.view.ApplyNewCreditcardView;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.apache.commons.lang3.StringUtils;
import org.owasp.esapi.AccessReferenceMap;
import org.owasp.esapi.reference.RandomAccessReferenceMap;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class CreditCaseDataDao extends CRUDQDao<CreditCaseData> {
    public static final String NAME = "CreditCaseData" ;

    @Override
    public int Create(CreditCaseData fullItem) {
        String sql = "INSERT INTO " + NAME
                + " (UniqId, UniqType, CreditNoAps, Status, ApsStatus, Idno, UserType, Phone, ProductId, ChtName, EngName, Birthday, Gender, IdCardDate, IdCardLocation, IdCardCRecord, ResAddrZipCode, ResAddr, CommAddrZipCode, CommAddr, HomeAddrZipCode, HomeAddr, BillAddrZipCode, BillAddr, SendCardAddrZipCode, SendCardAddr, Education, Marriage, EmailAddress, ResTelArea, ResTel, HomeTelArea, HomeTel, HomeType, HomeOwner, CorpName, CorpTelArea, CorpTel, CorpTelExten, CorpAddrZipCode, CorpAddr, Occupation, CorpDepart, JobTitle, YearlyIncome, OnBoardDate, SendType, FirstPresent, IpAddress, DataUse, CreateTime, UpdateTime ) "
        + " VALUES (:UniqId,:UniqType,:CreditNoAps,:Status,:ApsStatus,:Idno,:UserType,:Phone,:ProductId,:ChtName,:EngName,:Birthday,:Gender,:IdCardDate,:IdCardLocation,:IdCardCRecord,:ResAddrZipCode,:ResAddr,:CommAddrZipCode,:CommAddr,:HomeAddrZipCode,:HomeAddr,:BillAddrZipCode,:BillAddr,:SendCardAddrZipCode,:SendCardAddr,:Education,:Marriage,:EmailAddress,:ResTelArea,:ResTel,:HomeTelArea,:HomeTel,:HomeType,:HomeOwner,:CorpName,:CorpTelArea,:CorpTel,:CorpTelExten,:CorpAddrZipCode,:CorpAddr,:Occupation,:CorpDepart,:JobTitle,:YearlyIncome,:OnBoardDate,:SendType,:FirstPresent,:IpAddress,:DataUse, getdate(),  getdate()  )";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public CreditCaseData Read(CreditCaseData keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE UniqId = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CreditCaseData.class),
                    new Object[] { keyItem.getUniqId() });
        } catch (DataAccessException ex) {
            logger.error(NAME+"查無資料");           
            return null;
        }
    }

    @Override
    public int Update(CreditCaseData fullItem) {
        String sql = "UPDATE " + NAME + " SET UniqType=:UniqType, CreditNoAps=:CreditNoAps, Status=:Status, ApsStatus=:ApsStatus, Idno=:Idno, UserType=:UserType, Phone=:Phone, ProductId=:ProductId, ChtName=:ChtName, EngName=:EngName, Birthday=:Birthday, Gender=:Gender, IdCardDate=:IdCardDate, IdCardLocation=:IdCardLocation, IdCardCRecord=:IdCardCRecord, ResAddrZipCode=:ResAddrZipCode, ResAddr=:ResAddr, CommAddrZipCode=:CommAddrZipCode, CommAddr=:CommAddr, HomeAddrZipCode=:HomeAddrZipCode, HomeAddr=:HomeAddr, BillAddrZipCode=:BillAddrZipCode, BillAddr=:BillAddr, SendCardAddrZipCode=:SendCardAddrZipCode, SendCardAddr=:SendCardAddr, Education=:Education, Marriage=:Marriage, EmailAddress=:EmailAddress, ResTelArea=:ResTelArea, ResTel=:ResTel, HomeTelArea=:HomeTelArea, HomeTel=:HomeTel, HomeType=:HomeType, HomeOwner=:HomeOwner, CorpName=:CorpName, CorpTelArea=:CorpTelArea, CorpTel=:CorpTel, CorpTelExten=:CorpTelExten, CorpAddrZipCode=:CorpAddrZipCode, CorpAddr=:CorpAddr, Occupation=:Occupation, CorpDepart=:CorpDepart, JobTitle=:JobTitle, YearlyIncome=:YearlyIncome, OnBoardDate=:OnBoardDate, SendType=:SendType, FirstPresent=:FirstPresent, IpAddress=:IpAddress, DataUse=:DataUse, UpdateTime=getdate()"
        + " WHERE UniqId=:UniqId ";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(CreditCaseData keyItem) {
        String sql = "DELETE FROM " + NAME + " WHERE UniqId = ?";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getUniqId() });
    }

    @Override
	public List<CreditCaseData> Query(CreditCaseData keyItem) {
		String sql = "SELECT * FROM " + NAME + " WHERE Idno = ?";					
		try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getIdno() }, new BeanPropertyRowMapper<>(CreditCaseData.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }
    
    public void saveApplyNewCreditcardView(String uniqId, String productId, String ipAddress, ApplyNewCreditcardView view, CaseData caseData) {
        CreditCaseData o = new CreditCaseData();
        o.setUniqId(uniqId);
        o.setUniqType("02"); // 01 : 體驗 02: 申請 03:立約 04:專人聯絡 05:信用卡 12:薪轉 13:數三 14: 中壽
        o.setCreditNoAps("");
        o.setStatus("01"); // 01 填寫中
        o.setApsStatus(""); // TODO:
        o.setIdno(caseData.getIdno());
        o.setUserType("0"); // 0: 新戶 1:信用卡戶 2:存款戶 3:純貸款戶
        o.setPhone(caseData.getMobile_tel());
        o.setProductId(productId); // 519001
        o.setChtName(StringUtils.trimToEmpty(caseData.getCustomer_name()));
        o.setEngName(view.getEnglishName());
        o.setBirthday(StringUtils.trimToEmpty(caseData.getBirthday()));
        o.setGender(StringUtils.trimToEmpty(caseData.getGender()));
        o.setIdCardDate(StringUtils.trimToEmpty(caseData.getIdCardDate()));
        o.setIdCardLocation(StringUtils.trimToEmpty(caseData.getIdCardLocation()));
        o.setIdCardCRecord(StringUtils.trimToEmpty(caseData.getIdCardCRecord()));
        o.setResAddrZipCode(StringUtils.trimToEmpty(caseData.getResAddrZipCode()));
        o.setResAddr(StringUtils.trimToEmpty(caseData.getResAddr()));
        o.setHomeAddrZipCode(StringUtils.trimToEmpty(caseData.getHouse_city_code()));
        o.setHomeAddr(StringUtils.trimToEmpty(caseData.getHouse_address()));
        o.setEducation(StringUtils.trimToEmpty(caseData.getEducation()));
        o.setMarriage(StringUtils.trimToEmpty(caseData.getMarriage()));
        o.setEmailAddress(StringUtils.trimToEmpty(caseData.getEmail_address()));
        o.setResTelArea(StringUtils.trimToEmpty(caseData.getResTelArea()));
        o.setResTel(StringUtils.trimToEmpty(caseData.getResTel()));
        o.setHomeTelArea(StringUtils.trimToEmpty(caseData.getHouse_tel_area()));
        o.setHomeTel(StringUtils.trimToEmpty(caseData.getHouse_tel()));
        o.setHomeType(""); // TODO:
        o.setHomeOwner(StringUtils.trimToEmpty(caseData.getEstate_type()));
        o.setCorpName(StringUtils.trimToEmpty(caseData.getCorp_name()));
        o.setCorpTelArea(StringUtils.trimToEmpty(caseData.getCorp_tel_area()));
        o.setCorpTel(StringUtils.trimToEmpty(caseData.getCorp_tel()));
        o.setCorpTelExten(StringUtils.trimToEmpty(caseData.getCorp_tel_exten()));
        o.setCorpAddrZipCode(StringUtils.trimToEmpty(caseData.getCorp_city_code()));
        o.setCorpAddr(StringUtils.trimToEmpty(caseData.getCorp_address()));
        o.setOccupation(StringUtils.trimToEmpty(caseData.getOccupation()));
        o.setCorpDepart("");
        o.setJobTitle(StringUtils.trimToEmpty(caseData.getTitle()));
        o.setYearlyIncome(StringUtils.trimToEmpty(caseData.getYearly_income()));
        o.setOnBoardDate(StringUtils.substring(StringUtils.trimToEmpty(caseData.getOn_board_date()), 0, 6)); // yyyyMM
        // view
        o.setSendType(view.getBillingType()); // 1:實體帳單 2:電子帳單 @see select * from DropdownData dd where dd.Name = 'BillType' and Enable = 1; 
        o.setFirstPresent(view.getNitoCard()); // @see CreditCardGift.GiftCode 
        o.setIpAddress(ipAddress);
        o.setDataUse("01"); // 01:同意 02:不同意

        String billAddrZipCode = "";
        String billAddr = "";
        // ra1 同住家地址
        if (StringUtils.equals("ra1",view.getMailingAddress())) {
            billAddrZipCode = caseData.getHouse_city_code();
            billAddr = caseData.getHouse_address();
        } 
        // ra2 同公司地址
        else if (StringUtils.equals("ra2",view.getMailingAddress())) {
            billAddrZipCode = caseData.getCorp_city_code();
            billAddr = caseData.getCorp_address();
        } 
        // ra3 自行填寫
        else if (StringUtils.equals("ra3",view.getMailingAddress())) {
            // view
            billAddrZipCode = view.getCity_code();
            billAddr = view.getFullAddress();
        }

        o.setCommAddrZipCode(StringUtils.trimToEmpty(billAddrZipCode));
        o.setCommAddr(StringUtils.trimToEmpty(billAddr));
        o.setSendCardAddrZipCode(StringUtils.trimToEmpty(billAddrZipCode));
        o.setSendCardAddr(StringUtils.trimToEmpty(billAddr));
        o.setBillAddrZipCode(StringUtils.trimToEmpty(billAddrZipCode));
        o.setBillAddr(StringUtils.trimToEmpty(billAddr));

        // o.setCreateTime();
        // o.setUpdateTime();
        CreateOrUpdate(o);
    }
    
    public List<Map<String,Object>> getCreditDataForCheckHighRiskOccupation(String uniqId){
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        String sql = "SELECT Occupation occupation,JobTitle title from CreditCaseData where UniqId=?";
        return jdbcTemplate.queryForList(sql, new Object[]{uniqId});
    }

    public void updateStatusbyUniqId(String uniqid, String strStatus) throws Exception {
    	AccessReferenceMap<String> map = new RandomAccessReferenceMap();
        String indirect_uniqid = map.addDirectReference(uniqid);
        String indirect_strStatus = map.addDirectReference(strStatus);
        uniqid = map.getDirectReference(indirect_uniqid);
        strStatus = map.getDirectReference(indirect_strStatus);
    	
        // 建立DB連線及查詢字串
        JdbcTemplate jdbctemp = this.getJdbcTemplate();

        jdbctemp.update("update CreditCaseData set Status = ?,UpdateTime=GETDATE() where UniqId = ?;", new Object[]{strStatus, uniqid});
    }

    public List<Map<String, Object>> queryWaitPhotoCase() {
        // 取得待處理的專人處理聯絡case名單
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        try {
            /*List<Map<String, Object>> ls = jdbcTemplate.queryForList(
                    "SELECT case when cv.VerifyStatus is null then '0' else cv.VerifyStatus end VerifyStatus,ccd.* FROM CreditCaseData ccd left join CreditVerify cv on ccd.UniqId = cv.UniqId " + 
                    "where ccd.Status in (?,?)",
                    new Object[]{PlContractConst.CASE_STATUS_WRITE_SUCCESS, PlContractConst.CASE_STATUS_SUBMIT_FAIL});*/
        	List<Map<String, Object>> ls = jdbcTemplate.queryForList(
                    "SELECT * FROM CreditCaseData where Status in (?,?)",
                    new Object[]{PlContractConst.CASE_STATUS_WRITE_SUCCESS, PlContractConst.CASE_STATUS_SUBMIT_FAIL});
            return ls;
        } catch (DataAccessException e) {
//            e.printStackTrace();
            return null;
        }

    }

    public List<Map<String, Object>> getCreditCardTypeTitle(String uniqid, String productid) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String nowDate = sdf.format(cal.getTime());
        return jdbcTemplate.queryForList("SELECT ccp.CardType,ccp.CardTitle from CreditCaseData cc " +
                        "INNER JOIN CreditCardProduct ccp on cc.ProductId = ccp.ProductId WHERE cc.UniqId = ? and cc.ProductId = ? AND CONVERT(varchar,ccp.StartDate,112)<= ? " +
                        "AND CONVERT(varchar,ccp.EndDate,112)>= ? AND ccp.CardStatus in ('04','05') ORDER BY ccp.StartDate desc",
                new Object[]{uniqid, productid, nowDate, nowDate});
    }

    //需要修改 新表名稱
    public List<Map<String, Object>> queryCreidtCaseDoc(String caseno) throws Exception {
        // 取得待處理的專人處理聯絡case名單
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        try {
            List<Map<String, Object>> ls = jdbcTemplate.queryForList(
                    "SELECT PdfContent,idno FROM CreditCaseDoc WHERE UniqId = ?",
                    new Object[]{caseno});
            return ls;
        } catch (DataAccessException e) {
//            e.printStackTrace();
            return null;
        }

    }

    public List<Map<String, Object>> queryDatabyUniqId(String uniqid) throws Exception {
        // 取得creditcasedata
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();

        try {
            List<Map<String, Object>> ls = jdbcTemplate.queryForList("SELECT * FROM dbo.CreditCaseData WHERE UniqId = ?",
                    new Object[]{uniqid});
            return ls;
        } catch (DataAccessException e) {
            return null;
        }

    }

    public CreditCaseDataExtend getCreditDataByUniqId(String UniqId) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        try {
            String sql = "SELECT CC.*,dd.DataName as OccupationName,PC.PromoDepart1,PC.PromoMember1,PC.PromoDepart2,PC.PromoMember2 FROM CreditCaseData CC LEFT JOIN QR_PromoCase PC ON CC.UniqId = PC.UniqId LEFT JOIN DropdownData dd on dd.Enable='1' AND dd.Name='occupation' AND dd.DataKey=cc.Occupation WHERE CC.UniqId = ?;";
            return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(CreditCaseDataExtend.class),
                    new Object[]{UniqId});
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public List<Map<String, Object>> getCaseData(String uniqid) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        return jdbcTemplate.queryForList("SELECT cd.p_apy_amount,cc.ProductId,cd.p_purpose,cd.p_purpose_name,cd.ProductId as ProductId2,cd.p_period,cd.p_apy_amount from CreditCaseData cc " +
                        "INNER JOIN CaseData cd on cc.UniqId = cd.CaseNo WHERE cc.UniqId = ?;",
                new Object[]{uniqid});
    }

    public CreditCaseDataExtend getCreditDataExtendById(String UniqId) {
    	try {
	    	AccessReferenceMap<String> map = new RandomAccessReferenceMap();
	        String indirect_UniqId = map.addDirectReference(UniqId);
	        UniqId = map.getDirectReference(indirect_UniqId);
	    	
	        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
	        String sql = "SELECT * FROM CreditCaseData WHERE UniqId = ? AND 1=1;";
        
            return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(CreditCaseDataExtend.class),
                    new Object[]{UniqId});
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (Exception e) {
        	return null;
        }
    }

    //  在有了CreditCaseData之後依照ProductId與FirstPresent取得產品名稱與首刷禮
    public List<Map<String, Object>> getCardGiftName(String FirstPresent, String ProductId) {

        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        String sql = "select CardTitle ,"
                + "(select top(1) GiftTitle from CreditCardGift where GiftCode= ? ) as gift"
                + " from dbo.CreditCardProduct "
                + " where ProductId= ? group by CardTitle";
        try {
            return jdbcTemplate.queryForList
                    (sql, new Object[]{FirstPresent, ProductId});

        } catch (Exception e) {
//            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> getCreditVerify(String uniqId) {

        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();

        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT * FROM CreditVerify where UniqId=?");
            return jdbcTemplate.queryForList(sb.toString(), new Object[]{uniqId});

        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    //產生pdf的時候要用的
    public String getAuthType(String UniqId) {
        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        String result = "";
        String sql = " select AuthType from dbo.CaseAuth where UniqId = ? ";

        List<Map<String, Object>> tempList = jdbcTemplate.queryForList(sql, new Object[]{UniqId});
        if (tempList.size() != 0) {
            result = tempList.get(0).get("AuthType").toString();
        }
        return result;
    }

    //產生pdf的時候要用的 取CreditVerify 的 VerifyStatus資料
    public Map<String, Object> getVerifyStatus(String UniqId) {

        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        String sql = " select VerifyType,VerifyStatus from CreditVerify where UniqId = ? ";

        List<Map<String, Object>> tempList = jdbcTemplate.queryForList(sql, new Object[] { UniqId });
        if (tempList.size() > 0) {
            return tempList.get(0);
        } else {
            return null;
        }
    }

    public CreditEopDataDto queryEOPData(String uniqId){

        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        String sql = " select convert(varchar,cast(shorturl.PayDate1 as date),112) as PayDate1 , case shorturl.PayDate2 when '' then '' else  convert(varchar,cast(shorturl.PayDate2 as date),112) end as PayDate2 ," +
        		" shorturl.CorpID from EOP_EX_CreditCaseData eopCredit " + 
        		"  left join EOP_IdMapping idmapping on eopCredit.UniqId = idmapping.UniqId " + 
        		"  left join EOP_ShortUrl shorturl on idmapping.BatchId = shorturl.BatchId where eopCredit.AirloanUniqId = ? AND 1=1";
        try {
            return jdbcTemplate.queryForObject(sql,new BeanPropertyRowMapper<>(CreditEopDataDto.class),new Object[]{uniqId});
        }catch (Exception e){
			//e.printStackTrace();
            System.out.println("取得新轉撥薪日有誤");
        }
        return null;
    }

    // UniqId抓中壽卡分期[2020.03.20 Alen 新增]
    public List<Map<String, Object>> getCreditCaseStagingByUniqId(String uniqId) {
    	try {
	        
	        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
	        
	        StringBuilder sb = new StringBuilder();
	        sb.append("SELECT UniqId, IsCLCreditCard, IsCheckCLInstallment, InstallmentMonth, InstallmentAgreeTime ");
	        sb.append("FROM CreditCaseStaging where UniqId =? ");
	
            return jdbcTemplate.queryForList(sb.toString(), new Object[]{uniqId});
        } catch (Exception e) {
//            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param UniqId
     * @param UniqType
     * @param idno
     * @param PdfContent 將加密PDF及圖以Base64存入DB
     */
    public void setPDF(String UniqId, String UniqType, String idno, String PdfContent) {

        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("insert into CreditCaseDoc (UniqId, UniqType, idno, PdfContent) "
        		+ " values(?,?,?,?)",
                new Object[]{UniqId, UniqType, idno, PdfContent});

    }

}