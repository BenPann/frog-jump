package com.kgi.airloanex.ap.controller;

import com.kgi.airloanex.ap.service.KgiService;
import com.kgi.airloanex.common.dto.customDto.APIMInfoRspDto;
import com.kgi.eopend3.ap.controller.base.BaseController;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/apim")
public class APIMController extends BaseController {

    private static String context = "APIMController";

    @Autowired
    private KgiService kgiService;

    // APIM戶役政查詢身分證
    @PostMapping("/GET_APIM_DATA")
    public APIMInfoRspDto getAPIMData(@RequestBody String reqBody) throws Exception {
        logger.info("APIM前台輸入的資料" + reqBody);
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return kgiService.call_APIM_info(reqBody);
        } else {
            return null;
        }
    }
    
}
