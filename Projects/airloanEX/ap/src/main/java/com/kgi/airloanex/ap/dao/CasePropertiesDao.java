package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.CaseProperties;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class CasePropertiesDao extends CRUDQDao<CaseProperties> {
    
    @Override
    public int Create(CaseProperties fullItem) {
        String sql = "INSERT INTO "
                + " CaseProperties( UniqId, PropertyName, PropertyDesc, PropertyValue, CreateTime)"
                + "         VALUES(:UniqId,:PropertyName,:PropertyDesc,:PropertyValue, getdate())";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public CaseProperties Read(CaseProperties keyItem) {
        String sql = "SELECT * FROM CaseProperties WHERE UniqId = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseProperties.class), new Object[] { keyItem.getUniqId() });
        } catch (Exception e) {
            logger.error("CaseProperties 查無資料");
            return null;
        }
    }

    @Override
    public int Update(CaseProperties fullItem) {
        String sql = "UPDATE CaseProperties SET UniqId=:UniqId, PropertyName=:PropertyName, PropertyDesc=:PropertyDesc, PropertyValue=:PropertyValue  WHERE UniqId=:UniqId ";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(CaseProperties keyItem) {
        return 0; // TODO:
    }

    @Override
    public List<CaseProperties> Query(CaseProperties keyItem) {
        return null; // TODO:
    }
}
