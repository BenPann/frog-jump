package com.kgi.airloanex.ap.dao;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import com.kgi.eopend3.ap.dao.BaseDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ContractDocDao extends BaseDao {

	@Autowired
	@Qualifier("oracleWD")
	private DataSource oracleWD;

	public void createContractDoc(String conNo) {
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		jdbcTemplate.update("DELETE ContractDoc WHERE ConNo = ? AND 1=1", new Object[] { conNo });
		jdbcTemplate.update("INSERT INTO ContractDoc (ConNo, UTime) VALUES" + "(?,GETDATE())", new Object[] { conNo });
	}

	public int insertContractDoc(String conNo, String Previewcontract) {
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();

		String sql = "UPDATE ContractDoc SET PreviewContent = ?, UTime = GETDATE() WHERE ConNo = ? AND 1=1";
		return jdbcTemplate.update(sql, new Object[] { Previewcontract, conNo });
	}

	public int insertContractDocPhoto(String UniqId, int page, String image) {
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		ArrayList<Object> paramList = new ArrayList<Object>();

		String sql = "INSERT INTO ContractDocPhoto" + "(ConNo, Page, Image) VALUES" + "(?,?,?)";
		paramList.add(UniqId);
		paramList.add(page);
		paramList.add(image);
		return jdbcTemplate.update(sql, paramList.toArray());
	}

	public List<Map<String, Object>> getContractDoc(String conNo) throws Exception {
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		try {
			List<Map<String, Object>> ls = jdbcTemplate.queryForList(
					"SELECT A.*,B.EMail,B.Idno FROM dbo.ContractDoc A JOIN ContractMain B ON A.ConNo=B.UniqId WHERE A.ConNo = ?",
					new Object[] { conNo });
			return ls;
		} catch (DataAccessException e) {
			logger.error("Fail to getContractDoc.", e);
			return null;
		}

	}

	public void uploadSign(String uniqId, String sign) {
		JdbcTemplate jdbctemp = this.getJdbcTemplate();

		jdbctemp.update("UPDATE ContractDoc SET UserSign = ? WHERE ConNo = ? AND 1=1", new Object[] { sign, uniqId });

	}

	public void updateContractPdf(String conNo, String strContractPdf) {
		JdbcTemplate jdbctemp = this.getJdbcTemplate();
		jdbctemp.update("UPDATE ContractDoc set PdfContent = ? where ConNo = ?;",
				new Object[] { strContractPdf, conNo });
	}

	public List<Map<String, Object>> getContractDocPhoto(String conNo) throws Exception {
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		try {
			List<Map<String, Object>> ls = jdbcTemplate
					.queryForList("SELECT * FROM dbo.ContractDocPhoto WHERE ConNo = ?", new Object[] { conNo });
			return ls;
		} catch (DataAccessException e) {
//			e.printStackTrace();
			return null;
		}

	}
	
	public void deleteContractDocPhoto(String conNo) throws Exception {
		// 清送件用契約檔
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();

		jdbcTemplate.update("delete dbo.ContractDocPhoto WHERE ConNo = ?", new Object[] { conNo });
	}
	
	//[2019.09.06 GaryLiu] ==== START : 生成隨機字串 ====
	public String randomString(int count) {
		StringBuilder sb = new StringBuilder();
		while (count-- != 0) {
			int character = new SecureRandom().nextInt(26);
			sb.append((char)(character + 65));
		}
		return sb.toString();
	}
	//[2019.09.06 GaryLiu] ====  END  : 生成隨機字串 ====
}
