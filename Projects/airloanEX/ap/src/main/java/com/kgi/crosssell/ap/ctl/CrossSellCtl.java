package com.kgi.crosssell.ap.ctl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.QR_ChannelListDao;
import com.kgi.airloanex.common.dto.db.MemoData;
import com.kgi.airloanex.common.dto.db.QR_ChannelList;
import com.kgi.airloanex.common.dto.view.CSCommonGetDataView;
import com.kgi.airloanex.common.dto.view.CSGetDataView;
import com.kgi.airloanex.common.dto.view.CSInsertFucoApiLog;
import com.kgi.crosssell.ap.dao.MemoDataDao;
import com.kgi.crosssell.ap.service.APIMService;
import com.kgi.crosssell.ap.service.APSTmpService;
import com.kgi.crosssell.ap.service.ApplyLoanService;
import com.kgi.crosssell.ap.service.CrossSellService;
import com.kgi.crosssell.ap.service.FucoApiLogService;
import com.kgi.crosssell.ap.service.MessageQueueService;
import com.kgi.crosssell.common.SystemConst;
import com.kgi.crosssell.common.dto.CSCommon;
import com.kgi.crosssell.common.dto.CSCommonImage;
import com.kgi.crosssell.common.dto.CSCrossBusinessApply;
import com.kgi.crosssell.common.dto.CSStkCustInfo;
import com.kgi.crosssell.common.dto.CS_PersonalData;
import com.kgi.crosssell.common.util.XmlParser;
import com.kgi.eopend3.ap.service.SNService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.helper.StringUtil;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import net.sf.json.JSONObject;


@RestController
@RequestMapping("/cs")
public class CrossSellCtl {

	private static final Logger logger = LogManager.getLogger(CrossSellCtl.class.getName());
	@Autowired
	private CrossSellService crossSellServ;

	@Autowired
	private APIMService apimServ;

	@Autowired
	private APSTmpService apsServ;

	@Autowired
	private SNService SNSer;

	@Autowired
	private QR_ChannelListDao channelDao;

	@Autowired
	private MemoDataDao memoDataDao;

	@Autowired
	private ApplyLoanService applyLoanService;

	@Autowired
	private MessageQueueService MQService;

	@Autowired
	private FucoApiLogService fucoApiLogService;

    @Autowired
    AirloanEXConfig globalConfig;

	// 20181221
	// 1. 用channelId取得 channelist資料 (至少Eanble要啟用，異業入口時需再額外檢查CIF相關欄位 )
	// 2. 取得序號(uniqId和uniqType)
	// 3-1跨售證卷 如果token和idno不等於空值 就去取aumdata
	// 3-2異業入口 取得客戶提供的CIF資料
	// 4 把 uniqId、uniqType、需要導頁的網址(信用卡/貸款)、證卷/異業資料、商銀/非商銀帶到前端

	// 非0000或者取得跨售資料 也會都會往前導

	@PostMapping("/getCSData")
	// [2019.10.01 GaryLiu] ==== START : 修改Parameter Tampering ====
	// [2019.10.01 GaryLiu] 嘗試移除用Annotation的驗證方式，交給ESAPI判斷
	public String getCSData(@RequestBody String reqBody) {
		// public String getCSData(String prodid,@NotNull @NotNull
		// @Pattern(regexp="^[^<>;'`]+$",message="the string can't include
		// \"^,<,>,;,',`\"") String channelId, String token, String idno, String ip) {
		// [2019.10.01 GaryLiu] ==== END : 修改Parameter Tampering ====
		// [2019.09.02 GaryLiu] ==== START : 搬移程式碼位置，以符合白箱需求 ====
		System.out.println("進入 getCSData");

		String uniqId = "";
		String uniqType = "";
		String url = "";
		String redirectUrl = "";

		JSONObject res = new JSONObject();

		try {

			CSGetDataView view = new Gson().fromJson(reqBody, CSGetDataView.class);

			String prodid = view.getProdid();
			String channelId = view.getChannelId();
			String token = view.getToken();
			String idno = view.getIdno();

			System.out.println("prodid=" + prodid);
			System.out.println("channelId=" + channelId);
			System.out.println("token=" + token);
			System.out.println("idno=" + idno);

			// prodid = ESAPI.validator().getValidInput("", prodid, "AntiXSS",
			// Integer.MAX_VALUE, false);
			// channelId = ESAPI.validator().getValidInput("", channelId, "AntiXSS",
			// Integer.MAX_VALUE, false);
			// token = ESAPI.validator().getValidInput("", token, "AntiXSS",
			// Integer.MAX_VALUE, false);
			// idno = ESAPI.validator().getValidInput("", idno, "AntiXSS",
			// Integer.MAX_VALUE, false);
			// ip = ESAPI.validator().getValidInput("", ip, "AntiXSS", Integer.MAX_VALUE,
			// false);

			if (prodid.equals("loan")) {
				uniqType = "02";
				prodid = "2";
				url = "/apply/init";
				redirectUrl = globalConfig.CrossSellCCUrl();
			} else if (prodid.equals("credit")) {
				uniqType = "05";
				prodid = "1";
				url = "/applycc/init";
			}
			// 取得此channel的所有資料
			QR_ChannelList channel = channelDao.Read(new QR_ChannelList(channelId));
			// 如果channel啟用 且 cApiSetting 有設定
			if (channel != null && channel.getEnable().equals("1")) {
				// 取得序號
				uniqId = SNSer.getUniqId(uniqType);
				System.out.println("#### 呼叫 /getCSData 取信用卡案編=" + uniqId + "");
				res.put("UniqId", uniqId);
				res.put("UniqType", uniqType);
				res.put("channelType", channel.getChannelType());
				res.put("url", url);
				res.put("redirectUrl", redirectUrl);
				// 判斷token和idno是否有值
				if (!token.equals("") && !idno.equals("")) {
					System.out.println("token 和 idno 都不等於空值，將透過APIM向證券取得客戶資料(token=" + token + ")");
					// String csAumResult = "";
					try {
						// 透過APIM向證券取得客戶資料		
						CSCrossBusinessApply crossBusinessApply = apimServ.getCrossBusinessInfo("/v1/CROSS_CO/getDataFromURL?URL=" + channel.getAdditionalDataUrl(), token);
						
						// TODO 測試階段暫時放行，切記改回
						// if(!idno.equals(crossBusinessApply.getIdno())){
						// 	throw new RuntimeException("個人資料驗證失敗");
						// }

						CSStkCustInfo stkCust = apimServ.getStkCustInfo("/v1/CROSS_CO/getDataFromURL?URL=" + channel.getCifUrl(), crossBusinessApply.getIdno());

						crossSellServ.createCS(uniqId, uniqType, stkCust, crossBusinessApply, idno, prodid); // 寫入CS_PersonalData,CS_VerifyLog,CS_CommonData...

						CS_PersonalData cPersonalData = crossSellServ.Read(new CS_PersonalData(uniqId));
						if (cPersonalData != null) {
							// System.out.println("PersonalData:"+JSONObject.fromObject(cPersonalData).toString());
							res.put("PersonalData", JSONObject.fromObject(cPersonalData).toString());
						} else {
							System.out.println("aum資料回傳的checkcode非0000");
							res.put("PersonalData", "error");
						}
					} catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
						System.out.println("取得跨售aum資料失敗");
						System.out.println(e.getMessage());
						res.put("PersonalData", "error");
					}
				} else {
					System.out.println("不需要取得aum資料");
					res.put("PersonalData", "");
				}

			} else {
				throw new RuntimeException("此Channel未設定或Channel未啟用或未定義跨售取得AUM的API");
			}
		} catch (IntrusionException
		// | ValidationException
		e) {
			return "input validation failed";
		} catch (Exception e) {
			// TODO: handle exception
			// System.out.println(e.getMessage());
			// 不反查 直接回傳
			res.put("url", url);
		}
		// [2019.09.02 GaryLiu] ==== END : 搬移程式碼位置，以符合白箱需求 ====

		// [2019.08.26 GaeyLiu] ==== START : 增加ESAPI驗證 ====
		try {
			// [2019.08.21 GaryLiu] ==== START : 移除Encode.forHtml ====
			// return ESAPI.validator().getValidInput("", res.toString(), "AntiXSS",
			// Integer.MAX_VALUE, false);
			if (ESAPI.validator().isValidInput("", res.toString(), "AntiXSS", Integer.MAX_VALUE, false))
				return res.toString();
			else
				return "";
			// [2019.08.21 GaryLiu] ==== END : 移除Encode.forHtml ====
		} catch (IntrusionException e) {
			logger.info("Exception ESAPI : ", e);
			return "";
		} catch (Exception e) {
			logger.info("Exception : ", e);
			return "";
		}
		// [2019.08.26 GaeyLiu] ==== END : 增加ESAPI驗證 ====
	}

	
	@PostMapping("/getCSCommonData")
	// [2019.10.01 GaryLiu] ==== START : 修正DB Parameter Tampering ====
	// [2019.10.01 GaryLiu] 嘗試移除用Annotation的驗證方式，交給ESAPI判斷
	public String getCSCommonData(@RequestBody String reqBody) throws IOException {
		// public String getCSCommonData(String prodId, String channelId,@NotNull
		// @Pattern(regexp="^[^<>;'`]+$",message="the string can't include
		// \"^,<,>,;,',`\"") String token) throws IOException {
		// [2019.10.01 GaryLiu] ==== END : 修正DB Parameter Tampering ====
		// [2019.09.02 GaryLiu] ==== START : 搬移程式碼位置，以符合白箱需求 ====
		String uniqId = "";
		String uniqType = "";
		String url = "";
		String redirectUrl = "";
		JSONObject res = new JSONObject();
		try {

			CSCommonGetDataView view = new Gson().fromJson(reqBody, CSCommonGetDataView.class);
			
			String prodId = view.getProdId();
			String prodType = view.getProdType();
			String channelId = view.getChannelId();
			String token = view.getToken();

			// [2019.10.31 GaryLiu] ==== START : 白箱 : 修改 DB Parameter Tampering ====
			ESAPI.validator().isValidInput("", token, "AntiXSS", 32, false);
			// [2019.10.31 GaryLiu] ==== END : 白箱 : 修改 DB Parameter Tampering ====
			prodId = ESAPI.validator().getValidInput("", prodId, "AntiXSS", 32, false);
			channelId = ESAPI.validator().getValidInput("", channelId, "AntiXSS", 32, false);
			// token = ESAPI.validator().getValidInput("", token, "AntiXSS", 32, false);

			// 先依據產品取得一些預設值
			if (prodId.equals("loan")) {
				uniqType = "02";
				prodId = "2";
				url = "/apply/init";
				if ("PL".equals(prodType)){
					redirectUrl = globalConfig.CrossSellLoanPLUrl();
				} else if("RPL".equals(prodType)){
					redirectUrl = globalConfig.CrossSellLoanRPLUrl();
				}
			} else if (prodId.equals("credit")) {
				uniqType = "05";
				prodId = "1";
				url = "/applycc/init";
			}

			// 取得此channel的所有資料
			QR_ChannelList channel = channelDao.Read(new QR_ChannelList(channelId));
			// 如果啟用 且 需要反查cif
			if (channel != null && channel.getEnable().equals("1") && channel.getGetCif().equals("1")) {
				// 先判斷日期是否有輸入
				if (channel.getGetCifStartTime() == null || channel.getGetCifEndTime() == null) {
					throw new RuntimeException("未設定反查開始結束日期");
				}
				Date now = new Date();
				if (channel.getGetCifStartTime().after(now) || channel.getGetCifEndTime().before(now)) {
					throw new RuntimeException("本日不再反查日期之內");
				}

				// 取得序號
				uniqId = SNSer.getUniqId(uniqType);
				System.out.println("#### 呼叫 /getCSCommonData 取案編=" + uniqId + "");

				System.out.println("#### 開始查詢異業資料 (channel=" + channel.getChannelId() + ", uniqid=" + uniqId + ")");

				JSONObject dataWithAdditionalData = new JSONObject();
				CSCommon data = new CSCommon();

				// 取得客戶提供的CIF資料
				if (channel.getChannelId().equals("RT")) {

					// For pchome
					dataWithAdditionalData = crossSellServ.getCSCommonCifWithAdditionalData(channel, token);
					System.out
							.println("#### getCSCommonCifWithAdditionalData 回傳: " + dataWithAdditionalData.toString());

				} else {

					data = crossSellServ.getCSCommonCif(channel, token);
					System.out.println("#### getCSCommonCif 回傳: " + JSONObject.fromObject(data).toString());

				}

				// 儲存CIF
				MemoData cifData = new MemoData(uniqId, uniqType, SystemConst.MEMO_CSCOMMON_CIF);

				if (channel.getChannelId().equals("RT")) {
					cifData.setMemo(dataWithAdditionalData.toString());

				} else {
					cifData.setMemo(JSONObject.fromObject(data).toString());
				}

				this.memoDataDao.Create(cifData);

				// --把TOKEN，UNIQID存入TABLE:MemoDataTokenInfo--start
				try {
					// add @ 2021-03-15 : 把案編與Token紀錄於CaseProperties
					this.memoDataDao.SaveToMemoTokenInfo(uniqId, token, channel.getChannelId());

				} catch (Exception e) {
					// skip any error
				}

				System.out.println("#### " + uniqId + " Save To MemoData & CaseProperties 成功");

				// --把TOKEN，UNIQID存入TABLE:MemoDataTokenInfo--end

				if (channel.getMemoUrl() != null && !channel.getMemoUrl().equals("")) {
					String memo = crossSellServ.getCSCommonMemo(channel, token);
					JSONObject memoJson = JSONObject.fromObject(memo);
					if (memoJson.optString("checkCode", "1").equals("")) {
						// 儲存MEMO
						MemoData memoData = new MemoData(uniqId, uniqType, SystemConst.MEMO_CSCOMMON_MEMO);
						memoData.setMemo(memoJson.optJSONObject("data").toString());
						// 正常有回資料
						this.memoDataDao.Create(memoData);
					}
				}

				// 不是RT者，存圖片
				if (!channel.getChannelId().equals("RT")) {

					// 空白才是正常
					if (data.getCheckCode().equals("")) {
						// 儲存圖片 依據每個type儲存到addphoto
						CSCommonImage image = data.getData().getImage();
						if (image != null) {
							// 將圖片處理並儲存至DB
							crossSellServ.processImage(uniqId, prodId, image);
						}
						// 將圖片的內容移除
						data.getData().setImage(new CSCommonImage());
					} else {
						// 有錯誤 直接回傳
						return JSONObject.fromObject(data).toString();
					}

				}

				res.put("UniqId", uniqId);
				res.put("UniqType", uniqType);
				res.put("channelType", channel.getChannelType());
				res.put("url", url);
				res.put("redirectUrl", redirectUrl);
				if (channel.getChannelId().equals("RT")) {
					res.put("CSCommon", dataWithAdditionalData);
				} else {
					res.put("CSCommon", data);
				}

			} else {
				throw new RuntimeException("此Channel未設定或未設定為反查CIF或Channel未啟用");
			}
		} catch (IntrusionException | ValidationException e) {
			return "input validation failed";
		} catch (Exception e) {
			// e.printStackTrace();
			// System.out.println(e.getMessage());
			// 不反查 直接回傳
			res.put("url", url);

		}
		// [2019.09.02 GaryLiu] ==== END : 搬移程式碼位置，以符合白箱需求 ====

		try {
			// [2019.08.21 GaryLiu] ==== START : 移除Encode.forHtml ====
			// [2019.08.26 GaeyLiu] ==== START : 增加ESAPI驗證 ====
			return // ESAPI.validator().getValidInput("",
			res.toString()
			// , "AntiXSS", Integer.MAX_VALUE, false)
			;
			// [2019.08.26 GaeyLiu] ==== END : 增加ESAPI驗證 ====
			// [2019.08.21 GaryLiu] ==== END : 移除Encode.forHtml ====
		} catch (IntrusionException
		// | ValidationException
		e) {
			logger.info("Exception ESAPI : ", e);
			return "";
		} catch (Exception e) {
			logger.info("Exception : ", e);
			return "";
		}

	}

	@PostMapping("/insertFucoApiLog")
	public void insertFucoApiLog(@RequestBody String reqBody) {
		
		try {
			CSInsertFucoApiLog view = new Gson().fromJson(reqBody, CSInsertFucoApiLog.class);

			crossSellServ.insertFucoApiLog(view);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@RequestMapping(value = "/getCsAdditionalData", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
	@ResponseBody
	public String getCsAdditionalData(HttpServletRequest request, @RequestBody String reqStr) {

		JSONObject reqJson = JSONObject.fromObject(reqStr);
		String UniqId = reqJson.optString("UniqId", ""); // 可能是案件編號，也可能是CASENOWEB
		String inUniqId = UniqId; // 記下原始傳入uniqid
		String rtncheckCode = "99"; // default
		String errorJsonString = "{\r\n" + "\"token\":\"\",\r\n" + "\"agreeInfo\":\"\",\r\n" + "\"ipAdress\":\"\",\r\n"
				+ "\"eddaBankCode\":\"\",\r\n" + "\"eddaBankActNo\":\"\",\r\n" + "\"pcode2566BankCode\":\"\",\r\n"
				+ "\"pcode2566BankActNo\":\"\",\r\n" + "\"businessRevenueYear\":\"\",\r\n"
				+ "\"businessRevenuePerMonth\":\"\",\r\n" + "\"transAmountYear\":\"\",\r\n" + "\"refundRate\":\"\",\r\n"
				+ "\"suspendPaymentRate\":\"\",\r\n" + "\"avgPricePrevMonth\":\"\",\r\n"
				+ "\"avgPricePrevYear\":\"\",\r\n" + "\"redirectUrlAfterfinish\":\"\",\r\n"
				+ "\"creditLimit\":\"\",\r\n" + "\"rate\":\"\",\r\n" + "\"mobile\":\"\",\r\n" + "\"prjCode\":\"\"\r\n"
				+ "}";
		System.out.println("<<<< [" + inUniqId + "] getCsAdditionalData in ");

		String result = "";
		JSONObject errorJson = new JSONObject();
		try {

			if (StringUtil.isBlank(UniqId)) {
				rtncheckCode = "01";
				throw new Exception("UniqId id empty!");
			}

			// 如果是用理債編號來查
			if (UniqId.indexOf("CASE") == -1) {
				// 取得真正案件編號
				UniqId = applyLoanService.getCaseNoByCaseNoWeb(UniqId);
				System.out.println("#### [" + inUniqId + "] getCsAdditionalData in (Final UniqId=" + UniqId + ")");
			}

			// 取驗身結果
			String creditVerifyResult = applyLoanService.getCreditVerifyByCaseNo(UniqId);

			// 取實際驗身的銀行代號與帳號
			String sAuth2566BankId = "";
			String sAuth2566AccountNo = "";
			if (!creditVerifyResult.equals("null") && creditVerifyResult.length() > 0) {
				JSONObject creditVerify_object = new JSONObject();
				creditVerify_object = JSONObject.fromObject(creditVerifyResult);
				sAuth2566BankId = creditVerify_object.getString("bankId");
				sAuth2566AccountNo = creditVerify_object.getString("accountNo");
			}

			// 20200914測試假資料寫死
			// String rtnData =
			// "{\"businessRevenueYear\":\"200\",\"businessRevenuePerMonth\":\"20;20;20;40;20;20;20;40;20;20;20;40\",\"creditLimit\":\"120\",\"rate\":\"1.40\",\"mobile\":\"0955555555\",\"prjCode\":\"A1111\"}";
			// JSONObject json = JSONObject.fromObject(rtnData);
			// UniqId="CASE2020021300032";
			MemoData memoData = new MemoData(UniqId, "", SystemConst.MEMO_CSCOMMON_CIF);
			memoData = memoDataDao.Read(memoData);
			if (null != memoData) {
				JSONObject json2 = JSONObject.fromObject(memoData.getMemo());
				JSONObject json3 = json2.getJSONObject("data");
				JSONObject json4 = new JSONObject();
				if (!StringUtil.isBlank(json3.getString("additionalData"))) {
					json4 = json3.getJSONObject("additionalData");
					json4.put("pcode2566BankCodeAuth", sAuth2566BankId);
					json4.put("pcode2566BankActNoAuth", sAuth2566AccountNo);
					json4.put("checkCode", "");
					json4.put("message", "");
					result = json4.toString();
				} else {
					errorJson = JSONObject.fromObject(errorJsonString);
					errorJson.put("pcode2566BankCodeAuth", "");
					errorJson.put("pcode2566BankActNoAuth", "");
					errorJson.put("checkCode", "02");
					errorJson.put("message", "additionalData is empty or not found");
					result = errorJson.toString();
				}

			} else {
				errorJson = JSONObject.fromObject(errorJsonString);
				errorJson.put("pcode2566BankCodeAuth", "");
				errorJson.put("pcode2566BankActNoAuth", "");
				errorJson.put("checkCode", "02");
				errorJson.put("message", "crosscell data not found!");
				result = errorJson.toString();
			}

		} catch (Exception e) {
			// TODO: handle exception
			// logger.error(e.getMessage());
			errorJson = JSONObject.fromObject(errorJsonString);
			errorJson.put("pcode2566BankCodeAuth", "");
			errorJson.put("pcode2566BankActNoAuth", "");
			errorJson.put("checkCode", rtncheckCode);
			errorJson.put("message", e.getMessage());
			result = errorJson.toString();
		} finally {
			// 寫log
			fucoApiLogService.insertFucoApiLog("/cs/getCsAdditionalData", reqStr, result);

		}

		logger.info(">>>> [" + UniqId + "] getCsAdditionalData finish!");
		return result;

	}

	@RequestMapping(value = "/getCsMemoData", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
	@ResponseBody
	public String getCsMemoData(HttpServletRequest request, @RequestBody String reqStr) {

		JSONObject reqJson = JSONObject.fromObject(reqStr);
		String UniqId = reqJson.optString("UniqId", ""); // 可能是案件編號，也可能是CASENOWEB

		System.out.println("#### 1.getCsMemoData in (UniqId=" + UniqId + ")");

		String result = "";
		JSONObject errorJson = new JSONObject();
		try {
			logger.info("call getCsMemoData (UniqId:" + UniqId + ")");
			if (StringUtil.isBlank(UniqId)) {
				errorJson.put("checkCode", "01");
				errorJson.put("message", "can't get UniqId!");
			}
			if (UniqId.indexOf("CASE") == -1) {
				UniqId = applyLoanService.getCaseNoByCaseNoWeb(UniqId);
				System.out.println("#### getCsAdditionalData in (Final UniqId=" + UniqId + ")");

			}
			// 20200914測試假資料寫死
			// String rtnData =
			// "{\"businessRevenueYear\":\"200\",\"businessRevenuePerMonth\":\"20;20;20;40;20;20;20;40;20;20;20;40\",\"creditLimit\":\"120\",\"rate\":\"1.40\",\"mobile\":\"0955555555\",\"prjCode\":\"A1111\"}";
			// JSONObject json = JSONObject.fromObject(rtnData);
			// UniqId="CASE2020021300032";
			MemoData memoData = new MemoData(UniqId, "", SystemConst.MEMO_CSCOMMON_CIF);
			memoData = memoDataDao.Read(memoData);

			if (null != memoData) {

				JSONObject json2 = JSONObject.fromObject(memoData.getMemo());
				JSONObject jsonDataOfMemo = new JSONObject();
				if (!StringUtil.isBlank(json2.getString("data"))) {
					jsonDataOfMemo = json2.getJSONObject("data");
					jsonDataOfMemo.put("checkCode", "");
					jsonDataOfMemo.put("message", "success");
					result = jsonDataOfMemo.toString();
				} else {
					errorJson.put("checkCode", "02");
					errorJson.put("message", "data object of memo is empty or not found");
					result = errorJson.toString();
				}

			} else {

				errorJson.put("checkCode", "02");
				errorJson.put("message", "crosscell data not found!");
				result = errorJson.toString();

			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getMessage());
			errorJson.put("checkCode", "99");
			errorJson.put("message", e.getMessage());
			result = errorJson.toString();
		}

		logger.info("getCsMemoData done!");
		return result;
	}

	@RequestMapping(value = "/getCsEddaData", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
	@ResponseBody
	public String getCsEddaData(HttpServletRequest request, @RequestBody String reqStr) {

		JSONObject reqJson = JSONObject.fromObject(reqStr);
		String idno = reqJson.optString("IdNo", ""); // 客戶身分證
		String UniqId = "";
		String rtncheckCode = "99"; // default
		String result = "";

		// make default return format
		JSONObject jsonEdda = new JSONObject();
		jsonEdda.put("checkCode", "");
		jsonEdda.put("message", "");
		jsonEdda.put("eddaBankCode", "");
		jsonEdda.put("eddaBankActNo", "");

		try {

			if (StringUtil.isBlank(idno)) {
				rtncheckCode = "01";
				throw new Exception("idno is not found!");
			}
			if (idno.length() != 10) {
				rtncheckCode = "01";
				throw new Exception("idno format is error!");
			}

			System.out.println("<<<< getCsEddaData in (idno=" + idno.substring(0, 7) + "***)");

			// use idno to query casedata to find uniqid
			UniqId = applyLoanService.getCaseNoByIdNo(idno);
			System.out.println("#### getCsEddaData in (Find UniqId=" + UniqId + ")");

			MemoData memoData = new MemoData(UniqId, "", SystemConst.MEMO_CSCOMMON_CIF);
			memoData = memoDataDao.Read(memoData);
			if (null != memoData) {
				JSONObject jsonMemoData = JSONObject.fromObject(memoData.getMemo());
				JSONObject jsonData = jsonMemoData.getJSONObject("data");

				if (!StringUtil.isBlank(jsonData.getString("additionalData"))) {
					JSONObject additionalData = jsonData.getJSONObject("additionalData");

					jsonEdda.put("checkCode", "");
					jsonEdda.put("message", "success");
					jsonEdda.put("eddaBankCode", additionalData.getString("eddaBankCode"));
					jsonEdda.put("eddaBankActNo", additionalData.getString("eddaBankActNo"));

					result = jsonEdda.toString();
				} else {
					jsonEdda.put("checkCode", "02");
					jsonEdda.put("message", "EDDA Data is empty or not found");
					result = jsonEdda.toString();
				}

			} else {
				jsonEdda.put("checkCode", "02");
				jsonEdda.put("message", "crosscell data not found!");
				result = jsonEdda.toString();
			}

		} catch (Exception e) {
			jsonEdda.put("checkCode", rtncheckCode);
			jsonEdda.put("message", e.getMessage());
			result = jsonEdda.toString();
		} finally {
			// 寫log
			fucoApiLogService.insertFucoApiLog("/cs/getCsEddaData", reqStr, result);

		}

		// logger.info("getCsMemoData done!");
		System.out.println(">>>> getCsEddaData return: " + result + "");
		return result;
	}

	public static String blankIfNull(String s) {
		return s == null ? "" : s;
	}

	// 提供支付連或異業查詢案件狀態
	// notes: 因為支付連只有RPL，用TOKEN找案件時，找01跟02/12者即可
	@RequestMapping(value = "/getCrossSellCaseStatus", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
	@ResponseBody
	public String getCrossSellCaseStatus(HttpServletRequest request, @RequestBody String reqJsonData) {

		JSONObject reqJson = JSONObject.fromObject(reqJsonData);
		String uniqToken = reqJson.optString("token", "");
		String targetChannel = reqJson.optString("channel", "");

		String timeStampStart = "";
		String timeStampCurrent = "";
		String timeStampEnd = "";

		if (targetChannel.equals("")) {
			targetChannel = "RT"; // default
		}

		System.out.println("<<<< getCrossSellCaseStatus (channel=" + targetChannel + ", token=" + uniqToken + ")");

		String result = "";
		String CaseNo = "";

		// check request

		// make default return format
		JSONObject jsonRtn = new JSONObject();
		jsonRtn.put("checkCode", "");
		jsonRtn.put("message", "");
		jsonRtn.put("token", uniqToken);
		jsonRtn.put("kgiCaseNo", "");
		jsonRtn.put("caseStatusCode", "");
		jsonRtn.put("caseStatusDesc", "");

		timeStampStart = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(Calendar.getInstance().getTime());
		System.out.println("#### " + targetChannel + " 呼叫狀態查詢 Start @ " + timeStampStart);

		// 01:參數錯誤, 02:查無案件，99:其他非預期錯誤

		try {

			if (uniqToken.equals("")) {

				jsonRtn.put("checkCode", "01");
				jsonRtn.put("message", "參數錯誤");
				// goto RTN

			} else {

				// old method : slowly
				timeStampCurrent = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(Calendar.getInstance().getTime());
				System.out.println("#### " + targetChannel + " 呼叫狀態查詢(getCaseNoByToken) Start @ " + timeStampCurrent);

				// CaseNo = applyLoanService.getCaseNoByToken(uniqToken);

				// new method : faster
				CaseNo = applyLoanService.getCaseNoByTokenUseMemoTokenInfo(uniqToken);
				if (CaseNo.equals("") || CaseNo.equals(null)) {
					CaseNo = applyLoanService.getCaseNoByToken(uniqToken); // 比較慢

					if (!CaseNo.equals("") && !CaseNo.equals(null)) {

						// 補寫MemoTokenInfo
						try {
							this.memoDataDao.SaveToMemoTokenInfo(CaseNo, uniqToken, "RT");
							System.out.println("#### 補寫 MemoTokenInfo (" + CaseNo + "," + uniqToken + ") 完成");
						} catch (Exception e) {

						}
					}

				}

				timeStampCurrent = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(Calendar.getInstance().getTime());
				System.out.println("#### " + targetChannel + " 呼叫狀態查詢(getCaseNoByToken) end   @ " + timeStampCurrent);

				// use idno to query casedata to find uniqid
				System.out.println("#### applyLoanService.getCaseNoByToken (" + CaseNo + ")");

				if (CaseNo == null || CaseNo.equals("")) {
					jsonRtn.put("checkCode", "02");
					jsonRtn.put("message", "查無申請案件");
					jsonRtn.put("kgiCaseNo", "");
					jsonRtn.put("caseStatusCode", "0");
					jsonRtn.put("caseStatusDesc", "尚未申請");
					// goto RTN

				} else {

					String applyData = applyLoanService.getApplyDataByCaseNo(CaseNo);

					// System.out.println("#### applyData=" + applyData + "");
					JSONObject jsonCaseData = JSONObject.fromObject(applyData);
					String CaseStatus = jsonCaseData.getString("Status");
					String CaseStatusAPS = jsonCaseData.getString("ApsStatus");

					if (CaseStatus.equals("null") || CaseStatus.equals("")) {
						CaseStatus = "00";
					} // default
					if (CaseStatusAPS.equals("null") || CaseStatusAPS.equals("")) {
						CaseStatusAPS = "0";
					} // default

					System.out.println("#### CaseStatus=" + CaseStatus + "");
					System.out.println("#### CaseStatusAPS(from WEB)=" + CaseStatusAPS + "");

					// 20201209 added, 即時查詢APS狀態
					String sCaseNoWeb = blankIfNull(jsonCaseData.getString("CaseNoWeb"));
					System.out.println("#### CaseNoWeb(from WEB)=" + sCaseNoWeb + "");

					if (sCaseNoWeb.equals("null") || sCaseNoWeb.equals("")) {

						System.out.println("#### " + CaseNo + " 尚未起案，略過發查APS狀態");

					} else {

						timeStampCurrent = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss")
								.format(Calendar.getInstance().getTime());
						System.out.println(
								"#### " + targetChannel + " 呼叫狀態查詢(getDGTCaseInfo) start @ " + timeStampCurrent);

						String CaseInfoInAPS = apsServ.getDGTCaseInfo(sCaseNoWeb);
						System.out.println("#### " + CaseNo + " CaseInfoInAPS = " + CaseInfoInAPS);

						timeStampCurrent = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss")
								.format(Calendar.getInstance().getTime());
						System.out.println(
								"#### " + targetChannel + " 呼叫狀態查詢(getDGTCaseInfo) end   @ " + timeStampCurrent);

						JSONObject jsonCaseInfoInAPS = new JSONObject();
						jsonCaseInfoInAPS = JSONObject.fromObject(CaseInfoInAPS);

						try {
							System.out
									.println("#### APS_STATUS(from APS)=" + jsonCaseInfoInAPS.getString("APS_STATUS"));
							if (jsonCaseInfoInAPS.getString("APS_STATUS") != null) {
								CaseStatusAPS = jsonCaseInfoInAPS.getString("APS_STATUS");
							}
						} catch (Exception e) {
							// APS_STATUS not found ,skip
						}

					}

					timeStampCurrent = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss")
							.format(Calendar.getInstance().getTime());
					System.out
							.println("#### " + targetChannel + " 呼叫狀態查詢(GMHasLimitStart) start @ " + timeStampCurrent);

					// 判斷是否可貸款的初步檢核規則如下(須全部符合):
					// SO1814(現況)第一碼 = '0' 非假扣押
					// SO1814(現況)第三碼 = '0' 正常
					// SO1824(契約書有效期限)>=本日(15:30後算次日)
					// SO1825(目前授信額度)>0
					// SO1829(可用餘額)>0
					// SO1834(下次應繳日)>=本日(15:30後算次日) or = 00000000

					// example: <SO1801>H114460530
					// </SO1801><SO1802>886180183935001</SO1802><SO1803>0000</SO1803><SO1804>20201201</SO1804><SO1805>0000</SO1805><SO1806>886101840711</SO1806><SO1807>0</SO1807><SO1808>8861</SO1808><SO1809>夜間啟額測</SO1809><SO1810>
					// </SO1810><SO1811>19970101</SO1811><SO1812>2</SO1812><SO1813>00</SO1813><SO1814>000</SO1814><SO1815>886180183935001</SO1815><SO1816>0</SO1816><SO1817>6</SO1817><SO1818>1</SO1818><SO1819>0</SO1819><SO1820>
					// </SO1820><SO1821>D1</SO1821><SO1822>20201123</SO1822><SO1823>00000000</SO1823><SO1824>20211123</SO1824><SO1825>0</SO1825><SO1826>99</SO1826><SO1827>100000.00</SO1827><SO1828>0</SO1828><SO1829>0</SO1829><SO1830>13.990</SO1830><SO1831>0</SO1831><SO1832>00000000</SO1832><SO1833>00000000</SO1833><SO1834>00000000</SO1834><SO1835>0</SO1835><SO1836>0</SO1836><SO1837>0</SO1837><SO1838>00000000</SO1838><SO1839>00000000</SO1839><SO1840>23</SO1840><SO1841>000</SO1841><SO1842>000000000</SO1842><SO1843>000000000</SO1843><SO1844>000000000</SO1844><SO1845>000000000</SO1845><SO1846>0</SO1846><SO1847>0</SO1847><SO1848>000000000</SO1848><SO1849>00000000</SO1849><SO1850>00000000</SO1850><SO1851>00000000</SO1851><SO1852>0</SO1852><SO1853>000000000</SO1853><SO1854>00</SO1854><SO1855>000000000</SO1855><SO1856>0000</SO1856><SO1857>000000000</SO1857><SO1858>000000000</SO1858><SO1859>0</SO1859><SO1860>000000000</SO1860><SO1861>000000000</SO1861>

					String rtnXML = MQService.qryGMHasLimitStart(jsonCaseData.getString("Idno"));
					System.out.println("#### XML=" + rtnXML);

					Map<String, String> mapRtn = XmlParser.parse(rtnXML);

					String sSO1814 = blankIfNull(mapRtn.get("SO1814"));
					String sSO1824 = blankIfNull(mapRtn.get("SO1824"));
					String sSO1825 = blankIfNull(mapRtn.get("SO1825"));
					String sSO1829 = blankIfNull(mapRtn.get("SO1829"));
					String sSO1834 = blankIfNull(mapRtn.get("SO1834"));

					// get current date
					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
					LocalDateTime nowDate = LocalDateTime.now();
					String sCurrDate = dtf.format(nowDate);

					System.out.println("#### sCurrDate=" + sCurrDate);
					System.out.println("#### SO1814現況          =" + sSO1814);
					System.out.println("#### SO1824契約書有效期限=" + sSO1824);
					System.out.println("#### SO1825目前授信額度  =" + sSO1825);
					System.out.println("#### SO1829可用餘額      =" + sSO1829);
					System.out.println("#### SO1834下次應繳日    =" + sSO1834);

					String moneyCanUse = "N"; // 是否可動用
					int iChkResult = 0;

					if (sSO1814.length() >= 3) {
						if (sSO1814.substring(0, 1).equals("0") && sSO1814.substring(2, 3).equals("0")) {
							iChkResult = iChkResult + 1;
						}

					}

					if (sSO1824.length() == 8) {
						if (sSO1824.compareTo(sCurrDate) >= 0) {
							iChkResult = iChkResult + 10;
						}

					}

					if (sSO1825.length() > 0) {
						// 注意:值可能為40000.00，要用Float.parseFloat
						try {
							if (Float.parseFloat(sSO1825) > 0) {
								iChkResult = iChkResult + 100;
							}
						} catch (NumberFormatException e) {
							iChkResult = iChkResult + 0;
						}

					}

					// 可用餘額, @2020-12-11 改為0時也可以過
					if (sSO1829.length() >= 0) {
						// 注意:值可能為40000.00，要用Float.parseFloat
						try {
							if (Float.parseFloat(sSO1829) >= 0) {
								iChkResult = iChkResult + 1000;
							}
						} catch (NumberFormatException e) {
							iChkResult = iChkResult + 0;
						}

					}

					if (sSO1834.length() == 8) {
						if (sSO1834.compareTo(sCurrDate) >= 0 || sSO1834.equals("00000000")) {
							iChkResult = iChkResult + 10000;
						}

					}

					System.out.println(">>>> iChkResult=" + iChkResult);

					if (CaseStatus.equals("12") && iChkResult == 11111) {
						CaseStatusAPS = "I";
					}

					timeStampCurrent = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss")
							.format(Calendar.getInstance().getTime());
					System.out
							.println("#### " + targetChannel + " 呼叫狀態查詢(GMHasLimitStart) end   @ " + timeStampCurrent);

					String rtnStatusCode = "";
					String rtnStatusDesc = "";
					System.out.println("#### Final CaseStatus-CaseStatusAPS=" + CaseStatus + "-" + CaseStatusAPS);
					switch (CaseStatus + "-" + CaseStatusAPS) {
					case "00-0":
						rtnStatusCode = "0";
						rtnStatusDesc = "尚未申請";
						break; // 可选
					case "01-0":
						rtnStatusCode = "A";
						rtnStatusDesc = "申請尚未完成";
						break; // 可选
					case "01-1":
						rtnStatusCode = "A";
						rtnStatusDesc = "申請尚未完成";
						break; // 可选
					case "03-0":
						rtnStatusCode = "C";
						rtnStatusDesc = "申請處理中";
						break; // 可选
					case "03-1":
						rtnStatusCode = "C";
						rtnStatusDesc = "申請處理中";
						break; // 可选
					case "02-0":
						rtnStatusCode = "C";
						rtnStatusDesc = "申請處理中";
						break; // 可选
					case "02-1":
						rtnStatusCode = "C";
						rtnStatusDesc = "申請處理中";
						break; // 可选
					case "12-0":
						rtnStatusCode = "C";
						rtnStatusDesc = "申請處理中";
						break; // 可选
					case "12-1":
						rtnStatusCode = "D";
						rtnStatusDesc = "審核中";
						break; // 可选
					case "12-2":
						rtnStatusCode = "E";
						rtnStatusDesc = "審核已通過，待提供身分證進行簽約";
						break; // 可选
					case "08-0":
						rtnStatusCode = "F";
						rtnStatusDesc = "審核未通過";
						break; // 可选
					case "08-1":
						rtnStatusCode = "F";
						rtnStatusDesc = "審核未通過";
						break; // 可选
					case "09-0":
						rtnStatusCode = "F";
						rtnStatusDesc = "審核未通過";
						break; // 可选
					case "09-1":
						rtnStatusCode = "F";
						rtnStatusDesc = "審核未通過";
						break; // 可选
					case "09-3":
						rtnStatusCode = "F";
						rtnStatusDesc = "審核未通過";
						break; // 可选
					case "12-3":
						rtnStatusCode = "F";
						rtnStatusDesc = "審核未通過";
						break; // 可选
					case "12-4":
						rtnStatusCode = "G";
						rtnStatusDesc = "待線上簽約";
						break; // 可选
					case "12-5":
						rtnStatusCode = "H";
						rtnStatusDesc = "簽約完成";
						break; // 可选
					case "12-I":
						rtnStatusCode = "I";
						rtnStatusDesc = "啟額完成";
						break; // 可选
					case "06-0":
						rtnStatusCode = "J";
						rtnStatusDesc = "申請已逾期";
						break; // 可选
					case "06-6":
						rtnStatusCode = "J";
						rtnStatusDesc = "申請已逾期";
						break; // 可选
					case "12-7":
						rtnStatusCode = "K";
						rtnStatusDesc = "協定已撤件";
						break; // 可选

					// 你可以有任意数量的case语句
					default: // 可选
						rtnStatusCode = "X";
						rtnStatusDesc = "異常(" + CaseStatus + "-" + CaseStatusAPS + ")";
					}

					jsonRtn.put("checkCode", "");
					jsonRtn.put("message", "查詢成功");
					jsonRtn.put("kgiCaseNo", CaseNo);
					jsonRtn.put("caseStatusCode", rtnStatusCode);
					jsonRtn.put("caseStatusDesc", rtnStatusDesc);
					// goto RTN

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			jsonRtn.put("checkCode", "99");
			jsonRtn.put("message", e.getMessage());

		} finally {
			// 寫log
			fucoApiLogService.insertFucoApiLog("/cs/getCrossSellCaseStatus", reqJsonData, jsonRtn.toString());

		}

		// RTN

		result = jsonRtn.toString();

		timeStampEnd = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(Calendar.getInstance().getTime());
		System.out.println("#### " + targetChannel + " 呼叫狀態查詢 getCrossSellCaseStatus(" + uniqToken.substring(0, 5)
				+ "...) @ " + timeStampStart + " ~ " + timeStampEnd);

		System.out.println(">>>> getCrossSellCaseStatus return: " + result);
		return result;

	}

	@RequestMapping(value = "/getOrderRate", produces = "application/json;charset=utf-8")
	@ResponseBody
	public String getPchomeData(String UniqId) {
		String rate = "";
		UniqId = "CASE2020021300032";
		MemoData memoData = new MemoData(UniqId, "", SystemConst.MEMO_CSCOMMON_CIF);
		memoData = memoDataDao.Read(memoData);

		if (null != memoData) {
			JSONObject json2 = JSONObject.fromObject(memoData.getMemo());
			JSONObject json3 = json2.getJSONObject("data");
			JSONObject json4 = json3.getJSONObject("additionalData");

			if (null != json4) {
				if (!StringUtil.isBlank(json4.toString())) {
					rate = json4.getString("rate");
				}
			} else {
				rate = "error:additionalData 為空值";
			}
		} else {
			rate = "error:查無跨售資料";
		}
		return rate;
	}

	@RequestMapping(value = "/insertCSCommonData", produces = "application/json;charset=utf-8")
	@ResponseBody
	public void insertCSCommonData(@RequestBody String reqJsonData) {
		try {
			// uniqId = ESAPI.validator().getValidInput("", uniqId, "AntiXSS", 32, false);
			// uniqType = ESAPI.validator().getValidInput("", uniqType, "AntiXSS", 32,
			// false);
			// CSType = ESAPI.validator().getValidInput("", CSType, "AntiXSS", 32, false);
			// channelId = ESAPI.validator().getValidInput("", channelId, "AntiXSS", 32,
			// false);
			// resultData = ESAPI.validator().getValidInput("", resultData, "AntiXSS", 32,
			// false);
		} catch (Exception e1) {
			return;
		}
		try {
			JSONObject reqJson = JSONObject.fromObject(reqJsonData);
			String uniqId = reqJson.optString("uniqId", "");
			String uniqType = reqJson.optString("uniqType", "");
			String CSType = reqJson.optString("CSType", "");
			String channelId = reqJson.optString("channelId", "");
			String resultData = reqJson.optString("resultData", "");
			crossSellServ.insertCSCommonData(uniqId, uniqType, CSType, channelId, resultData);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

    @RequestMapping(value = "/getApplyPage", method = RequestMethod.GET)
	public String getApplyPage(@RequestParam Map<String, String> requestParams) {
		
		String returnUrl = null;
		try {
			
			String prodType = requestParams.get("prodType");
			returnUrl = crossSellServ.getApplyPageUrl(prodType);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return returnUrl;
	}
	

}
