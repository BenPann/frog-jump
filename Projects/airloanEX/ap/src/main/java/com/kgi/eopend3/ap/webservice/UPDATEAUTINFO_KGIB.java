/**
 * UPDATEAUTINFO_KGIB.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.kgi.eopend3.ap.webservice;
@SuppressWarnings("all")
public class UPDATEAUTINFO_KGIB  implements java.io.Serializable {
    private java.lang.String CHECKFLAG;

    private java.lang.String MSG;

    public UPDATEAUTINFO_KGIB() {
    }

    public UPDATEAUTINFO_KGIB(
           java.lang.String CHECKFLAG,
           java.lang.String MSG) {
           this.CHECKFLAG = CHECKFLAG;
           this.MSG = MSG;
    }


    /**
     * Gets the CHECKFLAG value for this UPDATEAUTINFO_KGIB.
     * 
     * @return CHECKFLAG
     */
    public java.lang.String getCHECKFLAG() {
        return CHECKFLAG;
    }


    /**
     * Sets the CHECKFLAG value for this UPDATEAUTINFO_KGIB.
     * 
     * @param CHECKFLAG
     */
    public void setCHECKFLAG(java.lang.String CHECKFLAG) {
        this.CHECKFLAG = CHECKFLAG;
    }


    /**
     * Gets the MSG value for this UPDATEAUTINFO_KGIB.
     * 
     * @return MSG
     */
    public java.lang.String getMSG() {
        return MSG;
    }


    /**
     * Sets the MSG value for this UPDATEAUTINFO_KGIB.
     * 
     * @param MSG
     */
    public void setMSG(java.lang.String MSG) {
        this.MSG = MSG;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UPDATEAUTINFO_KGIB)) return false;
        UPDATEAUTINFO_KGIB other = (UPDATEAUTINFO_KGIB) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CHECKFLAG==null && other.getCHECKFLAG()==null) || 
             (this.CHECKFLAG!=null &&
              this.CHECKFLAG.equals(other.getCHECKFLAG()))) &&
            ((this.MSG==null && other.getMSG()==null) || 
             (this.MSG!=null &&
              this.MSG.equals(other.getMSG())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCHECKFLAG() != null) {
            _hashCode += getCHECKFLAG().hashCode();
        }
        if (getMSG() != null) {
            _hashCode += getMSG().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UPDATEAUTINFO_KGIB.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "UPDATEAUTINFO_KGIB"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CHECKFLAG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CHECKFLAG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MSG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
