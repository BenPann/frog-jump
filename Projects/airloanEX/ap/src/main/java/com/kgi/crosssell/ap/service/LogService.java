package com.kgi.crosssell.ap.service;

import com.kgi.crosssell.ap.dao.ProcessTimeDao;
import com.kgi.eopend3.ap.dao.BrowseLogDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogService {
	@Autowired
	private BrowseLogDao blogDao;

	@Autowired
	private ProcessTimeDao processDao;

	/**
	 * @param msg
	 *            寫瀏覽記錄
	 */
	public void saveBrowseRecordLog(String uniqId, String uniqType, String page, String action) throws Exception {
		blogDao.CreateBrowseLog(uniqId, uniqType, page, action);
	}

	public void processStart(String UniqId, String UniqType) throws Exception {
		processDao.processStart(UniqId, UniqType);
	}

	public void processPageStart(String UniqId, String UniqType, String Page) throws Exception {
		processDao.processPageStart(UniqId, UniqType, Page);
	}
}
