package com.kgi.airloanex.ap.service;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kgi.eopend3.ap.dao.SNDao;
import com.kgi.eopend3.common.util.DateUtil;

import static com.kgi.airloanex.common.PlContractConst.*;

@Service
public class SrnoService {

	@Autowired
	private SNDao sndao;
	
	public String getpCode2566UniqId() {
		Integer sni = sndao.getSerialNumber("PCODE2566", "");
		Integer sn = sni % 10000;
		// 組成 格式 (流程別)(今日日期)(流水號)
		return StringUtils.leftPad(sn.toString(), 4, "0");
	}
	
	/** 登入編號 - QLOGyyyyMMdd12345 */
	public String getQLOGUniqId() {
		String date = DateUtil.GetDateFormatString("yyyyMMdd");
		Integer sni = sndao.getSerialNumber(QLOG, date);
		// 組成 格式 (流程別)(今日日期)(流水號)
		return QLOG.concat(date).concat(StringUtils.leftPad(sni.toString(), 5, "0"));
	}

	/** [信貸]申請流程 - 案件編號 - LOANyyyyMMdd12345 */
	public String getLOANUniqId() {
		String date = DateUtil.GetDateFormatString("yyyyMMdd");
		Integer sni = sndao.getSerialNumber(LOAN, date);
		// 組成 格式 (流程別)(今日日期)(流水號)
		return LOAN.concat(date).concat(StringUtils.leftPad(sni.toString(), 5, "0"));
	}

	/** [信貸]申請流程 - 額度體驗題目編號 - EXPyyyyMMdd12345 */
	public String getEXPUniqId() {
		String date = DateUtil.GetDateFormatString("yyyyMMdd");
		Integer sni = sndao.getSerialNumber(EXP, date);
		// 組成 格式 (流程別)(今日日期)(流水號)
		return EXP.concat(date).concat(StringUtils.leftPad(sni.toString(), 5, "0"));
	}

	/** [信貸]立約流程 - 案件編號 - CONTyyyyMMdd12345 */
	public String getUniqId() {
		String date = DateUtil.GetDateFormatString("yyyyMMdd");
		Integer sni = sndao.getSerialNumber(CONT, date);
		// 組成 格式 (流程別)(今日日期)(流水號)
		return CONT.concat(date).concat(StringUtils.leftPad(sni.toString(), 5, "0"));
	}
	
	/**
	 * EDDA交易序號必須為唯一值
	 * @return
	 */
	public String getEddaSrno() {
		String date = DateUtil.GetDateFormatString("yyyyMMdd");
		Integer sni = sndao.getSerialNumber(EDDA, date);
		// 
		return StringUtils.leftPad(sni.toString(), 10, "0");
    }

	/** 聯絡客服編號 - CTACyyyyMMdd12345 */
	public String getCTACId(){
		String date = DateUtil.GetDateFormatString("yyyyMMdd");
		Integer sni = sndao.getSerialNumber(CTAC, date);
		// 組成 格式 (流程別)(今日日期)(流水號)
		return CTAC.concat(date).concat(StringUtils.leftPad(sni.toString(), 5, "0"));
	}

}