package com.kgi.airloanex.ap.service;

import com.kgi.airloanex.ap.dao.CaseAuthDao;
import com.kgi.airloanex.common.dto.db.CaseAuth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CaseAuthService {
    @Autowired
    private CaseAuthDao caseAuthDao;

    public int saveCaseAuth(CaseAuth caseAuth) {
        return caseAuthDao.CreateOrUpdate(caseAuth);
    }

    public CaseAuth Read(CaseAuth caseAuth){
        return caseAuthDao.Read(caseAuth);
    }

    public void updateCaseAuth(CaseAuth caseAuth){
        caseAuthDao.Update(caseAuth);
    }
}
