package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.CaseMoiData;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class CaseMoiDataDao extends CRUDQDao<CaseMoiData> {

    @Override
    public int Create(CaseMoiData fullItem) {
        String sql = "INSERT INTO "
                + " CaseMoiData( UniqId, IdNo, CustomerName, IssueLocation, IssueType, IssueDate, Request, Response, ResponseCode, Memo, CreateTime, UpdateTime)"
                + "      VALUES(:UniqId,:IdNo,:CustomerName,:IssueLocation,:IssueType,:IssueDate,:Request,:Response,:ResponseCode,:Memo,  getdate(), getdate())";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public CaseMoiData Read(CaseMoiData keyItem) {
        String sql = "SELECT * FROM CaseMoiData WHERE UniqId = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseMoiData.class), new Object[] { keyItem.getUniqId() });
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int Update(CaseMoiData fullItem) {
        String sql = "UPDATE CaseMoiData SET UniqId=:UniqId, IdNo=:IdNo, CustomerName=:CustomerName, IssueLocation=:IssueLocation, IssueType=:IssueType, IssueDate=:IssueDate, Request=:Request, Response=:Response, ResponseCode=:ResponseCode, Memo=:Memo, UpdateTime=getdate() WHERE UniqId=:UniqId ";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(CaseMoiData keyItem) {
        return 0; // TODO:
    }

    @Override
    public List<CaseMoiData> Query(CaseMoiData keyItem) {
        return null; // TODO:
    }

}
