package com.kgi.airloanex.ap.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.kgi.airloanex.common.dto.db.DropdownData;
import com.kgi.airloanex.ap.pdf.dto.PDFCaseData;
import com.kgi.eopend3.ap.dao.BaseDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionStatus;

@Repository
public class PDFDao extends BaseDao {
	
	/**
	 * @param caseNo
	 * @return
	 * 從DB下載PDF套表所需的資料
	 */
	public PDFCaseData loadCaseData(String caseNo){
//		List<PDFCaseData> result = new ArrayList<PDFCaseData>();
		PDFCaseData result = null;
		
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		
		try {
			String sql = " SELECT cv.UniqId, cv.CreateTime as applyccUtime, cd.* " + 
			" FROM CaseData cd left join CreditVerify cv on cd.CaseNo = cv.UniqId " + 
			" WHERE cd.CaseNo = ?";
			List<Map<String, Object>> ls =	
					jdbcTemplate.queryForList(sql, new Object[] { caseNo }); 
			if(null != ls && ls.size() > 0){
				result = new PDFCaseData(ls.get(0)); 
	        }
		} catch (DataAccessException e){
//			 e.printStackTrace();
		}
		return result;
	}

	public PDFCaseData loadRejectCaseData(String caseNo) {
		PDFCaseData result = null;
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();

		try {
			List<Map<String, Object>> ls = jdbcTemplate.queryForList("SELECT * FROM dbo.CaseData WHERE CaseNo = ?",
					new Object[] { caseNo });
			if (null != ls && ls.size() > 0) {
				result = new PDFCaseData();
				result.setRejectPDFCaseData(ls.get(0));
			}
		} catch (DataAccessException e) {
//			e.printStackTrace();
		}
		return result;
	}
	
	public List<Map<String, Object>>  loadContractDoc(String conNo){
//		List<PDFCaseData> result = new ArrayList<PDFCaseData>();
		// PDFCaseData result = null;
		
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		
		try {
			List<Map<String, Object>> ls = jdbcTemplate
					.queryForList("SELECT cd.* ,ca.AuthType FROM dbo.ContractDoc cd left join CaseAuth ca on cd.ConNo = ca.UniqId and ca.UniqType = '03' WHERE ConNo = ? ", new Object[] { conNo });
			return ls;
		} catch (DataAccessException e) {
//			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * CaseDoc
	 * 將加密PDF及圖以Base64存入DB
	 * 
	 * @param caseNo
	 * @param idNo
	 * @param pdfBase64Str
	 * @param imageBase64Str
	 */
	public void storePDF(String caseNo, String idNo, String pdfBase64Str, String imageBase64Str){
		JdbcTemplate jdbctemp = this.getJdbcTemplate();
		// 增加先刪除的方式
		TransactionStatus status = this.BeginTransaction();
		
		String fir = "delete from CaseDoc where caseNo=? ; ";
		String sec = "insert into CaseDoc values(?,?,?,?); ";
		try {
			jdbctemp.update(fir,
					new Object[] { caseNo});
			jdbctemp.update(sec,
					new Object[] { caseNo, idNo, pdfBase64Str, imageBase64Str });
			this.CommitTransaction(status);
		} catch (DataAccessException e) {
			this.RollbakcTransaction(status);
		}
	}	
}
