package com.kgi.crosssell.ap.ctl;



import java.util.ArrayList;
import java.util.List;

import com.kgi.airloanex.common.dto.db.ChannelData;
import com.kgi.crosssell.ap.service.QRCodeService;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/qr")
public class QRCodeCtl {
    @Autowired
    private QRCodeService qrCodeService;
    
    @RequestMapping(value = "/getNewChannelData", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String getNewChannelData(String shortUrl) {
    	System.out.println(">>>> getNewChannelData(" + shortUrl + ")");
    	ChannelData cd = qrCodeService.getChannelData(shortUrl);
        
		if (cd == null) {
			System.out.println(">>>> getNewChannelData(" + shortUrl + ") --->!!! 找不到通路資料");
             return "{}";
        } else {
         	
         	String tempProductId = cd.getPProductId();
         	List<String> ProductIdList = new ArrayList<String>();
         	boolean isProducdId = false;
         	if(tempProductId.length() > 1){
         		String[]tempProductIdList = tempProductId.split(",");
         		for(String st : tempProductIdList){
         			isProducdId = qrCodeService.isProducdId(st);
         			if(isProducdId) ProductIdList.add(st);
         		}
         	} else if(tempProductId.length() == 1){
         		isProducdId = qrCodeService.isProducdId(tempProductId);
         		if(isProducdId) ProductIdList.add(tempProductId);
         	} else {
         		 ProductIdList.add(tempProductId);
         	}

         	if(ProductIdList.isEmpty()){
         		System.out.println(">>>> getNewChannelData(" + shortUrl + ") --->!!! 找不到產品資料");
				return "{}";
         	} else {
         		cd.setPProductIdList(ProductIdList);
                 // 更新使用次數
                 qrCodeService.updateShortUrlCount(shortUrl);
                 return JSONObject.fromObject(cd).toString();
         	}
        }
    }
    
    @RequestMapping(value = "/createUrlCaseMatch", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public void createUrlCaseMatch(String shortUrl, String uniqId, String uniqType) {
    	
		System.out.println("#### [/createUrlCaseMatch] createUrlCaseMatch (" + shortUrl + "," + uniqId + "," + uniqType + ")");
		
		try {
    		shortUrl = ESAPI.validator().getValidInput("", shortUrl, "AntiXSS", 32, false);
    		uniqId = ESAPI.validator().getValidInput("", uniqId, "AntiXSS", 128, false);
    		uniqType = ESAPI.validator().getValidInput("", uniqType, "AntiXSS", 128, false);
		} catch (Exception e1) {
			return;
		}
        qrCodeService.createUrlCaseMatch(shortUrl,uniqId,uniqType);
    }
}