package com.kgi.crosssell.ap.service;

import com.kgi.eopend3.ap.dao.EntryDataDao;
import com.kgi.eopend3.common.dto.db.EntryData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EntryDataService {

	@Autowired
	private EntryDataDao entryDataDao;

	public void saveEntryData(String uniqId, String uniqType, String entry, String member, String process,
			String browser, String platform, String os, String other) {
		EntryData entryData = new EntryData();
		entryData.setUniqId(uniqId);
		entryData.setUniqType(uniqType);
		entryData.setEntry(entry);
		entryData.setMember(member);
		entryData.setProcess(process);
		entryData.setBrowser(browser);
		entryData.setPlatform(platform);
		entryData.setOS(os);
		entryData.setOther(other);
		entryDataDao.Create(entryData);
	}
	
	public void deleteEntryData(String uniqId) {
		EntryData entryData = new EntryData();
		entryData.setUniqId(uniqId);
		entryDataDao.Delete(entryData);
	}
	
}
