package com.kgi.airloanex.ap.pdf;

import static com.kgi.airloanex.common.PlContractConst.UNIQ_TYPE_02;

import java.io.IOException;
import java.util.List;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.CaseDocumentDao;
import com.kgi.airloanex.ap.dao.PDFDao;
import com.kgi.airloanex.ap.pdf.dto.PDFCaseData;
import com.kgi.airloanex.ap.service.CaseDataService;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.db.CaseDocument;
import com.kgi.airloanex.common.util.ExportUtil;
import com.kgi.eopend3.ap.config.GlobalConfig;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class CaseFinalePDF extends BasePDFDocument {
    
    private static final Logger logger = LogManager.getLogger(CasePDF.class.getName());

    private String _uniqId = "";

    @Autowired
    private GlobalConfig global;
    
	@Autowired
	private AirloanEXConfig globalConfig;

    @Autowired
    private PDFDao pdfDao;
    
    @Autowired
    private CaseDocumentDao caseDocumentDao;

    @Autowired
    private PageFooter footer;
    
    private String templateVersion = "";

    private ApplicationContext appContext;

    private PDFCaseData pdfCaseData = null;

    private CasePDF casePDF;
    private CaseKycPDF caseKycPDF;
    private CaseRelationDegreePDF caseRelationDegreePDF;
    
    public CaseFinalePDF(ApplicationContext appContext) {
        super(appContext);
        this.appContext = appContext;
    }

    public void init(String... params) throws IOException {
        this._uniqId = params[0];
        // 是否加密
        this.setUseEncrepyt(true);
        // this.setEncryptPassword(idno);
        // 是否使用浮水印
        this.setUseWaterMark(true);
		this.setWaterMarkText(globalConfig.IMG_WATER_FONT());
		this.setWaterMarkFont(globalConfig.PDF_FONT_WATER_TTF());
        // 是否要PDF轉成圖片
        this.setPdfToImage(false);
        // 是否存DB
        this.setUseSaveToDB(true);
        // 是否發信
        this.setUseSendEMail(false);
        // 是否設定每頁的事件
        this.setPageEvent(footer);

        try {
            pdfCaseData = pdfDao.loadCaseData(this._uniqId);

            // 1. APPLY
            initCasePDF();
            // 2. KYC
            initCaseKycPDF();
            // 3. RelationDegree
            if (needRelationDegreeReport()) {
                initCaseRelationDegreePDF();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void initCasePDF() throws IOException {
        this.casePDF = new CasePDF(appContext);
        this.casePDF.init(this._uniqId);
    }

    private void initCaseKycPDF() throws Exception {
        this.caseKycPDF = new CaseKycPDF(appContext);
        this.caseKycPDF.init(this._uniqId);
    }

    private void initCaseRelationDegreePDF() throws Exception {
        this.caseRelationDegreePDF = new CaseRelationDegreePDF(appContext);
        this.caseRelationDegreePDF.init(this._uniqId);
    }

    @Override
    public void beforePdfProcess() {
        
        StringBuilder sb = new StringBuilder();
        // 1. APPLY
        concatRawHtml(sb, this.casePDF); // finish setRawHtml()
        // 2. KYC
        concatRawHtml(sb, this.caseKycPDF); // finish setRawHtml()
        // 3. RelationDegree
        if (needRelationDegreeReport()) {
            concatRawHtml(sb, this.caseRelationDegreePDF); // finish setRawHtml() 
        }
        String raw = sb.toString();
        this.setRawHtml(raw);
        templateVersion = this.casePDF.templateVersion;

        this.setEncryptPassword(this.casePDF.getPdfCaseData().idno);
    }

    private void concatRawHtml(StringBuilder sb, BasePDFDocument pdfDocument) {
        if (pdfDocument != null) {
            try{
                pdfDocument.beforePdfProcess();
                String raw = pdfDocument.getRawHtml();
                sb.append(raw);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        return;
    }

    // Charles:傳什麼 raw 值都沒差
    @Override
    public String replaceField(String raw) {
        StringBuilder sb = new StringBuilder();
        // 1. APPLY
        concatDone(sb, this.casePDF);
        // 2. KYC
        concatDone(sb, this.caseKycPDF);
        // 3. RelationDegree
        if (needRelationDegreeReport()) {
            concatDone(sb, this.caseRelationDegreePDF);
        }
        String done = sb.toString();
        return done;
    }

    private void concatDone(StringBuilder sb, BasePDFDocument pdfDocument) {
        if (pdfDocument != null) {
            try {
                String raw = pdfDocument.getRawHtml();
                String done = pdfDocument.replaceField(raw);
                sb.append(done);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        return;
    }

    @Override
    public void afterPdfProcess() {

    }

    @Override
    public void saveToDB(byte[] pdfByte) {
        
        // Save bytes to TABLE: CaseDocument
        CaseDocument cd = new CaseDocument(this._uniqId, UNIQ_TYPE_02, PlContractConst.DOCUMENT_TYPE_LOAN_APPLYPDF_FINALE);
        cd.setContent(pdfByte);
        cd.setVersion(templateVersion);
        caseDocumentDao.CreateOrUpdate(cd);
        
        // Save string base64 to TABLE: CaseDoc
        String pdfbase64 = Base64.encodeBase64String(pdfByte);
        pdfDao.storePDF(this._uniqId, this.casePDF.getPdfCaseData().idno, pdfbase64, "");
        logger.info("Save Pdf To DB");

        // PDF檔 - @export 儲存至檔案系統
        if (globalConfig.isPDF_Export()) {
            ExportUtil.export(global.PdfDirectoryPath + "/" + _uniqId + ".pdf", pdfByte);
        }
    }

    @Override
    public void pdfToImage(List<byte[]> byteList) {

    }

    @Override
    public void sendEmail(byte[] pdfByte) {
        // Note! Do not send the KYC&RelationDegree report to customer
    }

    public boolean needRelationDegreeReport() {
        PDFCaseData pdfCaseData = this.pdfCaseData;
        if (pdfCaseData != null && CaseDataService.needRelationDegreeReport(pdfCaseData.p_apy_amount)) {
            return true;
        } else {
            return false;
        }
    }

    // setter & getter

    public CasePDF getCasePDF() {
        return casePDF;
    }

    public void setCasePDF(CasePDF casePDF) {
        this.casePDF = casePDF;
    }

    public CaseKycPDF getCaseKycPDF() {
        return caseKycPDF;
    }

    public void setCaseKycPDF(CaseKycPDF caseKycPDF) {
        this.caseKycPDF = caseKycPDF;
    }

    public CaseRelationDegreePDF getCaseRelationDegreePDF() {
        return caseRelationDegreePDF;
    }

    public void setCaseRelationDegreePDF(CaseRelationDegreePDF caseRelationDegreePDF) {
        this.caseRelationDegreePDF = caseRelationDegreePDF;
    }

    
}
