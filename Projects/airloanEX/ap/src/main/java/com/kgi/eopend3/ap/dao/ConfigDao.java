package com.kgi.eopend3.ap.dao;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.kgi.eopend3.common.dto.db.Config;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;


@Repository
public class ConfigDao extends CRUDQDao<Config> {

	public List<Config> findAll() {
		String sql = "SELECT * FROM Config";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] {},
                    new BeanPropertyRowMapper<>(Config.class));
        } catch (DataAccessException ex) {
        	ex.printStackTrace();
            return null;
        }
	}

	@Override
	public int Create(Config fullItem) {
		String sql = "INSERT INTO Config Values(?,?,?,GETDATE())";
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getKeyName(), fullItem.getKeyValue(), fullItem.getDescription() });
	}

	@Override
	public Config Read(Config keyItem) {
		String sql = "SELECT * FROM Config WHERE KeyName = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(Config.class),
                    new Object[] { keyItem.getKeyName() });
        } catch (DataAccessException ex) {
			logger.error("Config查無資料");
            return null;
        }
	}

	@Override
	public int Update(Config fullItem) {
		String sql = "UPDATE Config SET KeyValue = ? WHERE KeyName = ?";
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getKeyValue(), fullItem.getKeyName() });
	}

	@Override
	public int Delete(Config keyItem) {
		String sql = "DELETE Config WHERE KeyName = ? ";
        return this.getJdbcTemplate().update(sql,
                new Object[] { keyItem.getKeyName() });
	}

	@Override
	public List<Config> Query(Config keyItem) {
		String sql = "SELECT * FROM Config WHERE KeyName like ?+'.%'";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getKeyName() },
                    new BeanPropertyRowMapper<>(Config.class));
        } catch (DataAccessException ex) {
        	ex.printStackTrace();
            return null;
        }
	}

	public Map<String, String> QueryToMap(Config keyItem) {
		List<Config> list = this.Query(keyItem);
		if (list == null){
			return null;
		}
		Map<String, String> map = new HashMap<String, String>();
		for (Config con : list) {
			map.put(con.getKeyName(), con.getKeyValue());
		}
		return map;
	}

	public String ReadConfigValue(String name) {
		return ReadConfigValue(name, null);
	}

	public HashMap<String, String> ReadConfigValue(Object ...params) {
		String sql = "SELECT KeyName, KeyValue FROM Config WHERE KeyName in (";
		
		for (int loop=0; loop < params.length; loop++) {
			sql += "?," ;
		}
		sql = sql.substring(0, sql.length()-1) ;
		sql += ")" ;
		
        try {
            return this.getJdbcTemplate().query(sql,  params, (ResultSet rs) -> {
                HashMap<String,String> results = new HashMap<>();
                while (rs.next()) {
                    results.put(rs.getString("KeyName"), rs.getString("KeyValue"));
                }
                return results;
            });
        } catch (DataAccessException ex) {
//        	ex.printStackTrace();
			logger.error("Config查無資料");
			logger.error(ex.getMessage());
            return null;
        }
	}

	public String ReadConfigValue(String name, String defaultValue) {
		Config con = this.Read(new Config(name));
		if (con == null) {
			logger.info(name + " - Config KeyName not found.");
			return defaultValue;
		}
		return con.getKeyValue();
	}

}
