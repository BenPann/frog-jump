package com.kgi.eopend3.ap.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class CreateXML {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	DocumentBuilderFactory docFactory;
	DocumentBuilder docBuilder;
	Document doc;
	Element rootElement;

	public CreateXML() {
		try {
			docFactory = DocumentBuilderFactory.newInstance();
			try {
				docFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			} catch (ParserConfigurationException e1) {
				e1.printStackTrace();
			}
			docBuilder = docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	public Element setRootTag(String tagName) {
		// root elements
		doc = docBuilder.newDocument();
		rootElement = doc.createElement(tagName);
		doc.appendChild(rootElement);
		return rootElement;
	}

	public void setAttribute(Element element, Map<String, String> map) {
		for (String key : map.keySet()) {
			element.setAttribute(key, map.get(key));
		}
	}

	public void setChildHasTextNode(Element element, String tagName, String content) {
		Element child = doc.createElement(tagName);
		child.appendChild(doc.createTextNode(content));
		element.appendChild(child);
	}

	public Element setChild(Element element, String tagName) {
		Element child = doc.createElement(tagName);
		element.appendChild(child);
		return child;
	}
	
	public void setChildHasCDATA(Element element, String tagName, String content) {
		Element child = doc.createElement(tagName);
		child.appendChild(doc.createCDATASection(content));
		element.appendChild(child);
	}
	
	public void setChildHasTextNodeByMap(Element element, Map<String, Object> map) {
		for (String key : map.keySet()) {
			Element child = doc.createElement(key);
			String text = map.get(key)!=null?map.get(key).toString():"";
			child.appendChild(doc.createTextNode(text));
			element.appendChild(child);
		}		
	}

	public Element setChildTagNoTextNode(Element element, String tagName) {
		Element child = doc.createElement(tagName);
		element.appendChild(child);
		return child;
	}

	public String toXMLFile() throws TransformerException {
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new StringWriter());
		transformer.transform(source, result);
		return result.getWriter().toString();
	}


	public Document parse(String xml){
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8")));
		try {
			return docBuilder.parse(byteArrayInputStream);
		} catch (SAXException|IOException e) {
			logger.error("Parse XML Fail",e);
			return null;
		}
	}

}
