package com.kgi.airloanex.ap.dao;

import com.kgi.airloanex.common.dto.db.ChatBotPageTeTrigger;
import com.kgi.eopend3.ap.dao.BaseDao;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class ChatBotPageTeTriggerDao extends BaseDao  {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public static final String NAME = "ChatBotPageTeTrigger";

    public ChatBotPageTeTrigger findTrigger(String productType, String flowType, String pageName, String subPageName) {
        if (StringUtils.isBlank(subPageName)) {
            return findByProductTypeAndFlowTypeAndPageName(productType, flowType, pageName);
        } else {
            return findByProductTypeAndFlowTypeAndPageNameAndSubPageName(productType, flowType, pageName, subPageName);
        }
    }

    public ChatBotPageTeTrigger findByProductTypeAndFlowTypeAndPageName(String productType, String flowType, String pageName) {
        String sql = "SELECT * FROM " + NAME + " WHERE ProductType = ? AND FlowType = ? AND PageName = ? AND Enable = '1' ";
        String sqlCmd = sql;
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + productType + "'");
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + flowType + "'");
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + pageName + "'");
        logger.info("### " + sqlCmd);
        try {
            return this.getJdbcTemplate().queryForObject(sql, new Object[] { productType, flowType, pageName }, new BeanPropertyRowMapper<>(ChatBotPageTeTrigger.class));
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料或多筆資料");
            return null;
        }
    }

    public ChatBotPageTeTrigger findByProductTypeAndFlowTypeAndPageNameAndSubPageName(String productType, String flowType, String pageName, String subPageName) {
        String sql = "SELECT * FROM " + NAME + " WHERE ProductType = ? AND FlowType = ? AND PageName = ? AND SubPageName = ? AND Enable = '1' ";
        String sqlCmd = sql;
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + productType + "'");
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + flowType + "'");
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + pageName + "'");
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + subPageName + "'");
        logger.info("### " + sqlCmd);
        try {
            return this.getJdbcTemplate().queryForObject(sql, new Object[] { productType, flowType, pageName, subPageName }, new BeanPropertyRowMapper<>(ChatBotPageTeTrigger.class));
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料或多筆資料");
            return null;
        }
    }

    public ChatBotPageTeTrigger findDefaultByProductTypeAndSort(String productType) {
        String sql = "SELECT * FROM " + NAME + " WHERE ProductType = ? AND Sort = '000' ";
        String sqlCmd = sql;
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + productType + "'");
        logger.info("### " + sqlCmd);
        try {
            return this.getJdbcTemplate().queryForObject(sql, new Object[] { productType}, new BeanPropertyRowMapper<>(ChatBotPageTeTrigger.class));
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料或多筆資料");
            return null;
        }
    }

    public String findTeTriggerByProductTypeAndFlowTypeAndPageName(String productType, String flowType, String pageName) {
        String sql = "SELECT TeTrigger FROM " + NAME + " WHERE ProductType = ? AND FlowType = ? AND PageName = ? AND Enable = '1' ";
        String sqlCmd = sql;
        sqlCmd.replaceFirst("\\?", "'" + productType + "'");
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + flowType + "'");
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + pageName + "'");
        logger.info("### " + sqlCmd);
        try {
            return this.getJdbcTemplate().queryForObject(sql, new Object[] { productType, flowType, pageName }, String.class);
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料或多筆資料");
            return findDefaultTeTriggerByProductTypeAndSort(productType);
        }
    }

    public String findTeTriggerByProductTypeAndFlowTypeAndPageNameAndSubPageName(String productType, String flowType, String pageName, String subPageName) {
        String sql = "SELECT TeTrigger FROM " + NAME + " WHERE ProductType = ? AND FlowType = ? AND PageName = ? AND SubPageName = ? AND Enable = '1' ";
        String sqlCmd = sql;
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + productType + "'");
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + flowType + "'");
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + pageName + "'");
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + subPageName + "'");
        logger.info("### " + sqlCmd);
        try {
            return this.getJdbcTemplate().queryForObject(sql, new Object[] { productType, flowType, subPageName, pageName }, String.class);
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料或多筆資料");
            return findDefaultTeTriggerByProductTypeAndSort(productType);
        }
    }

    public String findDefaultTeTriggerByProductTypeAndSort(String productType) {
        String sql = "SELECT TeTrigger FROM " + NAME + " WHERE ProductType = ? AND Sort = '000' ";
        String sqlCmd = sql;
        sqlCmd = sqlCmd.replaceFirst("\\?", "'" + productType + "'");
        logger.info("### " + sqlCmd);
        try {
            return this.getJdbcTemplate().queryForObject(sql, new Object[] { productType}, String.class);
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料或多筆資料");
            return null;
        }
    }
}
