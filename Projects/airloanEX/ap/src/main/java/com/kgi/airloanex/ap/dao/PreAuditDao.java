package com.kgi.airloanex.ap.dao;

import javax.sql.DataSource;

import com.kgi.airloanex.common.dto.db.PreAudit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * 查詢預核名單
 */
@Repository
public class PreAuditDao {

    public static final String NAME = "PSC_GM_SCORE_LOG" ;

    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
	@Qualifier("oracleDwst")
    private DataSource oracleDwst;

	public PreAudit findPreAudit(String idno) throws DataAccessException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(oracleDwst);
        String sql = " SELECT" 
                + " CUSTOMER_ID AS customerId," 
                + " nvl(CREDIT_LINE_FINAL,0) AS creditLine," 
                + " INTEREST_RATE AS interestRate," 
                + " CREATE_TIME AS createTime,"
                + " PROJECT_CODE AS projectCode,"
                + " CASE_NO AS caseNo,"
                + " SEX AS gender,"
                + " CREATE_TIME + INTERVAL '38' DAY AS lastscoreday" 
                + " from PSC_GM_SCORE_LOG "
				+ " WHERE CUSTOMER_ID = ? AND CREATE_TIME >= sysdate - INTERVAL '38' DAY AND ROWNUM = 1 "
				+ " ORDER BY CREATE_TIME DESC";
        try {
            return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(PreAudit.class), new Object[] { idno });
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料");           
            return null;
        }
	}
}
