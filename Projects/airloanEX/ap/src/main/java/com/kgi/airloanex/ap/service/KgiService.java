package com.kgi.airloanex.ap.service;

import static com.kgi.airloanex.common.PlContractConst.ExternalCallPostMan;

import java.util.List;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.dao.ContractApiLogDao;
import com.kgi.airloanex.ap.dao.ContractWhiteListDao;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.customDto.*;
import com.kgi.airloanex.common.dto.db.ContractApiLog;
import com.kgi.airloanex.common.dto.db.ContractWhiteList;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ALN 理債平台
 * /KGI/GET_OTP_TEL
 * /KGI/GET_CIF_INFO
 * /KGI/GET_TWDRATE
 * /KGI/CREATE_NEW_CASE
 * /KGI/UPDATE_CASE_INFO
 * /KGI/CREATE_NEW_CASE_STEP2
 * 
 * APS EXP # 額度利率體驗PL計算用 API 呼叫用的HOST
 * /KGI/QRY_QA
 * /KGI/SEND_ASW
 * 
 * APS 貸款徵審系統
 * /KGI/GET_DGT_CONST
 * /KGI/QRY_EMP_DEP
 * /KGI/UPDATE_DGT_CONST
 * /KGI/ADD_RELSHIP_DATA
 * /KGI/ADD_IDCARD_INFO
 * /KGI/QRY_IDCARD_INFO
 * 
 * DEC 決策平台
 * /api/KGI/ADD_LOAN_CASE
 * /api/KGI/SEND_JCIC
 * /api/KGI/CHECK_Z07_RESULT
 * /api/KGI/CHECK_HAVE_FINPF
 * /api/KGI/CHECK_NO_FINPF
 * /api/KGI/CHECK_GUARSHIP_RESULT
 * /api/KGI/CHECK_JCIC_RESULT
 * /api/KGI/GET_JCIC_LOAN_DETAIL
 * /api/KGI/CHECK_SALARY_ACCOUNT
 * /api/KGI/GET_OTP_TEL_STP
 * /api/KGI/GET_KYC_DEBT_INFO
 * /api/KGI/GET_TEST_RPL_SCORE
 * /api/KGI/CHECK_RELP_RESULT
 * 
 * CRP 黃頁平台
 * /api/company/full-name
 * /api/company/info
 * 
 * SMS 報表簡訊平台(SMS) power by Ben
 * /sendSMS
 * /imgAPI/GET_CASE_COMPLETE
 * 
 * APIM 戶役政
 * /v1/moi/rwv2c2
 * 
 * 
 * @see [功能規格]WebAPI介接說明_決策平台.docx
 * @see [功能規格]WebAPI介接說明_數位信貸.docx
 * @see WebAPI介接說明_同一關係人資料.docx
 */
@Service
public class KgiService {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private APSPlContractService apsPlContractService;

    @Autowired
    private ContractApiLogDao contractApiLogDao;

    @Autowired
    private ContractWhiteListDao contractWhiteListDao;
    
    // ------------------------------------------------------------------------------------------------------------------------------------
    // ALN
    // @see [功能規格]WebAPI介接說明_數位信貸.docx
    // @see airloanEX.ALN.ServerHost

    // 2.3. 傳入基本資料，同時寫入數位平台資料庫
    // 2.3.1. WebAPI名稱：KGI/GET_CUST_INFO

    // 2.4. 新增案件
    // 2.4.1. WebAPI名稱：KGI/ADD_NEW_CASE

    /**
     * 2.5. 取得OTP電話 
     * 2.5.1. WebAPI名稱：KGI/GET_OTP_TEL
     * 
     * @param uniqId
     * @param CUSTOMER_ID
     * @param BIRTHDATE
     * @return
     * @throws Exception
     */
    public GetOtpTelRspDto call_GET_OTP_TEL(String uniqId, String CUSTOMER_ID, String BIRTHDATE) throws Exception {
        GetOtpTelReqDto req = new GetOtpTelReqDto(CUSTOMER_ID, BIRTHDATE);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryALN("/KGI/GET_OTP_TEL", gson.toJson(req), uniqId);
        GetOtpTelRspDto rtn = gson.fromJson(rspString, GetOtpTelRspDto.class);
        return rtn;
    }

    /**
     * 2.6. 取得客戶留存CIF資料 
     * 2.6.1. WebAPI名稱：KGI/GET_CIF_INFO
     * 
     * @param uniqId
     * @param CUSTOMER_ID
     * @param BIRTHDATE
     * @return
     * @throws Exception
     */
    public GetCifInfoRspDto call_GET_CIF_INFO(String uniqId, String CUSTOMER_ID, String BIRTHDATE) throws Exception {
        GetCifInfoReqDto req = new GetCifInfoReqDto(CUSTOMER_ID, BIRTHDATE);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryALN("/KGI/GET_CIF_INFO", gson.toJson(req), uniqId);
        GetCifInfoRspDto cif = gson.fromJson(rspString, GetCifInfoRspDto.class);
        return cif;
    }

    // 2.7. 名片OCR
    // 2.7.1. WebAPI名稱：KGI/OCR_BIZ_CARD

    // 2.8. 影像系統介接APS系統
    // 2.8.1. WebAPI名稱：KGI/IMG_TO_APS

    // 2.9. 檢核申請人在APS系統是否有案件尚未結案
    // 2.9.1. WebAPI名稱：KGI/CHECK_REP_CASE
    // @deprecated 後來採用 NEW_CASE_CHK 
    // public CheckRepCaseRsqDto call_CHECK_REP_CASE(String uniqId, String CUSTOMER_ID, String DATATYPE) throws Exception {
    //     CheckRepCaseReqDto req = new CheckRepCaseReqDto(CUSTOMER_ID, DATATYPE);
    //     Gson gson = new Gson();
    //     String rspString = apsPlContractService.queryALN("/KGI/CHECK_REP_CASE", gson.toJson(req), uniqId);
    //     CheckRepCaseRsqDto cif = gson.fromJson(rspString,  CheckRepCaseRsqDto.class);
    //     return cif;
    // }

    // 2.10. 取得指數利率(i)
    // 2.10.1. WebAPI名稱：KGI/GET_TWDRATE
    public String call_GET_TWDRATE(String uniqId) throws Exception {
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryALN("/KGI/GET_TWDRATE", "{}", uniqId);
        GetTwdrateRspDto cif = gson.fromJson(rspString,  GetTwdrateRspDto.class);
        if (StringUtils.equals("", cif.getCHECK_CODE())) {
            return cif.getTWDRATE();
        } else {
            return null;
        }
    }

    // 2.11. 取得線上立約資料
    // 2.11.1. WebAPI名稱：KGI/GET_DGT_CONST

    // 2.12. 取得案件申請評分等..資訊
    // 2.12.1. WebAPI名稱：KGI/GET_DGTCASE_INFO
    public GetDgtcaseInfoRspDto call_GET_DGTCASE_INFO(String uniqId, String CASE_NO) throws Exception {
        GetDgtcaseInfoReqDto req = new GetDgtcaseInfoReqDto(CASE_NO);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryALN("/KGI/GET_DGTCASE_INFO", gson.toJson(req), uniqId);
        GetDgtcaseInfoRspDto rtn = gson.fromJson(rspString,  GetDgtcaseInfoRspDto.class);
        return rtn;
    }

    // 2.13. 更新立約狀態
    // 2.13.1. WebAPI名稱：KGI/UPDATE_DGT_CONST

    // 2.14. 用ID取得APS案件資訊
    // 2.14.1. WebAPI名稱：KGI/GET_APSSTATUS_BYID

    // 2.15. 取得聯徵判讀資訊
    // 2.15.1. WebAPI名稱：KGI/GET_JCIC_CREDIT

    // 2.16. 計算月付貸款金額
    // 2.16.1. WebAPI名稱：KGI/GET_MONTHLY_PAY

    // 2.17. 取得公司行業別..資訊
    // 2.17.1. WebAPI名稱：KGI/GET_COMP_INFO

    // 2.18. 取得TextMining資訊
    // 2.18.1. WebAPI名稱：KGI/GET_CORP_TITLE

    // 2.19. 取得評分資訊
    // 2.19.1. WebAPI名稱：KGI/GET_SCORE_INFO

    // 2.20. 取得客戶留存CIF資料(信用卡申辦)
    // 2.20.1. WebAPI名稱：KGI/GET_CIF_INFO_CC

    // 2.21. 傳送影像系統結案資訊
    // 2.21.1. WebAPI名稱：KGI/CLOSECASE_TO_IMAGE

    // 2.22. 更新前台案件資訊
    // 2.22.1. WebAPI名稱：KGI/UPDATECASE_TO_COMPLETE

    /*
     * 2.23.	新增案件(STP)
     * 2.23.1.	WebAPI名稱：KGI/CREATE_NEW_CASE
     */
    public CreateNewCaseRspDto call_CREATE_NEW_CASE(String uniqId, CreateNewCaseReqDto req) throws Exception {
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryALN("/KGI/CREATE_NEW_CASE", gson.toJson(req), uniqId);
        CreateNewCaseRspDto rtn = gson.fromJson(rspString, CreateNewCaseRspDto.class);
        return rtn;
    }

    /**
     * 2.24.	更新案件資訊(STP)
     * 2.24.1.	WebAPI名稱：KGI/UPDATE_CASE_INFO
     * 2.24.2.	傳入參數(JSON格式)：
     * (1)	CASE_NO：案件編號(2.23功能取得的案件編號)，(string)
     * (2)	HOUSE_REG_CITY_CODE：戶籍地址行政區代碼，(string)
     * (3)	HOUSE_REG_ADDRESS：戶籍地址，(string)
     * (4)	HOUSE_REG_AREA：戶籍電話區碼，(string)
     * (5)	HOUSE_REG_TEL：戶籍電話，(string)
     * (6)	AML_FLG：洗錢職業類別比對結果(Y/N)，(string)
     * (7)	AML_RESULT：洗錢AML比對結果(Y/V/N/O/P) Y-ID命中禁制名單、V-疑似高 風險名單、N-非高風險名單、新增兩個狀態code"O-系統異常無法比對、P-非當日即時查詢，(string)
     * (8)	COMP_CASE：是否為完整件(Y/N)，(string)
     */
    public UpdateCaseInfoRsqDto call_UPDATE_CASE_INFO(String uniqId, String CASE_NO, String HOUSE_REG_CITY_CODE, String HOUSE_REG_ADDRESS, String HOUSE_REG_AREA, String HOUSE_REG_TEL, String AML_FLG, String AML_RESULT, String COMP_CASE, String FAST_ID_FLG) throws Exception {
        UpdateCaseInfoReqDto req = new UpdateCaseInfoReqDto(CASE_NO);
        req.setHOUSE_REG_CITY_CODE(HOUSE_REG_CITY_CODE);
        req.setHOUSE_REG_ADDRESS(HOUSE_REG_ADDRESS);
        req.setHOUSE_REG_AREA(HOUSE_REG_AREA);
        req.setHOUSE_REG_TEL(HOUSE_REG_TEL);
        req.setAML_FLG(AML_FLG);
        req.setAML_RESULT(AML_RESULT);
        req.setCOMP_CASE(COMP_CASE);
        req.setFAST_ID_FLG(FAST_ID_FLG);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryALN("/KGI/UPDATE_CASE_INFO", gson.toJson(req), uniqId);
        UpdateCaseInfoRsqDto rtn = gson.fromJson(rspString, UpdateCaseInfoRsqDto.class);
        return rtn;
    }

    /**
     * 2.25.	新增案件步驟2(STP)
     * 2.25.1.	WebAPI名稱：KGI/CREATE_NEW_CASE_STEP2
     * 2.25.2.	傳入參數(JSON格式)：
     * (1)	CASE_NO：案件編號(2.23功能取得的案件編號)，(string)
     */
    public CreatenewCaseStep2RspDto call_CREATE_NEW_CASE_STEP2(String uniqId, String CASE_NO) throws Exception {
        CreatenewCaseStep2ReqDto req = new CreatenewCaseStep2ReqDto(CASE_NO);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryALN("/KGI/CREATE_NEW_CASE_STEP2", gson.toJson(req), uniqId);
        CreatenewCaseStep2RspDto rtn = gson.fromJson(rspString, CreatenewCaseStep2RspDto.class);
        return rtn;
    }

    /**
     * 2.26.	更新APS系統秒貸案件資訊
     * 2.26.1.	WebAPI名稱：KGI/UPDATE_APS_FAST_CASE
     * 2.26.2.	傳入參數(JSON格式)：
     * (1)	CASE_NO：案件編號(2.23功能取得的案件編號)，(string)
     * (2)	ERR_FLAG：異常件註記(Y/N)，(string)
     * (3)	IMG_READY_FLAG：影像系統已索引註記(Y/N)，(string)
     * (4)	DOC_FLAG：申請書/ID/財力完整者(Y/N)，(string)
     */
    public UpdateApsFastCaseRspDao call_UPDATE_APS_FAST_CASE(String uniqId, String CASE_NO, String ERR_FLAG, String IMG_READY_FLAG, String DOC_FLAG) throws Exception {
        UpdateApsFastCaseReqDto req = new UpdateApsFastCaseReqDto(CASE_NO, ERR_FLAG, IMG_READY_FLAG, DOC_FLAG);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryALN("/KGI/UPDATE_APS_FAST_CASE", gson.toJson(req), uniqId);
        UpdateApsFastCaseRspDao rtn = gson.fromJson(rspString, UpdateApsFastCaseRspDao.class);
        return rtn;
    }

    // ------------------------------------------------------------------------------------------------------------------------------------
    // ALN EXP # 額度利率體驗PL計算用 API 呼叫用的HOST
    /**
     * 2.1. 取得額度體驗題目說明功能，接收KGIB回傳額度體驗題目內容及案件編號 
     * 2.1.1. WebAPI名稱：KGI/QRY_QA
     * 
     * @param uniqId
     * @param CASE_NO_WEB
     * @param IP_ADDRESS
     * @return
     * @throws Exception
     */
    public QryQaRspDto call_QRY_QA(String uniqId, String CASE_NO_WEB, String IP_ADDRESS) throws Exception {
        QryQaReqDto req = new QryQaReqDto(CASE_NO_WEB, IP_ADDRESS);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryALN_EXP("/KGI/QRY_QA", gson.toJson(req), uniqId);
        QryQaRspDto rtn = gson.fromJson(rspString, QryQaRspDto.class);
        return rtn;
    }

    /**
     * 2.2. 傳入額度體驗答案，取得額度體驗評分資訊 
     * 2.2.1. WebAPI名稱：KGI/SEND_ASW
     * 
     * @param uniqId
     * @param CASE_NO
     * @param QUEST_LIST
     * @param QUEST_LANSWER_LISTIST
     * @return
     * @throws Exception
     */
    public SendAseRspDto call_SEND_ASW(String uniqId, String CASE_NO, String QUEST_LIST, String ANSWER_LIST)
            throws Exception {
        SendAseReqDto req = new SendAseReqDto(CASE_NO, QUEST_LIST, ANSWER_LIST);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryALN_EXP("/KGI/SEND_ASW", gson.toJson(req), uniqId);
        SendAseRspDto rtn = gson.fromJson(rspString, SendAseRspDto.class);
        return rtn;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------
    // APS 貸款徵審系統
    // @see WebAPI介接說明(數位線上立約).docx

    public GetDgtConstRspDto touch_GET_DGT_CONST(String uniqId, String CUSTOMER_ID, String DATA_TYPE) throws Exception {
        ContractApiLog log = findContractApiLogOfGET_DGT_CONST(uniqId, CUSTOMER_ID, DATA_TYPE);
        if (log != null) {
            GetDgtConstRspDto one = new Gson().fromJson(log.getResponse(), GetDgtConstRspDto.class);
            return one;
        } else {
            return call_GET_DGT_CONST(uniqId, CUSTOMER_ID, DATA_TYPE);
        }
    }

    public String touchString_GET_DGT_CONST(String uniqId, String CUSTOMER_ID, String DATA_TYPE) throws Exception {
        ContractApiLog log = findContractApiLogOfGET_DGT_CONST(uniqId, CUSTOMER_ID, DATA_TYPE);
        if (log != null) {
            String one = log.getResponse();
            return one;
        } else {
            return callString_GET_DGT_CONST(uniqId, CUSTOMER_ID, DATA_TYPE);
        }
    }

    private ContractApiLog findContractApiLogOfGET_DGT_CONST(String uniqId, String CUSTOMER_ID, String DATA_TYPE) {
        ContractApiLog rtn = null;
        Gson gson = new Gson();
        List<ContractApiLog> logs = contractApiLogDao.findByKeyInfoAndApiName(uniqId, "/KGI/GET_DGT_CONST");
        logs.removeIf(o -> o.isUsed());
        for (ContractApiLog log : logs) {
            GetDgtConstReqDto req = gson.fromJson(log.getRequest(), GetDgtConstReqDto.class);
            // rsp = gson.fromJson(log.getResponse(), GetDgtConstRspDto.class);
            if (StringUtils.equals(CUSTOMER_ID.trim(), req.getCUSTOMER_ID().trim())
                    && StringUtils.equals(DATA_TYPE, req.getDATA_TYPE())) {
                rtn = log;
            }
        }
        return rtn;
    }

    public void updateDgtConstIsUsed(String uniqId, String CUSTOMER_ID, String DATA_TYPE) {
        ContractApiLog log = findContractApiLogOfGET_DGT_CONST(uniqId, CUSTOMER_ID, DATA_TYPE);
        if (log != null) {
            contractApiLogDao.updateUsed(log.getSerial());
        }
    }

    /**
     * 1.1. 取得待立約資料 
     * 1.1.1. WebAPI名稱：KGI\GET_DGT_CONST
     */
    public GetDgtConstRspDto call_GET_DGT_CONST(String uniqId, String CUSTOMER_ID, String DATA_TYPE) throws Exception {
        GetDgtConstReqDto req = new GetDgtConstReqDto(CUSTOMER_ID, DATA_TYPE);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryAPS("/KGI/GET_DGT_CONST", gson.toJson(req), uniqId);
        GetDgtConstRspDto rtn = gson.fromJson(rspString, GetDgtConstRspDto.class);
        return rtn;
    }

    /**
     * 1.1. 取得待立約資料 
     * 1.1.1. WebAPI名稱：KGI\GET_DGT_CONST
     * 回傳 JSON 字串
     */
    public String callString_GET_DGT_CONST(String uniqId, String CUSTOMER_ID, String DATA_TYPE) throws Exception {
        GetDgtConstReqDto req = new GetDgtConstReqDto(CUSTOMER_ID, DATA_TYPE);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryAPS("/KGI/GET_DGT_CONST", gson.toJson(req), uniqId);
        return rspString;
    }

    /**
     * 1.1.	查詢行內人員對應單位代號
     * 1.1.1.	WebAPI名稱：KGI\QRY_EMP_DEP
     * 
     * (1)	EMP_ID：員工編號，NVARCHAR (6)
     */
    public QryEmpDepRspDto call_QRY_EMP_DEP(String uniqId, String EMP_ID) throws Exception {
        QryEmpDepReqDto req = new QryEmpDepReqDto(EMP_ID);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryAPS("/KGI/QRY_EMP_DEP", gson.toJson(req), uniqId);
        QryEmpDepRspDto rtn = gson.fromJson(rspString, QryEmpDepRspDto.class);
        return rtn;
    }

    /**
     * 1.2. 更新立約完成資料 
     * 1.2.1. WebAPI名稱：KGI\UPDATE_DGT_CONST
     * 
     * @param uniqId
     * @param req
     * @return
     * @throws Exception
     */
    public UpdateDgtConstRspDto call_UPDATE_DGT_CONST(String uniqId, String req) throws Exception {
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryAPS("/KGI/UPDATE_DGT_CONST", req, uniqId);
        UpdateDgtConstRspDto rtn = gson.fromJson(rspString, UpdateDgtConstRspDto.class);
        return rtn;
    }

    /**
     * /KGI/ADD_RELSHIP_DATA
     * 新增同一個關係人資料
     * @see WebAPI介接說明_同一關係人資料.docx
     */
    public AddRelShipRspDto call_ADD_RELSHIP_DATA(String uniqId, AddRelShipReqDto reqBody) throws Exception {
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryAPS("/KGI/ADD_RELSHIP_DATA", gson.toJson(reqBody), uniqId);
        AddRelShipRspDto rtn = gson.fromJson(rspString, AddRelShipRspDto.class);
        return rtn;
    }
    
    /**
     * 1.1.	新增戶役政檢核結果資料
     * 1.1.1.	WebAPI名稱：KGI/ADD_IDCARD_INFO
     * (1)	CUSTOMER_ID：客戶身分證字號，NVARCHAR(10)
     * (2)	DGTPlatformNo：數位案件編號，NVARCHAR(20)
     * (3)	IDCARD_DATE：發證日期(YYYYMMDD)，NVARCHAR(8)
     * (4)	IDCARD_CITY：發證地點，NVARCHAR(5) 例如:68000 桃市
     * (5)	IDCARD_TYPE：領補換類別，NVARCHAR(5)
     * (6)	IDCARD_CHK_DATE:戶役政驗證完成時間(YYYYMMDDHHMMSS)，NVARCHAR(14)
     */
    public AddIdcardInfoRspDto call_ADD_IDCARD_INFO(String uniqId, String CUSTOMER_ID, String DGTPlatformNo, String IDCARD_DATE, String IDCARD_CITY, String IDCARD_TYPE, String IDCARD_CHK_DATE) throws Exception {
        AddIdcardInfoReqDto req = new AddIdcardInfoReqDto(CUSTOMER_ID, DGTPlatformNo, IDCARD_DATE, IDCARD_CITY, IDCARD_TYPE, IDCARD_CHK_DATE);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryAPS("/KGI/ADD_IDCARD_INFO", gson.toJson(req), uniqId);
        AddIdcardInfoRspDto rtn = gson.fromJson(rspString, AddIdcardInfoRspDto.class);
        return rtn;
    }

    /**
     * 1.2.	查詢行內戶役政檢核結果資料
     * 1.2.1.	WebAPI名稱：KGI/QRY_IDCARD_INFO
     */
    public QryIdcardInfoRspDto call_QRY_IDCARD_INFO(String uniqId, String CUSTOMER_ID) throws Exception {
        QryIdcardInfoReqDto req = new QryIdcardInfoReqDto(CUSTOMER_ID);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryAPS("/KGI/QRY_IDCARD_INFO", gson.toJson(req), uniqId);
        QryIdcardInfoRspDto rtn = gson.fromJson(rspString, QryIdcardInfoRspDto.class);
        return rtn;
    }
    
    /**
     * 1.1.	查詢立約案件熔斷狀態(客戶自行填寫撥款資訊時查詢)
     * 1.1.1.	WebAPI名稱：KGI/GET_EXCEED_STATUS
     * (1)	CASE_NO：徵審案件編號，NVARCHAR(10)
     * (2)	EXP_BANK_CODE：撥款銀行代號，NVARCHAR(7)
     * (3)	EXP_BANK_ACCT_NO：撥款銀行帳號，NVARCHAR(20)
     * 
     * @see WebAPI介接說明_查詢立約案件熔斷狀態.docx
     */
    public GetExceedStatusRspDto call_GET_EXCEED_STATUS(String uniqId, String CASE_NO, String EXP_BANK_CODE, String EXP_BANK_ACCT_NO) throws Exception {
        GetExceedStatusReqDto req = new GetExceedStatusReqDto(CASE_NO, EXP_BANK_CODE, EXP_BANK_ACCT_NO);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryAPS("/KGI/GET_EXCEED_STATUS", gson.toJson(req), uniqId);
        GetExceedStatusRspDto rtn = gson.fromJson(rspString, GetExceedStatusRspDto.class);
        return rtn;
    }
    
    /**
     * 1.1.	貸款重複進件檢核
     * 1.1.1.	WebAPI名稱：KGI/NEW_CASE_CHK
     * (1)	CUSTOMER_ID：客戶身分證字號，NVARCHAR(10)
     * (2)	PROD_TYPE：產品別(1-GM/RPL、2-PLOAN)，NCHAR(1)
     *
     * @see WebAPI介接說明_貸款重複進件檢核.docx
    */
    public NewCaseChkRspDto call_NEW_CASE_CHK(String uniqId, String CUSTOMER_ID, String PROD_TYPE) throws Exception {
        NewCaseChkReqDto req = new NewCaseChkReqDto(CUSTOMER_ID, PROD_TYPE);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryAPS("/KGI/NEW_CASE_CHK", gson.toJson(req), uniqId);
        NewCaseChkRspDto rtn = gson.fromJson(rspString, NewCaseChkRspDto.class);
        return rtn;
    }

    // 白名單資料 GET_DGT_CONST
    public GetDgtConstRspDto getWhiteDgtConst(String uniqId, String idno, String DATA_TYPE) throws Exception {
        String startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
        String request = "{\"CUSTOMER_ID\":\" " + idno + "\",\"DATA_TYPE\":\"" + DATA_TYPE + "\"}";

        ContractWhiteList contractWhiteList = new ContractWhiteList();
        contractWhiteList.setProductId(DATA_TYPE);
        contractWhiteList.setStatus("01");

        ContractWhiteList resultList = contractWhiteListDao.Read(contractWhiteList);
        Gson gson = new Gson();
        GetDgtConstRspDto rtn = gson.fromJson(resultList.getResponse(), GetDgtConstRspDto.class);

        String endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");

        // 寫ContractApiLog
        contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, uniqId, "/KGI/GET_DGT_CONST", request,
                resultList.getResponse(), startTime, endTime);

        return rtn;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------
    // STP
    // @see [功能規格]WebAPI介接說明_決策平台.docx
    // @see airloanEX.DEC.ServerHost
    
    /*
     * 取數位編號
     * 2.1.	新增案件
     * 2.1.1.	WebAPI名稱：api/KGI/ADD_LOAN_CASE
     * 
     * (1)	CUSTOMER_ID：客戶ID，(string)
     * (2)	DATA_TYPE：介接系統(AA.信用卡系統,BB.PLOAN徵審系統,CC.數位申辦平台)，(string)
     * (3)	CASE_NO_REF：介接系統案件編號(卡加貸串接WEB端UUID)，CHAR(10)
     */
    public AddLoanCaseRspDto call_ADD_LOAN_CASE(String uniqId, String CUSTOMER_ID, String DATA_TYPE, String CASE_NO_REF) throws Exception {
        AddLoanCaseReqDto req = new AddLoanCaseReqDto(CUSTOMER_ID, DATA_TYPE, CASE_NO_REF);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryDEC("/api/KGI/ADD_LOAN_CASE", gson.toJson(req), uniqId);
        AddLoanCaseRspDto rtn = gson.fromJson(rspString, AddLoanCaseRspDto.class);
        return rtn;
    }
  
    /*
     * 發查 Z07 
     * PRODUCT帶入Z07
     * 2.3.	發查聯徵/票信
     * 2.3.1.	WebAPI名稱：api/KGI/SEND_JCIC
     * 
     * 
     * (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string)
     * (2)	APY_TYPE：產品別,1.現金卡(G+C)2.PLOAN(e貸寶)(P+G) (P+E) (P+C) (E+C) (P+G+C) (P+E+C)，(string)
     * (3)	PRODUCT: 發查產品,1.$04 2.$05(J10) 3.N30 4.票信 5.B72 6.Z07 7.Z13 8.B29($73) 9.A98，(string)
     * (4)	UNIT_ID：單位代號，非必填，預設值為消金徵審，(string)
     */
    public SendJcicRspDto call_SEND_JCIC(String uniqId, String CASE_NO, String APY_TYPE, String PRODUCT, String UNIT_ID) throws Exception {
        SendJcicReqDto req = new SendJcicReqDto(CASE_NO, APY_TYPE, PRODUCT, UNIT_ID);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryDEC("/api/KGI/SEND_JCIC", gson.toJson(req), uniqId);
        SendJcicRspDto rtn = gson.fromJson(rspString, SendJcicRspDto.class);
        return rtn;
    }

    /*
     * 2.117.	判斷是否有通報案件記錄(Z07)
     * 2.117.1.	WebAPI名稱：api/KGI/CHECK_Z07_RESULT
     * 
     * (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string)
     * (2)	APY_TYPE：產品別,1.現金卡(e貸寶) 2.PLOAN，(string)
     * (3)	IS_DGT：是否為數位案件(Y/N)，(string)
     * (4)	JCIC_DATE：聯徵查詢日期(YYYYMMDD)，(string)
     */
    public CheckZ07ResultRspDto call_CHECK_Z07_RESULT(String uniqId, String CASE_NO, String APY_TYPE, String IS_DGT, String JCIC_DATE) throws Exception {
        CheckZ07ResultReqDto req = new CheckZ07ResultReqDto(CASE_NO, APY_TYPE, IS_DGT, JCIC_DATE);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryDEC("/api/KGI/CHECK_Z07_RESULT", gson.toJson(req), uniqId);
        CheckZ07ResultRspDto rtn = gson.fromJson(rspString, CheckZ07ResultRspDto.class);
        return rtn;
    }

    /*
     * 判斷是否有舊有ID/財力
     * IMG_TYPE帶入2或3
     * 2.154.	判斷是否有舊有財力
     * 2.154.1.	WebAPI名稱：api/KGI/CHECK_HAVE_FINPF
     * 
     * (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string)
     * (2)	CUST_TYPE：客戶類型(01-PL、02-GM、03-RPL、04-CC)，(string)
     * (3)	IMG_TYPE：影像類別,1.申請書 2.身份證 3.財力證明 4.其他，(string)
     */
    public CheckHaveFinpfRspDto call_CHECK_HAVE_FINPF(String uniqId, String CASE_NO, String CUST_TYPE, String IMG_TYPE) throws Exception {
        CheckHaveFinpfReqDto req = new CheckHaveFinpfReqDto(CASE_NO, CUST_TYPE, IMG_TYPE);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryDEC("/api/KGI/CHECK_HAVE_FINPF", gson.toJson(req), uniqId);
        CheckHaveFinpfRspDto rtn = gson.fromJson(rspString, CheckHaveFinpfRspDto.class);
        return rtn;
    }

    /*
     * 2.127.	判斷是否為免財力證明客戶
     * 2.127.1.	WebAPI名稱：api/KGI/CHECK_NO_FINPF
     * 
     * (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string)
     * (2)	APY_TYPE：產品別,1.現金卡(e貸寶) 2.PLOAN 3.信用卡，(string)
     * (3)	IS_DGT：是否為數位案件(Y/N)，(string)
     * (4)	IS_IMM：是否為秒貸案件(Y/N)(預設為N)，(string)
     * (5)	CORP_TYPE：行業別，(string)
     * (6)	COMPANY_NAME：公司名稱，(string)
     * (7)	TITLE：職稱，(string)
     * (8)	CORP_TITLE：職務名稱代碼(textmining)，(string)
     * (9)	CUST_KIND：客戶類型(1.貸款2.現金卡3.信用卡4.薪轉戶，如果有多個類型用『;』區隔,例如01;02)，(string)
     * (10)	AML_RESULT：洗錢AML比對結果(Y/V/N/O/P)，(string)
     * (11)	APPLY_AMT：申請金額(單位:萬元)，(string)
     */
    public CheckNoFinpfRspDto call_CHECK_NO_FINPF(String uniqId, String CASE_NO, String APY_TYPE, String IS_DGT, String IS_IMM, String CORP_TYPE, String COMPANY_NAME, String TITLE, String CORP_TITLE, String CUST_KIND, String AML_RESULT, String APPLY_AMT, String YEARLY_INCOME) throws Exception {
        CheckNoFinpfReqDto req = new CheckNoFinpfReqDto(CASE_NO, APY_TYPE, IS_DGT, IS_IMM, CORP_TYPE, COMPANY_NAME, TITLE, CORP_TITLE, CUST_KIND, AML_RESULT, APPLY_AMT, YEARLY_INCOME);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryDEC("/api/KGI/CHECK_NO_FINPF", gson.toJson(req), uniqId);
		logger.info("######## CHECK_NO_FINPF=" + rspString);
        CheckNoFinpfRspDto rtn = gson.fromJson(rspString, CheckNoFinpfRspDto.class);
        return rtn;
    }

    /*
     * 2.15.	司法家事查詢(是否有監護/輔助事件資料)
     * 2.15.1.	WebAPI名稱：api/KGI/CHECK_GUARSHIP_RESULT
     * 
     * (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string)
     * (2)	CUSTOMER_NAME：客戶姓名，(string)
     * 
     */
    public CheckGuarshipResultRspDto call_CHECK_GUARSHIP_RESULT(String uniqId, String CASE_NO, String CUSTOMER_NAME) throws Exception {
        CheckGuarshipResultReqDto req = new CheckGuarshipResultReqDto(CASE_NO, CUSTOMER_NAME);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryDEC("/api/KGI/CHECK_GUARSHIP_RESULT", gson.toJson(req), uniqId);
        CheckGuarshipResultRspDto rtn = gson.fromJson(rspString, CheckGuarshipResultRspDto.class);
        return rtn;
    }

    /*
     * 2.115.	判斷聯徵發查是否正常
     * 2.115.1.	WebAPI名稱：api/KGI/CHECK_JCIC_RESULT
     * 
     * (1)	JCIC_HTML：聯徵判讀結果(string)
     */
    public CheckJcicResultRspDto call_CHECK_JCIC_RESULT(String uniqId, String JCIC_HTML) throws Exception {
        CheckJcicResultReqDto req = new CheckJcicResultReqDto(JCIC_HTML);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryDEC("/api/KGI/CHECK_JCIC_RESULT", gson.toJson(req), uniqId);
        CheckJcicResultRspDto rtn = gson.fromJson(rspString, CheckJcicResultRspDto.class);
        return rtn;
    }

    /*
     * 2.5.	取得聯徵貸款明細
     * 2.5.1.	WebAPI名稱：api/KGI/GET_JCIC_LOAN_DETAIL
     * 
     * (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string)
     */
    public GetJcicLoanDetailRspDto call_GET_JCIC_LOAN_DETAIL(String uniqId, String CASE_NO) throws Exception {
        GetJcicLoanDetailReqDto req = new GetJcicLoanDetailReqDto(CASE_NO);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryDEC("/api/KGI/GET_JCIC_LOAN_DETAIL", gson.toJson(req), uniqId);
        GetJcicLoanDetailRspDto rtn = gson.fromJson(rspString, GetJcicLoanDetailRspDto.class);
        return rtn;
    }

    /**
     * 2.152.	判斷客戶是否為薪轉戶(徵審)
     * 2.152.1.	WebAPI名稱：api/KGI/CHECK_SALARY_ACCOUNT
     * 2.152.2.	呼叫方式：POST
     * 2.152.3.	傳入參數(JSON格式)：
     * (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string)
     */
    public CheckSalaryAccountRspDto call_CHECK_SALARY_ACCOUNT(String uniqId, String CASE_NO) throws Exception {
        CheckSalaryAccountReqDto req = new CheckSalaryAccountReqDto(CASE_NO);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryDEC("/api/KGI/CHECK_SALARY_ACCOUNT", gson.toJson(req), uniqId);
        CheckSalaryAccountRspDto rtn = gson.fromJson(rspString, CheckSalaryAccountRspDto.class);
        return rtn;
    }

    /**
     * 2.128. 取得OTP電話 
     * 2.128.1. WebAPI名稱：KGI/GET_OTP_TEL_STP
     * 
     * @param uniqId
     * @param CUSTOMER_ID
     * @param BIRTHDATE
     * @return
     * @throws Exception
     */
    public GetOtpTelStpRspDto call_GET_OTP_TEL_STP(String uniqId, String CUSTOMER_ID, String BIRTHDATE)
            throws Exception {
        GetOtpTelStpReqDto req = new GetOtpTelStpReqDto(CUSTOMER_ID, BIRTHDATE);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryDEC("/api/KGI/GET_OTP_TEL_STP", gson.toJson(req), uniqId);
        GetOtpTelStpRspDto rtn = gson.fromJson(rspString, GetOtpTelStpRspDto.class);
        return rtn;
    }

    /**
     * 2.161.	取得KYC表負債狀況
     * 2.161.1.	WebAPI名稱：api/KGI/GET_KYC_DEBT_INFO
     * 2.161.2.	傳入參數(JSON格式)：
     * (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string)
     * (2)	APY_TYPE：產品別,1.現金卡(e貸寶) 2.PLOAN，(string)
     * (3)	IS_DGT：是否為數位案件(Y/N)，(string)
     * (4)	IS_IMM：是否為秒貸案件(Y/N)(預設為N)，(string)
     * (5)	JCIC_DATE：聯徵日(YYYYMMDD)，(string)
     */
    public GetKycDebtInfoRspDto call_GET_KYC_DEBT_INFO(String uniqId, String CASE_NO, String APY_TYPE, String IS_DGT, String IS_IMM, String JCIC_DATE) throws Exception {
        GetKycDebtInfoReqDto req = new GetKycDebtInfoReqDto(CASE_NO, APY_TYPE, IS_DGT, IS_IMM, JCIC_DATE);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryDEC("/api/KGI/GET_KYC_DEBT_INFO", gson.toJson(req), uniqId);
        GetKycDebtInfoRspDto rtn = gson.fromJson(rspString, GetKycDebtInfoRspDto.class);
        return rtn;
    }

    /**
     * 2.157.	取得額度體驗評分資訊(RPL)
     * 2.157.1.	WebAPI名稱：api/KGI/GET_TEST_RPL_SCORE
     * 2.157.2.	傳入參數(JSON格式)：
     * (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string)
     * (2)	MARRIAGE：婚姻狀況(1.未婚2.已婚)，(string)
     * (3)	JOBYEAR：工作年資，(string)
     * (4)	AGE：年齡，(string)
     * (5)	SEX：性別(1.男2.女)，(string)
     * (6)	EDUCATION：教育程度(1.高中以下2.大專3.大學4.研究所以上)，(string)
     * (7)	YEARLY_INCOME：年收入(單位:萬元)，(string)
     * (8)	CORP_TYPE：行業別代號，(string)
     * (9)	CORP_TITLE：職務名稱代碼，(string)    
     */
    public GetTestRplScoreRspDto call_GET_TEST_RPL_SCORE(String uniqId, String CASE_NO, String MARRIAGE, String JOBYEAR, String AGE, String SEX, String EDUCATION, String YEARLY_INCOME, String CORP_TYPE, String CORP_TITLE) throws Exception {
        GetTestRplScoreReqDto req = new GetTestRplScoreReqDto(CASE_NO, MARRIAGE, JOBYEAR, AGE, SEX, EDUCATION, YEARLY_INCOME, CORP_TYPE, CORP_TITLE);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryDEC("/api/KGI/GET_TEST_RPL_SCORE", gson.toJson(req), uniqId);
        GetTestRplScoreRspDto rtn = gson.fromJson(rspString, GetTestRplScoreRspDto.class);
        return rtn;
    }

    /**
     * 2.14.	判斷是否為利害關係人
     * 2.14.1.	WebAPI名稱：api/KGI/CHECK_RELP_RESULT
     * 2.14.2.	呼叫方式：POST
     * 2.14.3.	傳入參數(JSON格式)：
     * (1)	CASE_NO：案件編號(功能2.1取得的編號)，(string)
     * (2)	USER：查詢者，(string)
     */
    public CheckRelpResultRspDto call_CHECK_RELP_RESULT(String uniqId, String CASE_NO, String USER) throws Exception {
        CheckRelpResultReqDto req = new CheckRelpResultReqDto(CASE_NO, USER);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryDEC("/api/KGI/CHECK_RELP_RESULT", gson.toJson(req), uniqId);
        CheckRelpResultRspDto rtn = gson.fromJson(rspString, CheckRelpResultRspDto.class);
        return rtn;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------
    // CRP 黃頁平台
    /**
     * 根據片段名稱查詢公司名稱相似清單
     * api/company/full-name
     * 欄位 ShortName 必須是最小長度為 '2' 的字串或陣列型別。
     */
    public CompanyFullNameRspDto call_company_full_name(String uniqId, String ShortName, String Take) throws Exception {
        CompanyFullNameReqDto req = new CompanyFullNameReqDto(ShortName, Take);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryCRP("/api/company/full-name", gson.toJson(req), uniqId);
        CompanyFullNameRspDto rtn = gson.fromJson(rspString, CompanyFullNameRspDto.class);
        return rtn;
    }

    /**
     * 根據統編查詢完整公司資訊
     * api/company/info
     */
    public CompanyInfoRspDto call_company_info(String uniqId, String CompanyId, String CompanyName) throws Exception {
        CompanyInfoReqDto req = new CompanyInfoReqDto(CompanyId, CompanyName);
        Gson gson = new Gson();
        String rspString = apsPlContractService.queryCRP("/api/company/info", gson.toJson(req), uniqId);
        CompanyInfoRspDto rtn = gson.fromJson(rspString, CompanyInfoRspDto.class);
        return rtn;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------
    // SMS 報表簡訊平台(SMS) power by Ben
    /** 發送簡訊 sendSMS Charles:沒有規格文件只有email*/
    public SendSMSRspDto call_sendSMS(String uniqId, String phoneNumber, String billDepart, String message) throws Exception {
        SendSMSReqDto req = new SendSMSReqDto(phoneNumber, billDepart, message);
        Gson gson = new Gson();
        String rspString = apsPlContractService.querySMS("/sendSMS", gson.toJson(req), uniqId);
        SendSMSRspDto rtn = gson.fromJson(rspString, SendSMSRspDto.class);
        return rtn;
    }

    /** 
     * 檢核是否後端整件狀態已完整
     *  7.客戶重新登入，有接續斷點時，多呼叫影像系統新API(GetCaseComplete)檢核是否後端整件狀態已完整，如已完整，接續後直接到完成申辦頁(不用再上傳任何財力ID)
     */
    public GetCaseCompleteRspDto call_GET_CASE_COMPLETE(String uniqId, String ID, String UUID) throws Exception {
        GetCaseCompleteReqDto req = new GetCaseCompleteReqDto(ID, UUID);
        Gson gson = new Gson();
        String rspString = apsPlContractService.querySMS("/imgAPI/GET_CASE_COMPLETE", gson.toJson(req), uniqId);
        GetCaseCompleteRspDto rtn = gson.fromJson(rspString, GetCaseCompleteRspDto.class);
        return rtn;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------
    // APIM 戶役政
    /**
     * 查詢APIM戶役政 
     * 參數 personId : 身分證字號 
     *      applyYyymmdd: 發證日期 
     *      applyCode : 0:未領 1:領證 2:補證 3:換證 
     *      issueSiteId : 更換證件縣市 SELECT * FROM DropdownData WHERE enable=1 AND Name ='idlocation_new' ORDER BY DataKey DESC
     */
    public APIMInfoRspDto call_APIM_info(String reqBody) throws Exception {
        Gson reqGson = new Gson();
        APIMInfoReqDto req = reqGson.fromJson(reqBody, APIMInfoReqDto.class);
        APIMInfoRspDto rtn = new APIMInfoRspDto();
        Gson gson = new Gson();

        for(int i = 1; i < 5; i++) {
            String rspString = apsPlContractService.queryAPIM("/v1/moi/rwv2c2", gson.toJson(req), ExternalCallPostMan);
            System.out.println("### call_APIM_info run" + i + "==> " + rspString);
            rtn = gson.fromJson(rspString, APIMInfoRspDto.class);
            if (StringUtils.equals("OK", rtn.getHttpMessage()) && StringUtils.equals("200", rtn.getHttpCode())) {
                break;
            }
            rtn = new APIMInfoRspDto();
        }
        return rtn;
    }

    public APIMInfoRspDto call_APIM_info(String uniqId, String PersonId, String applyYyymmdd, String applyCode, String issueSiteId) throws Exception {
        APIMInfoReqDto req = new APIMInfoReqDto(PersonId, applyYyymmdd, applyCode, issueSiteId);
        APIMInfoRspDto rtn = new APIMInfoRspDto();
        Gson gson = new Gson();
        for(int i = 1; i < 5; i++) {
            String rspString = apsPlContractService.queryAPIM("/v1/moi/rwv2c2", gson.toJson(req), uniqId);
            System.out.println("### call_APIM_info run" + i + "==> " + rspString);
            rtn = gson.fromJson(rspString, APIMInfoRspDto.class);
            if (StringUtils.equals("OK", rtn.getHttpMessage()) && StringUtils.equals("200", rtn.getHttpCode())) {
                break;
            }
            rtn = new APIMInfoRspDto();
        }
        return rtn;
    }

    // ------------------------------------------------------------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------------------------------------------------------------
}
