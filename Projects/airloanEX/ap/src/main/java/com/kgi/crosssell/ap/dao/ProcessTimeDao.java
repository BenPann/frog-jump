package com.kgi.crosssell.ap.dao;

import java.util.List;

import com.kgi.eopend3.ap.dao.CRUDQDao;
import com.kgi.eopend3.common.dto.db.ProcessTime;

import org.springframework.stereotype.Repository;

@Repository
public class ProcessTimeDao extends CRUDQDao<ProcessTime> {

	@Override
	public int Create(ProcessTime fullItem) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ProcessTime Read(ProcessTime keyItem) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int Update(ProcessTime fullItem) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int Delete(ProcessTime keyItem) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<ProcessTime> Query(ProcessTime keyItem) {
		// TODO Auto-generated method stub
		return null;
	}
	public int processStart(String uniqId, String uniqType) {

		String sql = "INSERT INTO ProcessTime (UniqId, UniqType, StartTime, EndTime ,UpdateTime) VALUES (?,?,GETDATE(),null,GETDATE())";
		return this.getJdbcTemplate().update(sql,
                new Object[] { uniqId, uniqType });
	}
	
	public int processPageStart(String uniqId, String uniqType,String page) {

		String sql = "INSERT INTO ProcessPageTime (UniqId, UniqType,Page ,StartTime, EndTime ,UpdateTime) VALUES (?,?,?,GETDATE(),null,GETDATE())";
		return this.getJdbcTemplate().update(sql,
                new Object[] { uniqId, uniqType, page });
	}
	
}
