package com.kgi.eopend3.ap.service;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kgi.eopend3.ap.dao.SNDao;
import com.kgi.eopend3.common.util.DateUtil;


@Service
public class SNService {

	@Autowired
	private SNDao sndao;
	
	public String getUniqId(String uniqType) {
		//System.out.println("#### 呼叫取號....uniqType=" + uniqType);
		String preText = "";
		switch (uniqType) {
		case "01":
			// 體驗
			preText = "EXP";
			break;
		case "02":
			// 申請
			preText = "CASE";
			break;
		case "03":
			// 立約
			preText = "CON";
			break;
		case "04":
			//首頁的專人聯絡
			preText = "CONTACT";
			break;
		case "05":
			//信用卡申請
			preText = "CRED";
			break;
		default:
//			System.out.println("錯誤TYPE: " + uniqType);
			throw new RuntimeException("錯誤的TYPE");
		}
		String date = DateUtil.GetDateFormatString("yyyyMMdd");
		Integer sni = sndao.getSerialNumber(preText, date);
		// 組成 格式 (流程別)(今日日期)(流水號)
		//System.out.println("#### 取號結果=" + preText.concat(date).concat("1").concat(StringUtils.leftPad(sni.toString(), 4, "0")));
		return preText.concat(date).concat("1").concat(StringUtils.leftPad(sni.toString(), 4, "0"));
	}
	
	public String getpCode2566UniqId() {
		Integer sni = sndao.getSerialNumber("PCODE2566", "");
		Integer sn = sni % 10000;
		// 組成 格式 (流程別)(今日日期)(流水號)
		return StringUtils.leftPad(sn.toString(), 4, "0");
	}
	
	public String getUniqId() {		//先做數三
		String date = DateUtil.GetDateFormatString("yyyyMMdd");
		Integer sni = sndao.getSerialNumber("EDA", date);
		// 組成 格式 (流程別)(今日日期)(流水號)
		return "EDA".concat(date).concat(StringUtils.leftPad(sni.toString(), 5, "0"));
    }


	public String getAumAddCLSn() {
		String prefix = "CL" ;
		String date = DateUtil.GetDateFormatString() ;

//		String date = DateUtil.GetDateFormatString("yyyyMMdd");
		Integer sni = sndao.getSerialNumber("AUM_CL", date.substring(0, 6));
		// 組成 格式 (流程別)(今日日期)(流水號)
		return prefix.concat(date).concat(StringUtils.leftPad(sni.toString(), 6, "0"));
    }


}