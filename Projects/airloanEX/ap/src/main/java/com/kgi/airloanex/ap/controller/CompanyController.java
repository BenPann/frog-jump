package com.kgi.airloanex.ap.controller;

import javax.servlet.http.HttpServletRequest;

import com.kgi.airloanex.ap.service.CompanyService;
import com.kgi.eopend3.ap.controller.base.BaseController;
import com.kgi.eopend3.common.dto.KGIHeader;
import com.kgi.eopend3.common.dto.WebResult;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/company")
public class CompanyController extends BaseController {

    private static String context = "CompanyController";
    
    @Autowired
    private CompanyService companyService;

    @PostMapping("/full-name")
    public String fullName(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return companyService.companyFullName(uniqId, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/info")
    public String info(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return companyService.companyInfo(uniqId, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

}
