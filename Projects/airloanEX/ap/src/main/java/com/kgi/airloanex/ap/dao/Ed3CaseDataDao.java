package com.kgi.airloanex.ap.dao;

import com.kgi.airloanex.common.dto.db.Ed3CaseDataDto;
import com.kgi.eopend3.ap.dao.BaseDao;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class Ed3CaseDataDao extends BaseDao {
    
    public Ed3CaseDataDto getEd3CaseDataByAirloanUniqId(String airloanUniqId) {
    	String sql = "select e.Process, e.UserType, e.PromoChannelID, e.PromoDepart, e.BranchID from ED3_CaseData e " +
    							" inner join ED3_EX_CreditCaseData c " + 
    							" on e.UniqId=c.UniqId and c.AirloanUniqId=? ";
    	try {
    		return this.getJdbcTemplate().queryForObject(sql,new BeanPropertyRowMapper<>(Ed3CaseDataDto.class), airloanUniqId);
    	} catch (Exception e) {
    		return null ;
    	}
    }
}
