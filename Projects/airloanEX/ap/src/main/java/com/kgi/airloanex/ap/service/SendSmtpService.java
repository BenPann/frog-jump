package com.kgi.airloanex.ap.service;

import com.kgi.airloanex.ap.dao.MailHistoryDao;
import com.kgi.airloanex.common.dto.db.MailHistory;
import com.kgi.eopend3.common.util.CommonFunctionUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SendSmtpService {

    private final String MAILTYEP = "2";
    
    @Autowired
    private SmtpContentService contentService;

    @Autowired
    private MailHistoryDao mailHistoryDao;

    public void smtpWithCreditCardApplySuccess(String uniqId,String uniqType, String updateTime, String name,
                                               String idno, String gender, String phone, String cardTitle,String TMMail,String userEmail) {
    	String product = "信用卡";
    	if("12".equals(uniqType)) {
    		product="薪+卡";
    	}else if("13".equals(uniqType)) {
    		product="存+卡";
    	}
        String applyDate = updateTime.split(" ")[0].replaceAll("-", "/");
        String applyTime = updateTime.split(" ")[1];
        String contacTime = "無";
        String status = "已上傳影像";
        String title = "【信用卡】申辦完成" + CommonFunctionUtil.hiddenUserDataWithStar(name, 1, 1);
        String content = "";
        MailHistory mailHistory = MailHistory.getInstance();
        try {
            content = contentService.getSMTPContent(uniqId,uniqType,null,applyDate, applyTime, name, idno, gender, phone, contacTime, status, cardTitle,product,"","","","",userEmail, "");
            mailHistory = setSimpleMailHistory(title, content, uniqId, TMMail);
        } catch (Exception e) {
//            e.printStackTrace();
            mailHistory.setStatus("9");
            mailHistory.setErrorMessage(e.getStackTrace().toString());
        }

        mailHistoryDao.Create(mailHistory);

    }

    private MailHistory setSimpleMailHistory(String title, String content, String uniqId, String mailAddress) {
        MailHistory mailHistory = MailHistory.getInstance();
        mailHistory.setContent(content).setUniqId(uniqId).setMailType(MAILTYEP).setStatus("0").setTitle(title).setEMailAddress(mailAddress);
        return mailHistory;
    }


}
