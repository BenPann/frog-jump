package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.QR_ChannelDepartListEOP;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class QR_ChannelDepartListEOPDao extends CRUDQDao<QR_ChannelDepartListEOP> {

    @Override
    public int Create(QR_ChannelDepartListEOP fullItem) {
        return 0;
    }

    @Override
    public QR_ChannelDepartListEOP Read(QR_ChannelDepartListEOP keyItem) {
        String sql = "SELECT * FROM QR_ChannelDepartListEOP WHERE ChannelId = ? AND DepartId = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_ChannelDepartListEOP.class),
                    new Object[] { keyItem.getChannelId(), keyItem.getDepartId() });
        } catch (DataAccessException ex) {
//            System.out.println("QR_ChannelDepartListEOP查無資料");
            return null;
        }
    }

    @Override
    public int Update(QR_ChannelDepartListEOP fullItem) {
        return 0;
    }

    @Override
    public int Delete(QR_ChannelDepartListEOP keyItem) {
        return 0;
    }

    @Override
    public List<QR_ChannelDepartListEOP> Query(QR_ChannelDepartListEOP keyItem) {
        return null;
    }
}