package com.kgi.airloanex.ap.service;

import static com.kgi.airloanex.common.PlContractConst.*;

import java.io.InputStream;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.CreditCaseDataDao;
import com.kgi.airloanex.ap.dao.Ed3CaseDataDao;
import com.kgi.airloanex.ap.dao.EopQueryHelper;
import com.kgi.airloanex.ap.dao.MailHistoryDao;
import com.kgi.airloanex.ap.dao.QR_ChannelDepartListDao;
import com.kgi.airloanex.ap.dao.UserPhotoDao;
import com.kgi.airloanex.ap.pdf.PDFHelper;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.db.CreditCaseDataExtend;
import com.kgi.airloanex.common.dto.db.Ed3CaseDataDto;
import com.kgi.airloanex.common.dto.db.MailHistory;
import com.kgi.airloanex.common.dto.db.QR_ChannelDepartList;
import com.kgi.airloanex.common.dto.db.UserPhoto;
import com.kgi.airloanex.common.util.ExportUtil;
import com.kgi.eopend3.ap.config.GlobalConfig;
import com.kgi.eopend3.ap.dao.DropdownDao;
import com.kgi.eopend3.ap.xml.CreateXML;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

// import com.kgi.airloanex.ap.dao.UserPhotoDao;

@Service
public class PhotoService {

    @Autowired
	private GlobalConfig global;

    @Autowired
	private AirloanEXConfig globalConfig;

    // @Autowired
    // private UserPhotoDao userPhotoDao;

    @Autowired
    private JobLogService jobLogService;

    @Autowired
    private CreditCaseDataDao creditCaseDataDao;

    @Autowired
    private FTPService ftpservice;

    @Autowired
    private UserPhotoDao userPhotoDao;

    @Autowired
    private AMLService amlService;

    @Autowired
    private SOAPService soapService;

    @Autowired
    private MailHistoryDao mailHistoryDao;

    @Autowired
    private ChannelDepartService channelDepartService;

    @Autowired
    private QR_ChannelDepartListDao channelDepartListDao;

    @Autowired
    private SendSmtpService sendSmtpService;

    @Autowired
    private EopQueryHelper eopQueryHelper;

    @Autowired
    private DropdownDao dropdownDataDao;

    @Autowired
    Ed3CaseDataDao ed3CaseDataDao ;

	//信用卡申請件上傳影像系統處理(C001)
    public void uploadCreditCaseDataToOrbit() throws Exception {
        //撈取尚未進件的案件資料
        List<Map<String, Object>> creditcasedata = creditCaseDataDao.queryWaitPhotoCase();

        if (creditcasedata.size() > 0) {
            int cntOK = 0;
            for (int i = 0; i < creditcasedata.size(); i++) {
                boolean pdfimageOK = false; //測試開true

                boolean photimageOK = false; //測試開true
                ArrayList<String> jpgfilename = new ArrayList<String>();
                int err = 0;
                String uniqid = creditcasedata.get(i).get("UniqId").toString();
                String idno = creditcasedata.get(i).get("Idno").toString();
                
				try {
                    String name = creditcasedata.get(i).get("ChtName").toString();
                    String updatetime = creditcasedata.get(i).get("UpdateTime").toString();
                    String gender = creditcasedata.get(i).get("Gender").toString();
                    String phone = creditcasedata.get(i).get("Phone").toString();
                    String productid = creditcasedata.get(i).get("ProductId").toString();
                    String userMail = creditcasedata.get(i).get("EmailAddress").toString();
                    List<Map<String, Object>> list = creditCaseDataDao.getCreditCardTypeTitle(uniqid, productid);
                    err = 99;
                    String cardtitle = "";
                    if (list.size() > 0) {
                        cardtitle = list.get(0).get("CardTitle").toString();
                    } else {
                        throw new RuntimeException("無對應之信用卡產品!!");
                    }

                    err = 1;
                    //2.將案件狀態標註為11送件中
                    creditCaseDataDao.updateStatusbyUniqId(uniqid, PlContractConst.CASE_STATUS_SUBMITTING);
                    //3.上傳申請書到FTP,申請書檔案放在第一個
                    List<Map<String, Object>> creditcasedoc = creditCaseDataDao.queryCreidtCaseDoc(uniqid);
                    //if(creditCaseDataDao.getCreditVerify(uniqid).get(0).get("VerifyStatus").equals("1")) {
                    int pdfphotosize = 0;
                    if (creditcasedoc.size() > 0) {
                        err = 2;
                        //將pdf轉成圖檔
                        PDFHelper pdfHelper = new PDFHelper();
                        byte[] photobyte = pdfHelper.DecryptPDF(Base64.getDecoder().decode(creditcasedoc.get(0).get("PdfContent").toString()), idno);
                        List<byte[]> listpdfphoto = pdfHelper.getImageList(photobyte);
                        pdfphotosize = listpdfphoto.size();
                        for (int j = 0; j < listpdfphoto.size(); j++) {
                            //申請書--組檔名
                            String filename = uniqid.concat(idno).concat("_" + String.format("%03d", j + 1)).concat(".jpg");
                            // System.out.println("### 1 filename=" + filename);

                            byte[] image = listpdfphoto.get(j);
                            
                            // IMAGE檔 - @export 儲存至檔案系統
                            if (globalConfig.isPDF_Export()) {
                                ExportUtil.export(global.PdfDirectoryPath + "/" + filename, image);
                            }
                            
                            //上傳到ftp
                            pdfimageOK = ftpservice.uploadImgBase64File(filename, image);
                            //上傳ftp失敗，拋出例外
                            if (!pdfimageOK) {
                                throw new RuntimeException("上傳ftp失敗!!");
                            }
                            jpgfilename.add(filename);
                        }
                    } else {
                        jobLogService.AddSendDataChkfailLog("UniqId:" + uniqid + " 查無申請書無法上傳");
                        //沒產好pdf需查問題狀態
                        creditCaseDataDao.updateStatusbyUniqId(uniqid, PlContractConst.CASE_STATUS_SUBMIT_ERROR);
                    }
                    err = 1;
//                    List<Map<String, Object>> casephotolist = creditCaseDataDao.queryPhotoData(idno, PlContractConst.PHOTO_ONLINE_STATUS_ONLINE, PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);
                    UserPhoto up = new UserPhoto();
                    up.setUniqId(uniqid);
                    up.setStatus(PlContractConst.USERPHOTO_STATUS_WAIT_UPLOAD_4);
                    up.setOnline(String.valueOf(PlContractConst.PHOTO_ONLINE_STATUS_ONLINE));
                    List<UserPhoto> userPhotoList = userPhotoDao.QueryByPKAndStatus(up);
                    int userPhotoListSize = userPhotoList.size();
                    for (int j = 0; j < userPhotoList.size(); j++) {
                        up = userPhotoList.get(j);
                        err = 2;
                        //3.1轉檔
                        ftpservice.fileToInputStream(up.getImageBig());
                        //3.2先組檔名
                        String filename = uniqid.concat(idno).concat("_" + String.format("%03d", (j + 1 + pdfphotosize))).concat(".jpg"); //如之後有要申請書 需注意檔名要從j+2開始
                        // System.out.println("### 2 filename=" + filename);
                        // IMAGE檔 - @export 儲存至檔案系統
                        if (globalConfig.isPDF_Export()) {
                            ExportUtil.export(global.PdfDirectoryPath + "/" + filename, up.getImageBig());
                        }

                        //上傳到ftp
                        photimageOK = ftpservice.uploadImgBase64File(filename);
                        //上傳ftp失敗，拋出例外
                        if (!photimageOK) {
//                            throw new RuntimeException("圖檔上傳ftp失敗!!");
                            up.setStatus(PlContractConst.USERPHOTO_STATUS_UPLOAD_FAIL);
                        } else {
                            up.setStatus(PlContractConst.USERPHOTO_STATUS_UPLOAD_SUCCESS);
                            jpgfilename.add(filename);
                        }
                        userPhotoDao.Update(up);
                    }
                  //中壽投保財力證明
                    List<Map<String, Object>> clPolicyImage = eopQueryHelper.getPolicyImageByCreditUniqId(uniqid);
                    
                    if (clPolicyImage!=null&&clPolicyImage.size() > 0 ) {
                        err = 25;
                        //將pdf轉成圖檔
                        PDFHelper pdfHelper = new PDFHelper();
                    	String b64 = org.apache.commons.codec.binary.Base64.encodeBase64String((byte[])clPolicyImage.get(0).get("PolicyImage")) ;
                        
                		int idx = b64.indexOf("base64") ;
                		if (idx > 0) {
                			b64 = b64.substring(idx+6, b64.length()) ;
                		}
                		
                		byte[] data =  org.apache.commons.codec.binary.Base64.decodeBase64(b64) ;
                        //byte[] photobyte = Base64.getDecoder().decode(clPolicyImage.get(0).get("PolicyImage").toString());
                        List<byte[]> listpdfphoto = pdfHelper.getImageList(data);
                        for (int j = 0; j < listpdfphoto.size(); j++) {
                            String filename = uniqid.concat(idno).concat("_" + String.format("%03d", (j + 1 + pdfphotosize+userPhotoListSize))).concat(".jpg");
                            // System.out.println("### 3 filename=" + filename);

                            byte[] image = listpdfphoto.get(j);

                            // IMAGE檔 - @export 儲存至檔案系統
                            if (globalConfig.isPDF_Export()) {
                                ExportUtil.export(global.PdfDirectoryPath + "/" + filename, image);
                            }

                            //上傳到ftp
                            pdfimageOK = ftpservice.uploadImgBase64File(filename, image);
                            //上傳ftp失敗，拋出例外
                            if (!pdfimageOK) {
                            	//申請書--組檔名
                                throw new RuntimeException("上傳ftp失敗!!");
                            }
                            jpgfilename.add(filename);
                        }
                    }

                    QR_ChannelDepartList channelDepart = this.channelDepartService.getCreditChannelDepart(uniqid);
                    //xmLComposeMain(身分證字號,案件編號,documentType,該案件所有資料,上傳影像之檔名,是否上傳影像成功,是否上傳pdf影像成功)
                    String photoxml = xmLComposeMain(idno, uniqid, "C001", creditcasedata.get(i), jpgfilename, jpgfilename.size() > pdfphotosize, pdfimageOK, channelDepart);
                    
                    if (!photoxml.equals("")) {
                        String fileName = DateUtil.GetDateFormatString("yyyyMMddHHmmssS").concat(uniqid).concat(idno) + ".xml";
                        // System.out.println("### 4 filename=" + fileName);
                        // XML檔 - @export 儲存至檔案系統
                        if (globalConfig.isPDF_Export()) {
                            ExportUtil.export(global.PdfDirectoryPath + "/" + fileName, photoxml.toString().getBytes());
                        }
                        
                        //將資料上傳到影像系統
                        InputStream in = IOUtils.toInputStream(photoxml, "UTF-8");
                        if (!ftpservice.uploadImgXml(fileName, in)) {
                            throw new RuntimeException("XML檔上傳ftp失敗!!");
                        }
                        //完成
                        jobLogService.AddSendDataInfoLog("信用卡一般進件案件：" + uniqid + ",XML檔名：" + fileName + ",XML資訊：" + photoxml);
                        err = 3;
                        String uniqType = creditcasedata.get(i).get("UniqType").toString();

                        sendSmtpService.smtpWithCreditCardApplySuccess(uniqid, uniqType, updatetime, name, idno, gender, phone, productid + "-" + cardtitle, channelDepart.getEMail(), userMail);
                        jobLogService.AddSendDataInfoLog("信用卡一般進件案件(UniqId:" + uniqid + ")發送SMTP成功");
                        //呼叫webservice
                        err = 4;

                        String assignDepart = this.channelDepartService.getAssignDepart(channelDepart, creditcasedata.get(i).get("UniqType").toString(), creditcasedata.get(i).get("UserType").toString());

                        //取得creditcasedata.UniqType

                        //是否為卡加貸 如果為02 就是卡加貸
                        Boolean isCCLoan = uniqType.equals("02");
                        //12/21 於信用卡以及貸款送件之後 判斷此案件是否黑名單(AML)或是否高風險職業類別,如果是 在發送給TM時 增加註記
                        String plusMessage = null;
                        if (isCCLoan) {
                            plusMessage = amlService.checkIsHighRisk(uniqid, "applyccLoan-cc");
                        } else {
                            plusMessage = amlService.checkIsHighRisk(uniqid, "creditcard");
                        }
                        MailHistory mailHistory = soapService.getCreditCMBProgress(idno, creditcasedata.get(i).get("ChtName").toString(), assignDepart, plusMessage);
                        mailHistory.setUniqId(uniqid);
                        mailHistoryDao.Create(mailHistory);

                        //6.將案件狀態標註為12送件完成
                        creditCaseDataDao.updateStatusbyUniqId(uniqid, PlContractConst.CASE_STATUS_SUBMIT_SUCCESS);
//                        creditCaseDataDao.updatePhotoStatus(idno, PlContractConst.PHOTO_STATUS_SUBMIT, PlContractConst.PHOTO_ONLINE_STATUS_ONLINE, PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);
                        cntOK++;
                    }
                    if (cntOK > 0) {
                        jobLogService.AddSendDataInfoLog("信用卡線上案件IDNO:" + idno + "打包後送程序本次處理成功");
                        try {
                            String delete = globalConfig.AdditionalPhotoDelete();
                            if (delete.equals("1")) {
                                UserPhoto userPhoto = new UserPhoto();
                                userPhoto.setUniqId(uniqid);
                                userPhoto.setOnline(String.valueOf(PlContractConst.PHOTO_ONLINE_STATUS_ONLINE));
                                userPhoto.setProdType(PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);
                                userPhotoDao.deleteUserPhotoByAllValue(userPhoto);
                                /*creditCaseDataDao.deletePhotoData(idno, PlContractConst.PHOTO_ONLINE_STATUS_ONLINE,
                                        PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);*/
                            }
                            jobLogService.AddSendDataInfoLog("信用卡線上案件IDNO:" + idno + "完成送件，處理刪除相片資料完成。");
                        } catch (Exception e) {
                            jobLogService.AddSendDataErrorLog("信用卡線上案件IDNO:" + idno + "完成送件，處理刪除相片資料失敗。");
                        }
                    } else {
                        jobLogService.AddSendDataInfoLog("信用卡線上案件IDNO:" + idno + "打包後送程序本次處理失敗");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    switch (err) {
                        case 1:
                            jobLogService.AddSendDataConnOutLog("一般進件案件".concat(uniqid).concat("資料庫連線失敗"));
                            break;
                        case 2:
                            jobLogService.AddSendDataConnOutLog("一般進件案件".concat(uniqid).concat("XML或圖片或申請書上傳ftp失敗"));
                            break;
                        case 3:
                            jobLogService.AddSendDataConnOutLog("一般進件案件".concat(uniqid).concat("發信通知客戶失敗"));
                            break;
                        case 4:
                            jobLogService.AddSendDataConnOutLog("一般進件案件".concat(uniqid).concat("呼叫影像WebService失敗"));
                            break;
                        case 99:
                            jobLogService.AddSendDataErrorLog("一般進件案件".concat(uniqid).concat("無對應之信用卡產品"));
                            break;
                    }
                    //jobLogService.AddSendDataErrorLog("一般進件案件".concat(e.toString()));
                    //當連不到ws時，讓狀態直接完成，不影響後面流程。
                    if (err == 3) {
                        //if(creditCaseDataDao.getCreditVerify(uniqid).get(0).get("VerifyStatus").equals("1")) {
                        creditCaseDataDao.updateStatusbyUniqId(uniqid, PlContractConst.CASE_STATUS_SUBMIT_SUCCESS);
//                        creditCaseDataDao.updatePhotoStatus(idno, PlContractConst.PHOTO_STATUS_SUBMIT, PlContractConst.PHOTO_ONLINE_STATUS_ONLINE, PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);
                        //}else {
                        //creditCaseDataDao.updateStatusbyUniqId(uniqid, PlContractConst.CASE_STATUS_REJECT_SENDED);
                        //}

                    } else if (err == 99) {
                        creditCaseDataDao.updateStatusbyUniqId(uniqid, PlContractConst.CASE_STATUS_PRODUCT_ERROR);
                    } else {
                        creditCaseDataDao.updateStatusbyUniqId(uniqid, PlContractConst.CASE_STATUS_SUBMIT_FAIL);
                    }

                }
            }
            jobLogService.AddSendDataInfoLog("信用卡一般進件案件打包後送本次處理總共：" + creditcasedata.size() + "筆，處理成功計有：" + cntOK + "筆，處理失敗計有" + (creditcasedata.size() - cntOK) + "筆。");
        }
    }

    // public void uploadAddPhotoOrbitData() throws Exception {
    //     // 取得狀態為待處理的addPhotoStatus
    //     // List<Map<String, Object>> caselist = creditCaseDataDao.queryPhotoDataByStatus(PlContractConst.PHOTO_STATUS_WAIT);
    //     List<UserPhoto> userPhotos = userPhotoDao.findByStatus(USERPHOTO_STATUS_WAIT_UPLOAD_4);
    //     if (userPhotos.size() == 0) {
    //         System.out.println("no Data");
    //         return;
    //     }
    //     for (int i = 0; i < userPhotos.size(); i++) {
    //         if (userPhotos.get(i).getProdType().equals(USERPHOTO_ProdType_1)) {
    //             updateCreditCardAddPhotoStatus(userPhotos.get(i));
    //         }
    //     }
    // }

    public void updateCreditCardAddPhotoStatus(UserPhoto userPhoto) {
        String idno = "";
        // 先判斷是線上還線下
        if (userPhoto.getOnline().equals("1")) {
            // 線上件
//            String cname = "";
//            String uniqid = "";
            // 取得最新的一筆CreditCaseData且是送出申請出成功的
//            List<Map<String, Object>> casebyidlist = creditCaseDataDao.queryNewCreditCaseData(idno, PlContractConst.CASE_STATUS_SUBMIT_SUCCESS);
//            if (casebyidlist.size() > 0) {
            try {
                List<Map<String, Object>> creditCaseData = uploadonlineCase(userPhoto);
                /*if (creditCaseData != null && creditCaseData.size() > 0) {
                    String cname = creditCaseData.get(0).get("ChtName").toString();
                    String uniqid = creditCaseData.get(0).get("UniqId").toString();
                    idno = creditCaseData.get(0).get("Idno").toString();
                    // 補件案件 呼叫kgi訊息平台 1:成功
                    QR_ChannelDepartList channelDepart = channelDepartService.getCreditChannelDepart(uniqid);
                    CreditCaseDataExtend credit = creditCaseDataDao.getCreditDataByUniqId(uniqid);
                    String assignDepart = channelDepartService.getAssignDepart(channelDepart, credit.getUniqType(), credit.getUserType());
                    MailHistory creditAddPhotoCMBProgress = soapService.getCreditAddPhotoCMBProgress(idno, cname, assignDepart);
                    creditAddPhotoCMBProgress.setUniqId(uniqid);
                    mailHistoryDao.Create(creditAddPhotoCMBProgress);

                    // 將送上ftp的檔案註記為已送到影像中心
                    //creditCaseDataDao.updatePhotoStatus(idno, PlContractConst.PHOTO_STATUS_SUBMIT, PlContractConst.PHOTO_ONLINE_STATUS_ONLINE, PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);
                }*/
            } catch (Exception e) {
                jobLogService.AddSendDataErrorLog("補件案件(C) ".concat(idno).concat(" " + e.toString()));
            }
//            }
        } else {
            String unitId = userPhoto.getUnitId();
            idno = userPhoto.getUniqId();
            try {
                // 線下件
                uploadofflineCase(userPhoto);
                // 補件案件 呼叫kgi訊息平台 1:成功
                /*MailHistory creditAddPhotoCMBProgress = soapService.getCreditAddPhotoCMBProgress(idno, "", unitId);
                creditAddPhotoCMBProgress.setUniqId(unitId);
                mailHistoryDao.Create(creditAddPhotoCMBProgress);*/
                System.out.println("ftp上傳成功!");
                //creditCaseDataDao.updatePhotoStatus(idno, PlContractConst.PHOTO_STATUS_SUBMIT, PlContractConst.PHOTO_ONLINE_STATUS_OFFLINE, PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);

            } catch (Exception e) {
                jobLogService.AddSendDataErrorLog("補件案件(D) ".concat(idno).concat(e.toString()));
            }
        }
    }

	//信用卡線上補件案件上傳
    public List<Map<String, Object>> uploadonlineCase(UserPhoto userPhoto) throws Exception {
        String uniqid = userPhoto.getUniqId();
        String idno = "";
        List<Map<String, Object>> caselist = creditCaseDataDao.queryDatabyUniqId(uniqid);
        if (caselist != null && caselist.size() > 0) {
            int cntOK = 0;
            // 過濾 case狀態 為填寫中、填寫完成、送件中的caseno
            if (caselist.get(0).get("Status").equals("01") || caselist.get(0).get("Status").equals("02")
                    || caselist.get(0).get("Status").equals("11")) {

            } else {
                idno = String.valueOf(caselist.get(0).get("Idno"));
                try {
                    boolean photimageOK = false;
//                    List<Map<String, Object>> casephotolist = creditCaseDataDao.queryPhotoData(idno,
//                            PlContractConst.PHOTO_ONLINE_STATUS_ONLINE, PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);
                    List<UserPhoto> querys = userPhotoDao.Query(userPhoto);
                    ArrayList<String> jpgfilename = new ArrayList<String>();
                    String phototime = DateUtil.GetDateFormatString("HHmmssS");
                    for (int j = 0; j < querys.size(); j++) {
                        userPhoto = querys.get(j);
                        // 3.1轉檔
                        ftpservice.fileToInputStream(userPhoto.getImageBig());
                        // 3.2先組檔名
                        String filename = uniqid.concat(idno).concat(phototime).concat("_P" + String.format("%03d", (j + 1))).concat(".jpg"); // 如之後有要申請書 需注意檔名要從j+2開始
                        // System.out.println("### 5 filename=" + filename);
                        // IMAGE檔 - @export 儲存至檔案系統
                        if (globalConfig.isPDF_Export()) {
                            ExportUtil.export(global.PdfDirectoryPath + "/" + filename, userPhoto.getImageBig());
                        }

                        // 上傳到ftp
                        photimageOK = ftpservice.uploadImgBase64File(filename);
                        // 上傳ftp失敗，拋出例外
                        if (!photimageOK) {
//                            throw new RuntimeException("圖檔上傳ftp失敗!!");
                            userPhoto.setStatus(PlContractConst.USERPHOTO_STATUS_UPLOAD_FAIL);
                        } else {
                            userPhoto.setStatus(PlContractConst.USERPHOTO_STATUS_UPLOAD_SUCCESS);
                            jpgfilename.add(filename);
                        }
                        userPhotoDao.Update(userPhoto);
                    }
                    QR_ChannelDepartList channelDepart = this.channelDepartService.getCreditChannelDepart(uniqid);
                    String photoxml = xmLComposeMainSubCaseType0(idno, uniqid, "C002", caselist.get(0), jpgfilename, jpgfilename.size() > 0, false, channelDepart);
                    if (!photoxml.equals("")) {
                        String fileName = DateUtil.GetDateFormatString("yyyyMMddHHmmssS").concat(uniqid).concat(idno)
                                + ".xml";
                        // 將資料上傳到影像系統
                        InputStream in = IOUtils.toInputStream(photoxml, "UTF-8");
                        // System.out.println("### 6 filename=" + fileName);
                        // XML檔 - @export 儲存至檔案系統
                        if (globalConfig.isPDF_Export()) {
                            ExportUtil.export(global.PdfDirectoryPath + "/" + fileName, photoxml.toString().getBytes());
                        }

                        if (!ftpservice.uploadImgXml(fileName, in)) {
                            throw new RuntimeException("XML檔上傳ftp失敗!!");
                        }
                        // 完成
                        jobLogService.AddSendDataInfoLog(
                                "信用卡線上件補件案件：" + uniqid + ",XML檔名：" + fileName + ",XML資訊：" + photoxml);
                        cntOK++;
                    }
                } catch (Exception e) {
                    jobLogService
                            .AddSendDataErrorLog("信用卡線上件補件案件：" + uniqid + " 上傳 ftp error:".concat(e.toString()));
                    throw new RuntimeException("信用卡線上補件失敗");
                }
            }
            if (cntOK > 0) {
                jobLogService.AddSendDataInfoLog("信用卡線上補件案件 IDNO:" + idno + " 處理成功");
                try {
                    String delete = globalConfig.AdditionalPhotoDelete();
                    if (delete.equals("1")) {
//                        creditCaseDataDao.deletePhotoData(idno, PlContractConst.PHOTO_ONLINE_STATUS_ONLINE,
//                                PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);
                        UserPhoto up = new UserPhoto();
                        up.setUniqId(uniqid);
                        up.setOnline(String.valueOf(PlContractConst.PHOTO_ONLINE_STATUS_ONLINE));
                        up.setProdType(PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);
                        userPhotoDao.deleteUserPhotoByAllValue(up);
						jobLogService.AddSendDataInfoLog("信用卡線上補件案件 IDNO:" + idno + " 完成上傳，刪除相片資料完成。");
					}
                    
                } catch (Exception e) {
                    jobLogService.AddSendDataErrorLog("信用卡線上補件案件 IDNO:" + idno + " 完成上傳，刪除相片資料失敗。");
                }
                String cname = caselist.get(0).get("ChtName").toString();
                // 補件案件 呼叫kgi訊息平台 1:成功
                QR_ChannelDepartList channelDepart = channelDepartService.getCreditChannelDepart(uniqid);
                CreditCaseDataExtend credit = creditCaseDataDao.getCreditDataByUniqId(uniqid);
                String assignDepart = channelDepartService.getAssignDepart(channelDepart, credit.getUniqType(), credit.getUserType());
                MailHistory creditAddPhotoCMBProgress = soapService.getCreditAddPhotoCMBProgress(idno, cname, assignDepart);
                creditAddPhotoCMBProgress.setUniqId(uniqid);
                mailHistoryDao.Create(creditAddPhotoCMBProgress);
            } else {
                
				if (!idno.equals("") && idno != null){
					jobLogService.AddSendDataInfoLog("信用卡線上補件案件(A) IDNO:" + idno + " 處理失敗");
				}
            }
        }
        return caselist;
    }

    public void uploadofflineCase(UserPhoto userPhoto) throws Exception {

//        List<Map<String, Object>> photolist = creditCaseDataDao.queryPhotoData(idno, PlContractConst.PHOTO_ONLINE_STATUS_OFFLINE, PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);
        List<UserPhoto> photolist = userPhotoDao.Query(userPhoto);
        String unitId = userPhoto.getUnitId();
        String idno = userPhoto.getUniqId();
        if (photolist.size() > 0) {

            int cntOK = 0;
            boolean sendimageOK = false;
            //日期先產好
//            String datestr = DateUtil.GetDateFormatString();
            List<String> fileNameList = new ArrayList<>();
            for (int j = 0; j < photolist.size(); j++) {
                userPhoto = photolist.get(j);

                // 3.1轉檔
                ftpservice.fileToInputStream(userPhoto.getImageBig());
                // 3.2先組檔名
                String filename = idno.concat(DateUtil.GetDateFormatString()).concat("_" + String.format("%03d", j + 1)).concat(".jpg");
                // System.out.println("### 7 filename=" + filename);
                // IMAGE檔 - @export 儲存至檔案系統
                if (globalConfig.isPDF_Export()) {
                    ExportUtil.export(global.PdfDirectoryPath + "/" + filename, userPhoto.getImageBig());
                }

                // 上傳到ftp
                sendimageOK = ftpservice.uploadImgBase64File(filename);
                // 上傳ftp失敗，改圖片狀態，繼續處理下一張圖片
                if (!sendimageOK) {
                    userPhoto.setStatus(PlContractConst.USERPHOTO_STATUS_UPLOAD_FAIL);
                } else {
                    userPhoto.setStatus(PlContractConst.USERPHOTO_STATUS_UPLOAD_SUCCESS);
                    fileNameList.add(filename);
                }
                userPhotoDao.Update(userPhoto);
            }

            if (fileNameList.size() > 0) {
                // 產圖檔的XML檔將XML上傳到FTP
                InetAddress ip = InetAddress.getLocalHost();
                CreateXML photoxml = new CreateXML();
                Element root = photoxml.setRootTag("Orbit");
                photoxml.setChildHasTextNode(root, "machineIP", ip.getHostAddress());
                photoxml.setChildHasTextNode(root, "machineName", "mweb");
                /**
                 * 2019/02/26
                 * 補件功能：當選擇線下補件，產品選信用卡，當客戶選擇單位後，
                 * 影像上傳時xml中的上傳單位需依據單位配件大表的最右方欄位（新戶配件單位）指定。
                 */
                QR_ChannelDepartList kb = new QR_ChannelDepartList(PlContractConst.KB_9743_CHANNEL_ID, unitId);
                QR_ChannelDepartList read = channelDepartListDao.Read(kb);
                String branchId = (read != null ? read.getAssignDepartNew() : unitId);
                //2019/03/06 branchId是使用者輸入的單位
                photoxml.setChildHasTextNode(root, "branchID", unitId);

                photoxml.setChildHasTextNode(root, "userID", "9999");
                photoxml.setChildHasTextNode(root, "userName", "webuser");
                photoxml.setChildHasTextNode(root, "createTime", DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss"));
                // photoxml.setChildHasTextNode(root, "cus_type", "");
                photoxml.setChildHasTextNode(root, "subcaseType", "0");
                    System.out.println("#### 線下補件 : subcaseType=1");
                
                // photoxml.setChildHasTextNode(root, "uuid", "");
                photoxml.setChildHasTextNode(root, "email", "");
                photoxml.setChildHasTextNode(root, "notes", "客戶填寫資料:");
                photoxml.setChildHasTextNode(root, "attachment", "");
                // photoxml.setChildHasTextNode(root, "productTypeID", "");//撈取creditcasedata.productid
                // photoxml.setChildHasTextNode(root, "parentUUID", "");  //純信用卡不塞值  卡加貸的話塞CaseNo(UniqId)
                // photoxml.setChildHasTextNode(root, "cMarketingProgramId", "");//撈取creditcasedata.productid
                // photoxml.setChildHasTextNode(root, "cIncomeType", "");
                // photoxml.setChildHasTextNode(root, "cIncomeNote", "");
                // photoxml.setChildHasTextNode(root, "cSalesUnit", "");//撈取QR_PromoCase.PromoDepart1推廣單位
                // photoxml.setChildHasTextNode(root, "cSalesId", "");//撈取QR_PromoCase.PromoMember1推廣人員
                // photoxml.setChildHasTextNode(root, "mApplyAmount", "");//純信用卡不塞值  卡加貸的話塞貸款金額(CaseData.p_apy_amount)
                // photoxml.setChildHasTextNode(root, "cReceiveBranch", "");
                Element scanFiles = photoxml.setChildTagNoTextNode(root, "scanFiles");

                try {
                    HashMap<String, String> map = new HashMap<String, String>();
                    for (int j = 0; j < fileNameList.size(); j++) {
                        String jpgname = fileNameList.get(j);
                        Element row = photoxml.setChildTagNoTextNode(scanFiles, "row");

                        // 判斷ROw第一筆 塞主要 XML Data
                        if (j == 0) {
                            map.put("fileName", jpgname);
                            map.put("caseId", "1");
                            map.put("FMID", "A001");
                            map.put("covertPage", "A001");
                            map.put("productType", "P038");
                            //2019/03/06 branchType是使用者輸入的推廣單位後  去 對應 ChannelDepartList 表格中的 AssignDepartNew 欄位值
                            map.put("branchType", "B" + branchId);//要改
                            map.put("casePriority", "F038");
                            map.put("documentType", "C002");
                            map.put("cardApplyType", "");
                            map.put("cardType", "");
                            map.put("idNumber", idno);
                            map.put("cardNumber", "");
                            map.put("detailId", "");
                        } else {
                            map.put("fileName", jpgname);
                            map.put("caseId", "");
                            map.put("FMID", "");
                            map.put("covertPage", "");
                            map.put("productType", "");
                            map.put("branchType", "");
                            map.put("casePriority", "");
                            map.put("documentType", "");
                            map.put("cardApplyType", "");
                            map.put("cardType", "");
                            map.put("idNumber", "");
                            map.put("cardNumber", "");
                            map.put("detailId", "");
                        }
                        photoxml.setAttribute(row, map);
                        map.clear();
                    }
                    String fileName = DateUtil.GetDateFormatString().concat(idno) + ".xml";
                    // 將資料上傳到影像系統
                    jobLogService.AddSendDataInfoLog("信用卡線下補件案件IDNO:" + idno + "打包後送XML檔名:" + fileName + " XML資訊：" + photoxml.toXMLFile());
                    // System.out.println("### 8 filename=" + fileName);
                    // XML檔 - @export 儲存至檔案系統
                    if (globalConfig.isPDF_Export()) {
                        ExportUtil.export(global.PdfDirectoryPath + "/" + fileName, photoxml.toXMLFile().getBytes());
                    }

                    InputStream in = IOUtils.toInputStream(photoxml.toXMLFile(), "UTF-8");
                    // 上傳ftp失敗，拋出例外
                    if (!ftpservice.uploadImgXml(fileName, in)) {
                        throw new RuntimeException("xml檔上傳ftp失敗!!");
                    }
                    cntOK++;
                } catch (Exception e) {
                    jobLogService
                            .AddSendDataConnOutLog("信用卡線下件補件案件IDNO:" + idno + " XML上傳ftp err:".concat(e.toString()));
                }
            }
            if (cntOK > 0) {

                jobLogService.AddSendDataInfoLog("信用卡線下補件案件IDNO:" + idno + "打包後送程序本次處理成功");
                try {
                    String delete = globalConfig.AdditionalPhotoDelete();
                    if (delete.equals("1")) {
//                        creditCaseDataDao.deletePhotoData(idno, PlContractConst.PHOTO_ONLINE_STATUS_OFFLINE, PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);
                        UserPhoto up = new UserPhoto();
                        up.setUniqId(idno);
                        up.setOnline(String.valueOf(PlContractConst.PHOTO_ONLINE_STATUS_OFFLINE));
                        up.setProdType(PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);
                        userPhotoDao.deleteUserPhotoByAllValue(up);
                        System.out.println("刪除成功!");
                    }
                    jobLogService.AddSendDataInfoLog("信用卡線下補件案件IDNO:" + idno + "完成送件，處理刪除相片資料完成。");
                } catch (Exception e) {
                    jobLogService.AddSendDataErrorLog("信用卡線下補件案件IDNO:" + idno + "完成送件，處理刪除相片資料失敗。");
                }
                MailHistory creditAddPhotoCMBProgress = soapService.getCreditAddPhotoCMBProgress(idno, "", unitId);
                creditAddPhotoCMBProgress.setUniqId(unitId);
                mailHistoryDao.Create(creditAddPhotoCMBProgress);
            } else {
                jobLogService.AddSendDataInfoLog("信用卡線下補件案件IDNO:" + idno + "打包後送程序本次處理失敗");
            }
        }
    }

    public String xmLComposeMainSubCaseType0(String idno, String uniqid, String documentType, Map<String, Object> creditcasedata, List<String> jpgfilename, boolean photo, boolean pdfphoto, QR_ChannelDepartList channelDepart) {
        int err = 0;
        try {

            if (photo || pdfphoto) {
                //5. 產圖檔的XML檔將XML上傳到FTP
                err = 2;
                //取得creditcasedata.UniqType
                String uniqType = creditcasedata.get("UniqType").toString();
                //是否為卡加貸 如果為02 就是卡加貸
                Boolean isCCLoan = uniqType.equals("02");
                String branchID = "" ;
                
                // 判斷是不是數三案件，是的話取數三的預約分行
                Ed3CaseDataDto ed3Casedata = ed3CaseDataDao.getEd3CaseDataByAirloanUniqId(uniqid) ;
                
                if (ed3Casedata != null && PlContractConst.CASEDATA_PROCESS_REV_BRANCH.equals(ed3Casedata.getProcess())) {
                	//20200602 與Vicky確認, 將分行號碼向右補0去查詢QR_ChannelDepartList 
                	QR_ChannelDepartList channelDepartRevBranch = new QR_ChannelDepartList();
                	channelDepartRevBranch.setChannelId(PlContractConst.KB_9743_CHANNEL_ID);
                	channelDepartRevBranch.setDepartId(ed3Casedata.getBranchID());
                	channelDepartRevBranch = channelDepartListDao.queryByChannelIdAndDepartId(channelDepartRevBranch);
                	branchID = channelDepartService.getAssignDepart(channelDepartRevBranch, uniqType, creditcasedata.get("UserType").toString());
                } else {
                	branchID = channelDepartService.getAssignDepart(channelDepart, uniqType, creditcasedata.get("UserType").toString());
                }

                if (branchID.equals("")) {
                    throw new RuntimeException("branchId是空值");
                }
                //取得creditcasedata.productid
                String productid = creditcasedata.get("ProductId").toString();
//				String productid = ""; //1016 依需求改空值

                err = 3;
                InetAddress ip = InetAddress.getLocalHost();
                CreateXML photoxml = new CreateXML();
                Element root = photoxml.setRootTag("Orbit");
                photoxml.setChildHasTextNode(root, "machineIP", ip.getHostAddress());
                photoxml.setChildHasTextNode(root, "machineName", "mweb");
                photoxml.setChildHasTextNode(root, "branchID", branchID);
                photoxml.setChildHasTextNode(root, "userID", "9999");
                photoxml.setChildHasTextNode(root, "userName", "webuser");
                photoxml.setChildHasTextNode(root, "createTime", DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss"));
                //photoxml.setChildHasTextNode(root, "cus_type", "");
                photoxml.setChildHasTextNode(root, "subcaseType", "0"); // 2020/07/27 影像系統要求信用卡補件subcaseType改為 0
				 System.out.println("#### xmLComposeMain : subcaseType=0");

                photoxml.setChildHasTextNode(root, "email", "");

                photoxml.setChildHasTextNode(root, "attachment", "");
                String productTypeID = "";
                if (uniqType.equals("02")) {
                    productTypeID = "P036";
                } else if (uniqType.equals("12")) {
                    productTypeID = "P042";
//                } else if (uniqType.equals("12")) {
                } else { // 薪轉／數三／中壽  同規則
                    productTypeID = "P038";
                }

                if (documentType.equals("C001")) {
                    // 申請件
                    photoxml.setChildHasTextNode(root, "uuid", uniqid);
                    photoxml.setChildHasTextNode(root, "productTypeID", productTypeID);// 撈取creditcasedata.productid
                    photoxml.setChildHasTextNode(root, "cMarketingProgramId", "");// 撈取creditcasedata.productid

                    if (isCCLoan) {
                        Map<String, Object> casedata = creditCaseDataDao.getCaseData(uniqid).get(0);
                        photoxml.setChildHasTextNode(root, "parentUUID", uniqid); // 純信用卡不塞值 卡加貸的話塞CaseNo(UniqId)
                        //卡加貸塞固定值9
                        photoxml.setChildHasTextNode(root, "cIncomeType", "9");
                        String period = casedata.get("p_period").toString();
                        String applyAmount = casedata.get("p_apy_amount").toString();
                        String productName = "";
                        switch (casedata.get("ProductId2").toString()) {
                            case "1":
                                productName = "PL";
                                break;
                            case "2":
                                productName = "PL";
                                break;
                            case "3":
                                productName = "RPL";
                                break;
                            default:
                                throw new RuntimeException("錯誤的產品ID");
                        }
                        String purposeName = "";
                        if (casedata.get("p_purpose").toString().equals("16")) {
                            purposeName = casedata.get("p_purpose_name").toString();
                        } else {
                            purposeName = dropdownDataDao.getDataNameByNameAndDataKey("moneypurpose", casedata.get("p_purpose").toString());
                        }
                        String notes = productName + "," + period + "年," + applyAmount + "萬," + purposeName;
                        //塞 產品ID 貸款用途
                        photoxml.setChildHasTextNode(root, "cIncomeNote", notes);
                        photoxml.setChildHasTextNode(root, "mApplyAmount", casedata.get("p_apy_amount").toString()); // 純信用卡不塞值
                        // 卡加貸的話塞貸款金額(CaseData.p_apy_amount)
                        photoxml.setChildHasTextNode(root, "notes", "客戶填寫資料:" + notes);
                    } else {
                        photoxml.setChildHasTextNode(root, "parentUUID", "");
                        photoxml.setChildHasTextNode(root, "cIncomeType", "");
                        photoxml.setChildHasTextNode(root, "cIncomeNote", "");
                        photoxml.setChildHasTextNode(root, "mApplyAmount", "");
                        photoxml.setChildHasTextNode(root, "notes", "");
                    }
                    // 20200605 cSalesUnit移除申請類型判斷不再分數三或一般信用卡申請
                    photoxml.setChildHasTextNode(root, "cSalesUnit",channelDepart.getPromoDepart1());//
                    photoxml.setChildHasTextNode(root, "cSalesId", channelDepart.getPromoMember1());//

                    photoxml.setChildHasTextNode(root, "cReceiveBranch", "");
                    photoxml.setChildHasTextNode(root, "referralUnit", channelDepart.getPromoDepart2());
                    photoxml.setChildHasTextNode(root, "referralMemberCode", channelDepart.getPromoMember2());
                    String chtName = creditcasedata.get("ChtName") != null ? creditcasedata.get("ChtName").toString() : "";
                    photoxml.setChildHasTextNode(root, "applicant", chtName);
                    String phone = creditcasedata.get("Phone") != null ? creditcasedata.get("Phone").toString() : "";
                    photoxml.setChildHasTextNode(root, "mobileNum", phone);
                    photoxml.setChildTagNoTextNode(root, "highRisk");
                    String occupation = creditcasedata.get("Occupation") != null ? creditcasedata.get("Occupation").toString() : "";
                    photoxml.setChildHasTextNode(root, "jobType", occupation);


                } else if (documentType.equals("C002")) {
                    // 補件
                }


                Element scanFiles = photoxml.setChildTagNoTextNode(root, "scanFiles");
                HashMap<String, String> map = new HashMap<String, String>();
                for (int j = 0; j < jpgfilename.size(); j++) {
                    String jpgname = jpgfilename.get(j);

                    Element row = photoxml.setChildTagNoTextNode(scanFiles, "row");
                    // 判斷ROw第一筆 塞主要 XML Data
                    if (j == 0) {
                        map.put("fileName", jpgname);
                        map.put("caseId", "1");
                        map.put("FMID", "A001");
                        map.put("covertPage", "A001");
                        String productType = "";
                        if (uniqType.equals("02")) {
                            productType = "P036";
                        } else if (uniqType.equals("05")) {
                            productType = "P038";
//                        } else if (uniqType.equals("12")) {
                        } else { // 薪轉／數三／中壽  同規則
                            productType = "P001";
                        }
                        map.put("productType", productType);

                        map.put("branchType", "B" + branchID);//要改
                      //2019/12/9 子誼說只有薪轉才要用財力證明判斷
                        if (uniqType.equals("02")) {
                            List<Map<String, Object>> caseDataList = creditCaseDataDao.getCaseData(uniqid);
                            System.out.println("CaseDataList : " + caseDataList.size());
                            if (caseDataList.size() > 0 && caseDataList.get(0).get("ProductId2").equals("3")) {
                                map.put("casePriority", "F036");
                            } else {
                                map.put("casePriority", "F037");
                            }
                        } else if (uniqType.equals("12")) {
                        	UserPhoto uPhoto = new UserPhoto();
                            uPhoto.setUniqId(uniqid);
                            uPhoto.setOnline(String.valueOf(PlContractConst.PHOTO_ONLINE_STATUS_ONLINE));
                            uPhoto.setProdType(PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);
                            List<UserPhoto> userPhotos = userPhotoDao.getFinancial(uPhoto);
                            if (userPhotos != null && userPhotos.size() > 0) {
                                map.put("casePriority", "F045");
                            } else {
                                map.put("casePriority", "F046");
                            }
//                        } else if (uniqType.equals("12")) {
                        } else { // 薪轉／數三／中壽  同規則
                            map.put("casePriority", "F038");                            
                        }
                        map.put("documentType", documentType);
                        map.put("cardApplyType", "D001");
                        // List<Map<String, Object>> creditCardTypeTitle = creditCaseDataDao.getCreditCardTypeTitle(uniqid, productid);
                        // System.out.println("creditCardTypeTitle : " + creditCardTypeTitle.size());
                        String cardType = "";
                        try {
                            cardType = creditCaseDataDao.getCreditCardTypeTitle(uniqid, productid).get(0).get("CardType").toString();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        map.put("cardType", "|" + cardType + "|");
                        map.put("idNumber", idno);
                        map.put("cardNumber", "");
                        map.put("detailId", "");
                    } else {
                        map.put("fileName", jpgname);
                        map.put("caseId", "");
                        map.put("FMID", "");
                        map.put("covertPage", "");
                        map.put("productType", "");
                        map.put("branchType", "");
                        map.put("casePriority", "");
                        map.put("documentType", "");
                        map.put("cardApplyType", "");
                        map.put("cardType", "");
                        map.put("idNumber", "");
                        map.put("cardNumber", "");
                        map.put("detailId", "");
                    }
                    photoxml.setAttribute(row, map);
                    map.clear();
                }
                return photoxml.toXMLFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
            switch (err) {
                case 0:
                    jobLogService.AddSendDataConnOutLog("信用卡一般進件案件".concat(uniqid).concat("資料庫連線失敗"));
                    break;
                case 1:
                    jobLogService.AddSendDataConnOutLog("信用卡一般進件案件".concat(uniqid).concat("圖片上傳ftp失敗"));
                    break;
                case 2:
                    jobLogService.AddSendDataConnOutLog("信用卡一般進件案件".concat(uniqid).concat("取得BranchId有誤"));
                    break;
                case 3:
                    jobLogService.AddSendDataConnOutLog("信用卡一般進件案件".concat(uniqid).concat("XML組合失敗"));
                    break;
            }
        }
        return "";

    }

    /**
     * @deprecated 未使用
     */
    public String xmLComposeMain(String idno, String uniqid, String documentType, Map<String, Object> creditcasedata, List<String> jpgfilename, boolean photo, boolean pdfphoto, QR_ChannelDepartList channelDepart) {
        int err = 0;
        try {

            if (photo || pdfphoto) {
                //5. 產圖檔的XML檔將XML上傳到FTP
                err = 2;
                //取得creditcasedata.UniqType
                String uniqType = creditcasedata.get("UniqType").toString();
                //是否為卡加貸 如果為02 就是卡加貸
                Boolean isCCLoan = uniqType.equals("02");
                String branchID = "" ;
                
                // 判斷是不是數三案件，是的話取數三的預約分行
                Ed3CaseDataDto ed3Casedata = ed3CaseDataDao.getEd3CaseDataByAirloanUniqId(uniqid) ;
                
                if (ed3Casedata != null && PlContractConst.CASEDATA_PROCESS_REV_BRANCH.equals(ed3Casedata.getProcess())) {
                	//20200602 與Vicky確認, 將分行號碼向右補0去查詢QR_ChannelDepartList 
                	QR_ChannelDepartList channelDepartRevBranch = new QR_ChannelDepartList();
                	channelDepartRevBranch.setChannelId(PlContractConst.KB_9743_CHANNEL_ID);
                	channelDepartRevBranch.setDepartId(ed3Casedata.getBranchID());
                	channelDepartRevBranch = channelDepartListDao.queryByChannelIdAndDepartId(channelDepartRevBranch);
                	branchID = channelDepartService.getAssignDepart(channelDepartRevBranch, uniqType, creditcasedata.get("UserType").toString());
                } else {
                	branchID = channelDepartService.getAssignDepart(channelDepart, uniqType, creditcasedata.get("UserType").toString());
                }

                if (branchID.equals("")) {
                    throw new RuntimeException("branchId是空值");
                }
                //取得creditcasedata.productid
                @SuppressWarnings("unused")
                String productid = creditcasedata.get("ProductId").toString();
//				String productid = ""; //1016 依需求改空值

                err = 3;
                InetAddress ip = InetAddress.getLocalHost();
                CreateXML photoxml = new CreateXML();
                Element root = photoxml.setRootTag("Orbit");
                photoxml.setChildHasTextNode(root, "machineIP", ip.getHostAddress());
                photoxml.setChildHasTextNode(root, "machineName", "mweb");
                photoxml.setChildHasTextNode(root, "branchID", branchID); // Charles: Ask Ben.
                photoxml.setChildHasTextNode(root, "userID", "9999");
                photoxml.setChildHasTextNode(root, "userName", "webuser");
                photoxml.setChildHasTextNode(root, "createTime", DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss"));
                //photoxml.setChildHasTextNode(root, "cus_type", "");
                photoxml.setChildHasTextNode(root, "subcaseType", "1"); // 2019/11/26 影像系統要求信用卡進件改為 1
                photoxml.setChildHasTextNode(root, "email", "");

                photoxml.setChildHasTextNode(root, "attachment", "");
                String productTypeID = "";
                if (uniqType.equals("02")) {
                    productTypeID = "P036";
                } else if (uniqType.equals("12")) {
                    productTypeID = "P042";
//                } else if (uniqType.equals("12")) {
                } else { // 薪轉／數三／中壽  同規則
                    productTypeID = "P038";
                }

                if (documentType.equals("C001")) {
                    // 申請件
                    photoxml.setChildHasTextNode(root, "uuid", uniqid);
                    photoxml.setChildHasTextNode(root, "productTypeID", productTypeID);// 撈取creditcasedata.productid
                    photoxml.setChildHasTextNode(root, "cMarketingProgramId", "");// 撈取creditcasedata.productid

                    if (isCCLoan) {
                        /*Map<String, Object> casedata = creditCaseDataDao.getCaseData(uniqid).get(0);
                        photoxml.setChildHasTextNode(root, "parentUUID", uniqid); // 純信用卡不塞值 卡加貸的話塞CaseNo(UniqId)
                        //卡加貸塞固定值9
                        photoxml.setChildHasTextNode(root, "cIncomeType", "9");
                        String period = casedata.get("p_period").toString();
                        String applyAmount = casedata.get("p_apy_amount").toString();
                        String productName = "";
                        switch (casedata.get("ProductId2").toString()) {
                            case "1":
                                productName = "PL";
                                break;
                            case "2":
                                productName = "PL";
                                break;
                            case "3":
                                productName = "RPL";
                                break;
                            default:
                                throw new RuntimeException("錯誤的產品ID");
                        }
                        String purposeName = "";
                        if (casedata.get("p_purpose").toString().equals("16")) {
                            purposeName = casedata.get("p_purpose_name").toString();
                        } else {
                            purposeName = dropdownDataDao.getDataNameByNameAndDataKey("moneypurpose", casedata.get("p_purpose").toString());
                        }
                        String notes = productName + "," + period + "年," + applyAmount + "萬," + purposeName;
                        //塞 產品ID 貸款用途
                        photoxml.setChildHasTextNode(root, "cIncomeNote", notes);
                        photoxml.setChildHasTextNode(root, "mApplyAmount", casedata.get("p_apy_amount").toString()); // 純信用卡不塞值
                        // 卡加貸的話塞貸款金額(CaseData.p_apy_amount)
                        photoxml.setChildHasTextNode(root, "notes", "客戶填寫資料:" + notes);*/
                    } else {
                        photoxml.setChildHasTextNode(root, "parentUUID", "");
                        photoxml.setChildHasTextNode(root, "cIncomeType", "");
                        photoxml.setChildHasTextNode(root, "cIncomeNote", "");
                        photoxml.setChildHasTextNode(root, "mApplyAmount", "");
                        photoxml.setChildHasTextNode(root, "notes", "");
                    }
                    String cSalesUnit = "";
                    /*if (ed3Casedata != null && PlContractConst.CASEDATA_PROCESS_REV_BRANCH.equals(ed3Casedata.getProcess())) {
                    	cSalesUnit = StringUtils.leftPad(branchID, 4, "0") ; 
                    } else {
                    	cSalesUnit = channelDepart.getPromoDepart1();
                    }*/

                    photoxml.setChildHasTextNode(root, "cSalesUnit",cSalesUnit);//
                    photoxml.setChildHasTextNode(root, "cSalesId", channelDepart.getPromoMember1());//

                    photoxml.setChildHasTextNode(root, "cReceiveBranch", "");
                    photoxml.setChildHasTextNode(root, "referralUnit", channelDepart.getPromoDepart2());
                    photoxml.setChildHasTextNode(root, "referralMemberCode", channelDepart.getPromoMember2());
                    String chtName = creditcasedata.get("ChtName") != null ? creditcasedata.get("ChtName").toString() : "";
                    photoxml.setChildHasTextNode(root, "applicant", chtName);
                    String phone = creditcasedata.get("Phone") != null ? creditcasedata.get("Phone").toString() : "";
                    photoxml.setChildHasTextNode(root, "mobileNum", phone);
                    photoxml.setChildTagNoTextNode(root, "highRisk");
                    String occupation = creditcasedata.get("Occupation") != null ? creditcasedata.get("Occupation").toString() : "";
                    photoxml.setChildHasTextNode(root, "jobType", occupation);


                } else if (documentType.equals("C002")) {
                    // 補件
                }


                Element scanFiles = photoxml.setChildTagNoTextNode(root, "scanFiles");
                HashMap<String, String> map = new HashMap<String, String>();
                for (int j = 0; j < jpgfilename.size(); j++) {
                    String jpgname = jpgfilename.get(j);

                    Element row = photoxml.setChildTagNoTextNode(scanFiles, "row");
                    // 判斷ROw第一筆 塞主要 XML Data
                    if (j == 0) {
                        map.put("fileName", jpgname);
                        map.put("caseId", "1");
                        map.put("FMID", "A001");
                        map.put("covertPage", "A001");
                        String productType = "";
                        if (uniqType.equals("02")) {
                            productType = "P036";
                        } else if (uniqType.equals("05")) {
                            productType = "P038";
//                        } else if (uniqType.equals("12")) {
                        } else { // 薪轉／數三／中壽  同規則
                            productType = "P001";
                        }
                        map.put("productType", productType);

                        map.put("branchType", "B" + branchID);//要改
                      //2019/12/9 子誼說只有薪轉才要用財力證明判斷
                        /*if (uniqType.equals("02")) { // FIXME: Charles: another day...
                            List<Map<String, Object>> caseDataList = creditCaseDataDao.getCaseData(uniqid);
                            System.out.println("CaseDataList : " + caseDataList.size());
                            if (caseDataList.size() > 0 && caseDataList.get(0).get("ProductId2").equals("3")) {
                                map.put("casePriority", "F036");
                            } else {
                                map.put("casePriority", "F037");
                            }
                        } else if (uniqType.equals("12")) {
                        	UserPhoto uPhoto = new UserPhoto();
                            uPhoto.setUniqId(uniqid);
                            uPhoto.setOnline(String.valueOf(PlContractConst.PHOTO_ONLINE_STATUS_ONLINE));
                            uPhoto.setProdType(PlContractConst.PHOTO_PRODUCTTYPE_CREDIT);
                            List<UserPhoto> userPhotos = userPhotoDao.getFinancial(uPhoto);
                            if (userPhotos != null && userPhotos.size() > 0) {
                                map.put("casePriority", "F045");
                            } else {
                                map.put("casePriority", "F046");
                            }
//                        } else if (uniqType.equals("12")) {
                        } else { // 薪轉／數三／中壽  同規則*/
                            map.put("casePriority", "F038");                            
                        /*}*/
                        map.put("documentType", documentType);
                        map.put("cardApplyType", "D001");
                        // FIXME: Charles: another day...
                        /*List<Map<String, Object>> creditCardTypeTitle = creditCaseDataDao.getCreditCardTypeTitle(uniqid, productid);
                        System.out.println("creditCardTypeTitle : " + creditCardTypeTitle.size());*/
                        String cardType = "";
                        /*try { // FIXME: Charles: another day...
                            cardType = creditCaseDataDao.getCreditCardTypeTitle(uniqid, productid).get(0).get("CardType").toString();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/
                        map.put("cardType", "|" + cardType + "|");
                        map.put("idNumber", idno);
                        map.put("cardNumber", "");
                        map.put("detailId", "");
                    } else {
                        map.put("fileName", jpgname);
                        map.put("caseId", "");
                        map.put("FMID", "");
                        map.put("covertPage", "");
                        map.put("productType", "");
                        map.put("branchType", "");
                        map.put("casePriority", "");
                        map.put("documentType", "");
                        map.put("cardApplyType", "");
                        map.put("cardType", "");
                        map.put("idNumber", "");
                        map.put("cardNumber", "");
                        map.put("detailId", "");
                    }
                    photoxml.setAttribute(row, map);
                    map.clear();
                }
                return photoxml.toXMLFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
            switch (err) {
                case 0:
                    jobLogService.AddSendDataConnOutLog("信用卡一般進件案件".concat(uniqid).concat("資料庫連線失敗"));
                    break;
                case 1:
                    jobLogService.AddSendDataConnOutLog("信用卡一般進件案件".concat(uniqid).concat("圖片上傳ftp失敗"));
                    break;
                case 2:
                    jobLogService.AddSendDataConnOutLog("信用卡一般進件案件".concat(uniqid).concat("取得BranchId有誤"));
                    break;
                case 3:
                    jobLogService.AddSendDataConnOutLog("信用卡一般進件案件".concat(uniqid).concat("XML組合失敗"));
                    break;
            }
        }
        return "";

    }
}