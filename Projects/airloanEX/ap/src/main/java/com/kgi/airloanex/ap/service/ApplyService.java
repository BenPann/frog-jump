package com.kgi.airloanex.ap.service;

import static com.kgi.airloanex.ap.service.CaseDataService.isGM;
import static com.kgi.airloanex.ap.service.CaseDataService.isMobilePhone;
import static com.kgi.airloanex.ap.service.CaseDataService.isNonePreAudit;
import static com.kgi.airloanex.ap.service.CaseDataService.isOldOne;
import static com.kgi.airloanex.ap.service.CaseDataService.isPreAudit;
import static com.kgi.airloanex.ap.service.CaseDataService.isRPL;
import static com.kgi.airloanex.ap.service.CaseDataService.splitAddress;
import static com.kgi.airloanex.common.PlContractConst.CASEDATA_marriage_married;
import static com.kgi.airloanex.common.PlContractConst.CASEDATA_marriage_single;
import static com.kgi.airloanex.common.PlContractConst.CASE_STATUS_WRITE_SUCCESS;
import static com.kgi.airloanex.common.PlContractConst.CIF_MARRIAGE_NO;
import static com.kgi.airloanex.common.PlContractConst.CIF_MARRIAGE_YES;
import static com.kgi.airloanex.common.PlContractConst.DOCUMENT_TYPE_LOAN_APPLYPDF;
import static com.kgi.airloanex.common.PlContractConst.DOCUMENT_TYPE_LOAN_KYC_PDF;
import static com.kgi.airloanex.common.PlContractConst.DROPDOWNDATA_NAME_ZIPCODEMAPPING;
import static com.kgi.airloanex.common.PlContractConst.Get_CDD_Rate_SystemType_G08;
import static com.kgi.airloanex.common.PlContractConst.IsPreAudit_1;
import static com.kgi.airloanex.common.PlContractConst.IsPreAudit_2;
import static com.kgi.airloanex.common.PlContractConst.KB_9743_DEPART_ID;
import static com.kgi.airloanex.common.PlContractConst.LOAN;
import static com.kgi.airloanex.common.PlContractConst.LOAN0000000000000;
import static com.kgi.airloanex.common.PlContractConst.LOAN_Err_NEW_CASE_CHK;
import static com.kgi.airloanex.common.PlContractConst.LOAN_ProductId_1_PA;
import static com.kgi.airloanex.common.PlContractConst.LOAN_ProductId_2_PL;
import static com.kgi.airloanex.common.PlContractConst.LOAN_ProductId_3_RPL;
import static com.kgi.airloanex.common.PlContractConst.UNIQ_TYPE_02;
import static com.kgi.airloanex.common.PlContractConst.USERPHOTO_PTYPE_1;
import static com.kgi.airloanex.common.PlContractConst.USERPHOTO_PTYPE_2;
import static com.kgi.airloanex.common.PlContractConst.USERPHOTO_ProdType_2;
import static com.kgi.airloanex.common.PlContractConst.USERPHOTO_SUBTYPE_99;
import static com.kgi.airloanex.common.PlContractConst.USERPHOTO_SUBTYPE_IDCARDBANK;
import static com.kgi.airloanex.common.PlContractConst.USERPHOTO_SUBTYPE_IDCARDFRONT;
import static com.kgi.airloanex.common.PlContractConst.USER_TYPE_0_NEW_ONE;
import static com.kgi.airloanex.common.PlContractConst.USER_TYPE_3_OLD_ONE;
import static com.kgi.airloanex.common.PlContractConst.CaseDataJobStatus.*;
import static com.kgi.airloanex.common.PlContractConst.CaseDataSubStatus.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.*;
import com.kgi.airloanex.common.PlContractConst.StempDataType;
import com.kgi.airloanex.common.dto.customDto.APIMInfoReqDto;
import com.kgi.airloanex.common.dto.customDto.APIMInfoRspDto;
import com.kgi.airloanex.common.dto.customDto.AddLoanCaseRspDto;
import com.kgi.airloanex.common.dto.customDto.AddRelShipReqDto;
import com.kgi.airloanex.common.dto.customDto.AddRelShipReqDtoDataCompList;
import com.kgi.airloanex.common.dto.customDto.AddRelShipReqDtoDataList;
import com.kgi.airloanex.common.dto.customDto.AddRelShipRspDto;
import com.kgi.airloanex.common.dto.customDto.CheckHaveFinpfRspDto;
import com.kgi.airloanex.common.dto.customDto.CheckNoFinpfRspDto;
import com.kgi.airloanex.common.dto.customDto.CheckRelpResultRspDto;
import com.kgi.airloanex.common.dto.customDto.CheckSalaryAccountRspDto;
import com.kgi.airloanex.common.dto.customDto.CheckZ07ResultRspDto;
import com.kgi.airloanex.common.dto.customDto.CreateNewCaseReqDto;
import com.kgi.airloanex.common.dto.customDto.CreateNewCaseRspDto;
import com.kgi.airloanex.common.dto.customDto.GetCaseCompleteRspDto;
import com.kgi.airloanex.common.dto.customDto.GetCifInfoRspDto;
import com.kgi.airloanex.common.dto.customDto.GetKycDebtInfoRspDtoDebtInfo;
import com.kgi.airloanex.common.dto.customDto.GetOtpTelStpRspDto;
import com.kgi.airloanex.common.dto.customDto.GetOtpTelStpRspDtoOtpData;
import com.kgi.airloanex.common.dto.customDto.GetTestRplScoreRspDto;
import com.kgi.airloanex.common.dto.customDto.GetTestRplScoreRspDtoScoreInfo;
import com.kgi.airloanex.common.dto.customDto.NewCaseChkRspDto;
import com.kgi.airloanex.common.dto.customDto.PlcusReqDto;
import com.kgi.airloanex.common.dto.customDto.QryIdcardInfoRspDto;
import com.kgi.airloanex.common.dto.customDto.SendAseRspDto;
import com.kgi.airloanex.common.dto.customDto.SendJcicRspDto;
import com.kgi.airloanex.common.dto.db.AuditData;
import com.kgi.airloanex.common.dto.db.CaseData;
import com.kgi.airloanex.common.dto.db.CaseMoiData;
import com.kgi.airloanex.common.dto.db.CaseProperties;
import com.kgi.airloanex.common.dto.db.FastPass;
import com.kgi.airloanex.common.dto.db.MemoData;
import com.kgi.airloanex.common.dto.db.PLCUs;
import com.kgi.airloanex.common.dto.db.Plexp;
import com.kgi.airloanex.common.dto.db.PreAudit;
import com.kgi.airloanex.common.dto.db.QR_ChannelDepartList;
import com.kgi.airloanex.common.dto.db.QR_ChannelList;
import com.kgi.airloanex.common.dto.db.QR_PromoCaseUserInput;
import com.kgi.airloanex.common.dto.db.UspGetCddRate;
import com.kgi.airloanex.common.dto.response.ApplyConfirmToOtp2Resp;
import com.kgi.airloanex.common.dto.response.ApplyPreAuditResp;
import com.kgi.airloanex.common.dto.response.ApplyPrepareConfirmResp;
import com.kgi.airloanex.common.dto.response.ApplyStartResp;
import com.kgi.airloanex.common.dto.response.ApplyWorkResp;
import com.kgi.airloanex.common.dto.response.CalMainResp;
import com.kgi.airloanex.common.dto.response.CaseDataResp;
import com.kgi.airloanex.common.dto.response.LoginResp;
import com.kgi.airloanex.common.dto.response.MustUploadFinpfResp;
import com.kgi.airloanex.common.dto.response.MustUploadIdcardResp;
import com.kgi.airloanex.common.dto.view.ApplyCSDataView;
import com.kgi.airloanex.common.dto.view.ApplyCalResultView;
import com.kgi.airloanex.common.dto.view.ApplyGetTestRplScoreView;
import com.kgi.airloanex.common.dto.view.ApplyInfoView;
import com.kgi.airloanex.common.dto.view.ApplyLaterAddonView;
import com.kgi.airloanex.common.dto.view.ApplyNewCreditcardView;
import com.kgi.airloanex.common.dto.view.ApplyRelationDegreeCorp;
import com.kgi.airloanex.common.dto.view.ApplyRelationDegreeView;
import com.kgi.airloanex.common.dto.view.ApplySecondDegree;
import com.kgi.airloanex.common.dto.view.ApplyStartView;
import com.kgi.airloanex.common.dto.view.ApplyThemePathView;
import com.kgi.airloanex.common.dto.view.ApplyUploadIdcardView;
import com.kgi.crosssell.ap.dao.CS_PersonalDataDao;
import com.kgi.crosssell.ap.dao.MemoDataDao;
import com.kgi.crosssell.common.SystemConst;
import com.kgi.crosssell.common.dto.CSCommonCifData;
import com.kgi.eopend3.ap.dao.BrowseLogDao;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.ap.dao.DropdownDao;
import com.kgi.eopend3.ap.dao.EOP_IPLogDao;
import com.kgi.eopend3.ap.dao.EntryDataDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.ap.service.DropDownService;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.db.BrowseLog;
import com.kgi.eopend3.common.dto.db.EOP_IPLog;
import com.kgi.eopend3.common.dto.db.EntryData;
import com.kgi.eopend3.common.dto.db.QR_ShortUrl;
import com.kgi.eopend3.common.util.CheckUtil;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.json.JSONObject;

@Service
public class ApplyService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ConfigDao configDao;

    @Autowired
    private SrnoService snSer;

    @Autowired
    private CaseDataDao caseDataDao;

    @Autowired
    private EntryDataDao entryDataDao;

    @Autowired
    private EOP_IPLogDao eopIPLogDao;

    @Autowired
    private BrowseLogDao browseLogDao;

    @Autowired
    private KgiService kgiService;

    @Autowired
    private CaseDataRelationDegreeDao caseDataRelationDegreeDao;

    @Autowired
    private CreditCaseDataDao creditCaseDataDao;

    @Autowired
    private AirloanEXConfig globalConfig;

    @Autowired
    private PdfService pdfService;

    @Autowired
    private PlexpService plexpService;

    @Autowired
    private PLCUsDao plcusdao;

    @Autowired
    private QR_PromoCaseUserInputDao qr_PromoCaseUserInputDao;

    @Autowired
    private AMLService amlService;

    @Autowired
    private CaseMoiDataDao casemoidatadao;

    @Autowired
    private QR_ChannelDepartListDao qr_ChannelDepartListDao;
    
    @Autowired
    private QR_ChannelListDao QR_ChannelListDao;

    @Autowired
    private DropdownDao dropdownDao;

    @Autowired
    private DropDownService dropdownService;

    @Autowired
    private WhiteListDao whiteListDao;

    @Autowired
    private CaseDocumentDao caseDocumentDao;

    @Autowired
    private CaseDataKycDebtInfoDao caseDataKycDebtInfoDao;

    @Autowired
    private UserPhotoService userPhotoService;

    @Autowired
    UspDao uspDao;

    // Oracle
    @Autowired
    private PreAuditDao preAuditDao;

    // MSSQL
    @Autowired
    private AuditDataDao auditDataDao;

    @Autowired
    private BankService bankService;

    @Autowired
    private QR_ShortUrlDao qrShortUrlDao;

    @Autowired
    private QR_ShortUrlCaseMatchDao qrShortUrlCaseMatchDao;

    @Autowired
    private FastPassDao fastPassDao;

    @Autowired
    CasePropertiesDao casePropertiesDao;

    @Autowired
    CS_PersonalDataDao csPersonalDataDao;

    @Autowired
    MemoDataDao memoDataDao;

    @Autowired
    private APSPlContractService apsService;

    @Autowired
    private SmsService smsService;

    @Autowired
    private CaseDataService caseDataService;

    @Autowired
    private ChannelDepartService channelDepartService;

    public String applyStart(String reqJson) {
        try {
            // 測試產製 CasePDF
            // pdfService.pdfGeneratorActViewAndSendEmail("CASE2020021700055"); // FIXME:
            // DELETE_ME
            // 測試產製 CreditCardPDF
            // pdfService.pdfGeneratorCreditCardPDF("CASE2020031300011");
            // 測試AML風險評級介接查詢規格
            // uspDao.usp_Get_CDD_Rate("ABC","A123456789","9600","104777");
            // 測試SMS簡訊發送
            // kgiService.call_sendSMS("QLOG2020122900001", "0929825703", "9600", "查爾斯 - 凱基銀行數位信貸測試簡訊!");
            // 測試PreAudit
            // PreAudit L123 = preAuditDao.findPreAudit("L123728181");
            // PreAudit D121 = preAuditDao.findPreAudit("D121319366");
            // logger.info("### L123=" + new Gson().toJson(L123));
            // logger.info("### D121=" + new Gson().toJson(D121));

            ApplyStartView login = new Gson().fromJson(reqJson, ApplyStartView.class);

            if (!CheckUtil.check(login)) {
                throw new ErrorResultException(1,
                        configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
            } else {
                String productId = login.getProductId();
                String entry = login.getEntry();
                String entryOther = login.getEntryOther();
                String uniqType = login.getUniqType();
                String idno = login.getIdno();
                String birthday = login.getBirthday();
                birthday = StringUtils.replace(birthday, "/", ""); // 1999/01/01 -> 19990101
                birthday = StringUtils.replace(birthday, "-", ""); // 1999-01-01 -> 19990101
                String phone = login.getPhone();
                String agentNo = login.getAgentNo();
                String forceNewApply = login.getForceNewApply();
                String expCaseNo = login.getExpCaseNo();
                String ipAddress = login.getIpAddress();
                String browser = login.getBrowser();
                String platform = login.getPlatform();
                boolean ch1 = login.isCh1();
                boolean ch2 = login.isCh2();
                String os = login.getOs();
                String mobile_tel = phone; // 預先以用戶輸入行動電話
                String stempDataType = ""; // DATA_TYPE 客戶類型(1.信用卡 2.現金卡(靈活卡) 3.存款 4.貸款) 目的是給apply verify 簡訊驗證顯示用戶於本行有效行動電話之服務名稱
                String shortUrl = findEntryDataOtherParamValueByParamName(entryOther, "qr");
                String pj = findEntryDataOtherParamValueByParamName(entryOther, "PJ");
                String cs = findEntryDataOtherParamValueByParamName(entryOther, "CS");

                // logger.info("###1st mobile_tel => " + mobile_tel);
                // --------------------------------------------------------------------
                GetCifInfoRspDto cif = null;
                // 呼叫API: GET_OTP_TEL_STP
                // 1.檢核生日是否正確 
                // 2.檢核API回傳身分別 
                // 3.取得API回傳電話陣列 
                // 4.如為舊戶，使用登入時輸入電話與行內電話比對，有比對到，則使用該電話發OTP，比對不到，使用行內電話第一順位電話(信用卡)發OTP
                // 5.如為舊戶，但沒有行內電話，顯示未於行內留存有效手機 
                // 6.如為新戶，使用登入時輸入發OTP

                // "0":驗證失敗，"":驗證成功(舊戶)，"10":新戶
                // @call 1. GET_OTP_TEL_STP
                GetOtpTelStpRspDto getOtpTelRspDto = null;

                try {
                    getOtpTelRspDto = kgiService.call_GET_OTP_TEL_STP(LOAN0000000000000, idno, birthday);
                } catch (Exception e) {
                    logger.error("未處理的錯誤", e);
                    return WebResult.GetResultString(99, "系統問題，如有任何需要協助，請洽客服人員(GET_OTP_TEL_STP)", null);
                }

                // logger.info("### GET_OTP_TEL_STP: " + new Gson().toJson(getOtpTelRspDto));

                boolean isNewOne = false; // 新戶
                boolean isOldOne = false; // 既有戶

                // "0":驗證失敗
                if (StringUtils.equals("0", getOtpTelRspDto.getCHECK_CODE())) {
                    logger.info("### GET_OTP_TEL_STP 驗證失敗");
                    return WebResult.GetResultString(99, getOtpTelRspDto.getERR_MSG(), null);
                } else { // "":驗證成功(舊戶)
                    if (StringUtils.equals("", getOtpTelRspDto.getCHECK_CODE())) {
                        if (has_OTP_TEL(getOtpTelRspDto.getOTP_DATA())) {
                            isOldOne = true;
                        } else {
                            isNewOne = true;    
                        }
                    } // "10":新戶
                    else if (StringUtils.equals("10", getOtpTelRspDto.getCHECK_CODE())) {
                        isNewOne = true;
                    } else {
                        logger.info("### 異常不是新戶或既有戶");
                        return WebResult.GetResultString(99, configDao.ReadConfigValue("CONT_Err_GETAPI", "系統問題，如有任何需要協助，請洽客服人員"), null);
                    }
                }
                logger.info(isNewOne ? "### 新戶" : "");
                logger.info(isOldOne ? "### 既有戶" : "");
                // --------------------------------------------------------------------
                // 是否為預核名單
                PreAudit preAudit = findPreAudit(pj, isOldOne, idno);
                boolean isPreAudit = (preAudit != null);
                // --------------------------------------------------------------------
                // 根據是否為預核名單、是否為額度利率體驗通路，判定 productId 為何？
                Plexp plexp = plexpService.findByCaseNo(expCaseNo);
                productId = makeProductId(productId, isPreAudit, plexp);
                // --------------------------------------------------------------------
                String creditLine = null;
                if (plexp != null) {
                    creditLine = plexp.getCreditLine();
                }
                // --------------------------------------------------------------------
                CaseData existing = null;
                boolean caseCreated = false; // 理債平台 NEW_CASE_CHK OK
                // 是否重新申請(isForceNewApply=discardOriginal 棄用原主檔)
                boolean isBegining = StringUtils.isBlank(forceNewApply);
                boolean isForceNewApply = StringUtils.equals("Y", forceNewApply);
                boolean isResumeApply = StringUtils.equals("N", forceNewApply);
                logger.info("### isBegining=" + isBegining);
                logger.info("### isForceNewApply=" + isForceNewApply);
                logger.info("### isResumeApply=" + isResumeApply);
                if (isBegining || isResumeApply) {
                    existing = caseDataDao.readInitData(idno, productId);
                    // logger.info("### existing=" + new Gson().toJson(existing));
                }
                // --------------------------------------------------------------------
                // 有 CaseData 主檔
                boolean isExists = existing != null;
                // 是否採用原主檔，接續斷點
                boolean isResumeBreakPoint = (isResumeApply && isExists);
                logger.info("### isResumeBreakPoint=" + isResumeBreakPoint);
                // --------------------------------------------------------------------
                String uniqId = null;

                // 進行登入
                if (isBegining) {
                    logger.info("### 進行登入");

                    if (isExists) {
                        logger.info("### 有舊 CaseData " + existing.getCaseNo());
                        // 原案件編號
                        uniqId = existing.getCaseNo();
                    } else {
                        uniqId = snSer.getLOANUniqId();
                        // 呼叫貸款重複進件檢核(KGI/NEW_CASE_CHK)，
                        // @call 9. NEW_CASE_CHK
                        if (whiteListDao.isBypassLoan(idno)) {
                            // bypass NEW_CASE_CHK
                        } else {
                            try {
                                // - 若否(無案件尚未結案)，則再呼叫[**理債平台**] 新增案件(STP) (KGI/CREATE_NEW_CASE)，取得理債平台編號 寫入 CaseNoWeb 欄位。
                                boolean isReadyForCreateNewCaseNo = isReadyForCreateNewCaseNo(uniqId, idno, productId);
                                caseCreated = !isReadyForCreateNewCaseNo;
                                if (caseCreated) {
                                    return WebResult.GetResultString(99, configDao.ReadConfigValue(LOAN_Err_NEW_CASE_CHK, "系統問題，如有任何需要協助，請洽客服人員"), null);
                                }
                            } catch (Exception e) {
                                logger.error("未處理的錯誤", e);
                                return WebResult.GetResultString(99, "系統問題，如有任何需要協助，請洽客服人員(NEW_CASE_CHK)", null);
                            }
                        }
                    }

                }
                // A. 採用原主檔，接續斷點
                else if (isResumeBreakPoint) { // isForceNewApply=N && CaseData exist
                    logger.info("### 採用原主檔，接續斷點");
                    logger.info("### 有舊 CaseData " + existing.getCaseNo());
                    if("04".equals(existing.getStatus())){

                        logger.info("已有紙本進件,不進斷點");

                        return WebResult.GetResultString(99, configDao.ReadConfigValue(LOAN_Err_NEW_CASE_CHK, "系統問題，如有任何需要協助，請洽客服人員"), null);
                    }
                    // 延用原案件編號
                    uniqId = existing.getCaseNo();

                    // 呼叫貸款重複進件檢核(KGI/NEW_CASE_CHK)，
                    // @call 9. NEW_CASE_CHK
                    if (whiteListDao.isBypassLoan(idno)) {
                        // bypass NEW_CASE_CHK
                    } else {
                        try {
                            // 7.客戶重新登入，有接續斷點時，多呼叫影像系統新API(GetCaseComplete)檢核是否後端整件狀態已完整，如已完整，接續後直接到完成申辦頁(不用再上傳任何財力ID)
                            boolean hasCaseNoWeb = StringUtils.isNotBlank(existing.getCaseNoWeb()); // 即已 CREATE_NEW_CASE
                            boolean isCaseComplete = isCaseComplete(existing.getCaseNo(), existing.getIdno(), existing.getCaseNoWeb());
                            if (isExists && hasCaseNoWeb && isCaseComplete) {
                                
                                // @setup caseData.SubStatus 12 100
                                caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_12_SubStatus_100);
                                
                                String page = "/UPL/stepok";
                                ApplyStartResp resp = makeResumeApplyStartResp(existing, idno, uniqType, ipAddress, true, page);
                                return WebResult.GetResultString(0, "成功", resp);
                            }
                            // - 若否(無案件尚未結案)，則再呼叫[**理債平台**] 新增案件(STP) (KGI/CREATE_NEW_CASE)，取得理債平台編號 寫入 CaseNoWeb 欄位。
                            boolean isReadyForCreateNewCaseNo = isReadyForCreateNewCaseNo(uniqId, idno, productId);
                            caseCreated = !isReadyForCreateNewCaseNo;

                            // 20210510: 判斷該案件CASENOWEB是否為空 是空的(代表線上尚未起案)，呼叫NEW_CASE_CHK
                            if (!hasCaseNoWeb) {
                                // 呼叫NEW_CASE_CHK，失敗--->alert(您尚有未結案的案件處理中)
                                if (caseCreated) {
                                    return WebResult.GetResultString(99, configDao.ReadConfigValue(LOAN_Err_NEW_CASE_CHK, "系統問題，如有任何需要協助，請洽客服人員"), null);
                                }
                            }
                        } catch (Exception e) {
                            logger.error("未處理的錯誤", e);
                            return WebResult.GetResultString(99, "系統問題，如有任何需要協助，請洽客服人員(NEW_CASE_CHK)", null);
                        }
                    }
                } 
                // B. 創立新主檔，不接續斷點(或無斷點)
                else if (isForceNewApply) { // isForceNewApply=Y
                    logger.info("### 創立新案件");
                    // 創立案件編號
                    uniqId = snSer.getLOANUniqId();

                    // 呼叫貸款重複進件檢核(KGI/NEW_CASE_CHK)，
                    // @call 9. NEW_CASE_CHK
                    if (whiteListDao.isBypassLoan(idno)) {
                        // bypass NEW_CASE_CHK
                    } else {
                        try {
                            // - 若否(無案件尚未結案)，則再呼叫[**理債平台**] 新增案件(STP) (KGI/CREATE_NEW_CASE)，取得理債平台編號 寫入 CaseNoWeb 欄位。
                            boolean isReadyForCreateNewCaseNo = isReadyForCreateNewCaseNo(uniqId, idno, productId);
                            caseCreated = !isReadyForCreateNewCaseNo;
                            if (caseCreated) {
                                return WebResult.GetResultString(99, configDao.ReadConfigValue(LOAN_Err_NEW_CASE_CHK, "系統問題，如有任何需要協助，請洽客服人員"), null);
                            }
                        } catch (Exception e) {
                            logger.error("未處理的錯誤", e);
                            return WebResult.GetResultString(99, "系統問題，如有任何需要協助，請洽客服人員(NEW_CASE_CHK)", null);
                        }
                    }
                }
                // --------------------------------------------------------------------
                // qrcode 短網址 shortURL 通路登入需寫入 QR_ShortUrlCaseMatch 
                // qr=短網址
                String channelId = findEntryDataOtherParamValueByParamName(entryOther, "channelId");
                if (StringUtils.isNotBlank(shortUrl)) {
                    qrShortUrlCaseMatchDao.insert(shortUrl, uniqId, UNIQ_TYPE_02);
                }
                // --------------------------------------------------------------------
                // Charles:再多打一次 GET_OTP_TEL_STP 避免用 UniqId 在 ContractApiLog 找不到
                // kgiService.call_GET_OTP_TEL_STP(uniqId, idno, birthday);

                String checkSalaryAccount = "N"; // 是否為薪轉戶
                String checkCreditcardAccount = "N"; // 是否為信用卡戶
                String checkCashcardAccount = "N"; // 是否為現金卡戶
                String checkKgibankAccount = "N"; // 是否為存款戶
                String checkLoanAccount = "N"; // 是否為貸款戶

                CSCommonCifData csCifData = new CSCommonCifData();

                // 若既有戶，則呼叫API: GET_CIF_INFO 
                // 用此API取得舊戶個人基本資料供前端帶入
                if (isOldOne) {

                    String stempMobile_tel = "";
                    
                    List<GetOtpTelStpRspDtoOtpData> otpDatas = getOtpTelRspDto.getOTP_DATA();
                    boolean isFirstPhone = true;
                    // DATA_TYPE 客戶類型(1.信用卡2.現金卡3.存款4.貸款)
                    for (GetOtpTelStpRspDtoOtpData otpData : otpDatas) {
                        if (StringUtils.equals("1", otpData.getDATA_TYPE())) { // 1.信用卡
                            checkCreditcardAccount = "Y";
                        } else if (StringUtils.equals("2", otpData.getDATA_TYPE())) { // 2.現金卡
                            checkCashcardAccount = "Y";
                        } else if (StringUtils.equals("3", otpData.getDATA_TYPE())) { // 3.存款
                            checkKgibankAccount = "Y";
                        } else if (StringUtils.equals("4", otpData.getDATA_TYPE())) { // 4.貸款
                            checkLoanAccount = "Y";
                        }
                        // 如果GET_OTP行動電話有效 改以此設定
                        if(isFirstPhone && StringUtils.equals("Y", otpData.getFORMAT()) && StringUtils.equals("", stempMobile_tel)) {
                            stempMobile_tel = otpData.getOTP_TEL();
                            stempDataType = otpData.getDATA_TYPE();
                            isFirstPhone = false;
                        } 
                        
                    }
                    // @call 2. GET_CIF_INFO
                    try {
                        cif = kgiService.call_GET_CIF_INFO(uniqId, idno, birthday);
						
						logger.info("#### before GET_CIF_INFO: " + new Gson().toJson(cif));
						logger.info("#### before EDU: " + cif.getEDU());
						//避免CIF回傳非代碼資料
						if (StringUtils.length(cif.getEDU())!=1){ 
							cif.setEDU("");
							logger.info("#### after GET_CIF_INFO: " + new Gson().toJson(cif));
						}	
						
						
                    } catch (Exception e) {
                        logger.error("未處理的錯誤", e);
                        return WebResult.GetResultString(99, "系統問題，如有任何需要協助，請洽客服人員(GET_CIF_INFO)", null);
                    }
                    // logger.info("### GET_CIF_INFO: " + new Gson().toJson(cif));

                    if (StringUtils.isNotBlank(stempMobile_tel)) {
                        mobile_tel = stempMobile_tel;
                    } else {
                        if(StringUtils.equals("", cif.getCHECK_CODE()) && !StringUtils.equals("", cif.getMOBILE_TEL())) {
                            mobile_tel = cif.getMOBILE_TEL();
                            stempDataType = StempDataType.Vip.code;
                         }
                    }

                    // logger.info("###2nd mobile_tel => " + mobile_tel);
                }

                // CrossSell 異業 make additionalData
                if(StringUtils.isNotEmpty(cs)){
                    csCifData = this.applyGetCSMemoData(cs, SystemConst.MEMO_CSCOMMON_CIF);
                }

                // --------------------------------------------------------------------
                // 寫入entry資料
                EntryData entryData = new EntryData(uniqId, uniqType, entry, LOAN, browser, platform, os);
                entryData.setOther(entryOther);
                entryDataDao.CreateOrUpdate(entryData);

                // EOP_IPLog
                EOP_IPLog eop_IPLog = new EOP_IPLog(uniqId, ipAddress);
                eopIPLogDao.Create(eop_IPLog);

                // --------------------------------------------------------------------
                // A. 採用原主檔，接續斷點
                if (isResumeBreakPoint) {
                    // pdfService.pdfGeneratorCaseKycPDF(existing.getCaseNo()); // TODO: DELETE_ME
                    if("04".equals(existing.getStatus())){
                        System.out.println("ApplyService ,applyStart 621 :"); 
                        logger.info("已有紙本進件,不進斷點");

                        return WebResult.GetResultString(99, configDao.ReadConfigValue(LOAN_Err_NEW_CASE_CHK, "系統問題，如有任何需要協助，請洽客服人員"), null);
                    }

                    // 20210510: 有斷點時，先判斷該案件CASENOWEB是否為空: 是空的(代表線上尚未起案)，呼叫NEW_CASE_CHK
                    boolean hasCaseNoWeb = StringUtils.isNotBlank(existing.getCaseNoWeb()); // 即已 CREATE_NEW_CASE
                    try {
                        // 呼叫NEW_CASE_CHK: 失敗--->alert(您尚有未結案的案件處理中)
                        boolean isReadyForCreateNewCaseNo = isReadyForCreateNewCaseNo(uniqId, idno, productId);
                        caseCreated = !isReadyForCreateNewCaseNo;
                        if (!hasCaseNoWeb) {
                            if (caseCreated) {
                                return WebResult.GetResultString(99, configDao.ReadConfigValue(LOAN_Err_NEW_CASE_CHK, "系統問題，如有任何需要協助，請洽客服人員"), null);
                            }
                        }
                    } catch (Exception e) {
                        logger.error("未處理的錯誤", e);
                        return WebResult.GetResultString(99, "系統問題，如有任何需要協助，請洽客服人員(NEW_CASE_CHK)", null);
                    }

                    String resume = resumeBreakPoint(existing, idno, birthday, phone, agentNo, uniqType, ipAddress,
                            entry, entryOther, expCaseNo, caseCreated);
                    return resume;
                }
                // D. 初登入且有 CaseData
                else if (isBegining && isExists) {
                    if("04".equals(existing.getStatus())){

                        logger.info("已有紙本進件,不進斷點");

                        return WebResult.GetResultString(99, configDao.ReadConfigValue(LOAN_Err_NEW_CASE_CHK, "系統問題，如有任何需要協助，請洽客服人員"), null);
                    }

                    // 20210510: 有斷點時，先判斷該案件CASENOWEB是否為空: 是空的(代表線上尚未起案)，呼叫NEW_CASE_CHK
                    boolean hasCaseNoWeb = StringUtils.isNotBlank(existing.getCaseNoWeb()); // 即已 CREATE_NEW_CASE
                    try {
                        // 呼叫NEW_CASE_CHK: 失敗--->alert(您尚有未結案的案件處理中)
                        boolean isReadyForCreateNewCaseNo = isReadyForCreateNewCaseNo(uniqId, idno, productId);
                        caseCreated = !isReadyForCreateNewCaseNo;
                        if (!hasCaseNoWeb) {
                            if (caseCreated) {
                                return WebResult.GetResultString(99, configDao.ReadConfigValue(LOAN_Err_NEW_CASE_CHK, "系統問題，如有任何需要協助，請洽客服人員"), null);
                            }
                        }
                    } catch (Exception e) {
                        logger.error("未處理的錯誤", e);
                        return WebResult.GetResultString(99, "系統問題，如有任何需要協助，請洽客服人員(NEW_CASE_CHK)", null);
                    }

                    String resume = resumeBreakPoint(existing, idno, birthday, phone, agentNo, uniqType, ipAddress,
                            entry, entryOther, expCaseNo, caseCreated);
                    return resume;
                }
                // C. 初登入且無 CaseData
                // B. 創立新主檔，不接續斷點(或無斷點)
                else if ((isBegining && existing == null) || isForceNewApply) {
                    // 1. 申請流程 -> 登入後 -> 如果客戶有於第一頁輸入員編，則呼叫 [貸款APS系統] API(KGI\QRY_EMP_DEP)
                    // 取得單位代號，並記錄於 QR_PromoCaseUserInput，如沒輸入員編，預設單位代號為9743。
                    String agentDepart = saveQR_PromoCaseUserInput(uniqId, uniqType, agentNo, shortUrl);

                    // 2. 申請流程->登入後(取號後)->呼叫 [決策平台]API(api/KGI;l.kl./km,l./kkjl;/ADD_LOAN_CASE) 取得決策平台編號 寫入
                    // DecisionCaseNo 欄位。
                    String decisionCaseNo = null;
                    try {
                        decisionCaseNo = decisionCaseNo(uniqId, idno);
                    } catch(Exception e) {
                        logger.error("未處理的錯誤", e);
                        return WebResult.GetResultString(99, "系統問題，如有任何需要協助，請洽客服人員(ADD_LOAN_CASE)", null);
                    }
                    if (StringUtils.isBlank(decisionCaseNo)) { // 異常
                        throw new Exception("decisionCaseNo not found.");
                    } else {
                        // 判斷客戶是否為薪轉戶(徵審)
                        checkSalaryAccount = checkSalaryAccount(uniqId, decisionCaseNo); // Y 薪轉戶 N 非薪轉戶
                    }
                    // CrossSell 異業 cif, additionalData
                    String newPoint = createNewPoint(uniqId, uniqType, idno, birthday, phone, agentNo, agentDepart, ipAddress, cif,
                            productId, entry, entryOther, decisionCaseNo, 
                            isNewOne,
                            isOldOne,
                            checkSalaryAccount, 
                            checkCreditcardAccount, 
                            checkCashcardAccount, 
                            checkKgibankAccount, 
                            checkLoanAccount,
                            mobile_tel,
                            stempDataType,
                            expCaseNo,
                            creditLine,
                            preAudit,
                            ch1,
                            ch2,
                            pj,
                            cs,
                            channelId,
                            csCifData);
                    return newPoint;
                } else {
                    return WebResult.GetResultString(99, "系統問題，如有任何需要協助，請洽客服人員", null);
                }

                // --------------------------------------------------------------------
            }

        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }

    } // end applyStart

    /** 貸款重複進件檢核 */
    private boolean isReadyForCreateNewCaseNo(String uniqId, String idno, String productId) throws Exception {
        NewCaseChkRspDto newCaseChkRsp = callNewCaseChkRspDto(uniqId, idno, productId);
        return StringUtils.equals("", newCaseChkRsp.getCHECK_CODE()) && StringUtils.equals("00",newCaseChkRsp.getRESULT_CODE());
    }

    /** 貸款重複進件檢核 */
    private NewCaseChkRspDto callNewCaseChkRspDto(String uniqId, String idno, String productId) throws Exception {
        String prodType = makeProdType(productId);
        NewCaseChkRspDto newCaseChkRsp = kgiService.call_NEW_CASE_CHK(uniqId, idno, prodType);
        return newCaseChkRsp;
    }

    private String makeProdType(String productId) {
        if (isRPL(productId) || isGM(productId)) {
            return "1"; // 1-GM/RPL
        } else { // Default PL
            return "2"; // 2-PLOAN
        }
        
    }

    /**
     * 查詢預核名單資料
     */
    private PreAudit findPreAudit(String pj, boolean isOldOne, String idno) {
        PreAudit preAudit = null; // 新戶，未預核的
        // 是否為預核名單
        // 1.當登入頁參數PJ有值時 (有指定專案)，如果為既有戶，則[不]檢查是否為預核客戶，一律走非預核流程
        // 2.當登入頁參數PJ為空值或沒此參數時，如果為既有戶，才檢查是否為預核客戶，如為預核客戶走預核流程
        if (StringUtils.isBlank(pj) && isOldOne) {
            preAudit = preAuditDao.findPreAudit(idno);
        }
        return preAudit;
    }

    /**
     * 根據是否為預核名單、是否為額度利率體驗通路，判定 productId 為何？
     */
    private String makeProductId(String productId, boolean isPreAudit, Plexp plexp) {
        // --------------------------------------------------------------------
        // 是否為預核名單
        if (isPreAudit) {
            productId = LOAN_ProductId_1_PA;
        }
        // --------------------------------------------------------------------
        // 是否為額度利率體驗通路
        if (plexp != null) {
            String expProductType = StringUtils.isNotBlank(plexp.getExpProductType()) ? plexp.getExpProductType() : "PL";
            if (StringUtils.equalsIgnoreCase("PL", expProductType)) {
                productId = LOAN_ProductId_2_PL;
            } else if (StringUtils.equalsIgnoreCase("RPL", expProductType)) {
                productId = LOAN_ProductId_3_RPL;
            }
        }
        // --------------------------------------------------------------------
        if (StringUtils.isBlank(productId)) {
            productId = LOAN_ProductId_2_PL;
        }
        logger.info("### productid=" + productId);
        // --------------------------------------------------------------------
        return productId;
    }

    /** 取 EntryData 的 Other 中的參數值，例如：[{"qr":"05rp0cfk", "CH":...}] */
    private String findEntryDataOtherParamValueByParamName(String entryOther, String paramName) {
        if (StringUtils.isNotBlank(entryOther) 
            && StringUtils.contains(entryOther, paramName)
            && !StringUtils.equals("[]", entryOther) ) {
            List<Map<String, String>> others = new Gson().fromJson(entryOther, new TypeToken<List<Map<String, String>>>() {}.getType());
            Map<String, String> other = others.stream().filter(x -> x.get(paramName) != null).collect(Collectors.toList()).stream().findFirst().orElseGet(null);
            String paramValue = other.get(paramName);
            return paramValue;
        }
        return null;
    }

    /** 是否有行內留存手機 */
    private boolean has_OTP_TEL(List<GetOtpTelStpRspDtoOtpData> otpDatas) {
        return find_OTP_TEL(otpDatas) != null;
    }

    /** 取得一行內留存手機 */
    private String find_OTP_TEL(List<GetOtpTelStpRspDtoOtpData> otpDatas) {
        if (otpDatas != null && otpDatas.size() > 0) {
            for (GetOtpTelStpRspDtoOtpData otpData : otpDatas) {
                String format = otpData.getFORMAT();
                String otpTel = otpData.getOTP_TEL();
                if (StringUtils.equals("Y", format) && StringUtils.length(otpTel) == 10) {
                    return otpTel;
                }
            }
        }
        return null;
    }

    /**
     * 根據業務員代號記錄申請信貸的業績歸屬
     */
    private String saveQR_PromoCaseUserInput(String uniqId, String uniqType, String agentNo, String shortUrl) {
        String depCodeAct = caseDataService.findAgentDepart(uniqId, agentNo, shortUrl);

        QR_PromoCaseUserInput qr = new QR_PromoCaseUserInput();
        qr.setUniqId(uniqId);
        qr.setUniqType(uniqType); // 01:體驗 02:申請 03:立約 04:專人聯絡 05:信用卡
        qr.setPromoDepart(depCodeAct);
        qr.setPromoMember(agentNo);
        String channelId = caseDataService.channelId(uniqId);
        qr.setChannelId(channelId);
        qr_PromoCaseUserInputDao.Create(qr);
        return depCodeAct;
    } // end saveQR_PromoCaseUserInput

    private String decisionCaseNo(String uniqId, String idno) throws Exception {
        String decisionCaseNo = "";
        // @call 4. ADD_LOAN_CASE
        AddLoanCaseRspDto addLoanCaseRspDto = kgiService.call_ADD_LOAN_CASE(uniqId, idno, "CC", ""); // DATA_TYPE：介接系統(AA.信用卡系統,BB.PLOAN徵審系統,CC.數位申辦平台)，(string)
        if (StringUtils.equals("", addLoanCaseRspDto.getCHECK_CODE())) {
            decisionCaseNo = addLoanCaseRspDto.getCASE_NO();
        }
        return decisionCaseNo;
    }

    private String checkSalaryAccount(String uniqId, String decisionCaseNo) {
        String result = "N"; // N 非薪轉戶
        try {
            // @call 5. CHECK_SALARY_ACCOUNT
            CheckSalaryAccountRspDto checkSalaryAccountRspDto = kgiService.call_CHECK_SALARY_ACCOUNT(uniqId, decisionCaseNo);
            if (StringUtils.equals("", checkSalaryAccountRspDto.getCHECK_CODE())) {
                checkSalaryAccountRspDto.getRESULT();
            }
        } catch(Exception e) {
            logger.error("Fail to CHECK_SALARY_ACCOUNT.", e);
        }
        return result;
    }

    private boolean isCaseComplete(String uniqId, String idno, String caseNoWeb) {
        boolean isCaseComplete = false;
        if (StringUtils.isNotBlank(caseNoWeb)) {
            GetCaseCompleteRspDto complete;
            try {
                complete = kgiService.call_GET_CASE_COMPLETE(uniqId, idno, caseNoWeb);
                caseDataDao.updateImgSysCaseComplete(uniqId, complete.getRTN_CODE(), DateUtil.GetDateFormatString("yyyy-MM-dd HH:mm:ss.SSS"));
                if (StringUtils.equals("", complete.getCHECK_CODE()) && StringUtils.equals("Y", complete.getRTN_CODE())) {
                    isCaseComplete = true;
                }
            } catch (Exception e) {
                logger.error("Fail to GET_CASE_COMPLETE.");
            }
        }
        return isCaseComplete;
    }

    /**
     * 接斷點
     * 
     * @param idno
     * @param birthday
     * @param uniqType
     * @param ipAddress
     * @return WebResult.GetResultString
     */
    private String resumeBreakPoint(CaseData existing, String idno, String birthday, String phone, String agentNo,
            String uniqType, String ipAddress, String entry, String entryOther,
            String expCaseNo, boolean caseCreated) {
        logger.info("### resumeBreakPoint");
        if (existing != null) {
            String uniqId = existing.getCaseNo();
            String page = findBreakPoint(uniqId);
            logger.info("### breakPoint = " + page);

            ApplyStartResp resp = makeResumeApplyStartResp(existing, idno, uniqType, ipAddress, caseCreated, page);

            existing.setIp_address(ipAddress);
            caseDataDao.Update(existing);

            // updatePlexpCaseNoWeb(expCaseNo, uniqId);

            return WebResult.GetResultString(0, "成功", resp);
        }
        return null;
    } // end resumeBreakPoint

    private ApplyStartResp makeResumeApplyStartResp(CaseData existing, String idno, String uniqType, String ipAddress, boolean caseCreated, String page) {
        String uniqId = existing.getCaseNo();
        ApplyStartResp resp = new ApplyStartResp();
        LoginResp loginResp = new LoginResp();
        loginResp.setUniqId(uniqId);
        loginResp.setUniqType(uniqType);
        loginResp.setIdno(idno);
        loginResp.setIpAddress(ipAddress);
        resp.setLoginResp(loginResp);
        CaseDataResp caseDataResp = new CaseDataResp();
        BeanUtils.copyProperties(existing, caseDataResp);
        resp.setCaseDataResp(caseDataResp);
        if (StringUtils.isBlank(page)) {
            resp.setBreakPoint("/apply/verify"); // 預設 OTP1
        } else {
            resp.setBreakPoint(page);
        }
        resp.setCaseCreated(caseCreated);
        logger.info("### resp = " + new Gson().toJson(resp));
        return resp;
    }

    /**
     * 創建主檔 CaseData
     * 
     * @param uniqId
     * @param uniqType
     * @param idno
     * @param birthday
     * @param phone
     * @param ipAddress
     * @return WebResult.GetResultString
     */
    private String createNewPoint(String uniqId, String uniqType, String idno, String birthday, String phone,
            String agentNo, String agentDepart, String ipAddress, GetCifInfoRspDto cif, String productId, String entry, String entryOther,
            String decisionCaseNo, 
            boolean isNewOne,
            boolean isOldOne,
            String checkSalaryAccount, 
            String checkCreditcardAccount, 
            String checkCashcardAccount, 
            String checkKgibankAccount, 
            String checkLoanAccount,
            String mobile_tel,
            String stempDataType,
            String expCaseNo,
            String creditLine,
            PreAudit preAudit,
            boolean ch1,
            boolean ch2,
            String pj,
            String cs,
            String channelId,
            CSCommonCifData csCifData) {
        //
        ApplyStartResp resp = new ApplyStartResp();
        LoginResp loginResp = new LoginResp();
        loginResp.setUniqId(uniqId);
        loginResp.setUniqType(uniqType);
        loginResp.setIdno(idno);
        loginResp.setIpAddress(ipAddress);
        resp.setLoginResp(loginResp);

        logger.info("### 申請流程 uniqId = " + uniqId);

        // 寫入主檔CaseData資料
        CaseData caseData = new CaseData();
        caseData.setCaseNo(uniqId);        
        caseData.setIsPreAudit(IsPreAudit_2); // 2 非預核
        caseData.setIdno(idno);
        caseData.setBirthday(birthday);
        caseData.setPhone(phone);
        caseData.setMobile_tel(mobile_tel);
        caseData.setAgentNo(agentNo);
        caseData.setAgentDepart(agentDepart);
        caseData.setProductId(productId);
        caseData.setCheckSalaryAccount(checkSalaryAccount);
        caseData.setCheckCreditcardAccount(checkCreditcardAccount);
        caseData.setCheckCashcardAccount(checkCashcardAccount);
        caseData.setCheckKgibankAccount(checkKgibankAccount);
        caseData.setCheckLoanAccount(checkLoanAccount);
        caseData.setIp_address(ipAddress);

        // 是否為預核名單
        if (preAudit != null) {
            caseData.setIsPreAudit(IsPreAudit_1); // 1 預核
            caseData.setPrj_code(preAudit.getProjectCode());
            try {
                auditDataDao.createAuditData(uniqId, uniqType, preAudit.getProjectCode(), preAudit.getCreditLine(), preAudit.getInterestRate(), null, null, null);
            } catch(Exception e) {
                logger.error("Fail to createAuditData.", e);
            }
        }

        // RPL 額度利率體驗預設貸款金額
        caseData.setP_apy_amount(creditLine);

        // 既有戶
        if (isOldOne) {
            caseData.setUserType(USER_TYPE_3_OLD_ONE); // 3:純貸款戶
        } 
        // 新戶
        else if (isNewOne) {
            caseData.setUserType(USER_TYPE_0_NEW_ONE); // 0: 新戶
        }

        caseData.setDecisionCaseNo(decisionCaseNo);
        // caseData.setCheckSalaryAccount(checkSalaryAccount);  //重複459行

        if (cif != null) {
            caseData.setCustomer_name(   cif.getNAME());
            caseData.setEmail_address(   cif.getEML_ADDR());
            caseData.setHouse_city_code( cif.getADR_ZIP());
            caseData.setResAddrZipCode(  cif.getADR_ZIP_1());
            caseData.setHouse_address(   makeAddr(cif.getADR(), cif.getADR_ZIP()));
            caseData.setResAddr(         makeAddr(cif.getADR_1(), cif.getADR_ZIP_1()));
            caseData.setHouse_tel_area(  cif.getTEL_NO_AREACODE());
            caseData.setResTelArea(      cif.getTEL_NO_AREACODE_1());
            caseData.setHouse_tel(       cif.getTEL_NO());
            // TEL_NO_EXT
            caseData.setResTel(          cif.getTEL_NO_1());
            // TEL_NO_EXT_1
            caseData.setEducation(       cif.getEDU());

            if (StringUtils.equals(CIF_MARRIAGE_YES, cif.getMARRIAGE())) { // M 已婚
                caseData.setMarriage(   CASEDATA_marriage_married); // 2 已婚
            } else if (StringUtils.equals(CIF_MARRIAGE_NO, cif.getMARRIAGE())) { // S 未婚
                caseData.setMarriage(   CASEDATA_marriage_single); // 1 未婚
            }
            
            caseData.setCorp_name(       cif.getCOMPANY_NAME());
            caseData.setCorp_city_code(  cif.getCOMPANY_ADDR_ZIP());
            caseData.setCorp_address(    makeAddr(cif.getCOMPANY_ADDR(), cif.getCOMPANY_ADDR_ZIP()));
            caseData.setCorp_tel_area(   cif.getCOMPANY_TEL_AREACODE());
            caseData.setCorp_tel(        cif.getCOMPANY_TEL());
            caseData.setCorp_tel_exten(  cif.getCOMPANY_TEL_EXT());
            caseData.setTitle(           cif.getOCCUPATION());
            caseData.setOn_board_date(   cif.getON_BOARD_DATE());
            caseData.setYearly_income(   cif.getINCOME());
            caseData.setEstate_type(     cif.getHOUSE_OWNER());
        }
        
        //  CrossSell 異業 caseDataResp 設定 additionalData
        if(StringUtils.isNotEmpty(cs) && csCifData != null){
            // 露天一旦跨售有cif資料，一律蓋過既有資料
            if("RT".equals(channelId)){
                caseData.setCustomer_name(   csCifData.getChtName());
                caseData.setEmail_address(   csCifData.getEmail());
                caseData.setHouse_city_code( csCifData.getCommAddrZipCode());
                caseData.setResAddrZipCode(  csCifData.getHomeAddrZipCode());
                caseData.setHouse_address(   this.csAddrSplitByArea(csCifData.getCommAddr()));
                caseData.setResAddr(         this.csAddrSplitByArea(csCifData.getHomeAddr()));
                caseData.setHouse_tel_area(  csCifData.getHomeTelArea());
                caseData.setResTelArea(      csCifData.getResTelArea());
                caseData.setHouse_tel(       csCifData.getHomeTel());
                caseData.setResTel(          csCifData.getResTel());
                caseData.setEducation(       csCifData.getEducation());
                caseData.setMarriage(        csCifData.getMarriage());
                caseData.setCorp_name(       csCifData.getCorpName());
                caseData.setCorp_type(       csCifData.getOccupation());
                caseData.setCorp_city_code(  csCifData.getCorpAddrZipCode());
                caseData.setCorp_address(    this.csAddrSplitByArea(csCifData.getCorpAddr()));
                caseData.setCorp_tel_area(   csCifData.getCorpTelArea());
                caseData.setCorp_tel(        csCifData.getCorpTel());
                caseData.setCorp_tel_exten(  csCifData.getCorpTelExten());
                caseData.setTitle(           csCifData.getJobTitle());
                caseData.setOn_board_date(   csCifData.getOnBoardDate());
                caseData.setYearly_income(   csCifData.getYearlyIncome());
                caseData.setEstate_type(     csCifData.getEstateType());
                if(csCifData.getAdditionalData() != null){
                    Object creditLimit = csCifData.getAdditionalData().get("creditLimit");
                    if(creditLimit != null){
                        caseData.setCreditLimit(creditLimit.toString());
                    }
                }
            } else{
                // 除露天以外跨售資料，只有在既有資料沒資料時才押上
                if(StringUtils.isEmpty(caseData.getCustomer_name())){
                    caseData.setCustomer_name(   csCifData.getChtName());
                }
                if(StringUtils.isEmpty(caseData.getEmail_address())){
                    caseData.setEmail_address(   csCifData.getEmail());
                }
                if(StringUtils.isEmpty(caseData.getHouse_city_code())){
                    caseData.setHouse_city_code( csCifData.getCommAddrZipCode());
                    caseData.setHouse_address(   this.csAddrSplitByArea(csCifData.getCommAddr()));
                }
                if(StringUtils.isEmpty(caseData.getResAddrZipCode())){
                    caseData.setResAddrZipCode(  csCifData.getHomeAddrZipCode());
                    caseData.setResAddr(         this.csAddrSplitByArea(csCifData.getHomeAddr()));
                }
                if(StringUtils.isEmpty(caseData.getHouse_tel_area())){
                    caseData.setHouse_tel_area(  csCifData.getHomeTelArea());
                    caseData.setHouse_tel(       csCifData.getHomeTel());
                }
                if(StringUtils.isEmpty(caseData.getResTelArea())){
                    caseData.setResTelArea(      csCifData.getResTelArea());
                    caseData.setResTel(          csCifData.getResTel());
                }
                if(StringUtils.isEmpty(caseData.getEducation())){
                    caseData.setEducation(       csCifData.getEducation());
                }
                if(StringUtils.isEmpty(caseData.getMarriage())){
                    caseData.setMarriage(        csCifData.getMarriage());
                }
                if(StringUtils.isEmpty(caseData.getCorp_name())){
                    caseData.setCorp_name(       csCifData.getCorpName());
                }
                if(StringUtils.isEmpty(caseData.getCorp_type())){
                    caseData.setCorp_type(       csCifData.getOccupation());
                }
                if(StringUtils.isEmpty(caseData.getCorp_city_code())){
                    caseData.setCorp_city_code(  csCifData.getCorpAddrZipCode());
                    caseData.setCorp_address(    this.csAddrSplitByArea(csCifData.getCorpAddr()));
                }
                if(StringUtils.isEmpty(caseData.getCorp_tel_area())){
                    caseData.setCorp_tel_area(   csCifData.getCorpTelArea());
                    caseData.setCorp_tel(        csCifData.getCorpTel());
                    caseData.setCorp_tel_exten(  csCifData.getCorpTelExten());
                }
                if(StringUtils.isEmpty(caseData.getTitle())){
                    caseData.setTitle(           csCifData.getJobTitle());
                }
                if(StringUtils.isEmpty(caseData.getOn_board_date())){
                    caseData.setOn_board_date(   csCifData.getOnBoardDate());
                }
                if(StringUtils.isEmpty(caseData.getYearly_income())){
                    caseData.setYearly_income(   csCifData.getYearlyIncome());
                }
                if(StringUtils.isEmpty(caseData.getEstate_type())){
                    caseData.setEstate_type(     csCifData.getEstateType());
                }
                if(StringUtils.isEmpty(caseData.getP_apy_amount()) 
                        && csCifData.getAdditionalData() != null){
                    Object creditLimit = csCifData.getAdditionalData().get("creditLimit");
                    if(creditLimit != null){
                        caseData.setP_apy_amount(creditLimit.toString());
                    }
                }
            }
        }

        // logger.info("### createNewPoint - caseData: " + new Gson().toJson(caseData));
		
		//2021-05-26: 改到CREATE NEW CASE時更新AddNewCaseTime欄位 
        //caseData.setAddNewCaseTime(DateUtil.nowDateString()); // 1. 發動起案時間(產生 decisionCaseNo 的時間) 
		
        caseData.setPJ(pj);

        String gender = "0";
        String genderNumber = String.valueOf(caseData.getIdno().charAt(1));
        if (caseData.getIdno() != null) {
            if ("1".equals(genderNumber) || "8".equals(genderNumber)) {
                gender = "1";
            } else if ("2".equals(genderNumber) || "9".equals(genderNumber)) {
                gender = "2";
            }
        }
        caseData.setGender(gender);

        // @setup caseData.SubStatus 00 000
        caseDataDao.createNewPoint(caseData);

        // 更新 同意使用與貴行往來之資料申辦貸款
        if (ch2) {
            caseDataDao.updateLoan_personal_data(uniqId, "Y", DateUtil.nowDateString());
        } else {
            caseDataDao.updateLoan_personal_data(uniqId, "N", DateUtil.nowDateString());
        }

        plexpService.updatePlexpCaseNoWeb(expCaseNo, uniqId);

        CaseDataResp caseDataResp = new CaseDataResp();
        BeanUtils.copyProperties(caseData, caseDataResp);
        caseDataResp.setStempDataType(stempDataType);
        resp.setCaseDataResp(caseDataResp);
        return WebResult.GetResultString(0, "成功", resp);
    } // end createNewPoint

    /**
     * 將'臺'轉為'台'，例如："臺北市中山區"轉為"台北市中山區"
     * 將縣市區加分隔符號';'，例如："台北市中山區南京西路1號"轉為"台北市中山區;南京西路1號"
     */
    private String makeAddr(String adr, String zipcode) {
        if (StringUtils.length(adr) >= 4) {
            if (StringUtils.startsWith(adr, "臺")) {
                adr = StringUtils.replace(adr, "臺", "台");
            }
            List<String> list = dropdownDao.findDataKeysByName(DROPDOWNDATA_NAME_ZIPCODEMAPPING);
            if (StringUtils.startsWithAny(adr, list.stream().toArray(String[]::new))) {
                for (String dataKey : list) {
                    if (StringUtils.startsWith(adr, dataKey)) {
                        adr = new StringBuilder(adr).insert(dataKey.length(), ';').toString();
                        break;
                    }
                }
            } else {
                String dataKey = dropdownDao.findDataKeysByNameAndDataName(DROPDOWNDATA_NAME_ZIPCODEMAPPING, zipcode);
                if (StringUtils.isNotBlank(dataKey)) {
                    adr = new StringBuilder(dataKey).append(';').append(adr).toString();
                }
            }
            
        }
        return adr;
    }

    private String findBreakPoint(String uniqId) {
        String page = null;
        BrowseLog browseLog = browseLogDao.findLatestPageByUniqId(uniqId);
        if (browseLog != null) {
            page = browseLog.getPage();
        }
        return page;
    }

    public String applyInfo(String uniqId, String reqJson) {
        try {
            ApplyInfoView infoView = new Gson().fromJson(reqJson, ApplyInfoView.class);
            CaseData caseData = caseDataDao.ReadByCaseNo(uniqId);
            copyProperties(infoView, caseData);
            caseDataDao.Update(caseData);
            return WebResult.GetResultString(0, "成功", null);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end applyInfo

    public String applyVerify(String uniqId) {
        try {
            // @setup caseData.SubStatus 00 010
			//修正: 修正當接續斷點時OTP過後，狀態被回復到 00.010 問題
			if (StringUtils.equals("00",caseDataDao.findStatusByCaseNo(uniqId))){
				caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_00_SubStatus_010);
			}
            return WebResult.GetResultString(0, "成功", null);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    public String applyLoan(String uniqId) {
        try {
            // @setup caseData.SubStatus 01 000
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_000);
            return WebResult.GetResultString(0, "成功", null);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    /**
     * 呼叫 GET_AML_DATA, CHECK_NO_FINPF, CHECK_HAVE_FINPF2, CHECK_HAVE_FINPF3
     * 1. 起案前 -> 先打 AML電文。
     * 2. 起案前 -> 用DecisionCaseNo ，判斷是否為免財力證明客戶 API (api/KGI/CHECK_NO_FINPF)。
     * 3. 起案前 -> 用DecisionCaseNo ，背景呼叫 是否有舊有ID API (api/KGI/CHECK_HAVE_FINPF, IMG_TYPE=2)，並更新 HasOldIDImg & OldIDImgNoList。
     * 4. 起案前 -> 用DecisionCaseNo ，背景呼叫 是否有舊有財力 API (api/KGI/CHECK_HAVE_FINPF, IMG_TYPE=3)，並更新 HasOldFinaImg & OldFinaImgNoList。
     */
    public ApplyWorkResp prepareCreateNewCaseNo(CaseData caseData) {

        String uniqId = caseData.getCaseNo();
        String idno = caseData.getIdno();
        String channelId = caseDataService.channelId(uniqId);
        String departId = caseDataService.departId(caseData.getAgentNo(), caseData.getAgentDepart());

        logger.info("### channelId=" + channelId);
        logger.info("### departId=" + departId);
        QR_ChannelDepartList qr_ChannelDepartList = channelDepartService.findQR_ChannelDepartList(channelId, departId);
		
		//查AML單位要跟起案單位一致
        String assignDepart = (StringUtils.isNotBlank(qr_ChannelDepartList.getAssignDepart())
            ? qr_ChannelDepartList.getAssignDepart() : "9743");

		
        ApplyWorkResp resp = new ApplyWorkResp();
        resp.setHasOldIdImg("N");
        resp.setHasOldFinaImg("N");
        try {
            // AML 驗證洗錢防治
            String depCodeAct = KB_9743_DEPART_ID; // 2020-10-14 如沒輸入員編，預設單位代號為9743 demand by Ben
			
            //QR_PromoCaseUserInput qr = qr_PromoCaseUserInputDao.findByUniqId(uniqId);
            //if (qr != null) {
            //    depCodeAct = qr.getPromoDepart();
            //}
			depCodeAct = assignDepart; //查AML單位要跟起案單位一致
			
            // amlService
            // @call 10. GET_AML_DATA
            Map<String, String> aml = amlService.processAml(uniqId, idno, caseData.getCustomer_name(), depCodeAct);
            String amlCheckCode = null;	//可能是 空值   /99
            String amlResult = null;	//可能是 Y/N/V/9/99
            String amlTime = null;
            if (aml != null) {
                amlCheckCode = aml.get("CHECK_CODE");	//可能是 空值   /99
                amlResult = aml.get("AML_RESULT");		//可能是 Y/N/V/9/99
                amlTime = aml.get("TIME");  // yyyy-MM-dd hh:mm:ss.SSS
            }
            if (StringUtils.equals("", amlCheckCode)) {
                // @setup caseData.SubStatus 01 030
                caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_030);
                // N 身分為正常 Y 身分為黑名單 V 身分為疑似有異常
                if (StringUtils.equals("N", amlResult)) { // N 身分為正常
                    // @HasQryAml
                    caseData.setHasQryAml("01");
                } else if (StringUtils.equals("Y", amlResult)) { // Y 身分為黑名單
                    
                } else if (StringUtils.equals("V", amlResult)) { // V 身分為疑似有異常
                    
                } else { // 通常是 9/99

                }
            }else{
				//must be 99
				
			}
            caseDataDao.updateAml(uniqId, amlResult, amlTime);

            // 判斷是否為免財力證明客戶
            // @call 11. CHECK_NO_FINPF
            checkNoFinph(uniqId, amlResult, caseData);
			
			//假如登入時有勾選同意取用舊ID、財力，才取
            if (StringUtils.equals("Y", caseData.getLoan_personal_data_Result())) {
                // 判斷是否有 2.身份證
                // @call 12. CHECK_HAVE_FINPF2
                checkHaveFinpf2(uniqId, caseData);
                // 判斷是否有 3.財力證明
                // @call 13. CHECK_HAVE_FINPF3
                checkHaveFinpf3(uniqId, caseData);
            }

            // 新戶
            // else if (isNewOne) {
            // }
            resp.setHasOldIdImg(StringUtils.equals("01", caseData.getHasOldIdImg())?"Y":"N");
            resp.setHasOldFinaImg(StringUtils.equals("01", caseData.getHasOldFinaImg())?"Y":"N");
            return resp;
        } catch (ErrorResultException e) {
            e.printStackTrace();
            return resp;
        } catch (Exception e) {
            e.printStackTrace();
            return resp;
        }
    } // end prepareCreateNewCaseNo

    public String applyConfirm(String uniqId, String idno, String reqJson) {
        try {
            ApplyInfoView infoView = new Gson().fromJson(reqJson, ApplyInfoView.class);
            String birthday = infoView.getBirthday(); // yyyyMMdd
            birthday = StringUtils.replace(birthday, "/", ""); // 1999/01/01 -> 19990101
            birthday = StringUtils.replace(birthday, "-", ""); // 1999-01-01 -> 19990101
            infoView.setBirthday(birthday);
            CaseData caseData = caseDataDao.ReadByCaseNo(uniqId);
            copyProperties(infoView, caseData);
            caseData.setSendCustInfoTime(DateUtil.nowDateString()); // 2. 送詳細資料時間 
            caseDataDao.Update(caseData);

            if (infoView.isCh1()) {
                CaseProperties cp = new CaseProperties();
                cp.setUniqId(uniqId);
                cp.setPropertyName("JCICQuery.Agree.Time");
                cp.setPropertyDesc("客戶聯徵查詢同意");
                cp.setPropertyValue("Y");
                casePropertiesDao.CreateOrUpdate(cp);
            } else {
                CaseProperties cp = new CaseProperties();
                cp.setUniqId(uniqId);
                cp.setPropertyName("JCICQuery.Agree.Time");
                cp.setPropertyDesc("客戶聯徵查詢同意");
                cp.setPropertyValue("N");
                casePropertiesDao.CreateOrUpdate(cp);
            }

            return WebResult.GetResultString(0, "成功", null); // FIXME: Charles: check angular side
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end applyConfirm

    // 確認頁完成直接OTP2 不驗 pcode2566
    public String applyConfirmToOtp2(String uniqId, String idno, String ipAddress) {
        ApplyConfirmToOtp2Resp resp = new ApplyConfirmToOtp2Resp();
        try {
            CaseData caseData = caseDataDao.ReadByCaseNo(uniqId);
            boolean isPreAudit = isPreAudit(caseData.getIsPreAudit()); // 預核名單
            boolean isOldOne = isOldOne(caseData.getUserType());

            if (isPreAudit || isOldOne) {
                // 不需要驗身故無條件寫入驗身資料(CreditVerify)
                bankService.passOldOne(uniqId, UNIQ_TYPE_02);
                resp.setToOtp2(true);
            } else {
                resp.setToOtp2(false);
            }

            return WebResult.GetResultString(0, "成功", resp);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    /** 已驗身(含他行帳戶驗身、OTP 2) */
    public String applyIdentified(String uniqId, String idno, String ipAddress) {
        try {

            // @setup caseData.SubStatus 01 010
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_010);

            // 新增案件：呼叫 NEW_CASE_CHK, GET_AML_DATA, CHECK_NO_FINPF, CHECK_HAVE_FINPF2, CHECK_HAVE_FINPF3, CREATE_NEW_CASE
            ApplyWorkResp applyWorkResp = applyEstablish(uniqId, idno, ipAddress);

            // @call 15. previewCasePDFHtml
            pdfService.pdfGeneratorActViewAndSendEmail(uniqId);

            // 是否正常產製 CasePDF 貸款申請書 HTML
            boolean isCasePdfOk = caseDocumentDao.isExist(uniqId, DOCUMENT_TYPE_LOAN_APPLYPDF);
            if (isCasePdfOk) {
                // @setup caseData.SubStatus 01 100
                caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_100, CASE_STATUS_01_JobStatus_010);
            } else {
                // @setup caseData.SubStatus 01 101
                caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_101);
            }

            return WebResult.GetResultString(0, "成功", applyWorkResp);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end applyIdentified

    /** 是否需要進入身份證上傳頁 */
    public String mustUploadIdcard(String uniqId) {
        try {
            boolean mustUploadIdcard = caseDataService.mustUploadIdcard(uniqId);
            boolean mustUploadFinpf = caseDataService.mustUploadFinpf(uniqId);
            MustUploadIdcardResp resp = new MustUploadIdcardResp(mustUploadIdcard, mustUploadFinpf);
            return WebResult.GetResultString(0, "成功", resp);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end mustUploadIdcard

    /** 是否需要進入財力證明上傳頁 */
    public String mustUploadFinpf(String uniqId) {
        try {
            boolean mustUploadFinpf = caseDataService.mustUploadFinpf(uniqId);
            MustUploadFinpfResp resp = new MustUploadFinpfResp(mustUploadFinpf);
            return WebResult.GetResultString(0, "成功", resp);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end mustUploadFinpf

    /** 新增案件：呼叫 NEW_CASE_CHK, GET_AML_DATA, CHECK_NO_FINPF, CHECK_HAVE_FINPF2, CHECK_HAVE_FINPF3, CREATE_NEW_CASE */
    public ApplyWorkResp applyEstablish(String uniqId, String idno, String ipAddress) throws Exception {

        CaseData caseData = caseDataDao.ReadByCaseNo(uniqId);
        ApplyWorkResp applyWorkResp;
        // 白名單
        if (whiteListDao.isBypassLoan(idno)) { // FIXME: use TABLE WhiteListData instead.
            // bypass NEW_CASE_CHK
            // bypass GET_AML_DATA
            // bypass CHECK_NO_FINPF
            // bypass CHECK_HAVE_FINPF2
            // bypass CHECK_HAVE_FINPF3
            // bypass CREATE_NEW_CASE
            String createNewCaseCaseNo = "9999";
            String hasOldIdImg = "00";
            String hasOldFinaImg = "00";
            boolean idCard = false;
            applyWorkResp = new ApplyWorkResp();
            applyWorkResp.setHasOldIdImg(hasOldIdImg);
            applyWorkResp.setHasOldFinaImg(hasOldFinaImg);
            caseData.setCaseNoWeb(createNewCaseCaseNo);
            caseData.setHasOldIdImg(hasOldIdImg);
            caseData.setHasOldFinaImg(hasOldFinaImg);

        } else {
            
            if (StringUtils.isNotBlank(caseData.getCaseNoWeb())) { // 避免已成立案件 CREATE_NEW_CASE 有 CaseNoWeb 卻未完成
                applyWorkResp = new ApplyWorkResp();
                applyWorkResp.setHasOldIdImg(StringUtils.equals("01", caseData.getHasOldIdImg())?"Y":"N");
                applyWorkResp.setHasOldFinaImg(StringUtils.equals("01", caseData.getHasOldFinaImg())?"Y":"N");
            } else {
                // - 若否(無案件尚未結案)，則再呼叫[**理債平台**] 新增案件(STP) (KGI/CREATE_NEW_CASE)，取得理債平台編號 寫入 CaseNoWeb 欄位。
                boolean isReadyForCreateNewCaseNo = isReadyForCreateNewCaseNo(uniqId, caseData.getIdno(), caseData.getProductId());
                if (isReadyForCreateNewCaseNo) {

                    // @setup caseData.SubStatus 01 020
                    caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_020);

                    // 呼叫 GET_AML_DATA, CHECK_NO_FINPF, CHECK_HAVE_FINPF2, CHECK_HAVE_FINPF3
                    // 更新 CaseData及ApplyWorkResp 的 hasOldIdImg, hasOldFinaImg 欄位值
                    applyWorkResp = prepareCreateNewCaseNo(caseData);

                    // 2.23.新增案件(STP)
                    // @call 14. CREATE_NEW_CASE
					
					//step-1:check AddNewCaseTime, if null then write Time, if not null then skip createNewCase
					//todo:
					String  nowCaseAddNewCaseTime = caseDataDao.findAddNewCaseTimeByCaseNo(uniqId);
					
					if (nowCaseAddNewCaseTime!=null){
						
						logger.info("@@@@ " + uniqId + " AddNewCaseTime 有值( " + nowCaseAddNewCaseTime + " )，略過起案.");						
						
					}else{
						
						logger.info("@@@@ " + uniqId + " AddNewCaseTime 為空，可以起案....");
						String sNowDate = DateUtil.nowDateString();
						caseData.setAddNewCaseTime(sNowDate);
						caseDataDao.updateAddNewCaseTime(uniqId,sNowDate);	//update Table first
						logger.info("@@@@ " + uniqId + " 馬上更新 AddNewCaseTime = "  + sNowDate);
						//起案
						CreateNewCaseRspDto createNewCase = this.createNewCase(caseData);
						//這裡可能會很久
						String createNewCaseCaseNo = createNewCase.getCASE_NO();
						
						// 0331-001.發CREATE_NEW_CASE時，如果遇到CHECK_CODE=99，一樣更新CASENOWEB後，繼續下一步，不要報錯
						boolean isOkCreateNewCase = 
							(StringUtils.equals("", createNewCase.getCHECK_CODE()) || StringUtils.equals("99", createNewCase.getCHECK_CODE()) )
							&& StringUtils.isNotBlank(createNewCaseCaseNo);
							
						if (isOkCreateNewCase) {
							// @setup caseData.SubStatus 01 070
							caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_070);
							
							//如果CASENOWEB是空白才更新
							if (StringUtils.equals("",caseDataDao.findCaseNoWebByCaseNo(uniqId)) ||  caseDataDao.findCaseNoWebByCaseNo(uniqId)==null ){
								caseData.setCaseNoWeb(createNewCaseCaseNo);
							}
							
							
						} else {
							throw new Exception("Fail to CREATE_NEW_CASE.");
						}
						
					}

					
                } else {
                    // - 若是(有案件尚未結案)，則無法下一步。
                    logger.error("有案件尚未結案");
                    throw new Exception("有案件尚未結案");
                }

                // checkCaseMoiData > checkApimAndMore > [checkZ07, checkDebtInfo]
                boolean isOkApim = checkCaseMoiData(caseData);
                // @CanUseOldIdImg
                if (!isOkApim) {
                    caseData.setCanUseOldIdImg("00");
                } else {
                    caseData.setCanUseOldIdImg("01");
                }
                // applyWorkResp.setHasOldIdImg() - done by prepareCreateNewCaseNo()
                // applyWorkResp.setHasOldFinaImg() - done by prepareCreateNewCaseNo()
            } 
        } // end else

        caseData.setIp_address(ipAddress);
        caseDataDao.Update(caseData);

        // @setup caseData.SubStatus 01 090
        caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_090);

        return applyWorkResp;
    } // end applyEstablish
	
	//上傳ID畫面最後submit會呼叫這裡
    public String applyUploadIdcard(String uniqId, String idno, String reqJson) {
        try {
            ApplyUploadIdcardView view = new Gson().fromJson(reqJson, ApplyUploadIdcardView.class);
			System.out.println(">>>>>> view.getIdcardLocation() = " + view.getIdcardLocation());
            CaseData caseData = caseDataDao.ReadByCaseNo(uniqId);
            // 身份證正面
            // caseData.setCustomer_name(name);
            caseData.setIdCardDate(view.getIdcardDate());
            caseData.setIdCardLocation(view.getIdcardLocation());
            caseData.setIdCardCRecord(view.getIdcardCRecord());
            // 身份證反面
            caseData.setResAddrZipCode(view.getTownship());
            if (StringUtils.isNotBlank(view.getCountiesName()) && StringUtils.isNotBlank(view.getTownshipName()) && StringUtils.isNotBlank(view.getAddress())) {
                String resAddr = 
                view.getCountiesName() + view.getTownshipName() + ";" 
                + view.getAddress();
                caseData.setResAddr(resAddr);
            }

            // @HasUploadId
            caseData.setHasUploadId("01");
            logger.info(new Gson().toJson(caseData));
            caseDataDao.Update(caseData);

            // @setup caseData.SubStatus 01 400
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_400);

            // Update Status To Wait Upload by Scheduled Job
            userPhotoService.updateStatusToWaitUpload(uniqId, USERPHOTO_ProdType_2, USERPHOTO_PTYPE_1, USERPHOTO_SUBTYPE_IDCARDFRONT);
            userPhotoService.updateStatusToWaitUpload(uniqId, USERPHOTO_ProdType_2, USERPHOTO_PTYPE_1, USERPHOTO_SUBTYPE_IDCARDBANK);
			System.out.println(">>>>>> caseData.getIdCardLocation() = " + caseData.getIdCardLocation());
            String idCardLocation = caseData.getIdCardLocation();
            String applyYyymmdd = caseData.getIdCardDate();
            String applyCode = caseData.getIdCardCRecord();
            String createNewCaseCaseNo = caseData.getCaseNoWeb();
            String decisionCaseNo = caseData.getDecisionCaseNo();
            String productId = caseData.getProductId();
            String agentDepart = caseData.getAgentDepart();
            String agentNo = caseData.getAgentNo();
            Map<String, String> rspmap = checkApimAndMore(caseData, idCardLocation, applyYyymmdd, applyCode, createNewCaseCaseNo, decisionCaseNo, productId, agentDepart, agentNo);

            caseDataDao.updateJobStatusOnly(uniqId, CASE_STATUS_01_JobStatus_020.getJobStatus());

            // // @call 15. previewCasePDFHtml
            // pdfService.pdfGeneratorActViewAndSendEmail(uniqId);

            // // 是否正常產製 CasePDF 貸款申請書 HTML
            // boolean isCasePdfOk = caseDocumentDao.isExist(uniqId, DOCUMENT_TYPE_LOAN_APPLYPDF);
            // if (isCasePdfOk) {
            //     // @setup caseData.SubStatus 01 100
            //     caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_100, CASE_STATUS_01_JobStatus_010);
            // } else {
            //     // @setup caseData.SubStatus 01 101
            //     caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_101);
            // }

            return WebResult.GetResultString(0, "成功", rspmap);
        } catch (ErrorResultException e) {
			logger.error("未處理的錯誤A", e);
			System.out.println("#### ID 上傳頁面錯誤-A");
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤B", e);
			System.out.println("#### ID 上傳頁面錯誤-B");
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end applyUploadIdcard

    /**
     * 舊戶(曾經成功發查戶役政)，用idno去查詢CaseMoiData最新一筆<br />
     * 若舊戶(CaseMoiData)發查戶役政成功, 則略過上傳身份證正反面頁及財力證明頁，進行發查聯徵, 呼叫Z07, 呼叫外部負債資料查詢, 產生KYC表。<br />
     * 若舊戶(CaseMoiData)發查戶役政失敗, 則視為新戶。
     */
    public boolean checkCaseMoiData(CaseData caseData) throws Exception {

        String uniqId = caseData.getCaseNo();
        String idno = caseData.getIdno();
        String hasOldIdImg = caseData.getHasOldIdImg();
        String decisionCaseNo = caseData.getDecisionCaseNo();
        String productId = caseData.getProductId();
        String createNewCaseCaseNo = caseData.getCaseNoWeb();
        String agentDepart = caseData.getAgentDepart();
        String agentNo = caseData.getAgentNo();

        boolean isOkApim = false; // 是否戶役政驗證通過
        // 9. 填寫資料完成->做以下兩點判斷來決定是否出現身份證上傳頁面。
        // 11. 舊戶(曾經成功發查戶役政)，用idno去查詢**CaseMoiData**最新一筆，

        // 查詢行內戶役政檢核結果資料
        QryIdcardInfoRspDto qryIdcard = kgiService.call_QRY_IDCARD_INFO(uniqId, idno);
        boolean hasOldIdImgData = StringUtils.equals("", qryIdcard.getCHECK_CODE());
        if (hasOldIdImgData && StringUtils.equals("01", hasOldIdImg)) {
            // - 若HasOldIDImg=01且**CaseMoiData**有資料，拿CaseMoiData舊資料發查戶役政，
            // - 若發查成功，則畫面跳過身份證上傳頁，並寫入CaseData以下欄位，
            // - IdCardDate(身份證換發日期)
            // - IdCardLocation(身份證換發地點)
            // - IdCardCRecord(身份證換發紀錄 0:初領 1:補領 2:換發)
            // - 及身份證背面相關資料。

            // String uniqId = caseData.getCaseNo();
            // String idno = casemoidata.getIdNo(); // or caseData.getIdno();
            String idCardLocation = dropdownService.findDataName_idlocation_new(qryIdcard.getIDCARD_CITY());
            String applyYyymmdd = qryIdcard.getIDCARD_DATE();
            String applyCode = qryIdcard.getIDCARD_TYPE();
            // String createNewCaseCaseNo = caseData.getCaseNoWeb();
            Map<String, String> rspmap = checkApimAndMore(caseData, idCardLocation, applyYyymmdd, applyCode, createNewCaseCaseNo, decisionCaseNo, productId, agentDepart, agentNo);

            String message = rspmap.get("message");
            isOkApim = StringUtils.isBlank(message);
            
        } else {
            // 10. 新戶，用idno去查詢**CaseMoiData**查無資料，則直接進入上傳身份證頁，
            // - 若發查戶役政失敗，僅寫入ContractApiLog停在原頁無法下一步。
            // - 若發查失敗，則直接進入上傳身份證頁。
            // - 若無資料，則直接進入上傳身份證頁。
            isOkApim = false;
        }
        // // 無舊ID - 若無資料，則直接進入上傳身份證頁。
        // caseData.setHasOldIdImg("00");
        // IdCard = "{\"IdCard\": false}";
        return isOkApim;
    } // end checkCaseMoiData

    /**
     * 發查戶役政, 發查聯徵, 呼叫Z07, 呼叫外部負債資料查詢, 產生KYC表
     * 
     * @param uniqId
     * @param idno
     * @param idCardLocation 身份證換發地點(中文名稱，例如:北縣)
     * @param applyYyymmdd 身份證換發日期
     * @param applyCode 身份證換發紀錄 0:初領 1:補領 2:換發
     * @param createNewCaseCaseNo
     * @return
     * @throws Exception
     */
    public Map<String, String> checkApimAndMore(CaseData caseData, String idCardLocation, String applyYyymmdd, String applyCode, String createNewCaseCaseNo, String decisionCaseNo, String productId, String agentDepart, String agentNo) throws Exception {

        String uniqId = caseData.getCaseNo();
        String idno = caseData.getIdno();

        // 是否戶役政驗證通過
        boolean isOkApim = false;

        // @call 17. APIM
        // 取5碼縣市代碼，例如 68000 桃市
        String issueSiteId = dropdownService.findDataKey_idlocation_new(idCardLocation.trim());
        // 透過call戶役政驗證身分證
        APIMInfoRspDto apimData = null;

        // 送回前端資訊
        Map<String, String> rspmap = new HashMap<>(); 
        // rspmap.put("message", ""); // FIXME: 戶役政通過時的狀態 message=""

        if (whiteListDao.isBypassLoan(idno)) {
            // bypass APIM 戶役政驗證
        } else {
            try {
                apimData = kgiService.call_APIM_info(uniqId, idno, applyYyymmdd, applyCode, issueSiteId);
                if (StringUtils.equals("200", apimData.getHttpCode())) {
                    if (StringUtils.equals("RS7009", apimData.getRdCode()) && StringUtils.equals("1", apimData.getResponseData().getCheckIdCardApply())) {
                        isOkApim = true;
                        logger.info("#### " + uniqId + " 戶役政驗證通過");
                        rspmap.put("message", "");
                        // @setup caseData.SubStatus 01 410
                        caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_410);
                    } else {
                        logger.error("#### " + uniqId + " 戶役政驗證失敗");
                        rspmap.put("message", "身分證資料有誤，如有任何需要協助，請洽客服人員");
                        // @setup caseData.SubStatus 01 411
                        caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_411);
                    }
                } else {
                    logger.error("#### " + uniqId + " 戶役政連線異常");
                    rspmap.put("message", "戶役政連線系統問題，如有任何需要協助，請洽客服人員");
                }
                
                String message = rspmap.get("message");
                isOkApim = StringUtils.isBlank(message);
            } catch(Exception e) {
                logger.error("Fail to call_APIM_info.", e);
            }
        }

        if (isOkApim) {
            // @HasQryIdInfo
            caseData.setHasQryIdInfo("01");

            // 新增戶役政檢核結果資料
            kgiService.call_ADD_IDCARD_INFO(uniqId, idno, caseData.getCaseNoWeb(), applyYyymmdd, issueSiteId, applyCode, DateUtil.GetDateFormatString());

            // 新增戶役政檢核結果資料至 DB TABLE: CaseMoiData
            saveCaseMoiData(uniqId, idno, idCardLocation, applyYyymmdd, applyCode, issueSiteId, apimData);
        }

        String Z07Date = DateUtil.GetDateFormatString("yyyyMMdd");

        // @call AML風險評級介接查詢
        checkCddRateRisk(uniqId, idno, agentDepart, agentNo);

        // 非預核名單(一般流程)才查詢 Z07
        if (isNonePreAudit(caseData.getIsPreAudit())) {
            // @call 19. Z07 呼叫Z07查詢
            String Z07Result = checkZ07(uniqId, decisionCaseNo, productId, Z07Date, caseData.getAgentNo(), caseData.getAgentDepart());

            // Y：是(有通報案件記錄), N：否
            // @HasQryZ07
            if (StringUtils.equals("N", Z07Result)) {
                caseData.setHasQryZ07("01");
            } else {
                caseData.setHasQryZ07("00");
            }
        }
        
        // @call 20. 呼叫外部負債資料查詢
        checkDebtInfo(uniqId, decisionCaseNo, productId, Z07Date);

        // previewCaseKycPDFHtml
        pdfService.pdfGeneratorCaseKycPDF(uniqId);
        
        // 是否正常產製 CaseKycPDF KYC表 HTML
        // @HasFinishKyc
        boolean isCasePdfOk = caseDocumentDao.isExist(uniqId, DOCUMENT_TYPE_LOAN_KYC_PDF);
        if (isCasePdfOk) {
            caseData.setHasFinishKyc("01");
            // @call 21. KYC 產生KYC表
            // @setup caseData.SubStatus 01 450
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_450, CASE_STATUS_01_JobStatus_020);
        } else {
            caseData.setHasFinishKyc("00");
            // @setup caseData.SubStatus 01 451
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_451);
        }

        // 判斷是否為利害關係人
        checkRelpResult(caseData);
		logger.info("#### " + uniqId + " 查詢是否為利害關係人...完成");
        
		
        caseDataDao.Update(caseData);
        return rspmap;
    } // end checkApimAndMore

    private void saveCaseMoiData(String uniqId, String idno, String idCardLocation, String applyYyymmdd, String applyCode, String issueSiteId, APIMInfoRspDto apimData) {
        CaseMoiData casemoidata = new CaseMoiData(uniqId);
        casemoidata.setIdNo(idno);
        // casemoidata.setCustomerName();
        casemoidata.setIssueLocation(idCardLocation);
        casemoidata.setIssueType(applyCode);
        casemoidata.setIssueDate(applyYyymmdd);
        casemoidata.setRequest(new Gson().toJson(new APIMInfoReqDto(idno, applyYyymmdd, applyCode, issueSiteId)));
        if (apimData != null) {
            casemoidata.setResponse(new Gson().toJson(apimData));
            casemoidata.setResponseCode(apimData.getResponseData().getCheckIdCardApply());
        }
        // casemoidata.setMemo();
        casemoidatadao.CreateOrUpdate(casemoidata);
    }

    /**
     * 風險評級:高 中 低
     */
    public void checkCddRateRisk(String uniqId, String idno, String agentDepart, String agentNo) {
        UspGetCddRate cddRate = uspDao.usp_Get_CDD_Rate(uniqId, Get_CDD_Rate_SystemType_G08, idno, agentDepart, agentNo);
        String risk = cddRate.getRisk();
        if (risk != null) {
            caseDataDao.updateCddRateRisk(uniqId, risk); // 風險評級:高 中 低
            // @setup caseData.SubStatus 01 420
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_420);
        } else {
            // @setup caseData.SubStatus 01 421
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_421);
        }
    }

    /**
     * Z07 異常身證通報紀錄(簡單說就是身份證有無被偽冒的紀錄)
     * 
     * @param uniqId
     * @param decisionCaseNo
     * @param productId @see PlContractConst.LOAN_ProductId_*
     * @param Z07Date yyyyMMdd
     * @return Z07Result: null, Y：是(有通報案件記錄), N：否
     * @throws Exception
     */
    public String checkZ07(String uniqId, String decisionCaseNo, String productId, String Z07Date, String agentNo, String agentDepart) {

        String Z07Html = null;
        String Z07Result = null; // null, Y：是(有通報案件記錄), N：否
        try {
            String apyType = apyType(productId);
            String PRODUCT = "6"; // 6.Z07
            String channelId = caseDataService.channelId(uniqId);
            String departId = caseDataService.departId(agentNo, agentDepart);
            QR_ChannelDepartList qr_ChannelDepartList = channelDepartService.findQR_ChannelDepartList(channelId, departId);
            String UNIT_ID = null;
            SendJcicRspDto sendJcic = kgiService.call_SEND_JCIC(uniqId, decisionCaseNo, apyType, PRODUCT, UNIT_ID);
            Z07Html = sendJcic.getHTML();
            if (StringUtils.equals("", sendJcic.getCHECK_CODE())) {
                String IS_DGT = "Y";
                CheckZ07ResultRspDto checkZ07Result = kgiService.call_CHECK_Z07_RESULT(uniqId, decisionCaseNo, apyType, IS_DGT, Z07Date);
                if (StringUtils.equals("", checkZ07Result.getCHECK_CODE())) {
                    Z07Result = checkZ07Result.getRESULT(); // Y：是(有通報案件記錄)，N：否
                    String Z07Time = DateUtil.GetDateFormatString("yyyy-MM-dd HH:mm:ss.SSS");
                    caseDataDao.updateZ07(uniqId, Z07Html, Z07Result, Z07Time);
                }
            }
        } catch(Exception e) {
            logger.error("Fail to checkZ07.", e);
        }

        if (Z07Result != null) {
            // @setup caseData.SubStatus 01 430
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_430);
        } else {
            // @setup caseData.SubStatus 01 431
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_431);
        }
        return Z07Result;
    } // end checkZ07

    /**
     * 呼叫外部負債資料查詢
     */
    public GetKycDebtInfoRspDtoDebtInfo checkDebtInfo(String uniqId, String decisionCaseNo, String productId, String Z07Date) throws Exception {
        GetKycDebtInfoRspDtoDebtInfo debtInfo = caseDataService.checkDebtInfo(uniqId, decisionCaseNo, productId, Z07Date);
        if (debtInfo != null) {
            // @setup caseData.SubStatus 01 440
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_440);
        } else {
            // @setup caseData.SubStatus 01 441
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_441);
        }
        return debtInfo;
    }

    /** 判斷是否為利害關係人 */
    public void checkRelpResult(CaseData caseData) throws Exception {
        String hasNoRelp = "00";
        hasNoRelp = checkRelpResult(caseData.getCaseNo(), caseData.getDecisionCaseNo());
        caseData.setHasNoRelp(hasNoRelp);
    }

    /** 判斷是否為利害關係人 */
    public String checkRelpResult(String uniqId, String decisionCaseNo) throws Exception {
        CheckRelpResultRspDto rsp = kgiService.call_CHECK_RELP_RESULT(uniqId, decisionCaseNo, "PLOAN");
        String hasNoRelp = "00"; // 不通過
        String checkRelpResult = "";
        String checkRelpTime = DateUtil.GetDateFormatString("yyyy-MM-dd HH:mm:ss.SSS");
        if (StringUtils.equals("", rsp.getCHECK_CODE())) {
            checkRelpResult = rsp.getRESULT();
            if (StringUtils.equals("N", checkRelpResult)) {
                hasNoRelp = "01"; // 通過
            }
        }
        caseDataDao.updateRelp(uniqId, checkRelpResult, checkRelpTime);
        return hasNoRelp;
    }

    /**
     * 確認上傳財力證明
     */
    public String applyUploadFinpf(String uniqId) {
        try {
            CaseData caseData = caseDataDao.ReadByCaseNo(uniqId);
            // @HasUploadFina
            caseData.setHasUploadFina("01");
            caseDataDao.Update(caseData);

            // @setup caseData.SubStatus 01 600
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_600);

            caseDataDao.updateJobStatusOnly(uniqId, CASE_STATUS_01_JobStatus_020.getJobStatus());

            // Update Status To Wait Upload by Scheduled Job
            userPhotoService.updateStatusToWaitUpload(uniqId, USERPHOTO_ProdType_2, USERPHOTO_PTYPE_2, USERPHOTO_SUBTYPE_99);

            return WebResult.GetResultString(0, "成功", null);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    private boolean checkNoFinph(String uniqId, String amlResult, CaseData caseData) throws Exception {

        String apyType = apyType(caseData.getProductId());
        String decisionCaseNo = caseData.getDecisionCaseNo();

        // (9)	CUST_KIND：客戶類型(1.貸款2.現金卡3.信用卡4.薪轉戶，如果有多個類型用『;』區隔,例如01;02)，(string)
        String custKind = StringUtils.join(custKinds(caseData), ";");

        // 判斷是否為免財力證明客戶
        CheckNoFinpfRspDto checkNoFinpf = kgiService.call_CHECK_NO_FINPF(uniqId, decisionCaseNo, apyType, "Y", "N", caseData.getCorp_type(), caseData.getCorp_name(), caseData.getTitle(), "", custKind, amlResult, caseData.getP_apy_amount(), caseData.getYearly_income());


		// save CHECK_NO_FINPF's MEMO
		caseDataDao.updateCHECK_NO_FINPF_MEMO(uniqId, checkNoFinpf.getMEMO());
		logger.info("#### checkNoFinpf.getMEMO()=" + checkNoFinpf.getMEMO());
		caseData.setCHECK_NO_FINPF_MEMO(checkNoFinpf.getMEMO());
		logger.info("#### set caseData.getCHECK_NO_FINPF_MEMO()=" + caseData.getCHECK_NO_FINPF_MEMO());


        String applyWithoutFina = "00";
        if (StringUtils.equals("", checkNoFinpf.getCHECK_CODE())) {

            // @setup caseData.SubStatus 01 040
            //caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_040);

            // boolean resultBefor = StringUtils.equals("Y", checkNoFinpf.getRESULT_BEFOR());
            boolean resultAfter = StringUtils.equals("Y", checkNoFinpf.getRESULT_AFTER());
            if (resultAfter) { // Y
                applyWithoutFina = "01";
            } else { // N
                applyWithoutFina = "00";
            }
        } else {
            applyWithoutFina = "00";
        }
        // @ApplyWithoutFina
        caseData.setApplyWithoutFina(applyWithoutFina);
		
		// @setup caseData.SubStatus 01 040
		caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_040);		

        if (StringUtils.equals("01", applyWithoutFina)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * (9)	CUST_KIND：客戶類型(1.貸款2.現金卡3.信用卡4.薪轉戶，如果有多個類型用『;』區隔,例如01;02)，(string)
     */
    private List<String> custKinds(CaseData caseData) {
        boolean checkSalaryAccount = StringUtils.equals("Y", caseData.getCheckSalaryAccount());
        boolean checkCreditcardAccount = StringUtils.equals("Y", caseData.getCheckCreditcardAccount());
        boolean checkCashcardAccount = StringUtils.equals("Y", caseData.getCheckCashcardAccount());
        boolean checkKgibankAccount = StringUtils.equals("Y", caseData.getCheckKgibankAccount());
        boolean checkLoanAccount = StringUtils.equals("Y", caseData.getCheckLoanAccount());
        System.out.println(checkSalaryAccount);
        System.out.println(checkCreditcardAccount);
        System.out.println(checkCashcardAccount);
        System.out.println(checkKgibankAccount);
        System.out.println(checkLoanAccount);
        return custKinds(checkSalaryAccount, checkCreditcardAccount, checkCashcardAccount, checkKgibankAccount, checkLoanAccount);
    }

    /**
     * (9)	CUST_KIND：客戶類型(1.貸款2.現金卡3.信用卡4.薪轉戶，如果有多個類型用『;』區隔,例如01;02)，(string)
     */
    private List<String> custKinds(boolean checkSalaryAccount, boolean checkCreditcardAccount, boolean checkCashcardAccount, boolean checkKgibankAccount, boolean checkLoanAccount) {
        List<String> custKinds = new ArrayList<>();
        if (checkLoanAccount) { // 1.貸款
            custKinds.add("1");
        }
        if (checkKgibankAccount) { // 若為存款戶(checkKgibankAccount=Y)，則預設1.貸款 demand by Ben
            custKinds.add("1");
        }
        if (checkCashcardAccount) { // 2.現金卡
            custKinds.add("2");
        }
        if (checkCreditcardAccount) { // 3.信用卡
            custKinds.add("3");
        }
        if (checkSalaryAccount) { // 4.薪轉戶
            custKinds.add("4");
        }
        return custKinds;
    }

    /**
     * 產品別,1.現金卡(e貸寶) 2.PLOAN 3.信用卡，(string)
     */
    private String apyType(String productId) throws Exception {
        if (StringUtils.equals("1", productId) || StringUtils.equals("2", productId)) { // 1 預核 2 PL
            return "2";
        } else if (StringUtils.equals("3", productId)) { // 3 RPL
            return "1";
        } else {
            throw new Exception("productId not found.");
        }
    }

    /**
     * (2)	CUST_TYPE：客戶類型(01-PL、02-GM、03-RPL、04-CC)
     */
    private List<String> custTypes(CaseData caseData) throws Exception {
        // boolean checkSalaryAccount = StringUtils.equals("Y", caseData.getCheckSalaryAccount());
        boolean checkCreditcardAccount = StringUtils.equals("Y", caseData.getCheckCreditcardAccount());
        boolean checkCashcardAccount = StringUtils.equals("Y", caseData.getCheckCashcardAccount());
        // boolean checkKgibankAccount = StringUtils.equals("Y", caseData.getCheckKgibankAccount());
        boolean checkLoanAccount = StringUtils.equals("Y", caseData.getCheckLoanAccount());

        List<String> custTypes = new ArrayList<>();

        // (2)	CUST_TYPE：客戶類型(01-PL、02-GM、03-RPL、04-CC)
        if (checkLoanAccount) { // 01-PL
            custTypes.add("01");
        }
        if (checkCashcardAccount) { // 02-GM
            custTypes.add("02");
        }
        // if (NotAvailable) { // 03-RPL
        //  custTypes.add("03");
        // }
        if (checkCreditcardAccount) { // 04-CC
            custTypes.add("04");
        }

        // 若無 CUST_TYPE ，預設 01-PL demand by Ben
        if (custTypes.size() == 0) {
            custTypes.add("01");
        }
        return custTypes;
    }

    /**
     * 判斷是否有 2.身份證
     * 依身份別 CUST_TYPE 呼叫多次 CHECK_HAVE_FINPF
     */
    private void checkHaveFinpf2(String uniqId, CaseData caseData) throws Exception {

        String decisionCaseNo = caseData.getDecisionCaseNo();
        List<String> custTypes = custTypes(caseData);

        String hasOldIdImg = "00";
        List<String> oldIdImgNos = new ArrayList<>();

        for (String custType : custTypes) {
            CheckHaveFinpfRspDto checkHaveFinpf2 = kgiService.call_CHECK_HAVE_FINPF(uniqId, decisionCaseNo, custType, "2");
            if (StringUtils.equals("", checkHaveFinpf2.getCHECK_CODE())) {

                String caseNoApsPrev = checkHaveFinpf2.getCASE_NO_APS();
                if (StringUtils.isNotBlank(caseNoApsPrev)) {
                    caseDataDao.updateCaseNoApsPrev(uniqId, caseNoApsPrev);
                }

                // @setup caseData.SubStatus 01 050
                caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_050);

                if (StringUtils.equals("Y", checkHaveFinpf2.getHAVE_IMG())) {
                    hasOldIdImg = "01";
                    oldIdImgNos.add(checkHaveFinpf2.getIMG_CASE_NO());
                }
            }
        } // end for

        // @HasOldIdImg @OldIdImgNoList
        if (StringUtils.equals("00", hasOldIdImg)) {
            caseData.setHasOldIdImg("00");
            caseData.setOldIdImgNoList("");
        } else if (StringUtils.equals("01", hasOldIdImg)) {
            caseData.setHasOldIdImg(hasOldIdImg);
            caseData.setOldIdImgNoList(StringUtils.join(oldIdImgNos, ";")); // 用";"串接OldIdImgNoList
        }
    } // end checkHaveFinpf2

    /**
     * 判斷是否有 3.財力證明
     * 依身份別 CUST_TYPE 呼叫多次 CHECK_HAVE_FINPF
     */
    private void checkHaveFinpf3(String uniqId, CaseData caseData) throws Exception {
        
        String decisionCaseNo = caseData.getDecisionCaseNo();
        List<String> custTypes = custTypes(caseData);

        String hasOldFinaImg = "00";
        List<String> oldFinaImgNos = new ArrayList<>();

        for (String custType : custTypes) {
            CheckHaveFinpfRspDto checkHaveFinpf3 = kgiService.call_CHECK_HAVE_FINPF(uniqId, decisionCaseNo, custType, "3");
            if (StringUtils.equals("", checkHaveFinpf3.getCHECK_CODE())) {
                
                String caseNoPasPrev = checkHaveFinpf3.getCASE_NO_APS();
                if (StringUtils.isNotBlank(caseNoPasPrev)) {
                    caseDataDao.updateCaseNoApsPrev(uniqId, caseNoPasPrev);
                }

                // @setup caseData.SubStatus 01 060
                caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_060);
                
                if (StringUtils.equals("Y", checkHaveFinpf3.getHAVE_IMG())) {
                    hasOldFinaImg = "01";
                    oldFinaImgNos.add(checkHaveFinpf3.getIMG_CASE_NO());
                }
            }
        } // end for

        // @HasOldFinaImg @OldFinaImgNoList
        if (StringUtils.equals("00", hasOldFinaImg)) {
            caseData.setHasOldFinaImg("00");
            caseData.setOldFinaImgNoList("");
        } else if (StringUtils.equals("01", hasOldFinaImg)) {
            caseData.setHasOldFinaImg(hasOldFinaImg);
            caseData.setOldFinaImgNoList(StringUtils.join(oldFinaImgNos, ";")); // 用";"串接OldIdImgNoList
        }
    } // end checkHaveFinpf3

    public String applyPrepareConfirm(String uniqId) {
        try {
            ApplyPrepareConfirmResp resp = new ApplyPrepareConfirmResp();
            CaseData existing = caseDataDao.ReadByCaseNo(uniqId);
            CaseDataResp caseDataResp = new CaseDataResp();
            BeanUtils.copyProperties(existing, caseDataResp);
            resp.setCaseDataResp(caseDataResp);
            return WebResult.GetResultString(0, "成功", resp);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end applyPrepareConfirm

    /**
     * 確認上傳同一關係人表
     */
    public String applyRelationDegree(String uniqId, String reqJson) {
        try {
            CaseData existing = caseDataDao.ReadByCaseNo(uniqId);

            ApplyRelationDegreeView view = new Gson().fromJson(reqJson, ApplyRelationDegreeView.class);
            caseDataRelationDegreeDao.saveApplyRelationDegreeView(uniqId, view);
            
            AddRelShipReqDto reqBody = new AddRelShipReqDto();
            reqBody.setCUSTOMER_ID(existing.getIdno());
            reqBody.setDGTPlatformNo(existing.getCaseNoWeb());
            // DATA_LIST
            for (ApplySecondDegree sec : view.getAddSecondDegreeData()) {
                String relationshipName = sec.getRelationshipName();
                String relationshipId = sec.getRelationshipId();
                String relationship = sec.getRelationship();
                if (StringUtils.isNotBlank(relationshipName) && StringUtils.isNotBlank(relationshipId) && StringUtils.isNotBlank(relationship)) {
                    AddRelShipReqDtoDataList data = new AddRelShipReqDtoDataList(relationshipName, null, relationshipId, relationship);
                    reqBody.getDATA_LIST().add(data);
                }
            }
            
            // DATA_COMP_LIST
            for (ApplyRelationDegreeCorp corp : view.getAddData()) {
                if (StringUtils.length(corp.getCorpRelationDegree()) == 4) {
                    AddRelShipReqDtoDataCompList dataComp =  new AddRelShipReqDtoDataCompList(corp.getCorpName(), corp.getCorpnumber(), corp.getCorpRelationDegree().substring(0, 2), corp.getCorpRelationDegree().substring(2, 4), null);
                    reqBody.getDATA_COMP_LIST().add(dataComp);
                }
            }
            try {
                AddRelShipRspDto rsp = kgiService.call_ADD_RELSHIP_DATA(uniqId, reqBody);
                // ADD_RELSHIP_DATA 成功與否都完成 2021-03-23 demand by Ben
                // if (!StringUtils.equals("", rsp.getCHECK_CODE())) {
                //     return WebResult.GetResultString(99, rsp.getERR_MSG(), null);
                // }
            } catch(Exception e) {
                logger.error("Fail to ADD_RELSHIP_DATA.", e);
            }
            // @setup caseData.SubStatus 01 700
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_700);
            
            return WebResult.GetResultString(0, "成功", null);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end applyRelationDegree

    /**
     * 確認完成申請
     */
    public String applyStepok(String uniqId) {
        try {
            // @setup caseData.SubStatus 01 800
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_800, CASE_STATUS_01_JobStatus_030);

            return WebResult.GetResultString(0, "成功", null);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end applyStepok

    /**
     * 取得預核名單資料
     */
    public String applyPreAudit(String uniqId) {
        try {
            ApplyPreAuditResp applyPreAuditResp = new ApplyPreAuditResp();
            AuditData ad = auditDataDao.findByUniqId(uniqId);
            applyPreAuditResp.setProjectCode(ad.getProjectCode());
            applyPreAuditResp.setCreditLine(ad.getCreditLine());
            applyPreAuditResp.setIntrestRate(ad.getIntrestRate());
            String twdrate = kgiService.call_GET_TWDRATE(uniqId);
            applyPreAuditResp.setTwdrate(twdrate);

            return WebResult.GetResultString(0, "成功", applyPreAuditResp);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end applyPreAudit

    public String applyNewCreditcard(String uniqId, String ipAddress, String reqJson) {
        try {
            logger.info("### applyNewCreditcard - reqJson = " + reqJson);
            ApplyNewCreditcardView view = new Gson().fromJson(reqJson, ApplyNewCreditcardView.class);
            CaseData existing = caseDataDao.Read(new CaseData(uniqId));
            String productId = globalConfig.Apply_Creditcard_ProductId_Default(); // 519001
            creditCaseDataDao.saveApplyNewCreditcardView(uniqId, productId, ipAddress, view, existing);
            
            return WebResult.GetResultString(0, "成功", null);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end applyNewCreditcard

    // public String applyCreateNewCase(String uniqId) {
    //     try {
    //         createNewCase(uniqId);
    //         return WebResult.GetResultString(0, "成功", null);
    //     } catch (ErrorResultException e) {
    //         return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
    //     } catch (Exception e) {
    //         logger.error("未處理的錯誤", e);
    //         return WebResult.GetResultString(99,
    //                 configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
    //     }
    // } // end applyCreateNewCase

    /**
     * 已完成他行信用卡驗身，視同完成填寫資料02，進行產製 HTML 上傳 FTP
     */
    public void previewApplyCreditCard(String uniqId) throws Exception {
        pdfService.pdfGeneratorCreditCardPDF(uniqId);
        creditCaseDataDao.updateStatusbyUniqId(uniqId, CASE_STATUS_WRITE_SUCCESS);

        // @setup caseData.SubStatus 03 000
        caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_03_SubStatus_000);
    } // end previewApplyCreditCard

    private String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<String>();
        for (java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null)
                emptyNames.add(pd.getName());
        }

        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }

    // then use Spring BeanUtils to copy and ignore null using our function
    private void copyProperties(Object src, Object target) {
        BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
    }

    public String getQryQa(String uniqId, String IpAddress) {
        try {
            String casenoweb = snSer.getEXPUniqId();
            return WebResult.GetResultString(0, "成功", kgiService.call_QRY_QA(uniqId, casenoweb, IpAddress));
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    public String applyGetTestRplScore(String uniqId, String IpAddress, String reqJson) {
        ApplyGetTestRplScoreView view = new Gson().fromJson(reqJson, ApplyGetTestRplScoreView.class);
        try {
            String CASE_NO = decisionCaseNo(uniqId, "A123456789");
            GetTestRplScoreRspDto rsp = kgiService.call_GET_TEST_RPL_SCORE(uniqId, CASE_NO, view.getMARRIAGE(), view.getJOBYEAR(), view.getAGE(), view.getSEX(), view.getEDUCATION(), view.getYEARLY_INCOME(), view.getCORP_TYPE(), view.getCORP_TITLE());
            GetTestRplScoreRspDtoScoreInfo scoreInfo = rsp.getSCORE_INFO();
            // creditLine萬元
            String creditLine = String.valueOf(Integer.valueOf(scoreInfo.getCREDIT_LINE())/10000);

            String casenoweb = snSer.getEXPUniqId();
            Plexp fullItem = new Plexp();
            fullItem.setExpNo(casenoweb);
            fullItem.setDate(casenoweb.substring(3, 9));
            fullItem.setCaseNoWeb(""); // 新增 CaseData 的 CaseNo 時回寫入 PLExp 的 CaseNoWeb
            fullItem.setCaseNo(CASE_NO);
            fullItem.setAnsList("");
            fullItem.setProjectCode(scoreInfo.getPROJECT_NO());
            fullItem.setCreditLine(creditLine);
            fullItem.setIntrestRate(scoreInfo.getINTEREST_RATE());
            fullItem.setRank(scoreInfo.getSCORE_RANK());
            fullItem.setScore(scoreInfo.getTOTAL_SCORE());
            fullItem.setExpProductType(view.getProductType());
            // view.getCORP_TYPE()
            // view.getCORP_TITLE()
            fullItem.setSex(view.getSEX());
            fullItem.setAge(view.getAGE());
            fullItem.setEdu(view.getEDUCATION());
            fullItem.setMarriage(view.getMARRIAGE());
            fullItem.setJobType("");
            fullItem.setWorkyear(view.getJOBYEAR());
            fullItem.setYearincome(view.getYEARLY_INCOME());
            System.out.println("### " + new Gson().toJson(fullItem));
            plexpService.insert(fullItem);

            CalMainResp resp = new CalMainResp();
            resp.setCASE_NO(CASE_NO);
            resp.setEXP_NO(casenoweb);
            resp.setCREDIT_LINE(creditLine);
            resp.setINTEREST_RATE(scoreInfo.getINTEREST_RATE());

            return WebResult.GetResultString(0, "成功", resp);
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    public String getSendAsw(String uniqId, String IpAddress, String reqJson) {
        try {
            String casenoweb = snSer.getEXPUniqId();
            String CASE_NO = kgiService.call_QRY_QA(uniqId, casenoweb, IpAddress).getCASE_NO();

            Map<String, Object> json = new Gson().fromJson(reqJson, new TypeToken<Map<String, String>>() {}.getType());
//            SendAseReqDto json = new Gson().fromJson(reqJson, SendAseReqDto.class);
            String QUEST_LIST = json.get("QUEST_LIST").toString();
            String ANSWER_LIST = json.get("ANSWER_LIST").toString();
            String ProductType = json.get("ProductType") != null ? json.get("ProductType").toString() : "";
            String[] list = ANSWER_LIST.split(";");
            SendAseRspDto scoreInfo = kgiService.call_SEND_ASW(uniqId, CASE_NO, QUEST_LIST, ANSWER_LIST);
            
            Plexp fullItem = new Plexp();
            fullItem.setExpNo(casenoweb);
            fullItem.setDate(casenoweb.substring(3, 9));
            fullItem.setCaseNoWeb(""); // 新增 CaseData 的 CaseNo 時回寫入 PLExp 的 CaseNoWeb
            fullItem.setCaseNo(CASE_NO);
            fullItem.setAnsList(ANSWER_LIST);
            fullItem.setProjectCode(scoreInfo.getPROJECT_CODE());
            fullItem.setCreditLine(scoreInfo.getCREDIT_LINE());
            fullItem.setIntrestRate(scoreInfo.getINTEREST_RATE());
            fullItem.setRank(scoreInfo.getRANK());
            fullItem.setScore(scoreInfo.getSCORE());
            fullItem.setExpProductType(ProductType);
            fullItem.setSex(list[0]);
            fullItem.setAge(list[1]);
            fullItem.setEdu(list[2]);
            fullItem.setMarriage(list[3]);
            fullItem.setJobType(list[5]);
            fullItem.setWorkyear(list[6]);
            fullItem.setYearincome(list[7]);
            plexpService.insert(fullItem);
            
            scoreInfo.setCASE_NO(CASE_NO);
            CalMainResp resp = new CalMainResp();
            resp.setCASE_NO(CASE_NO);
            resp.setEXP_NO(casenoweb);
            resp.setCREDIT_LINE(scoreInfo.getCREDIT_LINE());
            resp.setINTEREST_RATE(scoreInfo.getINTEREST_RATE());
            return WebResult.GetResultString(0, "成功", resp);
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    public String getPLCUs(String uniqId, String reqJson) {
        try {
            PlcusReqDto json = new Gson().fromJson(reqJson, PlcusReqDto.class);
            PLCUs fullItem = new PLCUs();
            fullItem.setUniqId(uniqId);
            fullItem.setUniqType("04");
            fullItem.setName(json.getName());
            fullItem.setPhone(json.getPhone());
            fullItem.setEMail(json.getEMail());
            fullItem.setContactTime(json.getContactTime());
            fullItem.setNote(json.getNote());
            fullItem.setProductId(json.getProductId());
            fullItem.setDepartId(json.getDepartId());
            fullItem.setMember(json.getMember());
            // fullItem.setPrevPage();
            // fullItem.setContactFlag();
            // fullItem.setUTime();

            plcusdao.Create(fullItem);
            return WebResult.GetResultString(0, "成功", null);
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    public String applyPaper(String uniqId) {
        try {
            // @setup caseData.SubStatus 04 000
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_04_SubStatus_000);
            return WebResult.GetResultString(0, "成功", null);
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    /** sms 簡訊發送 稍後補件 */
    public String applyLaterAddon(String uniqId, String reqJson) {
        try {
            ApplyLaterAddonView view = new Gson().fromJson(reqJson, ApplyLaterAddonView.class);
            CaseData caseData = caseDataDao.findByCaseNo(uniqId);
            if (caseData != null) {
                String phoneNumber = view.getPhone();
                String billDepart = caseData.getAgentDepart();
                String message = globalConfig.SMS_applyLaterAddon()
                        .replace("%ApplyStart%", globalConfig.Apply_Start());
                String previousSendTime = caseData.getLaterAddonSendTime();
                boolean sendSms = smsService.sendSms(uniqId, phoneNumber, billDepart, message, previousSendTime);
                if (sendSms) {
                    caseDataDao.updateLaterAddonSendTime(uniqId);
                }
            }
            return WebResult.GetResultString(0, "成功", null);
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    /** sms 簡訊發送 額度利率體驗傳送方案 */
    public String applyCalResult(String reqJson) {
        try {
            
            ApplyCalResultView view = new Gson().fromJson(reqJson, ApplyCalResultView.class);
            Plexp plexp = plexpService.findByCaseNo(view.getCASE_NO());
            plexp.setPhone(view.getPhone());
            plexpService.updatePlexpPhone(plexp.getCaseNo(),plexp.getPhone());
            if (plexp != null) {
                String expNo = plexp.getExpNo(); // 即額度利率體驗的 UniqId 格式 EXP0000000000000
    
                String phoneNumber = view.getPhone();
                String billDepart = globalConfig.SMS_applyCalResult_BillDep(); // 9722

                String PT = "2";
                String productType = plexp.getExpProductType(); // PL/RPL
                if (StringUtils.equalsIgnoreCase("RPL", productType)) { // RPL
                    PT = "3";
                } else { // PL
                    PT = "2";
                }
                // 額度利率體驗結果
                String message = globalConfig.SMS_applyCalResult()
                    .replace("%CreditLine%", plexp.getCreditLine())
                    .replace("%IntrestRate%", plexp.getIntrestRate())
                    .replace("%ApplyStart%", globalConfig.Apply_Start().concat("?EXP=").concat(plexp.getCaseNo()));
                    // .replace("%ApplyStart%", globalConfig.Apply_Start().concat("?PT=").concat(PT).concat("&EXP=").concat(plexp.getCaseNo()));

                String previousSendTime = null;
                System.out.println(message);
                smsService.sendSms(expNo, phoneNumber, billDepart, message, previousSendTime);
            }
            return WebResult.GetResultString(0, "成功", null);
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }
    
	//起案進入處
    public CreateNewCaseRspDto createNewCase(CaseData caseData) throws Exception{
        String uniqId = caseData.getCaseNo();
        String authType = apsService.externalCheckTypeForAddNewCase(caseData.getCaseNo());
        String externalCheckType = apsService.transAuthType(authType);

        CreateNewCaseReqDto req = new CreateNewCaseReqDto();
        req.setCASE_NO_WEB(caseData.getCaseNo()); // (1)
        req.setCUSTOMER_ID(caseData.getIdno()); // (2)
        req.setCUSTOMER_NAME(caseData.getCustomer_name()); // (3)
        req.setDATA_TYPE(caseData.getProductId()); // (4)
        req.setBIRTHDAY(caseData.getBirthday()); // (5)
        req.setMOBILE_TEL(caseData.getMobile_tel()); // (6)
        req.setP_APY_AMOUNT(caseData.getP_apy_amount()); // (7)
        req.setHOUSE_CITY_CODE(caseData.getHouse_city_code()); // (8)
        req.setHOUSE_ADDRESS(splitAddress(caseData.getHouse_address())); // (9)
        req.setAUTH_TYPE(authType); // (10)
        String channelId = caseDataService.channelId(uniqId);
        String departId = caseDataService.departId(caseData.getAgentNo(), caseData.getAgentDepart());
        req.setENT_TYPE(channelId); // (11)
        req.setSTATUS_CODE(""); // (12)
        req.setMESSAGE(""); // (13)
        // -------------
        // 卡加貸 區塊
        req.setPLOAN_CC_FLG(""); // (14)
        req.setPLOAN_CC_JCIC_DATE(""); // (15)
        req.setPLOAN_CC_JCIC_HTML(""); // (16)
        req.setORG_PROJECT_NO(""); // (17)
        req.setORG_CREDIT_LINE("0"); // (18) 無資料時錯誤，預設帶0
        req.setORG_INTEREST_RATE("0"); // (19) 無資料時錯誤，預設帶0
        req.setORG_RANK(""); // (20)
        req.setORG_SCORE("0"); // (21) 無資料時錯誤，預設帶0
        // -------------
        req.setIS_CONSENT("2"); // (22)
        req.setIS_CONSENT_DATETIME("00000000000000"); // (23) 日期內容待確認
        req.setEXTERNAL_CHECK_TYPE(externalCheckType); // (24)
        req.setCASE_NO_SCORE_CC(""); // (25)

        req.setEXTERNAL_SOURCE(channelId); // (26)
        // 用(EXTERNAL_SOURCE)兩碼查詢 QR_CHANNELLIST 後取 CHANNEL_NAME 欄位
        QR_ChannelList qr_channellist = new QR_ChannelList();
        qr_channellist.setChannelId(channelId);
        qr_channellist = QR_ChannelListDao.Read(qr_channellist);
        if(qr_channellist != null){
            req.setEXTERNAL_SOURCE_CH(qr_channellist.getChannelName());
        } else {
            req.setEXTERNAL_SOURCE_CH("");
        }

        logger.info("### channelId=" + channelId);
        logger.info("### departId=" + departId);
        QR_ChannelDepartList qr_ChannelDepartList = channelDepartService.findQR_ChannelDepartList(channelId, departId);

        req.setASSIGN_BRANCH(StringUtils.isNotBlank(qr_ChannelDepartList.getAssignDepart())
            ? qr_ChannelDepartList.getAssignDepart()
            : "9743");

        // (29) B_S_UNIT：轉介單位
        // 以登入頁輸入之業務人員代號為優先
        // 若無，為取結果列之TransDepart欄位
        // 若再無，則轉介單位為9743
        if (StringUtils.isNotBlank(caseData.getAgentDepart())) {
            req.setB_S_UNIT(caseData.getAgentDepart());
        } else if ( qr_ChannelDepartList != null && StringUtils.isNotBlank(qr_ChannelDepartList.getTransDepart())) {
            req.setB_S_UNIT(qr_ChannelDepartList.getTransDepart());
        } else {
            req.setB_S_UNIT(KB_9743_DEPART_ID);
        }

        // (30) B_S_CODE：轉介人員
        // 以登入頁輸入之業務人員代號為優先，
        // 若無，為取結果列之TransMember欄位
        // 若再無，則轉介人員為空值
        if (StringUtils.isNotBlank(caseData.getAgentNo())) {
            req.setB_S_CODE(caseData.getAgentNo());
        } else if ( qr_ChannelDepartList != null && StringUtils.isNotBlank(qr_ChannelDepartList.getTransMember())) {
            req.setB_S_CODE(qr_ChannelDepartList.getTransMember());
        } else {
            req.setB_S_CODE("");
        }

        /*
         * 2021/05/12 數位平台申請轉介員編帶入推廣員編邏輯
         * 目前數位平台登入頁之轉介員編/單位未帶入推廣員編/單位，請調整如下：
         * 1-1.數位平台登入頁之員編(轉介)對應轉介單位為DS(8220~8270)或TM(9740、9743、8882)或客服(9621)時，則轉介員編/單位應同步帶入推廣員編/單位。
         * 1-2.數位平台登入頁之員編(轉介)對應轉介單位非DS、TM或客服，則僅帶入轉介單位對應大表後得到的推廣單位欄位(PromoDepart)的值到推廣單位，推廣員編則以空值帶入。
         * 2.數位平台登入頁未輸入員編(轉介)或輸入錯誤員編(轉介)時，轉介單位與推廣單位均為9743、且推廣員編帶入登入頁員編(轉介)資料或空值。
         *
         * (31) SYMNO：推廣單位
         * (32) SYKNO：推廣人員
         */

        // 有輸入 數位平台登入頁之員編(轉介員編)，則判斷 轉介單位 是否為 DS(8220~8270) 或 TM(9740、9743、8882) 或 客服(9621)
        if (StringUtils.isNotBlank(caseData.getAgentNo())) {

            String[] configValues = configDao.ReadConfigValue("airloanEX.SyncTransInfoToPromoInfo", "").split(",");
            for (String configValue : configValues) {

                // 1-1. 是：轉介資訊(單位、員編) 同步帶入 推廣資訊(單位、員編)
                if (configValue.equals(req.getB_S_UNIT())) {
                    req.setSYMNO(req.getB_S_UNIT());
                    req.setSYKNO(req.getB_S_CODE());
                    break;

                // 1-2. 否(非DS、TM或客服)：轉介單位 僅帶入轉介單位對應大表後得到的推廣單位欄位(PromoDepart)的值到推廣單位，推廣員編 以空值帶入
                } else {
                    req.setSYMNO(qr_ChannelDepartList.getPromoDepart());
                    req.setSYKNO("");
                }
            }

        // 沒輸入 數位平台登入頁之員編(轉介員編) 或輸入錯誤員編(轉介)時
        // 2.轉介單位與推廣單位均為9743、且推廣員編帶入登入頁員編(轉介)資料。
        } else {
            req.setSYMNO(KB_9743_DEPART_ID);
            req.setSYKNO("");
        }

//        // (29)
//        if (StringUtils.isNotBlank(caseData.getAgentDepart())) {
//            req.setB_S_UNIT(caseData.getAgentDepart());
//        } else if (qr_ChannelDepartList != null) {
//            req.setB_S_UNIT(qr_ChannelDepartList.getTransDepart());
//        }
//        // 同(28)，B_S_UNIT為取結果列之PromoDepart欄位
//        req.setSYMNO(StringUtils.isNotBlank(qr_ChannelDepartList.getPromoDepart())
//            ? qr_ChannelDepartList.getPromoDepart()
//            : "0");
//        // 同(28)，B_S_UNIT為取結果列之PromoMember欄位
//        req.setSYKNO(StringUtils.isNotBlank(qr_ChannelDepartList.getPromoMember())
//            ? qr_ChannelDepartList.getPromoMember()
//            : "0");
//
//        // 同(28)，B_S_UNIT為取結果列之TransDepart欄位
//        // 以登入頁輸入之業務人員代號為優先，
//        // 若無，則取同(28)結果列之TransMember欄位
//        // (30)
//        if (StringUtils.isNotBlank(caseData.getAgentNo())) {
//            req.setB_S_CODE(caseData.getAgentNo());
//        } else if (qr_ChannelDepartList != null) {
//            req.setB_S_CODE(qr_ChannelDepartList.getTransMember());
//        }

        req.setCOMP_CASE("N"); // (33) 

        if(StringUtils.equals("00", caseData.getApplyWithoutFina())){
            req.setNO_FINPF("N");
        } else {
            req.setNO_FINPF("Y");
        }
        if(StringUtils.equals("00", caseData.getHasOldFinaImg())){
            req.setHAVE_FINPF("N");
        } else {
            req.setHAVE_FINPF("Y");
        }
        req.setSALARY_ACCOUNT(caseData.getCheckSalaryAccount());
        req.setEDUCATION(caseData.getEducation());
        req.setCORP_NAME(caseData.getCorp_name());
        req.setYEARLY_INCOME(caseData.getYearly_income());
        req.setON_BOARD_DATE(caseData.getOn_board_date());
        req.setCORP_CLASS(caseData.getCorp_class());
        req.setCORP_TYPE(caseData.getCorp_type());
        req.setTITLE(caseData.getTitle());

        req.setAML_FLG("");

        String occupation = caseData.getOccupation();
        String occupationName = dropdownService.findDataName_occupation(occupation);
        req.setCORP_AML(occupation); //Occupation 職業類別
        req.setCORP_AML_NAME(occupationName); // OccupationName 職業類別名稱
        req.setAML_RESULT("");

        // FastPass
        FastPass fastPass = fastPassDao.Read(new FastPass(uniqId));
        if (fastPass != null) {
            req.setQRY_AML_REQUEST(fastPass.getAMLRequest());
            req.setQRY_AML_RESULT(fastPass.getAMLResponse());
        }
        
        req.setMARRIAGE(caseData.getMarriage());
        req.setCHT_FLAG("0");
        req.setCREDIT_VALUE("");

        req.setCORP_CITY_CODE(caseData.getCorp_city_code());
        req.setCORP_ADDRESS(splitAddress(caseData.getCorp_address()));

        String corpTelArea = caseData.getCorp_tel_area();
        String corpTel = caseData.getCorp_tel();
        if (isMobilePhone(corpTel)) {
            corpTelArea = "00";
        }
        req.setCORP_TEL_AREA(corpTelArea);
        req.setCORP_TEL(corpTel);
        req.setCORP_TEL_EXTEN(caseData.getCorp_tel_exten());
        
        req.setEMAIL_ADDRESS(caseData.getEmail_address());
        
        String houseTelArea = caseData.getHouse_tel_area();
        String houseTel = caseData.getHouse_tel();
        if (isMobilePhone(houseTel)) {
            houseTelArea = "00";
        }
        req.setHOUSE_TEL_AREA(houseTelArea);
        req.setHOUSE_TEL(houseTel);
        
        req.setESTATE_TYPE(caseData.getEstate_type());
        req.setIP_ADDRESS(caseData.getIp_address());
        req.setFIN_STATE("4");
        req.setP_PURPOSE(caseData.getP_purpose());
        req.setP_PURPOSE_NAME(caseData.getP_purpose_name());
        req.setTAX_ID("");

        String haveFinpfType;
        if (StringUtils.equals("01", caseData.getHasOldFinaImg())) {
            haveFinpfType = "1"; // 1.影像系統舊財力
        } 
        else if (StringUtils.equals("Y", caseData.getCheckSalaryAccount())) {
            haveFinpfType = "2"; // 2.本行薪轉戶
        } 
        else {
            haveFinpfType = "3"; // 3.無既有財力，(string)
        }
        req.setHAVE_FINPF_TYPE(haveFinpfType);
        req.setSPONSOR("");
        
        String knowChannelType = caseDataService.knowChannelType(uniqId, caseData.getAgentNo(), isOldOne(caseData.getUserType()), isPreAudit(caseData.getIsPreAudit()), qr_ChannelDepartList, qr_channellist);

        req.setINFOSOURCE_CDE(knowChannelType);
        caseDataDao.updateINFOSOURCE_CDE(uniqId, knowChannelType);

        QR_ShortUrl qrShortUrl = caseDataService.qrShortUrl(uniqId);
        if (qrShortUrl != null && StringUtils.isNotBlank(qrShortUrl.getPrjCode())) {
            req.setPROJECT_CODE(qrShortUrl.getPrjCode());
        } else {
            req.setPROJECT_CODE(caseData.getPJ());
        }
        req.setP_PERIOD(caseData.getP_period());
        String consentFinancial;
        
		//是否同意使用舊財力
		if (StringUtils.equals("Y", caseData.getLoan_personal_data_Result())) {
            consentFinancial = "Y";
        } else {
            consentFinancial = "N";
        }
        req.setCONSENT_FINANCIAL(consentFinancial);
        req.setNO_FINPF_MEMO(caseData.getCHECK_NO_FINPF_MEMO());
		logger.info("######## NO_FINPF_MEMO=" + caseData.getCHECK_NO_FINPF_MEMO());
		logger.info("######## 1.ApplyService.CREATE_NEW_CASE....");
        return kgiService.call_CREATE_NEW_CASE(uniqId, req);
    }

    /** 取得跨售申辦資料 */
    public String applyGetCSMemoData(String reqJson) {
        try {
            ApplyCSDataView view = new Gson().fromJson(reqJson, ApplyCSDataView.class);

            CSCommonCifData csData = this.applyGetCSMemoData(view.getUniqId(), SystemConst.MEMO_CSCOMMON_CIF);
            return WebResult.GetResultString(0, "成功", csData);
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    private CSCommonCifData applyGetCSMemoData(String uniqId, String memoName){
        MemoData condition = new MemoData();
        condition.setUniqId(uniqId);
        condition.setMemoName(memoName);
        MemoData memoData = memoDataDao.Read(condition);

        CSCommonCifData result = new CSCommonCifData();

        if(memoData != null && StringUtils.isNotEmpty(memoData.getMemo())){
            JSONObject memoJson = JSONObject.fromObject(memoData.getMemo());
            String data = memoJson.get("data").toString();
            
            Gson gson = new Gson();
            result = gson.fromJson(data, CSCommonCifData.class);
        }

        return result;
    }
    
    /** 取得申辦css主題檔 */
    public String applyGetThemePath(String reqJson) {
        try {
            
            ApplyThemePathView view = new Gson().fromJson(reqJson, ApplyThemePathView.class);
            QR_ChannelList condition = new QR_ChannelList();
            condition.setChannelId(view.getChannelId());
            QR_ChannelList channelData = QR_ChannelListDao.Read(condition);
            if(channelData != null){
                return WebResult.GetResultString(0, "成功", channelData.getThemePath());
            }
            return WebResult.GetResultString(99,
                configDao.ReadConfigValue("CONT_Err_Unexpected", "查無資料"), null);
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    private String csAddrSplitByArea(String allAddr){
        if(allAddr == null){
            return "";
        }
        String areaStr = "區";
        int index = allAddr.indexOf(areaStr);
        return allAddr.substring(index + 1, allAddr.length());
    }
}
