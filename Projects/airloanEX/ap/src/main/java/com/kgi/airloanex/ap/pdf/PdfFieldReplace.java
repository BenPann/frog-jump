package com.kgi.airloanex.ap.pdf;

import java.util.List;
import java.util.Map;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.common.dto.db.DropdownData;
import com.kgi.airloanex.ap.pdf.dto.PDFCaseData;
import com.kgi.airloanex.common.PlContractConst;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Component
public class PdfFieldReplace {

	@Autowired
	private AirloanEXConfig globalConfig;

	static boolean isPersonCredit = false;
	static boolean isRecycleCredit = false;
	@SuppressWarnings("all")
	public static String contentReplacement(String str, PDFCaseData pdfCaseData, List<DropdownData> dropdownDatas,
			Map mp) {

		str = str.replace("[MemberNo]", "");
		str = str.replace("[AccountNo]", "");
		str = str.replace("[ISBN]", "");
		if (pdfCaseData.productId.equalsIgnoreCase("3")) {
			isRecycleCredit = true;
			isPersonCredit = false;
		} else {
			isRecycleCredit = false;
			isPersonCredit = true;
		}

		// 個人信用貸款checkbox
		if (isPersonCredit) { // checked
			str = str.replace("[Personal_credit_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJzSURBVGiB7Zo9SBxBGIafCImKKNyhgpIqpEshasDqRBsLG5GEYIqEVCEEQkQUKyOIRYoUdjaWgoXiIdgIFrFLJATEQoRgYwQlBk08JUT8KfY7VmW9m5n9G2VfGGa5/X7e9/ZmZ76ZgwQJEvhBLTAMLAM7wFnEbUdyDwE1piKeA7kYyF/XDoBnuiKeAqcSYBrIACndIAEgBbQCM8LlFOhWda4G/ojj+zDYGaIPh9MekFZxGMF9ErYhi8NtWMV4RYwzYTIyRBsOt+8qxgdiXBEiIVNU4nD7q2Kcf0vYCk9+JTEQCQWJENtgo5DSoALFOdhfAD+BxwVslPnFJaQFOJLcbwvYWS3kIbAreT8VsbVWSApYk5xZio9bK4XcAz5LvmWgXMHHSiETkmsTqFf0sU5Ir+TJAU0aflYJ6QCOcQqlHk3fQIWUAgvAa00SAA24K+xBA/9AhTzBLT3faZCoATbEdxK4o+Grw0/L8A1wIrYfFUhVAN/EfhG4q0LGBz+tMdID/Bf7ca6fA0pwNxDWUay5/fLTHeydwKH4TOH9TY/K/V/AA43YvviZvLUywL74zXN5Ynspn/8jmH2A0F+/jbi7kUtAFdCO+9N7ZRDTFz8/88gjYEv8vwDbcj1mGM8LkU2IdcDqhTizBFvARTqzp3GeyFfUFoI6iHyJkgLuBxDnKqxaa/lBsq91I3CrheSkt3UTG5wy4BK8hGxIr1O1RYVm6X9cveElZE76/tDomGNA+rmCVoKLR299YTEywACaR2/gfRhaFga7IijHx2FoHt3AbwofGUfZdoEuXRF5pIEPOJtn+eIpynaI+4eBOI7HEyS4NTgH3tZKOTc9lX8AAAAASUVORK5CYII=\"/>");
		} else { // unchecked
			str = str.replace("[Personal_credit_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFFSURBVGiB7ZqxSgNBFEUPVopYJKgfonXEP5CACH6FEEypAX9HIeQ3LAR7sQ8ooknaaDFvicqosxPHfcg9MLxi3+zew8IW8xaEEMuwDQyAa2AMvP7xGtuzz4CtXIljYNpA+K/WBDiqK3EIzO0Gl0AHaNW9yS/QAvaAK8syB7qpmzeBZ9t4UiJdJj1CpiegnbLhgsWb8MaQkG2Q0nxrzZ2SiTLZJ2S7SWmeWPN6wUC5bBCyvaQ0V18Jr0TzrTQQpAgS8YZEvCERb0jEGxLxhkS8IRFvSMQbEvGGRLwhEW9IxBsS8UZMZGrV6yE2hIP2D8RE7q3uFIuTz67Vu88XYiIjq6fF4uTTtzr6tst4P3rrlUqUQZ+aozeID0NXS6T7gTWWGIZWdIFHmh9LV+sBOKgrUdEGzglD+1kD4WcsfhhoYjwuxL/hDTwNt7HmQsC4AAAAAElFTkSuQmCC\"/>");
		}
		// 循環信用貸款checkbox
		if (isRecycleCredit) { // checked
			str = str.replace("[Personal_recycle_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJzSURBVGiB7Zo9SBxBGIafCImKKNyhgpIqpEshasDqRBsLG5GEYIqEVCEEQkQUKyOIRYoUdjaWgoXiIdgIFrFLJATEQoRgYwQlBk08JUT8KfY7VmW9m5n9G2VfGGa5/X7e9/ZmZ76ZgwQJEvhBLTAMLAM7wFnEbUdyDwE1piKeA7kYyF/XDoBnuiKeAqcSYBrIACndIAEgBbQCM8LlFOhWda4G/ojj+zDYGaIPh9MekFZxGMF9ErYhi8NtWMV4RYwzYTIyRBsOt+8qxgdiXBEiIVNU4nD7q2Kcf0vYCk9+JTEQCQWJENtgo5DSoALFOdhfAD+BxwVslPnFJaQFOJLcbwvYWS3kIbAreT8VsbVWSApYk5xZio9bK4XcAz5LvmWgXMHHSiETkmsTqFf0sU5Ir+TJAU0aflYJ6QCOcQqlHk3fQIWUAgvAa00SAA24K+xBA/9AhTzBLT3faZCoATbEdxK4o+Grw0/L8A1wIrYfFUhVAN/EfhG4q0LGBz+tMdID/Bf7ca6fA0pwNxDWUay5/fLTHeydwKH4TOH9TY/K/V/AA43YvviZvLUywL74zXN5Ynspn/8jmH2A0F+/jbi7kUtAFdCO+9N7ZRDTFz8/88gjYEv8vwDbcj1mGM8LkU2IdcDqhTizBFvARTqzp3GeyFfUFoI6iHyJkgLuBxDnKqxaa/lBsq91I3CrheSkt3UTG5wy4BK8hGxIr1O1RYVm6X9cveElZE76/tDomGNA+rmCVoKLR299YTEywACaR2/gfRhaFga7IijHx2FoHt3AbwofGUfZdoEuXRF5pIEPOJtn+eIpynaI+4eBOI7HEyS4NTgH3tZKOTc9lX8AAAAASUVORK5CYII=\"/>");
		} else { // unchecked
			str = str.replace("[Personal_recycle_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFFSURBVGiB7ZqxSgNBFEUPVopYJKgfonXEP5CACH6FEEypAX9HIeQ3LAR7sQ8ooknaaDFvicqosxPHfcg9MLxi3+zew8IW8xaEEMuwDQyAa2AMvP7xGtuzz4CtXIljYNpA+K/WBDiqK3EIzO0Gl0AHaNW9yS/QAvaAK8syB7qpmzeBZ9t4UiJdJj1CpiegnbLhgsWb8MaQkG2Q0nxrzZ2SiTLZJ2S7SWmeWPN6wUC5bBCyvaQ0V18Jr0TzrTQQpAgS8YZEvCERb0jEGxLxhkS8IRFvSMQbEvGGRLwhEW9IxBsS8UZMZGrV6yE2hIP2D8RE7q3uFIuTz67Vu88XYiIjq6fF4uTTtzr6tst4P3rrlUqUQZ+aozeID0NXS6T7gTWWGIZWdIFHmh9LV+sBOKgrUdEGzglD+1kD4WcsfhhoYjwuxL/hDTwNt7HmQsC4AAAAAElFTkSuQmCC\"/>");
		}
		// 申請個人信用貸款checkbox
		if (isPersonCredit) { // checked
			str = str.replace("[Apply_personal_credit_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJzSURBVGiB7Zo9SBxBGIafCImKKNyhgpIqpEshasDqRBsLG5GEYIqEVCEEQkQUKyOIRYoUdjaWgoXiIdgIFrFLJATEQoRgYwQlBk08JUT8KfY7VmW9m5n9G2VfGGa5/X7e9/ZmZ76ZgwQJEvhBLTAMLAM7wFnEbUdyDwE1piKeA7kYyF/XDoBnuiKeAqcSYBrIACndIAEgBbQCM8LlFOhWda4G/ojj+zDYGaIPh9MekFZxGMF9ErYhi8NtWMV4RYwzYTIyRBsOt+8qxgdiXBEiIVNU4nD7q2Kcf0vYCk9+JTEQCQWJENtgo5DSoALFOdhfAD+BxwVslPnFJaQFOJLcbwvYWS3kIbAreT8VsbVWSApYk5xZio9bK4XcAz5LvmWgXMHHSiETkmsTqFf0sU5Ir+TJAU0aflYJ6QCOcQqlHk3fQIWUAgvAa00SAA24K+xBA/9AhTzBLT3faZCoATbEdxK4o+Grw0/L8A1wIrYfFUhVAN/EfhG4q0LGBz+tMdID/Bf7ca6fA0pwNxDWUay5/fLTHeydwKH4TOH9TY/K/V/AA43YvviZvLUywL74zXN5Ynspn/8jmH2A0F+/jbi7kUtAFdCO+9N7ZRDTFz8/88gjYEv8vwDbcj1mGM8LkU2IdcDqhTizBFvARTqzp3GeyFfUFoI6iHyJkgLuBxDnKqxaa/lBsq91I3CrheSkt3UTG5wy4BK8hGxIr1O1RYVm6X9cveElZE76/tDomGNA+rmCVoKLR299YTEywACaR2/gfRhaFga7IijHx2FoHt3AbwofGUfZdoEuXRF5pIEPOJtn+eIpynaI+4eBOI7HEyS4NTgH3tZKOTc9lX8AAAAASUVORK5CYII=\"/>");
		} else { // unchecked
			str = str.replace("[Apply_personal_credit_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFFSURBVGiB7ZqxSgNBFEUPVopYJKgfonXEP5CACH6FEEypAX9HIeQ3LAR7sQ8ooknaaDFvicqosxPHfcg9MLxi3+zew8IW8xaEEMuwDQyAa2AMvP7xGtuzz4CtXIljYNpA+K/WBDiqK3EIzO0Gl0AHaNW9yS/QAvaAK8syB7qpmzeBZ9t4UiJdJj1CpiegnbLhgsWb8MaQkG2Q0nxrzZ2SiTLZJ2S7SWmeWPN6wUC5bBCyvaQ0V18Jr0TzrTQQpAgS8YZEvCERb0jEGxLxhkS8IRFvSMQbEvGGRLwhEW9IxBsS8UZMZGrV6yE2hIP2D8RE7q3uFIuTz67Vu88XYiIjq6fF4uTTtzr6tst4P3rrlUqUQZ+aozeID0NXS6T7gTWWGIZWdIFHmh9LV+sBOKgrUdEGzglD+1kD4WcsfhhoYjwuxL/hDTwNt7HmQsC4AAAAAElFTkSuQmCC\"/>");
		}
		// 申請個人信用貸款金額
		if (isPersonCredit) {
			str = str.replace("[Apply_personal_credit_money]", pdfCaseData.p_apy_amount + "萬元");
		} else {
			str = str.replace("[Apply_personal_credit_money]", "____________萬元");
		}
		// 申請循環信用貸款checkbox
		if (isRecycleCredit) { // checked
			str = str.replace("[Apply_personal_recycle_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJzSURBVGiB7Zo9SBxBGIafCImKKNyhgpIqpEshasDqRBsLG5GEYIqEVCEEQkQUKyOIRYoUdjaWgoXiIdgIFrFLJATEQoRgYwQlBk08JUT8KfY7VmW9m5n9G2VfGGa5/X7e9/ZmZ76ZgwQJEvhBLTAMLAM7wFnEbUdyDwE1piKeA7kYyF/XDoBnuiKeAqcSYBrIACndIAEgBbQCM8LlFOhWda4G/ojj+zDYGaIPh9MekFZxGMF9ErYhi8NtWMV4RYwzYTIyRBsOt+8qxgdiXBEiIVNU4nD7q2Kcf0vYCk9+JTEQCQWJENtgo5DSoALFOdhfAD+BxwVslPnFJaQFOJLcbwvYWS3kIbAreT8VsbVWSApYk5xZio9bK4XcAz5LvmWgXMHHSiETkmsTqFf0sU5Ir+TJAU0aflYJ6QCOcQqlHk3fQIWUAgvAa00SAA24K+xBA/9AhTzBLT3faZCoATbEdxK4o+Grw0/L8A1wIrYfFUhVAN/EfhG4q0LGBz+tMdID/Bf7ca6fA0pwNxDWUay5/fLTHeydwKH4TOH9TY/K/V/AA43YvviZvLUywL74zXN5Ynspn/8jmH2A0F+/jbi7kUtAFdCO+9N7ZRDTFz8/88gjYEv8vwDbcj1mGM8LkU2IdcDqhTizBFvARTqzp3GeyFfUFoI6iHyJkgLuBxDnKqxaa/lBsq91I3CrheSkt3UTG5wy4BK8hGxIr1O1RYVm6X9cveElZE76/tDomGNA+rmCVoKLR299YTEywACaR2/gfRhaFga7IijHx2FoHt3AbwofGUfZdoEuXRF5pIEPOJtn+eIpynaI+4eBOI7HEyS4NTgH3tZKOTc9lX8AAAAASUVORK5CYII=\"/>");
		} else { // unchecked
			str = str.replace("[Apply_personal_recycle_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFFSURBVGiB7ZqxSgNBFEUPVopYJKgfonXEP5CACH6FEEypAX9HIeQ3LAR7sQ8ooknaaDFvicqosxPHfcg9MLxi3+zew8IW8xaEEMuwDQyAa2AMvP7xGtuzz4CtXIljYNpA+K/WBDiqK3EIzO0Gl0AHaNW9yS/QAvaAK8syB7qpmzeBZ9t4UiJdJj1CpiegnbLhgsWb8MaQkG2Q0nxrzZ2SiTLZJ2S7SWmeWPN6wUC5bBCyvaQ0V18Jr0TzrTQQpAgS8YZEvCERb0jEGxLxhkS8IRFvSMQbEvGGRLwhEW9IxBsS8UZMZGrV6yE2hIP2D8RE7q3uFIuTz67Vu88XYiIjq6fF4uTTtzr6tst4P3rrlUqUQZ+aozeID0NXS6T7gTWWGIZWdIFHmh9LV+sBOKgrUdEGzglD+1kD4WcsfhhoYjwuxL/hDTwNt7HmQsC4AAAAAElFTkSuQmCC\"/>");
		}
		// 申請循環信用貸款金額
		if (isRecycleCredit) {
			str = str.replace("[Apply_personal_recycle_money]", pdfCaseData.p_apy_amount + "萬元");
		} else {
			str = str.replace("[Apply_personal_recycle_money]", "____________萬元");
		}
		// str = str.replace("[Loan_period]", "_______"); //貸款期間(年)
		// str = str.replace("[Capital_use]", ""); //資金用途
		// str = str.replace("[Apply_user_name]", ""); //申請人姓名
		// str = str.replace("[Apply_user_gender]", ""); //性別
		// str = str.replace("[Apply_user_company]", ""); //公司名稱
		// //出生日期
		// str = str.replace("[Apply_user_birth]",
		// "民國&nbsp;&nbsp;年&nbsp;&nbsp;月&nbsp;&nbsp;日");
		// str = str.replace("[Apply_user_marriage]", ""); //婚姻
		// //公司電話
		// str = str.replace("[Apply_user_company_phone]",
		// "(&nbsp;&nbsp;&nbsp;)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;分機");
		// str = str.replace("[Apply_user_identity_code]", ""); //身分證統一編號
		// str = str.replace("[Apply_user_company_address]", ""); //公司地址
		// str = str.replace("[Apply_user_education]", ""); //教育程度
		// str = str.replace("[Apply_user_annual_income]", "________萬元"); //年收入
		// //到職年月
		// str = str.replace("[Apply_user_first_day_of_work]",
		// "民國&nbsp;&nbsp;&nbsp;年&nbsp;&nbsp;&nbsp;月");
		// str = str.replace("[Apply_user_email]", ""); //電子信箱
		// str = str.replace("[Apply_user_job_title]", ""); //職稱
		// str = str.replace("[Apply_user_real estate]", ""); //不動產狀況
		// str = str.replace("[Apply_user_mobile_number]", ""); //行動電話
		// str = str.replace("[Apply_user_address]", ""); //現居地址
		// str = str.replace("[Apply_user_home_phone]", ""); //居住電話
		// str = str.replace("[Apply_user_project_code]", ""); //專案代碼
		// //申請日期
		// str = str.replace("[Apply_date]",
		// "&nbsp;&nbsp;&nbsp;年&nbsp;&nbsp;&nbsp;月&nbsp;&nbsp;&nbsp;日");

		str = str.replace("[Loan_period]",
				(pdfCaseData.p_period == 0) ? "" : String.valueOf(pdfCaseData.p_period) + "年"); // 貸款期間(年)
		boolean noCapitaluse = true;
		for (DropdownData dropdownData : dropdownDatas) {

			if (dropdownData.getName().equalsIgnoreCase("moneypurpose")
					&& dropdownData.getDataKey().equalsIgnoreCase(String.valueOf(pdfCaseData.p_purpose))) {
				// 資金用途
				// 選其他時，套上其他說明欄位
				if (dropdownData.getDataKey().equalsIgnoreCase("16")) {
					str = str.replace("[Capital_use]", pdfCaseData.p_purpose_name);
				} else {
					str = str.replace("[Capital_use]", dropdownData.getDataName());
				}
				noCapitaluse = false;
				break;
			}
		}
		if (noCapitaluse) {
			str = str.replace("[Capital_use]", "");
		}
		str = str.replace("[Apply_user_name]", pdfCaseData.customer_name); // 申請人姓名
		str = str.replace("[Apply_user_gender]", (pdfCaseData.gender.equalsIgnoreCase("1")) ? "男" : "女"); // 性別
		str = str.replace("[Apply_user_company]", pdfCaseData.corp_name); // 公司名稱
		// 出生日期
		if (!pdfCaseData.birthday.isEmpty()) {
			if (pdfCaseData.birthday.length() == 8)
				str = str.replace("[Apply_user_birth]",
						"民國" + String.valueOf((Integer.parseInt(pdfCaseData.birthday.substring(0, 4)) - 1911)) + "年"
								+ pdfCaseData.birthday.substring(4, 6) + "月" + pdfCaseData.birthday.substring(6, 8)
								+ "日");
			else
				str = str.replace("[Apply_user_birth]", "民國____年____月____日");
		}

		for (DropdownData dropdownData : dropdownDatas) {
			if (dropdownData.getName().equalsIgnoreCase("marriage")
					&& dropdownData.getDataKey().equalsIgnoreCase(String.valueOf(pdfCaseData.marriage))) {
				str = str.replace("[Apply_user_marriage]", dropdownData.getDataName()); // 婚姻
				break;
			}
		}
		// 公司電話
		str = str.replace("[Apply_user_company_phone]", "(" + pdfCaseData.corp_tel_area.trim() + ")"
				+ pdfCaseData.corp_tel.trim() + " 分機:" + pdfCaseData.corp_tel_exten);
		str = str.replace("[Apply_user_identity_code]", pdfCaseData.idno); // 身分證統一編號
		str = str.replace("[Apply_user_company_address]", pdfCaseData.corp_city_code.concat(pdfCaseData.corp_address)); // 公司地址(含郵遞區碼)
		for (DropdownData dropdownData : dropdownDatas) {
			if (dropdownData.getName().equalsIgnoreCase("education")
					&& dropdownData.getDataKey().equalsIgnoreCase(String.valueOf(pdfCaseData.education))) {
				str = str.replace("[Apply_user_education]", dropdownData.getDataName()); // 教育程度
				break;
			}
		}
		str = str.replace("[Apply_user_annual_income]", String.valueOf(pdfCaseData.yearly_income) + "萬元"); // 年收入
		// 到職年月
		if (!pdfCaseData.on_board_date.isEmpty()) {
			if (pdfCaseData.on_board_date.length() == 8)
				str = str.replace("[Apply_user_first_day_of_work]",
						"民國" + String.valueOf((Integer.parseInt(pdfCaseData.on_board_date.substring(0, 4)) - 1911))
								+ "年" + pdfCaseData.on_board_date.substring(4, 6) + "月");
			else
				str = str.replace("[Apply_user_first_day_of_work]", "民國____年____月");
		}
		str = str.replace("[Apply_user_email]", pdfCaseData.email_address); // 電子信箱
		str = str.replace("[Apply_user_job_title]", pdfCaseData.title); // 職稱
		for (DropdownData dropdownData : dropdownDatas) {
			if (dropdownData.getName().equalsIgnoreCase("estate")
					&& dropdownData.getDataKey().equalsIgnoreCase(String.valueOf(pdfCaseData.estate_type))) {
				str = str.replace("[Apply_user_real_estate]", dropdownData.getDataName()); // 不動產狀況
				break;
			}
		}
		str = str.replace("[Apply_user_mobile_number]", pdfCaseData.mobile_tel); // 行動電話
		str = str.replace("[Apply_user_address]", pdfCaseData.house_city_code.concat(pdfCaseData.house_address)); // 現居地址
		str = str.replace("[Apply_user_home_phone]",
				"(" + pdfCaseData.house_tel_area.trim() + ")" + pdfCaseData.house_tel); // 居住電話(含郵遞區碼)
		str = str.replace("[Apply_user_project_code]", pdfCaseData.prj_code); // 專案代碼
		// 申請日期
		// String applydate = DateUtil.GetDateFormatString();
		String applydate = pdfCaseData.CaseNo.substring(4, 12);

		str = str.replace("[Apply_date]",
				applydate.substring(0, 4) + "年" + applydate.substring(4, 6) + "月" + applydate.substring(6, 8) + "日");
		// str = str.replace("[Apply_date]", "____年____月____日");

		// 申請人ip address
		str = str.replace("[ip_address]", pdfCaseData.ip_address);
		str = str.replace("[Promo_Depart]",
				mp.get("TransDepart").toString() != "" ? mp.get("TransDepart").toString() : "");
		str = str.replace("[Promo_Employee]",
				mp.get("TransMember").toString() != "" ? mp.get("TransMember").toString() : "");
		str = str.replace("[Trans_Depart]",
				mp.get("PromoDepart").toString() != "" ? mp.get("PromoDepart").toString() : "");
		str = str.replace("[Trans_Employee]",
				mp.get("PromoMember").toString() != "" ? mp.get("PromoMember").toString() : "");

		return str;
	}

	public static String rejectContentReplacement(String str, JSONObject pdfCaseData,
			List<DropdownData> dropdownDatas) {

		str = str.replace("[MemberNo]", "");
		str = str.replace("[AccountNo]", "");
		str = str.replace("[ISBN]", "");

		if (pdfCaseData.optString("ProductId").equalsIgnoreCase("3")) {
			isRecycleCredit = true;
			isPersonCredit = false;
		} else {
			isRecycleCredit = false;
			isPersonCredit = true;
		}

		// 個人信用貸款checkbox
		if (isPersonCredit) { // checked
			str = str.replace("[Personal_credit_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJzSURBVGiB7Zo9SBxBGIafCImKKNyhgpIqpEshasDqRBsLG5GEYIqEVCEEQkQUKyOIRYoUdjaWgoXiIdgIFrFLJATEQoRgYwQlBk08JUT8KfY7VmW9m5n9G2VfGGa5/X7e9/ZmZ76ZgwQJEvhBLTAMLAM7wFnEbUdyDwE1piKeA7kYyF/XDoBnuiKeAqcSYBrIACndIAEgBbQCM8LlFOhWda4G/ojj+zDYGaIPh9MekFZxGMF9ErYhi8NtWMV4RYwzYTIyRBsOt+8qxgdiXBEiIVNU4nD7q2Kcf0vYCk9+JTEQCQWJENtgo5DSoALFOdhfAD+BxwVslPnFJaQFOJLcbwvYWS3kIbAreT8VsbVWSApYk5xZio9bK4XcAz5LvmWgXMHHSiETkmsTqFf0sU5Ir+TJAU0aflYJ6QCOcQqlHk3fQIWUAgvAa00SAA24K+xBA/9AhTzBLT3faZCoATbEdxK4o+Grw0/L8A1wIrYfFUhVAN/EfhG4q0LGBz+tMdID/Bf7ca6fA0pwNxDWUay5/fLTHeydwKH4TOH9TY/K/V/AA43YvviZvLUywL74zXN5Ynspn/8jmH2A0F+/jbi7kUtAFdCO+9N7ZRDTFz8/88gjYEv8vwDbcj1mGM8LkU2IdcDqhTizBFvARTqzp3GeyFfUFoI6iHyJkgLuBxDnKqxaa/lBsq91I3CrheSkt3UTG5wy4BK8hGxIr1O1RYVm6X9cveElZE76/tDomGNA+rmCVoKLR299YTEywACaR2/gfRhaFga7IijHx2FoHt3AbwofGUfZdoEuXRF5pIEPOJtn+eIpynaI+4eBOI7HEyS4NTgH3tZKOTc9lX8AAAAASUVORK5CYII=\"/>");
		} else { // unchecked
			str = str.replace("[Personal_credit_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFFSURBVGiB7ZqxSgNBFEUPVopYJKgfonXEP5CACH6FEEypAX9HIeQ3LAR7sQ8ooknaaDFvicqosxPHfcg9MLxi3+zew8IW8xaEEMuwDQyAa2AMvP7xGtuzz4CtXIljYNpA+K/WBDiqK3EIzO0Gl0AHaNW9yS/QAvaAK8syB7qpmzeBZ9t4UiJdJj1CpiegnbLhgsWb8MaQkG2Q0nxrzZ2SiTLZJ2S7SWmeWPN6wUC5bBCyvaQ0V18Jr0TzrTQQpAgS8YZEvCERb0jEGxLxhkS8IRFvSMQbEvGGRLwhEW9IxBsS8UZMZGrV6yE2hIP2D8RE7q3uFIuTz67Vu88XYiIjq6fF4uTTtzr6tst4P3rrlUqUQZ+aozeID0NXS6T7gTWWGIZWdIFHmh9LV+sBOKgrUdEGzglD+1kD4WcsfhhoYjwuxL/hDTwNt7HmQsC4AAAAAElFTkSuQmCC\"/>");
		}
		// 循環信用貸款checkbox
		if (isRecycleCredit) { // checked
			str = str.replace("[Personal_recycle_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJzSURBVGiB7Zo9SBxBGIafCImKKNyhgpIqpEshasDqRBsLG5GEYIqEVCEEQkQUKyOIRYoUdjaWgoXiIdgIFrFLJATEQoRgYwQlBk08JUT8KfY7VmW9m5n9G2VfGGa5/X7e9/ZmZ76ZgwQJEvhBLTAMLAM7wFnEbUdyDwE1piKeA7kYyF/XDoBnuiKeAqcSYBrIACndIAEgBbQCM8LlFOhWda4G/ojj+zDYGaIPh9MekFZxGMF9ErYhi8NtWMV4RYwzYTIyRBsOt+8qxgdiXBEiIVNU4nD7q2Kcf0vYCk9+JTEQCQWJENtgo5DSoALFOdhfAD+BxwVslPnFJaQFOJLcbwvYWS3kIbAreT8VsbVWSApYk5xZio9bK4XcAz5LvmWgXMHHSiETkmsTqFf0sU5Ir+TJAU0aflYJ6QCOcQqlHk3fQIWUAgvAa00SAA24K+xBA/9AhTzBLT3faZCoATbEdxK4o+Grw0/L8A1wIrYfFUhVAN/EfhG4q0LGBz+tMdID/Bf7ca6fA0pwNxDWUay5/fLTHeydwKH4TOH9TY/K/V/AA43YvviZvLUywL74zXN5Ynspn/8jmH2A0F+/jbi7kUtAFdCO+9N7ZRDTFz8/88gjYEv8vwDbcj1mGM8LkU2IdcDqhTizBFvARTqzp3GeyFfUFoI6iHyJkgLuBxDnKqxaa/lBsq91I3CrheSkt3UTG5wy4BK8hGxIr1O1RYVm6X9cveElZE76/tDomGNA+rmCVoKLR299YTEywACaR2/gfRhaFga7IijHx2FoHt3AbwofGUfZdoEuXRF5pIEPOJtn+eIpynaI+4eBOI7HEyS4NTgH3tZKOTc9lX8AAAAASUVORK5CYII=\"/>");
		} else { // unchecked
			str = str.replace("[Personal_recycle_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFFSURBVGiB7ZqxSgNBFEUPVopYJKgfonXEP5CACH6FEEypAX9HIeQ3LAR7sQ8ooknaaDFvicqosxPHfcg9MLxi3+zew8IW8xaEEMuwDQyAa2AMvP7xGtuzz4CtXIljYNpA+K/WBDiqK3EIzO0Gl0AHaNW9yS/QAvaAK8syB7qpmzeBZ9t4UiJdJj1CpiegnbLhgsWb8MaQkG2Q0nxrzZ2SiTLZJ2S7SWmeWPN6wUC5bBCyvaQ0V18Jr0TzrTQQpAgS8YZEvCERb0jEGxLxhkS8IRFvSMQbEvGGRLwhEW9IxBsS8UZMZGrV6yE2hIP2D8RE7q3uFIuTz67Vu88XYiIjq6fF4uTTtzr6tst4P3rrlUqUQZ+aozeID0NXS6T7gTWWGIZWdIFHmh9LV+sBOKgrUdEGzglD+1kD4WcsfhhoYjwuxL/hDTwNt7HmQsC4AAAAAElFTkSuQmCC\"/>");
		}
		// 申請個人信用貸款checkbox
		if (isPersonCredit) { // checked
			str = str.replace("[Apply_personal_credit_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJzSURBVGiB7Zo9SBxBGIafCImKKNyhgpIqpEshasDqRBsLG5GEYIqEVCEEQkQUKyOIRYoUdjaWgoXiIdgIFrFLJATEQoRgYwQlBk08JUT8KfY7VmW9m5n9G2VfGGa5/X7e9/ZmZ76ZgwQJEvhBLTAMLAM7wFnEbUdyDwE1piKeA7kYyF/XDoBnuiKeAqcSYBrIACndIAEgBbQCM8LlFOhWda4G/ojj+zDYGaIPh9MekFZxGMF9ErYhi8NtWMV4RYwzYTIyRBsOt+8qxgdiXBEiIVNU4nD7q2Kcf0vYCk9+JTEQCQWJENtgo5DSoALFOdhfAD+BxwVslPnFJaQFOJLcbwvYWS3kIbAreT8VsbVWSApYk5xZio9bK4XcAz5LvmWgXMHHSiETkmsTqFf0sU5Ir+TJAU0aflYJ6QCOcQqlHk3fQIWUAgvAa00SAA24K+xBA/9AhTzBLT3faZCoATbEdxK4o+Grw0/L8A1wIrYfFUhVAN/EfhG4q0LGBz+tMdID/Bf7ca6fA0pwNxDWUay5/fLTHeydwKH4TOH9TY/K/V/AA43YvviZvLUywL74zXN5Ynspn/8jmH2A0F+/jbi7kUtAFdCO+9N7ZRDTFz8/88gjYEv8vwDbcj1mGM8LkU2IdcDqhTizBFvARTqzp3GeyFfUFoI6iHyJkgLuBxDnKqxaa/lBsq91I3CrheSkt3UTG5wy4BK8hGxIr1O1RYVm6X9cveElZE76/tDomGNA+rmCVoKLR299YTEywACaR2/gfRhaFga7IijHx2FoHt3AbwofGUfZdoEuXRF5pIEPOJtn+eIpynaI+4eBOI7HEyS4NTgH3tZKOTc9lX8AAAAASUVORK5CYII=\"/>");
		} else { // unchecked
			str = str.replace("[Apply_personal_credit_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFFSURBVGiB7ZqxSgNBFEUPVopYJKgfonXEP5CACH6FEEypAX9HIeQ3LAR7sQ8ooknaaDFvicqosxPHfcg9MLxi3+zew8IW8xaEEMuwDQyAa2AMvP7xGtuzz4CtXIljYNpA+K/WBDiqK3EIzO0Gl0AHaNW9yS/QAvaAK8syB7qpmzeBZ9t4UiJdJj1CpiegnbLhgsWb8MaQkG2Q0nxrzZ2SiTLZJ2S7SWmeWPN6wUC5bBCyvaQ0V18Jr0TzrTQQpAgS8YZEvCERb0jEGxLxhkS8IRFvSMQbEvGGRLwhEW9IxBsS8UZMZGrV6yE2hIP2D8RE7q3uFIuTz67Vu88XYiIjq6fF4uTTtzr6tst4P3rrlUqUQZ+aozeID0NXS6T7gTWWGIZWdIFHmh9LV+sBOKgrUdEGzglD+1kD4WcsfhhoYjwuxL/hDTwNt7HmQsC4AAAAAElFTkSuQmCC\"/>");
		}
		// 申請個人信用貸款金額
		if (isPersonCredit) {
			str = str.replace("[Apply_personal_credit_money]", pdfCaseData.optString("p_apy_amount") + "萬元");
		} else {
			str = str.replace("[Apply_personal_credit_money]", "____________萬元");
		}
		// 申請循環信用貸款checkbox
		if (isRecycleCredit) { // checked
			str = str.replace("[Apply_personal_recycle_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJzSURBVGiB7Zo9SBxBGIafCImKKNyhgpIqpEshasDqRBsLG5GEYIqEVCEEQkQUKyOIRYoUdjaWgoXiIdgIFrFLJATEQoRgYwQlBk08JUT8KfY7VmW9m5n9G2VfGGa5/X7e9/ZmZ76ZgwQJEvhBLTAMLAM7wFnEbUdyDwE1piKeA7kYyF/XDoBnuiKeAqcSYBrIACndIAEgBbQCM8LlFOhWda4G/ojj+zDYGaIPh9MekFZxGMF9ErYhi8NtWMV4RYwzYTIyRBsOt+8qxgdiXBEiIVNU4nD7q2Kcf0vYCk9+JTEQCQWJENtgo5DSoALFOdhfAD+BxwVslPnFJaQFOJLcbwvYWS3kIbAreT8VsbVWSApYk5xZio9bK4XcAz5LvmWgXMHHSiETkmsTqFf0sU5Ir+TJAU0aflYJ6QCOcQqlHk3fQIWUAgvAa00SAA24K+xBA/9AhTzBLT3faZCoATbEdxK4o+Grw0/L8A1wIrYfFUhVAN/EfhG4q0LGBz+tMdID/Bf7ca6fA0pwNxDWUay5/fLTHeydwKH4TOH9TY/K/V/AA43YvviZvLUywL74zXN5Ynspn/8jmH2A0F+/jbi7kUtAFdCO+9N7ZRDTFz8/88gjYEv8vwDbcj1mGM8LkU2IdcDqhTizBFvARTqzp3GeyFfUFoI6iHyJkgLuBxDnKqxaa/lBsq91I3CrheSkt3UTG5wy4BK8hGxIr1O1RYVm6X9cveElZE76/tDomGNA+rmCVoKLR299YTEywACaR2/gfRhaFga7IijHx2FoHt3AbwofGUfZdoEuXRF5pIEPOJtn+eIpynaI+4eBOI7HEyS4NTgH3tZKOTc9lX8AAAAASUVORK5CYII=\"/>");
		} else { // unchecked
			str = str.replace("[Apply_personal_recycle_checkbox]",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFFSURBVGiB7ZqxSgNBFEUPVopYJKgfonXEP5CACH6FEEypAX9HIeQ3LAR7sQ8ooknaaDFvicqosxPHfcg9MLxi3+zew8IW8xaEEMuwDQyAa2AMvP7xGtuzz4CtXIljYNpA+K/WBDiqK3EIzO0Gl0AHaNW9yS/QAvaAK8syB7qpmzeBZ9t4UiJdJj1CpiegnbLhgsWb8MaQkG2Q0nxrzZ2SiTLZJ2S7SWmeWPN6wUC5bBCyvaQ0V18Jr0TzrTQQpAgS8YZEvCERb0jEGxLxhkS8IRFvSMQbEvGGRLwhEW9IxBsS8UZMZGrV6yE2hIP2D8RE7q3uFIuTz67Vu88XYiIjq6fF4uTTtzr6tst4P3rrlUqUQZ+aozeID0NXS6T7gTWWGIZWdIFHmh9LV+sBOKgrUdEGzglD+1kD4WcsfhhoYjwuxL/hDTwNt7HmQsC4AAAAAElFTkSuQmCC\"/>");
		}
		// 申請循環信用貸款金額
		if (isRecycleCredit) {
			str = str.replace("[Apply_personal_recycle_money]", pdfCaseData.optString("p_apy_amount", "0") + "萬元");
		} else {
			str = str.replace("[Apply_personal_recycle_money]", "____________萬元");
		}
		str = str.replace("[Loan_period]", ""); // 貸款期間(年)
		str = str.replace("[Capital_use]", ""); // 資金用途

		str = str.replace("[Apply_user_name]", pdfCaseData.optString("customer_name", "")); // 申請人姓名
		str = str.replace("[Apply_user_gender]", ""); // 性別
		str = str.replace("[Apply_user_company]", ""); // 公司名稱
		// 出生日期
		if (!pdfCaseData.optString("birthday").isEmpty()) {
			if (pdfCaseData.optString("birthday").length() == 8)
				str = str.replace("[Apply_user_birth]",
						"民國" + String
								.valueOf((Integer.parseInt(pdfCaseData.optString("birthday").substring(0, 4)) - 1911))
								+ "年" + pdfCaseData.optString("birthday").substring(4, 6) + "月"
								+ pdfCaseData.optString("birthday").substring(6, 8) + "日");
			else
				str = str.replace("[Apply_user_birth]", "民國____年____月____日");
		}
		str = str.replace("[Apply_user_marriage]", ""); // 婚姻

		// 公司電話
		str = str.replace("[Apply_user_company_phone]", "");
		str = str.replace("[Apply_user_identity_code]", pdfCaseData.optString("Idno", "")); // 身分證統一編號
		str = str.replace("[Apply_user_company_address]", ""); // 公司地址(含郵遞區碼)
		str = str.replace("[Apply_user_education]", ""); // 教育程度

		str = str.replace("[Apply_user_annual_income]", ""); // 年收入
		str = str.replace("[Apply_user_first_day_of_work]", "");

		str = str.replace("[Apply_user_email]", ""); // 電子信箱
		str = str.replace("[Apply_user_job_title]", ""); // 職稱
		str = str.replace("[Apply_user_real_estate]", ""); // 不動產狀況

		str = str.replace("[Apply_user_mobile_number]", pdfCaseData.optString("mobile_tel", "")); // 行動電話
		str = str.replace("[Apply_user_address]", ""); // 現居地址
		str = str.replace("[Apply_user_home_phone]", ""); // 居住電話(含郵遞區碼)
		str = str.replace("[Apply_user_project_code]", ""); // 專案代碼
		// 申請日期
		// String applydate = DateUtil.GetDateFormatString("yyyyMMdd");
		String applydate = pdfCaseData.optString("CaseNo").substring(4, 12);

		str = str.replace("[Apply_date]",
				applydate.substring(0, 4) + "年" + applydate.substring(4, 6) + "月" + applydate.substring(6, 8) + "日");
		// str = str.replace("[Apply_date]", "____年____月____日");

		// 申請人ip address
		str = str.replace("[ip_address]", pdfCaseData.optString("ip_address"));
		return str;
	}

	// public static String contractContentReplacement(String str, String userSign) {
	// 	try {
	// 		String StaticFilePath = globalConfig.StaticFilePath;
	// 		String imageUrl = StaticFilePath + "/image/BankSeal.base64";
	// 		File file = new File(imageUrl);
	// 		String contentBase64 = new String(Files.readAllBytes(file.toPath()));
	// 		str = str.replace("<div style=\"display: none;\">[Bank_seal]</div>",
	// 				"<img src=\"data:image/jpeg;base64," + contentBase64 + "\" " + "alt=\"\" style=\"width:120px;\" />");
	// 		str = str.replace("%user_cname%", userSign);
	// 	} catch (Exception e) {

	// 	}
	// 	return str;
	// }

	public String contractHtmlReplacement(String str, String custinfo) {

		try {
			JSONObject r = JSONObject.fromObject(custinfo);
			String productId = r.optString("productId", "1");

			str = str.replace("%user_cname%", r.optString("name", ""));
			String readDate = r.optString("readDate", "19900101");
			str = str.replace("%agrDateYY%", String.valueOf((Integer.parseInt(readDate.substring(0, 4)) - 1911)));
			str = str.replace("%agrDateMM%", readDate.substring(5, 7));
			str = str.replace("%agrDateDD%", readDate.substring(8, 10));

			str = str.replace("%aprove_amount%", r.optString("aprove_amount", ""));
			// 期間
			str = str.replace("%aprove_period%", r.optString("aprove_period", "1"));
			int LoanEndYear;
			// 當數位信貸時撥貸期間為paydate與aprove_period相加，其餘產品為paydate加一年
			if (productId.equals(PlContractConst.CONTRACT_PRODUCT_ID_TYPE_LOAN)) {
				LoanEndYear = Integer.valueOf(r.optString("pay_date").substring(0, 4)) + r.optInt("aprove_period", 1);
			} else {
				LoanEndYear = Integer.valueOf(r.optString("pay_date").substring(0, 4)) + 1;
			}

			str = str.replace("%LoanBeginYear%",
					String.valueOf((Integer.parseInt(r.optString("pay_date").substring(0, 4)) - 1911)));
			str = str.replace("%LoanBeginMonth%", r.optString("pay_date").substring(4, 6));
			str = str.replace("%LoanBeginDay%", r.optString("pay_date").substring(6, 8));
			str = str.replace("%LoanEndYear%", String.valueOf((LoanEndYear - 1911)));
			str = str.replace("%LoanEndMonth%", r.optString("pay_date").substring(4, 6));
			str = str.replace("%LoanEndDay%", r.optString("pay_date").substring(6, 8));

			str = str.replace("%rate_desc%", r.optString("rate_desc", ""));
			str = str.replace("%pay_desc%", r.optString("pay_desc", ""));
			str = str.replace("%pay_kind%", r.optString("pay_kind", ""));
			str = str.replace("%pay_way%", r.optString("pay_way", ""));
			str = str.replace("%notify%", r.optString("notify", "").equals("01") ? "簡訊" : "書面");
			str = str.replace("%apy_fee_kind%", r.optString("apy_fee_kind", ""));
			str = str.replace("%sum_rate%", r.optString("sum_rate", ""));
			str = str.replace("%pen_fee_kind%", r.optString("pen_fee_kind", ""));
			str = str.replace("%agrService%", "同意");
			str = str.replace("%districtCourt%", r.optString("districtCourt", ""));
			str = str.replace("%idNo%", r.optString("idNo", ""));

			str = str.replace("%pay_cashcard_num%", r.optString("pay_cashcard_num", ""));
			str = str.replace("%apply_credit_line%", r.optString("apply_credit_line", ""));
			str = str.replace("%p_loan_no%", r.optString("p_loan_no", ""));
			str = str.replace("%apy_fee%", r.optString("apy_fee", ""));
			str = str.replace("%pay_kind_day%", r.optString("pay_kind_day", ""));
			str = str.replace("%payWayFlag%", r.optString("payWayFlag", ""));
			str = str.replace("%agrService2%", "同意");
			str = str.replace("%member_no%", r.optString("member_no", ""));
			String contractdate = r.optString("contractDate", "");
			if (contractdate.length() > 0) {
				str = str.replace("%ROCyear%", String.valueOf((Integer.parseInt(contractdate.substring(0, 4)) - 1911)));
				str = str.replace("%year%", String.valueOf((Integer.parseInt(contractdate.substring(0, 4)) - 1911)));
				str = str.replace("%month%", contractdate.substring(4, 6));
				str = str.replace("%day%", contractdate.substring(6, 8));
				str = str.replace("%hour%", contractdate.substring(8, 10));
				str = str.replace("%minute%", contractdate.substring(10, 12));
			} else {
				str = str.replace("%ROCyear%", "　");
				str = str.replace("%year%", "　");
				str = str.replace("%month%", "　");
				str = str.replace("%day%", "　");
				str = str.replace("%hour%", "　");
				str = str.replace("%minute%", "　");
			}

			str = str.replace("%idPass%", "OTP認證");
			str = str.replace("%IPAddress%", r.optString("ipAddress", ""));
			
			// 指定銀行名單
			String paybank = r.optString("paybank", "");
			if (paybank.equals("")) {
				str = str.replace("%sTable%", "");
			} else {
				JSONArray constArr = JSONArray.fromObject(paybank);
				String table = "";
				for (int i = 0; i < constArr.size(); i++) {
					JSONObject bank = constArr.getJSONObject(i);
					table = table.concat("<tr>");
					table = table.concat(
							"<td>" + bank.optString("pay_bank", "").replace("NULL", "&nbsp;").replace("null", "&nbsp;")
									+ "</td>");

					table = table.concat("<td>"
							+ bank.optString("pay_account", "").replace("NULL", "&nbsp;").replace("null", "&nbsp;")
							+ "</td>");
					table = table.concat("</tr>");
				}
				str = str.replace("%sTable%", table);
			}
			// 專案者親簽flag
			String signflg = r.optString("sign_flg", "N");
			if (signflg.equals("N")) {
				// 則不印上user簽名
				str = str.replace("<div style=\"display: none;\">[usersign]</div>(親簽)<b>「如選擇本條第一項第(六)款述專案者請記得親簽」</b>",
						"(親簽)<b>「如選擇本條第一項第(六)款述專案者請記得親簽」</b>");
			}

		} catch (Exception e) {

		}
		return str;
	}
}
