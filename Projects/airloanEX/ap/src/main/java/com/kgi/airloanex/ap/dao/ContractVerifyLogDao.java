package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.ContractVerifyLog;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class ContractVerifyLogDao extends CRUDQDao<ContractVerifyLog> {
    
    @Override
    public int Create(ContractVerifyLog fullItem) {
        String sql = "INSERT INTO ContractVerifyLog (UniqId,VerifyType,Count,Request,Response,CheckCode,AuthCode,Other,Status,CreateTime) "
                  +" values(:UniqId,:VerifyType,:Count,:Request,:Response,:CheckCode,:AuthCode,:Other,:Status,GETDATE())";
        return this.getNamedParameterJdbcTemplate().update(sql,new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public ContractVerifyLog Read(ContractVerifyLog keyItem) {
        String sql = "SELECT * FROM ContractVerifyLog WHERE UniqId = ? AND VerifyType = ? AND Count = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(ContractVerifyLog.class),
                    new Object[] { keyItem.getUniqId(), keyItem.getVerifyType(), keyItem.getCount() });
        } catch (DataAccessException ex) {
            logger.error("ContractVerifyLog查無資料");                        
            return null;
        }
    }

    @Override
    public int Update(ContractVerifyLog fullItem) {
        String sql = "UPDATE ContractVerifyLog SET Request = ?, Response = ?, CheckCode = ?, AuthCode = ?, Other = ?, Status = ? WHERE UniqId = ? AND VerifyType = ? AND Count = ?";
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getRequest(), fullItem.getResponse(), fullItem.getCheckCode(),
                        fullItem.getAuthCode(), fullItem.getOther(), fullItem.getStatus(), fullItem.getUniqId(), fullItem.getVerifyType(),
                        fullItem.getCount() });
    }

    @Override
    public int Delete(ContractVerifyLog keyItem) {
        String sql = "DELETE ContractVerifyLog WHERE UniqId = ? AND VerifyType = ? AND Count = ?";
        return this.getJdbcTemplate().update(sql,
                new Object[] { keyItem.getUniqId(), keyItem.getVerifyType(), keyItem.getCount() });
    }

    @Override
    public List<ContractVerifyLog> Query(ContractVerifyLog keyItem) {
        String sql = "SELECT * FROM ContractVerifyLog WHERE UniqId = ? AND VerifyType = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getUniqId(), keyItem.getVerifyType() },
                    new BeanPropertyRowMapper<>(ContractVerifyLog.class));
        } catch (DataAccessException ex) {
            return null;
        }

    }
}