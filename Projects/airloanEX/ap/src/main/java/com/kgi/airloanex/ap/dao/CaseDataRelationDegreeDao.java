package com.kgi.airloanex.ap.dao;

import java.util.ArrayList;
import java.util.List;

import com.kgi.airloanex.common.dto.db.CaseDataRelationDegree;
import com.kgi.airloanex.common.dto.view.ApplyRelationDegreeCorp;
import com.kgi.airloanex.common.dto.view.ApplyRelationDegreeView;
import com.kgi.airloanex.common.dto.view.ApplySecondDegree;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

/**
 * 同一關係人表 Dao
 */
@Repository
public class CaseDataRelationDegreeDao extends CRUDQDao<CaseDataRelationDegree> {
    public static final String NAME = "CaseDataRelationDegree";

    @Override
    public int Create(CaseDataRelationDegree fullItem) {
        String sql = "INSERT INTO " + NAME
        + " ( UniqId, RelationDegree, Idno, Name, CorpName, CorpNumber, CorpJobTitle, Memo)"
 + " VALUES (:UniqId,:RelationDegree,:Idno,:Name,:CorpName,:CorpNumber,:CorpJobTitle,:Memo)";

        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public CaseDataRelationDegree Read(CaseDataRelationDegree keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE UniqId = ? AND Idno = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseDataRelationDegree.class),
                    new Object[] { keyItem.getUniqId(), keyItem.getIdno() });
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料");
            return null;
        }
    }

    @Override
    public int Update(CaseDataRelationDegree fullItem) {
        String sql = "UPDATE " + NAME
                + " SET RelationDegree = :RelationDegree, Name = :Name, CorpName = :CorpName, CorpNumber = :CorpNumber, CorpJobTitle = :CorpJobTitle, Memo = :Memo"
                + " WHERE UniqId = :UniqId AND Idno = :Idno";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(CaseDataRelationDegree keyItem) {
        String sql = "DELETE FROM " + NAME + " WHERE UniqId = ?";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getUniqId() });
    }

    @Override
    public List<CaseDataRelationDegree> Query(CaseDataRelationDegree keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE UniqId = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getUniqId() },
                    new BeanPropertyRowMapper<>(CaseDataRelationDegree.class));
        } catch (DataAccessException ex) {
            return new ArrayList<>();
        }
    }

    public CaseDataRelationDegree findOne(String uniqId, String idno) {
        CaseDataRelationDegree keyItem = new CaseDataRelationDegree();
        keyItem.setUniqId(uniqId);
        keyItem.setIdno(idno);
        return Read(keyItem);

    }

    public List<CaseDataRelationDegree> findByUniqId(String uniqId) {
        CaseDataRelationDegree keyItem = new CaseDataRelationDegree();
        keyItem.setUniqId(uniqId);
        return Query(keyItem);
    }

    public int deleteByUniqId(String uniqId) {
        CaseDataRelationDegree keyItem = new CaseDataRelationDegree();
        keyItem.setUniqId(uniqId);
        return Delete(keyItem);
    }
    
    public void saveApplyRelationDegreeView(String uniqId, ApplyRelationDegreeView view) {
        @SuppressWarnings("unused")
        int deleted = deleteByUniqId(uniqId);
        List<CaseDataRelationDegree> list = new ArrayList<>();
        // 本人
        list.add(new CaseDataRelationDegree(uniqId, "0", view.getIdno(), view.getCustomer_name(), null, null, null, null));
        // 親屬
        for (ApplySecondDegree data : view.getAddSecondDegreeData()) {
            list.add(new CaseDataRelationDegree(uniqId, data.getRelationship(), data.getRelationshipId(), data.getRelationshipName(), null, null, null, null));
        }
        // 若您或您的配偶擔任「企業負責人」,請填寫
        for (ApplyRelationDegreeCorp data : view.getAddData()) {
            if (StringUtils.isNotBlank(data.getCorpRelationDegree()) && StringUtils.isNotBlank(data.getCorpName())) {
                list.add(new CaseDataRelationDegree(uniqId, data.getCorpRelationDegree(), null, null, data.getCorpName(), data.getCorpnumber(), data.getCorpJobTitle(), data.getMemo()));
            }
        } // end for

        for (CaseDataRelationDegree o : list) {
            Create(o);
        }
    }
}
