package com.kgi.eopend3.ap.service;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.imageio.ImageIO;

@Service
public class ImageService {

    @Autowired
    AirloanEXConfig globalConfig;
    
    // 壓浮水印
	public BufferedImage addTextWatermark(String text, BufferedImage oriImage) throws IOException {
		BufferedImage sourceImage = cloneImage(oriImage);
		Graphics2D g2d = (Graphics2D) sourceImage.getGraphics();

		// initializes necessary graphic properties

		int fontSize = getFontSize(oriImage.getWidth(), oriImage.getHeight());
		float imageAlpha = Float.parseFloat(globalConfig.upload_image_alpha());
		AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, imageAlpha); // 1.0f为透明度 ，值从0-1.0，依次变得不透明
		g2d.setComposite(alphaChannel);
		// g2d.setColor(new Color(0x00, 0xA1, 0xDF));
		g2d.setColor(new Color(255, 55, 0));
		g2d.setFont(new Font("Dialog", Font.BOLD, fontSize));
		FontMetrics fontMetrics = g2d.getFontMetrics();
		Rectangle2D rect = fontMetrics.getStringBounds(text, g2d);

		// calculates the coordinate where the String is painted
		int centerX = (sourceImage.getWidth() - (int) rect.getWidth()) / 2;
		int centerY = sourceImage.getHeight() / 2;

		// paints the textual watermark
		g2d.drawString(text, centerX, centerY);

		g2d.dispose();

		return sourceImage;
	}

	private BufferedImage cloneImage(BufferedImage oriImage) {
		return oriImage.getSubimage(0, 0, oriImage.getWidth(), oriImage.getHeight());
	}

	private int getFontSize(int width, int height) {
		int fontSize = width;
		if (width > height) {
			fontSize = (fontSize / 14 > 0) ? fontSize / 14 : 1;
			return fontSize;
		}
		fontSize = (fontSize / 20 > 0) ? fontSize / 20 : 1;

		return fontSize;
	}
    
    // 圖檔 resize
    public BufferedImage resizeToPixels(BufferedImage bigImage, int pixels) {
        // 取得圖的寬高
        double width = bigImage.getWidth();
        double height = bigImage.getHeight();
        // 如果畫素大於 pixels 就 reize
        if (height * width > pixels) {
            double p = width / height; // 計算出比例
            // 透過總畫素與比例計算出resize後的寬高
            int newHeight = (int) Math.sqrt(pixels / p); // 新的高
            int newWidth = pixels / newHeight; // 新的寬
            return getScaledInstance(bigImage, newWidth, newHeight, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR, false);
        }
        return getScaledInstance(bigImage, bigImage.getWidth(), bigImage.getHeight(), RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR, false);
    }

    public BufferedImage getScaledInstance(BufferedImage img, int targetWidth, int targetHeight, Object hint, boolean higherQuality) {
        //int type = (img.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        int type = BufferedImage.TYPE_3BYTE_BGR;
        BufferedImage ret = (BufferedImage) img;
        int w, h;
        if (higherQuality) {
            w = img.getWidth();
            h = img.getHeight();
        } else {
            w = targetWidth;
            h = targetHeight;
        }

        do {
            if (higherQuality && w > targetWidth) {
                w /= 2;
                if (w < targetWidth) {
                    w = targetWidth;
                }
            }

            if (higherQuality && h > targetHeight) {
                h /= 2;
                if (h < targetHeight) {
                    h = targetHeight;
                }
            }
            BufferedImage tmp = new BufferedImage(w, h, type);
            Graphics2D g2 = tmp.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
            g2.drawImage(ret, 0, 0, w, h, null);
            g2.dispose();

            ret = tmp;
        } while (w != targetWidth || h != targetHeight);

        return ret;
    }   

	// 影像 resize 成大小圖 + 壓浮水印
	public byte[] resizeImageFile(byte[] bigPhoto, String waterText, boolean drawWaterText) throws IOException {
		// 讀入原始圖檔
		InputStream is = new ByteArrayInputStream(bigPhoto);
		BufferedImage bigImg = ImageIO.read(is);

		// 大圖 resize
		bigImg = resizeToPixels(bigImg, 3000000);

		// 壓浮水印
		if (drawWaterText && waterText != null && waterText.length() > 0) {
			bigImg = addTextWatermark(waterText, bigImg);
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bigImg, "jpg", baos);
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();
		return imageInByte;
		// 從大圖做出小圖

	}

	public byte[] resizeSmallImageFile(byte[] bigPhoto) throws IOException {
		InputStream is = new ByteArrayInputStream(bigPhoto);
		BufferedImage bigImg = ImageIO.read(is);
		BufferedImage smallImage = null;
        smallImage = getScaledInstance(bigImg, 250, 166,RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR, false);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(smallImage, "jpg", baos);
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();
		return imageInByte;
	}

}