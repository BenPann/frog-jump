package com.kgi.airloanex.ap.service;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.dao.CreditCardProductDao;
import com.kgi.airloanex.common.dto.response.GiftListResp;
import com.kgi.airloanex.common.dto.view.CreditCardProductView;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.dto.WebResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreditCardService {
    @SuppressWarnings("unused")
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CreditCardProductDao creditCardProductDao;

    public String findGiftList(String uniqId){
        try {
            List<GiftListResp> giftListByUniqId = creditCardProductDao.findGiftListByUniqId(uniqId);
            giftListByUniqId = giftListByUniqId == null ? new ArrayList<>() :giftListByUniqId;
            return WebResult.GetSuccessResult(giftListByUniqId);
        }catch (ErrorResultException e){
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        }catch (Exception e){
            return WebResult.GetSystemFailResult();
        }
    } // end findGiftList

    public String findGiftListByProductIdView(String reqJson) {
        CreditCardProductView view = new Gson().fromJson(reqJson, CreditCardProductView.class);
        String productId = view.getProductId();
        return findGiftListByProductId(productId);
    } // end findGiftListByProductId

    public String findGiftListByProductId(String productId) {
        try {
            List<GiftListResp> giftListByProductId = creditCardProductDao.findGiftListByProductId(productId);
            return WebResult.GetSuccessResult(giftListByProductId);
        }catch (ErrorResultException e){
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        }catch (Exception e){
            return WebResult.GetSystemFailResult();
        }
    } // end findGiftListByProductId
}
