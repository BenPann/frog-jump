package com.kgi.airloanex.ap.pdf.dto;

import java.util.ArrayList;
import java.util.List;

import com.kgi.airloanex.common.dto.customDto.UploadFileDto;

public class PDFCaseDataShoot {

    int cntOK = 0;

    int err = 0;

    boolean sendimageOK = false; // 測試開true

    List<UploadFileDto> fileList = new ArrayList<UploadFileDto>();

    boolean fastPass = false;

    String dgtType = "";
    String amlQueryTime = "";
    String amlStatus = "";

    boolean goReturn = false;

    // after buildOrbitData
    String branchId;

    public int getCntOK() {
        return cntOK;
    }

    public void setCntOK(int cntOK) {
        this.cntOK = cntOK;
    }

    public int getErr() {
        return err;
    }

    public void setErr(int err) {
        this.err = err;
    }

    public boolean isSendimageOK() {
        return sendimageOK;
    }

    public void setSendimageOK(boolean sendimageOK) {
        this.sendimageOK = sendimageOK;
    }

    public List<UploadFileDto> getFileList() {
        return fileList;
    }

    public void setFileList(List<UploadFileDto> fileList) {
        this.fileList = fileList;
    }

    public boolean isFastPass() {
        return fastPass;
    }

    public void setFastPass(boolean fastPass) {
        this.fastPass = fastPass;
    }

    public String getDgtType() {
        return dgtType;
    }

    public void setDgtType(String dgtType) {
        this.dgtType = dgtType;
    }

    public String getAmlQueryTime() {
        return amlQueryTime;
    }

    public void setAmlQueryTime(String amlQueryTime) {
        this.amlQueryTime = amlQueryTime;
    }

    public String getAmlStatus() {
        return amlStatus;
    }

    public void setAmlStatus(String amlStatus) {
        this.amlStatus = amlStatus;
    }

    public boolean isGoReturn() {
        return goReturn;
    }

    public void setGoReturn(boolean goReturn) {
        this.goReturn = goReturn;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }
}
