package com.kgi.airloanex.ap.pdf;

import static com.kgi.airloanex.ap.service.CaseDataService.isNewOne;
import static com.kgi.airloanex.ap.service.CaseDataService.isOldOne;
import static com.kgi.airloanex.common.PlContractConst.CROSS_ZONES;
import static com.kgi.airloanex.common.PlContractConst.UNIQ_TYPE_02;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.CaseDataKycDebtInfoDao;
import com.kgi.airloanex.ap.dao.CaseDocumentDao;
import com.kgi.airloanex.ap.dao.PDFDao;
import com.kgi.airloanex.ap.dao.QR_ChannelListDao;
import com.kgi.airloanex.ap.pdf.dto.PDFCaseData;
import com.kgi.airloanex.ap.service.APSPlContractService;
import com.kgi.airloanex.ap.service.ChannelDepartService;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.db.CaseDataKycDebtInfo;
import com.kgi.airloanex.common.dto.db.CaseDocument;
import com.kgi.airloanex.common.dto.db.QR_ChannelDepartList;
import com.kgi.airloanex.common.dto.db.QR_ChannelList;
import com.kgi.airloanex.common.util.ExportUtil;
import com.kgi.eopend3.ap.config.GlobalConfig;
import com.kgi.eopend3.ap.service.DropDownService;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class CaseKycPDF extends BasePDFDocument {
    
    private static final Logger logger = LogManager.getLogger(CaseKycPDF.class.getName());

    private String _uniqId = "";

    @Autowired
    private GlobalConfig global;
    
	@Autowired
    private AirloanEXConfig globalConfig;
    
    @Autowired
    private APSPlContractService apsService;

    @Autowired
    private PDFDao pdfDao;

    @Autowired
    private CaseDataKycDebtInfoDao caseDataKycDebtInfoDao;

    @Autowired
    private CaseDocumentDao caseDocumentDao;

    @Autowired
    private QR_ChannelListDao channelListDao;

    @Autowired
    private ChannelDepartService channelDepartService;

    @Autowired
    private DropDownService dropDownService;

    private PDFCaseData pdfCaseData = null;
    private CaseDataKycDebtInfo debtInfo = null;

    private QR_ChannelDepartList channelDepart;
    private QR_ChannelList channel;

    private String templateVersion = "";

    public CaseKycPDF(ApplicationContext appContext) {
        super(appContext);
    }

    @Override
    public void init(String... params) throws Exception {
        this._uniqId = params[0];
        // 是否加密
        this.setUseEncrepyt(true);
        // this.setEncryptPassword(idno);
        // 是否使用浮水印
        this.setUseWaterMark(true);
        QR_ChannelDepartList qrc = channelDepartService.findQR_ChannelDepartList(this._uniqId);
        String departId = qrc.getDepartId();
        String warterFontKyc = StringUtils.replace(globalConfig.IMG_WATER_FONT_KYC(), "${ASSIGN_BRANCH}", departId);
		this.setWaterMarkText(warterFontKyc);
		this.setWaterMarkFont(globalConfig.PDF_FONT_WATER_TTF());
        // 是否要PDF轉成圖片
        this.setPdfToImage(false);
        // 是否存DB
        this.setUseSaveToDB(true);
        // 是否發信
        this.setUseSendEMail(false);

    }

    @Override
    public void beforePdfProcess() {
        try {
            // 取得Html的方法
            String StaticFilePath = global.StaticFilePath;
            String htmlUrl = StaticFilePath + globalConfig.PDF_KYC_TemplateFilePath();
            templateVersion = globalConfig.PDF_KYC_VerNo();
            logger.info("### htmlUrl => " + htmlUrl);
            InputStream input = new FileInputStream(new File(htmlUrl));
            this.setRawHtml(this.getContentFromFilePath(input));

            pdfCaseData = pdfDao.loadCaseData(this._uniqId);
            debtInfo = caseDataKycDebtInfoDao.findByUniqId(this._uniqId);

            channelDepart = this.channelDepartService.getLoanChannelDepart(this._uniqId);
            channel = this.channelListDao.Read(new QR_ChannelList(channelDepart.getChannelId()));

            this.setEncryptPassword(pdfCaseData.idno);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("取得PDF檔案失敗! CaseNo = " + this._uniqId);
        }
    }

    @Override
    public String replaceField(String raw) {
        String sKeyCaseNo = pdfCaseData.CaseNo;
		logger.info("### " + sKeyCaseNo + " Begin - replaceField");

        String str = raw;

        String applydate = "";
        String verifyType = "";
		
		
        
        boolean hasNoCreditVerify = !pdfCaseData.applycc;
        logger.info("### " + sKeyCaseNo + " hasNoCreditVerify=" + hasNoCreditVerify);
        if(hasNoCreditVerify) { // hasNoCreditVerify
        	applydate = pdfCaseData.applyccUtime;
        	verifyType = PlContractConst.CREDITVERIFY_OTP;
        } else { // hasCreditVerify
            applydate = pdfCaseData.CaseNo.substring(4, 12);
            String authType = apsService.externalCheckTypeForAddNewCase(this._uniqId);
            verifyType = apsService.transAuthType(authType);
            logger.info("### " + sKeyCaseNo + " authType=" + authType);
            logger.info("### " + sKeyCaseNo + " verifyType=" + verifyType);
        }
        
        str = str.replace("[UniqId]", this._uniqId);
        str = str.replace("[Query_Date]", pdfCaseData.Z07Time); // yyyy-MM-dd HH:mm:ss.SSS
        str = str.replace("[Apply_user_name]", pdfCaseData.customer_name);
        
        str = str.replace("[Apply_user_idno]", pdfCaseData.idno);

        str = str.replace("[Apply_user_age]", String.valueOf(DateUtil.calculateAge(pdfCaseData.birthday))); // yyyyMMdd
        str = str.replace("[Apply_user_job_tenure]", String.valueOf(DateUtil.calculateAge(pdfCaseData.on_board_date))); // yyyyMMdd
        str = str.replace("[Apply_user_company_address]", StringUtils.replace(pdfCaseData.corp_address, ";", ""));
        str = str.replace("[Apply_user_house_address]", StringUtils.replace(pdfCaseData.house_address, ";", ""));
        str = str.replace("[Promo_Depart]", this.channelDepart.getPromoDepart());
        str = str.replace("[Assign_Depart]", this.channelDepart.getAssignDepart());
        str = str.replace("[Cross_zone_case]", crossZoneCaseDesc(this.channelDepart.getAssignDepart(), pdfCaseData.corp_address, pdfCaseData.house_address)); // TODO:
        str = str.replace("[Finacial_type]", "D.其他文件");
        str = str.replace("[Apply_user_annual_income]", String.valueOf(pdfCaseData.yearly_income * 10000)); // 元
        str = str.replace("[Apply_user_avg_month_income]", String.valueOf(pdfCaseData.yearly_income * 10000 / 12)); // 元
        str = str.replace("[AML_Query_Date]", StringUtils.substring(pdfCaseData.AmlTime, 0, 10)); // yyyy-MM-dd
        
        str = str.replace("[AML_RESULT]", amlResultDesc(pdfCaseData.AmlResult));
        str = str.replace("[Apply_user_high_risk_occupation]", highRiskOccupationDesc(pdfCaseData.Occupation, pdfCaseData.title));
        
        str = str.replace("[Kgibank_idno_check_risk_grade_result]", pdfCaseData.CddRateRisk);

        str = str.replace("[CHECK_Z07_RESULT]", checkZ07ResultDesc(pdfCaseData.Z07Result));
        
		logger.info("#### " + sKeyCaseNo + " replace [SEND_JCIC_Z07_HTML]...");
		str = str.replace("[SEND_JCIC_Z07_HTML]", makeZ07Html(pdfCaseData.Z07Html));
		//str = str.replace("[SEND_JCIC_Z07_HTML]", pdfCaseData.Z07Html);
		//str = str.replace("[SEND_JCIC_Z07_HTML]", makeZ07Htmlv2(pdfCaseData.Z07Html));

        String Stakeholder_Query_Date = " ";
        String Stakeholder_Query_Result = "利關人查詢失敗請補查";
        if (StringUtils.isNotBlank(pdfCaseData.CheckRelpResult)) {
            Stakeholder_Query_Date = pdfCaseData.CheckRelpTime;
            Stakeholder_Query_Result = pdfCaseData.CheckRelpResult;
        }
        str = str.replace("[Stakeholder_Query_Date]", StringUtils.substring(Stakeholder_Query_Date, 0, 10)); // yyyy-MM-dd
        str = str.replace("[Stakeholder_Query_Result]", Stakeholder_Query_Result);

        str = str.replace("[Auth_Type]", verifyType);
        str = str.replace("[ip_address]", pdfCaseData.ip_address);
        str = str.replace("[Apply_date]", "中華民國".concat(String.valueOf(Integer.parseInt(applydate.substring(0, 4))-1911)) + "年" + applydate.substring(4, 6) + "月" + applydate.substring(6, 8) + "日");

        String mortagageDesc = "";
        String carLoanDesc = "";
        String ploanDesc = "";
        String creditCardDesc = "";
        String cashCardDesc = "";

        if (debtInfo != null) {
            String mortagageFlag      = debtInfo.getMORTAGAGE_FLAG();
            String mortagageAmt       = debtInfo.getMORTAGAGE_AMT();
            String mortagagePay       = debtInfo.getMORTAGAGE_PAY();
            String carLoanFlag        = debtInfo.getCAR_LOAN_FLAG();
            String carLoanAmt         = debtInfo.getCAR_LOAN_AMT();
            String carLoanPay         = debtInfo.getCAR_LOAN_PAY();
            String ploanFlag          = debtInfo.getPLOAN_FLAG();
            String ploanAmt           = debtInfo.getPLOAN_AMT();
            String ploanPay           = debtInfo.getPLOAN_PAY();
            String creditCardFlag     = debtInfo.getCREDIT_CARD_FLAG();
            String creditCardAmt      = debtInfo.getCREDIT_CARD_AMT();
            String creditCardPayKind  = debtInfo.getCREDIT_CARD_PAY_KIND();
            String creditCardCycleAmt = debtInfo.getCREDIT_CARD_CYCLE_AMT();
            String cashCardFlag       = debtInfo.getCASH_CARD_FLAG();
            String cashCardAmt        = debtInfo.getCASH_CARD_AMT();

            mortagageDesc = mortagageDesc(mortagageFlag, mortagageAmt, mortagagePay);
            carLoanDesc = carLoanDesc(carLoanFlag, carLoanAmt, carLoanPay);
            ploanDesc = ploanDesc(ploanFlag, ploanAmt, ploanPay);
            creditCardDesc = creditCardDesc(creditCardFlag, creditCardAmt, creditCardPayKind, creditCardCycleAmt);
            cashCardDesc = cashCardDesc(cashCardFlag, cashCardAmt);
        }

        str = str.replace("[Apply_user_MORTAGAGE]", mortagageDesc);
        str = str.replace("[Apply_user_CAR_LOAN]", carLoanDesc);
        str = str.replace("[Apply_user_PLOAN]", ploanDesc);
        str = str.replace("[Apply_user_CREDIT_CARD]", creditCardDesc);
        str = str.replace("[Apply_user_CASH_CARD]", cashCardDesc);
        str = str.replace("[Apply_user_IMAGE_LIST]", imageList(pdfCaseData.OldIdImgNoList, pdfCaseData.OldFinaImgNoList));
        str = str.replace("[HAVE_FINPF_APS_NO]", pdfCaseData.CaseNoApsPrev);

        logger.info("### " + sKeyCaseNo + " End - replaceField");
        logger.info("### " + sKeyCaseNo + " previewHtml.length()=" + str.length());

        // HTML檔 - @export 儲存至檔案系統
        if (globalConfig.isPDF_Export()) {
            ExportUtil.export(global.PdfDirectoryPath + "/" + "CaseKycPDF.previewHtml.html", str);
        }
        return str;
    }

    /** 跨區案件=是/否 */
    private String crossZoneCaseDesc(String assignDepart, String corp_address, String house_address) {

        if (StringUtils.isBlank(assignDepart)) {
            return "";
        }
        
        // 說明: 以(7)為主，對(5)、(6)對應下列表的判斷，是否為跨區案件
        // 範例:
        // a.推廣單位所屬案件處理單位是北一區8220，客戶工作地點是台南市，客戶通訊地址是高雄市，跨區案件=是
        // b.推廣單位所屬案件處理單位是台南區8260，客戶工作地點是台南市，客戶通訊地址是高雄市，跨區案件=否

        // 北一區: 基隆市、台北市、新北市、桃園、宜蘭
        // 北二區: 基隆市、台北市、新北市、桃園、宜蘭、花蓮
        // 桃竹區: 新北市、桃園、新竹、苗栗
        // 台中區: 苗栗、台中、彰化、南投
        // 台南區: 雲林、嘉義、台南、高雄
        // 高雄區: 台南、高雄、屏東、台東

        // true 不是跨區案件(跨區案件=否)
        // (預設)false 是跨區案件(跨區案件=是)
        boolean isInZones = false; // 在六大區內
        boolean isNotCross = false;
        for (Map.Entry<String, String[]> crossZone : CROSS_ZONES.entrySet()) {
            String k = crossZone.getKey();
            String[] v = crossZone.getValue();
            boolean isInZone = StringUtils.equals(k, assignDepart);
            isInZones |= isInZone;
            if (isInZone) { // assignDepart 在六大區內
                isNotCross |= StringUtils.containsAny(corp_address, v);
                isNotCross |= StringUtils.containsAny(house_address, v);
            }
        }

        if (!isInZones) { // 在六大區內
            return "不適用";
        } else { // 不在六大區內
            if (isNotCross) {
                return "否";
            } else {
                return "是";
            }
        }
    }

    private String highRiskOccupationDesc(String occupation, String title) {
        String follow = "(" + dropDownService.findDataName_occupation(pdfCaseData.Occupation) + " " + pdfCaseData.title + ")";
        if (StringUtils.startsWith(occupation, "1")) {
            return "是" + follow;
        } else {
            return "否" + follow;
        }
    }

    private String amlResultDesc(String amlResult) {
        String follow = "(" + amlResult + ")";
        if (StringUtils.equals("Y", amlResult)) {
            return "異常" + follow;
        } else if (StringUtils.equals("N", amlResult)) {
            return "正常" + follow;
        } else if (StringUtils.equals("V", amlResult)) {
            return "異常" + follow;
        } else {
            return "AML發查失敗，請補查";
        }
    }

    private String checkZ07ResultDesc(String Z07Result) {
        if (StringUtils.equals("Y", Z07Result)) {
            return "是"; // Y：是(有通報案件記錄)
        } else if (StringUtils.equals("N", Z07Result)) {
            return "否";
        } else {
            return "";
        }
    }

    private String makeZ07Htmlv2(String Z07Html) {
        // final String scriptStr = "<SCRIPT language=\"JavaScript\">";
        final String scriptEnd = "</SCRIPT>";
        int indexStr = Z07Html.indexOf(scriptEnd) + scriptEnd.length();
        Z07Html = StringUtils.substring(Z07Html, indexStr);
		
        //Z07Html = Z07Html.replaceAll("(?i)<(/?html[^>]*)>", "");
        //Z07Html = Z07Html.replaceAll("(?i)<(/?body[^>]*)>", "");

        //Z07Html = Z07Html.replace("<table width=100% border=2 cellspacing=2><th", "<table width=100% border=2 cellspacing=2><tr><th");
        //Z07Html = Z07Html.replace("</th></table>", "</th></tr></table>");

        //Z07Html = Z07Html.replace("<table width=100% border=0><td", "<table width=100% border=0><tr><td");
        //Z07Html = Z07Html.replace("</td></table>", "</td></tr></table>");

        //Z07Html = Z07Html.replace("<hr size=0>", "<hr size=0></hr>");
        Z07Html = Z07Html.replace("<HR>", "<HR></HR>");

        //Z07Html = Z07Html.replace("<<通報案件紀錄資訊>>", "&lt;&lt;通報案件紀錄資訊&gt;&gt;");
        //Z07Html = Z07Html.replace("<basefont=4 font FACE=\"細明體\" LANG=\"ZH-TW\">", "");
        //Z07Html = Z07Html.replace("</basefont>", "");
        Z07Html = Z07Html.replace("<br>", "<br />");
        return Z07Html;
    }


    private String makeZ07Html(String Z07Html) {
        // final String scriptStr = "<SCRIPT language=\"JavaScript\">";
        final String scriptEnd = "</SCRIPT>";
        int indexStr = Z07Html.indexOf(scriptEnd) + scriptEnd.length();
        Z07Html = StringUtils.substring(Z07Html, indexStr);
		
		System.out.println(Z07Html);
		
        Z07Html = Z07Html.replaceAll("(?i)<(/?html[^>]*)>", "");
        Z07Html = Z07Html.replaceAll("(?i)<(/?body[^>]*)>", "");
		
		Z07Html = Z07Html.replaceAll("(?i)<(/?head[^>]*)>", "");

        Z07Html = Z07Html.replace("<table width=100% border=2 cellspacing=2><th", "<table width='100%' border='2' cellspacing='2'><tr><th");
        Z07Html = Z07Html.replace("</th></table>", "</th></tr></table>");

        Z07Html = Z07Html.replace("<table width=100% border=0><td", "<table width='100%' border='0'><tr><td");
        Z07Html = Z07Html.replace("</td></table>", "</td></tr></table>");

        Z07Html = Z07Html.replace("<hr size=0>", "<br/><hr size='1'></hr><br/>");
        Z07Html = Z07Html.replace("<HR>", "<HR></HR>");

        Z07Html = Z07Html.replace("<<通報案件紀錄資訊>>", "<br/>**通報案件紀錄資訊**<br/>");
        Z07Html = Z07Html.replace("<basefont=4 font FACE=\"細明體\" LANG=\"ZH-TW\">", "");
        Z07Html = Z07Html.replace("</basefont>", "");
        Z07Html = Z07Html.replace("<br>", "<br/>");
		
        Z07Html = Z07Html.replace("<font size=5>", "<font size='5'>");	
		Z07Html = Z07Html.replace("<font size=4>", "<font size='4'>");
		Z07Html = Z07Html.replace("<font size=3>", "<font size='3'>");
		Z07Html = Z07Html.replace("<font size=2>", "<font size='2'>");	
		Z07Html = Z07Html.replace("<font size='-1'>", "<font size='2'>");
		
		Z07Html = Z07Html.replace("align=center", "align='center' style='text-align:center;' ");
		Z07Html = Z07Html.replace("align=left", "align='left' style='text-align:left;' ");
		Z07Html = Z07Html.replace("align=right", "align='right' style='text-align:right;' ");

		
		
		Z07Html = Z07Html.replace("使用者帳號:", "<br/>使用者帳號:");
		Z07Html = Z07Html.replace("所屬單位:", "　　　　　　　　所屬單位:");
		Z07Html = Z07Html.replace("身分證號:", "身分證號:");
		Z07Html = Z07Html.replace("消債/其他註記:", "<br/>消債/其他註記:");
		Z07Html = Z07Html.replace("通報案件紀錄:", "<br/>通報案件紀錄:");
		
		Z07Html = Z07Html.replace(" 查詢理由</b>", " 查詢理由</b><br/>");
		Z07Html = Z07Html.replace("新業務申請", "新業務申請<br/>");
		Z07Html = Z07Html.replace("紀錄明細如下:", "紀錄明細如下:<br/>");
		Z07Html = Z07Html.replace("被查詢理由", "<br/>被查詢理由");
		
		Z07Html = Z07Html.replace("<center>***** 上述資訊僅供會員參考", "<br/><center>***** 上述資訊僅供會員參考");
		
		Z07Html = Z07Html.replace("該戶最近三個月內", "<br/>該戶最近三個月內");
		Z07Html = Z07Html.replace("***** 電子密章", "<br/>***** 電子密章");
		
		Z07Html = Z07Html.replace("________________________________________________________________________________", "<br/><hr size='1'></hr><br/>");


		Z07Html = Z07Html.replace("<center>", "<b><font size='4' style='line-height:30px; color: black;'>");	
		Z07Html = Z07Html.replace("</center>", "</font></b>");	

		Z07Html = Z07Html.replace("</sub></p>", "</sub></p><br/>");
		Z07Html = Z07Html.replaceAll("(?i)<(/?pre[^>]*)>", "");
		Z07Html = Z07Html.replaceAll("(?i)<(/?sub[^>]*)>", "");
		
		Z07Html = Z07Html.replace("'", "\"");
		
		System.out.println("/////////////////////////////////////////////////");
		System.out.println(Z07Html);
		
        return Z07Html;
    }

    private String mortagageDesc(String mortagageFlag, String mortagageAmt, String mortagagePay) {
        StringBuilder sb = new StringBuilder();
        sb.append(flagDesc(mortagageFlag));
        sb.append(titleAndField("房貸餘額", mortagageAmt));
        sb.append(titleAndField("房貸每月應繳金額", mortagagePay));
        return sb.toString();
    }
    private String carLoanDesc(String carLoanFlag, String carLoanAmt, String carLoanPay) {
        StringBuilder sb = new StringBuilder();
        sb.append(flagDesc(carLoanFlag));
        sb.append(titleAndField("車貸餘額", carLoanAmt));
        sb.append(titleAndField("車貸每月應繳金額", carLoanPay));
        return sb.toString();
    }
    private String ploanDesc(String ploanFlag, String ploanAmt, String ploanPay) {
        StringBuilder sb = new StringBuilder();
        sb.append(flagDesc(ploanFlag));
        sb.append(titleAndField("信貸餘額", ploanAmt));
        sb.append(titleAndField("信貸每月應繳金額", ploanPay));
        return sb.toString();
    }
    private String creditCardDesc(String creditCardFlag, String creditCardAmt, String creditCardPayKind, String creditCardCycleAmt) {
        StringBuilder sb = new StringBuilder();

        sb.append(flagDesc(creditCardFlag));
        sb.append(titleAndField("信用卡餘額", creditCardAmt));
        sb.append(titleAndField("信用卡付款方式", creditCardPayKindDesc(creditCardPayKind)));
        sb.append(titleAndField("信用卡循環金額", creditCardCycleAmt));
        return sb.toString();
    }
    private String cashCardDesc(String cashCardFlag, String cashCardAmt) {
        StringBuilder sb = new StringBuilder();
        sb.append(flagDesc(cashCardFlag));
        sb.append(titleAndField("現金卡動用餘額", cashCardAmt));
        return sb.toString();
    }

    private String imageList(String OldIdImgNoList, String OldFinaImgNoList) {
        if (StringUtils.isBlank(OldIdImgNoList) && StringUtils.isBlank(OldFinaImgNoList)) {
            return "查無影編資訊";
        } else {
            return StringUtils.join(new String[]{OldIdImgNoList, OldFinaImgNoList}, ",");
        }
    }

    private String titleAndField(String title, String field) {
        if (StringUtils.isNotBlank(field)) {
            return title + field;
        } else {
            return "";
        }
    }

    private String flagDesc(String flag) {
        if (StringUtils.equals("1", flag)) {
            return "本行";
        } else if (StringUtils.equals("2", flag)) {
            return "他行";
        } else if (StringUtils.equals("3", flag)) {
            return "本行他行"; // 皆有
        } else {
            return "";
        }
    }

    private String creditCardPayKindDesc(String flag) {
        if (StringUtils.equals("1", flag)) {
            return "全清";
        } else if (StringUtils.equals("2", flag)) {
            return "循環";
        } else {
            return "";
        }
    }

    @Override
    public void afterPdfProcess() throws Exception {

    }

    @Override
    public void saveToDB(byte[] pdfByte) {
        
        // Save bytes to TABLE: CaseDocument
        CaseDocument cd = new CaseDocument(this._uniqId, UNIQ_TYPE_02, PlContractConst.DOCUMENT_TYPE_LOAN_KYC_PDF);
        cd.setContent(pdfByte);
        cd.setVersion(templateVersion);
        caseDocumentDao.CreateOrUpdate(cd);
        
        // Save string base64 to TABLE: CaseDoc
        String pdfbase64 = Base64.encodeBase64String(pdfByte);
        pdfDao.storePDF(this._uniqId, this.pdfCaseData.idno, pdfbase64, "");
        System.out.println("Save Pdf To DB");

        // PDF檔 - @export 儲存至檔案系統
        if (globalConfig.isPDF_Export()) {
            ExportUtil.export(global.PdfDirectoryPath + "/" + _uniqId + "kyc.pdf", pdfByte);
        }
    }

    @Override
    public void pdfToImage(List<byte[]> byteList) {

    }

    @Override
    public void sendEmail(byte[] pdfByte) {
        // Note! Do not send the KYC report to customer
    }
}
