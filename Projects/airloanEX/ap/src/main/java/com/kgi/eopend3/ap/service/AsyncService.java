package com.kgi.eopend3.ap.service;

// import java.util.concurrent.Future;

import com.kgi.airloanex.ap.dao.UserPhotoDao;
import com.kgi.airloanex.common.dto.db.UserPhoto;
import com.kgi.eopend3.common.dto.view.UserPhotoView;

// import org.owasp.esapi.ESAPI;
// import org.springframework.scheduling.annotation.Async;
// import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.kgi.airloanex.common.PlContractConst.*;


@Service
public class AsyncService {

	@Autowired
	private UserPhotoDao userPhotoDao;
	// Charles: 我也不知道他為什麼(吃飽了)要用非同步，總之會異常。
	/*@Async("threadPoolTaskExecutor")
    public Future<String> outputAfterCreate(String uniqId,UserPhotoView view,byte[] bigImg,byte[] smallImg, boolean isPrimary){
//		System.out.println("outputAfterCreate start");
		// 主證件只能有一張
		if (isPrimary) {
			userPhotoDao.DeletePrimary(uniqId, view.getSType());
		} else {
			userPhotoDao.DeleteSub(uniqId, view.getSubSerial());
		}


		UserPhoto photo = new UserPhoto();
		photo.setUniqId(uniqId);
		photo.setOnline(PHOTO_ONLINE_STATUS_ONLINE.toString()); // FIXME: Charles 日後可能要修正。
		photo.setProdType("1"); // FIXME: Charles view.ProdType
		photo.setStatus(USERPHOTO_STATUS_USER_PROCESS);
		photo.setPType(Integer.valueOf(getPType(view.getSType())));				
		photo.setSType(Integer.valueOf(view.getSType()));
		photo.setProductId("");
		photo.setUnitId("");
		photo.setImageBig(bigImg);
		photo.setImageSmall(smallImg);
		photo = userPhotoDao.outputAfterCreate(photo);
		
//		System.out.println("outputAfterCreate end");
		return new AsyncResult<String>(String.valueOf(photo.getSubSerial()));
    }

    @Async("threadPoolTaskExecutor")
    public void esapiUserPhoto(String reqBody){
        ESAPI.validator().isValidInput("UserPhotoController", reqBody, "SafeJson", Integer.MAX_VALUE, false);
	}*/

	public String outputAfterCreate(String uniqId,UserPhotoView view,byte[] bigImg,byte[] smallImg, boolean isPrimary){
		// 主證件只能有一張
		if (isPrimary) {
			userPhotoDao.DeletePrimary(uniqId, view.getSType());
		} else {
			userPhotoDao.DeleteSub(uniqId, view.getSubSerial());
		}


		UserPhoto photo = new UserPhoto();
		photo.setUniqId(uniqId);
		photo.setOnline(PHOTO_ONLINE_STATUS_ONLINE.toString()); // FIXME: Charles 日後可能要修正。
		photo.setProdType(PHOTO_PRODUCTTYPE_LOAN);
		photo.setStatus(USERPHOTO_STATUS_USER_PROCESS);
		photo.setPType(Integer.valueOf(getPType(view.getSType())));				
		photo.setSType(Integer.valueOf(view.getSType()));
		photo.setProductId("");
		photo.setUnitId("");
		photo.setImageBig(bigImg);
		photo.setImageSmall(smallImg);
		photo = userPhotoDao.outputAfterCreate(photo);
		
		return String.valueOf(photo.getSubSerial());
	}
	
	public String getPType(String stype){
		if (
			USERPHOTO_SUBTYPE_IDCARDFRONT.equals(stype) 
			|| USERPHOTO_SUBTYPE_IDCARDBANK.equals(stype) 
			|| USERPHOTO_SUBTYPE_HEALTHIDCARD.equals(stype)){
			return USERPHOTO_PTYPE_1; // 身分證明文件(即身份證正反面及健保卡)
		} else {
			return USERPHOTO_PTYPE_2; // 財力證明文件
		}
	}
}