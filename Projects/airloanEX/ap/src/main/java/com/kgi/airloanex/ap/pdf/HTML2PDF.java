package com.kgi.airloanex.ap.pdf;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.Base64;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.HTML;
import com.itextpdf.tool.xml.html.TagProcessorFactory;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.AbstractImageProvider;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import com.itextpdf.tool.xml.pipeline.html.NoImageProviderException;
import com.kgi.airloanex.common.dto.db.DropdownData;
import com.kgi.airloanex.ap.pdf.dto.PDFCaseData;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.eopend3.ap.config.GlobalConfig;
import com.kgi.airloanex.ap.config.AirloanEXConfig;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.sf.json.JSONObject;

@Component
public class HTML2PDF {

	@Autowired
	private GlobalConfig global;
	
	@Autowired
	private AirloanEXConfig globalConfig;

	@Autowired
	private PdfFieldReplace pdfFieldReplace;
	@SuppressWarnings("all")
	public void createPdf(Path file, PDFCaseData pdfCaseData, List<DropdownData> dropdownData,Map mp)
			throws IOException, DocumentException, NoImageProviderException {
		String StaticFilePath = global.StaticFilePath;
		String htmlStr = StaticFilePath+"/html/loan_application.html";
		htmlStr = PdfFieldReplace.contentReplacement(htmlStr, pdfCaseData, dropdownData, mp);

		createPdf(file, htmlStr);
	}

	public void createRejectPdf(Path file, JSONObject pdfCaseData, List<DropdownData> dropdownData)
			throws IOException, DocumentException, NoImageProviderException {
		String StaticFilePath = global.StaticFilePath;
		String htmlStr = StaticFilePath+"/html/loan_application.html";
		htmlStr = PdfFieldReplace.rejectContentReplacement(htmlStr, pdfCaseData, dropdownData);

		createPdf(file, htmlStr);
	}

	// public void createConractPdf(Path file, String htmlStr, String userSign, String productId)
	// 		throws IOException, DocumentException, NoImageProviderException {

	// 	htmlStr = pdfFieldReplace.contractContentReplacement(htmlStr, userSign);
	// 	createPdf(file, htmlStr, productId);
	// }

	public String getConractHtml(String htmlStr, String custInfo)
			throws IOException, DocumentException, NoImageProviderException {
		return pdfFieldReplace.contractHtmlReplacement(htmlStr, custInfo);

	}

	public void createPdf(Path file, String htmlStr) throws IOException, DocumentException, NoImageProviderException {
		this.createPdf(file, htmlStr, "");
	}

	public void createPdf(Path file, String htmlStr, String productId)
			throws IOException, DocumentException, NoImageProviderException {

		// step 1
		Document document = new Document();
		// step 2
		PdfWriter writer = PdfWriter.getInstance(document, Files.newOutputStream(file));

		Footer event = new Footer();
		event.productId = productId;
		writer.setPageEvent(event);

		// step 3
		document.open();
		// step 4

		// CSS
		CSSResolver cssResolver = XMLWorkerHelper.getInstance().getDefaultCssResolver(true);

		TagProcessorFactory tagProcessorFactory = Tags.getHtmlTagProcessorFactory();
		tagProcessorFactory.removeProcessor(HTML.Tag.IMG);
		tagProcessorFactory.addProcessor(new ImageTagProcessor(), HTML.Tag.IMG);

		// HTML
		HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
		htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
		htmlContext.setImageProvider(new Base64ImageProvider());

		// Pipelines
		PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
		HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
		CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);

		// XML Worker
		XMLWorker worker = new XMLWorker(css, true);
		XMLParser p = new XMLParser(worker);

		// read file before parsing

		// String htmlStr =
		// readHtmlFile(HTML2PDF.class.getResource("/html/loan_application.html"));
		// System.out.println("htmlStr:"+htmlStr);

		// htmlStr = pdfFieldReplace.contentReplacement(htmlStr, pdfCaseData,
		// dropdownData); // 進行資料置換填值
		// System.out.println("FieldReplacehtmlStr:"+htmlStr);
		p.parse(new ByteArrayInputStream(htmlStr.getBytes("UTF-8")), Charset.forName("UTF-8"));

		// step 5
		document.close();
	}

	public static void doEncryptPdf(Path sFilePath, Path tFilePath, String USER_PWD) {
		InputStream fis;
		OutputStream fos;

		try {
			fis = Files.newInputStream(sFilePath);
			fos = Files.newOutputStream(tFilePath);

			PdfReader reader = new PdfReader(fis);
			PdfStamper stamper = new PdfStamper(reader, fos);
			stamper.setEncryption(USER_PWD.getBytes(), USER_PWD.getBytes(),
					PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_256);
			stamper.close();
		} catch (FileNotFoundException e) {
//			e.printStackTrace();
		} catch (DocumentException e) {
//			e.printStackTrace();
		} catch (IOException e) {
//			e.printStackTrace();
		}
	}

	public static String readHtmlFile(URL url) throws IOException {
		// 由於原做法因為不明原因 會無法讀取到Html 故先改用IOUtils
		return IOUtils.toString(url, Charset.forName("UTF-8"));
	}
	
	public static String readHtmlFromFilePath(InputStream path) throws IOException {
		return IOUtils.toString(path, Charset.forName("UTF-8"));
	}

	class Footer extends PdfPageEventHelper {
		public String productId = "";
		public PdfTemplate total;

		public void onEndPage(PdfWriter writer, Document document) {
			Font cFont = new Font(Font.FontFamily.UNDEFINED, 10, Font.ITALIC);
			
			PdfPTable table = new PdfPTable(4);
			try {
				table.setWidths(new int[] { 20, 2, 2, 20 });
				table.setTotalWidth(512);
				table.getDefaultCell().setFixedHeight(20);
				table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

				String ttfPath = globalConfig.PDF_FONT_WATER_TTF();
				BaseFont bfc;
				try {
					bfc = BaseFont.createFont(ttfPath, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
					cFont = new Font(bfc, 10, Font.NORMAL);
				} catch (IOException e1) {
//					e1.printStackTrace();
				}

				table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				table.addCell("");
				String footerKind = globalConfig.findContract_Footer(productId);
				Phrase footer = new Phrase(footerKind, cFont);
				
				table.addCell(String.format(" %d /", writer.getPageNumber()));
				PdfPCell cell = new PdfPCell(Image.getInstance(total));
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
				table.addCell(footer);
				table.writeSelectedRows(0, -1, 34, 30, writer.getDirectContent());
			} catch (DocumentException de) {
				throw new ExceptionConverter(de);
			}
		}

		public void onOpenDocument(PdfWriter writer, Document document) {
			total = writer.getDirectContent().createTemplate(30, 16);
		}

		public void onCloseDocument(PdfWriter writer, Document document) {
			ColumnText.showTextAligned(total, Element.ALIGN_LEFT, new Phrase(String.valueOf(writer.getPageNumber())), 2,
					2, 0);
		}
	}

	/**
	 * @param sFilePath
	 * @return base64 String PDF轉換成Base64的String
	 */
	public static String encodeFromFile(String sFilePath) {
		return Base64.encodeFromFile(sFilePath);
	}

	/**
	 * @param base64Str
	 * @param tFilePath
	 * @return 是否 decode成功
	 * @throws Exception Base64的String轉換成指定檔案(PDF)
	 */
	public static boolean decodeToFile(String base64Str, String tFilePath) throws Exception {
		return Base64.decodeToFile(base64Str, tFilePath);
	}

	class Base64ImageProvider extends AbstractImageProvider {

		@Override
		public Image retrieve(String src) {
			int pos = src.indexOf("base64,");
			try {
				if (src.startsWith("data") && pos > 0) {
					byte[] img = Base64.decode(src.substring(pos + 7));
					return Image.getInstance(img);
				} else {
					return Image.getInstance(src);
				}
			} catch (BadElementException ex) {
//				ex.printStackTrace();
				return null;
			} catch (IOException ex) {
//				ex.printStackTrace();
				return null;
			}
		}

		@Override
		public String getImageRootPath() {
			return null;
		}
	}
}
