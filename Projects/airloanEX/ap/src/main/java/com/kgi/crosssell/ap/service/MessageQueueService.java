package com.kgi.crosssell.ap.service;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;

import javax.xml.transform.TransformerException;

import com.fuco.prod.common.util.GlobalProp;
import com.ibm.mq.MQException;
import com.ibm.tw.commons.net.mq.CMQStrMessage;
import com.ibm.tw.commons.net.mq.MQReceiver;
import com.ibm.tw.commons.net.mq.MQSender;
import com.ibm.tw.commons.util.ConvertUtils;
import com.kgi.airloanex.ap.dao.APSDao;
import com.kgi.crosssell.ap.xml.CreateXML;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

@Service
public class MessageQueueService {
	@Autowired
	private APSDao apsDao;

	GlobalProp prop = GlobalProp.getInstance();

	private MQSender getPCode2566Sender() throws MQException {
		// DebugUtil.trace(netbanklog, "initSender called");
		MQSender sender = new MQSender();
		sender.setMQEnvironment(prop.getString("PCODE2566.HostName"), prop.getInt("PCODE2566.Port"),
				prop.getString("PCODE2566.Channel"));
		sender.connect(prop.getString("PCODE2566.QueueManagerName"), prop.getString("PCODE2566.QueueSendName"));
		return sender;
	}

	private MQReceiver getPCode2566SReceiver() throws MQException {
		// DebugUtil.trace(netbanklog, "initReceiver called");
		MQReceiver receiver = new MQReceiver();
		receiver.setMQEnvironment(prop.getString("PCODE2566.HostName"), prop.getInt("PCODE2566.Port"),
				prop.getString("PCODE2566.Channel"));
		receiver.connect(prop.getString("PCODE2566.QueueManagerName"), prop.getString("PCODE2566.QueueReceiveName"));
		return receiver;
	}

	public String sendMessageQueue(String rqObject, MQSender sender, MQReceiver receiver) {
		String returnObject = null;
		if (sender == null || receiver == null) {
			throw new RuntimeException("尚未初始化Sender或Receier");
		}
		try {
			byte[] bytes = rqObject.getBytes("UTF8");
			CMQStrMessage message = new CMQStrMessage();
			message.setCCSID(1208);
			message.setMessageData(bytes);
			sender.send(message);
			CMQStrMessage returnMessage = (CMQStrMessage) receiver.receive(message.getMessageId(), 30000);

			returnObject = ConvertUtils.bytes2Str(returnMessage.getMessageData(), "UTF8");
		} catch (Exception e) {
			// e.printStackTrace();
		} finally {
			if (sender != null) {
				sender.close();
			}
			if (receiver != null) {
				receiver.close();
			}
		}
		return returnObject;
	}

	// 是否啟額 CSMT18R1Q
	public String qryGMHasLimitStart(String idno) throws MQException {

		String rsp = "";
		String startTime = "";
		String startTimeInYYYYMMDD = DateUtil.GetDateFormatString("yyyyMMdd");
		;
		String endTime = "";
		String txnId = "GMSCSMT18R1Q";

		String TxnSvcRqContent = "<SvcRq xsi:type=\"gm:" + txnId + "SvcRqType\"><SI1801>" + idno
				+ "</SI1801><SI1802></SI1802><SI1803>" + startTimeInYYYYMMDD + "</SI1803></SvcRq>";

		try {
			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			MQSender sender = getPCode2566Sender();
			MQReceiver receiver = getPCode2566SReceiver();

			String xml = getCSMT18R1QXML(txnId, idno);

			xml = xml.replace("<replacementcontent/>", TxnSvcRqContent);

			rsp = sendMessageQueue(xml, sender, receiver);

			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			rsp = errors.toString();
		} finally {
			apsDao.insertApiLog("/ESB/Send", TxnSvcRqContent, rsp, startTime, endTime);
		}
		return rsp;

	}

	public String getCSMT18R1QXML(String txnId, String idno) {

		CreateXML xFile = new CreateXML();
		String clientID = prop.getString("PCODE2566.ClientId");
		String clientMagicD = prop.getString("PCODE2566.ClientPWD");
		String timeNow = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		clientMagicD = DigestUtils.md5Hex(clientMagicD + timeNow);

		Element Envelope = xFile.setRootTag("SOAP-ENV:Envelope");
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("xmlns:gm", "http://www.cosmos.com.tw/gm");
		map.put("xmlns:bns", "http://www.cosmos.com.tw/kgib");
		map.put("xmlns:crc", "http://www.cosmos.com.tw/crc");
		map.put("xmlns:fep", "http://www.cosmos.com.tw/fep");
		map.put("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
		map.put("xmlns:arws", "http://www.cosmos.com.tw/arws");
		map.put("xmlns:cdi", "http://www.cosmos.com.tw/cdi");
		map.put("xmlns:aml", "http://www.cosmos.com.tw/aml");
		map.put("xmlns:esb", "http://www.cosmos.com.tw/esb");
		map.put("xmlns:kgib", "http://www.cosmos.com.tw/kgib");
		map.put("xmlns:as400", "http://www.cosmos.com.tw/as400");
		map.put("xmlns:nefx", "http://www.cosmos.com.tw/nefx");
		map.put("xmlns:cics", "http://www.cosmos.com.tw/cics");
		map.put("xmlns:cardpac", "http://www.cosmos.com.tw/cardpac");
		map.put("xmlns:cactx", "http://www.cosmos.com.tw/cactx");
		map.put("xmlns:bill", "http://www.cosmos.com.tw/bill");
		map.put("xmlns:fund", "http://www.cosmos.com.tw/fund");
		map.put("xmlns:memo", "http://www.cosmos.com.tw/memo");
		map.put("xmlns:mix", "http://www.cosmos.com.tw/mix");
		map.put("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		map.put("xmlns:SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
		xFile.setAttribute(Envelope, map);

		xFile.setChildTagNoTextNode(Envelope, "SOAP-ENV:Header");
		Element Body = xFile.setChildTagNoTextNode(Envelope, "SOAP-ENV:Body");
		Element esb = xFile.setChildTagNoTextNode(Body, "esb:MsgRq");
		Element Header = xFile.setChildTagNoTextNode(esb, "Header");

		xFile.setChildHasTextNode(Header, "ClientId", clientID);
		xFile.setChildHasTextNode(Header, "ClientPwd", clientMagicD);

		clientMagicD = null;

		xFile.setChildHasTextNode(Header, "ClientDt", timeNow);
		xFile.setChildHasTextNode(Header, "TxnId", txnId);
		xFile.setChildHasTextNode(Header, "UUID", clientID + DateUtil.GetDateFormatString("yyyyMMddHHmmssSSS"));

		xFile.setChildTagNoTextNode(esb, "replacementcontent");

		String result = "";
		try {
			result = xFile.toXMLFile();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		return result;

	}

}
