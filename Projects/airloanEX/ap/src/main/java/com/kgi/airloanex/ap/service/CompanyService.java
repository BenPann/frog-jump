package com.kgi.airloanex.ap.service;

import com.google.gson.Gson;
import com.kgi.airloanex.common.dto.customDto.CompanyFullNameRspDto;
import com.kgi.airloanex.common.dto.customDto.CompanyInfoRspDto;
import com.kgi.airloanex.common.dto.response.CompanyFullNameResp;
import com.kgi.airloanex.common.dto.response.CompanyInfoResp;
import com.kgi.airloanex.common.dto.view.CompanyFullNameView;
import com.kgi.airloanex.common.dto.view.CompanyInfoView;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.dto.WebResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ConfigDao configDao;

    @Autowired
    private KgiService kgiService;

    public String companyFullName(String uniqId, String reqJson) {
        try {
            String take = "10"; // 預設10筆
            CompanyFullNameView view = new Gson().fromJson(reqJson, CompanyFullNameView.class);
            CompanyFullNameResp resp = new CompanyFullNameResp();
            CompanyFullNameRspDto rsp = kgiService.call_company_full_name(uniqId, view.getShortName(), take);
            BeanUtils.copyProperties(rsp, resp);
            return WebResult.GetResultString(0, "成功", resp);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    public String companyInfo(String uniqId, String reqJson) {
        try {
            CompanyInfoView view = new Gson().fromJson(reqJson, CompanyInfoView.class);
            CompanyInfoResp resp = new CompanyInfoResp();
            CompanyInfoRspDto rsp = kgiService.call_company_info(uniqId, view.getCompanyId(), view.getCompanyName());
            BeanUtils.copyProperties(rsp, resp);
            return WebResult.GetResultString(0, "成功", resp);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }
}
