package com.kgi.eopend3.ap.dao;

import java.util.List;

import com.kgi.eopend3.common.dto.db.EntryData;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class EntryDataDao extends CRUDQDao<EntryData> {

	@Override
	public int Create(EntryData fullItem) {
		String sql = "INSERT INTO EntryData (UniqId, UniqType, Entry, Member, Process, Browser, Platform, OS, Other, UTime) VALUES (?,?,?,?,?,?,?,?,?,GETDATE())";
		return this.getJdbcTemplate().update(sql, new Object[]{
				fullItem.getUniqId(), fullItem.getUniqType(),fullItem.getEntry(),fullItem.getMember(),fullItem.getProcess(),fullItem.getBrowser(),
				fullItem.getPlatform(),fullItem.getOS(),fullItem.getOther()
		});
	}

	@Override
	public EntryData Read(EntryData keyItem) {
		String sql = "Select * from EntryData where UniqId = ? ";
		try {
			EntryData rtn = this.getJdbcTemplate().queryForObject(sql, new Object[]{keyItem.getUniqId()}, new BeanPropertyRowMapper<>(EntryData.class));
			return rtn;
		} catch (Exception e) {
			// ignore
		}
		return null;
	}

	@Override
	public int Update(EntryData fullItem) {
		String sql = "UPDATE EntryData SET UniqId=:UniqId, UniqType=:UniqType, Entry=:Entry, Member=:Member, Process=:Process, Browser=:Browser, Platform=:Platform, OS=:OS, Other=:Other, UTime=GETDATE() WHERE UniqId=:UniqId ";
		return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
	}

	@Override
	public int Delete(EntryData keyItem) {
		String sql = "Delete From EntryData Where UniqId = ? ";
		return this.getJdbcTemplate().update(sql, new Object[]{ keyItem.getUniqId() });
	}

	@Override
	public List<EntryData> Query(EntryData keyItem) {
		return null;
	}	  
}
