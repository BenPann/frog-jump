
package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.CaseDocument;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class CaseDocumentDao extends CRUDQDao<CaseDocument> {

    @Override
    public int Create(CaseDocument fullItem) {
        String sql = "INSERT INTO CaseDocument VALUES(?,?,?,?,?,?,GETDATE(),GETDATE())";
        return this.getJdbcTemplate().update(sql, fullItem.getUniqId(), fullItem.getUniqType(),
                fullItem.getDocumentType(), fullItem.getContent(), fullItem.getVersion(), fullItem.getOther());
    }

    @Override
    public CaseDocument Read(CaseDocument keyItem) {
        String sql = "SELECT * FROM CaseDocument WHERE UniqId = ? AND DocumentType = ?;";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseDocument.class),
                    keyItem.getUniqId(), keyItem.getDocumentType());
        } catch (DataAccessException ex) {
            System.out.println("CaseDocument查無資料");
            return null;
        }
    }

    @Override
    public int Update(CaseDocument fullItem) {
        String sql = "UPDATE CaseDocument SET Content = ?, Version = ?,Other = ?, UpdateTime = GETDATE() WHERE UniqId = ? AND DocumentType = ?;";
        return this.getJdbcTemplate().update(sql, fullItem.getContent(), fullItem.getVersion(), fullItem.getOther(),
                fullItem.getUniqId(), fullItem.getDocumentType());
    }

    @Override
    public int Delete(CaseDocument keyItem) {
        String sql = "DELETE CaseDocument WHERE UniqId = ? AND DocumentType = ?;";
        return this.getJdbcTemplate().update(sql, keyItem.getUniqId(), keyItem.getDocumentType());
    }

    @Override
    public List<CaseDocument> Query(CaseDocument keyItem) {
        String sql = "SELECT * FROM CaseDocument WHERE UniqId = ? AND DocumentType like ?+'.%'";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getUniqId(), keyItem.getDocumentType() },
                    new BeanPropertyRowMapper<>(CaseDocument.class));
        } catch (DataAccessException ex) {
//            ex.printStackTrace();
            return null;
        }
    }

    public CaseDocument findByUniqIdAndDocumentType(String uniqId, String documentType) {
        CaseDocument keyItem = new CaseDocument();
        keyItem.setUniqId(uniqId);
        keyItem.setDocumentType(documentType);
        return Read(keyItem);
    }

    public boolean isExist(String uniqId, String documentType) {
        CaseDocument fullItem = findByUniqIdAndDocumentType(uniqId, documentType);
        return (fullItem != null);
    }
}
