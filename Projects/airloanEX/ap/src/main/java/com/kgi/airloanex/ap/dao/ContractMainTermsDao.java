package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.ContractMainTerms;
import com.kgi.eopend3.ap.dao.CRUDQDao;
import com.kgi.eopend3.ap.dao.TermsDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class ContractMainTermsDao extends CRUDQDao<ContractMainTerms> {
	public static final String NAME = "ContractMainTerms" ;

    @Override
    public int Create(ContractMainTerms fullItem) {
        String sql = "INSERT INTO " + NAME + " (UniqId,TermSerial,IsAgree,CreateTime) VALUES (?,?,?,getDate()) ";
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getUniqId(),fullItem.getTermSerial(),fullItem.getIsAgree() });
    }

    @Override
    public ContractMainTerms Read(ContractMainTerms keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE UniqId = ? and TermSerial = ? ";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(ContractMainTerms.class),
                    new Object[] { keyItem.getUniqId(),keyItem.getTermSerial() });
        } catch (DataAccessException ex) {
			logger.error(NAME + "查無資料");
            return null;
        }
    }

    @Override
    public int Update(ContractMainTerms fullItem) {
        String sql = "Update " + NAME + " Set IsAgree = ? where UniqId = ? and TermSerial = ?  ";
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getIsAgree(),fullItem.getUniqId(),fullItem.getTermSerial() });
    }

    @Override
    public int Delete(ContractMainTerms keyItem) {
        return 0;
    }

    public ContractMainTerms getContractMainTermsByTermsName(String uniqId, String termsName) {
        String sql = "SELECT E.* FROM " + NAME + " E inner join " +  TermsDao.NAME + " T on E.TermSerial=T.Serial "
        			+ " WHERE E.UniqId = ? and T.Name = ? and T.Status='03' ";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(ContractMainTerms.class),
                    new Object[] { uniqId, termsName });
        } catch (DataAccessException ex) {
			logger.error(NAME + "查無資料");
            return null;
        }
    }

    @Override
    public List<ContractMainTerms> Query(ContractMainTerms keyItem) {
        return null;
    }

}