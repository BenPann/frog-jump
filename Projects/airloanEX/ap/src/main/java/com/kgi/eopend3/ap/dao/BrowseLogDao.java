package com.kgi.eopend3.ap.dao;

import java.util.List;

import com.kgi.eopend3.common.dto.db.BrowseLog;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class BrowseLogDao extends CRUDQDao<BrowseLog> {

	public static final String NAME = "BrowseLog";

	public BrowseLog findLatestPageByUniqId(String uniqId) {
        String sql = "select top 1 * from BrowseLog where UniqId = ? order by UTime desc ";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new Object[] { uniqId },
                    new BeanPropertyRowMapper<>(BrowseLog.class));
        } catch (DataAccessException ex) {
            return null;
        }
	}

	@Override
	public int Create(BrowseLog fullItem) {
		String sql = "INSERT INTO " + NAME + " (UniqId, UniqType, Page, Action, IPAddress, UTime) VALUES (?, ?, ?, ?, ?, GETDATE())";
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getUniqId(), fullItem.getUniqType(), fullItem.getPage(), fullItem.getAction(), fullItem.getIPAddress() });
	}

	@Override
	public BrowseLog Read(BrowseLog keyItem) {
		return null;
	}

	@Override
	public int Update(BrowseLog fullItem) {
		return 0;
	}

	@Override
	public int Delete(BrowseLog keyItem) {
		return 0;
	}

	@Override
	public List<BrowseLog> Query(BrowseLog keyItem) {
		return null;
	}

	public int CreateBrowseLog(String uniqId, String uniqType, String page, String action) {

		String sql = "INSERT INTO BrowseLog (UniqId, UniqType, Page, Action, UTime) VALUES (?,?,?,?,GETDATE())";
		return this.getJdbcTemplate().update(sql,
                new Object[] { uniqId, uniqType, page, action });
	
	}

}
