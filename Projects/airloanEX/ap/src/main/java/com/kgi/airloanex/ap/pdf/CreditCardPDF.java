package com.kgi.airloanex.ap.pdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.CS_VerifyLogDao;
import com.kgi.airloanex.ap.dao.CaseAuthDao;
import com.kgi.airloanex.ap.dao.CaseDocumentDao;
import com.kgi.airloanex.ap.dao.CreditCaseDataDao;
import com.kgi.airloanex.ap.dao.QRCodeDao;
import com.kgi.airloanex.ap.dao.QR_ChannelListDao;
import com.kgi.airloanex.ap.service.ChannelDepartService;
import com.kgi.airloanex.ap.service.SendMailHunterService;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.db.CS_VerifyLog;
import com.kgi.airloanex.common.dto.db.CaseAuth;
import com.kgi.airloanex.common.dto.db.CaseDocument;
import com.kgi.airloanex.common.dto.db.CreditCaseDataExtend;
import com.kgi.airloanex.common.dto.db.CreditEopDataDto;
import com.kgi.airloanex.common.dto.db.QR_ChannelDepartList;
import com.kgi.airloanex.common.dto.db.QR_ChannelList;
import com.kgi.airloanex.common.util.ExportUtil;
import com.kgi.eopend3.ap.config.GlobalConfig;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.ap.dao.DropdownDao;
import com.kgi.eopend3.ap.service.APSService;
import com.kgi.eopend3.common.util.CommonFunctionUtil;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class CreditCardPDF extends BasePDFDocument {

	private static final Logger logger = LogManager.getLogger(CreditCardPDF.class.getName());

	@Autowired
    private GlobalConfig global;
    
	@Autowired
	private AirloanEXConfig globalConfig;

	@Autowired
	private CreditCaseDataDao creditCaseDataDao;
	@Autowired
    private QR_ChannelListDao channelListDao;
	@Autowired
	private QRCodeDao qRCodeDao;

	@Autowired
	private DropdownDao dropdao;

	@Autowired
	private ChannelDepartService channelDepartService;
	@Autowired
	private SendMailHunterService sendMailHunterService;

	@Autowired
	private CaseAuthDao caseAuthDao;
	
	@Autowired
	private CS_VerifyLogDao cs_VerifyLogDao;

	@Autowired
    private APSService apsService;

	@Autowired
	private CaseDocumentDao caseDocumentDao;

	@Autowired
	private ConfigDao configDao;

	@Autowired
    private PageFooter footer;

	private String _uniqId = "";

	private CreditCaseDataExtend cd = null;
    private String templateVersion = "";
    private List<Map<String, Object>> ccStaging = null;
	public CreditCardPDF(ApplicationContext appContext) {
		super(appContext);
	}

	public void init(String... params) throws IOException {
		this._uniqId = params[0];
		// 是否加密
		this.setUseEncrepyt(true);
		// this.setEncryptPassword(idno);
		// 是否使用浮水印
		this.setUseWaterMark(true);
		this.setWaterMarkText(globalConfig.IMG_WATER_FONT());
		this.setWaterMarkFont(globalConfig.PDF_FONT_WATER_TTF());
		// 是否要PDF轉成圖片
		this.setPdfToImage(false);
		// 是否存DB
		this.setUseSaveToDB(true);
		// 是否發信
		this.setUseSendEMail(true);
		// 是否設定每頁的事件
		this.setPageEvent(footer);
		// 取得Html的方法
		String StaticFilePath = global.StaticFilePath;
		
		// mbc與其他流程需讀取不同位置
		String htmlUrl ="";
		CaseAuth caseAuth = caseAuthDao.Read(new CaseAuth(_uniqId));
		if(caseAuth !=null  && PlContractConst.CASEAUTH_AUTH_TYPE_MBC.equals(caseAuth.getAuthType())) {
			htmlUrl = StaticFilePath + globalConfig.PDF_Credit_MBC_TemplateFilePath();
			templateVersion = globalConfig.PDF_Credit_MBC_VerNo();
		}else {
			htmlUrl = StaticFilePath + globalConfig.PDF_Credit_TemplateFilePath();
			templateVersion = globalConfig.PDF_Credit_VerNo();
		}
		System.out.println("===========================");
		System.out.println("StaticFilePath :" + StaticFilePath);
		System.out.println("htmlUrl :" + htmlUrl);
		System.out.println("===========================");
		InputStream input = new FileInputStream(new File(htmlUrl));
		this.setRawHtml(this.getContentFromFilePath(input));
	}

	@Override
	public void beforePdfProcess() {

	}

	@Override
	public String replaceField(String raw) {
		logger.info("### Begin - replaceField");
		// 取主要資料
		cd = creditCaseDataDao.getCreditDataExtendById(_uniqId);
		// 先把密碼設定好
		this.setEncryptPassword(cd.getIdno());
		// 取推廣人員
		/*ChannelData cData = qRCodeDao.getPromoCase(_uniqId);
		if (cData == null) {
			cData = new ChannelData();
		}*/
		// 2019/03/26 取ChannelName
		String channelName = "";
		String channelID = channelDepartService.getFinalChannelID(_uniqId);
		QR_ChannelList qr_ChannelList = new QR_ChannelList(channelID);
		qr_ChannelList = channelListDao.Read(qr_ChannelList);
		if(qr_ChannelList!=null) {
			channelName = qr_ChannelList.getChannelName();
		}
		/*ChannelData cData2 = qRCodeDao.getPromote(_uniqId);
		if (cData2 != null) {
			cData.setChannelName(cData2.getChannelName());
		}*/

		// 取得首刷禮
		List<Map<String, Object>> ms = creditCaseDataDao.getCardGiftName(cd.getFirstPresent(), cd.getProductId());
		Map<String, Object> mapCardTitle = new HashMap<String, Object>();
		if (ms.size() == 0) {
			mapCardTitle.put("CardTitle", "");
			mapCardTitle.put("gift", "");
		} else {
			mapCardTitle = ms.get(0);
			if (mapCardTitle.get("gift") == null) {
				mapCardTitle.put("gift", "");
			}
		}
		cd.setFirstPresent(mapCardTitle.get("gift").toString());

		// 將下拉選單換成中文
		// DropdownDao dropdao = new DropdownDao();
		cd.setOccupation(dropdao.getDataNameByNameAndDataKey("occupation", cd.getOccupation()));

		cd.setEducation(dropdao.getDataNameByNameAndDataKey("cceducation", cd.getEducation()));

		cd.setSendType(dropdao.getDataNameByNameAndDataKey("billType", cd.getSendType()));

		cd.setHomeOwner(dropdao.getDataNameByNameAndDataKey("estate", cd.getHomeOwner()));

		cd.setMarriage(dropdao.getDataNameByNameAndDataKey("creditmarriage", cd.getMarriage()));

		// 是否通過線上驗身
		String CreditVerify = "0";
		List<Map<String, Object>> tempCreditVerify = creditCaseDataDao.getCreditVerify(_uniqId);
		if (tempCreditVerify != null && tempCreditVerify.size() != 0) {
			Map<String, Object> tempMp = tempCreditVerify.get(0);
			CreditVerify = tempMp.get("VerifyStatus").toString().trim();
		}
		// 判斷是否為跨售資料，如果是跨售需要修改auth_type(驗身方式)
		CS_VerifyLog cs_VerifyLog = new CS_VerifyLog();
		cs_VerifyLog.setUniqId(cd.getUniqId());
		cs_VerifyLog = cs_VerifyLogDao.Read(cs_VerifyLog);
		if (cs_VerifyLog != null) {
			CreditVerify = PlContractConst.VERIFY_STATUS_SUCCESS ;
		}
		// 新戶且線上驗身沒過 將該欄位隱藏
		if (CreditVerify.equals(PlContractConst.VERIFY_STATUS_FAIL) 
		&& (cd.getUserType().trim().equals(PlContractConst.CREDITCASEDATA_USERTYPE_NEW) 
			|| cd.getUserType().trim().equals(PlContractConst.CREDITCASEDATA_USERTYPE_OTHER))) {
			raw = raw.replace("%nosign_class%", "hidden");
		}
		// × 已通過線上驗身者免簽名

		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date date1 = new Date();
		String strDate = formatDate.format(date1);
		java.util.Date date;
		try {
			date = formatDate.parse(strDate);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			int[] tempTime = { calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1,
					calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY),
					calendar.get(Calendar.MINUTE), };
			raw = raw.replace("%year%", String.valueOf(tempTime[0]));
			raw = raw.replace("%month%", StringUtils.leftPad(String.valueOf(tempTime[1]), 2, "0"));
			raw = raw.replace("%day%", StringUtils.leftPad(String.valueOf(tempTime[2]), 2, "0"));
			raw = raw.replace("%hour%", StringUtils.leftPad(String.valueOf(tempTime[3]), 2, "0"));
			raw = raw.replace("%minute%", StringUtils.leftPad(String.valueOf(tempTime[4]), 2, "0"));

			raw = raw.replace("%user_type%", cd.getUserType());

		} catch (ParseException e) {
//			e.printStackTrace();
		}

		raw = raw.replace("%prod_name%",
				mapCardTitle.get("CardTitle") != null ? mapCardTitle.get("CardTitle").toString() : "");
		raw = raw.replace("%first_present%",
				mapCardTitle.get("gift") != null ? mapCardTitle.get("gift").toString() : "");
		raw = raw.replace("%user_c_name%", cd.getChtName());
		raw = raw.replace("%user_e_name%", cd.getEngName());
		raw = raw.replace("%user_idno%", cd.getIdno());

		raw = raw.replace("%idcard_issue_city%", cd.getIdCardLocation());

		String tempIdCardCRecord = "", IdCardCRecord = "";
		tempIdCardCRecord = cd.getIdCardCRecord();
		switch (tempIdCardCRecord) {
		case ("0"):
			IdCardCRecord = "初領";
			break;
		case ("1"):
			IdCardCRecord = "補領";
			break;
		case ("2"):
			IdCardCRecord = "換發";
			break;
		}
		raw = raw.replace("%idcard_issue_type%", IdCardCRecord);
		String IdDate = "";
		IdDate = cd.getIdCardDate().length() > 10 ? cd.getIdCardDate().substring(0, 10) : cd.getIdCardDate();
//		System.out.println("IDATE:" + IdDate);
		if (IdDate.indexOf("1900") >= 0) {
			IdDate = "";
		} else {
			// 修改成民國
			IdDate = String.valueOf(Integer.valueOf(IdDate.substring(0, 4)) - 1911)
					.concat(IdDate.substring(4, IdDate.length()));
			IdDate = "民國" + IdDate.replaceAll("-", "/");
		}
		raw = raw.replace("%idcard_issue_date%", IdDate);
		String birthday = cd.getBirthday();
		birthday = String.valueOf(Integer.valueOf(birthday.substring(0, 4)) - 1911).concat("/")
				.concat(birthday.substring(4, 6).concat("/").concat(birthday.substring(6, birthday.length())));
		raw = raw.replace("%user_birthday%", "民國" + birthday);
//		System.out.println("cd.getBirthday():" + cd.getBirthday());
		// 性別
		String tempGender = "";
		tempGender = cd.getGender();
		raw = raw.replace("%user_sex%", tempGender.equals("1") ? "男" : "女");

		raw = raw.replace("%user_marrige%", cd.getMarriage());

		raw = raw.replace("%user_mobile%", cd.getPhone());
		raw = raw.replace("%user_education%", cd.getEducation());
		raw = raw.replace("%user_house_type%", cd.getHomeOwner());

		String tempTel = "";
		if (!cd.getResTel().equals("")) {
			tempTel = "(" + cd.getResTelArea() + ")" + cd.getResTel();
		}
		raw = raw.replace("%user_house_tel%", tempTel);
		String tempHTel = "";
		if (!cd.getHomeTel().equals("")) {
			tempHTel = "(" + cd.getHomeTelArea() + ")" + cd.getHomeTel();
		}
		raw = raw.replace("%user_live_tel%", tempHTel);
		// 戶籍地址
		raw = raw.replace("%user_house_zipcode%", cd.getResAddrZipCode());
		raw = raw.replace("%user_house_addr%", cd.getResAddr().replaceAll(";", ""));
		// 居住地址
		raw = raw.replace("%user_live_zipcode%", cd.getHomeAddrZipCode());
		raw = raw.replace("%user_live_addr%", cd.getHomeAddr().replaceAll(";", ""));
		// 寄送地址
		raw = raw.replace("%user_conmm_zipcode%", cd.getCommAddrZipCode());
		raw = raw.replace("%user_conmm_addr%", cd.getCommAddr().replaceAll(";", ""));
		raw = raw.replace("%user_email%", cd.getEmailAddress());
		// 寄送類型
		String sentType = "";
		sentType = cd.getSendType();

		raw = raw.replace("%send_type%", sentType);
		raw = raw.replace("%corp_name%", cd.getCorpName());
		raw = raw.replace("%corp_career_type%", cd.getOccupation());
		raw = raw.replace("%corp_dept%", cd.getCorpDepart());
		raw = raw.replace("%corp_job_title%", cd.getJobTitle());

		String tempCTel = "";
		tempCTel = "(" + cd.getCorpTelArea() + ")" + cd.getCorpTel() + " 分機 :" + cd.getCorpTelExten();
		raw = raw.replace("%corp_tel%", tempCTel);

		raw = raw.replace("%corp_onbord_date%", "民國 " + getMinkuo(cd.getOnBoardDate()));
		raw = raw.replace("%income%", String.valueOf(cd.getYearlyIncome()));

		raw = raw.replace("%corp_addr_zipcode%", cd.getCorpAddrZipCode());
		raw = raw.replace("%corp_addr%", cd.getCorpAddr().replaceAll(";", ""));

		QR_ChannelList channel = this.channelListDao.getChannelDataByUniqId(_uniqId);
		boolean isBank = true;
		boolean promoMemberIsID = false;
        if (channel != null) {
        	promoMemberIsID = channel.getIdLogic().equals("0");
        	isBank = channel.getChannelType().equals("0");
        } else { // 不是由 ShortUrl 進來的案件(ex: 中壽案件)，會取不到 Channel. 使用前面就取過的 Channel
        	if (qr_ChannelList != null) {
            	promoMemberIsID = qr_ChannelList.getIdLogic().equals("0");
            	isBank = qr_ChannelList.getChannelType().equals("0");
        	} else {
        		// 如果沒有 Channel, 不知道該怎麼辦，先不處理。
        	}
        }
		
		
		QR_ChannelDepartList chanelDepart = this.channelDepartService.getCreditChannelDepart(_uniqId);

		String promoMember1 = chanelDepart.getPromoMember1();
		String promoMember2 = chanelDepart.getPromoMember2();
		if(promoMemberIsID) {
			if(isBank) {
				promoMember1=CommonFunctionUtil.hiddenUserDataWithStar(promoMember1,3,3);
			}else {
				promoMember2=CommonFunctionUtil.hiddenUserDataWithStar(promoMember2,3,3);
			}
        }
		
		raw = raw.replace("%promo_1_unit%", chanelDepart.getPromoDepart1());
		raw = raw.replace("%promo_1_empolyee%", promoMember1);
		if (isBank) {
			raw = raw.replace("%promo_2_unit%", "");
			raw = raw.replace("%promo_1_empo2yee%", "");
		} else {
		raw = raw.replace("%promo_2_unit%", chanelDepart.getPromoDepart2());
		raw = raw.replace("%promo_1_empo2yee%", promoMember2);
		}

		raw = raw.replace("%prj_code%", cd.getProductId());
		raw = raw.replace("%channel_info%", channelName);
		//2019/09/24 增加uniqid
		raw = raw.replace("%UniqId%", this._uniqId);
		// 取得驗證方式
		String tempAuth_type = "";
		// 先看usertype 如果是1234->OTP
		if (!cd.getUserType().toString().equals(PlContractConst.CREDITCASEDATA_USERTYPE_NEW) 
		 && !cd.getUserType().toString().equals(PlContractConst.CREDITCASEDATA_USERTYPE_OTHER)) {
			String authType = creditCaseDataDao.getAuthType(_uniqId);
			tempAuth_type = authType.equals("2") ? PlContractConst.CREDITVERIFY_CHTMBC : PlContractConst.CREDITVERIFY_OTP ;
		} else {
			// 0 表示新戶 查 CreditVerify
			Map<String, Object> map = creditCaseDataDao.getVerifyStatus(_uniqId);
			if (map != null) {
				String type = (String) map.get("VerifyType");		
				String status = (String) map.get("VerifyStatus");
				//[2020.01.08 GaryLiu] ==== START : 因為有多種驗身方式，修改判斷驗身規則 ====
				//String message = type.equals("0") ? "他行卡驗身" : "他行帳戶驗身";
				String message = "";
				if(type != null) {
					if(PlContractConst.CREDITVERIFY_TYPE_NCCC.equals(type)) {
						message = PlContractConst.CREDITVERIFY_NCCC ;
					} else if(PlContractConst.CREDITVERIFY_TYPE_BANKACOUNT.equals(type)) {
						message = PlContractConst.CREDITVERIFY_BANKACOUNT ;
					} else if(PlContractConst.CREDITVERIFY_TYPE_CHTMBC.equals(type)) {
						message = PlContractConst.CREDITVERIFY_CHTMBC ;
					}
				}
				//[2020.01.08 GaryLiu] ====  END  : 因為有多種驗身方式，修改判斷驗身規則 ====
				tempAuth_type = status.equals("1") ? message : "";
			}
		}

		if (cs_VerifyLog != null && (cd.getUserType().trim().equals("0") || cd.getUserType().trim().equals("4"))) {
			String time = cs_VerifyLog.getTime();
			tempAuth_type = "凱基證券憑證驗身 " + cs_VerifyLog.getContractVersion() + "-" + cs_VerifyLog.getMessageSerial()
					+ "<br/>" + time.substring(0, time.length() - 3) + "<br/>" + cs_VerifyLog.getIpAddress();
		}

		raw = raw.replace("%auth_type%", tempAuth_type);
		raw = raw.replace("%user_ip%", cd.getIpAddress());
		
		//2019/07/30 增加薪轉欄位
		CreditEopDataDto creditEopDataDto = creditCaseDataDao.queryEOPData(_uniqId);
		raw = raw.replace("%corp_ent_number%", creditEopDataDto != null ? creditEopDataDto.getCorpID() : "");
		raw = raw.replace("%salary_pay_date%", creditEopDataDto != null ? creditEopDataDto.getPayDate2() != null && !creditEopDataDto.getPayDate2().isEmpty() ? creditEopDataDto.getPayDate1() + "," + creditEopDataDto.getPayDate2() : creditEopDataDto.getPayDate1() : "");

		// 中壽卡分期欄位[2020.03.20 Alen 新增]
		ccStaging = creditCaseDataDao.getCreditCaseStagingByUniqId(_uniqId);
		
		if(ccStaging != null && !ccStaging.isEmpty()) { // null isEmpty : 非中壽
			String isCLCreditCard = ""; 
			String isCheckCLInstallment = ""; 
			String installmentMonth = ""; 
			
			isCLCreditCard = ccStaging.get(0).get("IsCLCreditCard").toString().trim();
			isCheckCLInstallment = ccStaging.get(0).get("IsCheckCLInstallment").toString().trim();
		    if(isCLCreditCard.equals("1") && isCheckCLInstallment.equals("1")) {   // isCheckCLInstallment 1:有分期 0:沒分期
		     	raw = raw.replace("%check_or_uncheck%",
		    		" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJzSURBVGiB7Zo9SBxBGIafCImKKNyhgpIqpEshasDqRBsLG5GEYIqEVCEEQkQUKyOIRYoUdjaWgoXiIdgIFrFLJATEQoRgYwQlBk08JUT8KfY7VmW9m5n9G2VfGGa5/X7e9/ZmZ76ZgwQJEvhBLTAMLAM7wFnEbUdyDwE1piKeA7kYyF/XDoBnuiKeAqcSYBrIACndIAEgBbQCM8LlFOhWda4G/ojj+zDYGaIPh9MekFZxGMF9ErYhi8NtWMV4RYwzYTIyRBsOt+8qxgdiXBEiIVNU4nD7q2Kcf0vYCk9+JTEQCQWJENtgo5DSoALFOdhfAD+BxwVslPnFJaQFOJLcbwvYWS3kIbAreT8VsbVWSApYk5xZio9bK4XcAz5LvmWgXMHHSiETkmsTqFf0sU5Ir+TJAU0aflYJ6QCOcQqlHk3fQIWUAgvAa00SAA24K+xBA/9AhTzBLT3faZCoATbEdxK4o+Grw0/L8A1wIrYfFUhVAN/EfhG4q0LGBz+tMdID/Bf7ca6fA0pwNxDWUay5/fLTHeydwKH4TOH9TY/K/V/AA43YvviZvLUywL74zXN5Ynspn/8jmH2A0F+/jbi7kUtAFdCO+9N7ZRDTFz8/88gjYEv8vwDbcj1mGM8LkU2IdcDqhTizBFvARTqzp3GeyFfUFoI6iHyJkgLuBxDnKqxaa/lBsq91I3CrheSkt3UTG5wy4BK8hGxIr1O1RYVm6X9cveElZE76/tDomGNA+rmCVoKLR299YTEywACaR2/gfRhaFga7IijHx2FoHt3AbwofGUfZdoEuXRF5pIEPOJtn+eIpynaI+4eBOI7HEyS4NTgH3tZKOTc9lX8AAAAASUVORK5CYII=\"/>");

		      	installmentMonth = ccStaging.get(0).get("InstallmentMonth").toString().trim();
		       	raw = raw.replace("%period_num%", installmentMonth);
			} else {
				raw = raw.replace("%check_or_uncheck%","<img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFFSURBVGiB7ZqxSgNBFEUPVopYJKgfonXEP5CACH6FEEypAX9HIeQ3LAR7sQ8ooknaaDFvicqosxPHfcg9MLxi3+zew8IW8xaEEMuwDQyAa2AMvP7xGtuzz4CtXIljYNpA+K/WBDiqK3EIzO0Gl0AHaNW9yS/QAvaAK8syB7qpmzeBZ9t4UiJdJj1CpiegnbLhgsWb8MaQkG2Q0nxrzZ2SiTLZJ2S7SWmeWPN6wUC5bBCyvaQ0V18Jr0TzrTQQpAgS8YZEvCERb0jEGxLxhkS8IRFvSMQbEvGGRLwhEW9IxBsS8UZMZGrV6yE2hIP2D8RE7q3uFIuTz67Vu88XYiIjq6fF4uTTtzr6tst4P3rrlUqUQZ+aozeID0NXS6T7gTWWGIZWdIFHmh9LV+sBOKgrUdEGzglD+1kD4WcsfhhoYjwuxL/hDTwNt7HmQsC4AAAAAElFTkSuQmCC\"/>");
		   	    raw = raw.replace("%period_num%", "");
			}
	     } else {
	    	 raw = raw.replace("%check_or_uncheck%","<img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFFSURBVGiB7ZqxSgNBFEUPVopYJKgfonXEP5CACH6FEEypAX9HIeQ3LAR7sQ8ooknaaDFvicqosxPHfcg9MLxi3+zew8IW8xaEEMuwDQyAa2AMvP7xGtuzz4CtXIljYNpA+K/WBDiqK3EIzO0Gl0AHaNW9yS/QAvaAK8syB7qpmzeBZ9t4UiJdJj1CpiegnbLhgsWb8MaQkG2Q0nxrzZ2SiTLZJ2S7SWmeWPN6wUC5bBCyvaQ0V18Jr0TzrTQQpAgS8YZEvCERb0jEGxLxhkS8IRFvSMQbEvGGRLwhEW9IxBsS8UZMZGrV6yE2hIP2D8RE7q3uFIuTz67Vu88XYiIjq6fF4uTTtzr6tst4P3rrlUqUQZ+aozeID0NXS6T7gTWWGIZWdIFHmh9LV+sBOKgrUdEGzglD+1kD4WcsfhhoYjwuxL/hDTwNt7HmQsC4AAAAAElFTkSuQmCC\"/>");
	    	 raw = raw.replace("%period_num%", "");
	     }
		
		String dataUse = cd.getDataUse() != null ? cd.getDataUse().trim() : "";
		if (dataUse.equals("1") || dataUse.equals("01")) {
			raw = raw.replace("%cosale_agree%",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJzSURBVGiB7Zo9SBxBGIafCImKKNyhgpIqpEshasDqRBsLG5GEYIqEVCEEQkQUKyOIRYoUdjaWgoXiIdgIFrFLJATEQoRgYwQlBk08JUT8KfY7VmW9m5n9G2VfGGa5/X7e9/ZmZ76ZgwQJEvhBLTAMLAM7wFnEbUdyDwE1piKeA7kYyF/XDoBnuiKeAqcSYBrIACndIAEgBbQCM8LlFOhWda4G/ojj+zDYGaIPh9MekFZxGMF9ErYhi8NtWMV4RYwzYTIyRBsOt+8qxgdiXBEiIVNU4nD7q2Kcf0vYCk9+JTEQCQWJENtgo5DSoALFOdhfAD+BxwVslPnFJaQFOJLcbwvYWS3kIbAreT8VsbVWSApYk5xZio9bK4XcAz5LvmWgXMHHSiETkmsTqFf0sU5Ir+TJAU0aflYJ6QCOcQqlHk3fQIWUAgvAa00SAA24K+xBA/9AhTzBLT3faZCoATbEdxK4o+Grw0/L8A1wIrYfFUhVAN/EfhG4q0LGBz+tMdID/Bf7ca6fA0pwNxDWUay5/fLTHeydwKH4TOH9TY/K/V/AA43YvviZvLUywL74zXN5Ynspn/8jmH2A0F+/jbi7kUtAFdCO+9N7ZRDTFz8/88gjYEv8vwDbcj1mGM8LkU2IdcDqhTizBFvARTqzp3GeyFfUFoI6iHyJkgLuBxDnKqxaa/lBsq91I3CrheSkt3UTG5wy4BK8hGxIr1O1RYVm6X9cveElZE76/tDomGNA+rmCVoKLR299YTEywACaR2/gfRhaFga7IijHx2FoHt3AbwofGUfZdoEuXRF5pIEPOJtn+eIpynaI+4eBOI7HEyS4NTgH3tZKOTc9lX8AAAAASUVORK5CYII=\"/>");
		} else {
			raw = raw.replace("%cosale_agree%",
					" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFFSURBVGiB7ZqxSgNBFEUPVopYJKgfonXEP5CACH6FEEypAX9HIeQ3LAR7sQ8ooknaaDFvicqosxPHfcg9MLxi3+zew8IW8xaEEMuwDQyAa2AMvP7xGtuzz4CtXIljYNpA+K/WBDiqK3EIzO0Gl0AHaNW9yS/QAvaAK8syB7qpmzeBZ9t4UiJdJj1CpiegnbLhgsWb8MaQkG2Q0nxrzZ2SiTLZJ2S7SWmeWPN6wUC5bBCyvaQ0V18Jr0TzrTQQpAgS8YZEvCERb0jEGxLxhkS8IRFvSMQbEvGGRLwhEW9IxBsS8UZMZGrV6yE2hIP2D8RE7q3uFIuTz67Vu88XYiIjq6fF4uTTtzr6tst4P3rrlUqUQZ+aozeID0NXS6T7gTWWGIZWdIFHmh9LV+sBOKgrUdEGzglD+1kD4WcsfhhoYjwuxL/hDTwNt7HmQsC4AAAAAElFTkSuQmCC\"/>");
		}

		raw = raw.replace("[credit_checkbox]",
				" <img alt=\"\" style=\"width:20px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJzSURBVGiB7Zo9SBxBGIafCImKKNyhgpIqpEshasDqRBsLG5GEYIqEVCEEQkQUKyOIRYoUdjaWgoXiIdgIFrFLJATEQoRgYwQlBk08JUT8KfY7VmW9m5n9G2VfGGa5/X7e9/ZmZ76ZgwQJEvhBLTAMLAM7wFnEbUdyDwE1piKeA7kYyF/XDoBnuiKeAqcSYBrIACndIAEgBbQCM8LlFOhWda4G/ojj+zDYGaIPh9MekFZxGMF9ErYhi8NtWMV4RYwzYTIyRBsOt+8qxgdiXBEiIVNU4nD7q2Kcf0vYCk9+JTEQCQWJENtgo5DSoALFOdhfAD+BxwVslPnFJaQFOJLcbwvYWS3kIbAreT8VsbVWSApYk5xZio9bK4XcAz5LvmWgXMHHSiETkmsTqFf0sU5Ir+TJAU0aflYJ6QCOcQqlHk3fQIWUAgvAa00SAA24K+xBA/9AhTzBLT3faZCoATbEdxK4o+Grw0/L8A1wIrYfFUhVAN/EfhG4q0LGBz+tMdID/Bf7ca6fA0pwNxDWUay5/fLTHeydwKH4TOH9TY/K/V/AA43YvviZvLUywL74zXN5Ynspn/8jmH2A0F+/jbi7kUtAFdCO+9N7ZRDTFz8/88gjYEv8vwDbcj1mGM8LkU2IdcDqhTizBFvARTqzp3GeyFfUFoI6iHyJkgLuBxDnKqxaa/lBsq91I3CrheSkt3UTG5wy4BK8hGxIr1O1RYVm6X9cveElZE76/tDomGNA+rmCVoKLR299YTEywACaR2/gfRhaFga7IijHx2FoHt3AbwofGUfZdoEuXRF5pIEPOJtn+eIpynaI+4eBOI7HEyS4NTgH3tZKOTc9lX8AAAAASUVORK5CYII=\"/>");

		// 如果是中華電信的 用中華電信的時間
        CaseAuth caseAuth = new CaseAuth(this._uniqId);
        CaseAuth read = caseAuthDao.Read(caseAuth);
        boolean useCht = false;
        if (null != read) {
            useCht = read.getChtAgree().equals("1");
            if (useCht) {
            	String chtAgreeDate ="";
            	String chtAgreeTime ="";
            	// ChtAgreeTime
                if(read.getChtAgreeTime() != null && !"".contentEquals(read.getChtAgreeTime()) ) {
                	chtAgreeDate = read.getChtAgreeTime().split(" ")[0];
                	chtAgreeTime = read.getChtAgreeTime().split(" ")[1];
                }
                if(chtAgreeDate != null && !"".equals(chtAgreeDate) && chtAgreeDate.split("-").length == 3)
                raw = raw.replace("[Apply_date]","中華民國".concat(String.valueOf((Integer.parseInt(chtAgreeDate.split("-")[0]) - 1911))).concat("年").concat(chtAgreeDate.split("-")[1]).concat("月").concat(chtAgreeDate.split("-")[2]).concat("日"));
                if(chtAgreeTime != null && !"".equals(chtAgreeTime) )
                {
            		raw = raw.replace("[get_cht_rating_time]", chtAgreeTime.split(":")[0].concat(":").concat(chtAgreeTime.split(":")[1]));
                }
            }
        }

        // 申請人ip address
        raw = raw.replace("[ip_address]", cd.getIpAddress());
        logger.info("### End - replaceField");
        logger.info("### previewHtml.length()=" + raw.length());
        
        // HTML檔 - @export 儲存至檔案系統
        if (globalConfig.isPDF_Export()) {
            ExportUtil.export(global.PdfDirectoryPath + "/" + "CreditCardPDF.previewHtml.html", raw);
		}
		
		return raw;
	}

	private String getMinkuo(String onBoardDate) {
		if (onBoardDate.length() > 0 && onBoardDate.length() > 5) {
			return String.valueOf(Integer.valueOf(onBoardDate.substring(0, 4)) - 1911).concat("/")
					.concat(onBoardDate.substring(4, 6));
		} else {
			return "";
		}
	}

	@Override
	public void afterPdfProcess() {

	}

	@Override
	public void saveToDB(byte[] pdfByte) {

		// Save bytes to TABLE: CaseDocument
		CaseDocument doc = new CaseDocument(this._uniqId, "05", PlContractConst.DOCUMENT_TYPE_CREDIT_APPLYPDF);
		doc.setContent(pdfByte);
		doc.setVersion(templateVersion);
		caseDocumentDao.CreateOrUpdate(doc);

		// Save string base64 to TABLE: CaseDoc
		String str = Base64.encodeBase64String(pdfByte);
		creditCaseDataDao.setPDF(cd.getUniqId(), cd.getUniqType(), cd.getIdno(), str);
        System.out.println("Save Pdf To DB");

        // PDF檔 - @export 儲存至檔案系統
        if (globalConfig.isPDF_Export()) {
            ExportUtil.export(global.PdfDirectoryPath + "/" + _uniqId + ".pdf", pdfByte);
        }
	}

	@Override
	public void pdfToImage(List<byte[]> byteList) {

	}

	@Override
	public void sendEmail(byte[] pdfByte) {
		// String pdfBase64Str = Base64.encodeBase64String(pdfByte);
		// String CreditVerify = "0";
		// List<Map<String, Object>> tempCreditVerify =
		// creditCaseDataDao.getCreditVerify(_uniqId);
		// if (tempCreditVerify.size() != 0) {
		// Map<String, Object> tempMp = tempCreditVerify.get(0);
		// CreditVerify = tempMp.get("VerifyStatus").toString().trim();
		// }
		// // BASE64轉檔pdf 直接發送郵件
		// mailHunterService.sendCreditMailHunter(cd.getUniqId(), cd.getIdno(),
		// cd.getEmailAddress(), pdfBase64Str ,CreditVerify,cd.getUserType());
		// System.out.println("sendEmail !");
		sendMailHunterService.sendCreditCardPdf(cd, pdfByte);

	}
}
