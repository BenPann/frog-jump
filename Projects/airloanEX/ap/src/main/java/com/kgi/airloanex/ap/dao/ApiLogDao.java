package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.ApiLog;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class ApiLogDao extends CRUDQDao<ApiLog> {

    @Override
    public int Create(ApiLog fullItem) {
        String sql = "INSERT INTO ApiLog" + "(ApiName, Request, Response, StartTime, EndTime, UTime) VALUES"
                + "(?,?,?,?,?,GETDATE())";
        return this.getJdbcTemplate().update(sql, new Object[] { fullItem.getApiName(), fullItem.getRequest(),
                fullItem.getResponse(), fullItem.getStartTime(), fullItem.getEndTime() });
    }

    @Override
    public ApiLog Read(ApiLog keyItem) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int Update(ApiLog fullItem) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int Delete(ApiLog keyItem) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public List<ApiLog> Query(ApiLog keyItem) {
        // TODO Auto-generated method stub
        return null;
    }

    public String findCSResultNoToken() {
        String sql = " select Top 1 Response from ApiLog where ApiName = '/v1/internal/CrossSellingB/getStockByToken' "
                + " and Response like '%arychrRtnCode val=''0000''%arychrCurrency val=''TWD''%arychrtFileExtension val=''JPG''%' "
                + " order by Serial desc ";
        try {
            ApiLog result = this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(ApiLog.class),
                    new Object[] {});
            return result != null ? result.getResponse() : null;
        } catch (DataAccessException ex) {
            return ex.getMessage();
        }
    }
}
