package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.eopend3.ap.dao.CRUDQDao;
import com.kgi.eopend3.common.dto.db.QR_ShortUrlCaseMatch;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class QR_ShortUrlCaseMatchDao  extends CRUDQDao<QR_ShortUrlCaseMatch> {

	@Override
	public int Create(QR_ShortUrlCaseMatch fullItem) {
		String sql = "INSERT INTO QR_ShortUrlCaseMatch (ShortUrl, UniqId, UniqType) VALUES (:ShortUrl, :UniqId, :UniqType)";
		return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
	}

	@Override
	public QR_ShortUrlCaseMatch Read(QR_ShortUrlCaseMatch fullItem) {	
		String sql = "SELECT * FROM QR_ShortUrlCaseMatch WHERE ShortUrl = ? AND UniqId = ? ";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_ShortUrlCaseMatch.class),
                    new Object[] { fullItem.getShortUrl(), fullItem.getUniqId() });
        } catch (DataAccessException ex) {
//            System.out.println("QR_ShortUrlCaseMatch查無資料");
            return null;
        }
	}

	@Override
	public int Update(QR_ShortUrlCaseMatch fullItem) {
		return 0;
	}

	@Override
	public int Delete(QR_ShortUrlCaseMatch keyItem) {
		return 0;
	}

	@Override
	public List<QR_ShortUrlCaseMatch> Query(QR_ShortUrlCaseMatch keyItem) {
		return null;
	}

    public QR_ShortUrlCaseMatch find(String shortUrl, String uniqId) {
        QR_ShortUrlCaseMatch fullItem = new QR_ShortUrlCaseMatch();
        fullItem.setShortUrl(shortUrl);
        fullItem.setUniqId(uniqId);
        return Read(fullItem);
    }

    public String findShortUrlByUniqId(String uniqId) {
        String shortUrl = "";
        String sql = "SELECT top 1 * FROM QR_ShortUrlCaseMatch WHERE UniqId=?";
        try {
            QR_ShortUrlCaseMatch exist = this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_ShortUrlCaseMatch.class),
                    new Object[] { uniqId });
            if (exist != null) {
                shortUrl = exist.getShortUrl();
            }
        } catch (DataAccessException ex) {
			System.out.println("QR_ShortUrlCaseMatch查無資料");            
            return "";
        }
        return shortUrl;
    }

    public void insert(String shortUrl, String uniqId, String uniqType) {
        QR_ShortUrlCaseMatch exist = find(shortUrl, uniqId);
        if (exist == null) {
            QR_ShortUrlCaseMatch fullItem = new QR_ShortUrlCaseMatch();
            fullItem.setShortUrl(shortUrl);
            fullItem.setUniqId(uniqId);
            fullItem.setUniqType(uniqType);
            Create(fullItem);
        }
    }
}
