package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.QR_ChannelList;
import com.kgi.eopend3.ap.dao.CRUDQDao;
import com.kgi.eopend3.common.dto.respone.ChannelListResp;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class QR_ChannelListDao extends CRUDQDao<QR_ChannelList> {

    @Override
    public int Create(QR_ChannelList fullItem) {
        return 0;
    }

    @Override
    public QR_ChannelList Read(QR_ChannelList keyItem) {
        String sql = "SELECT * FROM QR_ChannelList WHERE ChannelId = ?;";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_ChannelList.class),
                    new Object[] { keyItem.getChannelId() });
        } catch (DataAccessException ex) {
//            System.out.println("QR_ChannelList查無資料");
            return null;
        }
    }

    @Override
    public int Update(QR_ChannelList fullItem) {
        return 0;
    }

    @Override
    public int Delete(QR_ChannelList keyItem) {
        return 0;
    }

    @Override
    public List<QR_ChannelList> Query(QR_ChannelList keyItem) {       
        return null;        
    }


    public List<ChannelListResp> getChannelList() {
        String sql = "SELECT * from QR_ChannelList where enable=1 order by Sort ";
        try {
            return this.getJdbcTemplate().query(sql, new BeanPropertyRowMapper<>(ChannelListResp.class), new Object[]{});
        } catch (DataAccessException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public QR_ChannelList getChannelByEntryData(String uniqId) {
        String sql = "SELECT cl.* FROM QR_ChannelList cl "
        		+ " INNER JOIN EntryData e on LEFT(e.Entry, 2)=cl.ChannelId "
        		+ " WHERE e.UniqId = ? ";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_ChannelList.class),
                    new Object[] { uniqId });
        } catch (DataAccessException ex) {
            System.out.println("QR_ChannelList查無資料");
            return null;
        }
    }

    public QR_ChannelList getChannelDataByUniqId(String uniqId) {
        try {
            String sql = "SELECT cdp.* FROM QR_ShortUrlCaseMatch su "
                    + "INNER JOIN QR_ShortUrl su2 on su.ShortUrl = su2.ShortUrl " 
                    + "INNER JOIN QR_ChannelList cdp on cdp.ChannelId = su2.ChannelId " 
                    + "WHERE su.UniqId = ?;";
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_ChannelList.class),
                    new Object[] { uniqId });
        } catch (DataAccessException ex) {
            System.out.println("getChannelDataByUniqId 無資料");
            return null;
        }
    }
}