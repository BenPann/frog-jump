package com.kgi.crosssell.ap.ctl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fuco.prod.common.util.HttpUtil2;
import com.kgi.crosssell.ap.service.LogService;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/log")
public class LogCtl {
	@Autowired
	LogService logServ;

	@RequestMapping(value = "/saveBrowseRecord", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	@ResponseBody
	public void saveBrowseRecord(HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {
			// 將取得的request轉為 json
			JSONObject req = JSONObject.fromObject(ESAPI.validator().getValidInput("", HttpUtil2.getPostData(request, "UTF-8"), "SafeJson", 10000, false));
//			System.out.println(Encode.forJava("getBrowseRecord: request = " + req.toString()));
			
			String id = req.getString("UniqId");
			String type = req.getString("UniqType");
			String page = req.getString("Page");
			String action = req.optString("Action", "");
			
			try {
				id = ESAPI.validator().getValidInput("", id, "AntiXSS", 32, false);
				type = ESAPI.validator().getValidInput("", type, "AntiXSS", 32, false);
				page = ESAPI.validator().getValidInput("", page, "AntiXSS", 1024, false);
				action = ESAPI.validator().getValidInput("", action, "AntiXSS", 32, false);
			} catch (Exception e1) {
				return;
			}
			// 寫入log檔
			logServ.saveBrowseRecordLog(id, type, page, action);
		} catch (Exception e) {
			return;
		}
	}

	@RequestMapping(value = "/processStart", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
	@ResponseBody
	public void processStart(String UniqId, String UniqType) throws Exception {
		try {
			UniqId = ESAPI.validator().getValidInput("", UniqId, "AntiXSS", 32, false);
			UniqType = ESAPI.validator().getValidInput("", UniqType, "AntiXSS", 32, false);
		} catch (Exception e1) {
			return;
		}
		logServ.processStart(UniqId, UniqType);
	}

	@RequestMapping(value = "/processPageStart", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
	@ResponseBody
	public void processPageStart(String UniqId, String UniqType, String Page) throws Exception {
		try {
			UniqId = ESAPI.validator().getValidInput("", UniqId, "AntiXSS", 32, false);
			UniqType = ESAPI.validator().getValidInput("", UniqType, "AntiXSS", 32, false);
			Page = ESAPI.validator().getValidInput("", Page, "AntiXSS", 1024, false);
		} catch (Exception e1) {
			return;
		}
		logServ.processPageStart(UniqId, UniqType, Page);
	}

}
