package com.kgi.airloanex.ap.service;

import static com.kgi.airloanex.common.PlContractConst.IsPreAudit_1;
import static com.kgi.airloanex.common.PlContractConst.KB_9743_CHANNEL_ID;
import static com.kgi.airloanex.common.PlContractConst.KB_9743_DEPART_ID;
import static com.kgi.airloanex.common.PlContractConst.LOAN_ProductId_1_PA;
import static com.kgi.airloanex.common.PlContractConst.LOAN_ProductId_2_PL;
import static com.kgi.airloanex.common.PlContractConst.LOAN_ProductId_3_RPL;
import static com.kgi.airloanex.common.PlContractConst.LOAN_ProductId_4_GM;
import static com.kgi.airloanex.common.PlContractConst.USER_TYPE_0_NEW_ONE;
import static com.kgi.airloanex.common.PlContractConst.USER_TYPE_3_OLD_ONE;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kgi.airloanex.ap.dao.CaseDataDao;
import com.kgi.airloanex.ap.dao.CaseDataKycDebtInfoDao;
import com.kgi.airloanex.ap.dao.CaseDataRelationDegreeDao;
import com.kgi.airloanex.ap.dao.QR_ShortUrlCaseMatchDao;
import com.kgi.airloanex.ap.dao.QR_ShortUrlDao;
import com.kgi.airloanex.ap.dao.UserPhotoDao;
import com.kgi.airloanex.common.dto.customDto.GetKycDebtInfoRspDto;
import com.kgi.airloanex.common.dto.customDto.GetKycDebtInfoRspDtoDebtInfo;
import com.kgi.airloanex.common.dto.customDto.QryEmpDepRspDto;
import com.kgi.airloanex.common.dto.db.CaseData;
import com.kgi.airloanex.common.dto.db.CaseDataKycDebtInfo;
import com.kgi.airloanex.common.dto.db.CaseDataRelationDegree;
import com.kgi.airloanex.common.dto.db.QR_ChannelDepartList;
import com.kgi.airloanex.common.dto.db.QR_ChannelList;
import com.kgi.airloanex.common.dto.db.UserPhoto;
import com.kgi.eopend3.ap.dao.EntryDataDao;
import com.kgi.eopend3.common.dto.db.EntryData;
import com.kgi.eopend3.common.dto.db.QR_ShortUrl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * LOAN 申請流程 CaseData 主檔業務邏輯
 */
@Service
public class CaseDataService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /** 檢核失敗提示: 缺ID */
    public static final String CHECK_idcard = "缺ID";
    
    /** 檢核失敗提示: 缺戶役政 */
    public static final String CHECK_rwv2c2 = "缺戶役政";
    
    /** 檢核失敗提示: 缺財力 */
    public static final String CHECK_finpf = "缺財力";
    
    /** 檢核失敗提示: 缺同一關係人表 */
    public static final String CHECK_relationDegree = "缺同一關係人表";
    
    /** 檢核失敗提示: 缺風險評級 */
    public static final String CHECK_cddRateRisk = "缺風險評級";
    
    /** 檢核失敗提示: 缺AML */
    public static final String CHECK_aml = "缺AML";
    
    /** 檢核失敗提示: 缺Z07 */
    public static final String CHECK_z07 = "缺Z07";
    
    /** 檢核失敗提示: 是利關人 */
    public static final String CHECK_relp = "是利關人";
    
    /** 檢核失敗提示: 缺KYC */
    public static final String CHECK_kyc = "缺KYC";
    
    /** 檢核失敗提示: APS異常件 */
    public static final String CHECK_aps = "APS異常件";
    
    
    @Autowired
    CaseDataDao caseDataDao;

    @Autowired
    private EntryDataDao entryDataDao;

    @Autowired
    UserPhotoDao userPhotoDao;

    @Autowired
    CaseDataRelationDegreeDao caseDataRelationDegreeDao;

    @Autowired
    private QR_ShortUrlDao qrShortUrlDao;

    @Autowired
    private QR_ShortUrlCaseMatchDao qrShortUrlCaseMatchDao;

    @Autowired
    private CaseDataKycDebtInfoDao caseDataKycDebtInfoDao;
    
    @Autowired
    private KgiService kgiService;

    private static boolean isOk(String s) {
        return StringUtils.equals("Y", s) || StringUtils.equals("01", s);
    }

    /** 判斷是否為既有戶 CaseData.UserType = 3 */
    public static boolean isOldOne(String userType) {
        return StringUtils.equals(USER_TYPE_3_OLD_ONE, userType);
    }

    /** 判斷是否為新戶 CaseData.UserType = 0 */
    public static boolean isNewOne(String userType) {
        return StringUtils.equals(USER_TYPE_0_NEW_ONE, userType);
    }

    /** 判斷是否為預核名單 CaseData.IsPreAudit = 1 */
    public static boolean isPreAudit(String IsPreAudit) {
        return StringUtils.equals(IsPreAudit_1, IsPreAudit);
    }

    /** 判斷是否為非預核名單(一般流程) CaseData.IsPreAudit = 2 */
    public static boolean isNonePreAudit(String IsPreAudit) {
        return !isPreAudit(IsPreAudit);
    }

    /** LOAN 申請流程 1 PA 預核名單preAudit(快速申辦不查聯徵) */
    public static boolean isPA(String productId) {
        return LOAN_ProductId_1_PA.equals(productId);
    }
    /** LOAN 申請流程 2 PL */
    public static boolean isPL(String productId) {
        return LOAN_ProductId_2_PL.equals(productId);
    }
    /** LOAN 申請流程 3 RPL */
    public static boolean isRPL(String productId) {
        return LOAN_ProductId_3_RPL.equals(productId);
    }
    /** LOAN 申請流程 4 GM(CASHCARD) 現金卡 */
    public static boolean isGM(String productId) {
        return LOAN_ProductId_4_GM.equals(productId);
    }
    /** 貸款金額大於100萬，需提供同一關係人表 */
    public static boolean needRelationDegreeReport(Integer p_apy_amount) {
        return p_apy_amount != null && p_apy_amount > 100;
    }

    public static String splitAddress(String address) {
        String[] splited = StringUtils.split(address, ";");
        if (splited.length > 1) {
            return splited[1];
        }
        return address;
    }

    /** 是否為手機號碼 */
    public static boolean isMobilePhone(String tel) {
        if (StringUtils.startsWith(tel, "09") && StringUtils.length(tel) == 10) {
            return true;
        }
        return false;
    }

    public boolean isComplete(String uniqId) throws Exception {
        Map<String, Boolean> completeMap = makeCompleteMap(uniqId);
        return isComplete(completeMap);
    }

    public boolean isComplete(Map<String, Boolean> completeMap) {
        boolean isComplete = true;
        for (Map.Entry<String, Boolean> entry : completeMap.entrySet()) {
            isComplete &= entry.getValue();
        }
        return isComplete;
    }

    /** cComplete=[Y/N] Y-完整, N-不完整 */
    public String cComplete(Map<String, Boolean> completeMap) {
        return isComplete(completeMap) ? "Y" : "N"; // Y-完整, N-不完整
    }

    /** cAbnormalReason=缺ID、缺戶役政、缺財力、缺同一關係人表、缺風險評級、缺AML、缺Z07、是利關人、缺KYC、APS異常件 */
    public String cAbnormalReason(Map<String, Boolean> completeMap) {
        List<String> reasons = new ArrayList<>();
        for (Map.Entry<String, Boolean> entry : completeMap.entrySet()) {
            if (entry.getValue() == false) { // true 檢核通過 false 檢核失敗
                reasons.add(entry.getKey());
            }
        }
        return StringUtils.join(reasons, "、");
    }

    /** 
     * 表示通過檢核 {"缺ID", true}
     * 表示通過檢核 {"缺戶役政", true}
     * 表示通過檢核 {"缺財力", true}
     * 表示通過檢核 {"缺同一關係人表", true}
     * 表示通過檢核 {"缺風險評級", true}
     * 表示通過檢核 {"缺AML", true}
     * 表示通過檢核 {"缺Z07", true}
     * 表示通過檢核 {"是利關人", true}
     * 表示通過檢核 {"缺KYC", true}
     * 表示通過檢核 {"APS異常件", true}
     * 
     * 完整件判斷條件 (XML中 subcaseType=2 and cComplete=Y)
     * 不完整判斷條件 (XML中 subcaseType=1 and cComplete=N and cAbnormalReason=缺ID、缺戶役政、缺財力、缺同一關係人表、缺風險評級、缺AML、缺Z07、是利關人、缺KYC、APS異常件)
     */
    public Map<String, Boolean> makeCompleteMap(String uniqId) throws Exception {
        Map<String, Boolean> completeMap = new LinkedHashMap<>();
        CaseData caseData = caseDataDao.ReadByCaseNo(uniqId);
        boolean isNewOne = isNewOne(caseData.getUserType());
        boolean isOldOne = isOldOne(caseData.getUserType());

        boolean passIdcard = passIdcard(uniqId);
        boolean passFinancial = passFinancial(uniqId);
        if (isNewOne) {
            // 新戶(以下條件為AND)
            boolean hasQryIdInfo = StringUtils.equals("01", caseData.getHasQryIdInfo());
            boolean hasRelationDegree = hasRelationDegree(uniqId, caseData.getP_apy_amount());
            boolean hasQryAMl = StringUtils.equals("01", caseData.getHasQryAml());
            boolean hasQryZ07 = StringUtils.equals("01", caseData.getHasQryZ07());
            boolean hasFinishKyc = StringUtils.equals("01", caseData.getHasFinishKyc());
            boolean hasNoRelp = StringUtils.equals("01", caseData.getHasNoRelp());
            boolean dgtcaseResult = StringUtils.equals("1", caseData.getDgtcaseResult()); // 1.正常執行完畢2.系統發生錯誤,空字串:系統逾時

            completeMap.put(CHECK_idcard, passIdcard); // 缺ID
            completeMap.put(CHECK_rwv2c2, hasQryIdInfo); // 缺戶役政
            completeMap.put(CHECK_finpf, passFinancial); // 缺財力
            completeMap.put(CHECK_relationDegree, hasRelationDegree); // 缺同一關係人表
            completeMap.put(CHECK_aml, hasQryAMl); // 缺AML
            completeMap.put(CHECK_z07, hasQryZ07); // 缺Z07
            completeMap.put(CHECK_relp, hasNoRelp); // 是利關人
            completeMap.put(CHECK_kyc, hasFinishKyc); // 缺KYC
            completeMap.put(CHECK_aps, dgtcaseResult); // APS異常件
        } else if (isOldOne) {
            // 舊戶(以下條件為AND)
            boolean hasQryIdInfo = StringUtils.equals("01", caseData.getHasQryIdInfo());
            boolean hasRelationDegree = hasRelationDegree(uniqId, caseData.getP_apy_amount());
            boolean hasCddRateRiskLow = StringUtils.equals("低", caseData.getCddRateRisk());
            boolean hasQryAMl = StringUtils.equals("01", caseData.getHasQryAml());
            boolean hasQryZ07 = StringUtils.equals("01", caseData.getHasQryZ07());
            boolean hasNoRelp = StringUtils.equals("01", caseData.getHasNoRelp());
            boolean hasFinishKyc = StringUtils.equals("01", caseData.getHasFinishKyc());
            boolean dgtcaseResult = StringUtils.equals("1", caseData.getDgtcaseResult()); // 1.正常執行完畢2.系統發生錯誤,空字串:系統逾時

            completeMap.put(CHECK_idcard, passIdcard); // 缺ID
            completeMap.put(CHECK_rwv2c2, hasQryIdInfo); // 缺戶役政
            completeMap.put(CHECK_finpf, passFinancial); // 缺財力
            completeMap.put(CHECK_relationDegree, hasRelationDegree); // 缺同一關係人表
            completeMap.put(CHECK_cddRateRisk, hasCddRateRiskLow); // 缺風險評級
            completeMap.put(CHECK_aml, hasQryAMl); // 缺AML
            completeMap.put(CHECK_z07, hasQryZ07); // 缺Z07
            completeMap.put(CHECK_relp, hasNoRelp); // 是利關人
            completeMap.put(CHECK_kyc, hasFinishKyc); // 缺KYC
            completeMap.put(CHECK_aps, dgtcaseResult); // APS異常件
        } else {
            throw new Exception("Illegal CaseData neither isOldOne or isNewOne.");
        }
        return completeMap;
    } // end makeCompleteMap

    /**
     * 以下情境時 UPDATE_DGTCASE_INFO 中 COMP_CASE=Y
     * | COMP_CASE=Y | 申請金額      | ID圖檔有上傳或有影編 | 財力圖檔有上傳或有影編 | KYC是否完整 | 是否有業務員編資訊 | 有填同一關係人表 |
     * | ----------- | ------------- | -------------------- | ---------------------- | ----------- | ------------------ | ---------------- |
     * | 情境一      | 5萬以下(含)   | Y                    | Y                      | Y           | -                  | -                |
     * | 情境二      | 5萬~100萬(含) | Y                    | Y                      | Y           | Y                  | -                |
     * | 情境三      | 大於100萬     | Y                    | Y                      | Y           | Y                  | Y                |
     */
    public String makeCOMP_CASE(String uniqId) throws Exception {
        Map<String, Boolean> map = this.makeCompleteMap(uniqId);
        CaseData caseData = caseDataDao.ReadByCaseNo(uniqId);
        String agentNo = caseData.getAgentNo();
        int amount = Integer.valueOf(caseData.getP_apy_amount());
        boolean scenario1 = map.get(CHECK_idcard) == true && map.get(CHECK_finpf) == true && map.get(CHECK_kyc) == true;
        boolean scenario2 = map.get(CHECK_idcard) == true && map.get(CHECK_finpf) == true && map.get(CHECK_kyc) == true && StringUtils.isNotBlank(agentNo);
        boolean scenario3 = map.get(CHECK_idcard) == true && map.get(CHECK_finpf) == true && map.get(CHECK_kyc) == true && StringUtils.isNotBlank(agentNo) && map.get(CHECK_relationDegree) == true ;
        if (amount <= 5) {
            if (scenario1) {
                return "Y";
            } else {
                return "N";
            }
        } else if (5 < amount && amount <= 100) {
            if (scenario2) {
                return "Y";
            } else {
                return "N";
            }
        } else if (100 < amount) {
            if (scenario3) {
                return "Y";
            } else {
                return "N";
            }
        } else {
            logger.error("Fail to makeCOMP_CASE().");
            return "N";
        }
    } // makeCOMP_CASE

    /** 有填寫同一關係人表(IF申請金額>100) */
    private boolean hasRelationDegree(String uniqId, String p_apy_amount) {
        boolean hasRelationDegree = false;
        if (Integer.valueOf(p_apy_amount) > 100) {
            List<CaseDataRelationDegree> cdrds = caseDataRelationDegreeDao.findByUniqId(uniqId);
            if (cdrds != null && cdrds.size() > 0) {
                hasRelationDegree = true;
            }
        } else {
            hasRelationDegree = true;
        }
        return hasRelationDegree;
    }

    public boolean isNotComplete(String uniqId) throws Exception {
        return !isComplete(uniqId);
    }

    public QR_ShortUrl qrShortUrl(String uniqId) {
        String shortUrl = qrShortUrlCaseMatchDao.findShortUrlByUniqId(uniqId); // 例如：shortUrl=05rp0cfk
        QR_ShortUrl qrShortUrl = qrShortUrlDao.findByShortUrl(shortUrl);
        return qrShortUrl;
    }

    public String channelId(String uniqId) {
        EntryData rtn = entryDataDao.Read(new EntryData(uniqId));
        QR_ShortUrl qrShortUrl = qrShortUrl(uniqId);
        return channelId(rtn, qrShortUrl);
    }

    public String channelId(EntryData rtn, QR_ShortUrl qrShortUrl) {
        String channelId = KB_9743_CHANNEL_ID;
        if (qrShortUrl != null) { // QRCODE短網址通路
            channelId = qrShortUrl.getChannelId();
        } else if(StringUtils.contains(rtn.getOther(), "CH")){
            String chValue = findEntryDataOtherParamValueByParamName(rtn.getOther(), "CH");
            channelId = StringUtils.substring(chValue, 0, 2);
            // 第一順位:ENTRYDATA中OTHERS資料中如有CH參數，則以CH參數前兩碼為優先
        } else if (StringUtils.isNotEmpty(rtn.getEntry())){
            // 第二順位:ENTRYDATA中的 ENTRY 欄位前兩碼
            channelId = StringUtils.substring(rtn.getEntry(), 0, 2);
        }
        return channelId;
    }

    /** 取 EntryData 的 Other 中的參數值，例如：[{"qr":"05rp0cfk", "CH":...}] */
    private String findEntryDataOtherParamValueByParamName(String entryOther, String paramName) {
        if (StringUtils.isNotBlank(entryOther) 
            && StringUtils.contains(entryOther, paramName)
            && !StringUtils.equals("[]", entryOther) ) {
            List<Map<String, String>> others = new Gson().fromJson(entryOther, new TypeToken<List<Map<String, String>>>() {}.getType());
            Map<String, String> other = others.stream().filter(x -> x.get(paramName) != null).collect(Collectors.toList()).stream().findFirst().orElseGet(null);
            String paramValue = other.get(paramName);
            return paramValue;
        }
        return null;
    }

    /** 根據業務員代號查詢部門代號(DepartId) */
    public String findAgentDepart(String uniqId, String agentNo, String shortUrl) {
        String depCodeAct = KB_9743_DEPART_ID; // 2020-10-14 如沒輸入員編，預設單位代號為9743 demand by Ben

        if (StringUtils.isNotBlank(shortUrl)) {
            QR_ShortUrl qrShortUrl = qrShortUrlDao.findByShortUrl(shortUrl);
            if (qrShortUrl != null) {
                if (!StringUtils.equals(KB_9743_CHANNEL_ID, qrShortUrl.getChannelId())) { // not KB
                    depCodeAct = qrShortUrl.getDepartId();
                    // logger.info("### not KB's DepartId=" + depCodeAct);
                } else { // is KB
                    depCodeAct = findDepartId(uniqId, agentNo, qrShortUrl.getDepartId());
                    // logger.info("###  is KB's DepartId=" + depCodeAct);
                }
            } else {
                depCodeAct = findDepartId(uniqId, agentNo, KB_9743_DEPART_ID);
            }
        } else {
            depCodeAct = findDepartId(uniqId, agentNo, KB_9743_DEPART_ID);
        }
        return depCodeAct;
    }

    /** 根據業務員代號查詢部門代號(DepartId) */
    private String findDepartId(String uniqId, String agentNo, String defaultDepartId) {
        String depCodeAct = defaultDepartId; // 2020-10-14 如沒輸入員編，預設單位代號為9743 demand by Ben
        try {
            if (StringUtils.isNotBlank(agentNo)) {
                // 業務人員
                // @call 3. QRY_EMP_DEP
                QryEmpDepRspDto emp = kgiService.call_QRY_EMP_DEP(uniqId, agentNo);
                if (StringUtils.equals(emp.getCHECK_CODE(), "")) {
                    depCodeAct = emp.getDEP_CODE_ACT();
                }
            }
        } catch (Exception e) {
            logger.error("Fail to save QR_PromoCaseUserInput.", e);
        }
        return depCodeAct;
    }

    public String departId(String agentNo, String agentDepart) {
        String departId = KB_9743_DEPART_ID;
        if (StringUtils.isNotBlank(agentNo)) {
            if (StringUtils.isNotBlank(agentDepart)) {
                // 1.登入時填入之員編透過API查得之單位代號D1，用(26)之ChannelId與D1前三碼查詢 QR_CHANNELDEPARTLIST
                // (ex: select * from QR_CHANNELDEPARTLIST WHERE CHANNELID=@channelid and LEFT(Depart,3)=LEFT(@D1,3); )
                // 結果只會有一列資料，派件單位=AssignDepart欄位
                departId = agentDepart;
            } else {
                // 2.如登入時填入之員編透過API查不到單位代號，則預設單位代號為'0000'，用(26)之ChannelId與0000查詢  QR_CHANNELDEPARTLIST
                // (ex: select * from QR_CHANNELDEPARTLIST WHERE CHANNELID=@channelid and Depart='0000'; )
                // 結果只會有一列資料，派件單位=AssignDepart欄位
                departId = "0000";
            }
        } else {
            // 3.如登入時未填員編，則預設單位代號為'9743'，用(26)之ChannelId與9743查詢  QR_CHANNELDEPARTLIST
            // (ex: select * from QR_CHANNELDEPARTLIST WHERE CHANNELID=@channelid and Depart='9743'; )
            // 結果只會有一列資料，派件單位=AssignDepart欄位
            departId = KB_9743_DEPART_ID;
        }
        return departId;
    }

    public String knowChannelType(String uniqId, String agentNo, boolean isOldOne, boolean isPreAudit, QR_ChannelDepartList qr_ChannelDepartList, QR_ChannelList qr_channellist) {
        String KnowChannelType = "";
        EntryData entryData = entryDataDao.Read(new EntryData(uniqId));
        QR_ShortUrl qrShortUrl = this.qrShortUrl(uniqId);
        boolean hasQrShortUrl = qrShortUrl != null;
        // Case Has QRCode || 登入頁有敲入員編
        if (hasQrShortUrl || StringUtils.isNotBlank(agentNo)) {
            String channelId = qr_ChannelDepartList.getChannelId();
            String departId = qr_ChannelDepartList.getDepartId();
            
            if (StringUtils.equals(KB_9743_CHANNEL_ID, channelId)) {
                if (StringUtils.containsAny(departId, "8220","8230","8240","8250","8260","8270")) {
                    if (isOldOne) {
                        KnowChannelType = "14";
                    } else {
                        KnowChannelType = "14";
                    }
                } else if (StringUtils.containsAny(departId, "9743","9740","1999","8882","9621")) {
                    if (isOldOne) {
                        if (isPreAudit) {
                            KnowChannelType = "59";
                        } else {
                            KnowChannelType = "13";
                        }
                    } else {
                        KnowChannelType = "16";
                    }
                } else {
                    if (isOldOne) {
                        KnowChannelType = "14";
                    } else {
                        KnowChannelType = "15";
                    }
                }
            } else {
                
                if (qr_channellist != null && StringUtils.equals("1", qr_channellist.getChannelType())) { // 集團異業
                    KnowChannelType = "85";
                } else { // 其他異業
                    KnowChannelType = "16";
                }
            }
        } 
        // 登入頁沒敲員編
        else { 
            if (StringUtils.isNotBlank(entryData.getEntry())) {
                String d2 = StringUtils.substring(entryData.getEntry(), 0, 2);
                if (StringUtils.containsAny(d2, "KB", "OS")) {
                    KnowChannelType = "20";
                } else {
                    KnowChannelType = "16";
                }
            } else {
                if (isOldOne) {
                    KnowChannelType = "13";
                } else {
                    KnowChannelType = "16";
                }
            }
        }
        return KnowChannelType;
    }

// 補充說明002:
// 得知管道邏輯
// 得知管道 KnowChannelType
// 案件有對應到短網址，ChannelId與DepartId依據短網址指定的:
// IF (Case Has QRCode || 登入頁有敲入員編)   
//         IF (ChannelId in ('KB') )
//                 IF (DepartId in ('8220','8230','8240','8250','8260'))
//                         IF (CaseData.UserType!='0')
//                                 KnowChannelType=13
//                         ELSE
//                                 KnowChannelType=14
//                         END IF
//                 ELSE IF (DepartId in ('9743','9621'))
//                         IF (CaseData.UserType!='0')
//                                 IF (CaseData.IsPreAudit==1)
//                                         KnowChannelType=59
//                                 ELSE
//                                         KnowChannelType=13
//                                 END IF
//                         ELSE
//                                 KnowChannelType=14
//                         END IF
//                 ELSE
//                         IF (CaseData.UserType!='0')
//                                 KnowChannelType=13
//                         ELSE
//                                 KnowChannelType=15
//                         END IF
//                 END IF
//         ELSE IF (ChannelId NOT in ('KB'))
//                 IF  (QR_ChannelList.ChannelType='1')  //集團異業
//                         KnowChannelType=85
//                 ELSE                                                                       //其他異業
//                         KnowChannelType=16
//                 END IF
//         END IF
// ELSE                                                                                       //登入頁沒敲員編
//         IF (EntryData.Entry!=null)
//                 IF (LEFT(EntryData.Entry,2) in ('KB','OS'))
//                         KnowChannelType=20                                    
//                 ELSE
//                         KnowChannelType=16                                    
//                 END IF  
//         ELSE      
//                 IF (CaseData.UserType!='0')
//                         KnowChannelType=13
//                 ELSE
//                         KnowChannelType=16
//                 END IF  
//         END IF
// END IF  

    /**
     * 呼叫外部負債資料查詢
     */
    public GetKycDebtInfoRspDtoDebtInfo checkDebtInfo(String uniqId, String decisionCaseNo, String productId, String Z07Date) throws Exception {
        GetKycDebtInfoRspDtoDebtInfo debtInfo = null;
        String apyType = apyType(productId);
        String IS_DGT = "Y";
        String IS_IMM = "N";
        GetKycDebtInfoRspDto kycDebtInfo = kgiService.call_GET_KYC_DEBT_INFO(uniqId, decisionCaseNo, apyType, IS_DGT, IS_IMM, Z07Date);
        if (StringUtils.equals("", kycDebtInfo.getCHECK_CODE())) {
            debtInfo = kycDebtInfo.getDEBT_INFO();
            CaseDataKycDebtInfo caseDataKycDebtInfo = new CaseDataKycDebtInfo();
            // 外部負債資料 debtInfo
            logger.info("### kycDebtInfo=" + new Gson().toJson(kycDebtInfo));
            BeanUtils.copyProperties(debtInfo, caseDataKycDebtInfo, "UniqId");
            caseDataKycDebtInfo.setUniqId(uniqId);
            caseDataKycDebtInfoDao.CreateOrUpdate(caseDataKycDebtInfo);
        }
        return debtInfo;
    }

    /**
     * 產品別,1.現金卡(e貸寶) 2.PLOAN 3.信用卡，(string)
     */
    private String apyType(String productId) throws Exception {
        if (StringUtils.equals("1", productId) || StringUtils.equals("2", productId)) { // 1 預核 2 PL
            return "2";
        } else if (StringUtils.equals("3", productId)) { // 3 RPL
            return "1";
        } else {
            throw new Exception("productId not found.");
        }
    }

    /** 是否需要進入身份證上傳頁 */
    public boolean mustUploadIdcard(String uniqId) {
        return !passIdcard(uniqId);
    }

    /** 是否需要進入財力證明上傳頁 */
    public boolean mustUploadFinpf(String uniqId) {
        return !passFinancial(uniqId);
    }

    /** 表示通過檢核 身份證正反面 */
    private boolean passIdcard(String uniqId) {
        CaseData caseData = caseDataDao.ReadByCaseNo(uniqId);
        boolean isNewOne = isNewOne(caseData.getUserType());
        boolean isOldOne = isOldOne(caseData.getUserType());
        List<UserPhoto> idcardFront = userPhotoDao.findIdcardFront(uniqId);
        List<UserPhoto> idcardBack = userPhotoDao.findIdcardBack(uniqId);
        boolean hasIdcardFront = idcardFront != null && idcardFront.size() > 0;
        boolean hasIdcardBack = idcardBack != null && idcardBack.size() > 0;
        boolean hasIdcard = hasIdcardFront && hasIdcardBack; // 已上傳身份證正反圖檔

        // 戶籍地址區碼與戶籍地址其中之一為空值時，要顯示ID上傳頁補上傳ID demand by Ben 
        if (StringUtils.isBlank(caseData.getResAddrZipCode()) || StringUtils.isBlank(caseData.getResAddr())) {
            logger.error("Fail find ResAddrZipCode or ResAddr.");
            return false;
        }

        if (isNewOne) {
            return hasIdcard;
        } else if (isOldOne) {
            // 既有戶是否有idcard影編
            boolean hasOldIdImg = StringUtils.equals("01", caseData.getHasOldIdImg()) && StringUtils.isNotBlank(caseData.getOldIdImgNoList());
            // 檢核戶役政
            boolean hasQryIdInfo = StringUtils.equals("01", caseData.getHasQryIdInfo());
            return hasIdcard || (hasOldIdImg && hasQryIdInfo);
        } else {
            logger.error("Fail find isNewOne or isOldOne in passIdcard.");
            return false;
        }
    }

    /** 表示通過檢核 財力證明 */
    private boolean passFinancial(String uniqId) {
        CaseData caseData = caseDataDao.ReadByCaseNo(uniqId);
        boolean isNewOne = isNewOne(caseData.getUserType());
        boolean isOldOne = isOldOne(caseData.getUserType());
        List<UserPhoto> financial = userPhotoDao.findFinancial(uniqId);
        boolean hasFinancial = financial != null && financial.size() > 0; // 已上傳財力證明圖檔
        boolean applyWithoutFina = StringUtils.equals("01", caseData.getApplyWithoutFina()); // 免財力證明
        boolean passFinancial = applyWithoutFina || hasFinancial;
        if (isNewOne) {
            return passFinancial;
        } else if (isOldOne) {
            // 既有戶是否有fina影編
            boolean hasOldFinaImg = StringUtils.equals("01", caseData.getHasOldFinaImg()) && StringUtils.isNotBlank(caseData.getOldFinaImgNoList());
            return passFinancial || hasOldFinaImg;
        } else {
            logger.error("Fail find isNewOne or isOldOne in passFina");
            return false;
        }
    }
}
