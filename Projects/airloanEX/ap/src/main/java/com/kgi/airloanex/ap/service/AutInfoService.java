package com.kgi.airloanex.ap.service;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.ContractApiLogDao;
import com.kgi.airloanex.ap.dao.ContractMainDao;
import com.kgi.eopend3.ap.dao.TermsDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.ap.webservice.AUTINFO_KGIB_NEW;
import com.kgi.eopend3.ap.webservice.AuthorizationKGIBProxy;
import com.kgi.eopend3.ap.webservice.UPDATEAUTINFO_KGIB;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.db.Terms;
import com.kgi.eopend3.common.dto.respone.AuthInfoResp;
import com.kgi.eopend3.common.dto.view.TermsView;
import com.kgi.eopend3.common.util.CheckUtil;
import com.kgi.eopend3.common.util.DateUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.json.JSONObject;

@Service
public class AutInfoService {
    @Autowired
    private ContractApiLogDao contractApiLogDao;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    AirloanEXConfig globalConfig;

    @Autowired
    TermsDao termsDao;

    @Autowired
    private ContractMainDao contractMainDao;

    public String getAutInfo(String reqJson, String idno, String uniqId) {
        String response = "";
        String startTime = "";
        String endTime = "";
        try {        
            Gson gson = new Gson();
            TermsView view = gson.fromJson(reqJson, TermsView.class);
            if(!CheckUtil.check(view)){
                throw new ErrorResultException(1, "參數錯誤", null);               
            }else{
                startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
                AuthorizationKGIBProxy service = new AuthorizationKGIBProxy(globalConfig.AutInfo_Url());
                AUTINFO_KGIB_NEW res = service.FUNGETAUTINFOBYID_KGIB_NEW(idno); 
                JSONObject r = JSONObject.fromObject(res);
                response = r.toString();
                endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
                AUTINFO_KGIB_NEW autInfo = gson.fromJson(response, AUTINFO_KGIB_NEW.class);
                AuthInfoResp resp = new AuthInfoResp();
                String authorizeToallCorp = autInfo.getA_ITEM01();
                if (authorizeToallCorp.equals("")) {
                    authorizeToallCorp = "N";
                }
                Terms terms = new Terms();
                terms.setName(view.getTermName());
                terms = termsDao.Read(terms);
                resp.setAuthorizeToallCorp(authorizeToallCorp);
                resp.setVerNo(terms.getVersion());
                return WebResult.GetResultString(0, "成功", resp);
            }
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception ex) {
            logger.error("未知錯誤", ex);
            return WebResult.GetResultString(99, "系統錯誤", null);
        } finally {
            contractApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "/getAutInfo", idno, response, startTime, endTime);
        }
    }

    // public String updateAutInfo(String idno, String reqBody, String ip, String uniqId) {
    //     System.out.println("###updateAutInfo >> idno= " + idno + " ;reqBody= " + reqBody + " ;ip= " + ip + " ;uniqId= " + uniqId);
    //     String response = "";
    //     String startTime = "";
    //     String endTime = "";
    //     String name = "";
    //     String authorizeToallCorp = "";
    //     String verNo = "2020.01"; // autVer 共銷版本號

    //     try {
    //         name = contractMainDao.ReadByUniqId(uniqId).getChtName();
    //         Gson gson = new Gson();
    //         TermsView view = gson.fromJson(reqBody, TermsView.class);  // EOPPersonalDataUsing
    //         System.out.println("### >> name= " + name + " ;view= " + view.getTermName());

    //         if(!CheckUtil.check(view)){
    //             throw new ErrorResultException(1, "參數錯誤", null);               
    //         } else {
    //             startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
    //             AuthorizationKGIBProxy service = new AuthorizationKGIBProxy();
    //             //01 同意 02 不同意
    //             String flag = authorizeToallCorp.equals("Y") ? "Y" : "";
    //             UPDATEAUTINFO_KGIB res = service.FUNUPDATEAUTINFO_KGIB_NEW(idno, name, flag, ip, verNo);
    //             JSONObject r = JSONObject.fromObject(res);
    //             response = r.toString();
    //             endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
    //         }
    //     } catch (Exception ex) {
    //         logger.error("未知錯誤", ex);
    //     } finally {
    //         JSONObject req = new JSONObject();
    //         req.put("idno", idno);
    //         req.put("name", name);
    //         req.put("authorizeToallCorp", authorizeToallCorp);
    //         req.put("ip", ip);
    //         req.put("verNo", verNo);
    //         contractApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "/updateAutInfo", req.toString(), response, startTime, endTime);
    //     }
    //     return response;
    // }
}
