package com.kgi.airloanex.ap.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.kgi.airloanex.common.dto.db.CaseAuth;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.owasp.esapi.errors.IntrusionException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class CaseAuthDao extends CRUDQDao<CaseAuth> {

    @Override
    public int Create(CaseAuth fullItem) {
        String sql = "INSERT INTO CaseAuth " 
        + "(UniqId, UniqType, AuthType, EntType, MBCValid, MBCCheckCode, MBCMessage, ChtAgree, ChtAgreeTime, CreateTime) " 
+ " values (?,?,?,?,?,?,?,?,?,GETDATE())";
        int rs = this.getJdbcTemplate().update(sql, new Object[]{
                fullItem.getUniqId(), fullItem.getUniqType(), fullItem.getAuthType(), fullItem.getEntType(), fullItem.getMbcValid()
                , fullItem.getMbcCheckCode(), fullItem.getMbcMessage(), fullItem.getChtAgree(), fullItem.getChtAgreeTime()
        });
        return rs;
    }

    @Override
    public CaseAuth Read(CaseAuth keyItem) {
        String sql = "Select * FROM CaseAuth where UniqId = ? AND 1=1";

        //[2019.09.12 GaryLiu] ==== START : 白箱測試 ====
        try{
            //return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseAuth.class), new Object[]{keyItem.getUniqId()});
        	String uniqId = keyItem.getUniqId();
//        	uniqId = ESAPI.validator().getValidInput("", uniqId, "AntiXSS", Integer.MAX_VALUE, false);
        	return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseAuth.class), new Object[]{uniqId});
        } catch (IntrusionException e) {
        	System.out.println("CaseAuth查無資料");
        	return null;
        } catch (DataAccessException ex) {
            System.out.println("CaseAuth查無資料");
            return null;
        }
        //[2019.09.12 GaryLiu] ====  END  : 白箱測試 ====
    }

    @Override
    public int Update(CaseAuth fullItem) {
        String sql = "UPDATE CaseAuth Set AuthType=?,EntType=?,MBCValid=?,MBCCheckCode=?,MBCMessage=?,ChtAgree=?,ChtAgreeTime=? where UniqId=?";
        return this.getJdbcTemplate().update(sql,new Object[]{fullItem.getAuthType(),fullItem.getEntType(),fullItem.getMbcValid(),fullItem.getMbcCheckCode(),
        fullItem.getMbcMessage(),fullItem.getChtAgree(),fullItem.getChtAgreeTime(),fullItem.getUniqId()});
    }

    @Override
    public int Delete(CaseAuth keyItem) {
        String sql = "DELETE CaseAuth where UniqId = ?";
        return this.getJdbcTemplate().update(sql,new Object[]{ keyItem.getUniqId()});
    }

    @Override
    public List<CaseAuth> Query(CaseAuth keyItem) {
        //TODO 不確定查詢條件
        String sql = "Select * from CaseAuth where 1=1";
        List<String> params = new ArrayList<>();
        if(!keyItem.getAuthType().equals("")){
            sql = sql.concat(" AND AuthType = ?");
            params.add(keyItem.getAuthType());
        }
        if(!keyItem.getEntType().equals("")){
            sql = sql.concat(" AND EntType = ?");
            params.add(keyItem.getEntType());
        }
        if(!keyItem.getMbcValid().equals("")){
            sql = sql.concat(" AND MBCValid = ?");
            params.add(keyItem.getMbcValid());
        }
        if(!keyItem.getMbcCheckCode().equals("")){
            sql = sql.concat(" AND MBCCheckCode = ?");
            params.add(keyItem.getMbcCheckCode());
        }
        if(!keyItem.getMbcMessage().equals("")){
            sql = sql.concat(" AND MBCMessage = ?");
            params.add(keyItem.getMbcMessage());
        }
        if(!keyItem.getChtAgree().equals("")){
            sql = sql.concat(" AND ChtAgree = ?");
            params.add(keyItem.getChtAgree());
        }
        if(!keyItem.getChtAgreeTime().equals("")){
            sql = sql.concat(" AND ChtAgreeTime <= ?");
            params.add(keyItem.getChtAgreeTime());
        }


        return this.getJdbcTemplate().query(sql, params.toArray(),new BeanPropertyRowMapper<>(CaseAuth.class));
    }

    public CaseAuth findByUniqId(String uniqId) {
        return Read(new CaseAuth(uniqId));
    }

    public List<Map<String, Object>> getVerifyTypeFroAddNewCase(String caseNo){
        //取出同一個UniqID下 MBC、OTP、 (信用卡、銀行帳戶) FOR 新戶 四種驗身方式
        String sql = "select AuthType,CreditVerify.VerifyStatus,CreditVerify.VerifyType " +
                "from CaseAuth " +
                "       left join CreditVerify on CreditVerify.UniqId = CaseAuth.UniqId " +
                "where CaseAuth.UniqId=? AND 1=1";
        return this.getJdbcTemplate().queryForList(sql, new Object[] { caseNo });

    }

    public int saveCaseAuth(String uniqId, String uniqType, String authType, String entType, String MBCValid, String MBCCheckCode, String MBCMessage, String ChtAgree, String ChtAgreeTime) {
        return this.getJdbcTemplate().update("DELETE FROM CaseAuth WHERE UniqId=?  INSERT INTO CaseAuth (UniqId,UniqType,AuthType,EntType,MBCValid,MBCCheckCode,MBCMessage,ChtAgree,ChtAgreeTime,CreateTime) "
                        + "VALUES (?,?,?,?,?,?,?,?,?,getdate());",
                new Object[]{uniqId, uniqId, uniqType, authType, entType, MBCValid, MBCCheckCode, MBCMessage, ChtAgree, ChtAgreeTime});

    }

    /**
     * 查詢CaseAuth,取得指定時間格式120
     * @param uniqId
     * @return
     */
    public CaseAuth getCaseAuthByCaseNoWithConvert(String uniqId) {
    	String sql ="SELECT UniqId AS uniqId, UniqType AS uniqType, AuthType AS authType, EntType AS entType, MbcValid AS mbcValid, MbcCheckCode AS mbcCheckCode, MbcMessage AS mbcMessage, ChtAgree AS chtAgree, convert(varchar,ChtAgreeTime,120) AS chtAgreeTime, convert(varchar,CreateTime,120) AS createTime FROM CaseAuth WHERE UniqId = ?";
    	return this.getJdbcTemplate().queryForObject(sql,  new BeanPropertyRowMapper<>(CaseAuth.class), new Object[]{uniqId});
    }

}
