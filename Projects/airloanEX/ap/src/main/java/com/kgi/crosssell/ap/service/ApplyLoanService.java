package com.kgi.crosssell.ap.service;

import com.kgi.airloanex.ap.dao.CaseDataDao;
import com.kgi.airloanex.ap.dao.CreditVerifyDao;
import com.kgi.airloanex.common.dto.db.CaseData;
import com.kgi.airloanex.common.dto.db.CreditVerify;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.json.JSONObject;

@Service
public class ApplyLoanService {

    @Autowired
    private CaseDataDao casedatadao;

    @Autowired
    private CreditVerifyDao creditVerifyDao;

    public String getApplyDataByCaseNo(String CaseNo) {
        // 改成json object
        CaseData caseData = casedatadao.findByCaseNo(CaseNo);
        // mp.values().removeIf(Objects::isNull);
        // mp.remove("UTime");
        JSONObject ob = JSONObject.fromObject(caseData);
        return ob.toString();
    }

    // [2019.12.24 GaryLiu] ==== START : 根據案件編號取得驗身狀態 ====
    public String getCreditVerifyByCaseNo(String caseNo) throws Exception {
        CreditVerify creditVerify = new CreditVerify();
        creditVerify.setUniqId(caseNo);
        creditVerify = creditVerifyDao.Read(creditVerify);
        JSONObject obj = JSONObject.fromObject(creditVerify);
        String result = obj.toString();
        if (ESAPI.validator().isValidInput("", result, "AntiXSS", Integer.MAX_VALUE, true)) {
            result = ESAPI.validator().getValidInput("", result, "AntiXSS", Integer.MAX_VALUE, true);
        }
        if (result != null && result.length() > 0) {
            return result;
        } else {
            return "";
        }

    }

    public String getCaseNoByCaseNoWeb(String caseNoWeb) {
        return casedatadao.findCaseNoByCaseNoWeb(caseNoWeb);
    }

    public String getCaseNoByIdNo(String idno) {
        return casedatadao.findCaseNoByIdno(idno);
    }

    public String getCaseNoByToken(String token) {
        return casedatadao.findCaseNoByToken(token);
    }

    public String getCaseNoByTokenUseMemoTokenInfo(String token) {
        return casedatadao.findCaseNoByTokenUseMemoTokenInfo(token);
    }

}
