package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.MailHistory;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class MailHistoryDao extends CRUDQDao<MailHistory> {

    @Override
    public int Create(MailHistory fullItem) {
        String sql = "insert into MailHistory values(?,?,?,?,?,?,?,?,?,GETDATE(),GETDATE())";

        this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getUniqId(), fullItem.getMailType(), fullItem.getStatus(),
                        fullItem.getErrorMessage(), fullItem.getTitle(), fullItem.getContent(),
                        fullItem.getTemplateId(), fullItem.getAttatchment(), fullItem.getEMailAddress() });
        return 0;
    }

    @Override
    public MailHistory Read(MailHistory keyItem) {
        try {
            String sql = "Select * from MailHistory where Serial=?";
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(MailHistory.class),
                    new Object[] { keyItem.getSerial() });
        } catch (DataAccessException ex) {
            System.out.println("MailHistory查無資料");
            return null;
        }
    }

    @Override
    public int Update(MailHistory fullItem) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE MailHistory SET ");
        sb.append("Status = ? ,");
        sb.append("ErrorMessage = ?,");
        sb.append("UpdateTime=getDate()");
        sb.append(" where Serial = ?");

        return this.getJdbcTemplate().update(sb.toString(),
                new Object[] { fullItem.getStatus(), fullItem.getErrorMessage(), fullItem.getSerial() });
    }

    @Override
    public int Delete(MailHistory keyItem) {
        String sql = "DELETE FROM MailHistory where Serial = " + keyItem.getSerial();
        return this.getJdbcTemplate().update(sql);
    }

    @Override
    public List<MailHistory> Query(MailHistory keyItem) {
        String sql = "SELECT * FROM MailHistory where Status = ?";
        return this.getJdbcTemplate().query(sql, new BeanPropertyRowMapper<>(MailHistory.class),
                new Object[] { keyItem.getStatus() });
    }

}
