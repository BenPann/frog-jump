package com.kgi.crosssell.ap.dao;

import java.io.IOException;
import java.util.Map;

import com.fuco.prod.common.util.GlobalProp;
import com.fuco.prod.common.util.HttpUtil3;
import com.kgi.airloanex.ap.dao.APSDao;
import com.kgi.eopend3.common.util.DateUtil;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class HttpTmpDao {
    @Autowired
    APSDao apsDao;
    @Autowired
    APIMDao apimDao;

    /** 使用httpPost 發出交易 (會寫Apilog) */
    public String sendHttpPost(String apiName, String url, String req) throws IOException {
        String res = "";
        String startTime = "";
        String endTime = "";
        try {
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            res = HttpUtil3.doPostRequest(url, req, HttpUtil3.JSON);
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            apsDao.insertApiLog(apiName, req, res, startTime, endTime);
        }
        return res;

    }

    public String sendApim(String apiName, String req, Map<String, String> optionMap) throws IOException {
        String res = "";
        String startTime = "";
        String endTime = "";
        String apimUrl = GlobalProp.getInstance().getString("APIMServerHost");
        String resultValidation = "";

        System.out.println("#### 透過 APIM 發查外部資料 ... apimUrl=" + apimUrl + ", apiName=" + apiName + ", req=" + req);

        try {
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            res = apimDao.queryAPIM(apimUrl, apiName, req, optionMap);
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");

            if (ESAPI.validator().isValidInput("", res, "AntiXSS", Integer.MAX_VALUE, true)) {
                resultValidation = res;
            }

            if (resultValidation.equals("")) {
                System.out.println("###### ESAPI validation 失敗 !!!!! ");
                System.out.println("###### ESAPI validation 失敗-start-----------");
                System.out.println(res);
                resultValidation = res; // 仍回傳
                System.out.println("###### ESAPI validation 失敗-end-------------");
            }

        } catch (IntrusionException e) {
            System.out.println("ESAPI validation failed");
            System.out.println(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ("".equals(endTime)) {
                endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            }

            if ("".equals(resultValidation)) {
                apsDao.insertApiLog(apiName, req, "-遠端未回傳資料，可能服務異常-", startTime, endTime);
            } else {
                apsDao.insertApiLog(apiName, req, resultValidation, startTime, endTime);
            }

        }
        return resultValidation;

    }
}
