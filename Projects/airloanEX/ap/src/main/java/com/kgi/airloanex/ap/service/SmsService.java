package com.kgi.airloanex.ap.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.CaseDataDao;
import com.kgi.airloanex.ap.dao.JobAlertDao;
import com.kgi.airloanex.common.dto.customDto.SendSMSRspDto;
import com.kgi.airloanex.common.dto.db.CaseData;
import com.kgi.airloanex.common.dto.db.JobAlert;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * 發送簡訊 sendSMS
 */
@Service
public class SmsService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ConfigDao configDao;

    @Autowired
    private CaseDataDao caseDataDao;

    @Autowired
    private JobAlertDao jobAlertDao;

    @Autowired
    private KgiService kgiService;

    @Autowired
    private AirloanEXConfig globalConfig;

    public boolean sendSms(String uniqId, String phoneNumber, String billDepart, String message, String previousSendTime) throws Exception {
        Date nowDate = new Date();
        if (StringUtils.isBlank(previousSendTime)) {                    
            return sendSms(uniqId, phoneNumber, billDepart, message);
        } else { // if (prev > 10 minutes) then send
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");
            Date prevDate = sdf.parse(previousSendTime);
            long diffInMillies = Math.abs(prevDate.getTime() - nowDate.getTime());
            long diffInMinutes = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
            if (diffInMinutes >= 10) { // TODO: Charles: Refactoring 10 minutes to Config.
                return sendSms(uniqId, phoneNumber, billDepart, message);
            }
        }
        return false;
    }

    private boolean sendSms(String uniqId, String phoneNumber, String billDepart, String message) throws Exception {
        boolean smsOn = StringUtils.equals("Y", globalConfig.SMS_on());
        if (smsOn) { // 發送簡訊
            SendSMSRspDto rsp = kgiService.call_sendSMS(uniqId, phoneNumber, billDepart, message);
            return StringUtils.equals("", rsp.getCheckCode());
        } else { // 不發送簡訊
            logger.info("### 不發送簡訊");
            return true;
        }
    }

    public String sessiontimeout(String uniqId) {

        try {
            boolean smsOn = StringUtils.equals("Y", globalConfig.SMS_on());
            if (smsOn && StringUtils.isNotBlank(uniqId)) {
                CaseData caseData = caseDataDao.findByCaseNo(uniqId);
                String phone = caseData.getPhone();
                if (StringUtils.isNotBlank(phone)) {
                    String alertType = "SMS";
                    String clazz = "Sms.Case.Timeout";
                    String billDepart = globalConfig.SMS_applyCalResult_BillDep();
                    String msgContent = globalConfig.SMS_applySessiontimeout().replace("%ApplyStart%", globalConfig.Apply_Start());
                    JobAlert ja = new JobAlert();
                    ja.setAlertType(alertType);
                    ja.setClazz(clazz);
                    ja.setUniqId(uniqId);
                    ja.setBillDepart(billDepart);
                    ja.setPhone(phone);
                    ja.setMsgContent(msgContent);
                    ja.setContent001(null);
                    ja.setContent002(null);
                    ja.setCreateTime(DateUtil.GetDateFormatString("yyyy-MM-dd HH:mm:ss.SSS"));
                    ja.setOrderTime(DateUtil.GetDateFormatString("yyyy-MM-dd HH:mm:ss.SSS"));
                    ja.setSendTime(null);
                    ja.setSendStatus("02");
                    jobAlertDao.Create(ja);
                }
            }
            return WebResult.GetResultString(0, "成功", null);
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }

    }
}
