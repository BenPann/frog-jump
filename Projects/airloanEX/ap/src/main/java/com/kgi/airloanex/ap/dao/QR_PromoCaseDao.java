package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.QR_PromoCase;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class QR_PromoCaseDao extends CRUDQDao<QR_PromoCase> {

    @Override
    public int Create(QR_PromoCase fullItem) {
        String sql = "INSERT INTO QR_PromoCase Values(?,?,?,?,?,?,?,?,?,?)";
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getUniqId(), fullItem.getUniqType(), fullItem.getTransDepart(),
                        fullItem.getTransMember(), fullItem.getPromoDepart(), fullItem.getPromoMember(),
                        fullItem.getPromoDepart1(), fullItem.getPromoMember1(), fullItem.getPromoDepart2(),
                        fullItem.getPromoMember2() });
    }

    @Override
    public QR_PromoCase Read(QR_PromoCase keyItem) {
        String sql = "SELECT * FROM QR_PromoCase WHERE UniqId = ?;";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_PromoCase.class),
                    new Object[] { keyItem.getUniqId() });
        } catch (DataAccessException ex) {
            System.out.println("QR_PromoCase查無資料");            
            return null;
        }
    }

    @Override
    public int Update(QR_PromoCase fullItem) {
        String sql = "UPDATE QR_PromoCase SET TransDepart = ?, TransDepart = ?,PromoDepart = ?,PromoMember = ? ,PromoDepart1 = ?,PromoMember1 = ? ,PromoDepart2 = ?,PromoMember2 = ? WHERE UniqId = ?";
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getTransDepart(), fullItem.getTransMember(), fullItem.getPromoDepart(),
                        fullItem.getPromoMember(), fullItem.getPromoDepart1(), fullItem.getPromoMember1(),
                        fullItem.getPromoDepart2(), fullItem.getPromoMember2(), fullItem.getUniqId() });
    }

    @Override
    public int Delete(QR_PromoCase keyItem) {
        String sql = "DELETE QR_PromoCase WHERE UniqId = ? ";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getUniqId() });
    }

    @Override
    public List<QR_PromoCase> Query(QR_PromoCase keyItem) {
        return null;
    }
}