package com.kgi.airloanex.ap.dao;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.service.JobLogService;
import com.kgi.airloanex.common.dto.db.FastPass;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.ap.dao.HttpDao;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.NodeList;

// import org.owasp.esapi.errors.IntrusionException;
@Service
public class AmlDao {
	private Logger logger = LogManager.getLogger(SOAPDao.class.getName());

	private QName ServiceBody_QNAME = new QName("http://kgiaml/", "checkAMLResponse");

	@Autowired
	HttpDao httpDao;

	@Autowired
	private ConfigDao configDao;

	@Autowired
	private FastPassDao fastPassDao;

	@Autowired
	JobLogService jobLogService;

	/**
	 * 若有 uniqId (申請流程)，則會回寫 TABLE FastPass。
	 * 若無 uniqId (電文呼叫)，則不回寫。
	 */
	public Map<String, String> getAMLResponse(String uniqId, String idno, String system_type, String check_type, String checkDept,
			List<Map<String, Object>> list) throws IOException {

		Date createTime = new Date();
				logger.info("###====AmlDao====###"
				+ " uniqId=" + uniqId + ","
				+ " idno=" + idno + ","
				+ " system_type=" + system_type + ","
				+ " check_type=" + check_type + ","
				+ " checkDept=" + checkDept + ","
				+ " list=" + list);
		
		Map<String, String> resultMap = new HashMap<String, String>();

		String amlUrl = configDao.ReadConfigValue("AML.URL","http://amlsit.kgibank.com/CheckAML/services/CheckAMLPort?wsdl"); // 172.31.10.6 80	

		String HHmmssSSS = DateUtil.GetDateFormatString("HHmmssSSS");

		String req = getAMLRequestStr(idno + HHmmssSSS, system_type, check_type, checkDept, list); // XML
		logger.info("### amlUrl => " + amlUrl);
		logger.info("### XML req => " + req);
		
		String rsp = ""; // XML
		String aml_data = "";
		String aml_result = "";
		try {
			rsp = ESAPI.validator().getValidInput("", httpDao.doSoapPostRequest(amlUrl, req), "Space", Integer.MAX_VALUE, false);
			logger.info("### XML rsp => " + rsp);
			aml_data = rsp;
		} catch (org.owasp.esapi.errors.ValidationException e) {
			e.printStackTrace();
			aml_data = "";
		} catch (Exception e) {
			e.printStackTrace();
			aml_data = "";
		} finally {
			aml_result = processResponse(rsp);

			resultMap.put("aml_data", aml_data);
			resultMap.put("aml_result", aml_result);
		}
		
		// save to FastPass。
		if (StringUtils.isNotBlank(uniqId)) {
			Date endTime = new Date();
			FastPass fp = new FastPass();
			fp.setCaseNo(uniqId);
			if (StringUtils.equals("99", aml_result)) {
				fp.setAML("9");
			} else {
				fp.setAML(StringUtils.substring(aml_result, 0, 1));
			}
			fp.setAddPhoto("0");
			fp.setAMLRequest(req);
			fp.setAMLResponse(rsp);
			fp.setCreateTime(createTime);
			fp.setUpdateTime(createTime);
			fp.setEndTime(endTime);
			logger.info(new Gson().toJson(fp));
			fastPassDao.CreateOrUpdate(fp);
		}
		return resultMap;
	} // end getAMLResponse

	private static String getAMLRequestStr(String unique_key, String system_type, String checkDept, String check_dept,
			List<Map<String, Object>> list) {

		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append(
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
		sb.append("<soap:Body>");
		sb.append("		<ns2:checkAML xmlns:ns2=\"http://kgiaml/\">");
		sb.append("			<arg0>");
		for (int i = 0; i < list.size(); i++) {
			sb.append("  			<checkEntities>");
			sb.append("    				<ch_name>" + list.get(i).get("ch_name") + "</ch_name>");
			sb.append("    				<country>" + list.get(i).get("country") + "</country>");
			sb.append("    				<en_name>" + list.get(i).get("en_name") + "</en_name>");
			sb.append("    				<entity_type>" + list.get(i).get("entity_type") + "</entity_type>");
			sb.append("    				<id>" + list.get(i).get("id") + "</id>");
			sb.append("    				<seq>" + list.get(i).get("seq") + "</seq>");
			sb.append("  			</checkEntities>");
		}
		sb.append("  			<check_dept>" + check_dept + "</check_dept>");
		sb.append("  			<check_type>" + checkDept + "</check_type>");
		sb.append("  			<system_type>" + system_type + "</system_type>");
		sb.append("  			<unique_key>" + unique_key + "</unique_key>");
		sb.append("			</arg0>");
		sb.append("		</ns2:checkAML>");
		sb.append("</soap:Body>");
		sb.append("</soap:Envelope>");
		return sb.toString();
    } // end getAMLRequestStr
    
    private String processResponse(String soapStr) {
		// 取不到結果 一樣帶9
		String result = "99";
		try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(soapStr.getBytes("UTF-8"))) {
			SOAPMessage response = MessageFactory.newInstance(SOAPConstants.DEFAULT_SOAP_PROTOCOL).createMessage(null,
					byteArrayInputStream);
			SOAPBody soapBody = response.getSOAPBody();
			Iterator<?> itServiceBody = soapBody.getChildElements(ServiceBody_QNAME);
			System.out.println("### soapBody ==> " + soapBody);
			System.out.println("### itServiceBody ==> " + itServiceBody);
			if (itServiceBody.hasNext()) {
				SOAPElement emtServiceBody = (SOAPBodyElement) itServiceBody.next();
				NodeList nodeList = (NodeList) emtServiceBody.getChildNodes();
				System.out.println("### emtServiceBody ==> " + emtServiceBody);
			    System.out.println("### nodeList ==> " + nodeList);
				try {
					for (int index1 = 0; index1 < nodeList.getLength(); index1++) {
						Node node1 = (Node) nodeList.item(index1);
						if (node1.getNodeName().endsWith("return")) {
							for (int index2 = 0; index2 < node1.getChildNodes().getLength(); index2++) {
								Node node2 = (Node) node1.getChildNodes().item(index2);
								if(node2.getNodeName().equals("error_code") && node2.getTextContent().equals("1")) {
									result = "9";
									break;
								}
								if(node2.getNodeName().equals("aml_result")) {
									result = node2.getTextContent();
								}
							}
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} catch (SOAPException e) {
			logger.error("## SOAPException : " + e.toString());
			jobLogService.AddSendAMLChkfailLog("解析電文失敗"+e.toString());
		} catch (IOException e) {
			logger.error("## IOException : " + e.toString());
			jobLogService.AddSendAMLChkfailLog("解析電文失敗"+e.toString());
		}
		return result;
	} // end processResponse
}
