package com.kgi.airloanex.ap.service;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.ChatBotPageTeTriggerDao;
import com.kgi.airloanex.common.dto.db.ChatBotPageTeTrigger;
import com.kgi.airloanex.common.dto.response.ChatbotUrlResp;
import com.kgi.airloanex.common.dto.view.TeTriggerView;
import com.kgi.eopend3.common.dto.WebResult;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChatBotPageTeTriggerService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private ChatBotPageTeTriggerDao chatBotPageTeTriggerDao;

    @Autowired
    private AirloanEXConfig globalConfig;

    public String chatbotUrl(String reqJson) {
        logger.info(reqJson);
        TeTriggerView view = new Gson().fromJson(reqJson, TeTriggerView.class);
        String productType = view.getProductType();
        String flowType = view.getFlowType();
        String pageName = view.getPageName();
        String subPageName = view.getSubPageName();
        ChatBotPageTeTrigger trigger = chatBotPageTeTriggerDao.findTrigger(productType, flowType, pageName, subPageName);
        if (trigger == null) {
            productType = "PL";
            flowType = "apply";
            pageName = "start";
            trigger = chatBotPageTeTriggerDao.findTrigger(productType, flowType, pageName, null);
            logger.error("Fail to find ChatBotPageTeTrigger use default instead.");
        }

        
        String chatbotUrl = globalConfig.ChatbotUrL();
        chatbotUrl = StringUtils.replace(chatbotUrl, "${trigger}", trigger.getTeTrigger());
        chatbotUrl = StringUtils.replace(chatbotUrl, "${productType}", trigger.getProductType());
        // chatbotUrl = StringUtils.replace(chatbotUrl, "${welcome}", trigger.getWelcome());
        // logger.info(chatbotUrl);
        ChatbotUrlResp resp = new ChatbotUrlResp();
        resp.setProductType(trigger.getProductType());
        resp.setFlowType(trigger.getFlowType());
        resp.setPageName(trigger.getPageName());
        resp.setTrigger(trigger.getTeTrigger());
        resp.setWelcome(trigger.getWelcome());
        resp.setChatbotUrl(chatbotUrl);
        return WebResult.GetResultString(0, "成功", resp);
    }
}
