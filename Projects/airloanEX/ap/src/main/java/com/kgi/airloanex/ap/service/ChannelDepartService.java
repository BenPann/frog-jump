package com.kgi.airloanex.ap.service;

import static com.kgi.airloanex.common.PlContractConst.KB_9743_CHANNEL_ID;
import static com.kgi.airloanex.common.PlContractConst.KB_9743_DEPART_ID;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.dao.CS_CommonDataDao;
import com.kgi.airloanex.ap.dao.CaseDataDao;
import com.kgi.airloanex.ap.dao.ContractApiLogDao;
import com.kgi.airloanex.ap.dao.QR_ChannelDepartListDao;
import com.kgi.airloanex.ap.dao.QR_ChannelListDao;
import com.kgi.airloanex.ap.dao.QR_PromoCaseDao;
import com.kgi.airloanex.ap.dao.QR_PromoCaseUserInputDao;
import com.kgi.airloanex.ap.dao.QR_ShortUrlDao;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.db.CS_CommonData;
import com.kgi.airloanex.common.dto.db.CaseData;
import com.kgi.airloanex.common.dto.db.QR_ChannelDepartList;
import com.kgi.airloanex.common.dto.db.QR_ChannelList;
import com.kgi.airloanex.common.dto.db.QR_PromoCase;
import com.kgi.airloanex.common.dto.db.QR_PromoCaseUserInput;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.ap.dao.EntryDataDao;
// import com.kgi.airloanex.ap.dao.CaseDataDao;
// import com.kgi.airloanex.ap.dao.QR_ChannelDepartListEOPDao;
// import com.kgi.airloanex.common.dto.db.CaseData;
// import com.kgi.airloanex.common.dto.db.QR_ChannelDepartListEOP;
// import com.kgi.airloanex.common.dto.db.QR_ChannelList;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.db.EntryData;
import com.kgi.eopend3.common.dto.db.QR_ShortUrl;
import com.kgi.eopend3.common.dto.respone.ChannelDepartListResp;
import com.kgi.eopend3.common.dto.respone.ChannelListResp;
import com.kgi.eopend3.common.dto.view.ChannelListView;
import com.kgi.eopend3.common.dto.view.ShortUrlView;
import com.kgi.eopend3.common.util.CheckUtil;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class ChannelDepartService {

    @Autowired
    private QR_ChannelListDao qrChannelListDao;

    @Autowired
    private QR_ChannelDepartListDao qrChannelDepartListDao;

    // @Autowired
    // private QR_ChannelDepartListEOPDao qrChannelDepartListEOPDao;

    // @Autowired
    // private CaseDataDao caseDataDao;

    @Autowired
    private ContractApiLogDao contractApiLogDao;

    @Autowired
    private QR_ShortUrlDao qrShortUrlDao;   

    @Autowired
    private EntryDataDao entryDataDao;

    @Autowired
    private QR_PromoCaseUserInputDao promoCaseUserInputDao;

    @Autowired
    private QR_PromoCaseDao promoCaseDao;

    @Autowired
    private CS_CommonDataDao cs_CommonDataDao;

    @Autowired
    private ConfigDao configDao;

    @Autowired
    private CaseDataDao caseDataDao;

    @Autowired
    private CaseDataService caseDataService;
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	public String getChannelList() {
        try {              
            List<ChannelListResp> list = qrChannelListDao.getChannelList();
            if(list!=null&&list.size()>0){
                return WebResult.GetResultString(0,"成功",list);
            } else{
                throw new ErrorResultException(2,"查無資料",null);
            }	
    	} catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
	}

	public String getChannelDepartList(String reqJson) {
        try {
            Gson gson = new Gson();
            ChannelListView channelListView = gson.fromJson(reqJson, ChannelListView.class);
            if (!CheckUtil.check(channelListView)) {
                throw new ErrorResultException(1, "參數錯誤", null);
        	}else{
                List<ChannelDepartListResp> respList = new ArrayList<ChannelDepartListResp>();    
                QR_ChannelDepartList qr_ChannelDepartList = new QR_ChannelDepartList();
                qr_ChannelDepartList.setChannelId(channelListView.getChannelID());
                List<QR_ChannelDepartList> list = qrChannelDepartListDao.Query(qr_ChannelDepartList);
                if(list!=null&&list.size()>0){                    
                    for(QR_ChannelDepartList result : list){
                        respList.add(new ChannelDepartListResp(result.getDepartId(),result.getDepartName()));
                    }
                    return WebResult.GetResultString(0,"成功",respList);
                } else{
                    throw new ErrorResultException(2,"查無資料",null);
                }               
            }			
    	} catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
	}

    public String qrshorturl(String reqJson) {
        try {
            Gson gson = new Gson();
            ShortUrlView shortUrlView = gson.fromJson(reqJson, ShortUrlView.class);
            String startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLIN,"", "/qrshorturl", reqJson, "",startTime,startTime);

            QR_ShortUrl qrShortUrl = null;
            String shortUrl = shortUrlView.getShortUrl();
            if(StringUtils.isNotBlank(shortUrl)) {
                qrShortUrl = qrShortUrlDao.findByShortUrl(shortUrl);
                if (qrShortUrl == null){
                    return WebResult.GetResultString(0,"成功","");
                } else {
                    QR_ShortUrl resp = new QR_ShortUrl();
                    resp.setMember(qrShortUrl.getMember());
                    return WebResult.GetResultString(0,"成功",resp);
                }
            }
            return WebResult.GetResultString(0,"成功","");
    	} catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
    }

    private QR_ShortUrl findShortUrlData(String shortUrl) {
        return qrShortUrlDao.findByShortUrl(shortUrl);
    }

    public String findMemberByShortUrl(String shortUrl) {
        QR_ShortUrl shortUrlData = findShortUrlData(shortUrl);
        return shortUrlData.getMember();
    }

	public String saveShortUrlData(String reqJson) {
        try {
            Gson gson = new Gson();
            ShortUrlView shortUrlView = gson.fromJson(reqJson, ShortUrlView.class);
            String startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLIN,"", "/qrcode", reqJson, "",startTime,startTime);

            String shortUrl = shortUrlView.getShortUrl();
            QR_ShortUrl qrShortUrl = new QR_ShortUrl();
            if(StringUtils.isNotBlank(shortUrl)) {
                qrShortUrl.setShortUrl(shortUrl);
                qrShortUrl = qrShortUrlDao.Read(qrShortUrl);
                if (qrShortUrl==null) {
                    return WebResult.GetResultString(0,"成功","");
                } else {
                    qrShortUrl.setExp("");
                    return WebResult.GetResultString(0,"成功",qrShortUrl);
                }
            }
            return WebResult.GetResultString(0,"成功","");
    	} catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
	}

	// public QR_ChannelDepartListEOP getEOPChannelDepart(String uniqId) {
	// 	String channelid = "";
    //     //先找登入時的推薦人身分
    //     QR_ChannelList channel = qrChannelListDao.getChannelDataByUniqId(uniqId);    
    //     boolean isBank = false;
    //     if (channel != null) {
    //         channelid = channel.getChannelId();
    //         isBank = channel.getChannelType().equals("0");
    //     } else {
    //         isBank = true;
    //         channelid = PlContractConst.DEFAULT_CHANNEL_ID;                 
    //     }

    //     QR_ChannelDepartListEOP result = null;
    
    //     CaseData ed3_CaseData = caseDataDao.Read(new CaseData(uniqId));
    //     if (ed3_CaseData.getPromoDepart().trim().equals("")) {
    //         result = qrChannelDepartListEOPDao.Read(new QR_ChannelDepartListEOP(PlContractConst.QR_CHANNELDEPARTLISTEOP_DEFAULT, PlContractConst.QR_CHANNELDEPARTLISTEOP_DEFAULT));
    //     } else {
    //         result = qrChannelDepartListEOPDao.Read(new QR_ChannelDepartListEOP(channelid, ed3_CaseData.getPromoDepart()));
    //     }

    //     if (isBank) {
    //         if (!ed3_CaseData.getPromoDepart().equals("")) {
    //             result.setPromoDepart1(ed3_CaseData.getPromoDepart());
    //         }
    //         if (!ed3_CaseData.getPromoMember().equals("")) {
    //             result.setPromoMember1(ed3_CaseData.getPromoMember());
    //         }
    //     } else {            
    //         if (!ed3_CaseData.getPromoDepart().equals("")) {
    //             result.setPromoDepart2(ed3_CaseData.getPromoDepart());
    //         }
    //         if (!ed3_CaseData.getPromoMember().equals("")) {
    //             result.setPromoMember2(ed3_CaseData.getPromoMember());
    //         }
    //     }

    //     return result;
    // }

    /** 貸款取得送影像案件時 要帶的Channel Depart 跟 member */
    public QR_ChannelDepartList getLoanChannelDepart(String uniqId) {
        String channelid = "";
        // 先查此人有沒有QRCode資料
        QR_ChannelList channel = this.qrChannelListDao.getChannelDataByUniqId(uniqId);
        QR_PromoCaseUserInput userInput = this.promoCaseUserInputDao.Read(new QR_PromoCaseUserInput(uniqId));
        Boolean isBank = false;
        if (channel != null) {
            channelid = channel.getChannelId();
            isBank = channel.getChannelType().equals("0");
        } else {
            CS_CommonData cs_CommonData = this.cs_CommonDataDao.Read(new CS_CommonData(uniqId));
            if(cs_CommonData!=null) {
                channelid = cs_CommonData.getChannelId();
                QR_ChannelList csChannelList = this.qrChannelListDao.Read(new QR_ChannelList(channelid));
                isBank = csChannelList.getChannelType().equals("0");
            }else {
                EntryData entryData = this.entryDataDao.Read(new EntryData(uniqId));
                if(entryData!=null && StringUtils.isNotBlank(entryData.getEntry())) {
                    channelid = entryData.getEntry().substring(0,2);
                }else {
                    if(userInput!=null&&userInput.getChannelId()!=null&&!userInput.getChannelId().equals("")) {
                        channelid = userInput.getChannelId();
                        QR_ChannelList csChannelList = this.qrChannelListDao.Read(new QR_ChannelList(channelid));
                        isBank = csChannelList.getChannelType().equals("0");            			
                    }else {
                        isBank = true;
                        channelid = KB_9743_CHANNEL_ID ;
                    }
                }
            }
            
        }

        QR_ChannelDepartList result = null;
        //QR_PromoCaseUserInput userInput = this.promoCaseUserInputDao.Read(new QR_PromoCaseUserInput(uniqId));
        // logger.info("### userInput=" + new Gson().toJson(userInput));
        if (userInput == null) {
            // 如果沒有新的資料 就把舊資料轉成新的input
            QR_PromoCase userInputOld = this.promoCaseDao.Read(new QR_PromoCase(uniqId));
            userInput = new QR_PromoCaseUserInput(uniqId);
            if (userInputOld != null) {
                userInput.setUniqType(userInputOld.getUniqType());
                userInput.setPromoDepart(userInputOld.getTransDepart());
                userInput.setPromoMember(userInputOld.getTransMember());
                // 順便塞回去
                this.promoCaseUserInputDao.Create(userInput);
            }
        }

        if (StringUtils.isBlank(userInput.getPromoDepart())) {
            result = this.qrChannelDepartListDao.Read(new QR_ChannelDepartList("-", "-"));
        } else {
            result = this.qrChannelDepartListDao.Read(new QR_ChannelDepartList(channelid, userInput.getPromoDepart()));
        }

        if (result == null) {
            logger.info("Fail to find QR_ChannelDepartList by");
            logger.info("ChannelId=" + channelid);
            logger.info("DepartId=" + userInput.getPromoDepart());
            result = this.qrChannelDepartListDao.Read(new QR_ChannelDepartList("-", "-"));
        }


        // (29) B_S_UNIT：轉介單位
        // 以 userInput 輸入之單位為優先，
        // 若無，則轉介單位為9743
        if (StringUtils.isNotBlank(userInput.getPromoDepart())) {
            result.setTransDepart(userInput.getPromoDepart());
        } else {
            result.setTransDepart(KB_9743_DEPART_ID);
        }

        // (30) B_S_CODE：轉介人員
        // 以 userInput 輸入之人員為優先，
        // 若無，則轉介人員為空值
        if (StringUtils.isNotBlank(userInput.getPromoMember())) {
            result.setTransMember(userInput.getPromoMember());
        } else {
            result.setTransMember("");
        }

        /*
         * 2021/05/12 數位平台申請轉介員編帶入推廣員編邏輯
         * 目前數位平台登入頁之轉介員編/單位未帶入推廣員編/單位，請調整如下：
         * 1-1.數位平台登入頁之員編(轉介)對應轉介單位為DS(8220~8270)或TM(9740、9743、8882)或客服(9621)時，則轉介員編/單位應同步帶入推廣員編/單位。
         * 1-2.數位平台登入頁之員編(轉介)對應轉介單位非DS、TM或客服，則僅帶入轉介單位對應大表後得到的推廣單位欄位(PromoDepart)的值到推廣單位，推廣員編則以空值帶入。
         * 2.數位平台登入頁未輸入員編(轉介)或輸入錯誤員編(轉介)時，轉介單位與推廣單位均為9743、且推廣員編帶入登入頁員編(轉介)資料或空值。
         *
         *  PromoDepart：推廣單位
         *  PromoMember：推廣人員
         */

        // userInput 有員編，則判斷 單位 是否為 DS(8220~8270) 或 TM(9740、9743、8882) 或 客服(9621)
        if (StringUtils.isNotBlank(userInput.getPromoMember())) {

            String[] configValues = configDao.ReadConfigValue("airloanEX.SyncTransInfoToPromoInfo", "").split(",");
            for (String configValue : configValues) {

                // 1-1. 是：轉介資訊(單位、員編) 同步帶入 推廣資訊(單位、員編)
                if (configValue.equals(result.getTransDepart())) {
                    result.setPromoDepart(result.getTransDepart());
                    result.setPromoMember(result.getTransMember());
                    break;

                // 1-2. 否(非DS、TM或客服)：轉介單位 僅帶入轉介單位對應大表後得到的推廣單位欄位(PromoDepart)的值到推廣單位，推廣員編 以空值帶入
                } else {
                    result.setPromoMember("");
                }
            }

        // 沒輸入 數位平台登入頁之員編(轉介員編) 或輸入錯誤員編(轉介)時
        // 2.轉介單位與推廣單位均為9743、且推廣員編帶入登入頁員編(轉介)資料。
        }else{
            result.setPromoDepart(KB_9743_DEPART_ID);
            result.setPromoMember("");
        }

        logger.info("### result=" + result);

//        if (isBank) {
//            // 如果transdepart <> promodepart transmember 塞使用者輸入
//            // 如果transdepart == promodepart transmember 跟 promomember 都塞使用者輸入
//
//            // 畫面上選擇的單位A，填寫員工編號B(QRCode進來則帶入並欄位鎖定)
//            // 送件後
//            // 用A去找QR_ChannelDepartList，找到該列TransDepart,TransMember,PromoDepart,PromoMember
//            // (1).若TransDepart<>PromoDepart
//            // 1.申請書上：轉介單位填A，轉介人員填B，推廣單位填PromoDepart，推廣人員填PromoMember
//            // 2.影像上傳XML中：cSalesUnit填PromoDepart，cSalesId填空
//            // (1).若TransDepart=PromoDepart
//            // 1.申請書上：轉介單位填A，轉介人員填B，推廣單位填PromoDepart，推廣人員填B
//            // 2.影像上傳XML中：cSalesUnit填PromoDepart，cSalesId填B
//
//            if (result.getTransDepart().equals(result.getPromoDepart())) {
//                if (!userInput.getPromoMember().equals("")) {
//                    result.setPromoMember(userInput.getPromoMember());
//                }
//            }
//            if (!userInput.getPromoDepart().equals("")) {
//                result.setTransDepart(userInput.getPromoDepart());
//            }
//            if (!userInput.getPromoMember().equals("")) {
//                result.setTransMember(userInput.getPromoMember());
//            }
//
//        } else {
//            // 畫面上填寫的單位A，填寫員工編號B(QRCode進來則帶入並欄位鎖定)
//            // 送件後
//            // 用A去找QR_ChannelDepartList，找到該列TransDepart,TransMember,PromoDepart,PromoMember
//            // 1.申請書上：轉介單位填A，轉介人員填B，推廣單位填PromoDepart，推廣人員填PromoMember
//            // 2.影像上傳XML中：cSalesUnit填PromoDepart，cSalesId填PromoMember
//
//            // 將使用者輸入的資料 寫入至transDerpart跟transmember 空的話就不塞入
//            if (!userInput.getPromoDepart().equals("")) {
//                result.setTransDepart(userInput.getPromoDepart());
//            }
//            if (!userInput.getPromoMember().equals("")) {
//                result.setTransMember(userInput.getPromoMember());
//            }
//        }

        return result;
    }

    /** 信用卡取得送影像案件時 要帶的Channel Depart 跟 member */
    public QR_ChannelDepartList getCreditChannelDepart(String uniqId) {
        String channelid = "";
        // 先查此人有沒有QRCode資料
        QR_ChannelList channel = this.qrChannelListDao.getChannelDataByUniqId(uniqId);
        QR_PromoCaseUserInput userInput = this.promoCaseUserInputDao.Read(new QR_PromoCaseUserInput(uniqId));
        Boolean isBank = false;
        if (channel != null) {
            channelid = channel.getChannelId();
            isBank = channel.getChannelType().equals("0");
        } else {
            CS_CommonData cs_CommonData = this.cs_CommonDataDao.Read(new CS_CommonData(uniqId));
            if(cs_CommonData!=null) {
                channelid = cs_CommonData.getChannelId();
                QR_ChannelList csChannelList = this.qrChannelListDao.Read(new QR_ChannelList(channelid));
                isBank = "0".equals(csChannelList.getChannelType());
            }else {
                // Get Channel by EntryData
                QR_ChannelList csChannelList = this.qrChannelListDao.getChannelByEntryData(uniqId) ;
                if(csChannelList != null
                    && csChannelList.getChannelId() != null
                    &&!"".equals(csChannelList.getChannelId())) {
                    channelid = csChannelList.getChannelId() ;
                    isBank = "0".equals(csChannelList.getChannelType());
                } else {
                    if(userInput!=null&&userInput.getChannelId()!=null&&!userInput.getChannelId().trim().equals("")) {
                        channelid = userInput.getChannelId();
                        csChannelList = this.qrChannelListDao.Read(new QR_ChannelList(channelid));
                        isBank = "0".equals(csChannelList.getChannelType());            			
                    }else {
                        isBank = true;
                        channelid = KB_9743_CHANNEL_ID;
                    }
                }
            }
        }

        QR_ChannelDepartList result = null;

        // 先找新表
        //QR_PromoCaseUserInput userInput = this.promoCaseUserInputDao.Read(new QR_PromoCaseUserInput(uniqId));
        if (userInput == null) {
            // 如果沒有新的資料 就把舊資料轉成新的input
            QR_PromoCase userInputOld = this.promoCaseDao.Read(new QR_PromoCase(uniqId));
            userInput = new QR_PromoCaseUserInput(uniqId);
            if (userInputOld != null) {
                userInput.setUniqType(userInputOld.getUniqType());
                if (isBank) {
                    userInput.setPromoDepart(userInputOld.getPromoDepart1());
                    userInput.setPromoMember(userInputOld.getPromoMember1());
                } else {
                    userInput.setPromoDepart(userInputOld.getPromoDepart2());
                    userInput.setPromoMember(userInputOld.getPromoMember2());
                }
                // 順便塞回去
                this.promoCaseUserInputDao.Create(userInput);
            }
        }

        if (userInput.getPromoDepart().trim().equals("")) {
            result = this.qrChannelDepartListDao.Read(new QR_ChannelDepartList("-", "-"));
        } else {
            // 依據是否非商銀 將使用者輸入蓋掉 預設值 空的話就不塞入
            result = this.qrChannelDepartListDao.Read(new QR_ChannelDepartList(channelid, userInput.getPromoDepart()));
        }

        if (isBank) {
            // 畫面上選擇的單位A，填寫的員工編號B(QRCode進來則帶入並欄位鎖定)
            // 送件後
            // 用A去找QR_ChannelDepartList，找到該列PromoDepart1,PromoMember1,PromoDepart2,PromoMember2
            // 1.申請書上：推廣單位一填A，推廣人員一填B，推廣單位二PromoDepart2，推廣人員二PromoMember2
            // 2.影像上傳XML中：cSalesUnit填A，cSalesId填B
            if (!userInput.getPromoDepart().equals("")) {
                result.setPromoDepart1(userInput.getPromoDepart());
            }
            if (!userInput.getPromoMember().equals("")) {
                result.setPromoMember1(userInput.getPromoMember());
            }
        } else {
            // 畫面上填寫的單位A，填寫的員工編號B(QRCode進來則帶入並欄位鎖定)
            // 送件後
            // 用A去找QR_ChannelDepartList，找到該列PromoDepart1,PromoMember1,PromoDepart2,PromoMember2
            // 1.申請書上：推廣單位一填PromoDepart1，推廣人員一填PromoMember1，推廣單位二填A，推廣人員二填B
            // 2.影像上傳XML中：cSalesUnit填PromoDepart1，cSalesId填PromoMember1
            if (!userInput.getPromoDepart().equals("")) {
                result.setPromoDepart2(userInput.getPromoDepart());
            }
            if (!userInput.getPromoMember().equals("")) {
                result.setPromoMember2(userInput.getPromoMember());
            }
        }

        return result;
    }

    public String getAssignDepart(QR_ChannelDepartList channelDepart, String uniqType, String userType) {
        String assignDepart = "";
        if (uniqType.equals("02")) {
            assignDepart = channelDepart.getAssignDepart();
        } else {
            if (userType.equals("1")) {
                assignDepart = channelDepart.getAssignDepartExist();
            } else {
                assignDepart = channelDepart.getAssignDepartNew();
            }
        }
        return assignDepart;
    }
    
    public String getFinalChannelID(String uniqId) {
        String channelid = "";
            // 先查此人有沒有QRCode資料
        QR_ChannelList qrchannel = this.qrChannelListDao.getChannelDataByUniqId(uniqId);
        if (qrchannel != null) {
            channelid = qrchannel.getChannelId();
        } else {
            CS_CommonData cs_CommonData = this.cs_CommonDataDao.Read(new CS_CommonData(uniqId));
            if(cs_CommonData!=null) {
                channelid = cs_CommonData.getChannelId();        		
            }else {
                EntryData entryData = this.entryDataDao.Read(new EntryData(uniqId));
                if(entryData!=null&&!entryData.getEntry().equals("")) {
                    channelid = entryData.getEntry().substring(0,2);
                }else {
                    QR_PromoCaseUserInput userInput = this.promoCaseUserInputDao.Read(new QR_PromoCaseUserInput(uniqId));
//            		if(userInput!=null&&!userInput.getPromoDepart().equals("")) {
                        if(userInput != null 
                        && userInput.getChannelId()!=null
                        &&!"".equals(userInput.getChannelId())) {
                            channelid = userInput.getChannelId();
                        }else {                    		
                            channelid = KB_9743_CHANNEL_ID;
                        }
//            		}
                }
            }
        }        
        return channelid;
    }

    public QR_ChannelDepartList findQR_ChannelDepartList(String uniqId) {
        CaseData caseData = caseDataDao.ReadByCaseNo(uniqId);
        String channelId = caseDataService.channelId(uniqId);
        String departId = caseDataService.departId(caseData.getAgentNo(), caseData.getAgentDepart());
        return findQR_ChannelDepartList(channelId, departId);
    }

    public QR_ChannelDepartList findQR_ChannelDepartList(String channelId, String departId) {
        QR_ChannelDepartList qr_ChannelDepartList = qrChannelDepartListDao.queryChannelIdAndDepartId(channelId, departId);

        if (qr_ChannelDepartList == null) { // Default KB 9743
            qr_ChannelDepartList = new QR_ChannelDepartList();
            qr_ChannelDepartList.setChannelId(KB_9743_CHANNEL_ID);
            qr_ChannelDepartList.setDepartId(KB_9743_DEPART_ID);
            logger.info("### channelId=KB");
            logger.info("### departId=9743");
            qr_ChannelDepartList = qrChannelDepartListDao.Read(qr_ChannelDepartList);
        }
        return qr_ChannelDepartList;
    }
    
}
