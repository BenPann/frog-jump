package com.kgi.airloanex.ap.pdf;

import static com.kgi.airloanex.common.PlContractConst.UNIQ_TYPE_02;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.CaseDataRelationDegreeDao;
import com.kgi.airloanex.ap.dao.CaseDocumentDao;
import com.kgi.airloanex.ap.dao.PDFDao;
import com.kgi.airloanex.ap.pdf.dto.PDFCaseData;
import com.kgi.airloanex.ap.service.APSPlContractService;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.db.CaseDataRelationDegree;
import com.kgi.airloanex.common.dto.db.CaseDocument;
import com.kgi.airloanex.common.util.ExportUtil;
import com.kgi.eopend3.ap.config.GlobalConfig;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class CaseRelationDegreePDF extends BasePDFDocument {

    
    private static final Logger logger = LogManager.getLogger(CaseRelationDegreePDF.class.getName());

    private String _uniqId = "";

    @Autowired
    private GlobalConfig global;
    
	@Autowired
    private AirloanEXConfig globalConfig;

    @Autowired
    private APSPlContractService apsService;

    @Autowired
    private PDFDao pdfDao;

    @Autowired
    private CaseDataRelationDegreeDao caseDataRelationDegreeDao;

    @Autowired
    private CaseDocumentDao caseDocumentDao;

    private PDFCaseData pdfCaseData = null;
    private List<CaseDataRelationDegree> caseDataRelationDegrees = new ArrayList<>();

    private String templateVersion = "";
    
    public CaseRelationDegreePDF(ApplicationContext appContext) {
        super(appContext);
    }

    @Override
    public void init(String... params) throws Exception {
        this._uniqId = params[0];
        // 是否加密
        this.setUseEncrepyt(true);
        // this.setEncryptPassword(idno);
        // 是否使用浮水印
        this.setUseWaterMark(true);
		this.setWaterMarkText(globalConfig.IMG_WATER_FONT());
		this.setWaterMarkFont(globalConfig.PDF_FONT_WATER_TTF());
        // 是否要PDF轉成圖片
        this.setPdfToImage(false);
        // 是否存DB
        this.setUseSaveToDB(true);
        // 是否發信
        this.setUseSendEMail(false);

    }

    @Override
    public void beforePdfProcess() {
        try {
            // 取得Html的方法
            String StaticFilePath = global.StaticFilePath;
            String htmlUrl = StaticFilePath + globalConfig.PDF_RelationDegree_TemplateFilePath();
            templateVersion = globalConfig.PDF_RelationDegree_VerNo();
            logger.info("### htmlUrl => " + htmlUrl);
            InputStream input = new FileInputStream(new File(htmlUrl));
            this.setRawHtml(this.getContentFromFilePath(input));

            pdfCaseData = pdfDao.loadCaseData(this._uniqId);
            caseDataRelationDegrees = caseDataRelationDegreeDao.findByUniqId(this._uniqId);

            this.setEncryptPassword(pdfCaseData.idno);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("取得PDF檔案失敗! CaseNo = " + this._uniqId);
        }
    }

    @Override
    public String replaceField(String raw) {
        logger.info("### Begin - replaceField");

        String str = raw;

        String applydate = "";
        String verifyType = "";
        
        boolean hasNoCreditVerify = !pdfCaseData.applycc;
        logger.info("### hasNoCreditVerify=" + hasNoCreditVerify);
        if(hasNoCreditVerify) { // hasNoCreditVerify
        	applydate = pdfCaseData.applyccUtime;
        	verifyType = PlContractConst.CREDITVERIFY_OTP;
        } else { // hasCreditVerify
            applydate = pdfCaseData.CaseNo.substring(4, 12);
            String authType = apsService.externalCheckTypeForAddNewCase(this._uniqId);
            verifyType = apsService.transAuthType(authType);
            logger.info("### authType=" + authType);
            logger.info("### verifyType=" + verifyType);
        }

        Map<String, String> map = new LinkedHashMap<String, String>();

        map.put("[CustomerName]", "");
        map.put("[IdNo]", "");
        map.put("[CoupleName]", "");
        map.put("[CoupleIdNo]", "");

        // [CallTitle1] ... [CallTitle8]
        for (int n = 1; n <= 8; n++) {
            map.put("[CallTitle" + n + "]", "");
            map.put("[CallName" + n + "]", "");
            map.put("[CallIdNo" + n + "]", "");
        }

        // [SelfCorpName1] ... [SelfCorpName5]
        // [CoupleCorpName1] ... [CoupleCorpName5]
        for (int n = 1; n <= 5; n++) {
            map.put("[SelfCorpName" + n + "]", "");
            map.put("[SelfCorpNum" + n + "]", "");
            map.put("[SelfCorpJobTitle" + n + "]", "");
            map.put("[SelfCorpMemo" + n + "]", "");
            map.put("[CoupleCorpName" + n + "]", "");
            map.put("[CoupleCorpNum" + n + "]", "");
            map.put("[CoupleCorpJobTitle" + n + "]", "");
            map.put("[CoupleCorpMemo" + n + "]", "");
        }

        List<CaseDataRelationDegree> self = new ArrayList<>();
        List<CaseDataRelationDegree> couple = new ArrayList<>();
        List<CaseDataRelationDegree> parent = new ArrayList<>();
        List<CaseDataRelationDegree> child = new ArrayList<>();
        List<CaseDataRelationDegree> selfCorps = new ArrayList<>();
        List<CaseDataRelationDegree> coupleCorps = new ArrayList<>();

        for (int i = 0; caseDataRelationDegrees != null && i < caseDataRelationDegrees.size(); i++) {
            CaseDataRelationDegree o = caseDataRelationDegrees.get(i);
            if (StringUtils.equals("0117", o.getRelationDegree())) {
                selfCorps.add(o); // 0117 本人擔任負責人之企業資料
            } else if (StringUtils.equals("0217", o.getRelationDegree())) {
                coupleCorps.add(o); // 0217 配偶擔任負責人之企業資料
            } else if (StringUtils.equals("0", o.getRelationDegree())) {
                self.add(o); // 0 本人
            } else if (StringUtils.equals("08", o.getRelationDegree())) { 
                couple.add(o); // 08 配偶
            } else if (StringUtils.equals("01", o.getRelationDegree())) { 
                parent.add(o); // 01 父母
            } else if (StringUtils.equals("02", o.getRelationDegree())) {
                child.add(o); // 02 子女
            }
        }

        if (self != null && self.size() > 0) {
            map.put("[CustomerName]", self.get(0).getName());
            map.put("[IdNo]", self.get(0).getIdno());
        }

        if (couple != null && couple.size() > 0) {
            map.put("[CoupleName]", couple.get(0).getName());
            map.put("[CoupleIdNo]", couple.get(0).getIdno());
        }

        List<CaseDataRelationDegree> calls = new ArrayList<>();
        calls.addAll(parent);
        calls.addAll(child);
        for (int i = 0; i < calls.size(); i++) {
            int n = i + 1;
            String callTitle = "";
            if (StringUtils.equals("01", calls.get(i).getRelationDegree())) {
                callTitle = "父母";
            } else if (StringUtils.equals("02", calls.get(i).getRelationDegree())) {
                callTitle = "子女";
            }
            map.put("[CallTitle" + n + "]", callTitle);
            map.put("[CallName" + n + "]", calls.get(i).getName());
            map.put("[CallIdNo" + n + "]", calls.get(i).getIdno());
        }

        for (int i = 0; i < selfCorps.size(); i++) {
            int n = i + 1;
            map.put("[SelfCorpName" + n + "]", selfCorps.get(i).getCorpName());
            map.put("[SelfCorpNum" + n + "]", selfCorps.get(i).getCorpNumber());
            map.put("[SelfCorpJobTitle" + n + "]", selfCorps.get(i).getCorpJobTitle());
            map.put("[SelfCorpMemo" + n + "]", selfCorps.get(i).getMemo());
        }

        for (int i = 0; i < coupleCorps.size(); i++) {
            int n = i + 1;
            map.put("[CoupleCorpName" + n + "]", coupleCorps.get(i).getCorpName());
            map.put("[CoupleCorpNum" + n + "]", coupleCorps.get(i).getCorpNumber());
            map.put("[CoupleCorpJobTitle" + n + "]", coupleCorps.get(i).getCorpJobTitle());
            map.put("[CoupleCorpMemo" + n + "]", coupleCorps.get(i).getMemo());
        }

        for (Map.Entry<String, String> entry : map.entrySet()) {
            str = str.replace(entry.getKey(), entry.getValue());    
        }

        str = str.replace("[Auth_Type]", verifyType);
        str = str.replace("[ip_address]", pdfCaseData.ip_address);
        str = str.replace("[Apply_date]", "中華民國".concat(String.valueOf(Integer.parseInt(applydate.substring(0, 4))-1911)) + "年" + applydate.substring(4, 6) + "月" + applydate.substring(6, 8) + "日");
        str = str.replace("[UniqId]", this._uniqId);

        logger.info("### End - replaceField");
        logger.info("### previewHtml.length()=" + str.length());

        // HTML檔 - @export 儲存至檔案系統
        if (globalConfig.isPDF_Export()) {
            ExportUtil.export(global.PdfDirectoryPath + "/" + "CaseRelationDegreePDF.previewHtml.html", str);
        }
        return str;
    }

    @Override
    public void afterPdfProcess() throws Exception {

    }

    @Override
    public void saveToDB(byte[] pdfByte) {
        
        // Save bytes to TABLE: CaseDocument
        CaseDocument cd = new CaseDocument(this._uniqId, UNIQ_TYPE_02, PlContractConst.DOCUMENT_TYPE_LOAN_KYC_PDF);
        cd.setContent(pdfByte);
        cd.setVersion(templateVersion);
        caseDocumentDao.CreateOrUpdate(cd);
        
        // Save string base64 to TABLE: CaseDoc
        String pdfbase64 = Base64.encodeBase64String(pdfByte);
        pdfDao.storePDF(this._uniqId, this.pdfCaseData.idno, pdfbase64, "");
        System.out.println("Save Pdf To DB");

        // PDF檔 - @export 儲存至檔案系統
        if (globalConfig.isPDF_Export()) {
            ExportUtil.export(global.PdfDirectoryPath + "/" + _uniqId + "kyc.pdf", pdfByte);
        }
    }

    @Override
    public void pdfToImage(List<byte[]> byteList) {

    }

    @Override
    public void sendEmail(byte[] pdfByte) {
        // Note! Do not send the RelationDegree report to customer
    }
}
