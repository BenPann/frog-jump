package com.kgi.airloanex.ap.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.google.gson.Gson;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.db.UspGetCddRate;
import com.kgi.eopend3.ap.dao.BaseDao;
import com.kgi.eopend3.common.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class UspDao extends BaseDao  {
    
    @Autowired
	@Qualifier("g08")
    private DataSource dataSource;

    @Autowired
    private ContractApiLogDao contractApiLogDao;
    
    /**
     * AML風險評級介接查詢(Get_CDD_Rate)
     * 
     * @param uniqId
     * @param SystemType 系統代號
     * @param strID 客戶身分證
     * @param strUnitId 發查單位
     * @param strUserId 發查人員員編
     * @return
     * @see AML風險評級介接查詢規格_2020_v1.pptx
     */
    public UspGetCddRate usp_Get_CDD_Rate(String uniqId, String SystemType, String strID, String strUnitId, String strUserId) {
        System.out.println("### usp_Get_CDD_Rate");

        String keyInfo = uniqId;
        String qryName = "call fcfcore.usp_Get_CDD_Rate";
        String queryJson = 
        "{" 
        + "SystemType: \"" + SystemType + "\"," 
        + "strID: \"" + strID + "\"," 
        + "strUnitId: \"" + strUnitId + "\"," 
        + "strUserId: \"" + strUserId + "\"," 
        + "}";
        String response = "";
        String startTime = "";
        String endTime = "";

        String procedureCall = "{call fcfcore.usp_Get_CDD_Rate(?, ?, ?, ?)}";
		Connection connection = null;
        // int returnVal = -1;
        List<UspGetCddRate> list = new ArrayList<>();
		try {
            
            connection = dataSource.getConnection();
            ResultSet rs = null;
            CallableStatement cstmt = connection.prepareCall(procedureCall,
                ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY);
			cstmt.setString(1, SystemType);
            cstmt.setString(2, strID);
            cstmt.setString(3, strUnitId);
            cstmt.setString(4, strUserId);

            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            boolean results = cstmt.execute();
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            
            int rowsAffected = 0;

            // Protects against lack of SET NOCOUNT in stored prodedure
            while (results || rowsAffected != -1) {
                if (results) {
                    rs = cstmt.getResultSet();
                    break;
                } else {
                    rowsAffected = cstmt.getUpdateCount();
                }
                results = cstmt.getMoreResults();
            }
            while (rs.next()) {
                UspGetCddRate o = new UspGetCddRate();
                o.setSystemType(rs.getString("SystemType"));
                o.setPARTY_NUMBER(rs.getString("PARTY_NUMBER"));
                o.setPARTY_NAME(rs.getString("PARTY_NAME"));
                o.setRisk_Classification(rs.getString("Risk_Classification"));
                o.setRisk(rs.getString("Risk"));
                o.setQueryDateTime(rs.getString("QueryDateTime"));
                list.add(o);
                // System.out.println("### " + new Gson().toJson(o));
            }
            
            UspGetCddRate o = null;
            if (list.size() > 0) {
                o = list.get(0);
            }

            if (o != null) {
                response = new Gson().toJson(o);
            }
            // 寫ContractApilog
            contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, keyInfo, qryName, queryJson, response, startTime, endTime);

            return o;
		} catch (SQLException e) {
            e.printStackTrace();
			//logger.error("getSerialNumber", e);
		} finally {
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException e) {
                    e.printStackTrace();
					//logger.error("getSerialNumber", e);
				}
		}
        // return returnVal;
        return null;
    }
}
