package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.db.ContractMain;
import com.kgi.eopend3.ap.dao.CRUDQDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class ContractMainDao extends CRUDQDao<ContractMain> {
    public static final String NAME = "ContractMain";

    @Override
    public int Create(ContractMain fullItem) {
        String sql = "INSERT INTO " + NAME
                + " (UniqId, UserType, CaseNoWeb, GmCardNo, Status, ProductId, Idno, ChtName, Birthday, Email, DealPayDay, FinalPayDay, PayBankOption, SordBankOption, ContractDay, PaymentDay, DocAddr, ReadDate, Notify, Court, DataUse, ApsConstData, ResultHtml, SignTime, SendTime, IpAddress, CreateTime, UpdateTime "
                + " ) "
                + " VALUES (:UniqId, :UserType, :CaseNoWeb, :GmCardNo, :Status, :ProductId, :Idno, :ChtName, :Birthday, :Email, :DealPayDay, :FinalPayDay, :PayBankOption, :SordBankOption, :ContractDay, :PaymentDay, :DocAddr, :ReadDate, :Notify, :Court, :DataUse, :ApsConstData, :ResultHtml, :SignTime, :SendTime, :IpAddress, getdate(), getdate() )";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public ContractMain Read(ContractMain keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE UniqId = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(ContractMain.class),
                    new Object[] { keyItem.getUniqId() });
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料");
            throw new ErrorResultException(9, NAME + "查無資料", ex);
        }
    }

    public ContractMain ReadByUniqId(String uniqId) {
        ContractMain contractMain = new ContractMain(uniqId);
        return this.Read(contractMain);
    }

    @Override
    public int Update(ContractMain fullItem) {
        String sql = "UPDATE " + NAME
                + " SET UserType = :UserType, CaseNoWeb = :CaseNoWeb, GmCardNo = :GmCardNo, Status = :Status, ProductId = :ProductId, Idno = :Idno, ChtName = :ChtName, Birthday = :Birthday, Email = :Email, DealPayDay = :DealPayDay, FinalPayDay = :FinalPayDay, PayBankOption = :PayBankOption, SordBankOption = :SordBankOption, ContractDay = :ContractDay, PaymentDay = :PaymentDay, DocAddr = :DocAddr, ReadDate = :ReadDate, Notify = :Notify, Court = :Court, DataUse = :DataUse, ApsConstData = :ApsConstData, ResultHtml = :ResultHtml, SignTime = :SignTime, SendTime = :SendTime, IpAddress = :IpAddress,  UpdateTime= getdate() "
                + " WHERE UniqId=:UniqId ";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(ContractMain keyItem) {
        String sql = "DELETE FROM " + NAME + " WHERE UniqId = ?";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getUniqId() });
    }

    @Override
    public List<ContractMain> Query(ContractMain keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE Status = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getStatus() },
                    new BeanPropertyRowMapper<>(ContractMain.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }

	public void updateContractPayDay(String uniqId, String dealPayDay, String finalPayDay, String contractDay) {
		this.getJdbcTemplate().update("UPDATE " + NAME + " SET DealPayDay = ?,FinalPayDay = ?, ContractDay = ? WHERE UniqId = ? ",
				new Object[] { dealPayDay, finalPayDay, contractDay , uniqId});
    }
    
    public void updateContractStatus(String uniqId, String caseStatus) {
		this.getJdbcTemplate().update("UPDATE " + NAME + " SET Status = ?, UpdateTime = getdate() WHERE UniqId = ? ",
				new Object[] { caseStatus, uniqId });
    }

    public void updateContractStatusWhenStatusEq(String uniqId, String caseStatus, String whenStatusEq) {
		this.getJdbcTemplate().update("UPDATE " + NAME + " SET Status = ?, UpdateTime = getdate() WHERE UniqId = ? AND Status = ? ",
				new Object[] { caseStatus, uniqId, whenStatusEq });
    }

    public void updateSignTime(String uniqId) {
		this.getJdbcTemplate().update("UPDATE " + NAME + " SET SignTime = getdate() WHERE UniqId = ? and SignTime is null", new Object[] { uniqId });
    }

    public void updateSendTime(String uniqId) {
		this.getJdbcTemplate().update("UPDATE " + NAME + " SET SendTime = getdate() WHERE UniqId = ? ", new Object[] { uniqId });
    }
    
    public List<ContractMain> queryWaitSendContract() throws Exception {
		// 取得待處理契約名單
		try {
			List<ContractMain> ls = this.getJdbcTemplate().query(
					"  SELECT * FROM " + NAME + " where Status in( ? )",
                    new Object[] { PlContractConst.CASE_STATUS_WRITE_SUCCESS },
                    new BeanPropertyRowMapper<>(ContractMain.class));
			return ls;
		} catch (DataAccessException e) {
			return null;
		}

	}
}