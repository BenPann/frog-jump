package com.kgi.airloanex.ap.service;

import static com.kgi.airloanex.common.PlContractConst.LOAN;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.CaseAuthDao;
import com.kgi.airloanex.ap.dao.CaseDataDao;
import com.kgi.airloanex.ap.dao.ContractApiLogDao;
import com.kgi.airloanex.ap.dao.ContractVerifyLogDao;
import com.kgi.airloanex.ap.dao.CreditVerifyDao;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.customDto.IdentityVerify;
import com.kgi.airloanex.common.dto.db.CaseAuth;
import com.kgi.airloanex.common.dto.db.CaseData;
import com.kgi.airloanex.common.dto.db.ContractVerifyLog;
import com.kgi.airloanex.common.dto.db.CreditVerify;
import com.kgi.airloanex.common.dto.response.PCode2566Resp;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.ap.dao.DropdownDao;
import com.kgi.eopend3.ap.dao.HttpDao;
import com.kgi.eopend3.ap.dao.SNDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.customDto.BNS00040007Dto;
import com.kgi.eopend3.common.dto.customDto.NCCCDto;
import com.kgi.eopend3.common.dto.customDto.PCode2566Dto;
import com.kgi.airloanex.common.dto.db.DropdownData;
import com.kgi.eopend3.common.dto.respone.CheckOTPResp;
import com.kgi.eopend3.common.dto.respone.HttpResp;
import com.kgi.eopend3.common.dto.respone.LogFromValidation;
import com.kgi.eopend3.common.dto.respone.NCCCResp;
import com.kgi.eopend3.common.dto.respone.SendOTPResp;
import com.kgi.eopend3.common.dto.view.CheckOTPView;
import com.kgi.eopend3.common.dto.view.NCCCView;
import com.kgi.eopend3.common.dto.view.PCode2566View;
import com.kgi.eopend3.common.dto.view.SendOTPView;
import com.kgi.eopend3.common.util.CheckUtil;
import com.kgi.eopend3.common.util.DateUtil;
import com.kgi.eopend3.common.util.SecureProc;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BankService {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AirloanEXConfig globalConfig;

	@Autowired
	private DropdownDao dropdownDao;

	@Autowired
	private SNDao sndao;

	@Autowired
    private SrnoService snSer;

    @Autowired
    private CaseDataDao caseDataDao;
    
	@Autowired
	private ContractApiLogDao contractApiLogDao;

	@Autowired
	private ContractVerifyLogDao verifyLogDao;
    
    @Autowired
    private HttpDao httpDao;
    
    @Autowired
	private CaseAuthDao caseAuthDao;

	@Autowired
	private CreditVerifyDao creditVerifyDao;

	@Autowired
	private ConfigDao configDao;

	/** 既有戶驗身為 OTP 2 */
	public void passOldOne(String uniqId, String uniqType) {
		CaseAuth caseAuth = new CaseAuth(uniqId);
		caseAuth.setUniqType(uniqType);
		caseAuth.setAuthType("1"); // 1:OTP
		caseAuth.setMbcValid("0");
		caseAuth.setChtAgree("0");
		caseAuthDao.CreateOrUpdate(caseAuth);
		
        CreditVerify creditVerify = new CreditVerify(uniqId);
        creditVerify.setUniqType(uniqType);
        creditVerify.setVerifyStatus(PlContractConst.VERIFY_STATUS_SUCCESS);
        creditVerify.setVerifyType(PlContractConst.CREDITVERIFY_TYPE_9);
        creditVerify.setBankId("");
        creditVerify.setAccountNo("");
        creditVerify.setErrorCount(0);
        creditVerify.setPcode2566ErrorCount(0);
        creditVerifyDao.CreateOrUpdate(creditVerify);
	}

   	/**
	 * 取得 OTP
	 * */
	public SendOTPResp getSessionKey(SendOTPView view, String uniqId, String idno) {
		logger.info("--> getSessionKey()");
//		String response = "";
		String startTime = "";
		String endTime = "";
		String logReq = "", logRes = "" ;

		try {
			view.setIdno(idno);
			view.setBillDep(globalConfig.OTP_BillDep());
			view.setOpId(globalConfig.OTP_OpID());

			// 交易代號 格式(AIRLOAN + 年月日(8) + 流水號(4))
			String date = DateUtil.GetDateFormatString("yyyyMMdd");
			// 從DN取流水號 並將其串成四位字串
			Integer sni = sndao.getSerialNumber("OTP", date);
			String sn = StringUtils.leftPad(sni.toString(), 4, "0");
			String txnId = LOAN + date + sn;
			view.setTxnId(txnId);
			String jsonStr = new Gson().toJson(view);
			logReq = jsonStr ;

			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			HttpResp resp = httpDao.postJson(globalConfig.ValidationServerUrl() + SystemConst.API_SEND_OTP, jsonStr);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			logRes = resp.getResponse() ;
			logger.info("getSessionKey - resp.getResponse()");
			logger.info(resp.getResponse());
			LogFromValidation log = new Gson().fromJson(resp.getResponse(), LogFromValidation.class) ;
			if (log != null) {
				if (!CheckUtil.isEmpty(log.getReq())) {
					logReq = log.getReq() ;
				}
				
				if (!CheckUtil.isEmpty(log.getRes())) {
					logRes = log.getRes() ;
				}
			}
			SendOTPResp sendOpt = new Gson().fromJson(resp.getResponse(), SendOTPResp.class) ;

			// 2020-08-11 demand by Ben
			logReq = jsonStr;
			logRes = resp.getResponse();

			// 寫入apilog記錄
			contractApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "/sendOTP", logReq, logRes, startTime, endTime);

			return sendOpt;
		} catch (Exception e) {
			logger.error("未處理的錯誤", e);
			SendOTPResp sendOpt = new SendOTPResp() ; 
			sendOpt.setCode("F999");
			sendOpt.setMessage("系統錯誤");
			return sendOpt;
		} finally {
			logger.info("<-- getSessionKey()");
		}
	} // end getSessionKey

	/**
	 * 檢核 OTP
	 */
	public CheckOTPResp checkSessionKeyOtp(CheckOTPView view, String uniqId, String idno, String ipAddr) {
		logger.info("--> checkSessionKeyOtp()");
//		String response = "";
		String startTime = "";
		String endTime = "";
		String logReq = "", logRes = "" ;

		try {
			view.setIdno(idno);
			view.setBillDep(globalConfig.OTP_BillDep());
			view.setOpId(globalConfig.OTP_OpID());

			String jsonStr = new Gson().toJson(view);
			logReq = jsonStr ;

			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			HttpResp response = httpDao.postJson(globalConfig.ValidationServerUrl() + SystemConst.API_CHECK_OTP, jsonStr);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			logRes = response.getResponse() ;
			logger.info("checkSessionKeyOtp - response.getResponse()");
			logger.info(response.getResponse());
			LogFromValidation log = new Gson().fromJson(response.getResponse(), LogFromValidation.class) ;
			if (log != null) {
				if (!CheckUtil.isEmpty(log.getReq())) {
					logReq = log.getReq() ;
				}
				
				if (!CheckUtil.isEmpty(log.getRes())) {
					logRes = log.getRes() ;
				}
			}
			CheckOTPResp checkOpt = new Gson().fromJson(response.getResponse(), CheckOTPResp.class) ;

			// 2020-08-11 demand by Ben
			logReq = jsonStr;
			logRes = response.getResponse();

			// 寫入apilog記錄
			contractApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "/checkOTP", logReq, logRes, startTime, endTime);

			CaseData caseData = caseDataDao.Read(new CaseData(uniqId));
			caseData.setIp_address(ipAddr);
			// // 更新立約狀態
			// if (caseData.getStatus().equals(PlContractConst.CASE_STATUS_INIT)) {
			// 	// 填寫中
			// 	caseData.setStatus(PlContractConst.CASE_STATUS_WRITING);
			// } else if(caseData.getStatus().equals(PlContractConst.CASE_STATUS_WRITING)) {
			// 	// 立約完成
			// 	caseData.setStatus(PlContractConst.CASE_STATUS_WRITE_SUCCESS);
			// };

			caseDataDao.Update(caseData);

			boolean isCheckOtpSuccess = "0000".equals(checkOpt.getCode());

            CaseAuth caseAuth = new CaseAuth(uniqId);
            // CreditVerify creditVerify = new CreditVerify(uniqId); // 既有戶無需 insert creditVerify
            caseAuth.setUniqType("03");
            caseAuth.setAuthType("1"); // 1:OTP
            caseAuth.setMbcValid("0");
            caseAuth.setChtAgree("0");
            caseAuthDao.CreateOrUpdate(caseAuth);

			return checkOpt;
		} catch (ErrorResultException e) {
			CheckOTPResp checkOpt = new CheckOTPResp() ;
			checkOpt.setCode(String.valueOf(e.getStatus()));
			checkOpt.setMessage("查無資料");
			return checkOpt;
		} catch (Exception e) {
			CheckOTPResp checkOpt = new CheckOTPResp() ;
			checkOpt.setCode("F099");
			checkOpt.setMessage( "系統錯誤");
			logger.error("系統錯誤", e);

			return checkOpt;
		} finally {
			logger.info("<-- checkSessionKeyOtp()");
		}
	} // end checkSessionKeyOtp

	// /**
	//  * 讀取案件的驗證失敗次數
	//  * 如果案件不存在 ContractMainActInfo ，建立之
	//  * */
	// public String getVerifyCount(String ulid) {
	// 	try {
	// 		ContractMainActInfo verify = contractMainActInfoDao.touch(new ContractMainActInfo(ulid));
	// 		VerifyCountResp resp = new VerifyCountResp();
	// 		resp.setAuthErrCount(verify.getAuthErrCount());
	// 		resp.setSignErrCount(verify.getSignErrCount());
	// 		resp.setEddaErrCount(verify.getEddaErrCount());

	// 		return WebResult.GetResultString(0, "成功", resp);
	// 	} catch (Exception e) {
	// 		logger.error("未處理的錯誤", e);
	// 		return WebResult.GetResultString(99, "系統錯誤", null);
	// 	}
	// } // end getVerifyCount

	public String checkPCode2566(String uniqId, String idno, PCode2566View view) throws Exception {

		caseDataDao.updatePcode2566_phone(uniqId, view.getPhone());

		logger.info("--> checkPCode2566()" );
		ContractVerifyLog verifyLog = new ContractVerifyLog();
		verifyLog.setUniqId(uniqId);
		verifyLog.setVerifyType(SystemConst.VERIFYLOG_TYPE_PCODE);
		verifyLog.setCount(verifyLogDao.Query(verifyLog).size() + 1);
		CreditVerify creditVerify = new CreditVerify(uniqId);
		creditVerify = creditVerifyDao.Read(creditVerify);

		String accountNo = view.getAccount();
		String accountNo16 = String.format("%016d", Long.parseLong(accountNo));
		view.setAccount(accountNo16);
		view.setIdno(idno);

		// PCode2566 IdentityVerify
		IdentityVerify identityVerify = new IdentityVerify();
		identityVerify.setUniqId(uniqId);
		identityVerify.setVerifyType("PCode2566");
		identityVerify.setBankId(view.getBank());
		identityVerify.setAccountNo(view.getAccount());
		
		// Auth/Sign/Edda - (DB to DTO) ContractMainActInfo -> IdentityVerify
		identityVerify.setErrorCount((creditVerify != null && creditVerify.getErrorCount() != null) ? creditVerify.getErrorCount() : 0);

        // 主要驗證邏輯 - 將 PCode2566 驗證結果回填至 identityVerify
        if (view.getBank().equals(SystemConst.KGI_BANK_ID)) {
            kgiPCode2566(uniqId, view, identityVerify, verifyLog);
        } else {
            otherPCode2566(uniqId, view, identityVerify, verifyLog);
        }

		// Auth/Sign/Edda - (DTO to DB)IdentityVerify -> ContractMainActInfo
		PCode2566Resp resp = new PCode2566Resp();

        resp.setPcode2566CheckCode(identityVerify.getPcode2566CheckCode());
        resp.setPcode2566Message(identityVerify.getPcode2566Message());
        resp.setAuthErrCount(identityVerify.getErrorCount());

        CaseAuth caseAuth = new CaseAuth(uniqId);
        caseAuth.setUniqType("03");
        caseAuth.setAuthType("4"); // 4:其它
        caseAuth.setMbcValid("0");
        caseAuth.setChtAgree("0");
		caseAuthDao.CreateOrUpdate(caseAuth);
		
        creditVerify = new CreditVerify(uniqId);
        creditVerify.setUniqType("03");
        creditVerify.setVerifyStatus(PlContractConst.VERIFY_STATUS_SUCCESS);
        creditVerify.setVerifyType(PlContractConst.CREDITVERIFY_TYPE_BANKACOUNT);
        creditVerify.setBankId(identityVerify.getBankId());
        creditVerify.setAccountNo(identityVerify.getAccountNo());
        creditVerify.setErrorCount(identityVerify.getErrorCount());
        creditVerify.setPcode2566ErrorCount(identityVerify.getErrorCount());
        creditVerifyDao.CreateOrUpdate(creditVerify);

		return WebResult.GetResultString(0, "成功", resp);
	} // end checkPCode2566

	/**
	 * 檢核他行 Pcode2566
	 * */
	private void otherPCode2566(String uniqId, PCode2566View view, IdentityVerify identityVerify, ContractVerifyLog verifyLog) {
		logger.info("--> otherPCode2566() uniqId=[" + uniqId + "]" );
		String startTime = "";
		String endTime = "";
		String reqJson = "";
//		String response = "";
		String noteCode = "";
		String statusCode = "";
		DropdownData dropDown = new DropdownData();
		String logRes = "", logReq = "" ;

		try {
			view.setTmnlId(globalConfig.PCODE2566_TMNL_ID());
			view.setTmnlType(globalConfig.PCODE2566_TMNL_TYPE());


			CaseData CaseData = caseDataDao.Read(new CaseData(uniqId));
			view.setBirthday(CaseData.getBirthday());
			view.setPcodeUid(snSer.getpCode2566UniqId());

			// ESB Setting
			view.setESBChannel(globalConfig.ESB_Channel());
			view.setESBClientId(globalConfig.ESB_ClientId());
			view.setESBQueueReceiveName(globalConfig.ESB_QueueReceiveName());
			String esbClientPAZZD = globalConfig.ESB_ClientPAZZD();
			String timeNow = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
			esbClientPAZZD = DigestUtils.md5Hex(esbClientPAZZD + timeNow);
			view.setESBTimestamp(timeNow);
			view.setESBClientPAZZD(esbClientPAZZD);

			reqJson = new Gson().toJson(view);
			logReq = reqJson ;

			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			HttpResp response = httpDao.postJson(globalConfig.ValidationServerUrl() + SystemConst.API_OTHER_PCDOE2566, reqJson);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			logger.info(SystemConst.API_OTHER_PCDOE2566 + " response: [" + response + "]");
			LogFromValidation log = new Gson().fromJson(response.getResponse(), LogFromValidation.class) ;
			logRes = response.getResponse() ;
			if (log != null) {
				if (!CheckUtil.isEmpty(log.getReq())) {
					logReq = log.getReq() ;
				}
				
				if (!CheckUtil.isEmpty(log.getRes())) {
					logRes = log.getRes() ;
				}
			}

			PCode2566Dto pcode2566Resp = new Gson().fromJson(response.getResponse(), PCode2566Dto.class);

			// 2020-08-11 demand by Ben
			logReq = reqJson;
			logRes = response.getResponse();

			if (pcode2566Resp.getResult() == 0) {
				// 檢驗結果
				String verifyCodeByVerifyType = pcode2566Resp.getVerifyCodeByVerifyType();
				// 帳號檢驗結果
				String verifyCodeByAccountState = pcode2566Resp.getVerifyCodeByAccountState();
				// 開戶狀態
				String verifyCodeByOpenAccountState = pcode2566Resp.getVerifyCodeByOpenAccountState();
				noteCode = verifyCodeByVerifyType + verifyCodeByAccountState + verifyCodeByOpenAccountState;

				if (verifyCodeByVerifyType.equals("00")) {
					if (verifyCodeByAccountState.equals("00")) {
						// 2019/01/14 新增加檢核第5 6碼
						if (!verifyCodeByOpenAccountState.equals("01")) {
							setErrorRespAndErrorCountPlus("PCode2566NoteCodeOpenAccountState",
									verifyCodeByOpenAccountState, "資料錯誤，請與開戶行聯繫", identityVerify);
						} else {
							identityVerify.setPcode2566CheckCode("0");
							identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_SUCCESS);
						}
					} else { // verifyCodeByAccountState != "00"
						setErrorRespAndErrorCountPlus("PCode2566NoteCodeAccountState", verifyCodeByAccountState,
								"資料錯誤，請與開戶行聯繫", identityVerify);
					}
				} else { // verifyCodeByVerifyType != "00"
					setErrorRespAndErrorCountPlus("PCode2566NoteCodeVerifyType", verifyCodeByVerifyType,
							"資料錯誤，請與開戶行聯繫", identityVerify);
				}
			} else if (pcode2566Resp.getResult() < 0) { // 小於0的錯誤號碼代表系統錯誤，不需增加錯誤次數
				if (pcode2566Resp.getResult() == -2538) {
					dropDown.setName("PCode2566ConnectFailCode");
					dropDown.setDataKey(pcode2566Resp.getStatusCode());
					dropDown = dropdownDao.Read(dropDown);
					identityVerify.setPcode2566CheckCode(String.valueOf(pcode2566Resp.getStatusCode()));
					if (dropDown != null) {
						identityVerify.setPcode2566Message(dropDown.getDataName());
					}
				} else {
					identityVerify.setPcode2566Message("系統錯誤" + pcode2566Resp.getMessage());
				}
				identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_FAIL);
				identityVerify.setErrorMsg(identityVerify.getPcode2566Message());

			} else {// 小於0的錯誤號碼代表商務邏輯錯誤，需增加錯誤次數
				identityVerify.setPcode2566CheckCode(pcode2566Resp.getStatusCode());
				identityVerify.setPcode2566Message("您本次填寫的驗證資料不正確");
				setErrorRespAndErrorCountPlus("PCode2566StatusCode", statusCode, "資料錯誤，請與開戶行聯繫", identityVerify);
				identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_FAIL);
				identityVerify.setErrorMsg(identityVerify.getPcode2566Message());
			}
		} catch (Exception e) {
			identityVerify.setPcode2566CheckCode("-999");
			identityVerify.setPcode2566Message("您本次填寫的驗證資料不正確");
			
			
			identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_FAIL);
			identityVerify.setErrorMsg(identityVerify.getPcode2566Message());
		} finally {
			contractApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "/pCode2566/other", logReq, logRes, startTime, endTime);

			verifyLog.setRequest(logReq);
			verifyLog.setResponse(logRes);
			// 晚點再補這兩個欄位
			verifyLog.setCheckCode(statusCode);
			verifyLog.setAuthCode(noteCode);
			verifyLogDao.Create(verifyLog);
		}
	} // end otherPCode2566

	/**
	 * 檢核凱基 Pcode2566
	 * */
	private void kgiPCode2566(String uniqId, PCode2566View view, IdentityVerify identityVerify,
			ContractVerifyLog verifyLog) throws Exception {
		logger.info("--> kgiPCode2566() uniqId=[" + uniqId + "]" );
		String startTime = "";
		String endTime = "";
		String reqJson = "";
//		String res = "";
		
		String logReq = "", logRes = "" ;
		try {
			// ESB Setting
			view.setESBChannel(globalConfig.ESB_Channel());
			view.setESBClientId(globalConfig.ESB_ClientId());
			view.setESBQueueReceiveName(globalConfig.ESB_QueueReceiveName());
			String esbClientPAZZD = globalConfig.ESB_ClientPAZZD() ;
			String timeNow = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
			esbClientPAZZD = DigestUtils.md5Hex(esbClientPAZZD + timeNow);
			view.setESBTimestamp(timeNow);
			view.setESBClientPAZZD(esbClientPAZZD);


			reqJson = new Gson().toJson(view);
			logReq = reqJson ;

			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			HttpResp res = httpDao.postJson(globalConfig.ValidationServerUrl() + SystemConst.API_KGI_PCODE2566, reqJson);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
//			logger.info(res);
			logRes = res.getResponse() ;
			LogFromValidation log = new Gson().fromJson(res.getResponse(), LogFromValidation.class) ;
			if (log != null) {
				if (!CheckUtil.isEmpty(log.getReq())) {
					logReq = log.getReq() ;
				}
				
				if (!CheckUtil.isEmpty(log.getRes())) {
					logRes = log.getRes() ;
				}
			}
			BNS00040007Dto bns00040007 = new Gson().fromJson(res.getResponse(), BNS00040007Dto.class);

			// 2020-08-11 demand by Ben
			logReq = reqJson;
			logRes = res.getResponse();

			// 舊戶正常帳戶:
			// 存摺掛失<PBStatus1> => 1: 正常 空值是無摺
			// 印鑑掛失<ChopStatus1> => 1:正常
			// Status1: 正常
			// NOOFHOLDS1: 00000
			// IDNo
			if (bns00040007.getResult() == 0) {
				String status = bns00040007.getStatus();
				String pbStatus = bns00040007.getPbStatus();
				String chopStatus = bns00040007.getChopStatus();
				String noOfHold = bns00040007.getNoOfHold();
				String bnsIdno = bns00040007.getBnsIdno();
				if ("正常".equals(status) 
					&& ("1".equals(pbStatus) || "".equals(pbStatus)) 
					&& "1".equals(chopStatus)
					&& "00000".equals(noOfHold) 
					&& view.getIdno().equals(bnsIdno)) {
					identityVerify.setPcode2566CheckCode("0");
					identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_SUCCESS);
				}
			}

			// 如果是業務面的失敗，驗身錯誤次數要 +1
			if (bns00040007.getResult() > 0 || !"0".equals(identityVerify.getPcode2566CheckCode())) {
				identityVerify.setPcode2566CheckCode("1");
				identityVerify.setPcode2566Message(bns00040007.getMessage());
				identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_FAIL);
				identityVerify.setErrorCount(identityVerify.getErrorCount() + 1);
				identityVerify.setErrorMsg(identityVerify.getPcode2566Message());
			}

		} catch (Exception e) {
			logger.error("未知錯誤", e);
			throw e;
		} finally {
			contractApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "/pCode2566/KGI", logReq, logRes, startTime, endTime);
			verifyLog.setRequest(logReq);
			verifyLog.setResponse(logRes);
			verifyLog.setCheckCode("");
			verifyLog.setAuthCode("");
			verifyLogDao.Create(verifyLog);
		}
	} // end kgiPCode2566

	private void setErrorRespAndErrorCountPlus(String name, String dataKey, String defaultMsg, IdentityVerify identityVerify) {
		DropdownData dropDown = new DropdownData();
		dropDown.setName(name);
		dropDown.setDataKey(dataKey);
		dropDown = dropdownDao.Read(dropDown);
		identityVerify.setPcode2566CheckCode(dataKey);
		if (dropDown != null) {
			identityVerify.setPcode2566Message(dropDown.getDataName());
		} else {
			identityVerify.setPcode2566Message(defaultMsg);
		}
		identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_FAIL);
		identityVerify.setErrorCount(identityVerify.getErrorCount() + 1);
	} // end setErrorRespAndErrorCountPlus

    /**
     * 是否為凱基銀行 809
     * @param bank_no
     * @return
     */
    private boolean isKgibank(String bank_no) {
        String bank_id = StringUtils.substring(bank_no, 0, 3); // 3碼銀行別
        return StringUtils.equals("809", bank_id);
	}
	
	public String verifyNCCC(String uniqId, String idno, NCCCView ncccView) {
		logger.info("--> verifyNCCC() uniqId=[" + uniqId + "] idno=[" + idno + "] view=[" + ncccView.toJsonString() + "]" );

		ContractVerifyLog vLog = new ContractVerifyLog();
		vLog.setUniqId(uniqId);
		vLog.setVerifyType(SystemConst.VERIFYLOG_TYPE_NCCC);
		vLog.setCount(verifyLogDao.Query(vLog).size() + 1);
		CreditVerify creditVerify = null;
		creditVerify = creditVerifyDao.Read(new CreditVerify(uniqId)); // TODO: 代替 ED3_IdentityVerification

		// PCode2566 IdentityVerify
		IdentityVerify identityVerify = new IdentityVerify();
		identityVerify.setUniqId(uniqId);
		identityVerify.setVerifyType(SystemConst.VERIFY_TYPE_NCCC);
		identityVerify.setErrorCount((creditVerify != null && creditVerify.getErrorCount() != null) ? creditVerify.getErrorCount() : 0);
		identityVerify.setNcccErrorCount((creditVerify != null && creditVerify.getNCCCErrorCount() != null) ? creditVerify.getNCCCErrorCount() : 0);
		identityVerify.setPcode2566ErrorCount((creditVerify != null && creditVerify.getPcode2566ErrorCount() != null) ? creditVerify.getPcode2566ErrorCount() : 0);
		
		boolean isKGICard = false;
		String kgiCardNumList = configDao.ReadConfigValue("KGICardNumVerify");
		try {
			if (kgiCardNumList != null && !kgiCardNumList.equals("")
				&& kgiCardNumList.indexOf(ncccView.getPan().substring(0, 5)) > -1) {
				isKGICard = true;
			}
		} catch (IndexOutOfBoundsException e) {
			logger.error("Compare kgiCardNumList Fail", e);
			logger.info(ncccView.getPan());
		}

		String wr = null;

		if (isKGICard) {
			wr = kgiNCCC(uniqId, idno, ncccView, identityVerify, vLog);
		} else {
			wr = otherNCCC(uniqId, idno, ncccView, identityVerify, vLog);
		}

		if (creditVerify == null) {
			creditVerify = new CreditVerify(uniqId);
		} 
		creditVerify.setVerifyStatus(identityVerify.getVerifyStatus());
		// creditVerify.setErrorMsg(identityVerify.getErrorMsg());
		creditVerify.setCardNo(identityVerify.getCardNo());
		creditVerify.setCardTime(identityVerify.getCardTime());
		creditVerify.setErrorCount(identityVerify.getErrorCount());
		creditVerify.setNCCCErrorCount(identityVerify.getNcccErrorCount());
		creditVerify.setPcode2566ErrorCount(identityVerify.getPcode2566ErrorCount());
		creditVerifyDao.CreateOrUpdate(creditVerify);

		return wr;
	}
	
	/**
	 * 檢核凱基信用卡
	 * */
	private String kgiNCCC(String uniqId, String idno, NCCCView view, IdentityVerify identityVerify, ContractVerifyLog vLog) {
		logger.info("--> kgiNCCC() uniqId=[" + uniqId + "]" );
		String startTime = "";
		String endTime = "";
		String reqJson = "";
//		String res = "";
		String statusCode = "";
		String logReq = "", logRes = "" ;
		
		NCCCResp resp = new NCCCResp();
		DropdownData dropDown = new DropdownData();
		try {
			CaseData caseData = caseDataDao.Read(new CaseData(uniqId));
			view.setBirthday(caseData.getBirthday());
			view.setIdno(idno);

			// ESB Setting
			view.setESBChannel(globalConfig.ESB_Channel());
			view.setESBClientId(globalConfig.ESB_ClientId());
			view.setESBQueueReceiveName(globalConfig.ESB_QueueReceiveName());
			String esbClientPAZZD = globalConfig.ESB_ClientPAZZD();
			String timeNow = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
			esbClientPAZZD = DigestUtils.md5Hex(esbClientPAZZD + timeNow);
			view.setESBTimestamp(timeNow);
			view.setESBClientPAZZD(esbClientPAZZD);

			reqJson = new Gson().toJson(view);
			logger.info(reqJson);
			logReq = reqJson;

			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			HttpResp res = httpDao.postJson(globalConfig.ValidationServerUrl() + SystemConst.API_KGI_NCCC, reqJson);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
//			logger.info(res);
			logRes = res.getResponse() ;
			LogFromValidation log = new Gson().fromJson(res.getResponse(), LogFromValidation.class) ;
			if (log != null) {
				if (!CheckUtil.isEmpty(log.getReq())) {
					logReq = log.getReq() ;
				}
				
				if (!CheckUtil.isEmpty(log.getRes())) {
					logRes = log.getRes() ;
				}
			}
			NCCCDto ncccDto = new Gson().fromJson(res.getResponse(), NCCCDto.class);
			statusCode = ncccDto.getStatusCode();

			if (ncccDto.getResult() == 0) {
				resp.setNcccCheckCode("0");
				identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_SUCCESS);
			} else if (ncccDto.getResult() > 0) {
				dropDown.setName("NCCCKGIErrorCode");
				dropDown.setDataKey(statusCode);
				dropDown = dropdownDao.Read(dropDown);
				if (dropDown != null) {
					resp.setNcccMessage(dropDown.getDataName());
				} else {
					resp.setNcccMessage("卡片資料錯誤，請與發卡行聯繫");
				}
				resp.setNcccCheckCode(statusCode);
				identityVerify.setErrorCount(identityVerify.getErrorCount() + 1);
				identityVerify.setNcccErrorCount(identityVerify.getNcccErrorCount() + 1);
				identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_FAIL);
				identityVerify.setErrorMsg(resp.getNcccMessage());
			} else {
				resp.setNcccCheckCode(String.valueOf(ncccDto.getResult()));
				resp.setNcccMessage(ncccDto.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setNcccCheckCode("-9999");
			resp.setNcccMessage(e.getMessage());
		} finally {
			// 寫ContractApiLog
			contractApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "/NCCC/kgi", logReq, logRes, startTime, endTime);
			//
			vLog.setRequest(logReq);
			vLog.setResponse(logRes);
			vLog.setCheckCode(statusCode);
			vLog.setAuthCode("");
			verifyLogDao.Create(vLog);

			logger.info("<-- kgiNCCC()" );
		}

		identityVerify.setCardNo(SecureProc.hiddenUserData(view.getPan(), 4, 8));
		identityVerify.setCardTime(view.getExpDate());
		identityVerify.setMyCard(SystemConst.VERIFY_CARD_KGI);

		resp.setErrorCount(identityVerify.getErrorCount());
		resp.setNcccErrorCount(identityVerify.getNcccErrorCount());
		resp.setPcode2566ErrorCount(identityVerify.getPcode2566ErrorCount());

		return WebResult.GetResultString(0, "成功", resp);
	}

	private String otherNCCC(String uniqId, String idno, NCCCView ncccView, IdentityVerify identityVerify, ContractVerifyLog verifyLog) {
		logger.info("--> otherNCCC() uniqId=[" + uniqId + "] idno=[" + idno + "]" );
		String startTime = "";
		String endTime = "";
		String reqJson = "";
//		String res = "";
		String logReq = "", logRes = "" ;

		try {
			// 設定交易資料
			ncccView.setIdno(idno);
			ncccView.setNcccMerchantId(globalConfig.NCCC_MerchantID());
			ncccView.setNcccTermId(globalConfig.NCCC_TID());
			ncccView.setNcccTransMode(globalConfig.NCCC_TransMode());
			ncccView.setTransAmt(globalConfig.NCCC_TransAmount());

			reqJson = new Gson().toJson(ncccView);
			logger.info(reqJson);
			logReq = reqJson ;

			// 執行交易(Internet用戶)
			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			HttpResp res = httpDao.postJson(globalConfig.ValidationServerUrl() + SystemConst.API_OTHER_NCCC, reqJson);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			logRes = res.getResponse() ;
			LogFromValidation log = new Gson().fromJson(res.getResponse(), LogFromValidation.class) ;
			if (log != null) {
				if (!CheckUtil.isEmpty(log.getReq())) {
					logReq = log.getReq() ;
				}
				
				if (!CheckUtil.isEmpty(log.getRes())) {
					logRes = log.getRes() ;
				}
			}
			NCCCResp resp = new Gson().fromJson(res.getResponse(), NCCCResp.class);

			verifyLog.setCheckCode(resp.getStatusCode());
			if (resp.getResult() == 0) {
				verifyLog.setAuthCode(resp.getApproveCode());
				resp.setNcccCheckCode("0");
				identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_SUCCESS);
				identityVerify.setCardNo(SecureProc.hiddenUserData(ncccView.getPan(), 4, 8));
				identityVerify.setCardTime(ncccView.getExpDate());
			} else {
				DropdownData dropDown = new DropdownData();
				dropDown.setName("NCCCErrorCode");
				dropDown.setDataKey(resp.getStatusCode());
				dropDown = dropdownDao.Read(dropDown);
				if (dropDown != null) {
					resp.setNcccMessage(dropDown.getDataName());
				} else {
					resp.setNcccMessage(resp.getMessage());
				}

				resp.setNcccCheckCode(resp.getStatusCode());
				if (resp.getResult() > 0) {
					identityVerify.setErrorCount(identityVerify.getErrorCount() + 1);
					identityVerify.setNcccErrorCount(identityVerify.getNcccErrorCount() + 1);
				}
				identityVerify.setVerifyStatus(SystemConst.VERIFY_STATUS_FAIL);
				identityVerify.setCardNo(SecureProc.hiddenUserData(ncccView.getPan(), 4, 8));
				identityVerify.setCardTime(ncccView.getExpDate());
				identityVerify.setErrorMsg(resp.getNcccMessage());
			}
			identityVerify.setMyCard(SystemConst.VERIFY_CARD_OTHER);

			resp.setErrorCount(identityVerify.getErrorCount());
			resp.setNcccErrorCount(identityVerify.getNcccErrorCount());
			resp.setPcode2566ErrorCount(identityVerify.getPcode2566ErrorCount());
			return WebResult.GetResultString(0, "成功", resp);
		} catch (ErrorResultException e) {
			return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
		} catch (Exception e) {
			logger.error("未知錯誤", e);
			return WebResult.GetResultString(99, "卡片資料錯誤，請與發卡行聯繫", null);
		} finally {
			// 寫ContractApiLog
			contractApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "/NCCC/other", logReq, logRes, startTime, endTime);
			//
			verifyLog.setRequest(logReq);
			verifyLog.setResponse(logRes);
			verifyLogDao.Create(verifyLog);
			
			logger.info("<-- otherNCCC()" );
		}
	}
}
