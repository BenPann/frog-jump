package com.kgi.airloanex.ap.dao;

import com.kgi.airloanex.common.dto.db.WhiteList;
import com.kgi.airloanex.common.dto.db.WhiteListData;
import com.kgi.eopend3.ap.dao.CRUDQDao;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class WhiteListDao extends CRUDQDao<WhiteList> {
    public static final String NAME = "WhiteList";

    @Override
    public int Create(WhiteList fullItem) {
        String sql = "INSERT INTO " + NAME
                + " (ID, PType, SType) "
                + " VALUES (:ID, :PType, :SType)";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public WhiteList Read(WhiteList keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE ID = ? AND PType = ? ";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(WhiteList.class),
                    new Object[] { keyItem.getID(), keyItem.getPType() });
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料");
            return null;
        }
    }

    @Override
    public int Update(WhiteList fullItem) {
        String sql = "UPDATE " + NAME
                + " SET  SType = :SType "
                + " WHERE ID=:ID AND PType=:PType";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(WhiteList keyItem) {
        String sql = "DELETE FROM " + NAME + " WHERE ID = ? AND PType = ?";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getID(), keyItem.getPType() });
    }

    @Override
    public List<WhiteList> Query(WhiteList keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE ID = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getID() },
                    new BeanPropertyRowMapper<>(WhiteList.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }

    public List<WhiteListData> findWhiteListDetail(String ID, String PType, String ApiName) {
        List<WhiteListData> list = new ArrayList<>();
        String sql = "select * from WhiteList w join WhiteListData d on w.ID = d.ID and w.PType = d.PType where w.ID = :ID and w.PType = :PType and ApiName = :ApiName;";
        try {
            list = this.getJdbcTemplate().query(sql, new Object[] { ID, PType, ApiName },
            new BeanPropertyRowMapper<>(WhiteListData.class));
        } catch (DataAccessException ex) {
            // ignore
        }
        return list;
    }

    public WhiteList findByIdAndPtype(String idno, String ptype) {
        String sql = "SELECT * FROM " + NAME + " WHERE ID = ? AND PType = ? ";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(WhiteList.class),
                    new Object[] { idno, ptype });
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料");
            return null;
        }
    }

    public boolean isBypassLoan(String idno) {
        WhiteList whiteList = findByIdAndPtype(idno, "1"); // 1 貸款
        return (whiteList != null);
    }
}
