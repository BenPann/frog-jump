package com.kgi.eopend3.ap.service;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.kgi.airloanex.ap.dao.QR_ChannelDepartListDao;
import com.kgi.airloanex.common.dto.db.QR_ChannelDepartList;
import com.kgi.eopend3.ap.dao.DropdownDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.airloanex.common.dto.db.DropdownData;
import com.kgi.eopend3.common.dto.respone.DropDownResp;
import com.kgi.eopend3.common.dto.view.DropDownView;
import com.kgi.eopend3.common.util.CheckUtil;
import com.kgi.eopend3.common.dto.WebResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class DropDownService {

    @Autowired
    private DropdownDao dropdownDao;

    @Autowired
    private QR_ChannelDepartListDao qr_ChannelDepartListDao;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /** 貸款PL資金用途清單 */
    public String getMoneypurpose() {
        return findDropDownData("moneypurpose");
    }

    /** 貸款學歷清單 */
    public String getEducation() {
        return findDropDownData("education");
    }

    /** 職業類別清單 */
    public String getOccupation() {
        return findDropDownData("occupation");
    }

    /** 職務名稱 */
    public String getOccupationtitle() {
        return findDropDownData("occupationtitle");
    }

    /** 帳單寄送方式清單 */
    public String getBilltype() {
        return findDropDownData("billtype");
    }

    /** 婚姻狀況 */
    public String getMarriage() {
        return findDropDownData("marriage");
    }

    /** 不動產狀況 */
    public String getEstate() {
        return findDropDownData("estate");
    }

    /** 身分證換補發縣市 */
    public String getIdcardlocation() {
        return findDropDownData("idcardlocation");
    }

    /** pcode2566 撥款銀行列表 */
    public String getPcode2566BankList() {
        return findDropDownData("pCode2566Bank");
    }

    /** edda 授扣銀行列表 */
    public String getEddaBankList() {
        return findDropDownData("EDDABank");
    }

    /** 二親等以內血親：11配偶, 12子女, 13父親, 14母親 */
    public String getRelationDegree() {
        return findDropDownData("RelationDegree");
    }

    /** 企業負責人：21本人擔任負責人之企業資料, 22配偶擔任負責人之企業資料 */
    public String getRelationDegreeCorp() {
        return findDropDownData("RelationDegreeCorp");
    }

    public String getBillType() {
        return findDropDownData("BillType");
    }

    public String findDataKey(String name, String dataName) {
        DropdownData d = dropdownDao.readByNameAndDataName(name, dataName.trim());
        return d.getDataKey();
    }

    public String findDataName(String name, String dataKey) {
        DropdownData d = dropdownDao.readByNameAndDataKey(name, dataKey.trim());
        return d.getDataName();
    }

    public String findDataKey_idlocation_new(String dataName) {
        return findDataKey("idlocation_new", dataName.trim());
    }

    public String findDataName_idlocation_new(String dataKey) {
        return findDataName("idlocation_new", dataKey.trim());
    }

    public String findDataName_occupation(String dataKey) {
        return findDataName("occupation", dataKey.trim());
    }

    public String getQR_ChannelDepartType() {
        return find_QR_ChannelDepartData();
    }

    public String getDropDownData(String reqJson) {
        Gson gson = new Gson();
        DropDownView dropDownView = gson.fromJson(reqJson, DropDownView.class);
        return getDropDownData(dropDownView);
    }

    private String findDropDownData(String name) {
        try {
            List<DropDownResp> respList = new ArrayList<DropDownResp>();
            DropdownData dropDown = new DropdownData();
            dropDown.setName(name);
            List<DropdownData> list = dropdownDao.Query(dropDown);
            if (list != null && list.size() > 0) {
                for (DropdownData result : list) {
                    DropDownResp resp = new DropDownResp();
                    resp.setDataKey(result.getDataKey());
                    resp.setDataName(result.getDataName());
                    respList.add(resp);
                }
                return WebResult.GetResultString(0, "成功", respList);
            } else {
                throw new ErrorResultException(2, "查無資料", null);
            }
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
    }

    public String getDropDownData(DropDownView dropDownView) {
        if (!CheckUtil.check(dropDownView)) {
            throw new ErrorResultException(1, "參數錯誤", null);
        } else {
            return findDropDownData(dropDownView.getName());
        }
    }

    public String findPcode2566Bank(String bandId) {
        try {
            return dropdownDao.getDataNameByNameAndDataKey("pCode2566Bank", bandId);
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
    }

    public String findEDDABank(String bandId) {
        try {
            return dropdownDao.getDataNameByNameAndDataKey("EDDABank", bandId);
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
    }

    private String find_QR_ChannelDepartData() {
        try {
            List<DropDownResp> respList = new ArrayList<DropDownResp>();
            QR_ChannelDepartList QRList = new QR_ChannelDepartList();
            List<QR_ChannelDepartList> list = qr_ChannelDepartListDao.findByKB();
            if (list != null && list.size() > 0) {
                for (QR_ChannelDepartList result : list) {
                    DropDownResp resp = new DropDownResp();
                    resp.setDataKey(result.getDepartId());
                    resp.setDataName(result.getDepartName());
                    respList.add(resp);
                }
                return WebResult.GetResultString(0, "成功", respList);
            } else {
                throw new ErrorResultException(2, "查無資料", null);
            }
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
    }
}
