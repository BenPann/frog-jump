package com.kgi.airloanex.ap.dao;

// import java.io.UnsupportedEncodingException;
// import java.sql.PreparedStatement;
// import java.sql.SQLException;
// import java.text.ParseException;
// import java.text.SimpleDateFormat;
// import java.util.Arrays;
// import java.util.Base64;
// import java.util.Date;
// import java.util.HashMap;
// import java.util.Map;
// import java.util.stream.Collectors;
// import javax.sql.DataSource;
// import org.springframework.jdbc.core.BatchPreparedStatementSetter;
// import org.springframework.jdbc.core.JdbcTemplate;
// import org.springframework.transaction.TransactionStatus;
// import net.sf.json.JSONArray;
import static com.kgi.airloanex.common.PlContractConst.*;

import java.util.ArrayList;
import java.util.List;

import com.kgi.airloanex.common.dto.db.UserPhoto;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class UserPhotoDao extends CRUDQDao<UserPhoto> {

    @Override
    public int Create(UserPhoto fullItem) {
        String sql = "INSERT INTO UserPhoto Values(?,?,(SELECT ISNULL(Max(SubSerial) + 1,1) from UserPhoto where UniqId = ? AND Online = ?),?,?,?,?,?,?,?,?,GETDATE(),GETDATE());";
        return this.getJdbcTemplate().update(sql,
                new Object[]{fullItem.getUniqId(), fullItem.getOnline(), fullItem.getUniqId(), fullItem.getOnline(),
                        fullItem.getStatus(), fullItem.getProdType(), fullItem.getPType(),
                        fullItem.getSType(), fullItem.getProductId(), fullItem.getUnitId(), fullItem.getImageBig(), fullItem.getImageSmall()});
    }

    /*public int[] batchCreate(List<UserPhoto> items) {
        String sql = "INSERT INTO UserPhoto Values(?,?,(SELECT ISNULL(Max(SubSerial) + 1,1) from UserPhoto where UniqId = ? AND Online = ?),?,?,?,?,?,?,?,?,?,GETDATE());";
        return this.getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, items.get(i).getUniqId());
                ps.setString(2, items.get(i).getOnline());
                ps.setString(3, items.get(i).getUniqId());
                ps.setString(4, items.get(i).getOnline());
                ps.setString(5, items.get(i).getStatus());
                ps.setString(6, items.get(i).getProdType());
                ps.setInt(7, items.get(i).getPType());
                ps.setInt(8, items.get(i).getSType());
                ps.setString(9, items.get(i).getProductId());
                ps.setString(10, items.get(i).getUnitId());
                ps.setBytes(11, items.get(i).getImageBig());
                ps.setBytes(12, items.get(i).getImageSmall());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
                Date parse = null;
                try {
                    parse = simpleDateFormat.parse(items.get(i).getCreatTime());
                } catch (ParseException e) {
//                    e.printStackTrace();
                    parse = new Date();
                }
                ps.setTimestamp(13, new java.sql.Timestamp(parse.getTime()));
            }

            @Override
            public int getBatchSize() {
                return items.size();
            }
        });
    }*/

    @Override
    public UserPhoto Read(UserPhoto keyItem) {
        String sql = "SELECT * FROM UserPhoto WHERE UniqId = ? AND Online = ? AND SubSerial = ?;";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(UserPhoto.class),
                    new Object[]{keyItem.getUniqId(), keyItem.getOnline(), keyItem.getSubSerial()});
        } catch (DataAccessException ex) {
            System.out.println("UserPhoto查無資料");
            return null;
        }
    }

    @Override
    public int Update(UserPhoto fullItem) {
        String sql = "UPDATE UserPhoto SET Status = ? ,ProdType = ?, PType = ?, SType = ?, ProductId = ?, UnitId = ? , UpdateTime = GETDATE() WHERE UniqId = ? AND Online = ? AND SubSerial = ?;";
        return this.getJdbcTemplate().update(sql, new Object[]{fullItem.getStatus(), fullItem.getProdType(),
                fullItem.getPType(), fullItem.getSType(), fullItem.getProductId(), fullItem.getUnitId(),
                fullItem.getUniqId(), fullItem.getOnline(), fullItem.getSubSerial()});
    }

    @Override
    public int Delete(UserPhoto keyItem) {
//        String mainTable = "UserPhoto";
        String sql = "";
        if (keyItem.getProdType().equals(USERPHOTO_ProdType_3)) {
//            mainTable = "EOP_UserPhoto";
            sql = "DELETE EOP_UserPhoto WHERE UniqId = ? AND SubSerial = ?; ";
            return this.getJdbcTemplate().update(sql,
                    new Object[]{keyItem.getUniqId(), keyItem.getSubSerial()});
        } else if (keyItem.getProdType().equals(USERPHOTO_ProdType_4)) {
            sql = "DELETE ED3_UserPhoto WHERE UniqId = ? AND SubSerial = ?; ";
            return this.getJdbcTemplate().update(sql,
                    new Object[]{keyItem.getUniqId(), keyItem.getSubSerial()});
        } else {
            sql = "DELETE UserPhoto WHERE UniqId = ? AND Online = ? AND SubSerial = ?; ";

            return this.getJdbcTemplate().update(sql,
                    new Object[]{keyItem.getUniqId(), keyItem.getOnline(), keyItem.getSubSerial()});
        }
    }

    @Override
    public List<UserPhoto> Query(UserPhoto keyItem) {
        List<String> params = new ArrayList<>();
        String sql = "SELECT * FROM UserPhoto WHERE UniqId = ? AND Online = ? AND Status = ?";
        params.add(keyItem.getUniqId());
        params.add(keyItem.getOnline());
        params.add(USERPHOTO_STATUS_WAIT_UPLOAD_4);
        if (StringUtils.isNotBlank(keyItem.getProductId())) {
            sql = sql.concat(" AND ProductId=?");
            params.add(keyItem.getProductId());
        }
        if (StringUtils.isNotBlank(keyItem.getProdType())) {
            sql = sql.concat(" AND ProdType=?");
            params.add(keyItem.getProdType());
        }
        if (StringUtils.isNotBlank(keyItem.getUnitId())) {
            sql = sql.concat(" AND UnitId=?");
            params.add(keyItem.getUnitId());
        }

        try {
            return this.getJdbcTemplate().query(sql, params.toArray(),
                    new BeanPropertyRowMapper<>(UserPhoto.class));
        } catch (DataAccessException ex) {
           ex.printStackTrace();
           return null;
        }
    }

    /*public List<UserPhoto> QueryByPKAndStatus(UserPhoto keyItem) {
        String sql = null;
        String prodType = keyItem.getProdType();
        List<String> params = new ArrayList<>();
        if (prodType.equals(ADDPHOTO_PROD_TYPE_EOP)) {
            sql = "SELECT * FROM EOP_UserPhoto WHERE UniqId=? AND Status=?";
            params.add(keyItem.getUniqId());
            params.add(keyItem.getStatus());
        } else if (prodType.equals(ADDPHOTO_PROD_TYPE_ED3)) {
            sql = "SELECT * FROM ED3_UserPhoto WHERE UniqId=? AND Status=?";
            params.add(keyItem.getUniqId());
            params.add(keyItem.getStatus());
        } else {
            sql = "SELECT * FROM UserPhoto WHERE UniqId = ? AND Online = ? AND Status = ?";
            params.add(keyItem.getUniqId());
            params.add(keyItem.getOnline());
            params.add(keyItem.getStatus());
        }
        try {
            return this.getJdbcTemplate().query(sql, params.toArray(), new BeanPropertyRowMapper<>(UserPhoto.class));
        } catch (DataAccessException ex) {
//            ex.printStackTrace();
            return null;
        }
    }

    public List<UserPhoto> QueryDistinctUniqIdByStatus(UserPhoto keyItem) {
        String sql = "SELECT DISTINCT UniqId, online,  prodtype,unitId FROM UserPhoto WHERE Status = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[]{keyItem.getStatus()},
                    new BeanPropertyRowMapper<>(UserPhoto.class));
        } catch (DataAccessException ex) {
//            ex.printStackTrace();
            return null;
        }
    }

    public UserPhoto outputAfterCreate(UserPhoto fullItem) {
        String sql;
        ArrayList<Object> params = new ArrayList<>();
        String mainTable = "";
        UserPhoto userPhoto = null;
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        DataSource dataSource = jdbcTemplate.getDataSource();
        TransactionStatus status = this.BeginTransaction(dataSource);
        try {
            if (ADDPHOTO_PROD_TYPE_EOP.equals(fullItem.getProdType())) {
            	UserPhoto applyCreditDto = queryEOPHasApplyCredit(fullItem.getUniqId());
                if (applyCreditDto != null) {
                    String anotherSql = "INSERT INTO UserPhoto Values(?,?,(SELECT ISNULL(Max(SubSerial) + 1,1) from UserPhoto where UniqId = ? AND Online = ?),?,?,?,?,?,?,?,?,GETDATE(),GETDATE());";
                    jdbcTemplate.update(anotherSql,applyCreditDto.getUniqId(),applyCreditDto.getOnline(),applyCreditDto.getUniqId(),applyCreditDto.getOnline(),applyCreditDto.getStatus(),applyCreditDto.getProdType(),fullItem.getPType(),fullItem.getSType(),"","",fullItem.getImageBig(),fullItem.getImageSmall());
                }
                mainTable = "EOP_UserPhoto";
                sql = "INSERT INTO " + mainTable + " OUTPUT INSERTED.* Values(?,(SELECT ISNULL(Max(SubSerial) + 1,1) from EOP_UserPhoto where UniqId = ?),?,?,?,?,?,?,GETDATE(),GETDATE(),?);";
                params.add(fullItem.getUniqId());
                params.add(fullItem.getUniqId());
                params.add(fullItem.getStatus());
                params.add(null);
                params.add(fullItem.getPType());
                params.add(fullItem.getSType());
                params.add(fullItem.getImageBig());
                params.add(fullItem.getImageSmall());
                params.add(null);
            } else if (ADDPHOTO_PROD_TYPE_ED3.equals(fullItem.getProdType())) {
                UserPhoto applyCreditDto = queryED3HasApplyCredit(fullItem.getUniqId());
                if (applyCreditDto != null) {
                    String anotherSql = "INSERT INTO UserPhoto (UniqId, Online, SubSerial, Status, ProdType, PType, SType, ProductId, UnitId, ImageBig, ImageSmall, CreateTime, UpdateTime) Values("
                                        + "?,?,(SELECT ISNULL(Max(SubSerial) + 1,1) from UserPhoto where UniqId = ? AND Online = ?),?,?,?,?,?,?,?,?,GETDATE(),GETDATE());";
                    jdbcTemplate.update(anotherSql, applyCreditDto.getUniqId(),applyCreditDto.getOnline(), applyCreditDto.getUniqId(), applyCreditDto.getOnline()
                                                    , applyCreditDto.getStatus(), applyCreditDto.getProdType(), fullItem.getPType(), fullItem.getSType(), "", "", fullItem.getImageBig(), fullItem.getImageSmall());
                }
                mainTable = "ED3_UserPhoto";
                sql = "INSERT INTO " + mainTable + " OUTPUT INSERTED.* Values(?,(SELECT ISNULL(Max(SubSerial) + 1,1) from ED3_UserPhoto where UniqId = ?),?,?,?,?,?,GETDATE(),GETDATE(),?);";
                params.add(fullItem.getUniqId());
                params.add(fullItem.getUniqId());
                params.add(fullItem.getStatus());
                params.add(fullItem.getPType());
                params.add(fullItem.getSType());
                params.add(fullItem.getImageBig());
                params.add(fullItem.getImageSmall());
                params.add(null);
//                return ed3jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(UserPhoto.class), params.toArray());
            } else {
                mainTable = "UserPhoto";
                sql = "INSERT INTO " + mainTable + " OUTPUT INSERTED.* Values(?,?,(SELECT ISNULL(Max(SubSerial) + 1,1) from UserPhoto where UniqId = ? AND Online = ?),?,?,?,?,?,?,?,?,GETDATE(),GETDATE());";
                params.add(fullItem.getUniqId());
                params.add(fullItem.getOnline());
                params.add(fullItem.getUniqId());
                params.add(fullItem.getOnline());
                params.add(fullItem.getStatus());
                params.add(fullItem.getProdType());
                params.add(fullItem.getPType());
                params.add(fullItem.getSType());
                params.add(fullItem.getProductId());
                params.add(fullItem.getUnitId());
                params.add(fullItem.getImageBig());
                params.add(fullItem.getImageSmall());
            }

//            if (fullItem.getPType() == 1) {
//                String deleteSql = "DELETE FROM " + mainTable + " WHERE UniqId=? and pType = ? and sType = ?";
//                jdbcTemplate.update(deleteSql, fullItem.getUniqId(), fullItem.getPType(), fullItem.getSType());
//            }
            userPhoto = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(UserPhoto.class), params.toArray());
            this.CommitTransaction(status);

        } catch (Exception e) {
            this.RollbakcTransaction(status);
//            e.printStackTrace();
            userPhoto = null;
        }
        return userPhoto;
    }

    public int BatchUpdate(UserPhoto fullItem) {
    	if (fullItem.getProdType().equals(ADDPHOTO_PROD_TYPE_EOP)) {
            String sql = "UPDATE EOP_UserPhoto SET Status = ?,UpdateTime = GETDATE() WHERE UniqId = ? and Status = ? ;";
            return this.getJdbcTemplate().update(sql, USERPHOTO_STATUS_WAIT_UPLOAD, fullItem.getUniqId(), USERPHOTO_STATUS_USER_PROCESS);
        } else if (fullItem.getProdType().equals(ADDPHOTO_PROD_TYPE_ED3)) {
            String sql = "UPDATE ED3_UserPhoto SET Status = ?, UpdateTime = GETDATE() WHERE UniqId = ? and Status = ? ;";
            return this.getJdbcTemplate().update(sql, USERPHOTO_STATUS_WAIT_UPLOAD, fullItem.getUniqId(), USERPHOTO_STATUS_USER_PROCESS);
        } else {
            String sql = "UPDATE UserPhoto SET Status = ?,UpdateTime = GETDATE() WHERE UniqId = ? AND Online = ? AND ProdType=? AND Status = ? ;";
            return this.getJdbcTemplate().update(sql, new Object[]{fullItem.getStatus(), fullItem.getUniqId(),
                    fullItem.getOnline(), fullItem.getProdType(), USERPHOTO_STATUS_USER_PROCESS});
        }
    }
*/
    public void deleteUserPhotoByAllValue(UserPhoto up) {
        deleteUserPhotoByAllValue(up, null);
    }
	
	public int setAllPhotoToWaitUpload(String uniqId, String status){
        String sql = "UPDATE UserPhoto SET Status = ?,UpdateTime = GETDATE() WHERE UniqId = ? ";
		return this.getJdbcTemplate().update(sql, new Object[]{status, uniqId});		
	}
	
	

    public void deleteUserPhotoByAllValue(UserPhoto up, String overTimeToDelete) {
        List<String> params = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE UserPhoto WHERE 1=1");
        if (StringUtils.isNotBlank(up.getUniqId())) {
            sb.append(" AND UniqId = ?");
            params.add(up.getUniqId());
        }
        if (StringUtils.isNotBlank(up.getProdType())) {
            sb.append(" AND ProdType = ?");
            params.add(up.getProdType());
        }
        if (up.getSubSerial() != 0) {
            sb.append(" AND SubSerial = ?");
            params.add(String.valueOf(up.getSubSerial()));
        }
        if (StringUtils.isNotBlank(up.getOnline())) {
            sb.append(" AND Online = ?");
            params.add(up.getOnline());
        }
        if (StringUtils.isNotBlank(up.getProductId())) {
            sb.append(" AND ProductId = ?");
            params.add(up.getProductId());
        }
        if (up.getPType() != 0) {
            sb.append(" AND PType = ?");
            params.add(String.valueOf(up.getPType()));
        }
        if (up.getSType() != 0) {
            sb.append(" AND　SType = ?");
            params.add(String.valueOf(up.getSType()));
        }
        if (StringUtils.isNotBlank(up.getStatus())) {
            sb.append(" AND Status = ?");
            params.add(up.getStatus());
        }
        if (StringUtils.isNotBlank(up.getUnitId())) {
            sb.append(" AND UnitId = ?");
            params.add(up.getUnitId());
        }
        if (overTimeToDelete != null && !overTimeToDelete.equals("")) {
            sb.append(" AND CONVERT(varchar,CreateTime,112) < ?");
            params.add(overTimeToDelete);
        }

        this.getJdbcTemplate().update(sb.toString(), params.toArray());
    }
/*
    public String getCCs() {
        String jsonstr = "";
        List<Map<String, Object>> ls = this.getJdbcTemplate().
                queryForList("select ProductId,CardTitle from dbo.CreditCardProduct where CardStatus = '04' order by CardSerial");
        JSONArray jsonArray = JSONArray.fromObject(ls);
        jsonstr = jsonArray.toString();
        // 回傳值
        return jsonstr;
    }

    public String copyAddphotoToUserPhoto() {
        String preSql = "Select MAX(Serial) serial from AddPhoto";
        Map<String, Object> tempMap = this.getJdbcTemplate().queryForMap(preSql);
        int maxSerial = Integer.valueOf(String.valueOf(tempMap.get("serial")));
        int min = 0;
        while (true) {
            int max = min + 500 < maxSerial ? min + 500 : maxSerial;
            String sql = "SELECT Case " +
                    "         when AddPhotoStatus.OnLine = 2 then AddPhotoStatus.ID " +
                    "         when (select top 1 UniqId " +
                    "               from CreditCaseData " +
                    "               where AddPhotoStatus.ID = CreditCaseData.Idno " +
                    "               order by substring(uniqid, 5, 8) desc) IS NULL then (select top 1 CaseNo " +
                    "                                                                    from CaseData " +
                    "                                                                    where AddPhotoStatus.ID = CaseData.Idno " +
                    "                                                                    order by substring(CaseNo, 5, 8) desc) " +
                    "         else " +
                    "           (select top 1 UniqId " +
                    "            from CreditCaseData " +
                    "            where AddPhotoStatus.ID = CreditCaseData.Idno " +
                    "            order by substring(uniqid, 5, 8) desc) end as UniqId " +
                    "    , " +
                    "       AddPhotoStatus.OnLine                           as online, " +
                    "       AddPhotoStatus.Status                           as status, " +
                    "       AddPhotoStatus.ProductType, " +
                    "       addphoto.PType, " +
                    "       addphoto.SType, " +
                    "       AddPhotoStatus.ProdID, " +
                    "       AddPhotoStatus.UnitID, " +
                    "       addphoto.PhotoB, " +
                    "       addphoto.photos, " +
                    "       AddPhotoStatus.id, " +
                    "       addphoto.Utime " +
                    "from AddPhotoStatus " +
                    "       inner join AddPhoto on addphoto.ID = AddPhotoStatus.ID " +
//                    "where AddPhotoStatus.Status in ('1', '2') and (AddPhoto.Serial between "+ min + " and " + max + " )";
                    "where AddPhotoStatus.ID='A145184308' ";
            List<Map<String, Object>> maps = this.getJdbcTemplate().queryForList(sql);
            List<UserPhoto> userPhotos = new ArrayList<>();
            List<Map<String, String>> waitUpdateAddphoto = new ArrayList<>();
            maps.forEach((map) -> {
                UserPhoto userPhoto = new UserPhoto();
                Map<String, String> addphotoStatus = new HashMap<>();
                if (null == map.get("UniqId")) {
                    userPhoto.setUniqId(String.valueOf(map.get("id")));
                } else {
                    userPhoto.setUniqId(String.valueOf(map.get("UniqId")));
                }
                addphotoStatus.put("id", String.valueOf(map.get("id")));

                userPhoto.setOnline(String.valueOf(map.get("online")));
                addphotoStatus.put("online", String.valueOf(map.get("online")));

                userPhoto.setStatus(USERPHOTO_STATUS_COPY_FROM_ADDPHOTO);
                userPhoto.setProdType(String.valueOf(map.get("ProductType")));
                addphotoStatus.put("ProductType", String.valueOf(map.get("ProductType")));
                userPhoto.setPType(Integer.valueOf(String.valueOf(map.get("PType"))));
                userPhoto.setSType(Integer.valueOf(String.valueOf(map.get("SType"))));
                userPhoto.setProductId(String.valueOf(map.get("ProdID")));
                userPhoto.setUnitId(String.valueOf(map.get("UnitID")));
                String photoB = String.valueOf(map.get("PhotoB"));
                String photoS = String.valueOf(map.get("photos"));
                try {
                    byte[] decodeB = Base64.getDecoder().decode(photoB.getBytes("UTF-8"));
                    byte[] decodeS = Base64.getDecoder().decode(photoS.getBytes("UTF-8"));
                    userPhoto.setImageBig(decodeB);
                    userPhoto.setImageSmall(decodeS);
                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
                }

                userPhoto.setCreatTime(String.valueOf(map.get("UTime")));
                userPhotos.add(userPhoto);
                waitUpdateAddphoto.add(addphotoStatus);
            });
            int[] ints = this.batchCreate(userPhotos);
            this.batchUpdateAddphotoStatus(waitUpdateAddphoto);
            @SuppressWarnings("unused")
            List<Integer> collect = Arrays.stream(ints).boxed().collect(Collectors.toList());
//            System.out.println(collect);

            if (maxSerial == max) {
                break;
            }
            min = max;
            break;
        }

        return "工作完成";
    }


    private int[] batchUpdateAddphotoStatus(List<Map<String, String>> addPhotoStatusList) {
        String sql = "UPDATE AddPhotoStatus SET Status = '4' where id=? and online=? and ProductType=?";
        return this.getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, String.valueOf(addPhotoStatusList.get(i).get("id")));
                ps.setString(2, String.valueOf(addPhotoStatusList.get(i).get("online")));
                ps.setString(3, String.valueOf(addPhotoStatusList.get(i).get("ProductType")));
            }

            @Override
            public int getBatchSize() {
                return addPhotoStatusList.size();
            }
        });
    }

    public List<UserPhoto> getFinancial(UserPhoto up) {
        String sql = "select * from UserPhoto where UniqId = ? and Online = ? and ProdType = ? and Ptype = '2'";
        try {
            return this.getJdbcTemplate().query(sql, new Object[]{up.getUniqId(), up.getOnline(), up.getProdType()},
                    new BeanPropertyRowMapper<>(UserPhoto.class));
        } catch (DataAccessException ex) {
//            ex.printStackTrace();
            return null;
        }
    }

    private UserPhoto queryEOPHasApplyCredit(String uniqId) {
        //[2019.08.19 GaryLiu]==== START : 修改程式結構 ====
    	// 原本的流程在跑queryForObject時若出現預期外的結果會throw Exception，造成後續一連串問題
    	// 但依照原流程來看，當查到的資料不符合預期的結果時應該要回傳null
    	// 基於以上原因，將流程改寫
    	//
    	UserPhoto userPhoto = new UserPhoto();
        try {
        	String sql = "select AirloanUniqId from EOP_EX_CreditCaseData where UniqId=? and Status='12'";

        	String result = null;
			try {
				result = this.getJdbcTemplate().queryForObject(sql, String.class, uniqId);
			} catch (DataAccessException ex) {
				//ex.printStackTrace();
				System.out.println("取得薪轉的airloanUniqId有誤");
			}
	        if (result != null && !result.isEmpty()) {
	            userPhoto.setUniqId(result);
	            userPhoto.setOnline("1");
	            userPhoto.setStatus("02");
	            userPhoto.setProdType("1");
	        } else {
	        	userPhoto = null;
	        }
        } catch (Exception e) {
        	userPhoto = null;
        }
        return userPhoto;
        //[2019.08.19 GaryLiu]====  END  : 修改程式結構 ====
    }
    
    private UserPhoto queryED3HasApplyCredit(String uniqId) {
        String sql = "select AirloanUniqId from ED3_EX_CreditCaseData where UniqId=? and AirloanUniqId is not null";
        String result = null;
        try {
        	result = this.getJdbcTemplate().queryForObject(sql, String.class, uniqId);
        } catch (DataAccessException ex) {
            //ex.printStackTrace();
        	System.out.println("取得薪轉的airloanUniqId有誤");
        }
        if (result != null && !result.isEmpty()) {
            UserPhoto userPhoto = new UserPhoto();
            userPhoto.setUniqId(result);
            userPhoto.setOnline("1");
            userPhoto.setStatus("02");
            userPhoto.setProdType("1");

            return userPhoto;
        } else {
            return null;
        }
    }
	
	public List<Map<String, Object>> getEopUserPhoto(String uniqId, Integer subSerial) {
		String sql = "SELECT * FROM EOP_UserPhoto WHERE UniqId = ? AND SubSerial = ?;";
		return this.getJdbcTemplate().queryForList(sql,
				new Object[] { uniqId,subSerial });
        
	}
	
	public List<UserPhoto> getED3UserPhoto(String ed3UniqId) {
		List<String> params = new ArrayList<>();
        String sql = "SELECT * FROM ED3_UserPhoto WHERE UniqId = ? AND Status in (?, ?)";
        params.add(ed3UniqId);
        params.add(USERPHOTO_STATUS_WAIT_UPLOAD);
        params.add(USERPHOTO_STATUS_UPLOAD_SUCCESS);

        try {
            return this.getJdbcTemplate().query(sql, params.toArray(),
                    new BeanPropertyRowMapper<>(UserPhoto.class));
        } catch (DataAccessException ex) {
            ex.printStackTrace();
            return null;
        }
        
    }*/
    
    // public int updateStatus(UserPhoto keyItem){
	// 	String sql = "UPDATE UserPhoto SET STATUS = ? , UpdateTime = getDate() ";
	// 	sql += " WHERE UniqId=? AND SubSerial=? ";
	// 	return this.getJdbcTemplate().update(sql, keyItem.getStatus(), keyItem.getUniqId(), keyItem.getSubSerial());
    // }

    public int updateStatus(String status, String uniqId, String online, String prodType, String ptype, String stype) {
        String sql = "UPDATE UserPhoto SET Status = ?,UpdateTime = GETDATE() WHERE UniqId = ? AND Online = ? AND ProdType=? AND Status = ? ";
		return this.getJdbcTemplate().update(sql, new Object[]{status, uniqId, online, prodType, USERPHOTO_STATUS_USER_PROCESS});
    }
    
    public int DeleteSub(String uniqId, String subSerial) {
		String sql = "DELETE UserPhoto WHERE UniqId = ? AND SubSerial = ?";
		return this.getJdbcTemplate().update(sql, new Object[] { uniqId, subSerial });
	}

	public int DeletePrimary(String uniqId, String stype) {
		String sql = "";
		sql = "DELETE UserPhoto WHERE UniqId = ? AND SType = ?";
		return this.getJdbcTemplate().update(sql, new Object[] { uniqId, stype });
    }
    
    public UserPhoto outputAfterCreate(UserPhoto fullItem){
		String sql = "INSERT INTO UserPhoto (UniqId,Online,SubSerial,ProdType,Status,PType,SType,ProductId,UnitId,ImageBig,ImageSmall,CreateTime,UpdateTime) "
            +" OUTPUT INSERTED.*  Values(?,?,(SELECT ISNULL(Max(SubSerial) + 1,1) from UserPhoto where UniqId=?),?,?,?,?,?,?,?,?,GETDATE(),GETDATE());";
			return this.getJdbcTemplate().queryForObject(sql,new BeanPropertyRowMapper<>(UserPhoto.class),
                new Object[] { fullItem.getUniqId(), fullItem.getOnline(), /* SubSerial */fullItem.getUniqId(), fullItem.getProdType(), fullItem.getStatus(),
                    fullItem.getPType(),fullItem.getSType(), fullItem.getProductId(), fullItem.getUnitId(), fullItem.getImageBig(), fullItem.getImageSmall() });
    }
    
    public List<UserPhoto> QueryByPKAndStatus(UserPhoto keyItem) {
        String sql = null;
        String prodType = keyItem.getProdType();
        List<String> params = new ArrayList<>();
        if (prodType.equals(USERPHOTO_ProdType_3)) {
            sql = "SELECT * FROM EOP_UserPhoto WHERE UniqId=? AND Status=?";
            params.add(keyItem.getUniqId());
            params.add(keyItem.getStatus());
        } else if (prodType.equals(USERPHOTO_ProdType_4)) {
            sql = "SELECT * FROM ED3_UserPhoto WHERE UniqId=? AND Status=?";
            params.add(keyItem.getUniqId());
            params.add(keyItem.getStatus());
        } else {
            sql = "SELECT * FROM UserPhoto WHERE UniqId = ? AND Online = ? AND Status = ?";
            params.add(keyItem.getUniqId());
            params.add(keyItem.getOnline());
            params.add(keyItem.getStatus());
        }
        try {
            return this.getJdbcTemplate().query(sql, params.toArray(), new BeanPropertyRowMapper<>(UserPhoto.class));
        } catch (DataAccessException ex) {
//            ex.printStackTrace();
            return null;
        }
    }

    public List<UserPhoto> findByStatus(String status) {
        String sql = "SELECT * FROM UserPhoto WHERE Status = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[]{status}, new BeanPropertyRowMapper<>(UserPhoto.class));
        } catch (DataAccessException ex) {
//            ex.printStackTrace();
            return null;
        }
    }

    public List<UserPhoto> findByStatusAndProdType(String status, String prodType) {
        String sql = "SELECT * FROM UserPhoto WHERE Status = ? AND ProdType = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[]{ status, prodType }, new BeanPropertyRowMapper<>(UserPhoto.class));
        } catch (DataAccessException ex) {
//            ex.printStackTrace();
            return null;
        }
    }

    /** 待上傳的身份證正反面 */
    public List<UserPhoto> findIdcardOnline(String uniqId, String status, String prodType) {
        String sql = "SELECT * FROM UserPhoto WHERE UniqId = ? AND Online = ? AND Status = ? AND ProdType = ? AND PType = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[]{ uniqId, USERPHOTO_ONLINE_1, status, prodType, USERPHOTO_PTYPE_1}, new BeanPropertyRowMapper<>(UserPhoto.class));
        } catch (DataAccessException ex) {
//            ex.printStackTrace();
            return null;
        }
    }

    /** 待上傳的財力證明 */
    public List<UserPhoto> findFinancialOnline(String uniqId, String status, String prodType) {
        String sql = "SELECT * FROM UserPhoto WHERE UniqId = ? AND Online = ? AND Status = ? AND ProdType = ? AND PType = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[]{ uniqId, USERPHOTO_ONLINE_1, status, prodType, USERPHOTO_PTYPE_2}, new BeanPropertyRowMapper<>(UserPhoto.class));
        } catch (DataAccessException ex) {
//            ex.printStackTrace();
            return null;
        }
    }

    public List<UserPhoto> getFinancial(UserPhoto up) {
        String sql = "select * from UserPhoto where UniqId = ? and Online = ? and ProdType = ? and Ptype = '2'";
        try {
            return this.getJdbcTemplate().query(sql, new Object[]{up.getUniqId(), up.getOnline(), up.getProdType()},
                    new BeanPropertyRowMapper<>(UserPhoto.class));
        } catch (DataAccessException ex) {
//            ex.printStackTrace();
            return null;
        }
    }

    public List<UserPhoto> findIdcardFront(String uniqId) {
        String sql = "select * from UserPhoto where UniqId = ? and Online = ? and ProdType = ? and Ptype = ? and SType = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[]{uniqId, USERPHOTO_ONLINE_1, USERPHOTO_ProdType_2, USERPHOTO_PTYPE_1, USERPHOTO_SUBTYPE_IDCARDFRONT},
                    new BeanPropertyRowMapper<>(UserPhoto.class));
        } catch (DataAccessException ex) {
            logger.error("UserPhoto 身份證正反 查無資料");
            return null;
        }
    }

    public List<UserPhoto> findIdcardBack(String uniqId) {
        String sql = "select * from UserPhoto where UniqId = ? and Online = ? and ProdType = ? and Ptype = ? and SType = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[]{uniqId, USERPHOTO_ONLINE_1, USERPHOTO_ProdType_2, USERPHOTO_PTYPE_1, USERPHOTO_SUBTYPE_IDCARDBANK},
                    new BeanPropertyRowMapper<>(UserPhoto.class));
        } catch (DataAccessException ex) {
            logger.error("UserPhoto 身份證正反 查無資料");
            return null;
        }
    }

    public List<UserPhoto> findFinancial(String uniqId) {
        String sql = "select * from UserPhoto where UniqId = ? and Online = ? and ProdType = ? and Ptype = ? and SType = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[]{uniqId, USERPHOTO_ONLINE_1, USERPHOTO_ProdType_2, USERPHOTO_PTYPE_2, USERPHOTO_SUBTYPE_99},
                    new BeanPropertyRowMapper<>(UserPhoto.class));
        } catch (DataAccessException ex) {
            logger.error("UserPhoto 財力證明 查無資料");
            return null;
        }
    }
}
