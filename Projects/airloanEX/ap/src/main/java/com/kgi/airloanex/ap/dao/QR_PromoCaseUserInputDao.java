package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.QR_PromoCaseUserInput;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class QR_PromoCaseUserInputDao extends CRUDQDao<QR_PromoCaseUserInput> {

    @Override
    public int Create(QR_PromoCaseUserInput fullItem) {
    	String sql = "INSERT INTO QR_PromoCaseUserInput (UniqId,UniqType,PromoDepart,PromoMember,ChannelId,CreateTime) Values(?,?,?,?,?,getDate())";
        return this.getJdbcTemplate().update(sql, new Object[] { fullItem.getUniqId(), fullItem.getUniqType(),
        		fullItem.getPromoDepart(), fullItem.getPromoMember(), fullItem.getChannelId() });

    }

    @Override
    public QR_PromoCaseUserInput Read(QR_PromoCaseUserInput keyItem) {
        String sql = "SELECT * FROM QR_PromoCaseUserInput WHERE UniqId = ?;";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_PromoCaseUserInput.class),
                    new Object[] { keyItem.getUniqId() });
        } catch (DataAccessException ex) {
            System.out.println("案件(" + keyItem.getUniqId() + ") 於 QR_PromoCaseUserInput 查無資料");                        
            return null;
        }
    }

    @Override
    public int Update(QR_PromoCaseUserInput fullItem) {
        String sql = "UPDATE QR_PromoCaseUserInput SET PromoDepart = ?,PromoMember = ? WHERE UniqId = ?";
        return this.getJdbcTemplate().update(sql,
                new Object[] {fullItem.getPromoDepart(),
                        fullItem.getPromoMember(), fullItem.getUniqId() });
    }

    @Override
    public int Delete(QR_PromoCaseUserInput keyItem) {
        String sql = "DELETE QR_PromoCaseUserInput WHERE UniqId = ? ";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getUniqId() });
    }

    @Override
    public List<QR_PromoCaseUserInput> Query(QR_PromoCaseUserInput keyItem) {
        return null;
    }

    public QR_PromoCaseUserInput findByUniqId(String uniqId) {
        QR_PromoCaseUserInput keyItem = new QR_PromoCaseUserInput();
        keyItem.setUniqId(uniqId);
        return Read(keyItem);
    }
}