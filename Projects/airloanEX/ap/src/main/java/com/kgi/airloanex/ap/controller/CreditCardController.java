package com.kgi.airloanex.ap.controller;

import javax.servlet.http.HttpServletRequest;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.service.CreditCardService;
import com.kgi.eopend3.ap.controller.base.BaseController;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.dto.WebResult;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/creditcard")
public class CreditCardController extends BaseController {

    private static String context = "CreditCardController";

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private AirloanEXConfig airloanEXConfig;
    
    /**
     * 根據信用卡別 ProductId 取得首刷禮
     * 
     * @param request
     * @param reqBody
     * @return
     * @deprecated
     */
    @PostMapping("/giftList")
    public String giftList(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("選擇信用卡別 : " + reqBody);
        try {
            // KGIHeader header = this.getHeader(request);
            // String uniqId = header.getUniqId();
            if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
                return creditCardService.findGiftListByProductIdView(reqBody);
            } else {
                throw new ErrorResultException(9, "Invalid Input", null);
            }
        } catch ( ErrorResultException e) {
            return WebResult.GetResultString(9, "Invalid Input", null);
        }
    } // end giftList

    /**
     * 取得預設信用卡別的首刷禮
     * 
     * @param request
     * @param reqBody
     * @return
     */
    @GetMapping("/firstGiftList")
    public String firstGiftList(HttpServletRequest request) {
        logger.info("取得預設信用卡別的首刷禮");
        try {
            // KGIHeader header = this.getHeader(request);
            // String uniqId = header.getUniqId();
            return creditCardService.findGiftListByProductId(airloanEXConfig.Apply_Creditcard_ProductId_Default());
        } catch ( ErrorResultException e) {
            return WebResult.GetResultString(9, "Invalid Input", null);
        }
    } // end firstGiftList
} // end CreditCardController
