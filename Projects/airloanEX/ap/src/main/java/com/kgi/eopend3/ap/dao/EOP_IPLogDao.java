package com.kgi.eopend3.ap.dao;

import java.util.List;

import com.kgi.eopend3.common.dto.db.EOP_IPLog;

import org.springframework.stereotype.Repository;

@Repository
public class EOP_IPLogDao extends CRUDQDao<EOP_IPLog> {

    @Override
    public int Create(EOP_IPLog fullItem) {
        String sql = "INSERT INTO EOP_IPLog (UniqId,IPAddress,CreateTime) VALUES (?,?,getDate()) ";
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getUniqId(), fullItem.getIPAddress() });
    }

    @Override
    public EOP_IPLog Read(EOP_IPLog keyItem) {
        return null;
    }

    @Override
    public int Update(EOP_IPLog fullItem) {
        return 0;
    }

    @Override
    public int Delete(EOP_IPLog keyItem) {
        return 0;
    }

    @Override
    public List<EOP_IPLog> Query(EOP_IPLog keyItem) {
        return null;
    }

}