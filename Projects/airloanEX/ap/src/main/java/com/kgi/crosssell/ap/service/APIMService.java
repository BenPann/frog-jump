package com.kgi.crosssell.ap.service;

import static com.kgi.airloanex.common.PlContractConst.ExternalCallPostMan;

import java.io.IOException;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.dao.ApiLogDao;
import com.kgi.airloanex.ap.service.APSPlContractService;
import com.kgi.airloanex.common.dto.db.ApiLog;
import com.kgi.crosssell.ap.dao.APIMDao;
import com.kgi.crosssell.common.dto.CSCrossBusinessApply;
import com.kgi.crosssell.common.dto.CSStkCustInfo;
import com.kgi.eopend3.common.util.DateUtil;

import org.owasp.esapi.errors.IntrusionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.json.JSONObject;

@Service
public class APIMService {

	@Autowired
	private APIMDao apimDao;

	@Autowired
	private ApiLogDao apiLogDao;
	
    @Autowired
    private APSPlContractService apsPlContractService;

	private String queryAPIM(String apimUrl, String qryName, String queryJson) throws IOException {
		// 1. 組合出 request 的 url
		String response = "";
		String startTime = "";
		String endTime = "";
		String resultValidation = "";
		try {
			// 2. 發送 request
			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			response = apimDao.queryAPI(apimUrl, qryName, queryJson);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			// if(ESAPI.validator().isValidInput("", response, "AntiXSS", Integer.MAX_VALUE,
			// true)) {
			resultValidation = response;
			// }
		} catch (IntrusionException e) {
			System.out.println("ESAPI validation failed");
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			// 3. 寫apilog
			if ("".equals(endTime)) {
				endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			}
			ApiLog apiLog = new ApiLog();
			apiLog.setApiName(qryName);
			apiLog.setRequest(queryJson);
			apiLog.setResponse(resultValidation);
			apiLog.setStartTime(startTime);
			apiLog.setEndTime(endTime);

			apiLogDao.Create(apiLog);
		}

		// 4. 返回結果
		return resultValidation;
	}

	public CSStkCustInfo getStkCustInfo(String queryString, String idno)
			throws Exception {
		JSONObject req = new JSONObject();
		req.put("idno", idno);
		String result = apsPlContractService.queryAPIMForCS(queryString, req.toString(), ExternalCallPostMan);	
		// String result = queryAPIM(url, apiGetStkName, req.toString());
		
		Gson gson = new Gson();
		
		return gson.fromJson(result, CSStkCustInfo.class);
	}

	public CSCrossBusinessApply getCrossBusinessInfo(String queryString, String token)
			throws Exception {
		JSONObject req = new JSONObject();
		req.put("tokenVal", token);
		String result = apsPlContractService.queryAPIMForCS(queryString, req.toString(), ExternalCallPostMan);	
		// String result = queryAPIM(url, apiGetCrossBusinessName, req.toString());
		
		Gson gson = new Gson();
		
		return gson.fromJson(result, CSCrossBusinessApply.class);
	}

}
