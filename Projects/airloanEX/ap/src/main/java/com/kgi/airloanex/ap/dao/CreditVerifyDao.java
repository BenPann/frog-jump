package com.kgi.airloanex.ap.dao;

import java.util.List;
import java.util.Map;

import com.kgi.airloanex.common.dto.db.CreditVerify;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;
@Repository
public class CreditVerifyDao extends CRUDQDao<CreditVerify> {

	
	
	/**
     * 取得信用卡或存款帳戶驗證結果
     */
    public List<Map<String, Object>> getCreditVerifyStatus(String uniqId) {
        return this.getJdbcTemplate().queryForList("SELECT VerifyStatus,VerifyType FROM CreditVerify WHERE UniqId = ? AND 1=1;",
                new Object[] { uniqId });
    }

	@Override
	public int Create(CreditVerify fullItem) {
		String sql = "INSERT INTO CreditVerify " 
		+ "( UniqId, UniqType, VerifyStatus, VerifyType, CardNo, CardTime, BankId, AccountNo, ErrorCount, CreateTime, UpdateTime, NCCCErrorCount, Pcode2566ErrorCount) " 
+ " VALUES (:UniqId,:UniqType,:VerifyStatus,:VerifyType,:CardNo,:CardTime,:BankId,:AccountNo,:ErrorCount, getdate(),  getdate(), :NCCCErrorCount,:Pcode2566ErrorCount) ";
return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));

	}

	@Override
	public CreditVerify Read(CreditVerify keyItem) {
		String sql = "SELECT * FROM CreditVerify WHERE UniqId = ? ";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CreditVerify.class),
                    keyItem.getUniqId());
        } catch (DataAccessException ex) {
            System.out.println("CreditVerify查無資料");
            return null;
        }
	}

	@Override
	public int Update(CreditVerify fullItem) {
		String sql = "UPDATE CreditVerify SET VerifyStatus = ? , VerifyType = ?, CardNo = ?, CardTime = ? ,BankId = ?, "
				+ " AccountNo = ? , ErrorCount = ? , UpdateTime = getdate() , NCCCErrorCount = ? , Pcode2566ErrorCount = ? "
				+ " WHERE UniqId = ? ";
	    return this.getJdbcTemplate().update(sql, fullItem.getVerifyStatus(), fullItem.getVerifyType(), 
	    		fullItem.getCardNo(), fullItem.getCardTime(), fullItem.getBankId(), fullItem.getAccountNo(), 
	    		fullItem.getErrorCount(), fullItem.getNCCCErrorCount(), fullItem.getPcode2566ErrorCount(), 
	    		fullItem.getUniqId());
	}

	@Override
	public int Delete(CreditVerify keyItem) {
		String sql = "DELETE CreditVerify WHERE UniqId = ? ";
        return this.getJdbcTemplate().update(sql, keyItem.getUniqId());
	}

	@Override
	public List<CreditVerify> Query(CreditVerify keyItem) {
		return null;
	}
}
