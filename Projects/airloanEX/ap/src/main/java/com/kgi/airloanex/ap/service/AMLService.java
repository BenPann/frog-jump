package com.kgi.airloanex.ap.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kgi.airloanex.ap.dao.APSDao;
import com.kgi.airloanex.ap.dao.AmlDao;
import com.kgi.airloanex.ap.dao.CaseDataDao;
import com.kgi.airloanex.ap.dao.ContractApiLogDao;
import com.kgi.airloanex.ap.dao.CreditCaseDataDao;
import com.kgi.airloanex.ap.dao.FastPassDao;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.db.AmlInfo;
import com.kgi.airloanex.common.dto.db.FastPass;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.common.dto.db.Config;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AMLService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AmlDao amlDao;

    @Autowired
    private ConfigDao configDao;

    @Autowired
    private APSDao apsDao;

    @Autowired
    private CaseDataDao caseDataDao;

    @Autowired
    private CreditCaseDataDao creditCaseDataDao;

    @Autowired
    private FastPassDao fastPassDao;

    @Autowired
    private ContractApiLogDao contractApiLogDao;

    public Map<String, String> getAMLResponse(String uniqId, String idno, String system_type, String check_type, String checkDept,
            List<Map<String, Object>> list) throws IOException {
        return amlDao.getAMLResponse(uniqId, idno, system_type, check_type, checkDept, list);
    }

    /**
     * 查詢AML
     * 參數 idno        : 身分證字號
     *      customerName: 姓名
     *      checkDept   : 發查單位
     */
    public Map<String, String> processAml(String reqBody) { // String idno, String name, String checkDept
        Gson reqGson = new Gson();
        AmlInfo amlInfo = reqGson.fromJson(reqBody, AmlInfo.class);
        return processAml(null, amlInfo);
    } // end processAml

    public Map<String, String> processAml(String uniqId, String idno, String name, String checkDept) {
        AmlInfo amlInfo = new AmlInfo();
        amlInfo.setID_NO(idno);
        amlInfo.setNAME(name);
        amlInfo.setCHECK_DEPT(checkDept);
        return processAml(uniqId, amlInfo);
    }

    /**
     * 打 AML
     * 
     * 若無uniqId，則寫入Apilog
     * 若有uniqId，則寫入ContractApiLog
     */
    public Map<String, String> processAml(String uniqId, AmlInfo amlInfo) {
        System.out.println("*****************GET AML*****************");
        String startTime = DateUtil.GetDateFormatString("yyyy-MM-dd hh:mm:ss.SSS");

        Gson reqGson = new Gson();
        String reqBody = reqGson.toJson(amlInfo);

        // 回傳電文字串 格式 CHECK_CODE, ERR_MSG, AML_RESULT, AML_DATA
        Map<String, String> respMap = new LinkedHashMap<>();

        // 傳回AML_DATA XML資料
        Map<String, String> rsp;
        
        String idno = amlInfo.getID_NO();
        String name = amlInfo.getNAME();
        String checkDept = amlInfo.getCHECK_DEPT();

        Map<String, String> amlMap = configDao.QueryToMap(new Config("AML"));
        logger.info("### amlMap => " + amlMap);

        String system_type = amlMap.getOrDefault("AML.system_type", "13"); // 原始值 09
        String check_type = amlMap.getOrDefault("AML.check_type", "1");
        // config.getString("AML.check_dept", "9743");

        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ch_name", name);
        map.put("country", "TW");
        map.put("en_name", "");
        map.put("entity_type", amlMap.getOrDefault("AML.entity_type", "3"));
        map.put("id", idno);
        map.put("seq", 1);
        list.add(map);

        // 寫ContractApiLog
        contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, uniqId, "/KGI/GET_AML_DATA_start", reqBody, "Start @ " + startTime, startTime, startTime); 		
        
        
		try {
            rsp = this.getAMLResponse(uniqId, idno, system_type, check_type, checkDept, list);
            if (StringUtils.equals("99", rsp.get("aml_result"))) {
                respMap.put("CHECK_CODE", "99");
                respMap.put("ERR_MSG", "資料錯誤");
                respMap.put("AML_RESULT", rsp.get("aml_result"));
                respMap.put("AML_XML_DATA", rsp.get("aml_data"));
            } else {
                respMap.put("CHECK_CODE", "");
                respMap.put("ERR_MSG", "");
                respMap.put("AML_RESULT", rsp.get("aml_result"));
                respMap.put("AML_XML_DATA", rsp.get("aml_data"));
            }
        } catch (IOException ex) {
            respMap.put("CHECK_CODE", "99");
            respMap.put("ERR_MSG", "連線失敗");
            respMap.put("AML_RESULT", "");
            respMap.put("AML_XML_DATA", "");
            logger.error("=== 連線失敗 ===" + ex);
        }

        String endTime = DateUtil.GetDateFormatString("yyyy-MM-dd hh:mm:ss.SSS");
        respMap.put("TIME", endTime);

        Gson resGson = new GsonBuilder().disableHtmlEscaping().create();

        String resStr = resGson.toJson(respMap);
        if (StringUtils.isBlank(uniqId)) {
            // 寫ApiLog
            apsDao.insertApiLog("/KGI/GET_AML_DATA", reqBody, resStr, startTime, endTime);
        } else {
            // 寫ContractApiLog
            contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, uniqId, "/KGI/GET_AML_DATA", reqBody, resStr, startTime, endTime);           
        }
        return respMap;
    }

	public String checkIsHighRisk(String uniqId,String type){
		List<Map<String, Object>> list = null;
		StringBuilder rs = new StringBuilder();
		rs.append(",");//先以逗號分隔
		//type = 卡加貸 | 純貸
		switch (type){
			case "applyccLoan-loan":
				rs.append("[卡加貸]");
			case "applyLoan":
				list = caseDataDao.getCaseDataForCheckHighRiskOccupation(uniqId);
				break;
			case "applyccLoan-cc":
				rs.append("[卡加貸]");
			case "creditcard":
				list = creditCaseDataDao.getCreditDataForCheckHighRiskOccupation(uniqId);
				break;
		}

		try {
			String occupation = String.valueOf(list.get(0).get("occupation"));
			String jobTitle = String.valueOf(list.get(0).get("titlie"));
			FastPass read = fastPassDao.Read(new FastPass(uniqId));
			String amlResult = (read != null && !read.getAML().equals("")) ? read.getAML() : "";
			if(isHighRiskOccupation(occupation,jobTitle)){
				rs.append("高風險職業:").append(occupation);
			}
			if(isHighRiskAMLResult(amlResult)){
				if(rs.length()>7) {
					//有高風險職業。
					rs.append(",");
				}
				rs.append("AML查詢結果:").append(amlResult);
			}

		}catch (IndexOutOfBoundsException | NullPointerException ex){
			return null;
		}
		//如果沒有高風險職業和AML高風險查詢結果 lenght<=6
		if(rs.length()<7){
			return null;
		}
		return rs.toString();
    }
    
    /** 檢查AML狀態是否為高風險 */
	public boolean isHighRiskAMLResult(String amlResult) {
		// 如果AML結果為Y或V
		return amlResult.equals("Y") || amlResult.equals("V");
	}

    /** 檢查職業類別是否為高風險 */
	public boolean isHighRiskOccupation(String occupation, String jobTitle) {
		boolean highRisk = false;
		// 是否高風險職業類別

		// 先組合起來
		String occupationJobTitle = occupation.concat(",").concat(jobTitle);

		// 第一碼為1 代表是高風險職業類別
		// if (occupation.substring(0,1).equals("1")) {
		// 	highRisk = true;
		// }

		// 找設定檔設定的高風險組合
		String highRiskOccupationJobTitle = configDao.ReadConfigValue("AML.HighRiskOccupationJobTitle", "");
		if (highRiskOccupationJobTitle != null && highRiskOccupationJobTitle.indexOf(occupationJobTitle)>=0) {
			highRisk = true;
		}

		return highRisk;
	}
}
