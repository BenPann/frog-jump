package com.kgi.crosssell.ap.service;

import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.APSDao;
import com.kgi.airloanex.ap.dao.ApiLogDao;
import com.kgi.airloanex.ap.dao.CS_CommonDataDao;
import com.kgi.airloanex.ap.dao.CS_VerifyLogDao;
import com.kgi.airloanex.ap.dao.UserPhotoDao;
import com.kgi.airloanex.ap.dao.WhiteListDataDao;
import com.kgi.airloanex.common.dto.db.CS_CommonData;
import com.kgi.airloanex.common.dto.db.CS_VerifyLog;
import com.kgi.airloanex.common.dto.db.QR_ChannelList;
import com.kgi.airloanex.common.dto.db.UserPhoto;
import com.kgi.airloanex.common.dto.view.CSInsertFucoApiLog;
import com.kgi.crosssell.ap.dao.APIMDao;
import com.kgi.crosssell.ap.dao.CS_AumDataDao;
import com.kgi.crosssell.ap.dao.CS_PersonalDataDao;
import com.kgi.crosssell.common.SystemConst;
import com.kgi.crosssell.common.dto.Attachments;
import com.kgi.crosssell.common.dto.CSCommon;
import com.kgi.crosssell.common.dto.CSCommonImage;
import com.kgi.crosssell.common.dto.CSCrossBusinessApply;
import com.kgi.crosssell.common.dto.CSStkCustInfo;
import com.kgi.crosssell.common.dto.CS_AumData;
import com.kgi.crosssell.common.dto.CS_PersonalData;
import com.kgi.crosssell.common.dto.Portfolios;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.ap.dao.HttpDao;
import com.kgi.eopend3.ap.service.ImageService;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.json.JSONObject;

@Service
public class CrossSellService {

	@Autowired
	private WhiteListDataDao whiteListDataDao;
	@Autowired
	private CS_AumDataDao cAumDataDao;
	@Autowired
	private CS_VerifyLogDao cVerifyLogDao;
	@Autowired
	private CS_PersonalDataDao cPersonalDataDao;
	@Autowired
	private FucoApiLogService fucoApiLogService;

	@Autowired
	private HttpDao httpDao;
	@Autowired
	private ImageService imageService;
	@Autowired
	private UserPhotoDao userPhotoDao;

	@Autowired
	private CS_CommonDataDao cs_CommonDataDao;
	
    @Autowired
    private APSDao apsDao;
    @Autowired
    private APIMDao apimDao;
	
    @Autowired
    AirloanEXConfig globalConfig;

	public String getFakeRtnData(String inToken) {
		String cifResult = "";
		// 取KEY(get right 10 char)

		// left part
		String token = inToken.substring(0, inToken.length() - 10);
		System.out.println("#### getFakeRtnData: token=" + token);

		// right part
		String idString = inToken.length() > 10 ? inToken.substring(inToken.length() - 10) : inToken;
		System.out.println("#### getFakeRtnData: idString=" + idString);

		// 會去WhiteListData找 apiname='{{sample_token}}' 的那一行資料當回傳
		cifResult = whiteListDataDao.findWhiteListData("{{sample_token}}", token, idString);
		return cifResult;
	}

	public void insertFucoApiLog(CSInsertFucoApiLog insertData) {
		JSONObject json = new JSONObject();
		json.put("prodid", insertData.getProdid());
		json.put("channelId", insertData.getChannelId());
		json.put("token", insertData.getToken());
		json.put("idno", insertData.getIdno());
		json.put("ip", insertData.getIp());
		json.put("shortUrl", insertData.getShortUrl());
		json.put("depart", insertData.getDepart());
		json.put("member", insertData.getMember());
		json.put("entry", insertData.getEntry());

		fucoApiLogService.insertFucoApiLog("/CS/redirect", json.toString(), "");
	}

	public CS_PersonalData Read(CS_PersonalData keyItem) {
		return cPersonalDataDao.Read(keyItem);
	}

	public void createCS(String uniqId, String uniqType, CSStkCustInfo stkCust
		, CSCrossBusinessApply crossBusinessApply, String idno, String prodid) {

		try{
			String hasAum = "0";
			if (crossBusinessApply != null && crossBusinessApply.getPortfolios() != null
				&& crossBusinessApply.getPortfolios().size() > 0) {
					hasAum = "1";
			}

			System.out.println("跨售B:解析個人基本資料-" + uniqId);
			addPesronDataAndVerifyLog(uniqId, uniqType, stkCust, crossBusinessApply, hasAum);

			System.out.println("跨售B:解析幣別資料-" + uniqId);
			addAUMData(uniqId, uniqType, crossBusinessApply);

			System.out.println("跨售B:解析圖片檔-" + uniqId);
			addPhoto(uniqId, prodid, crossBusinessApply);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private void addPesronDataAndVerifyLog(String uniqId, String uniqType, CSStkCustInfo custInfo
		, CSCrossBusinessApply crossBusinessApply, String hasAum) {
		
		String idno = custInfo.getIdno();

		CS_PersonalData personalData = new CS_PersonalData();
		personalData.setUniqId(uniqId);
		personalData.setUniqType(uniqType);
		personalData.setIdno(idno);
		personalData.setHasAum(hasAum);
		personalData.setName(custInfo.getCustName1());
		personalData.setMobile(custInfo.getPhone());
		personalData.setEMail(custInfo.getEmail());
		personalData.setCommAddress(custInfo.getCommAddr());
		personalData.setCommAddrZipCode(custInfo.getCommAddrZip());
		personalData.setBirthday(custInfo.getBirthday());
		
		String telPhone = StringUtils.trimToEmpty(custInfo.getHomeTel());
		if (StringUtils.isNotEmpty(telPhone) && telPhone.trim().length() > 2){
			personalData.setResTelArea(telPhone.substring(0, 2));
			personalData.setResPhone(telPhone.substring(2, telPhone.length()));
		} else {
			System.out.println("#### CS 傳入電話為空(uniqid=" + uniqId + ", id=" + idno + ")");
			personalData.setResTelArea("");
			personalData.setResPhone("");
		}

		cPersonalDataDao.Create(personalData);

		CS_VerifyLog cs_VerifyLog = new CS_VerifyLog();
		cs_VerifyLog.setUniqId(uniqId);
		cs_VerifyLog.setUniqType(uniqType);
		cs_VerifyLog.setIdno(idno);
		cs_VerifyLog.setIpAddress(crossBusinessApply.getIp());
		cs_VerifyLog.setTime(crossBusinessApply.getApplyDateTime());
		cs_VerifyLog.setMessageSerial(crossBusinessApply.getCaCert());
		cs_VerifyLog.setContractVersion(crossBusinessApply.getContractVer());
		
		cVerifyLogDao.Create(cs_VerifyLog);

	}

	private void addAUMData(String uniqId, String uniqType, CSCrossBusinessApply crossBusinessApply) {

		List<Portfolios> portfoliosList = crossBusinessApply.getPortfolios();
		for (Portfolios portfolios : portfoliosList){
			CS_AumData aumData = new CS_AumData();
			aumData.setUniqId(uniqId);
			aumData.setUniqType(uniqType);
			aumData.setCurrency(portfolios.getCurrency());
			aumData.setTotalFinancial(portfolios.getValueSum());
			aumData.setStockBaseDate(portfolios.getBaseDate());

			cAumDataDao.Create(aumData);
		}

	}

	private void addPhoto(String uniqId, String prodid, CSCrossBusinessApply crossBusinessApply) {

		List<Attachments> attachmentsList = crossBusinessApply.getAttachments();
		for (Attachments attachments :attachmentsList){
			
			String base64Image = attachments.getFileData();
			if (base64Image == null)
				return;
			if (base64Image.equals(""))
				return;
			try {
				byte[] bigImg = null;
				byte[] smallImg = null;
				String waterMark = "";
				Boolean drawWaterMark = false;
				String[] imageArr = base64Image.split("base64,");
				if (imageArr.length == 2) {
					base64Image = imageArr[1];
				}
				bigImg = Base64.decodeBase64(base64Image);
				bigImg = resizeImageFile(bigImg, waterMark, drawWaterMark);
				smallImg = resizeSmallImageFile(bigImg);

				UserPhoto photo = new UserPhoto();
				photo.setUniqId(uniqId);
				photo.setOnline(String.valueOf(SystemConst.PHOTO_ONLINE_STATUS_ONLINE));
				photo.setStatus(SystemConst.USERPHOTO_STATUS_TEMP);
				photo.setProdType(prodid);
				photo.setPType(2);
				photo.setSType(25);
				photo.setImageBig(bigImg);
				photo.setImageSmall(smallImg);
				userPhotoDao.Create(photo);

			} catch (IOException ex) {
				// 如果圖片處裡有問題 就換下一張圖 不丟出訊息
				// ex.printStackTrace();
			}
		}
	}

	public CSCommon getCSCommonCif(QR_ChannelList channel, String token) throws IOException {
		String cifResult = "";
		JSONObject json = new JSONObject();
		if (channel.getUrlType().equals("0")) {
			json.put("token", token);
			// 內網
			cifResult = this.sendHttpPost("CSCommon.getCif", channel.getCifUrl(), json.toString());
			// 20200911 pchome測試 假資料
			// cifResult="{\"checkCode\":\"\",\"errMsg\":\"\",\"data\":{\"chtName\":\"陳XX\",\"engName\":\"\",\"idno\":\"\",\"idCardDate\":\"\",\"idCardLocation\":\"\",\"idCardCRecord\":null,\"gender\":1,\"birthday\":\"\",\"mobile\":\"0966666666\",\"productType\":\"\",\"productId\":\"\",\"applyAmount\":20,\"period\":null,\"purpose\":null,\"purposeOther\":\"\",\"email\":\"w1050905@gmail.com\",\"education\":6,\"marriage\":null,\"resAddrZipCode\":\"\",\"resAddr\":\"\",\"commAddrZipCode\":\"\",\"commAddr\":\"\",\"homeAddrZipCode\":\"\",\"homeAddr\":\"\",\"resTelArea\":\"\",\"resTel\":\"\",\"homeTelArea\":\"\",\"homeTel\":\"\",\"estateType\":null,\"occupation\":\"\",\"corpName\":\"東港商號\",\"corpTelArea\":\"\",\"corpTel\":\"\",\"corpTelExten\":\"\",\"corpAddrZipCode\":\"\",\"corpAddr\":\"\",\"corpDepart\":\"\",\"jobTitle\":\"負責人\",\"onBoardDate\":\"201411\",\"yearlyIncome\":57,\"taxIdno\":\"\",\"projectCode\":\"\",\"additionalData\":{\"businessRevenueYear\":\"200\",\"businessRevenuePerMonth\":\"20;20;20;40;20;20;20;40;20;20;20;40\",\"creditLimit\":\"120\",\"rate\":\"1.40\",\"mobile\":\"0955555555\",\"prjCode\":\"A1111\"},\"score\":\"\",\"channelId\":\"BP\",\"promoDepart\":\"9999\",\"promoMember\":\"999999\",\"image\":\"\"}}";
		} else if (channel.getUrlType().equals("1")) {

			json.put("token", token);

			if (channel.getChannelId().equals("RT")) {
				// json.put("__branch__", "3402-kgi-memberData");
				System.out.println("#### 1.POST To PCHomePay >>> Target:[" + channel.getCifUrl() + "], json:["
						+ json.toString() + "]");
			}

			// 外網 要透過APIM
			Map<String, String> map = new HashMap<String, String>();
			map.put("URL", channel.getCifUrl());

			if (channel.getChannelId().equals("RT") && channel.getCifUrl().substring(0, 8).equals("FAKERTN:")) {
				System.out.println("#### 進入 FAKERTN 模式 >>>");

				// 20201015 pchome測試，如果目標 CifUrl 前端有 FAKERTN: 字樣，則回預設假資料
				cifResult = getFakeRtnData(token);

				if (cifResult == null || cifResult.equals("")) {
					System.out.println("#### (X)WhiteListData 無Token(" + token + ")對應資料");
				} else {
					System.out.println("#### (O)WhiteListData 有Token(" + token + ")對應資料");
				}

			} else {

				System.out.println("#### 呼叫 APIM >>> CifUrl:" + channel.getCifUrl() + ", token:" + token);
				cifResult = this.sendApim("/v1/CROSS_CO/getDataFromURL", json.toString(), map);

			}

			// cifResult = apimDao.queryAPIM(apimUrl, "/v1/CROSS_CO/getDataFromURL",
			// json.toString(), channel.getCifUrl());

		} else {
			return null;
		}

		if (cifResult == null || cifResult.equals("")) {
			System.out.println("getCSCommonCif:回傳資料為空");
			return null;
		}

		JSONObject cif = JSONObject.fromObject(cifResult);

		System.out.println("#### RTN >>> getCSCommonCif=" + cif.toString());

		return (CSCommon) JSONObject.toBean(cif, CSCommon.class);
	}

	//// For pchome
	public JSONObject getCSCommonCifWithAdditionalData(QR_ChannelList channel, String token) throws IOException {
		String cifResult = "";
		JSONObject json = new JSONObject();
		if (channel.getUrlType().equals("0")) {
			json.put("token", token);
			// 內網
			cifResult = this.sendHttpPost("CSCommon.getCif", channel.getCifUrl(), json.toString());

		} else if (channel.getUrlType().equals("1")) {
			json.put("token", token);
			if (channel.getChannelId().equals("RT")) {
				// json.put("__branch__", "3402-kgi-memberData");
				System.out.println("#### 2.POST To PCHomePay >>> Target:[" + channel.getCifUrl() + "], json:["
						+ json.toString() + "]");
			}

			// 外網 要透過APIM
			Map<String, String> map = new HashMap<String, String>();
			map.put("URL", channel.getCifUrl());

			if (channel.getChannelId().equals("RT") && channel.getCifUrl().substring(0, 8).equals("FAKERTN:")) {
				System.out.println("#### 進入 FAKERTN 模式 >>>");

				// 20201015 pchome測試，如果目標 CifUrl 前端有 FAKERTN: 字樣，則回預設假資料
				cifResult = getFakeRtnData(token);

			} else {

				cifResult = this.sendApim("/v1/CROSS_CO/getDataFromURL", json.toString(), map);

			}

		} else {
			return null;
		}

		if (cifResult == null || cifResult.equals("")) {
			System.out.println("getCSCommonCif:回傳資料為空");
			return null;
		}

		String cifResult2 = cifResult.replaceAll("null", "\\\\\"\\\\\"");

		// System.out.println("####** RT FAKERTN >>> cifResult2=" + cifResult2);

		JSONObject cifWithAdditionalData = JSONObject.fromObject(cifResult2);

		// System.out.println("#### RT RTN >>> getCSCommonCifWithAdditionalData=" +
		// cifWithAdditionalData.toString());

		return cifWithAdditionalData;
	}

	public String getCSCommonMemo(QR_ChannelList channel, String token) throws IOException {
		String cifResult = "";
		JSONObject json = new JSONObject();
		if (channel.getUrlType().equals("0")) {
			json.put("token", token);
			// 內網
			cifResult = this.sendHttpPost("CSCommon.getMemo", channel.getMemoUrl(), json.toString());

		} else if (channel.getUrlType().equals("1")) {
			json.put("token", token);
			// 外網 要透過APIM
			Map<String, String> map = new HashMap<String, String>();
			map.put("URL", channel.getCifUrl());
			cifResult = this.sendApim("/v1/CROSS_CO/getDataFromURL", json.toString(), map);
		} else {
			System.out.println("getCSCommonMemo:回傳資料為空");
			return null;
		}

		return cifResult;
	}

	public void processImage(String uniqId, String prodId, CSCommonImage image) {
		// 身分證正面
		uploadImage(image.getIdCardFront(), uniqId, prodId, 1, 1, SystemConst.USERPHOTO_STATUS_TEMP);
		// 身分證反面
		uploadImage(image.getIdCardBack(), uniqId, prodId, 1, 2, SystemConst.USERPHOTO_STATUS_TEMP);
		// 名片
		uploadImage(image.getBusinessCard(), uniqId, prodId, 1, 0, SystemConst.USERPHOTO_STATUS_TEMP);
		if (image.getFinancialProof() != null) {
			// 財力證明 先放其他
			for (String financial : image.getFinancialProof()) {
				uploadImage(financial, uniqId, prodId, 2, 99, SystemConst.USERPHOTO_STATUS_TEMP);
			}
		}
	}

	public void uploadImage(String base64Image, String uniqId, String prodId, Integer ptype, Integer stype,
			String status) {
		if (base64Image == null)
			return;
		if (base64Image.equals(""))
			return;
		try {
			byte[] bigImg = null;
			byte[] smallImg = null;
			String waterMark = "";
			Boolean drawWaterMark = false;
			String[] imageArr = base64Image.split("base64,");
			if (imageArr.length == 2) {
				base64Image = imageArr[1];
			}
			bigImg = Base64.decodeBase64(base64Image);
			bigImg = resizeImageFile(bigImg, waterMark, drawWaterMark);
			smallImg = resizeSmallImageFile(bigImg);
			UserPhoto photo = new UserPhoto();
			photo.setUniqId(uniqId);
			photo.setOnline(SystemConst.PHOTO_ONLINE_STATUS_ONLINE.toString());
			photo.setProdType(prodId);
			photo.setPType(ptype);
			photo.setSType(stype);
			photo.setStatus(status);
			photo.setImageBig(bigImg);
			photo.setImageSmall(smallImg);
			photo.setProductId("");
			photo.setUnitId("");
			userPhotoDao.Create(photo);
		} catch (IOException ex) {
			// 如果圖片處裡有問題 就換下一張圖 不丟出訊息
			// ex.printStackTrace();
		}
	}

	// 影像 resize 成大小圖 + 壓浮水印
	public byte[] resizeImageFile(byte[] bigPhoto, String waterText, boolean drawWaterText) throws IOException {
		// 讀入原始圖檔
		InputStream is = new ByteArrayInputStream(bigPhoto);
		BufferedImage bigImg = ImageIO.read(is);

		// 大圖 resize
		bigImg = imageService.resizeToPixels(bigImg, 3000000);

		// 壓浮水印
		if (drawWaterText && waterText != null && waterText.length() > 0) {
			bigImg = imageService.addTextWatermark(waterText, bigImg);
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bigImg, "jpg", baos);
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();
		return imageInByte;
		// 從大圖做出小圖

	}

	public byte[] resizeSmallImageFile(byte[] bigPhoto) throws IOException {
		InputStream is = new ByteArrayInputStream(bigPhoto);
		BufferedImage bigImg = ImageIO.read(is);
		BufferedImage smallImage = null;
		smallImage = imageService.getScaledInstance(bigImg, 250, 166,
				RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR, false);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(smallImage, "jpg", baos);
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();
		return imageInByte;
	}

	public void insertCSCommonData(String uniqId, String uniqType, String CSType, String channelId, String resultData) {
		// TODO Auto-generated method stub
		CS_CommonData cs_CommonData = new CS_CommonData(uniqId);
		cs_CommonData = cs_CommonDataDao.Read(cs_CommonData);
		if (cs_CommonData == null) {
			cs_CommonData = new CS_CommonData(uniqId);
			cs_CommonData.setUniqType(uniqType);
			cs_CommonData.setCSType(CSType);
			cs_CommonData.setChannelId(channelId);
			cs_CommonData.setResultData(resultData);
			cs_CommonDataDao.Create(cs_CommonData);
		}

	}

    /** 使用httpPost 發出交易 (會寫Apilog) */
    private String sendHttpPost(String apiName, String url, String req) throws IOException {
        String res = "";
        String startTime = "";
        String endTime = "";
        try {
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            res = httpDao.doJsonPostRequest(url, req);
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            apsDao.insertApiLog(apiName, req, res, startTime, endTime);
        }
        return res;

    }

    private String sendApim(String apiName, String req, Map<String, String> optionMap) throws IOException {
        String res = "";
        String startTime = "";
        String endTime = "";
        String apimUrl = globalConfig.APIM_ServerHost();
        String resultValidation = "";

        System.out.println("#### 透過 APIM 發查外部資料 ... apimUrl=" + apimUrl + ", apiName=" + apiName + ", req=" + req);

        try {
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            res = apimDao.queryAPIM(apimUrl, apiName, req, optionMap);
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");

            if (ESAPI.validator().isValidInput("", res, "AntiXSS", Integer.MAX_VALUE, true)) {
                resultValidation = res;
            }

            if (resultValidation.equals("")) {
                System.out.println("###### ESAPI validation 失敗 !!!!! ");
                System.out.println("###### ESAPI validation 失敗-start-----------");
                System.out.println(res);
                resultValidation = res; // 仍回傳
                System.out.println("###### ESAPI validation 失敗-end-------------");
            }

        } catch (IntrusionException e) {
            System.out.println("ESAPI validation failed");
            System.out.println(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ("".equals(endTime)) {
                endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            }

            if ("".equals(resultValidation)) {
                apsDao.insertApiLog(apiName, req, "-遠端未回傳資料，可能服務異常-", startTime, endTime);
            } else {
                apsDao.insertApiLog(apiName, req, resultValidation, startTime, endTime);
            }

        }
        return resultValidation;

    }

	public String getApplyPageUrl(String prodType) throws IOException {
		String result = "";

		if (StringUtils.isNotEmpty(prodType)){
			if ("CC".equals(prodType)){
				result = globalConfig.CrossSellCCUrl();
			} else if ("PL".equals(prodType)){
				result = globalConfig.CrossSellLoanPLUrl();
			} else if ("RPL".equals(prodType)){
				result = globalConfig.CrossSellLoanRPLUrl();
			}
		}

		return result;
	}
}
