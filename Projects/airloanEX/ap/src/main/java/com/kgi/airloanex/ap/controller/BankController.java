package com.kgi.airloanex.ap.controller;

import javax.servlet.http.HttpServletRequest;

import com.kgi.eopend3.ap.dao.ConfigDao;
import com.google.gson.Gson;
import com.kgi.airloanex.ap.service.BankService;
import com.kgi.eopend3.ap.controller.base.BaseController;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.dto.KGIHeader;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.view.NCCCView;
import com.kgi.eopend3.common.dto.respone.CheckOTPResp;
import com.kgi.eopend3.common.dto.respone.SendOTPResp;
import com.kgi.eopend3.common.dto.view.CheckOTPView;
import com.kgi.eopend3.common.dto.view.PCode2566View;
import com.kgi.eopend3.common.dto.view.SendOTPView;
import com.kgi.eopend3.common.util.CheckUtil;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * TABLES: CaseData, CaseAuth, Case
 * 
 */
@RestController
@RequestMapping("/bank")
public class BankController extends BaseController {

    @Autowired
    private BankService bankService;

    @Autowired
    private ConfigDao configDao;

    // @Autowired
    // private DropDownService dropDownService;

    @PostMapping("/sendOTP")
	public String sendOTP(HttpServletRequest request, @RequestBody String reqBody) {
		logger.info("--> sendOTP()" + reqBody);

		try {
			KGIHeader header = this.getHeader(request);
			String uniqId = header.getUniqId();
			String idno = header.getIdno();

			if (!ESAPI.validator().isValidInput("ValidationCtrl", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
				return WebResult.GetFailResult();
			}

			Gson gson = new Gson();
			SendOTPView view = gson.fromJson(reqBody, SendOTPView.class);
			if (!CheckUtil.check(view)) {
				return WebResult.GetResultString(1, configDao.ReadConfigValue("CONT_Err_Unexpected", "參數錯誤"), null);
			}
			SendOTPResp resp = bankService.getSessionKey(view, uniqId, idno); 
			if (!SystemConst.KGI_OTP_SUCESS.equals(resp.getCode())) {
				return WebResult.GetResultString(-1, resp.getMessage(), null);
			} else {
				return WebResult.GetResultString(0, "成功", resp);
			}
		} catch (IntrusionException e1) {
			return WebResult.GetResultString(9, configDao.ReadConfigValue("CONT_Err_Unexpected", "Invalid Input"), null);
		} catch (ErrorResultException er) {
			return WebResult.GetResultString(er.getStatus(), er.getMessage(), er.getResult());
		} catch (Exception e) {
			logger.error("未處理的錯誤", e);
			return WebResult.GetResultString(99, configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
		} finally {
			logger.info("<-- sendOTP()");
		}
	}

    @PostMapping("/checkOTP")
	public String checkOTP(HttpServletRequest request, @RequestBody String reqBody) {
		logger.info("--> checkOTP()" + reqBody);

		try {
			if (!ESAPI.validator().isValidInput("ValidationCtrl", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
				return WebResult.GetFailResult();
			}

			Gson gson = new Gson();
			CheckOTPView view = gson.fromJson(reqBody, CheckOTPView.class);
			if (!CheckUtil.check(view)) {
				return WebResult.GetResultString(1, configDao.ReadConfigValue("CONT_Err_Unexpected", "參數錯誤"), null);
			}

			KGIHeader header = this.getHeader(request);
			String uniqId = header.getUniqId();
			String idno = header.getIdno();
			String ipAddress = header.getIpAddress();

			CheckOTPResp resp = bankService.checkSessionKeyOtp(view, uniqId, idno, ipAddress); 
			return WebResult.GetResultString(0, "成功", resp);
		} catch (IntrusionException e) {
			return WebResult.GetResultString(9, configDao.ReadConfigValue("CONT_Err_Unexpected", "Invalid Input"), null);
		} catch (ErrorResultException er) {
			return WebResult.GetResultString(er.getStatus(), er.getMessage(), er.getResult());
		} catch (Exception e) {
			logger.error("未處理的錯誤", e);
			return WebResult.GetResultString(99, configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
		}
	}

    @PostMapping("/pcode2566")
    public String pcode2566(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("--> pcode2566()" + reqBody);

        try {
            if (!ESAPI.validator().isValidInput("BankController", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
                return WebResult.GetFailResult();
            }

            Gson gson = new Gson();
            PCode2566View view = gson.fromJson(reqBody, PCode2566View.class);
            if (!CheckUtil.check(view)) {
                return WebResult.GetResultString(1, "參數錯誤", null);
            }

            KGIHeader header = this.getHeader(request);
            String uniqId = header.getUniqId();
            String idno = header.getIdno();
            if (uniqId == null || "".equals(uniqId) || idno == null || "".equals(idno)) {
                return WebResult.GetResultString(1, "Header參數錯誤", null);
            }

            return bankService.checkPCode2566(uniqId, idno, view);
        } catch (IntrusionException e) {
            return WebResult.GetResultString(9, "Invalid Input", null);
        } catch (ErrorResultException er) {
            return WebResult.GetResultString(er.getStatus(), er.getMessage(), er.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        } finally {
            logger.info("<-- pcode2566()");
        }
    }

    @PostMapping("/nccc")
    public String nccc(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("nccc()" + reqBody);

        try {
			if (!ESAPI.validator().isValidInput("ValidationCtrl", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
				return WebResult.GetFailResult();
			}

            Gson gson = new Gson();
            NCCCView view = gson.fromJson(reqBody, NCCCView.class);
			if (!CheckUtil.check(view)) {
				return WebResult.GetResultString(1, "參數錯誤", null);
			}

            KGIHeader header = this.getHeader(request);
            String ulid = header.getUniqId() ;
            String idno = header.getIdno();
            
            if (ulid == null || "".equals(ulid) || idno == null || "".equals(idno))  {
				return WebResult.GetResultString(1, "Header參數錯誤", null);
            }
        	
              return bankService.verifyNCCC(ulid, idno, view);
        } catch (IntrusionException e) {
            return WebResult.GetResultString(9, "Invalid Input", null);
		} catch (ErrorResultException er) {
			return WebResult.GetResultString(er.getStatus(), er.getMessage(), er.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
    }
}
