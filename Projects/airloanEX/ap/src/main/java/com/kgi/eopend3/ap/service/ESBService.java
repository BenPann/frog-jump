package com.kgi.eopend3.ap.service;

import com.ibm.mq.MQException;
import com.ibm.tw.commons.net.mq.CMQStrMessage;
import com.ibm.tw.commons.net.mq.MQReceiver;
import com.ibm.tw.commons.net.mq.MQSender;
import com.ibm.tw.commons.util.ConvertUtils;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Service
public class ESBService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());


	@Autowired
	AirloanEXConfig globalConfig;

	public MQSender getALNSender() throws MQException {
        MQSender sender = new MQSender();
        logger.info("ALN Port : "+ Integer.parseInt(globalConfig.ALN_Port()));
        sender.setMQEnvironment(globalConfig.ALN_HostName(), Integer.parseInt(globalConfig.ALN_Port()) ,globalConfig.ALN_Channel());
        sender.connect(globalConfig.ALN_QueueManagerName(), globalConfig.ALN_QueueSendName());
        return sender;
    }

    public MQReceiver getALNReceiver() throws MQException {
        MQReceiver receiver = new MQReceiver();
        receiver.setMQEnvironment(globalConfig.ALN_HostName(), Integer.parseInt(globalConfig.ALN_Port()) ,globalConfig.ALN_Channel());
        receiver.connect(globalConfig.ALN_QueueManagerName(),globalConfig.ALN_QueueReceiveName());
        return receiver;
    }

	public String sendMessageQueue(String rqObject, MQSender sender, MQReceiver receiver) {
		String returnObject = null;
		if (sender == null || receiver == null) {
			throw new RuntimeException("尚未初始化Sender或Receier");
		}
		try {
			byte[] bytes = rqObject.getBytes("UTF8");
			CMQStrMessage message = new CMQStrMessage();
			message.setCCSID(1208);
			message.setMessageData(bytes);
			sender.send(message);
			CMQStrMessage returnMessage = (CMQStrMessage) receiver.receive(message.getMessageId(), 30000);

		    if (returnMessage == null) {
				return "遠端edda服務回應異常";
			}
			returnObject = ConvertUtils.bytes2Str(returnMessage.getMessageData(), "UTF8");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sender != null) {
				sender.close();
			}
			if (receiver != null) {
				receiver.close();
			}
		}
		return returnObject;
	}
    
}
