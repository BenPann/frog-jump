package com.kgi.airloanex.ap.controller;

import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.service.ValidationService;
import com.kgi.airloanex.common.PlContractConst.SituationPCode2566;
import com.kgi.eopend3.ap.controller.base.BaseController;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.ap.service.DropDownService;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.dto.KGIHeader;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.respone.CheckOTPResp;
import com.kgi.eopend3.common.dto.respone.SendOTPResp;
import com.kgi.eopend3.common.dto.view.CheckOTPView;
import com.kgi.eopend3.common.dto.view.EddaView;
import com.kgi.eopend3.common.dto.view.PCode2566View;
import com.kgi.eopend3.common.dto.view.SendOTPView;
import com.kgi.eopend3.common.util.CheckUtil;

import org.apache.commons.lang3.StringUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/validate")
public class ValidationCtrl extends BaseController {
	@Autowired
	ValidationService validationService;
	
	@Autowired
	private ConfigDao configDao;

	@Autowired
	private DropDownService dropDownService; 
	
	@PostMapping("/sendOTP")
	public String sendOTP(HttpServletRequest request, @RequestBody String reqBody) {
		logger.info("--> sendOTP()" + reqBody);

		try {
			KGIHeader header = this.getHeader(request);
			String uniqId = header.getUniqId();
			String idno = header.getIdno();

			if (!ESAPI.validator().isValidInput("ValidationCtrl", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
				return WebResult.GetFailResult();
			}

			Gson gson = new Gson();
			SendOTPView view = gson.fromJson(reqBody, SendOTPView.class);
			if (!CheckUtil.check(view)) {
				return WebResult.GetResultString(1, configDao.ReadConfigValue("CONT_Err_Unexpected", "參數錯誤"), null);
			}
			SendOTPResp resp = validationService.getSessionKey(view, uniqId, idno, SituationPCode2566.auth); 
			if (!SystemConst.KGI_OTP_SUCESS.equals(resp.getCode())) {
				return WebResult.GetResultString(-1, resp.getMessage(), null);
			} else {
				return WebResult.GetResultString(0, "成功", resp);
			}
		} catch (IntrusionException e1) {
			return WebResult.GetResultString(9, configDao.ReadConfigValue("CONT_Err_Unexpected", "Invalid Input"), null);
		} catch (ErrorResultException er) {
			return WebResult.GetResultString(er.getStatus(), er.getMessage(), er.getResult());
		} catch (Exception e) {
			logger.error("未處理的錯誤", e);
			return WebResult.GetResultString(99, configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
		} finally {
			logger.info("<-- sendOTP()");
		}
	}

	// @PostMapping("/nexttime")
	// public String nexttime(HttpServletRequest request, @RequestBody String reqBody) {
	// 	logger.info("--> nexttime()");
		
	// 	try {
	// 		if (!ESAPI.validator().isValidInput("ValidationCtrl", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
	// 			return WebResult.GetFailResult();
	// 		}

	// 		KGIHeader header = this.getHeader(request);
	// 		String uniqId = header.getUniqId();
			
	// 		return d3ApplyService.nexttime(uniqId) ;

	// 	} catch (IntrusionException e) {
	// 		return WebResult.GetResultString(9, "Invalid Input", null);
	// 	} catch (Exception e) {
	// 		logger.error("未處理的錯誤", e);
	// 		return WebResult.GetResultString(99, "系統錯誤", null);
	// 	}
		
		
	// }
	
	
	@PostMapping("/checkOTP")
	public String checkOTP(HttpServletRequest request, @RequestBody String reqBody) {
		logger.info("--> checkOTP()" + reqBody);

		try {
			if (!ESAPI.validator().isValidInput("ValidationCtrl", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
				return WebResult.GetFailResult();
			}

			Gson gson = new Gson();
			CheckOTPView view = gson.fromJson(reqBody, CheckOTPView.class);
			if (!CheckUtil.check(view)) {
				return WebResult.GetResultString(1, configDao.ReadConfigValue("CONT_Err_Unexpected", "參數錯誤"), null);
			}

			KGIHeader header = this.getHeader(request);
			String uniqId = header.getUniqId();
			String idno = header.getIdno();
			String ipAddress = header.getIpAddress();

			CheckOTPResp resp = validationService.checkSessionKeyOtp(view, uniqId, idno, ipAddress, SituationPCode2566.auth); 
			return WebResult.GetResultString(0, "成功", resp);
		} catch (IntrusionException e) {
			return WebResult.GetResultString(9, configDao.ReadConfigValue("CONT_Err_Unexpected", "Invalid Input"), null);
		} catch (ErrorResultException er) {
			return WebResult.GetResultString(er.getStatus(), er.getMessage(), er.getResult());
		} catch (Exception e) {
			logger.error("未處理的錯誤", e);
			return WebResult.GetResultString(99, configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
		}
	}
	
	/**
	 * 取得驗身總次數
	 * 
	 * @param request
	 * @return
	 */
    @GetMapping("/getVerifyCount")
    public String getVerifyCount(HttpServletRequest request) {
		logger.info("--> getVerifyCount()");
        try {
            KGIHeader header = this.getHeader(request);
            String ulid = header.getUniqId() ;
            return validationService.getVerifyCount(ulid);
        } catch (IntrusionException e) {
            return WebResult.GetResultString(9, "Invalid Input", null);
		} catch (ErrorResultException er) {
			return WebResult.GetResultString(er.getStatus(), er.getMessage(), er.getResult());
        } finally {
    		logger.info("<-- getVerifyCount()");
        }
    }

	/**
	 * 登入驗身時的 PCode2566
	 */
	@PostMapping("/pcode2566Auth")
	public String pcode2566Auth(HttpServletRequest request, @RequestBody String reqBody) {
		return pcode2566(request, reqBody, SituationPCode2566.auth);
	}

	/**
	 * 授扣款時的 PCode2566
	 */
	@PostMapping("/pcode2566Sign")
	public String pcode2566Sign(HttpServletRequest request, @RequestBody String reqBody) {
		return pcode2566(request, reqBody, SituationPCode2566.sign);
	}

	/**
	 * 原來功能僅有 ED3_CaseData.phone, ED3_IdentityVerification 
	 * 現由 ContractMain, ContractMainActInfo 取代
	 * 另外，有 IdentityVerify 擔任 DTO
	 * 最後，資料儲存會分為 Auth/Sign/Edda
	 * 
	 * @param request
	 * @param reqBody
	 * @param situation
	 * @return
	 */
	private String pcode2566(HttpServletRequest request, String reqBody, SituationPCode2566 situation) {

		logger.info("--> pcode2566()" + reqBody);
		try {
			if (!ESAPI.validator().isValidInput("ValidationCtrl", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
				return WebResult.GetFailResult();
			}

			Gson gson = new Gson();
			PCode2566View view = gson.fromJson(reqBody, PCode2566View.class);
			if (!CheckUtil.check(view)) { // phone, bank, account
				return WebResult.GetResultString(1, "參數錯誤", null);
			}

			KGIHeader header = this.getHeader(request);
			String uniqId = header.getUniqId();
			String idno = header.getIdno();
            if (StringUtils.isBlank(uniqId) || StringUtils.isBlank(idno))  {
				return WebResult.GetResultString(1, "Header參數錯誤", null);
			}

			String resultBankName = dropDownService.findPcode2566Bank(view.getBank());
			String[] ary = StringUtils.split(resultBankName, " ");
			if (ary != null && ary.length > 1) {
				view.setBankName(ary[1]); // 700 郵局
			} else {
				view.setBankName("");
			}

			return validationService.checkPCode2566(uniqId, idno, view, situation);
		} catch (IntrusionException e) {
			return WebResult.GetResultString(9, "Invalid Input", null);
		} catch (ErrorResultException er) {
			return WebResult.GetResultString(er.getStatus(), er.getMessage(), er.getResult());
		} catch (Exception e) {
			logger.error("未處理的錯誤", e);
			return WebResult.GetResultString(99, "系統錯誤", null);
		} finally {
			logger.info("<-- pcode2566()");
		}
	}

	/**
	 * 繳款資訊 驗證edda
	 */
	@PostMapping("/getVerifyEdda")
	public String getVerifyEdda(HttpServletRequest request, @RequestBody String reqBody) {
		logger.info("--> 繳款資訊Edda  " + reqBody);

		try{
           if (!ESAPI.validator().isValidInput("ValidationCtrl", reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
			   return WebResult.GetFailResult();
		   }

			Gson gson = new Gson();
			EddaView view = gson.fromJson(reqBody, EddaView.class);
			if (!CheckUtil.check(view)) { // bankIdPay, accountPay
				return WebResult.GetResultString(1, "參數錯誤", null);
			}

			KGIHeader header = this.getHeader(request);
			String uniqId = header.getUniqId();
			String idno = header.getIdno();
            if (StringUtils.isBlank(uniqId) || StringUtils.isBlank(idno))  {
				return WebResult.GetResultString(1, "Header參數錯誤", null);
            }
			
			String resultBankName = dropDownService.findEDDABank(view.getEddaBankId());
			String[] ary = StringUtils.split(resultBankName, " ");
			if (ary != null && ary.length > 1) {
				view.setEddaBankName(ary[1]); // 700 郵局
			} else {
				view.setEddaBankName("");
			}

			return validationService.checkEdda(uniqId, idno, view);

		} catch (IntrusionException e) {
			return WebResult.GetResultString(9, "Invalid Input", null);
		} catch (ErrorResultException er) {
			return WebResult.GetResultString(er.getStatus(), er.getMessage(), er.getResult());
		} catch (Exception e) {
			logger.error("未處理的錯誤", e);
			return WebResult.GetResultString(99, "系統錯誤", null);
		} finally {
			logger.info("<-- 繳款資訊Edda  ");
		}
	}

}
