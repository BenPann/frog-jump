package com.kgi.eopend3.ap.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class DataSourceConfig {

    @Autowired
    private StaticDBConfig staticDBConfig;

    @Autowired
    private OracleWDConfig oracleWDConfig;

    @Autowired
    private OracleDwstConfig oracleDwstConfig;

    @Autowired
    private G08Config g08Config;

    private final String datasourceName = "mainDB";

    @Bean("mainDB")
    @Primary
    public DataSource mainDataSource() {
        //引用參數建立datasource
        DataSource build = DataSourceBuilder.create().username(staticDBConfig.getUsername()).password(staticDBConfig.getPazzd())
                .url(staticDBConfig.getUrl()).driverClassName(staticDBConfig.getDriverClassName()).build();
        return build;
    }

    @Bean(name = "staticDBManager")
    @Primary
    public PlatformTransactionManager staticDBManager(@Qualifier(datasourceName) DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "oracleWD")
    public DataSource oracleWDDataSource() {
        DataSource build = DataSourceBuilder.create().username(oracleWDConfig.getUsername()).password(oracleWDConfig.getPazzd())
        .url(oracleWDConfig.getUrl()).driverClassName(oracleWDConfig.getDriverClassName()).build();
        return build;
    }

    @Bean(name = "oracleWDManager")
    public PlatformTransactionManager oracleWDManager(@Qualifier("oracleWD") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "oracleDwst")
    public DataSource oracleDwstDataSource() {
        DataSource build = DataSourceBuilder.create().username(oracleDwstConfig.getUsername()).password(oracleDwstConfig.getPazzd())
        .url(oracleDwstConfig.getUrl()).driverClassName(oracleDwstConfig.getDriverClassName()).build();
        return build;
    }

    @Bean(name = "oracleDwstManager")
    public PlatformTransactionManager oracleDwstManager(@Qualifier("oracleDwst") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "g08")
    public DataSource g08DataSource() {
        DataSource build = DataSourceBuilder.create().username(g08Config.getUsername()).password(g08Config.getPazzd())
        .url(g08Config.getUrl()).driverClassName(g08Config.getDriverClassName()).build();
        return build;
    }

    @Bean(name = "g08Manager")
    public PlatformTransactionManager g08Manager(@Qualifier("g08") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    // @Bean("kPDB01DB")
    // public DataSource kPDB01DBDataSource() {
    //     //引用參數建立datasource
    //     DataSource build = DataSourceBuilder.create().username(kPDB01DBConfig.getUsername()).password(kPDB01DBConfig.getPazzd())
    //             .url(kPDB01DBConfig.getUrl()).driverClassName(kPDB01DBConfig.getDriverClassName()).build();
    //     return build;
    // }
}
