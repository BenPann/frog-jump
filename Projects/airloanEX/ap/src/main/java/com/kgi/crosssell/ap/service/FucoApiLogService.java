package com.kgi.crosssell.ap.service;

import com.kgi.airloanex.ap.dao.FucoApiLogDao;
import com.kgi.airloanex.common.dto.db.FucoApiLog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FucoApiLogService {

    @Autowired
    private FucoApiLogDao fucoApiLogDao;

    public void insertFucoApiLog(String apiName, String request, String response) {

        FucoApiLog insertObj = new FucoApiLog();
        insertObj.setApiName(apiName);
        insertObj.setRequest(request);
        insertObj.setResponse(response);
        fucoApiLogDao.Create(insertObj);

    }

}