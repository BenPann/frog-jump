package com.kgi.eopend3.ap.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.kgi.airloanex.common.dto.db.DropdownData;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * TABLE: DropdownData 
 */
@Repository
public class DropdownDao extends CRUDQDao<DropdownData> {

    @Override
    public int Create(DropdownData fullItem) {
        return 0;
    }

    @Override
    public DropdownData Read(DropdownData keyItem) {
    	String sql = "SELECT * FROM DropdownData where enable=1 AND Name = ? AND DataKey = ? ";
	    try {
	        return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(DropdownData.class),
	                keyItem.getName(),keyItem.getDataKey());
	    } catch (DataAccessException ex) {
	        logger.error("DropdownData查無資料");
	        return null;
	    }
    }

    @Override
    public int Update(DropdownData fullItem) {
        return 0;
    }

    @Override
    public int Delete(DropdownData keyItem) {
        return 0;
    }

    @Override
    public List<DropdownData> Query(DropdownData keyItem) {
        String sql = "SELECT * from DropdownData where enable=1 AND Name = ? order by Sort ";
        try {
            return this.getJdbcTemplate().query(sql, new BeanPropertyRowMapper<>(DropdownData.class), new Object[]{keyItem.getName()});
        } catch (DataAccessException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public DropdownData readByNameAndDataName(String name ,String dataName) {
    	String sql = "SELECT * FROM DropdownData where enable=1 AND Name = ? AND DataName = ? ";
	    try {
	        return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(DropdownData.class), name, dataName);
	    } catch (DataAccessException ex) {
	        logger.error("DropdownData查無資料");
	        return null;
	    }
    }

	public DropdownData readByNameAndDataKey(String name ,String dataKey) {
    	String sql = "SELECT * FROM DropdownData where enable=1 AND Name = ? AND DataKey = ? ";
	    try {
	        return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(DropdownData.class), name, dataKey);
	    } catch (DataAccessException ex) {
	        logger.error("DropdownData查無資料");
	        return null;
	    }
    }
	
	/**
	 * 將下拉選單換成中文
	 */
    public String getDataNameByNameAndDataKey(String name, String dataKey) {
		name = name.trim();
		dataKey = dataKey.trim();
		List<Map<String, Object>> ls = this.getJdbcTemplate().queryForList(
				"SELECT DataName FROM DropdownData WHERE Name= ? AND DataKey = ? order by DataName, Sort", new Object[] { name, dataKey });
		if (ls.size() > 0) {
			return ls.get(0).get("DataName").toString();
		} else {
//			System.out.println("Dropdown Name = " + name + " DataKey = " + dataKey + " Not Found." );
			return "";
		}
	}
	
	public List<String> findDataKeysByName(String name) {
		List<String> ls = this.getJdbcTemplate().queryForList(
				"SELECT DataKey FROM DropdownData WHERE Name= ? order by DataName, Sort", new Object[] { name }, String.class);
		return ls;		
	}

	public String findDataKeysByNameAndDataName(String name, String dataName) {
		String rtn = "";
		try {
			rtn = this.getJdbcTemplate().queryForObject(
				"SELECT top 1 DataKey FROM DropdownData WHERE Name= ? AND DataName = ? order by DataName, Sort", new Object[] { name, dataName }, String.class);
		} catch(Exception e) {
			logger.error("DropdownData查無資料");
		}
		return rtn;		
	}
    
    /** 用ID取得DropdownData */
	public List<DropdownData> findByName(String name) {
		// 建立db連線及查詢字串
		return this.getJdbcTemplate().query(
				"SELECT Name, DataKey, DataName, Enable, Sort FROM DropdownData WHERE Enable = 1 and Name= ? ORDER BY Sort",
				new Object[] { name }, new BeanPropertyRowMapper<>(DropdownData.class));
	}

	/**
	 * @return 
	 * 從DB下載PDF套表所需的資料
	 */
	public List<DropdownData> loadDropdownData(){
		List<DropdownData> result = new ArrayList<DropdownData>();
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		
		try{
			result = jdbcTemplate.query("SELECT Name, DataKey, DataName, Enable, Sort FROM dbo.DropdownData", new BeanPropertyRowMapper<>(DropdownData.class));
		}catch (DataAccessException e){
//			 e.printStackTrace();
		}
		return result;
	}
}
