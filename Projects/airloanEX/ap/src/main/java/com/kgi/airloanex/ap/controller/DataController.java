package com.kgi.airloanex.ap.controller;

import javax.servlet.http.HttpServletRequest;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.eopend3.ap.controller.base.BaseController;
import com.kgi.eopend3.ap.service.DropDownService;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.respone.MaxVerifyCountResp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
// import org.owasp.esapi.ESAPI;
// import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/data")
public class DataController extends BaseController {

	// private static String context = "DataController";

	@Autowired
	private DropDownService dropDownService;

	@Autowired
	private AirloanEXConfig globalConfig;

	// /**
	// * 取得下拉選單內容
	// *
	// * @param reqBody
	// * @return
	// */
	// @PostMapping("/getDropDown")
	// public String getDropDownData(@RequestBody String reqBody) {
	// logger.info("前台輸入的資料" + reqBody);
	// if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson",
	// Integer.MAX_VALUE, false)) {
	// return dropDownService.getDropDownData(reqBody);
	// } else {
	// return WebResult.GetFailResult();
	// }
	// }

	/** 貸款PL資金用途清單 */
	@PostMapping("/moneypurpose")
	public String getMoneypurpose() {
		return dropDownService.getMoneypurpose();
	}

	/** 貸款學歷清單 */
	@PostMapping("/education")
	public String getEducation() {
		return dropDownService.getEducation();
	}

	/** 職業類別清單 */
	@PostMapping("/occupation")
	public String getOccupation() {
		return dropDownService.getOccupation();
	}

	/** 職務名稱 */
	@PostMapping("/occupationtitle")
	public String getOccupationtitle() {
		return dropDownService.getOccupationtitle();
	}

	/** 帳單寄送方式清單 */
	@PostMapping("/billtype")
	public String getBilltype() {
		return dropDownService.getBilltype();
	}

	/** 婚姻狀況 */
	@PostMapping("/marriage")
	public String getMarriage() {
		return dropDownService.getMarriage();
	}

	/** 不動產狀況 */
	@PostMapping("/estate")
	public String getEstate() {
		return dropDownService.getEstate();
	}

	/** 身分證換補發縣市 */
	@PostMapping("/idcardlocation")
	public String getIdcardlocation() {
		return dropDownService.getIdcardlocation();
	}

	/** pcode2566 撥款銀行列表 */
	@PostMapping("/pcode2566BankList")
	public String pcode2566BankList() {
		return dropDownService.getPcode2566BankList();
	}

	/** edda 授扣銀行列表 */
	@PostMapping("/eddaBankList")
	public String eddaBankList() {
		return dropDownService.getEddaBankList();
	}

	/** 二親等以內血親 */
	@PostMapping("/relationDegree")
	public String relationDegree() {
		return dropDownService.getRelationDegree();
	}

	/** 企業負責人：本人擔任負責人之企業資料, 配偶擔任負責人之企業資料 */
	@PostMapping("/relationDegreeCorp")
	public String relationDegreeCorp() {
		return dropDownService.getRelationDegreeCorp();
	}

	/** 填寫信用卡申請書頁 - 帳單類別 */
	@PostMapping("/billType")
	public String billType() {
		return dropDownService.getBillType();
	}

	/** 填寫信用卡申請書頁 - 帳單類別 */
	@PostMapping("/QR_ChannelDepartType")
	public String QR_ChannelDepartType() {
		return dropDownService.getQR_ChannelDepartType();
	}

	/**
	 * 取得NCCC和PCode2566最大驗身次數
	 * 
	 * @param request
	 * @return
	 */
	@GetMapping("/getMaxVerifyCount")
	public String getMaxVerifyCount(HttpServletRequest request) {

		int ncccTotalCount = 0;
		int pcode2566TotalCount = 0;
		MaxVerifyCountResp resp = new MaxVerifyCountResp();

		try {
			ncccTotalCount = Integer.valueOf(globalConfig.NcccTotalCount());
		} catch (Exception e) {
			ncccTotalCount = 0;
		}
		resp.setNcccTotalCount(ncccTotalCount);

		try {
			pcode2566TotalCount = Integer.valueOf(globalConfig.Pcode2566TotalCount());
		} catch (Exception e) {
			pcode2566TotalCount = 0;
		}
		resp.setPcode2566TotalCount(pcode2566TotalCount);

		return WebResult.GetResultString(0, "成功", resp);
	}
}