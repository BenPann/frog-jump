package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.PLCUs;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class PLCUsDao extends CRUDQDao<PLCUs> {

    @Override
    public int Create(PLCUs fullItem) {
        String sql = "INSERT INTO "
                + "PLContactUs ( UniqId ,UniqType ,Name ,Phone ,EMail ,ContactTime ,Note ,ProductId ,DepartId ,Member ,PrevPage ,ContactFlag ,UTime)"
                + "Values( :UniqId ,:UniqType ,:Name ,:Phone ,:EMail ,:ContactTime ,:Note ,:ProductId ,:DepartId ,:Member ,:PrevPage ,:ContactFlag ,getdate() );";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public PLCUs Read(PLCUs keyItem) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int Update(PLCUs fullItem) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int Delete(PLCUs keyItem) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public List<PLCUs> Query(PLCUs keyItem) {
        // TODO Auto-generated method stub
        return null;
    }

}
