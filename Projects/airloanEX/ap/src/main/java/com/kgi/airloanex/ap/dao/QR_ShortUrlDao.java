package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.eopend3.ap.dao.CRUDQDao;
import com.kgi.eopend3.common.dto.db.QR_ShortUrl;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class QR_ShortUrlDao  extends CRUDQDao<QR_ShortUrl> {

	@Override
	public int Create(QR_ShortUrl fullItem) {
		return 0;
	}

	@Override
	public QR_ShortUrl Read(QR_ShortUrl fullItem) {
		String sql = "select * FROM QR_ShortUrl where shortUrl=? ";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_ShortUrl.class),
                    new Object[] { fullItem.getShortUrl()});
        } catch (DataAccessException ex) {
//            System.out.println("QR_ShortUrl查無資料");
            return null;
        }
	}

	@Override
	public int Update(QR_ShortUrl fullItem) {
		return 0;
	}

	@Override
	public int Delete(QR_ShortUrl keyItem) {
		return 0;
	}

	@Override
	public List<QR_ShortUrl> Query(QR_ShortUrl keyItem) {
		return null;
	}

	public List<QR_ShortUrl> getByChannelId(String channelId) {
		String sql = " select * From QR_ShortUrl where ChannelId = ? ORDER BY CreateTime DESC ";
		try {
			return this.getJdbcTemplate().query(sql, new BeanPropertyRowMapper<>(QR_ShortUrl.class),new Object[] { channelId });
		}catch(DataAccessException ex) {
			return null;
		}
	}

	public QR_ShortUrl findByShortUrl(String shortUrl) {
		QR_ShortUrl fullItem = new QR_ShortUrl();
		fullItem.setShortUrl(shortUrl);
		return Read(fullItem);
	}
}