package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.WhiteListData;
import com.kgi.eopend3.ap.dao.CRUDQDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class WhiteListDataDao extends CRUDQDao<WhiteListData> {
    public static final String NAME = "WhiteListData";

    @Override
    public int Create(WhiteListData fullItem) {
        String sql = "INSERT INTO " + NAME
                + " (ID, PType, ApiName, Response) "
                + " VALUES (:ID, :PType, :ApiName, :Response)";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public WhiteListData Read(WhiteListData keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE ID = ? AND PType = ? AND ApiName = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(WhiteListData.class),
                    new Object[] { keyItem.getID(), keyItem.getPType(), keyItem.getApiName() });
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料");
            throw new ErrorResultException(9, NAME + "查無資料", ex);
        }
    }

    @Override
    public int Update(WhiteListData fullItem) {
        String sql = "UPDATE " + NAME
                + " SET  Response = :Response "
                + " WHERE ID=:ID AND PType=:PType AND ApiName=:ApiName";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(WhiteListData keyItem) {
        String sql = "DELETE FROM " + NAME + " WHERE ID = ? AND PType = ? AND ApiName = ?";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getID(), keyItem.getPType(), keyItem.getApiName() });
    }

    @Override
    public List<WhiteListData> Query(WhiteListData keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE ID = ? AND PType = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getID(), keyItem.getPType() },
                    new BeanPropertyRowMapper<>(WhiteListData.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }
	
	public String findWhiteListData(String inToken, String inKeyToken,String inKeyId) {
		
        String sql = "select Response FROM WhiteListData where apiname = ?";
        
        try {
            WhiteListData data = this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(WhiteListData.class),
                new Object[] { inToken });
            String resultStrFrom = data.getResponse();
			String resultStrTo = "";
			
			resultStrTo = resultStrFrom.replaceAll("%TOKEN%", inKeyToken);  
			resultStrTo = resultStrFrom.replaceAll("%ID_NO%", inKeyId);  

			return resultStrTo;
		} catch (DataAccessException ex) {
			System.out.println("#### 在 CaseData 查無對應資料.");
			return null;
		}
	}	
}