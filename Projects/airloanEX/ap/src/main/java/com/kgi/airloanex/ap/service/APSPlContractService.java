package com.kgi.airloanex.ap.service;

import java.util.List;
import java.util.Map;

import com.ibm.tw.commons.util.StringUtils;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.APSDao;
import com.kgi.airloanex.ap.dao.CaseAuthDao;
import com.kgi.airloanex.ap.dao.ContractApiLogDao;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.eopend3.ap.dao.HttpDao;
import com.kgi.eopend3.common.dto.respone.HttpResp;
import com.kgi.eopend3.common.util.DateUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.kgi.airloanex.common.PlContractConst.ExternalCallPostMan;

@Service
public class APSPlContractService {

    @Autowired
    AirloanEXConfig globalConfig;

    @Autowired
    HttpDao httpDao;

    @Autowired
    private APSDao apsDao;

    @Autowired
    CaseAuthDao caseAuthDao;

    @Autowired
    private ContractApiLogDao contractApiLogDao;    
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());  
    // ----------------------------------------------------------------------------------------------
    // DEC 決策平台
    public String queryDEC(String qryName, String queryJson,String keyInfo) throws Exception {
        String decUrl = globalConfig.DEC_ServerHost();
        return queryDEC(decUrl, qryName, queryJson,keyInfo);
    }

    public String queryDEC(String decUrl, String qryName, String queryJson,String keyInfo) throws Exception {
        // 1. 組合出 request 的 url
        HttpResp httpResp = new HttpResp();
        int statusCode = 500;
        String response = "";
        String startTime = "";
        String endTime = "";
        try {
            // 2. 發送 request
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            httpResp = this.httpDao.queryPost(decUrl, qryName, queryJson);
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            statusCode = httpResp.getStatusCode();
            response = httpResp.getResponse();
            if (statusCode != 200) {
                logger.error("呼叫 DEC 決策平台 失敗! " + decUrl.concat(qryName));
                throw new Exception("呼叫DEC Server失敗!!");
            }
        } catch(Exception e){
            logger.error("未知錯誤", e);
            throw new Exception();
        } finally {
            // 3. 寫ContractApiLog
            contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, keyInfo, qryName, queryJson, response, startTime, endTime);
        }
        // 4. 返回結果
        return response;
    }
    // ----------------------------------------------------------------------------------------------
    // ALN 理債平台
    public String queryALN(String qryName, String queryJson,String keyInfo) throws Exception {
        String alnUrl = globalConfig.ALN_ServerHost();
        return queryALN(alnUrl, qryName, queryJson,keyInfo);
    }

    public String queryALN(String alnUrl, String qryName, String queryJson,String keyInfo) throws Exception {
        // 1. 組合出 request 的 url
        HttpResp httpResp = new HttpResp();
        int statusCode = 500;
        String response = "";
        String startTime = "";
        String endTime = "";
		
        try {
            // 1.5. 寫ContractApiLog
			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			if (StringUtils.equals("/KGI/CREATE_NEW_CASE", qryName)){
				contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, keyInfo, qryName + "_start", queryJson, "start @ " + startTime, startTime, startTime);				
			}

            // 2. 發送 request
         
            httpResp = this.httpDao.queryPatch(alnUrl, qryName, queryJson);

            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            statusCode = httpResp.getStatusCode();
            response = httpResp.getResponse();
            if (statusCode != 200) {
                logger.error("呼叫 ALN 理債平台 失敗! " + alnUrl.concat(qryName));
                throw new Exception("呼叫ALN Server失敗!!");
            }
        } catch(Exception e){
            logger.error("未知錯誤", e);
            throw new Exception();
        } finally {
            // 3. 寫ContractApiLog
            contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, keyInfo, qryName, queryJson, response, startTime, endTime);
        }
        // 4. 返回結果
        return response;
    }
    // ----------------------------------------------------------------------------------------------
    // ALN EXP # 額度利率體驗PL計算用 API 呼叫用的HOST
    public String queryALN_EXP(String qryName, String queryJson,String keyInfo) throws Exception {
        String alnExpUrl = globalConfig.ALN_ServerHost_EXP();
        return queryALN_EXP(alnExpUrl, qryName, queryJson,keyInfo);
    }

    public String queryALN_EXP(String alnExpUrl, String qryName, String queryJson,String keyInfo) throws Exception {
        // 1. 組合出 request 的 url
        HttpResp httpResp = new HttpResp();
        int statusCode = 500;
        String response = "";
        String startTime = "";
        String endTime = "";
        try {
            // 2. 發送 request
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            httpResp = this.httpDao.queryPatch(alnExpUrl, qryName, queryJson);

            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            statusCode = httpResp.getStatusCode();
            response = httpResp.getResponse();
            if (statusCode != 200) {
                logger.error("呼叫 ALN EXP 理債平台 失敗! " + alnExpUrl.concat(qryName));
                throw new Exception("呼叫ALN EXP Server失敗!!");
            }
        } catch(Exception e){
            logger.error("未知錯誤", e);
            throw new Exception();
        } finally {
            // 3. 寫ContractApiLog
            contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, keyInfo, qryName, queryJson, response, startTime, endTime);
        }
        // 4. 返回結果
        return response;
    }
    // ----------------------------------------------------------------------------------------------
    // APS 貸款徵審系統
    public String queryAPS(String qryName, String queryJson,String keyInfo) throws Exception {
        String apsUrl = globalConfig.APS_ServerHost();
        return queryAPS(apsUrl, qryName, queryJson,keyInfo);
    }

    public String queryAPS(String apsUrl, String qryName, String queryJson,String keyInfo) throws Exception {
        // 1. 組合出 request 的 url
        HttpResp httpResp = new HttpResp();
        int statusCode = 500;
        String response = "";
        String startTime = "";
        String endTime = "";
        try {
            // 2. 發送 request
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            httpResp = this.httpDao.queryPost(apsUrl, qryName, queryJson);
            statusCode = httpResp.getStatusCode();
            response = httpResp.getResponse();
            if (statusCode != 200) {
                logger.error("呼叫 APS 貸款徵審系統 失敗! " + apsUrl.concat(qryName));
                throw new Exception("呼叫APS Server失敗!!");
            }
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
        } catch(Exception e){
            logger.error("未知錯誤", e);
            throw new Exception();
        } finally {
            // 3. 寫ContractApiLog
            contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, keyInfo, qryName, queryJson, response, startTime, endTime);
        }
        // 4. 返回結果
        return response;
    }
    // ----------------------------------------------------------------------------------------------
    // CRP 黃頁平台
    public String queryCRP(String qryName, String queryJson,String keyInfo) throws Exception {
        String crpUrl = globalConfig.CRP_ServerHost();
        return queryCRP(crpUrl, qryName, queryJson,keyInfo);
    }

    public String queryCRP(String crpUrl, String qryName, String queryJson,String keyInfo) throws Exception {
        // 1. 組合出 request 的 url
        HttpResp httpResp = new HttpResp();
        int statusCode = 500;
        String response = "";
        // String startTime = "";
        // String endTime = "";
        try {
            // 2. 發送 request
            // startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            httpResp = this.httpDao.queryPost(crpUrl, qryName, queryJson);
            statusCode = httpResp.getStatusCode();
            response = httpResp.getResponse();
            if (statusCode != 200) {
                logger.error("呼叫 CRP 黃頁平台 失敗! " + crpUrl.concat(qryName));
                throw new Exception("呼叫CRP Server失敗!!");
            }
            // endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
        } catch(Exception e){
            logger.error("未知錯誤", e);
            throw new Exception();
        } finally {
            // 3. 寫ContractApiLog Charles:黃頁不寫 ContractApiLog demand by Ben
            // contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, keyInfo, qryName, queryJson, response, startTime, endTime);
        }
        // 4. 返回結果
        return response;
    }
    // ----------------------------------------------------------------------------------------------
    // SMS 報表簡訊平台(SMS)
    public String querySMS(String qryName, String queryJson,String keyInfo) throws Exception {
        String smsUrl = globalConfig.SMS_ServerHost();
        return querySMS(smsUrl, qryName, queryJson,keyInfo);
    }

    public String querySMS(String smsUrl, String qryName, String queryJson,String keyInfo) throws Exception {
        // 1. 組合出 request 的 url
        HttpResp httpResp = new HttpResp();
        int statusCode = 500;
        String response = "";
        String startTime = "";
        String endTime = "";
        try {
            // 2. 發送 request
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            httpResp = this.httpDao.queryPost(smsUrl, qryName, queryJson);

            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            statusCode = httpResp.getStatusCode();
            response = httpResp.getResponse();
            if (statusCode != 200) {
                logger.error("呼叫 SMS 報表簡訊平台(SMS) 失敗! " + smsUrl.concat(qryName));
                throw new Exception("呼叫SMS Server失敗!!");
            }
        } catch(Exception e){
            logger.error("未知錯誤", e);
            throw new Exception();
        } finally {
            // 3. 寫ContractApiLog
            contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, keyInfo, qryName, queryJson, response, startTime, endTime);
        }
        // 4. 返回結果
        return response;
    }
    // ----------------------------------------------------------------------------------------------
    // OCR 身份證正反及財力證明
    public String queryOCR(String qryName, String queryJson,String uniqId) throws Exception {
        // 1. 組合出 request 的 url
        String apiurl = globalConfig.OCR_ServerHost().concat(qryName);
        String startTime = "";
        String endTime = "";
        String rep = "";
        try {
            // 2. 發送 request
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");

            logger.info("*************"+ qryName +" Start Time***************" + startTime);
            rep = httpDao.doJsonPostRequest(apiurl, queryJson);
            logger.info(rep);
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");            
            logger.info("*************"+ qryName +" End Time***************" + endTime);      
            return rep;
        } catch (Exception e) {
            throw new Exception();
        } finally {
            // 3. 寫ContractApiLog
            contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, uniqId, qryName, queryJson, rep, startTime, endTime);
            
        }
       
    }
    // ----------------------------------------------------------------------------------------------
    // APIM 戶役政
    public String queryAPIM(String qryName, String queryJson, String keyInfo) throws Exception {
        String apimUrl = globalConfig.APIM_ServerHost();
        return queryAPIM(apimUrl, qryName, queryJson, keyInfo);
    }

    public String queryAPIM(String apimUrl, String qryName, String queryJson, String keyInfo) throws Exception {
        // 1. 組合出 request 的 url
        HttpResp httpResp = new HttpResp();
        int statusCode = 500;
        String response = "";
        String startTime = "";
        String endTime = "";
        try {
            // 2. 發送 request
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            httpResp = this.httpDao.queryPost(apimUrl, qryName, queryJson);
            
            statusCode = httpResp.getStatusCode();
            response = httpResp.getResponse();
            if (statusCode != 200) {
                logger.error("呼叫 APIM 戶役政 失敗! " + apimUrl.concat(qryName));
                throw new Exception("呼叫APIM Server失敗!!");
            }
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");            
        } catch (Exception e) {
            logger.error("未知錯誤", e);
            throw new Exception();
        } finally {
            // 3. 
            if (StringUtils.equals(ExternalCallPostMan, keyInfo)) {
                // 寫ApiLog
                apsDao.insertApiLog("/KGI/GET_APIM_DATA", queryJson, response, startTime, endTime);
            } else {
                // 寫ContractApilog
                contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, keyInfo, qryName, queryJson, response, startTime, endTime);
            }
        }
        // 4. 返回結果
        return response;
    }
    public String queryAPIMForCS(String qryName, String queryJson, String keyInfo) throws Exception {
        String apimUrl = globalConfig.APIM_ServerHost();
        return queryAPIMForCS(apimUrl, qryName, queryJson, keyInfo);
    }

    public String queryAPIMForCS(String apimUrl, String qryName, String queryJson, String keyInfo) throws Exception {
        // 1. 組合出 request 的 url
        HttpResp httpResp = new HttpResp();
        int statusCode = 500;
        String response = "";
        String startTime = "";
        String endTime = "";
        try {
            // 2. 發送 request
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            httpResp = this.httpDao.queryPostForCS(apimUrl, qryName, queryJson);
            
            statusCode = httpResp.getStatusCode();
            response = httpResp.getResponse();
            if (statusCode != 200) {
                logger.error("呼叫 APIM 戶役政 失敗! " + apimUrl.concat(qryName));
                throw new Exception("呼叫APIM Server失敗!!");
            }
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");            
        } catch (Exception e) {
            logger.error("未知錯誤", e);
            throw new Exception();
        } finally {
            // 3. 
            if (StringUtils.equals(ExternalCallPostMan, keyInfo)) {
                // 寫ApiLog
                apsDao.insertApiLog("/KGI/GET_APIM_DATA", queryJson, response, startTime, endTime);
            } else {
                // 寫ContractApilog
                contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, keyInfo, qryName, queryJson, response, startTime, endTime);
            }
        }
        // 4. 返回結果
        return response;
    }
    // ----------------------------------------------------------------------------------------------
    /**
     * 
     * 回傳值
     * 1 - OTP
     * 2 - MBC
     * 3 - Nccc/信用卡
     * 4 - Pcode2566/銀行帳戶
     * 5 - 線下驗身 / 不驗身 / 預約分行
     * */
    public String externalCheckTypeForAddNewCase(String caseNo) {
        List<Map<String, Object>> verifyTypeFroAddNewCase = caseAuthDao.getVerifyTypeFroAddNewCase(caseNo);
        if (verifyTypeFroAddNewCase == null || verifyTypeFroAddNewCase.size() < 1) {
            return "";
        }
        
        //如果有過信用卡或是銀行驗證，優先使用，如果沒過，則找是否為MBC或是OTP驗身
        String verifyStatus = String.valueOf(verifyTypeFroAddNewCase.get(0).get("VerifyStatus"));
        if (verifyStatus.equals("1")) {
            String verifyType = String.valueOf(verifyTypeFroAddNewCase.get(0).get("VerifyType"));
            switch (verifyType) {
                case PlContractConst.CREDITVERIFY_TYPE_NCCC:
                    return "3";
                case PlContractConst.CREDITVERIFY_TYPE_BANKACOUNT:
                    return "4";
                case PlContractConst.CREDITVERIFY_TYPE_CHTMBC:
                	return PlContractConst.CREDITVERIFY_TYPE_CHTMBC;
                case PlContractConst.CREDITVERIFY_TYPE_NOTVERIFY:
                	return "5" ;
                case PlContractConst.CREDITVERIFY_9:
                    return "1";
                default:
                	return "1";
            }
        } else {
            String authType = String.valueOf(verifyTypeFroAddNewCase.get(0).get("AuthType"));
            switch (authType){
//                case"1":
//                    return "OTP";
                case PlContractConst.CREDITVERIFY_TYPE_CHTMBC:
                    return PlContractConst.CREDITVERIFY_TYPE_CHTMBC;
                default:
                	return "1";
            }
        }

//        return "";
	}

    /**
     * 身份認證(即驗身方式)
     * <ul>
     * <li>2 MBC</li>
     * <li>3 他行卡驗身</li>
     * <li>4 他行帳戶驗身</li>
     * <li>5 線下驗身</li>
     * <li>  OTP</li>
     * </ul>
     */
    public String transAuthType(String authType) {
        switch (authType) {
        case PlContractConst.CREDITVERIFY_TYPE_CHTMBC:
        	return PlContractConst.CREDITVERIFY_CHTMBC;
        case "3":
            return PlContractConst.CREDITVERIFY_NCCC;
        case "4":
            return PlContractConst.CREDITVERIFY_BANKACOUNT;
        case "5":
            return PlContractConst.CREDITVERIFY_NOTVERIFY;
        default: // 通常是 1
        	return PlContractConst.CREDITVERIFY_OTP;
        }
    	
    }

}
