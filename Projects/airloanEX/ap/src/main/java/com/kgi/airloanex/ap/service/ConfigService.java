package com.kgi.airloanex.ap.service;

import java.util.List;

import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.common.dto.db.Config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfigService {

	@Autowired
	private ConfigDao configDao;
	
	public Config Read(Config keyItem) {
		return configDao.Read(keyItem);
	}
	
	public List<Config> Query(Config keyItem) {
		return configDao.Query(keyItem);
	}
	
	public int Create(Config fullItem) {
		return configDao.Create(fullItem);
	}
	
	public Config queryConfigMultipleValue(Config keyItem, String spliter) {
        if (spliter == null) {
            spliter = ",";
        }
        List<Config> configs = this.Query(keyItem);
        Config rs = new Config();
        String keyValue = "";
        if(configs!=null) {
            for (Config config : configs) {
                keyValue = keyValue.equals("") ? config.getKeyValue() : keyValue.concat(spliter).concat(config.getKeyValue());
            }
            rs.setKeyValue(keyValue);
        }
        return rs;
    }
}