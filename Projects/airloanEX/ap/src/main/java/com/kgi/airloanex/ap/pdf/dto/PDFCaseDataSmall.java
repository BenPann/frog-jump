package com.kgi.airloanex.ap.pdf.dto;

public class PDFCaseDataSmall {
    
    // TABLE: CaseData
    private String CaseNo;
    private String case_no_est;
    private String Idno;
    private String customer_name;
    private String CaseNoWeb;
    private String IsPreAudit;
    private String Occupation;
    private String title;
    private String ProductId;

    // TABLE: CreditCaseData
    private String UniqType;

    public String getCaseNo() {
        return CaseNo;
    }

    public void setCaseNo(String caseNo) {
        CaseNo = caseNo;
    }

    public String getCase_no_est() {
        return case_no_est;
    }

    public void setCase_no_est(String case_no_est) {
        this.case_no_est = case_no_est;
    }

    public String getIdno() {
        return Idno;
    }

    public void setIdno(String idno) {
        Idno = idno;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCaseNoWeb() {
        return CaseNoWeb;
    }

    public void setCaseNoWeb(String caseNoWeb) {
        CaseNoWeb = caseNoWeb;
    }

    public String getIsPreAudit() {
        return IsPreAudit;
    }

    public void setIsPreAudit(String isPreAudit) {
        IsPreAudit = isPreAudit;
    }

    public String getOccupation() {
        return Occupation;
    }

    public void setOccupation(String occupation) {
        Occupation = occupation;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUniqType() {
        return UniqType;
    }

    public void setUniqType(String uniqType) {
        UniqType = uniqType;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }
}
