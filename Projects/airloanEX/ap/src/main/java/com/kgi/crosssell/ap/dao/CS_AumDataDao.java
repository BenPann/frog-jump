package com.kgi.crosssell.ap.dao;

import java.util.List;

import com.kgi.crosssell.common.dto.CS_AumData;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class CS_AumDataDao extends CRUDQDao<CS_AumData> {

	@Override
	public int Create(CS_AumData fullItem) {
		String sql = "INSERT INTO CS_AumData VALUES(?,?,?,?,?,?,?,?,getdate(),getdate(),?,?)";
		return this.getJdbcTemplate().update(sql,
				new Object[] { fullItem.getUniqId(), fullItem.getUniqType(), fullItem.getCurrency(),
						fullItem.getCurrentStock(), fullItem.getFinancingBalance(), fullItem.getBorrowStockBalance(),
						fullItem.getTotalFinancial(), fullItem.getExchangeRate(), fullItem.getStockBaseDate(),
						fullItem.getStockPriceDate() });

	}

	@Override
	public CS_AumData Read(CS_AumData keyItem) {
		String sql = "SELECT * FROM CS_AumData WHERE UniqId = ? AND Currency = ?";
		try {
			return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CS_AumData.class),
					new Object[] { keyItem.getUniqId(), keyItem.getCurrency() });
		} catch (DataAccessException ex) {

			System.out.println("#### " + keyItem.getUniqId() + " 在 CS_AumData 查無對應資料.");
			return null;
		}
	}

	@Override
	public int Update(CS_AumData fullItem) {
		String sql = "UPDATE CS_AumData SET ExchangeRate = ? , UpdateTime = getdate() WHERE UniqId = ? AND Currency = ?";
		return this.getJdbcTemplate().update(sql,
				new Object[] { fullItem.getExchangeRate(), fullItem.getUniqId(), fullItem.getCurrency() });
	}

	@Override
	public int Delete(CS_AumData keyItem) {
		String sql = "DELETE CS_AumData WHERE UniqId = ? AND Currency = ?";
		return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getUniqId(), keyItem.getCurrency() });
	}

	@Override
	public List<CS_AumData> Query(CS_AumData keyItem) {
		String sql = "SELECT * FROM CS_AumData WHERE UniqId = ? AND 1=1";
		try {
			return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getUniqId() },
					new BeanPropertyRowMapper<>(CS_AumData.class));
		} catch (DataAccessException ex) {
			return null;
		}
	}
}
