package com.kgi.airloanex.ap.dao;

import java.util.List;
import java.util.Map;

import com.kgi.eopend3.ap.dao.BaseDao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.AccessReferenceMap;
import org.owasp.esapi.reference.RandomAccessReferenceMap;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ChtDao extends BaseDao {
    /**
     * 新增中華電信信評資料
     */
	private static final Logger logger = LogManager.getLogger(ChtDao.class.getName());
	
    public void CreateCHT_PreFill(String uniqId, String uniqType, String name, String sex, String birthday,
            String homeAdress, String mobile, String email, String localPhone, String billAddress) {
    	try {
	    	AccessReferenceMap<String> map = new RandomAccessReferenceMap();
	        String indirect_uniqId = map.addDirectReference(uniqId);
	        uniqId = map.getDirectReference(indirect_uniqId);
	        String indirect_uniqType = map.addDirectReference(uniqType);
	        uniqType = map.getDirectReference(indirect_uniqType);
	        String indirect_name = map.addDirectReference(name);
	        name = map.getDirectReference(indirect_name);
	        String indirect_sex = map.addDirectReference(sex);
	        sex = map.getDirectReference(indirect_sex);
	        String indirect_birthday = map.addDirectReference(birthday);
	        birthday = map.getDirectReference(indirect_birthday);
	        String indirect_homeAdress = map.addDirectReference(homeAdress);
	        homeAdress = map.getDirectReference(indirect_homeAdress);
	        String indirect_mobile = map.addDirectReference(mobile);
	        mobile = map.getDirectReference(indirect_mobile);
	        String indirect_email = map.addDirectReference(email);
	        email = map.getDirectReference(indirect_email);
	        String indirect_localPhone = map.addDirectReference(localPhone);
	        localPhone = map.getDirectReference(indirect_localPhone);
	        String indirect_billAddress = map.addDirectReference(billAddress);
	        billAddress = map.getDirectReference(indirect_billAddress);
	    	
	        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        
            jdbcTemplate.update("INSERT INTO CHT_PreFill VALUES(?,?,?,?,?,?,?,?,?,?,getdate())",
                    new Object[] { uniqId, uniqType, name, sex, birthday, homeAdress, mobile, email, localPhone,
                            billAddress });
        } catch (DataAccessException e) {
        	logger.info("Exception was happened", e);
        } catch (Exception e) {
        	logger.info("Exception was happened", e);
        }
    }

    /**
     * 新增中華電信個人資料
     */
    public void CreateCHT_CreditRating(String uniqId, String uniqType, String customerPhone, String customerIdcard,
            String processyyymm, String isOldCustomer, String creditValue, String paymentBehaviorBeyond14,
            String paymentBehaviorBeyond30, String paymentMethod, String isSuspended, String isRevoked,
            String isRevoked6mon, String demandLetter, String debtRank, String hasDebt, String isRiskCustomer,
            String customerType, String hasDesignatedManager, String maxTenureMonth, String dataUsage,
            String voiceCallCnt, String voiceCallMin, String voSendTotNum, String workCity, String homeCity, String hasBadRecord, String hasData) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        try {
            jdbcTemplate.update("INSERT INTO CHT_CreditRating VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getdate())",
                    new Object[] { uniqId, uniqType, customerPhone, customerIdcard,
                            processyyymm, isOldCustomer, creditValue, paymentBehaviorBeyond14, 
                            paymentBehaviorBeyond30, paymentMethod, isSuspended, isRevoked, 
                            isRevoked6mon, demandLetter, debtRank, hasDebt, isRiskCustomer, 
                            customerType, hasDesignatedManager, maxTenureMonth, dataUsage, 
                            voiceCallCnt, voiceCallMin, voSendTotNum, workCity, homeCity, hasBadRecord, hasData });
        } catch (DataAccessException e) {
//            e.printStackTrace();
        }
    }

	public String hasdata(String  caseno) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        String sql = "SELECT UniqId,hasdata FROM CHT_CreditRating WHERE UniqId = ?;";
        List<Map<String, Object>>  ls = jdbcTemplate.queryForList(sql, new Object[] { caseno });
        if(ls.size()> 0){
                return ls.get(0).get("hasdata") == null? "0" :ls.get(0).get("hasdata").toString();
        }else{
                return "0";
        }
	}

	//2019/12/03 ben說轉正案後不用在call
	public List<Map<String,Object>> QueryCHTCreditRating(String uniqId,String idno){
                JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
//        String sql = "select * from CHT_CreditRating where CustomerIdcard=? and convert(varchar,CreateTime,111)>convert(varchar,dateadd(dd,-30,getdate()),111) order by  CreateTime desc";
        String sql = "insert into CHT_CreditRating (UniqId, UniqType, CustomerPhone, CustomerIdcard, Processyyymm, IsOldCustomer, CreditValue, " +
                "                              PaymentBehaviorBeyond14, PaymentBehaviorBeyond30, PaymentMethod, IsSuspended, IsRevoked, " +
                "                              IsRevoked6mon, DemandLetter, DebtRank, HasDebt, IsRiskCustomer, CustomerType, " +
                "                              HasDesignatedManager, MaxTenureMonth, DataUsage, VoiceCallCnt, VoiceCallMin, VoSendTotNum, " +
                "                              WorkCountry, HomeCountry, HasBadRecord, HasData, " +
                "                              CreateTime) output inserted.uniqid,inserted.UniqType,inserted.CustomerPhone ,inserted.CustomerIdcard ," +
                "                                                 inserted.Processyyymm ,inserted.IsOldCustomer, inserted.CreditValue ,inserted.PaymentBehaviorBeyond14 ," +
                "                                                inserted.PaymentBehaviorBeyond30 ,inserted.PaymentMethod , inserted.IsSuspended ," +
                "                                                inserted.IsRevoked ,inserted.IsRevoked6mon ,inserted.DemandLetter ,inserted.DebtRank ,inserted.HasDebt ," +
                "                                                inserted.IsRiskCustomer ,inserted.CustomerType ,inserted.HasDesignatedManager ,inserted.MaxTenureMonth ,inserted.DataUsage ," +
                "                                                 inserted.VoiceCallCnt ,inserted.VoiceCallMin ,inserted.VoSendTotNum ,inserted.WorkCountry ,inserted.HomeCountry , inserted.HasBadRecord , inserted.HasData , inserted.CreateTime " +
                " select top 1 " +
                "                   ?, " +
                "                   UniqType, " +
                "                   CustomerPhone, " +
                "                   CustomerIdcard, " +
                "                   Processyyymm, " +
                "                   IsOldCustomer, " +
                "                   CreditValue, " +
                "                   PaymentBehaviorBeyond14, " +
                "                   PaymentBehaviorBeyond30, " +
                "                   PaymentMethod, " +
                "                   IsSuspended, " +
                "                   IsRevoked, " +
                "                   IsRevoked6mon, " +
                "                   DemandLetter, " +
                "                   DebtRank, " +
                "                   HasDebt, " +
                "                   IsRiskCustomer, " +
                "                   CustomerType, " +
                "                   HasDesignatedManager, " +
                "                   MaxTenureMonth, " +
                "                   '1', " +
                "                   VoiceCallCnt, " +
                "                   VoiceCallMin, " +
                "                   VoSendTotNum, " +
                "                   WorkCountry, " +
                "                   HomeCountry, " +
                "                   HasBadRecord, " +
                "                   HasData, " +
                "                   CreateTime " +
                "        from CHT_CreditRating " +
                "        where CustomerIdcard = ? and convert(varchar,CreateTime,111)>=convert(varchar,dateadd(dd,-30,getdate()),111) " +
                "        order by CreateTime desc";
        try {
            return jdbcTemplate.queryForList(sql,uniqId,idno);
        }catch (DataAccessException e){
            e.printStackTrace();
            return null;
        }
    }
	
	
	
	/**
	 * 查詢來自中華電信的個資, 取最新的一筆資料
	 * @param uniqId
	 * @return
	 */
	public Map<String, Object> queryChtPreFill(String uniqId){
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        String sql = "SELECT TOP 1 * FROM CHT_PreFill cpf WHERE UniqId = ? ORDER BY cpf.CreateTime DESC;";
        return jdbcTemplate.queryForMap(sql, new Object[] { uniqId });
        
	}
}
