package com.kgi.airloanex.ap.pdf;

import static com.kgi.airloanex.common.PlContractConst.DOCUMENT_TYPE_CONTRACT_PDF;
import static com.kgi.airloanex.common.PlContractConst.UNIQ_TYPE_03;

import java.io.IOException;
import java.util.List;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.CaseDocumentDao;
import com.kgi.airloanex.ap.dao.ContractDocDao;
import com.kgi.airloanex.ap.dao.ContractMainActInfoDao;
import com.kgi.airloanex.ap.dao.ContractMainDao;
import com.kgi.airloanex.ap.service.SendMailHunterService;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.db.CaseDocument;
import com.kgi.airloanex.common.dto.db.ContractMain;
import com.kgi.airloanex.common.dto.db.ContractMainActInfo;
import com.kgi.airloanex.common.util.ExportUtil;
import com.kgi.eopend3.ap.config.GlobalConfig;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class ContractFinalePDF extends BasePDFDocument {
    
    private static final Logger logger = LogManager.getLogger(CasePDF.class.getName());

    private String _uniqId = "";

    @Autowired
    private GlobalConfig global;
    
	@Autowired
	private AirloanEXConfig globalConfig;

	@Autowired
	private ContractMainDao contDao;

	@Autowired
	private ContractMainActInfoDao contractMainActInfoDao;

	@Autowired
	private ContractDocDao contractDao;
    
    @Autowired
    private CaseDocumentDao caseDocumentDao;

	@Autowired
	private SendMailHunterService sendMailHunterService;

    @Autowired
    private PageFooter footer;
    
    private String templateVersion = "";

    private ApplicationContext appContext;

    private ContractPDF contractPDF;
    private ContractAchPDF contractAchPDF;

	/** 主檔 - 案件契約資料表(ContractMain) */
	private ContractMain contractMain;
	/** 立約帳戶驗身資料表(ContractMainActInfo) */
	private ContractMainActInfo contractMainActInfo;

    public ContractFinalePDF(ApplicationContext appContext) {
        super(appContext);
        this.appContext = appContext;
    }

    public void init(String... params) throws IOException {
        this._uniqId = params[0];
        // 是否加密
        this.setUseEncrepyt(true);
        // this.setEncryptPassword(idno);
        // 是否使用浮水印
        this.setUseWaterMark(true);
		this.setWaterMarkText(globalConfig.IMG_WATER_FONT());
		this.setWaterMarkFont(globalConfig.PDF_FONT_WATER_TTF());
        // 是否要PDF轉成圖片
        this.setPdfToImage(false);
        // 是否存DB
        this.setUseSaveToDB(true);
        // 是否發信
        this.setUseSendEMail(true);
        // 是否設定每頁的事件
        this.setPageEvent(footer);

        try {
            this.contractMain = contDao.ReadByUniqId(_uniqId);
            this.contractMainActInfo = contractMainActInfoDao.ReadByUniqId(_uniqId);
            
            // 1. Contract
            String param1 = "false";
            if(params != null && params.length > 1 && params[1] != null){
                param1 = params[1];
            }
            initContractPDF(param1);
            // 2. Ach
            if (isACH()) {
                initContractAchPDF();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initContractPDF(String param1) throws IOException {
        this.contractPDF = new ContractPDF(appContext);
        this.contractPDF.init(this._uniqId, param1);
    }

    private void initContractAchPDF() throws IOException {
        this.contractAchPDF = new ContractAchPDF(appContext);
        this.contractAchPDF.init(this._uniqId);
    }

    @Override
    public void beforePdfProcess() {
        
        StringBuilder sb = new StringBuilder();
        // 1. Contract
        concatRawHtml(sb, this.contractPDF); // finish setRawHtml()
        // 2. Ach
        if (isACH()) {
            concatRawHtml(sb, this.contractAchPDF); // finish setRawHtml()
        }

        String raw = sb.toString();
        this.setRawHtml(raw);
        templateVersion = this.contractPDF.templateVersion;

        this.setEncryptPassword(this.contractMain.getIdno());
    }

    private void concatRawHtml(StringBuilder sb, BasePDFDocument pdfDocument) {
        if (pdfDocument != null) {
            try{
                pdfDocument.beforePdfProcess();
                String raw = pdfDocument.getRawHtml();
                sb.append(raw);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        return;
    }

    // Charles:傳什麼 raw 值都沒差
    @Override
    public String replaceField(String raw) {
        StringBuilder sb = new StringBuilder();
        // 1. Contract
        concatDone(sb, this.contractPDF);
        // 2. Ach
        if (isACH()) {
            concatDone(sb, this.contractAchPDF);
        }

        String done = sb.toString();
        return done;
    }

    private void concatDone(StringBuilder sb, BasePDFDocument pdfDocument) {
        if (pdfDocument != null) {
            try {
                String raw = pdfDocument.getRawHtml();
                String done = pdfDocument.replaceField(raw);
                sb.append(done);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        return;
    }

    @Override
    public void afterPdfProcess() {

    }

    /**
	 * Save bytes to TABLE: CaseDocument
	 * Save string base64 to TABLE: ContractDoc
	 */
    @Override
    public void saveToDB(byte[] pdfByte) {
        
        // Save bytes to TABLE: CaseDocument
        CaseDocument cd = new CaseDocument(this._uniqId, UNIQ_TYPE_03, DOCUMENT_TYPE_CONTRACT_PDF);
        cd.setContent(pdfByte);
        cd.setVersion(templateVersion);
        caseDocumentDao.CreateOrUpdate(cd);
        
		// Save string base64 to TABLE: CaseDoc
		String str = Base64.encodeBase64String(pdfByte);
		contractDao.updateContractPdf(_uniqId, str);
        logger.info("Save Pdf To DB");

        // PDF檔 - @export 儲存至檔案系統
        if (globalConfig.isPDF_Export()) {
            ExportUtil.export(global.PdfDirectoryPath + "/" + _uniqId + ".pdf", pdfByte);
        }
    }

    @Override
    public void pdfToImage(List<byte[]> byteList) {

    }

    @Override
    public void sendEmail(byte[] pdfByte) {
        sendMailHunterService.sendContractMailHunter(_uniqId, contractMain.getEmail(), pdfByte);
    }

    public boolean isACH() {
        ContractMainActInfo actInfe = this.contractMainActInfo;
        // boolean isEddaPass = StringUtils.equals(actInfe.getEddaStatus(), "1");
        boolean isEddaFail = StringUtils.equals(actInfe.getEddaStatus(), "0"); // ACH
        return PlContractConst.isPL(this.contractMain.getProductId()) && isEddaFail;
    }

    public ContractPDF getContractPDF() {
        return contractPDF;
    }

    public void setContractPDF(ContractPDF contractPDF) {
        this.contractPDF = contractPDF;
    }

    public ContractAchPDF getContractAchPDF() {
        return contractAchPDF;
    }

    public void setContractAchPDF(ContractAchPDF contractAchPDF) {
        this.contractAchPDF = contractAchPDF;
    }
}
