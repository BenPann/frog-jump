package com.kgi.airloanex.ap.controller;

import javax.servlet.http.HttpServletRequest;

import com.kgi.airloanex.ap.service.BrowseService;
import com.kgi.eopend3.ap.controller.base.BaseController;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.dto.KGIHeader;
import com.kgi.eopend3.common.dto.WebResult;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/browse")
public class BrowseController extends BaseController {
    
    private static String context = "BrowseLogController";

    @Autowired
    private BrowseService browseService;

    @PostMapping("/setPage")
    public String setPage(HttpServletRequest request, @RequestBody String reqBody) {

        logger.info("k前台輸入的資料" + reqBody);
        try {

            KGIHeader header = this.getHeader(request);

            String uniqId = header.getUniqId();
            String uniqType = header.getUniqType();
            String ipAddress = header.getIpAddress();
            if(ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)){
                return browseService.setPage(reqBody, uniqId, uniqType, ipAddress);
            }else{
                return WebResult.GetFailResult();
            }
        } catch (ErrorResultException e) {
            return WebResult.GetFailResult();
        }
    }
}