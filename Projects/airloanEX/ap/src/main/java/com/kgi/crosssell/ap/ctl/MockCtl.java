package com.kgi.crosssell.ap.ctl;

import java.util.ArrayList;
import java.util.List;

import com.kgi.crosssell.common.dto.Attachments;
import com.kgi.crosssell.common.dto.Portfolios;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MockCtl {
	private static final Logger logger = LogManager.getLogger(MockCtl.class.getName());
    
	@PostMapping("/getCrossBusinessApplyInfo")
	// [2019.10.01 GaryLiu] ==== START : 修改Parameter Tampering ====
	// [2019.10.01 GaryLiu] 嘗試移除用Annotation的驗證方式，交給ESAPI判斷
	public CrossBusinessApplyReponse getCrossBusinessApplyInfo(@RequestBody String reqBody) {
        
        CrossBusinessApplyReponse result = new CrossBusinessApplyReponse();

        System.out.println("mock success!");

        result.setRtnCode("rtnCodeForTest");
        result.setMsg("msgForTest");
        result.setRtnSeq("rtnSeqForTest");
        result.setBusinessKind("businessKindForTest");
        result.setIdno("idnoForTest");
        result.setApplyDateTime("applyDateTimeForTest");
        result.setCaSert("caSertForTest");
        result.setContractVer("contractVerForTest");
        result.setIp("ipForTest");

        List<Portfolios> portfoliosList = new ArrayList<Portfolios>();
        List<Attachments> attachmentsList = new ArrayList<Attachments>();

        for(int i = 0; i < 3; i++){
            Portfolios portfolios = new Portfolios();
            portfolios.setBaseDate("baseDateForTest" + i);
            portfolios.setCurrency("CurrencyForTest" + i);
            portfolios.setMarketItem("marketItemForTest" + i);
            portfolios.setValueSum("sumForTest" + i);
            portfoliosList.add(portfolios);

            Attachments attachments = new Attachments();
            attachments.setFileType("fileTypeForTest" + i);
            attachments.setFileExtension("fileExtensionForTest" + i);
            attachments.setFileData("fileDataForTest" + i);
            attachmentsList.add(attachments);
        }
        

        result.setPortfolios(portfoliosList);
        result.setAttachments(attachmentsList);

        return result;
    }
    
	@PostMapping("/getStkCustInfo")
	// [2019.10.01 GaryLiu] ==== START : 修改Parameter Tampering ====
	// [2019.10.01 GaryLiu] 嘗試移除用Annotation的驗證方式，交給ESAPI判斷
	public StkCustInfoReponse getStkCustInfo(@RequestBody String reqBody) {
        
        StkCustInfoReponse result = new StkCustInfoReponse();
        
        result.setRtncode("0");
        result.setMessage("");
        result.setCustName1("張惠妹");
        result.setCustName2("ｋｕｌｉｌａｙ Ａｍｉｔ");
        result.setCustEngName("Chang Hui-mei");
        result.setIdno("A123456789");
        result.setIdKind("11");
        result.setBirthday("19700101");
        result.setResAddr("臺北市中正區忠孝西路一段１００號");
        result.setCommAddr("臺北市中正區忠孝西路一段１００號");
        result.setHomeTel("0277107383");
        result.setHomeTelExt("");
        result.setCorpTel("0223148800");
        result.setCorpTelExt("7383");
        result.setPhone("0900000000");
        result.setEmail("abc@gmail.com");
        result.setCorpName("中華開發金控凱基銀行");
        result.setJobType("000001");
        result.setJobTitle("rtnCodeForTest");
        result.setEducation("01");
        result.setMaritalStatus("S");
        result.setChildrenStatus("1");
        result.setCorpAddr("新北市中和區景平路１８８號５樓");
        result.setResAddrZip("");
        result.setCommAddrZip("");
        result.setCorpAddrZip("");

        return result;
    }

    class CrossBusinessApplyReponse{

        private String rtnCode;
        private String msg;
        private String rtnSeq;
        private String businessKind;
        private String idno;
        private String applyDateTime;
        private String caSert;
        private String contractVer;
        private String ip;
        private List<Portfolios> portfolios;
        private List<Attachments> attachments;

        public String getRtnCode() {
            return rtnCode;
        }
        public void setRtnCode(String rtnCode) {
            this.rtnCode = rtnCode;
        }
        public String getMsg() {
            return msg;
        }
        public void setMsg(String msg) {
            this.msg = msg;
        }
        public String getRtnSeq() {
            return rtnSeq;
        }
        public void setRtnSeq(String rtnSeq) {
            this.rtnSeq = rtnSeq;
        }
        public String getBusinessKind() {
            return businessKind;
        }
        public void setBusinessKind(String businessKind) {
            this.businessKind = businessKind;
        }
        public String getIdno() {
            return idno;
        }
        public void setIdno(String idno) {
            this.idno = idno;
        }
        public String getApplyDateTime() {
            return applyDateTime;
        }
        public void setApplyDateTime(String applyDateTime) {
            this.applyDateTime = applyDateTime;
        }
        public String getCaSert() {
            return caSert;
        }
        public void setCaSert(String caSert) {
            this.caSert = caSert;
        }
        public String getContractVer() {
            return contractVer;
        }
        public void setContractVer(String contractVer) {
            this.contractVer = contractVer;
        }
        public String getIp() {
            return ip;
        }
        public void setIp(String ip) {
            this.ip = ip;
        }
        public List<Portfolios> getPortfolios() {
            return portfolios;
        }
        public void setPortfolios(List<Portfolios> portfolios) {
            this.portfolios = portfolios;
        }
        public List<Attachments> getAttachments() {
            return attachments;
        }
        public void setAttachments(List<Attachments> attachments) {
            this.attachments = attachments;
        }
        
    }

    class StkCustInfoReponse{

        private String rtncode;
        private String message;
        private String custName1;
        private String custName2;
        private String custEngName;
        private String idno;
        private String idKind;
        private String birthday;
        private String resAddr;
        private String commAddr;
        private String homeTel;
        private String homeTelExt;
        private String corpTel;
        private String corpTelExt;
        private String phone;
        private String email;
        private String corpName;
        private String jobType;
        private String jobTitle;
        private String education;
        private String maritalStatus;
        private String childrenStatus;
        private String corpAddr;
        private String resAddrZip;
        private String commAddrZip;
        private String corpAddrZip;

        public String getRtncode() {
            return rtncode;
        }
        public void setRtncode(String rtncode) {
            this.rtncode = rtncode;
        }
        public String getMessage() {
            return message;
        }
        public void setMessage(String message) {
            this.message = message;
        }
        public String getCustName1() {
            return custName1;
        }
        public void setCustName1(String custName1) {
            this.custName1 = custName1;
        }
        public String getCustName2() {
            return custName2;
        }
        public void setCustName2(String custName2) {
            this.custName2 = custName2;
        }
        public String getCustEngName() {
            return custEngName;
        }
        public void setCustEngName(String custEngName) {
            this.custEngName = custEngName;
        }
        public String getIdno() {
            return idno;
        }
        public void setIdno(String idno) {
            this.idno = idno;
        }
        public String getIdKind() {
            return idKind;
        }
        public void setIdKind(String idKind) {
            this.idKind = idKind;
        }
        public String getBirthday() {
            return birthday;
        }
        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }
        public String getResAddr() {
            return resAddr;
        }
        public void setResAddr(String resAddr) {
            this.resAddr = resAddr;
        }
        public String getCommAddr() {
            return commAddr;
        }
        public void setCommAddr(String commAddr) {
            this.commAddr = commAddr;
        }
        public String getHomeTel() {
            return homeTel;
        }
        public void setHomeTel(String homeTel) {
            this.homeTel = homeTel;
        }
        public String getHomeTelExt() {
            return homeTelExt;
        }
        public void setHomeTelExt(String homeTelExt) {
            this.homeTelExt = homeTelExt;
        }
        public String getCorpTel() {
            return corpTel;
        }
        public void setCorpTel(String corpTel) {
            this.corpTel = corpTel;
        }
        public String getCorpTelExt() {
            return corpTelExt;
        }
        public void setCorpTelExt(String corpTelExt) {
            this.corpTelExt = corpTelExt;
        }
        public String getPhone() {
            return phone;
        }
        public void setPhone(String phone) {
            this.phone = phone;
        }
        public String getEmail() {
            return email;
        }
        public void setEmail(String email) {
            this.email = email;
        }
        public String getCorpName() {
            return corpName;
        }
        public void setCorpName(String corpName) {
            this.corpName = corpName;
        }
        public String getJobType() {
            return jobType;
        }
        public void setJobType(String jobType) {
            this.jobType = jobType;
        }
        public String getJobTitle() {
            return jobTitle;
        }
        public void setJobTitle(String jobTitle) {
            this.jobTitle = jobTitle;
        }
        public String getEducation() {
            return education;
        }
        public void setEducation(String education) {
            this.education = education;
        }
        public String getMaritalStatus() {
            return maritalStatus;
        }
        public void setMaritalStatus(String maritalStatus) {
            this.maritalStatus = maritalStatus;
        }
        public String getChildrenStatus() {
            return childrenStatus;
        }
        public void setChildrenStatus(String childrenStatus) {
            this.childrenStatus = childrenStatus;
        }
        public String getCorpAddr() {
            return corpAddr;
        }
        public void setCorpAddr(String corpAddr) {
            this.corpAddr = corpAddr;
        }
        public String getResAddrZip() {
            return resAddrZip;
        }
        public void setResAddrZip(String resAddrZip) {
            this.resAddrZip = resAddrZip;
        }
        public String getCommAddrZip() {
            return commAddrZip;
        }
        public void setCommAddrZip(String commAddrZip) {
            this.commAddrZip = commAddrZip;
        }
        public String getCorpAddrZip() {
            return corpAddrZip;
        }
        public void setCorpAddrZip(String corpAddrZip) {
            this.corpAddrZip = corpAddrZip;
        }

    }
}
