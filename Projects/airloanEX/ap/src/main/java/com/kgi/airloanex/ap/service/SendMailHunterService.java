package com.kgi.airloanex.ap.service;

import java.util.List;
import java.util.Map;

import com.kgi.airloanex.ap.dao.CS_VerifyLogDao;
import com.kgi.airloanex.ap.dao.CreditCaseDataDao;
import com.kgi.airloanex.ap.dao.MailHistoryDao;
import com.kgi.airloanex.ap.pdf.dto.PDFCaseData;
import com.kgi.airloanex.common.dto.db.CS_VerifyLog;
import com.kgi.airloanex.common.dto.db.CreditCaseDataExtend;
import com.kgi.airloanex.common.dto.db.MailHistory;
import com.kgi.eopend3.common.dto.db.Config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.json.JSONObject;

@Service
public class SendMailHunterService {

    private final String MAILTYPE = "1";

    @Autowired
    private ConfigService configService;

    @Autowired
    private MailHistoryDao mailHistoryDao;

    @Autowired
    private CreditCaseDataDao creditCaseDataDao;

    @Autowired
	private CS_VerifyLogDao cs_VerifyLogDao;
    
    /**
     * 立約PDF寄送
     * @param contractData
     * @param pdfByte
     */
    public void sendContractMailHunter(String uniqId, String email, byte[] pdfByte) {

        // 處理templateId,title
        String title = "凱基數位信貸線上立約完成通知";
        String templateName = "MailHunter.ContractTemplateId";
        String TemplateId = getConfigQuery(templateName);
        TemplateId = TemplateId.equals("") ? "1263" : TemplateId;
        // 處理後續會用到的ID資訊，目前想法是以JSON存在content內
        String idno = uniqId;/** 看原本的 template中的ID是帶入contractData的UniqId ，所以延用 */
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("idno", idno);
        String content = jsonObject.toString();

        MailHistory mailHistory = setMailHunterCaseMailHistory(uniqId, title, content, email, TemplateId);
        mailHistory.setAttatchment(pdfByte);
        mailHistoryDao.Create(mailHistory);

        System.out.println("set Email done!");
    } // end sendContractMailHunter

    /**
     * 貸款案件PDF申請書寄送
     *
     * @param pdfCaseData
     * @param pdfByte
     */
    public void sendCaseMailHunter(PDFCaseData pdfCaseData, byte[] pdfByte) {

        //處理templateId,title
        String title = "凱基數位信貸線上申請完成通知";
        String templateName = "MailHunter.ApplyDataTemplateId";
        String TemplateId = getConfigQuery(templateName).equals("")?"1263": getConfigQuery(templateName);

        //處理後續會用到的ID資訊，目前想法是以JSON存在content內
        String idno = pdfCaseData.idno;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("idno", idno);
        String content = jsonObject.toString();


        MailHistory mailHistory = setMailHunterCaseMailHistory(pdfCaseData.CaseNo, title, content, pdfCaseData.email_address, TemplateId);
        mailHistory.setAttatchment(pdfByte);
        mailHistoryDao.Create(mailHistory);

        System.out.println("set Email done!");
    } // end sendCaseMailHunter

    private String getConfigQuery(String keyName){
        String result = "";
        Config config = new Config();
        config.setKeyName(keyName);
        config = configService.Read(config);
        if(config != null && config.getKeyValue()!=null && !config.getKeyValue().equals(""))
            result = config.getKeyValue();
        return result;
    }

    /**
     * MailHunter用的template
     * @param uniqId
     * @param title
     * @param content
     * @param eMail
     * @param templateName
     * @return
     */
    private MailHistory setMailHunterCaseMailHistory(String uniqId, String title, String content, String eMail, String templateName) {
        MailHistory mailHistory = MailHistory.getInstance().setMailType(MAILTYPE).setStatus("0").setUniqId(uniqId).setEMailAddress(eMail).setTemplateId(templateName).setTitle(title).setContent(content);
        return mailHistory;
    }

        /**
     * 信用卡完成申請PDF申請書寄送
     *
     * @param cd
     * @param pdfByte
     */
    public void sendCreditCardPdf(CreditCaseDataExtend cd, byte[] pdfByte) {
        try {
            List<Map<String, Object>> tempCreditVerify = creditCaseDataDao.getCreditVerify(cd.getUniqId());
            String userType = cd.getUserType();
            String creditVerify = "0";
            if (tempCreditVerify.size() != 0) {
                Map<String, Object> tempMp = tempCreditVerify.get(0);
                creditVerify = tempMp.get("VerifyStatus").toString().trim();
            }
            //判斷是否為跨售資料，如果是跨售需要修改auth_type(驗身方式)
    		CS_VerifyLog cs_VerifyLog = new CS_VerifyLog();
    		cs_VerifyLog.setUniqId(cd.getUniqId());
    		cs_VerifyLog = cs_VerifyLogDao.Read(cs_VerifyLog);	
    		if(cs_VerifyLog!=null) {
    			creditVerify = "1";
    		}
            //處理templateId,title
            String title = "凱基信用卡線上申請完成通知";
            String templateName = "";
            // 身分驗證沒過
            if (creditVerify.equals("0") && (userType.equals("0") || userType.equals("4"))) {
                templateName = "MailHunter.CreditCaseNoVerifyTemplateId";
                title = "凱基信用卡線上申請未完成通知(請填寫申請書後再列印寄回)";
            } else if (userType.equals("1") || userType.equals("2") || userType.equals("3") || creditVerify.equals("1")) {
                templateName ="MailHunter.CreditCaseTemplateId";
            } else {
                title = "取得MailHunter的TemplateId失敗 CreditVerrify = " + creditVerify + " UserType = " + userType;
            }
            String TemplateId = getConfigQuery(templateName).equals("")?"3335": getConfigQuery(templateName);
            //處理後續會用到的ID資訊，目前想法是以JSON存在content內
            String idno = cd.getIdno();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("idno", idno);
            String content = jsonObject.toString();


            MailHistory mailHistory = setMailHunterCaseMailHistory(cd.getUniqId(), title, content, cd.getEmailAddress(), TemplateId);
            mailHistory.setAttatchment(pdfByte);
            mailHistoryDao.Create(mailHistory);

            System.out.println("set Email done!");

        } catch (Exception e) {

        }

    }
}