package com.kgi.airloanex.ap.service;

import static com.kgi.airloanex.common.PlContractConst.QLOG;

import com.google.gson.Gson;
import com.kgi.airloanex.common.dto.response.LoginResp;
import com.kgi.airloanex.common.dto.response.LogonResp;
import com.kgi.airloanex.common.dto.view.LogonView;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.ap.dao.EOP_IPLogDao;
import com.kgi.eopend3.ap.dao.EntryDataDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.db.EOP_IPLog;
import com.kgi.eopend3.common.dto.db.EntryData;
import com.kgi.eopend3.common.util.CheckUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogonService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private ConfigDao configDao;

    @Autowired
    private SrnoService snSer;

    @Autowired
    private EntryDataDao entryDataDao;

    @Autowired
    private EOP_IPLogDao eopIPLogDao;

    public String logon(String reqJson) {
        try {
            LogonView login = new Gson().fromJson(reqJson, LogonView.class);
            if (!CheckUtil.check(login)) {
                throw new ErrorResultException(1, configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
            } else {
                String uniqType = login.getUniqType();
                String entry = login.getEntry();
                String idno = login.getIdno();
                String ipAddress = login.getIpAddress();
                // String birthday = login.getBirthday();
                String browser = login.getBrowser();
                String platform = login.getPlatform();
                String os = login.getOs();

                // 創立登入編號
                String uniqId = snSer.getQLOGUniqId();
                
                LogonResp resp = new LogonResp();
                LoginResp loginResp = new LoginResp();
                loginResp.setUniqId(uniqId); // 該 uniqId 與主檔無關
                loginResp.setUniqType(uniqType);
                loginResp.setIdno(idno);
                loginResp.setIpAddress(ipAddress);
                resp.setLoginResp(loginResp);

                // 寫入entry資料
                EntryData entryData = new EntryData(uniqId, uniqType, entry, QLOG, browser, platform, os);
                entryDataDao.Create(entryData);

                // EOP_IPLog
                EOP_IPLog eop_IPLog = new EOP_IPLog(uniqId, ipAddress);
                eopIPLogDao.Create(eop_IPLog);

                return WebResult.GetResultString(0, "成功", resp);
            }
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end logon
}
