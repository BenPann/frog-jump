package com.kgi.eopend3.ap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class InitApplication {
  private Logger logger = LoggerFactory.getLogger(this.getClass());
 
  @Value("${FUCO_CONFIG_PATH}")
  String configPath;

  @EventListener(ContextRefreshedEvent.class)
  public void initSetting() throws RuntimeException {
    logger.info("初始化設定開始!");

    logger.info("設定ESAPI開始!");
    logger.info(configPath);
    System.setProperty("org.owasp.esapi.resources", this.configPath);
    logger.info("設定ESAPI結束!");
    logger.info("初始化設定結束!");

  }
}