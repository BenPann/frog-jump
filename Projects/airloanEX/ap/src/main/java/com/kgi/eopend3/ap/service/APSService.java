package com.kgi.eopend3.ap.service;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.ContractApiLogDao;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.eopend3.ap.dao.HttpDao;
import com.kgi.eopend3.common.dto.respone.HttpResp;
import com.kgi.eopend3.common.util.DateUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class APSService {

    @Autowired
    AirloanEXConfig globalConfig;

    @Autowired
    HttpDao httpDao;

    @Autowired
    private ContractApiLogDao contractApiLogDao;
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());  

    public String queryAPS(String qryName, String queryJson,String keyInfo) throws Exception {
        String apsUrl = globalConfig.ALN_ServerHost();
        return queryAPS(apsUrl, qryName, queryJson,keyInfo);
    }

    public String queryAPS(String apsUrl, String qryName, String queryJson,String keyInfo) throws Exception {
        // 1. 組合出 request 的 url
        HttpResp httpResp = new HttpResp();
        int statusCode = 500;
        String response = "";
        String startTime = "";
        String endTime = "";
        try {
            // 2. 發送 request
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            httpResp = this.httpDao.queryPatch(apsUrl, qryName, queryJson);
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            statusCode = httpResp.getStatusCode();
            response = httpResp.getResponse();
            if (statusCode != 200) {
                logger.error("呼叫APS失敗!");
                throw new Exception("呼叫APS Server失敗!!");
            }
        } catch(Exception e){
            logger.error("未知錯誤", e);
            throw new Exception();
        }finally {
            // 3. 寫ContractApiLog
            contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, keyInfo, qryName, queryJson, response, startTime, endTime);           
        }
        // 4. 返回結果
        return response;
    }

    public String queryOCR(String qryName, String queryJson,String uniqId) throws Exception {
        // 1. 組合出 request 的 url
        String apiurl = globalConfig.OCR_ServerHost().concat(qryName);
        String startTime = "";
        String endTime = "";
        String rep = "";
        try {
            // 2. 發送 request
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");

            logger.info("*************"+ qryName +" Start Time***************" + startTime);
            rep = httpDao.doJsonPostRequest(apiurl, queryJson);
            logger.info(rep);
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");            
            logger.info("*************"+ qryName +" End Time***************" + endTime);      
            return rep;
        } catch (Exception e) {
            throw new Exception();
        } finally {
            // 3. 寫ContractApiLog
            contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, uniqId, qryName, queryJson, rep, startTime, endTime);
            
        }
       
    }
    
}
