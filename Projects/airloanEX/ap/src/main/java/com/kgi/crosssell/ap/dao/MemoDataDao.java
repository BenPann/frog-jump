package com.kgi.crosssell.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.MemoData;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class MemoDataDao extends CRUDQDao<MemoData> {

    @Override
    public int Create(MemoData fullItem) {
        String sql = "INSERT INTO MemoData Values(?,?,?,?,GETDATE(),GETDATE());";
        return this.getJdbcTemplate().update(sql, new Object[] { fullItem.getUniqId(), fullItem.getUniqType(),
                fullItem.getMemoName(), fullItem.getMemo() });
    }

    @Override
    public MemoData Read(MemoData keyItem) {
        String sql = "SELECT * FROM MemoData WHERE UniqId = ? AND MemoName = ?;";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(MemoData.class),
                    new Object[] { keyItem.getUniqId(), keyItem.getMemoName() });
        } catch (DataAccessException ex) {

            System.out.println(
                    "#### " + keyItem.getUniqId() + " MemoName=" + keyItem.getMemoName() + " 在 MemoData 查無對應資料.");
            return null;
        }
    }

    @Override
    public int Update(MemoData fullItem) {
        String sql = "UPDATE MemoData SET Memo = ?,UpdateTime = GETDATE()  WHERE UniqId = ? AND MemoName = ?;";
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getMemo(), fullItem.getUniqId(), fullItem.getMemoName() });
    }

    @Override
    public int Delete(MemoData keyItem) {
        String sql = "DELETE MemoData WHERE UniqId = ? AND MemoName = ?;";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getUniqId(), keyItem.getMemoName() });
    }

    @Override
    public List<MemoData> Query(MemoData keyItem) {
        String sql = "SELECT * FROM MemoData WHERE UniqId = ?;";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getUniqId() },
                    new BeanPropertyRowMapper<>(MemoData.class));
        } catch (DataAccessException ex) {
            // ex.printStackTrace();
            return null;
        }
    }

    public int SaveToMemoTokenInfo(String UniqId, String Token, String ChannelId) {
        String sql = "INSERT INTO MemoTokenInfo (UniqId,Token,ChannelId,CreateTime) Values(?,?,?,GETDATE());";
        return this.getJdbcTemplate().update(sql, new Object[] { UniqId, Token, ChannelId });
    }

}
