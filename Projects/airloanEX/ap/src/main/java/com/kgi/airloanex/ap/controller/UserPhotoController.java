package com.kgi.airloanex.ap.controller;

import javax.servlet.http.HttpServletRequest;

import com.kgi.airloanex.ap.service.UserPhotoService;
import com.kgi.eopend3.ap.controller.base.BaseController;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.dto.KGIHeader;
import com.kgi.eopend3.common.dto.WebResult;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/photo")
public class UserPhotoController extends BaseController {

    @Autowired
    private UserPhotoService userPhotoService;

    @PostMapping("/upload")
    public String uploadPhoto(HttpServletRequest request, @RequestBody String reqBody) {

        try {
            KGIHeader header = this.getHeader(request);
            String uniqId = header.getUniqId();
            String idno = header.getIdno();
            if(ESAPI.validator().isValidInput("UserPhotoController", reqBody, "SafeJson", Integer.MAX_VALUE, false)){
                return userPhotoService.uploadPhoto(reqBody, uniqId, idno);
            }else{
                return WebResult.GetResultString(9, "Invalid Input");
            }
        } catch (ErrorResultException e) {
            return WebResult.GetFailResult();
        }
    }  

    @PostMapping("/deletePhoto")
    public String delete(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("前台輸入的資料" + reqBody);
        try {
            KGIHeader header = this.getHeader(request);
            String uniqId = header.getUniqId();
            if(ESAPI.validator().isValidInput("UserPhotoController", reqBody, "SafeJson", Integer.MAX_VALUE, false)){
                return userPhotoService.delete(reqBody, uniqId);
            }else{
                return WebResult.GetResultString(9, "Invalid Input");
            }
           
        } catch (ErrorResultException e) {
            return WebResult.GetFailResult();
        }
    }

    @GetMapping("/getPhotoList")
    public String getPhotoList(HttpServletRequest request) {
        try {
            KGIHeader header = this.getHeader(request);
            String uniqId = header.getUniqId();            
            return userPhotoService.getPhotoList(uniqId);
        } catch (ErrorResultException e) {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/uploadBCard")
    public String uploadBCard(HttpServletRequest request, @RequestBody String reqBody) {
        //logger.info("前台輸入的資料" + reqBody);
        try {
            KGIHeader header = this.getHeader(request);
            String uniqId = header.getUniqId();
            String idno = header.getIdno();
            if(ESAPI.validator().isValidInput("UserPhotoController", reqBody, "SafeJson", Integer.MAX_VALUE, false)){
                return userPhotoService.uploadBCard("uploadBCard",reqBody, uniqId, idno);
            }else{
                return WebResult.GetResultString(9, "Invalid Input");
            }
           
        } catch (ErrorResultException e) {
            return WebResult.GetFailResult();
        }
    }

}