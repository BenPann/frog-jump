package com.kgi.eopend3.ap.service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kgi.airloanex.ap.dao.ContractMainTermsDao;
import com.kgi.airloanex.common.dto.db.ContractMainTerms;
import com.kgi.eopend3.ap.dao.KGITermsDao;
import com.kgi.eopend3.ap.dao.TermsDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.customDto.KGITermsCustDto;
import com.kgi.eopend3.common.dto.db.Terms;
import com.kgi.eopend3.common.dto.view.TermPageView;
import com.kgi.eopend3.common.dto.view.TermsView;
import com.kgi.eopend3.common.util.CheckUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TermsService {

    @Autowired
    private TermsDao termsDao;

    @Autowired
    private ContractMainTermsDao contractMainTermsDao;

    @Autowired
    private KGITermsDao kgiTermsDao;
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public String getTerms(String TermsName) {
        Terms term = new Terms();
        term.setName(TermsName);
        term= termsDao.Read(term);
        if(term!=null){
            return term.getContent();
        }else{
            return "";
        }
    }
    
    public String getTermsList(String reqJson) {
        try {
            Gson gson = new Gson();
            TermPageView view = gson.fromJson(reqJson, TermPageView.class);
            if (!CheckUtil.check(view)) {
                throw new ErrorResultException(1, "參數錯誤", null);
            } else {
                List<KGITermsCustDto> list = kgiTermsDao.getTermsList(view.getUrl());                            
                return gson.toJson(list);
            }
        } catch (ErrorResultException e) {            
            logger.error("參數錯誤", e);
            return "[]";
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return "[]";
        }
    }

    public String setContractMainTerms(String reqJson, String uniqId) {//先寫死Y
		try {
			Gson gson = new Gson();    	
            Type founderListType = new TypeToken<ArrayList<TermsView>>(){}.getType();
            List<TermsView> founderList = gson.fromJson(reqJson, founderListType);
            if (founderList==null||founderList.size()<=0) {
                throw new ErrorResultException(1, "參數錯誤", null);
            } else {
                for(TermsView view : founderList){
                    Terms term = new Terms();
                    term.setName(view.getTermName());
                    term= termsDao.Read(term);
                    if(term!=null){
                        ContractMainTerms contractMainTerms = new ContractMainTerms(uniqId,term.getSerial());
                        contractMainTerms.setIsAgree(view.getCheck());
                        contractMainTermsDao.CreateOrUpdate(contractMainTerms);
                    }
                }
			    return WebResult.GetResultString(0,"成功","");
			}			
    	} catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
    }
}