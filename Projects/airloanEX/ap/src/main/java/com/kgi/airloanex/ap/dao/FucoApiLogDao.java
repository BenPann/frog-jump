package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.FucoApiLog;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.stereotype.Repository;

@Repository
public class FucoApiLogDao extends CRUDQDao<FucoApiLog> {

	@Override
	public int Create(FucoApiLog fullItem) {
		String sql = "INSERT INTO FucoApiLog" + "(ApiName, Request, Response, UTime) VALUES" + "(?,?,?,GETDATE())";
		return this.getJdbcTemplate().update(sql,
				new Object[] { fullItem.getApiName(), fullItem.getRequest(), fullItem.getResponse() });

	}

	@Override
	public FucoApiLog Read(FucoApiLog keyItem) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int Update(FucoApiLog fullItem) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int Delete(FucoApiLog keyItem) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<FucoApiLog> Query(FucoApiLog keyItem) {
		// TODO Auto-generated method stub
		return null;
	}
}
