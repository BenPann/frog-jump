package com.kgi.eopend3.ap.dao;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.eopend3.common.dto.respone.HttpResp;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.sf.json.JSONObject;

@Repository
public class HttpDao {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AirloanEXConfig airloanEXConfig;

    @Autowired
    AirloanEXConfig globalConfig;

    public String doSoapPostRequest(String urlStr, String postData) throws Exception {
        return doSoapPostRequest(urlStr, postData, "utf-8", "utf-8");
    }
    
    public String doSoapPostRequest(String urlStr, String postData, String acceptEnc, String contentEnc)
            throws Exception {
        String[][] prop = {{"Accept-Charset", acceptEnc}, {"Content-type", "text/xml;charset=".concat(contentEnc)},
                {"Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7"}};
        return doPostRequest(urlStr, postData, prop);
    }

    public String doJsonPostRequest(String urlStr, String postData)
            throws Exception {
        String[][] prop = {{"Content-type", "application/json;charset=UTF-8"},
                {"Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7"}};
        return doPostRequest(urlStr, postData, prop);
    }

    public String doPostRequestNoHeader(String urlStr, String postData) throws Exception {
        return doPostRequest(urlStr, postData, null);
    }

    public HttpResp postJson(String url, String json) {
        String[][] prop = {
											{"Accept", "application/json"},
        									{"Content-type", "application/json;charset=UTF-8"},
        									{"Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7"}
        							};
        return doPostRequest(url, json, prop, 30) ;
    }

    private HttpResp doPostRequest(String urlStr, String postData, String[][] prop, int timeout) {
        HttpResp resp = new HttpResp() ; 

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000)
                .build();

        CloseableHttpResponse response = null ;
        try (CloseableHttpClient client = createTrustAllHttpClientBuilder().setDefaultRequestConfig(config).build()) {

            // AP網址
            HttpPost post = new HttpPost(urlStr);
            // 必須用UTF-8 不然會有亂碼
            StringEntity entity = new StringEntity(postData, "UTF-8");
            post.setEntity(entity);
            if (prop != null) {
                for (int i = 0; i < prop.length; i++) {
                    post.setHeader(prop[i][0], prop[i][1]);
                }
            }
            
            response = client.execute(post);
            
            if (response != null) {
            	resp.setStatusCode(response.getStatusLine().getStatusCode());
   				resp.setResponse(EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"))) ;
            } else {
            	resp.setStatusCode(500) ;
    			resp.setResponse("未預期的系統錯誤，請稍後再試") ;
            }
        } catch (Exception ex) {
        	ex.printStackTrace();
        	resp.setStatusCode(500) ;
        	resp.setResponse(ex.getMessage());
        }  finally {
        	try {
                if (response != null) {
                    response.close();
                }
			} catch (IOException e) {}

        }
        
        return resp ;
    }
    

    public String doPostRequest(String urlStr, String postData, String[][] prop) throws Exception {

        String result = "";
        int timeout = Integer.parseInt(airloanEXConfig.SocketTimeoutMillisec());//90秒
        @SuppressWarnings("deprecation")
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout)
                .setConnectionRequestTimeout(timeout)
                .setSocketTimeout(timeout)
                .setStaleConnectionCheckEnabled(true).build();

        try (CloseableHttpClient client = createTrustAllHttpClientBuilder().setDefaultRequestConfig(config).build()) {
            // AP網址
            HttpPost post = new HttpPost(urlStr);
            // 必須用UTF-8 不然會有亂碼
            StringEntity entity = new StringEntity(postData, "UTF-8");
            post.setEntity(entity);
            if (prop != null) {
                for (int i = 0; i < prop.length; i++) {
                    post.setHeader(prop[i][0], prop[i][1]);
                }
            }
            CloseableHttpResponse response = client.execute(post);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // 必須用UTF-8 不然會有亂碼
                result = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
            } else if (statusCode == 404) {
                // 要回傳找不到給打API的人知道
                result = "404";
            } else {
                result = "";
            }
        }  finally {
//            try {
//                client.close();
//            } catch (Exception e) {
//            }
        }
        return result;
    }

    public HttpClientBuilder createTrustAllHttpClientBuilder() {
        try {
            SSLContextBuilder builder = new SSLContextBuilder();
            builder.loadTrustMaterial(null, (chain, authType) -> true);
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build(), NoopHostnameVerifier.INSTANCE);
            return HttpClients.custom().setSSLSocketFactory(sslsf);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public HttpResp queryPatch(String ap, String linkName, String queryJson) throws Exception {
        HttpResp resp = new HttpResp();
        //設定TIMEOUT時間
        int timeOut = Integer.parseInt(airloanEXConfig.SocketTimeoutMillisec());//90秒
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        // 對連線池設定SocketConfig物件
        connManager.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(timeOut).build());

        CloseableHttpResponse response = null ;
        try (CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build()) {

            HttpPatch httpPatch = new HttpPatch(ap.concat(linkName));
            StringEntity entity = new StringEntity(queryJson, "UTF-8");
            httpPatch.addHeader("Charset", "UTF-8");
            httpPatch.addHeader("Accept-Charset", "UTF-8");
            httpPatch.addHeader("Content-Type", "application/json");
            httpPatch.addHeader("Accept", "application/json");

            httpPatch.setEntity(entity);
            response = client.execute(httpPatch);

            if (response != null) {
                resp.setStatusCode(response.getStatusLine().getStatusCode());
                resp.setResponse(EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8")));
            } else {
                resp.setStatusCode(500);
                resp.setResponse("未預期的系統錯誤，請稍後再試");
            }
        } catch (Exception ex) {
        	ex.printStackTrace();
        	resp.setStatusCode(500) ;
        	resp.setResponse(ex.getMessage());
        }  finally {
        	try {
                if (response != null) {
                    response.close();
                }
			} catch (IOException e) {}
        }
        return resp;
    }

    public HttpResp queryPost(String ap, String linkName, String queryJson) throws Exception {
        HttpResp resp = new HttpResp();
        //設定TIMEOUT時間
        int timeOut = Integer.parseInt(airloanEXConfig.SocketTimeoutMillisec());//90秒
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        // 對連線池設定SocketConfig物件
        connManager.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(timeOut).build());

        CloseableHttpResponse response = null ;
        try (CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build()) {

            HttpPost httpPost = new HttpPost(ap.concat(linkName));
            StringEntity entity = new StringEntity(queryJson, "UTF-8");
            httpPost.addHeader("Charset", "UTF-8");
            httpPost.addHeader("Accept-Charset", "UTF-8");
            httpPost.addHeader("Content-Type", "application/json");
            httpPost.addHeader("Accept", "application/json");

            httpPost.setEntity(entity);
            response = client.execute(httpPost);

            if (response != null) {
                resp.setStatusCode(response.getStatusLine().getStatusCode());
                resp.setResponse(EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8")));
            } else {
                resp.setStatusCode(500);
                resp.setResponse("未預期的系統錯誤，請稍後再試");
            }
        } catch (Exception ex) {
        	ex.printStackTrace();
        	resp.setStatusCode(500) ;
        	resp.setResponse(ex.getMessage());
        }  finally {
        	try {
                if (response != null) {
                    response.close();
                }
			} catch (IOException e) {}
        }
        return resp;
    }

    public HttpResp queryPostForCS(String ap, String linkName, String queryJson) throws Exception {
        HttpResp resp = new HttpResp();
        //設定TIMEOUT時間
        int timeOut = Integer.parseInt(airloanEXConfig.SocketTimeoutMillisec());//90秒
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        // 對連線池設定SocketConfig物件
        connManager.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(timeOut).build());

        CloseableHttpResponse response = null ;
        try (CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build()) {

            // HttpPost httpPost = new HttpPost(ap.concat(linkName));
            HttpPost httpPost = new HttpPost(ap.concat(linkName));
            StringEntity entity = new StringEntity(queryJson, "UTF-8");
            httpPost.addHeader("Charset", "UTF-8");
            httpPost.addHeader("Accept-Charset", "UTF-8");
            httpPost.addHeader("Content-Type", "application/json");
            httpPost.addHeader("Accept", "application/json");

            Date current = new Date();
            SimpleDateFormat sdf = new java.text.SimpleDateFormat("E, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
            String date = sdf.format(current);

            httpPost.addHeader("Date", date);

            // AWS authorization
            // 文字內容先做MD5 + base64
            String md5Content = base64(md5(queryJson));
            httpPost.addHeader("Content-MD5", md5Content);

            // String apiKey = GlobalProp.getInstance().getString("APIMApiKey");
		
            String signature = Encryption(md5Content, "/v1/CROSS_CO/getDataFromURL", date);

            String apiKey = globalConfig.APIM_CrossSell_ApiKey();
		
		    String authorization = "AWS " + apiKey + ":" + signature;

            httpPost.addHeader("Authorization", authorization);

            System.out.println("queryJson : " + queryJson);
            System.out.println("date : " + date);
            System.out.println("md5Content : " + md5Content);
            System.out.println("Authorization : " + authorization);

            httpPost.setEntity(entity);
            response = client.execute(httpPost);

            if (response != null) {
                resp.setStatusCode(response.getStatusLine().getStatusCode());
                resp.setResponse(EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8")));
            } else {
                resp.setStatusCode(500);
                resp.setResponse("未預期的系統錯誤，請稍後再試");
            }
        } catch (Exception ex) {
        	ex.printStackTrace();
        	resp.setStatusCode(500) ;
        	resp.setResponse(ex.getMessage());
        }  finally {
        	try {
                if (response != null) {
                    response.close();
                }
			} catch (IOException e) {}
        }
        return resp;
    }

    public String doPostRequestByFormData(String urlStr, String[][] postData) throws Exception {
        String result = "";
        int timeout = 30;
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();


        try (CloseableHttpClient client = createTrustAllHttpClientBuilder().setDefaultRequestConfig(config).build()){
            // AP網址
            HttpPost post = new HttpPost(urlStr);
            // 必須用UTF-8 不然會有亂碼
            List<NameValuePair> form = new ArrayList<NameValuePair>();
            for (int i = 0; i < postData.length; i++) {
                form.add(new BasicNameValuePair(postData[i][0], postData[i][1]));
            }
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(form, Consts.UTF_8);
            post.setEntity(entity);
            post.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            CloseableHttpResponse response = client.execute(post);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // 必須用UTF-8 不然會有亂碼
                result = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
                logger.info("post result: " + result);
            } else if (statusCode == 404) {
                // 要回傳找不到給打API的人知道
                result = "404";
            } else {
                result = "";
            }
        } finally {
//            try {
//                client.close();
//            } catch (Exception e) {
//            }
        }
        return result;
    }


    public String doPostByFile(String urlStr, Map<String, String> params, Map<String, File> files, String[][] prop) throws Exception {

        //使用工具类创建 httpClient
        String result = "";
        int timeout = 30;
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();


        try (CloseableHttpClient client = createTrustAllHttpClientBuilder().setDefaultRequestConfig(config).build()){
            // AP網址
            HttpPost httpPost = new HttpPost(urlStr);
            // 必須用UTF-8 不然會有亂碼

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setCharset(Charset.forName("UTF-8"));//设置请求的编码格式
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);//设置浏览器兼容模式

            for (String key : files.keySet()) {
//				FileInputStream fileInputStream = new FileInputStream(files.get(key));
//
//
//				builder.addBinaryBody(key,fileInputStream, ContentType.create("application/zip"), key);
                builder.addBinaryBody(key, files.get(key));
            }
            if (params != null) {
                for (String key : params.keySet()) {
                    builder.addTextBody(key, params.get(key)); //设置请求参数
                }
            }

            HttpEntity httpEntity = builder.build();
            //将请求参数放入 HttpPost 请求体中
            //使用 httpEntity 后 Content-Type会自动被设置成 multipart/form-data
            httpPost.setEntity(httpEntity);
            if (prop != null) {
                for (int i = 0; i < prop.length; i++) {
                    httpPost.setHeader(prop[i][0], prop[i][1]);
                }
            }


            //httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            CloseableHttpResponse response = client.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // 必須用UTF-8 不然會有亂碼
                result = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
                logger.info("post result: " + result);
            } else if (statusCode == 404) {
                // 要回傳找不到給打API的人知道
                result = "404";
            } else {
                result = "";
            }
        }  finally {
//            try {
//                client.close();
//            } catch (Exception e) {
//            }
        }
        return result;


    }


//    public String doPostByInpuStream(String urlStr, Map<String, String> params, Map<String, InputStream> files, String[][] prop) throws Exception {
    public String doPostByInpuStream(String urlStr, Map<String, String> params, Map<String, byte[]> files, String[][] prop) throws Exception {

        //使用工具类创建 httpClient
        String result = "";
        int timeout = 30;
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();


        try (CloseableHttpClient client = createTrustAllHttpClientBuilder().setDefaultRequestConfig(config).build()){
            // AP網址
            HttpPost httpPost = new HttpPost(urlStr);
            // 必須用UTF-8 不然會有亂碼

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setCharset(Charset.forName("UTF-8"));//设置请求的编码格式
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);//设置浏览器兼容模式

            for (String key : files.keySet()) {
                builder.addBinaryBody(key, files.get(key), ContentType.create("application/zip"), key);
            }
            if (params != null) {
                for (String key : params.keySet()) {
                    builder.addTextBody(key, params.get(key)); //设置请求参数
                }
            }

            HttpEntity httpEntity = builder.build();
            //将请求参数放入 HttpPost 请求体中
            //使用 httpEntity 后 Content-Type会自动被设置成 multipart/form-data
            httpPost.setEntity(httpEntity);
            if (prop != null) {
                for (int i = 0; i < prop.length; i++) {
                    httpPost.setHeader(prop[i][0], prop[i][1]);
                }
            }


            //httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            CloseableHttpResponse response = client.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // 必須用UTF-8 不然會有亂碼
                result = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
                logger.info("post result: " + result);
            } else if (statusCode == 404) {
                // 要回傳找不到給打API的人知道
                result = "404";
            } else {
                result = "";
            }
        } finally {
//            try {
//                client.close();
//            } catch (Exception e) {
//            }
        }
        return result;


    }

	public String base64(byte[] source) {
		Base64.Encoder encoder = Base64.getEncoder();
		return encoder.encodeToString(source);
	}

	public byte[] md5(String str) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			return md.digest(str.getBytes()); // 將 byte 陣列加密
		} catch (Exception ex) {
			System.out.println("錯誤的驗算法名稱");
			return null;
		}
	}

	// 2. 加密用
	public String Encryption(String content, String linkName, String date) {

		String post = "POST\n" + content + "\n" + "application/json" + "\n" + date + "\n" + linkName;

        String secret = globalConfig.APIM_CrossSell_SecretKey();
		
		System.out.println("#### AWSinfo - post = \n" + post);
		System.out.println("#### AWSinfo - secret = " + secret);
		
		// 先做hmacSha1再轉base64
		return base64(hmacSha1(post, secret));
	}

	// HMAC-SHA1
	public byte[] hmacSha1(String value, String key) {
		try {
			// Get an hmac_sha1 key from the raw key bytes
			byte[] keyBytes = key.getBytes();
			SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");

			Mac mac = Mac.getInstance("HmacSHA1");
			mac.init(signingKey);
			return mac.doFinal(value.getBytes());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}