package com.kgi.airloanex.ap.dao;

import static com.kgi.airloanex.common.PlContractConst.CaseDataJobStatus.CASE_STATUS_01_JobStatus_010;
import static com.kgi.airloanex.common.PlContractConst.CaseDataJobStatus.CASE_STATUS_01_JobStatus_020;
import static com.kgi.airloanex.common.PlContractConst.CaseDataJobStatus.CASE_STATUS_01_JobStatus_030;
import static com.kgi.airloanex.common.PlContractConst.CaseDataJobStatus.CASE_STATUS_01_JobStatus_040;
import static com.kgi.airloanex.common.PlContractConst.CaseDataSubStatus.CASE_STATUS_00_SubStatus_000;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import com.kgi.airloanex.ap.pdf.dto.PDFCaseDataSmall;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.PlContractConst.CaseDataJobStatus;
import com.kgi.airloanex.common.PlContractConst.CaseDataSubStatus;
import com.kgi.airloanex.common.dto.db.CaseData;
import com.kgi.eopend3.ap.dao.CRUDQDao;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.common.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class CaseDataDao extends CRUDQDao<CaseData> {

	public static final String NAME = "CaseData" ;

    private static final String WHERE_LOAN = " WHERE CaseNo like 'LOAN%' AND ";

	@Autowired
	ConfigDao configDao;	
    
    // @Autowired
    // @Qualifier("kPDB01DB")
    // DataSource kPDB01DataSource;

    public CaseData findByCaseNo(String caseno) {
        CaseData caseData = new CaseData(caseno);
        return Read(caseData);
    }

    public CaseData findByIdnoAndBirthday(String idno, String birthday, String status) {
        String sql = "SELECT top 1 * FROM " + NAME + WHERE_LOAN + "Idno = ? and birthday = ? and Status = ? order by UTime desc";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseData.class),
                    new Object[] { idno, birthday, status });
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料");           
            return null;
        }
    }

    @Override
    public int Create(CaseData fullItem) {
        String sql = "INSERT INTO " + NAME 
                + " (CaseNo, CaseNoWeb, CaseNoAps, GmCardNo, Idno, Status, ApsStatus, IsPreAudit, ProductId, Occupation, customer_name, birthday, gender, house_city_code, house_address, house_tel_area, house_tel, education, marriage, corp_name, corp_city_code, corp_address, corp_tel_area, corp_tel, corp_tel_exten, corp_class, corp_type, title, on_board_date, yearly_income, estate_type, p_apy_amount, mobile_tel, p_period, p_purpose, p_purpose_name, email_address, ip_address, prj_code, case_no_est, ContactMe, ContactMeFlag, AddPhotoFlag, OccupatioFlag, ImportantCust, AMLFlag, UTime,      UserType, ResAddrZipCode, ResAddr, ResTelArea, ResTel, AgentNo, AgentDepart, CreateTime, EndTime, SendedTime, SubStatus, Process, IdCardDate, IdCardLocation, IdCardCRecord, AddNewCaseTime, SendCustInfoTime, HasOldFinaImg, OldFinaImgNoList, CanUseOldFinaImg, HasOldIdImg, OldIdImgNoList, CanUseOldIdImg, HasUploadId, HasUploadFina, HasQryAml, HasQryIdInfo, HasQryZ07, HasFinishKyc, HasNoRelp, IsCaseIntegrated, ApplyWithoutFina, DecisionCaseNo, CheckSalaryAccount, CheckCreditcardAccount, CheckCashcardAccount, CheckKgibankAccount, CheckLoanAccount,  phone, OCR_FRONT_Idno , OCR_FRONT_BYPASSID, OCR_FRONT_RspCode, OCR_FRONT_Name, OCR_FRONT_IDCARD_ISSUE_DT, OCR_FRONT_IDCARD_ISSUE_Type, OCR_FRONT_IDCARD_ISSUE_City, OCR_BACK_HouseholdAddr1, OCR_BACK_HouseholdAddr2, OCR_BACK_HouseholdAddr3, OCR_BACK_HouseholdAddr4, OCR_BACK_Marriage, PJ )" 
        + " VALUES (:CaseNo,:CaseNoWeb,:CaseNoAps,:GmCardNo,:Idno,:Status,:ApsStatus,:IsPreAudit,:ProductId,:Occupation,:customer_name,:birthday,:gender,:house_city_code,:house_address,:house_tel_area,:house_tel,:education,:marriage,:corp_name,:corp_city_code,:corp_address,:corp_tel_area,:corp_tel,:corp_tel_exten,:corp_class,:corp_type,:title,:on_board_date,:yearly_income,:estate_type,:p_apy_amount,:mobile_tel,:p_period,:p_purpose,:p_purpose_name,:email_address,:ip_address,:prj_code,:case_no_est,:ContactMe,:ContactMeFlag,:AddPhotoFlag,:OccupatioFlag,:ImportantCust,:AMLFlag, getdate(), :UserType,:ResAddrZipCode,:ResAddr,:ResTelArea,:ResTel,:AgentNo,:AgentDepart, getdate(),  NULL,    NULL,      :SubStatus,:Process,:IdCardDate,:IdCardLocation,:IdCardCRecord,:AddNewCaseTime,:SendCustInfoTime,:HasOldFinaImg,:OldFinaImgNoList,:CanUseOldFinaImg,:HasOldIdImg,:OldIdImgNoList,:CanUseOldIdImg,:HasUploadId,:HasUploadFina,:HasQryAml,:HasQryIdInfo,:HasQryZ07,:HasFinishKyc,:HasNoRelp,:IsCaseIntegrated,:ApplyWithoutFina,:DecisionCaseNo,:CheckSalaryAccount,:CheckCreditcardAccount,:CheckCashcardAccount,:CheckKgibankAccount,:CheckLoanAccount, :phone,:OCR_FRONT_Idno ,:OCR_FRONT_BYPASSID,:OCR_FRONT_RspCode,:OCR_FRONT_Name,:OCR_FRONT_IDCARD_ISSUE_DT,:OCR_FRONT_IDCARD_ISSUE_Type,:OCR_FRONT_IDCARD_ISSUE_City,:OCR_BACK_HouseholdAddr1,:OCR_BACK_HouseholdAddr2,:OCR_BACK_HouseholdAddr3,:OCR_BACK_HouseholdAddr4,:OCR_BACK_Marriage,:PJ )";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public CaseData Read(CaseData keyItem) {
        String sql = "SELECT * FROM " + NAME + WHERE_LOAN + "CaseNo = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseData.class),
                    new Object[] { keyItem.getCaseNo() });
        } catch (DataAccessException ex) {
            logger.error(NAME+"查無資料");           
            // throw new ErrorResultException(9, NAME+"查無資料", ex);
            // ex.printStackTrace();
            return null;
        }
    }

    public CaseData ReadByCaseNo(String caseNo) {
        CaseData caseData = new CaseData(caseNo);
        return this.Read(caseData);
    }

    @Override
    public int Update(CaseData fullItem) {
        String sql = 
        "UPDATE " + NAME + " SET CaseNoWeb=:CaseNoWeb, CaseNoAps=:CaseNoAps, GmCardNo=:GmCardNo, Idno=:Idno," 
        + " Status=:Status, ApsStatus=:ApsStatus, IsPreAudit=:IsPreAudit, ProductId=:ProductId, Occupation=:Occupation," 
        + " customer_name=:customer_name, birthday=:birthday, gender=:gender, house_city_code=:house_city_code,"
        + " house_address=:house_address, house_tel_area=:house_tel_area, house_tel=:house_tel, education=:education, marriage=:marriage,"
        + " corp_name=:corp_name, corp_city_code=:corp_city_code, corp_address=:corp_address,"
        + " corp_tel_area=:corp_tel_area, corp_tel=:corp_tel, corp_tel_exten=:corp_tel_exten,"
        + " corp_class=:corp_class, corp_type=:corp_type, title=:title, on_board_date=:on_board_date,"
        + " yearly_income=:yearly_income, estate_type=:estate_type, p_apy_amount=:p_apy_amount, mobile_tel=:mobile_tel,"
        + " p_period=:p_period, p_purpose=:p_purpose, p_purpose_name=:p_purpose_name, email_address=:email_address, ip_address=:ip_address," 
        + " prj_code=:prj_code, case_no_est=:case_no_est, ContactMe=:ContactMe, ContactMeFlag=:ContactMeFlag, AddPhotoFlag=:AddPhotoFlag,"
        + " OccupatioFlag=:OccupatioFlag, ImportantCust=:ImportantCust, AMLFlag=:AMLFlag," 
        + " UTime=getDate(), UserType=:UserType, ResAddrZipCode=:ResAddrZipCode, ResAddr=:ResAddr, ResTelArea=:ResTelArea, ResTel=:ResTel," 
        + " AgentNo=:AgentNo, AgentDepart=:AgentDepart, EndTime=:EndTime, SendedTime=:SendedTime, SubStatus=:SubStatus, Process=:Process,"
        + " IdCardDate=:IdCardDate, IdCardLocation=:IdCardLocation, IdCardCRecord=:IdCardCRecord,"
        + " AddNewCaseTime=:AddNewCaseTime, SendCustInfoTime=:SendCustInfoTime, HasOldFinaImg=:HasOldFinaImg, OldFinaImgNoList=:OldFinaImgNoList,"
        + " CanUseOldFinaImg=:CanUseOldFinaImg, HasOldIdImg=:HasOldIdImg, OldIdImgNoList=:OldIdImgNoList, CanUseOldIdImg=:CanUseOldIdImg,"
        + " HasUploadId=:HasUploadId, HasUploadFina=:HasUploadFina, HasQryAml=:HasQryAml, HasQryIdInfo=:HasQryIdInfo, HasQryZ07=:HasQryZ07,"
        + " HasFinishKyc=:HasFinishKyc, HasNoRelp=:HasNoRelp, IsCaseIntegrated=:IsCaseIntegrated, ApplyWithoutFina=:ApplyWithoutFina,"
        + " DecisionCaseNo=:DecisionCaseNo, CheckSalaryAccount=:CheckSalaryAccount, CheckCreditcardAccount=:CheckCreditcardAccount,"
        + " CheckCashcardAccount=:CheckCashcardAccount, CheckKgibankAccount=:CheckKgibankAccount, CheckLoanAccount=:CheckLoanAccount,"
        + " phone=:phone, OCR_FRONT_Idno=:OCR_FRONT_Idno, OCR_FRONT_BYPASSID=:OCR_FRONT_BYPASSID, OCR_FRONT_RspCode=:OCR_FRONT_RspCode,"
        + " OCR_FRONT_Name=:OCR_FRONT_Name, OCR_FRONT_IDCARD_ISSUE_DT=:OCR_FRONT_IDCARD_ISSUE_DT,"
        + " OCR_FRONT_IDCARD_ISSUE_Type=:OCR_FRONT_IDCARD_ISSUE_Type, OCR_FRONT_IDCARD_ISSUE_City=:OCR_FRONT_IDCARD_ISSUE_City,"
        + " OCR_BACK_HouseholdAddr1=:OCR_BACK_HouseholdAddr1, OCR_BACK_HouseholdAddr2=:OCR_BACK_HouseholdAddr2,"
        + " OCR_BACK_HouseholdAddr3=:OCR_BACK_HouseholdAddr3, OCR_BACK_HouseholdAddr4=:OCR_BACK_HouseholdAddr4,"
        + " OCR_BACK_Marriage=:OCR_BACK_Marriage, PJ=:PJ "
        + WHERE_LOAN + "CaseNo=:CaseNo ";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(CaseData keyItem) {
        String sql = "DELETE FROM " + NAME + WHERE_LOAN + "CaseNo = ?";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getCaseNo() });
    }

    @Override
    public List<CaseData> Query(CaseData keyItem) {
		String sql = "SELECT * FROM " + NAME + WHERE_LOAN + "Status = ?";					
		try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getStatus() }, new BeanPropertyRowMapper<>(CaseData.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }

    // public int finish(String caseNo) {
    //     String sql = "UPDATE " + NAME + " set Status = ? , UpdateTime = getdate() , EndTime = getdate()  where CaseNo = ? ";
    //     return this.getJdbcTemplate().update(sql, PlContractConst.CASE_STATUS_WRITE_SUCCESS, caseNo);
    // }

    // public int updateStatus(String caseNo, String status) {
    //     String sql = "UPDATE " + NAME + " set Status = ? , UpdateTime = getdate()  where CaseNo = ? ";
    //     return this.getJdbcTemplate().update(sql, status, caseNo);
    // }

    // public int updateStatusAfterSendOrbit(String caseNo, String status) {
    //     String sql = "UPDATE " + NAME + " SET Status = ?,UpdateTime = getdate(),SendedTime=getdate() WHERE CaseNo = ?";
    //     return this.getJdbcTemplate().update(sql, status, caseNo);
    // }

    // public int getSuccessCaseData(String idno,String[] successStatus) {
    //     StringBuilder sb = new StringBuilder();
    //     List<String> params = new ArrayList<>();
    //     sb.append(" SELECT count(*) FROM " + NAME + " cd LEFT JOIN ED3_AOResult ao on cd.CaseNo = ao.CaseNo " );
    //     sb.append(" WHERE cd.idno = ? and ( ao.Status is null or ao.Status in (?,?) ) ");
    //     params.add(idno);
    //     params.add(PlContractConst.AORESULT_STATUS_SUCCESS);
    //     params.add(PlContractConst.AORESULT_STATUS_WAIT_UPLOAD);
    //     for (int i = 0; i < successStatus.length; i++) {
    //         if(i == 0 ){
    //             sb.append(" AND cd.Status IN ( ?, ");
    //         }else if(i + 1 == successStatus.length) {
    //             sb.append("? )");
    //         } else {
    //             sb.append("?,");
    //         }
    //     }
    //     params.addAll(Arrays.asList(successStatus));
        	
	// 	try {
    //         return this.getJdbcTemplate().queryForObject(sb.toString(),Integer.class, params.toArray());
    //     } catch (DataAccessException ex) {
    //         return 0;
    //     }
    // }

    // public List<String> getHoliDay(int bookDay) throws Exception {    
    //     Calendar cal = Calendar.getInstance();        
    //     SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    //     String startDate = sdf.format(cal.getTime());    
    //     cal.add(Calendar.DATE, bookDay);
    //     String endDate = sdf.format(cal.getTime());    
    //     String sql = " SELECT convert(varchar, DATA_DATE,23) as donotBookingDate FROM ODOAS.OAS_LN_Days where BUSINESSDAY= ' ' AND CONVERT(varchar,DATA_DATE,112) between ? and ?  order by DATA_DATE ";
    //     try {
    //         return this.getJdbcTemplate(kPDB01DataSource).queryForList(sql, new Object[] {startDate, endDate},String.class);
    //     } catch (DataAccessException ex) {							
    //         return null;
    //     }        
    // }

    public CaseData findByIdnoAndProductId(String idno, String productId) {
        String sql = "SELECT top 1 * FROM " + NAME + WHERE_LOAN + "Idno = ? AND ProductId = ? order by UTime desc";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseData.class),
                    new Object[] { idno, productId });
        } catch (DataAccessException ex) {
            logger.error(NAME+"查無資料"); 
            return null;
        }
    }

    public CaseData readInitData(String idno, String productId) {
        String sql = "SELECT top 1 * FROM " + NAME + WHERE_LOAN + "Idno = ? AND ProductId = ? AND Status in ( ? , ? , ? ) order by UTime desc";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseData.class),
                    new Object[] { idno, productId, PlContractConst.CASE_STATUS_INIT,PlContractConst.CASE_STATUS_WRITING, PlContractConst.CASE_STATUS_APPLYPAPER});
        } catch (DataAccessException ex) {
            logger.error(NAME+"查無資料"); 
            return null;
        }
    }


    public List<CaseData> getOver30DaysData() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -30);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String chkdate = sdf.format(cal.getTime());
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * from " + NAME + WHERE_LOAN + "CONVERT(varchar,CreateTime,112)<=? AND Status = ? ");
        try {
            return this.getJdbcTemplate().query(sb.toString(), new Object[] { chkdate, PlContractConst.CASE_STATUS_WRITING }, new BeanPropertyRowMapper<>(CaseData.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }


    // public int inDWPhoneSuccess(String phone) {
	// 	String sql = "SELECT count(*) FROM ODBNS.BNS_CUSVAA WHERE MOBILE_NO = ? OR MOBILE_NO2 = ? ";					
	// 	try {
    //         return this.getJdbcTemplate(kPDB01DataSource).queryForObject(sql,Integer.class, new Object[] { phone , phone });
    //     } catch (DataAccessException ex) {
    //         return 0;
    //     }
    // } 

    // public int inDWEmailSuccess(String emailAddress) {
	// 	String sql = "SELECT count(*) FROM ODBNS.BNS_CUMI WHERE EMAIL_ADD1 = ? OR EMAIL_ADD2 = ?";					
	// 	try {
    //         return this.getJdbcTemplate(kPDB01DataSource).queryForObject(sql,Integer.class, new Object[] { emailAddress  , emailAddress });
    //     } catch (DataAccessException ex) {
    //         return 0;
    //     }
    // }

    //
    public boolean isApplyLoanCC(String caseno) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        boolean isApplyLoanCC = false;
        String sql = " SELECT * FROM dbo.CreditCaseData WHERE UniqId= ?";
        List<Map<String, Object>> ls = jdbcTemplate.queryForList(sql, new Object[]{caseno});
        if (ls.size() > 0) {
            isApplyLoanCC = true;
        }
        return isApplyLoanCC;
    }

    public List<Map<String, Object>> queryCaseDoc(String caseno, String idno) throws Exception {
        // 取得待處理的專人處理聯絡case名單
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        try {
            List<Map<String, Object>> ls = jdbcTemplate.queryForList(
                    "SELECT PdfContent,Image FROM dbo.CaseDoc  WHERE CaseNo = ? and idno = ?",
                    new Object[]{caseno, idno});
            return ls;
        } catch (DataAccessException e) {
//            e.printStackTrace();
            return null;
        }

    }

    public List<CaseData> queryDatabyCaseNo(String caseNo) throws Exception {
        try {
            return this.getJdbcTemplate().query(
                " SELECT * FROM CaseData " + WHERE_LOAN + "Caseno = ?", 
                new Object[]{caseNo}, 
                new BeanPropertyRowMapper<>(CaseData.class));
        } catch (DataAccessException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * 1. 等待 - 第一次開始產生申請書PDF
     *    貸款打包案件給影像系統
     *    [Status JobStatus]=01 010
     */
    public List<PDFCaseDataSmall> queryWaitPhotoCase() throws Exception {
        List<PDFCaseDataSmall> ls = new ArrayList<>();
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        try {
            ls = jdbcTemplate.query(
                " SELECT cd.CaseNo, cd.case_no_est, cd.Idno, cd.customer_name, cd.CaseNoWeb, cd.IsPreAudit, cd.Occupation, cd.title, case when ccd.UniqType is not null then ccd.UniqType else '' end UniqType  FROM dbo.CaseData cd "
                        + " left join CreditCaseData ccd on cd.CaseNo = ccd.UniqId WHERE cd.CaseNo like 'LOAN%' AND (cd.Status = ? and cd.JobStatus = ?)",
                new Object[]{
                    CASE_STATUS_01_JobStatus_010.getStatus(),
                    CASE_STATUS_01_JobStatus_010.getJobStatus(),
                }, 
                new BeanPropertyRowMapper<>(PDFCaseDataSmall.class));
            return ls;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return ls;
        }
    }

    public List<PDFCaseDataSmall> queryUploadIdcardOrFinpf() throws Exception {
        List<PDFCaseDataSmall> ls = new ArrayList<>();
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        try {
            ls = jdbcTemplate.query(
                " SELECT cd.CaseNo, cd.case_no_est, cd.Idno, cd.customer_name, cd.CaseNoWeb, cd.IsPreAudit, cd.Occupation, cd.title, case when ccd.UniqType is not null then ccd.UniqType else '' end UniqType  FROM dbo.CaseData cd "
                    + " left join CreditCaseData ccd on cd.CaseNo = ccd.UniqId" 
                    + " WHERE cd.CaseNo like 'LOAN%' AND (cd.JobStatus = ?)",
                new Object[]{
                    CASE_STATUS_01_JobStatus_020.getJobStatus(),
                }, 
                new BeanPropertyRowMapper<>(PDFCaseDataSmall.class));
            return ls;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return ls;
        }
    }

    /**
     * 等待 - 第二次開始產生申請書PDF
     */
    public List<PDFCaseDataSmall> queryWaitPhotoCaseByUniqId(String uniqId) throws Exception {
        List<PDFCaseDataSmall> ls = new ArrayList<>();
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        try {
            ls = jdbcTemplate.query(
                " SELECT cd.CaseNo, cd.case_no_est, cd.Idno, cd.customer_name, cd.CaseNoWeb, cd.IsPreAudit, cd.Occupation, cd.title, cd.ProductId, case when ccd.UniqType is not null then ccd.UniqType else '' end UniqType  FROM dbo.CaseData cd "
                        + " left join CreditCaseData ccd on cd.CaseNo = ccd.UniqId WHERE cd.CaseNo like 'LOAN%' AND cd.CaseNo = ?",
                new Object[]{ uniqId },
                new BeanPropertyRowMapper<>(PDFCaseDataSmall.class));
            return ls;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return ls;
        }
    }

    /**
     * 2. 等待 - 開始產生KYC表PDF
     *    KYC 表上傳ftp
     *    [Status JobStatus]=01 020
     */
    public List<PDFCaseDataSmall> queryWaitPhotoCaseKyc() throws Exception {
        List<PDFCaseDataSmall> ls = new ArrayList<>();
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        try {
            ls = jdbcTemplate.query(
                " SELECT cd.CaseNo, cd.case_no_est, cd.Idno, cd.customer_name, cd.CaseNoWeb, cd.IsPreAudit, cd.Occupation, cd.title, case when ccd.UniqType is not null then ccd.UniqType else '' end UniqType  FROM dbo.CaseData cd "
                    + " left join CreditCaseData ccd on cd.CaseNo = ccd.UniqId WHERE cd.CaseNo like 'LOAN%' AND (cd.JobStatus = ?)",
                new Object[]{
                    CASE_STATUS_01_JobStatus_020.getJobStatus(),
                }, 
                new BeanPropertyRowMapper<>(PDFCaseDataSmall.class));
            return ls;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return ls;
        }
    }

    /**
     * 3. 等待 - UPDATE_CASE_INFO 發查聯徵中
     *    發查聯徵
     *    [Status JobStatus]=01 030
     */
    public List<CaseData> queryWaitJcic() {
        List<CaseData> list = new ArrayList<>();
		String sql = "SELECT * FROM " + NAME + WHERE_LOAN + "Status = ? AND JobStatus = ?";
		try {
            list = this.getJdbcTemplate().query(sql, 
                new Object[] {
                    CASE_STATUS_01_JobStatus_030.getStatus(),
                    CASE_STATUS_01_JobStatus_030.getJobStatus(),
                }, 
                new BeanPropertyRowMapper<>(CaseData.class));
            return list;
        } catch (DataAccessException ex) {
            ex.printStackTrace();
            return list;
        }
    }

    /**
     * 4. 等待 - 第二次開始產生申請書PDF(含ID財力圖檔)
     *    online補件資料上傳ftp
     *    [Status JobStatus]=01 040
     */
    public List<CaseData> queryWaitPhotoCaseAddon() throws Exception {
        List<CaseData> ls = new ArrayList<>();
        try {
            return this.getJdbcTemplate().query(
                " SELECT * FROM CaseData " + WHERE_LOAN + " 1=1 " 
                + " AND Status = ? AND JobStatus = ?", 
                new Object[]{
                    CASE_STATUS_01_JobStatus_040.getStatus(),
                    CASE_STATUS_01_JobStatus_040.getJobStatus(),
                }, 
                new BeanPropertyRowMapper<>(CaseData.class));
        } catch (DataAccessException e) {
            e.printStackTrace();
            return ls;
        }
    }

    /**
     * 4. 等待 - 第二次開始產生申請書PDF(含ID財力圖檔)
     *    online補件資料上傳ftp
     *    [Status JobStatus]=01 040
     */
    public List<CaseData> queryWaitPhotoCaseAddon(String caseNo) throws Exception {
        List<CaseData> ls = new ArrayList<>();
        try {
            return this.getJdbcTemplate().query(
                " SELECT * FROM CaseData " + WHERE_LOAN + "Caseno = ?" 
                + " AND Status = ? AND JobStatus = ?", 
                new Object[]{
                    caseNo,
                    CASE_STATUS_01_JobStatus_040.getStatus(),
                    CASE_STATUS_01_JobStatus_040.getJobStatus(),
                }, 
                new BeanPropertyRowMapper<>(CaseData.class));
        } catch (DataAccessException e) {
            e.printStackTrace();
            return ls;
        }
    }

    public void updateSubStatusByCaseNo(String caseno, CaseDataSubStatus subStatus) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set Status = ?, SubStatus = ?" + WHERE_LOAN + "CaseNo = ?;", new Object[] { subStatus.getStatus(), subStatus.getSubStatus(), caseno });
		logger.info("#### " + caseno + " 更新狀態成: status=" + subStatus.getStatus() + ", subStatus=" + subStatus.getSubStatus());
		writeCurrentStatusInApiLog(caseno,"");
    }

    public void updateSubStatusByCaseNo(String caseno, CaseDataSubStatus subStatus, CaseDataJobStatus jobStatus) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set Status = ?, SubStatus = ?, JobStatus = ?" + WHERE_LOAN + "CaseNo = ?;", new Object[] { 
            subStatus.getStatus(), 
            subStatus.getSubStatus(), 
            jobStatus.getJobStatus(),
            caseno});
		logger.info("#### " + caseno + " 更新狀態成: status=" + subStatus.getStatus() + ", subStatus=" + subStatus.getSubStatus() + ", jobStatus=" + jobStatus.getJobStatus());
		writeCurrentStatusInApiLog(caseno,"");

    }

    public void updateJobStatusByCaseNo(String caseno, CaseDataJobStatus jobStatus) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set Status = ?, JobStatus = ?" + WHERE_LOAN + "CaseNo = ?;", new Object[] { jobStatus.getStatus(), jobStatus.getJobStatus(), caseno});
		logger.info("#### " + caseno + " 更新狀態成: status=" + jobStatus.getStatus() + ", jobStatus=" + jobStatus.getJobStatus());
		writeCurrentStatusInApiLog(caseno,"");
    }

    public void updateJobStatusOnly(String caseno, String jobStatus) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set JobStatus = ?" + WHERE_LOAN + "CaseNo = ?;", new Object[] { jobStatus, caseno });
		logger.info("#### " + caseno + " 更新狀態成: jobStatus=" + jobStatus);
		writeCurrentStatusInApiLog(caseno,"");
    }
	
	//紀錄目前狀態到APILOG
	public void writeCurrentStatusInApiLog(String CaseNo,String Memo){
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		jdbcTemplate.update("insert into ContractApiLog (keyinfo,apiname,request,response,starttime,endtime,CreateTime,used)  select ?,'/nowCaseStatus',ISNULL(status,'-')+'|'+ISNULL(substatus,'-')+'|'+ISNULL(jobstatus,'-'),?,getdate(),getdate(),getdate(),0 from casedata where caseno=?;", new Object[] { CaseNo, Memo ,CaseNo });
	}
    
    public void updateJcic(String caseno, String jcicOk) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set JcicOk = ?, JcicTime = getdate()" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                jcicOk, 
                caseno 
            });
    }
    /** 更新 發查Z07 回傳聯徵資料 */
    public void updateZ07(String caseno, String Z07Html, String Z07Result, String Z07Time) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set Z07Html = ?, Z07Result = ?, Z07Time = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                Z07Html, 
                Z07Result, 
                Z07Time, 
                caseno 
            });
    }
    /** 更新 風險評級:高 中 低 */
    public void updateCddRateRisk(String caseno, String risk) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set CddRateRisk = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                risk,
                caseno 
            });
    }
    /** 更新 發查AML */
    public void updateAml(String caseno, String AmlResult, String AmlTime) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set AmlResult = ?, AmlTime = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                AmlResult, 
                AmlTime,
                caseno 
            });
    }
    /** 更新 發查利害關係人 */
    public void updateRelp(String caseno, String checkRelpResult, String checkRelpTime) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set CheckRelpResult = ?, CheckRelpTime = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                checkRelpResult, 
                checkRelpTime,
                caseno 
            });
    }
    /** 更新 取得案件申請評分等..資訊 */
    public void updateDgtcaseResult(String caseno, String dgtcaseResult, String dgtcaseTime) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set DgtCaseResult = ?, DgtCaseTime = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                dgtcaseResult, 
                dgtcaseTime,
                caseno 
            });
    }
    /** 更新 影像系統新API(GetCaseComplete)檢核是否後端整件狀態已完整 */
    public void updateImgSysCaseComplete(String caseno, String imgSysCaseComplete, String imgSysCaseCompleteTime) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set ImgSysCaseComplete = ?, ImgSysCaseCompleteTime = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                imgSysCaseComplete, 
                imgSysCaseCompleteTime,
                caseno 
            });
    }
    /** 更新 同意使用與貴行往來之資料申辦貸款 */
    public void updateLoan_personal_data(String caseno, String loan_personal_data_Result, String loan_personal_data_Time) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set loan_personal_data_Result = ?, loan_personal_data_Time = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                loan_personal_data_Result, 
                loan_personal_data_Time,
                caseno 
            });
    }
    /** 更新 理債平台的前一案件編號 */
    public void updateCaseNoApsPrev(String caseno, String caseNoApsPrev) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set CaseNoApsPrev = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] {
                caseNoApsPrev,
                caseno 
            });
    }
    /** 更新 專案代號 */
    public void updatePrj_code(String caseno, String prj_code) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set prj_code = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                prj_code, 
                caseno 
            });
    }

    public void updateGmCardNo(String caseno, String GmCardNo) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set GmCardNo = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                GmCardNo, 
                caseno 
            });
    }

    public void updateINFOSOURCE_CDE(String caseno, String INFOSOURCE_CDE) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set INFOSOURCE_CDE = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                INFOSOURCE_CDE, 
                caseno 
            });
    }

    public void updatePcode2566_phone(String caseno, String pcode2566_phone) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set pcode2566_phone = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                pcode2566_phone, 
                caseno 
            });
    }

    public void updateSubcaseType(String caseno, String subcaseType) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set subcaseType = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                subcaseType, 
                caseno 
            });
    }

    public void updatecAbnormalReason(String caseno, String cAbnormalReason) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set cAbnormalReason = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                cAbnormalReason, 
                caseno 
            });
    }

    public void updateIsCaseIntegrated(String caseno, String IsCaseIntegrated) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set IsCaseIntegrated = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                IsCaseIntegrated, 
                caseno 
            });
    }

    public void updateCHECK_NO_FINPF_MEMO(String caseno, String CHECK_NO_FINPF_MEMO) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set CHECK_NO_FINPF_MEMO = ?" + WHERE_LOAN + "CaseNo = ?;", 
            new Object[] { 
                CHECK_NO_FINPF_MEMO, 
                caseno 
            });
    }

    public List<Map<String, Object>> queryRejectCase() throws Exception {
        // 取得拒貸case名單
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        try {
            List<Map<String, Object>> ls = jdbcTemplate.queryForList(
                    "SELECT c.*,promo.PromoDepart,promo.PromoMember FROM CaseData c LEFT JOIN QR_PromoCase promo on promo.UniqId = c.CaseNo WHERE c.CaseNo like 'LOAN%' AND c.Status = ?",
                    new Object[]{PlContractConst.CASE_STATUS_REJECT});
            return ls;
        } catch (DataAccessException e) {
//            e.printStackTrace();
            return null;
        }

    }

    public List<Map<String,Object>> getCaseDataForCheckHighRiskOccupation(String uniqId){
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        String sql = "SELECT Occupation occupation,title title from CaseData" + WHERE_LOAN + "CaseNo=?";
        return jdbcTemplate.queryForList(sql, new Object[]{uniqId});
    }

    public void updateAddPhotoFlag(String caseNo, String flag) throws Exception {
        // 建立DB連線及查詢字串
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set AddPhotoFlag = ?" + WHERE_LOAN + "CaseNo = ?;", new Object[]{flag, caseNo});

    }
	
	//2021-05-26 added
    public void updateAddNewCaseTime(String caseNo, String timeStamp) throws Exception {
        // 建立DB連線及查詢字串
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set AddNewCaseTime = ?" + WHERE_LOAN + "CaseNo = ?;", new Object[]{timeStamp,caseNo});

    }
	
	// //2021-05-26 added
    // public String checkAddNewCaseTime(String caseNo) {
                
		// String sql = "SELECT top 1 CaseNo FROM dbo.CaseData  WITH (NOLOCK) WHERE idno = ? AND Status>='00' AND CaseNo in (Select Uniqid from MemoData WITH (NOLOCK) ) order by CASENO Desc ";
        // try {
            // CaseData caseData = this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseData.class),
                // new Object[] { idno });
			// return caseData.getCaseNo();
		// } catch (DataAccessException ex) {
			// System.out.println("#### idno : " + idno + " 在 CaseData 查無對應資料.");
			// return null;
		// }
    // }		
	

    // 補件狀態-已補件
    public void isAddPhoto(String caseNo) throws Exception {
        String photoFlag = "1";// 已補件
        // 更新AddPhotoFlag狀態
        updateAddPhotoFlag(caseNo, photoFlag);
    }

    /**
     * 重新申請，則取消斷點，重新寫一筆新的主檔CaseData
     */
    public void createNewPoint(CaseData caseData) {
        String idno = caseData.getIdno();
        String productId = caseData.getProductId();
        caseData.setupSubStatus(CASE_STATUS_00_SubStatus_000);
        cancelPreviousBreakPoint(idno, productId);
        Create(caseData);
    }

    /**
     * 斷點取消
     * 若選擇重新申請，則取消斷點，將先前 00, 01 的狀態更新為 07
     */
    private void cancelPreviousBreakPoint(String idno, String productId) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set Status = ?" + WHERE_LOAN + "Idno = ? AND ProductId = ? AND Status in ( ? , ? ) AND CaseNoWeb is null", 
            new Object[]{PlContractConst.CASE_STATUS_CANCEL, idno, productId, PlContractConst.CASE_STATUS_INIT, PlContractConst.CASE_STATUS_WRITING});
    } // end cancelPreviousBreakPoint

    public void updateLaterAddonSendTime(String caseNo) {
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update CaseData set LaterAddonSendTime = ?" + WHERE_LOAN + "CaseNo = ?;", new Object[]{DateUtil.GetDateFormatString("yyyy-MM-dd HH:mm:ss.SSS"), caseNo});
    }
   
    public String findCaseNoByCaseNoWeb(String caseNoWeb) {
        
		String sql = "SELECT top 1 * FROM dbo.CaseData WHERE caseNoWeb = ?";
		try {
            CaseData caseData = this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseData.class),
                new Object[] { caseNoWeb });
			return caseData.getCaseNo();
		} catch (DataAccessException ex) {
			System.out.println("#### caseNoWeb : " + caseNoWeb + " 在 CaseData 查無對應資料.");
			return null;
		}
    }

    public String findCaseNoWebByCaseNo(String caseNo) {
        
		String sql = "SELECT top 1 * FROM dbo.CaseData WHERE CaseNo = ?";
		try {
            CaseData caseData = this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseData.class),
                new Object[] { caseNo });
			return caseData.getCaseNoWeb();
		} catch (DataAccessException ex) {
			System.out.println("#### caseNo : " + caseNo + " 在 CaseData 查無對應資料.");
			return null;
		}
    }    
    public String findStatusByCaseNo(String caseNo) {
        
		String sql = "SELECT top 1 * FROM dbo.CaseData WHERE CaseNo = ?";
		try {
            CaseData caseData = this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseData.class),
                new Object[] { caseNo });
			return caseData.getStatus();
		} catch (DataAccessException ex) {
			System.out.println("#### caseNo : " + caseNo + " 在 CaseData 查無對應資料.");
			return null;
		}
    } 
    public String findAddNewCaseTimeByCaseNo(String caseNo) {
        
		String sql = "SELECT top 1 * FROM dbo.CaseData WHERE caseNo = ?";
		try {
            CaseData caseData = this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseData.class),
                new Object[] { caseNo });
			return caseData.getAddNewCaseTime();
		} catch (DataAccessException ex) {
			System.out.println("#### caseNo : " + caseNo + " 在 CaseData 查無對應資料.");
			return null;
		}
    }

	
    public String findCaseNoByIdno(String idno) {
                
		String sql = "SELECT top 1 CaseNo FROM dbo.CaseData  WITH (NOLOCK) WHERE idno = ? AND Status>='00' AND CaseNo in (Select Uniqid from MemoData WITH (NOLOCK) ) order by CASENO Desc ";
        try {
            CaseData caseData = this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseData.class),
                new Object[] { idno });
			return caseData.getCaseNo();
		} catch (DataAccessException ex) {
			System.out.println("#### idno : " + idno + " 在 CaseData 查無對應資料.");
			return null;
		}
    }	
	
    public String findCaseNoByToken(String token) {
		
		String sTSQLFromConfig = configDao.ReadConfigValue("Airloan.Crosell.RT.QueryToken.TSQL","");
		
		System.out.println("#### TSQL parameter= " + sTSQLFromConfig);
		
		String sql = "";
		
		if (!sTSQLFromConfig.equals(null) && !sTSQLFromConfig.equals("") && sTSQLFromConfig.length()>1){
			
			sql = sTSQLFromConfig;
			System.out.println("#### TSQL from DB= " + sql);
			
		}else{
			
			sql = "select top 1 CaseNo from CASEDATA with (NOLOCK)  WHERE Utime>'2020-12-10' and status not in ('00') and caseno in (select uniqid from memodata with (NOLOCK) where CreateTime>'2020-12-10' and SUBSTRING(Memo, CHARINDEX('token', Memo, 0) + 8, 128)=? ) order by CaseNo desc";
			
			System.out.println("#### TSQL from Code= " + sql);
		}
		
        
        try {
            CaseData caseData = this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseData.class),
                new Object[] { token });
			return caseData.getCaseNo();
		} catch (DataAccessException ex) {
			System.out.println("#### 在 CaseData 查無對應資料.");
			return null;
		}

    }
	
    public String findCaseNoByTokenUseMemoTokenInfo(String token) {
		
        String sql = "select top 1 CaseNo from CASEDATA with (NOLOCK)  WHERE status not in ('00') and caseno in (select uniqid from MemoTokenInfo with (NOLOCK) where token = ? ) order by CaseNo desc";
        
        try {
            CaseData caseData = this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseData.class),
                new Object[] { token });
			return caseData.getCaseNo();
		} catch (DataAccessException ex) {
			System.out.println("#### 在 CaseData 查無對應資料.");
			return null;
		}

    }	
}
