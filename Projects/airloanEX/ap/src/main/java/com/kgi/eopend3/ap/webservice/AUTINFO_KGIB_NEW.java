/**
 * AUTINFO_KGIB_NEW.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.kgi.eopend3.ap.webservice;
@SuppressWarnings("all")
public class AUTINFO_KGIB_NEW  implements java.io.Serializable {
    private java.lang.String CHECKFLAG;

    private java.lang.String ID;

    private java.lang.String CUSTOMERNAME;

    private java.lang.String a_ITEM01;

    private java.lang.String FROMIP;

    private java.lang.String VERNO;

    private java.lang.String LASTMODIFIED;

    public AUTINFO_KGIB_NEW() {
    }

    public AUTINFO_KGIB_NEW(
           java.lang.String CHECKFLAG,
           java.lang.String ID,
           java.lang.String CUSTOMERNAME,
           java.lang.String a_ITEM01,
           java.lang.String FROMIP,
           java.lang.String VERNO,
           java.lang.String LASTMODIFIED) {
           this.CHECKFLAG = CHECKFLAG;
           this.ID = ID;
           this.CUSTOMERNAME = CUSTOMERNAME;
           this.a_ITEM01 = a_ITEM01;
           this.FROMIP = FROMIP;
           this.VERNO = VERNO;
           this.LASTMODIFIED = LASTMODIFIED;
    }


    /**
     * Gets the CHECKFLAG value for this AUTINFO_KGIB_NEW.
     * 
     * @return CHECKFLAG
     */
    public java.lang.String getCHECKFLAG() {
        return CHECKFLAG;
    }


    /**
     * Sets the CHECKFLAG value for this AUTINFO_KGIB_NEW.
     * 
     * @param CHECKFLAG
     */
    public void setCHECKFLAG(java.lang.String CHECKFLAG) {
        this.CHECKFLAG = CHECKFLAG;
    }


    /**
     * Gets the ID value for this AUTINFO_KGIB_NEW.
     * 
     * @return ID
     */
    public java.lang.String getID() {
        return ID;
    }


    /**
     * Sets the ID value for this AUTINFO_KGIB_NEW.
     * 
     * @param ID
     */
    public void setID(java.lang.String ID) {
        this.ID = ID;
    }


    /**
     * Gets the CUSTOMERNAME value for this AUTINFO_KGIB_NEW.
     * 
     * @return CUSTOMERNAME
     */
    public java.lang.String getCUSTOMERNAME() {
        return CUSTOMERNAME;
    }


    /**
     * Sets the CUSTOMERNAME value for this AUTINFO_KGIB_NEW.
     * 
     * @param CUSTOMERNAME
     */
    public void setCUSTOMERNAME(java.lang.String CUSTOMERNAME) {
        this.CUSTOMERNAME = CUSTOMERNAME;
    }


    /**
     * Gets the a_ITEM01 value for this AUTINFO_KGIB_NEW.
     * 
     * @return a_ITEM01
     */
    public java.lang.String getA_ITEM01() {
        return a_ITEM01;
    }


    /**
     * Sets the a_ITEM01 value for this AUTINFO_KGIB_NEW.
     * 
     * @param a_ITEM01
     */
    public void setA_ITEM01(java.lang.String a_ITEM01) {
        this.a_ITEM01 = a_ITEM01;
    }


    /**
     * Gets the FROMIP value for this AUTINFO_KGIB_NEW.
     * 
     * @return FROMIP
     */
    public java.lang.String getFROMIP() {
        return FROMIP;
    }


    /**
     * Sets the FROMIP value for this AUTINFO_KGIB_NEW.
     * 
     * @param FROMIP
     */
    public void setFROMIP(java.lang.String FROMIP) {
        this.FROMIP = FROMIP;
    }


    /**
     * Gets the VERNO value for this AUTINFO_KGIB_NEW.
     * 
     * @return VERNO
     */
    public java.lang.String getVERNO() {
        return VERNO;
    }


    /**
     * Sets the VERNO value for this AUTINFO_KGIB_NEW.
     * 
     * @param VERNO
     */
    public void setVERNO(java.lang.String VERNO) {
        this.VERNO = VERNO;
    }


    /**
     * Gets the LASTMODIFIED value for this AUTINFO_KGIB_NEW.
     * 
     * @return LASTMODIFIED
     */
    public java.lang.String getLASTMODIFIED() {
        return LASTMODIFIED;
    }


    /**
     * Sets the LASTMODIFIED value for this AUTINFO_KGIB_NEW.
     * 
     * @param LASTMODIFIED
     */
    public void setLASTMODIFIED(java.lang.String LASTMODIFIED) {
        this.LASTMODIFIED = LASTMODIFIED;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AUTINFO_KGIB_NEW)) return false;
        AUTINFO_KGIB_NEW other = (AUTINFO_KGIB_NEW) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CHECKFLAG==null && other.getCHECKFLAG()==null) || 
             (this.CHECKFLAG!=null &&
              this.CHECKFLAG.equals(other.getCHECKFLAG()))) &&
            ((this.ID==null && other.getID()==null) || 
             (this.ID!=null &&
              this.ID.equals(other.getID()))) &&
            ((this.CUSTOMERNAME==null && other.getCUSTOMERNAME()==null) || 
             (this.CUSTOMERNAME!=null &&
              this.CUSTOMERNAME.equals(other.getCUSTOMERNAME()))) &&
            ((this.a_ITEM01==null && other.getA_ITEM01()==null) || 
             (this.a_ITEM01!=null &&
              this.a_ITEM01.equals(other.getA_ITEM01()))) &&
            ((this.FROMIP==null && other.getFROMIP()==null) || 
             (this.FROMIP!=null &&
              this.FROMIP.equals(other.getFROMIP()))) &&
            ((this.VERNO==null && other.getVERNO()==null) || 
             (this.VERNO!=null &&
              this.VERNO.equals(other.getVERNO()))) &&
            ((this.LASTMODIFIED==null && other.getLASTMODIFIED()==null) || 
             (this.LASTMODIFIED!=null &&
              this.LASTMODIFIED.equals(other.getLASTMODIFIED())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCHECKFLAG() != null) {
            _hashCode += getCHECKFLAG().hashCode();
        }
        if (getID() != null) {
            _hashCode += getID().hashCode();
        }
        if (getCUSTOMERNAME() != null) {
            _hashCode += getCUSTOMERNAME().hashCode();
        }
        if (getA_ITEM01() != null) {
            _hashCode += getA_ITEM01().hashCode();
        }
        if (getFROMIP() != null) {
            _hashCode += getFROMIP().hashCode();
        }
        if (getVERNO() != null) {
            _hashCode += getVERNO().hashCode();
        }
        if (getLASTMODIFIED() != null) {
            _hashCode += getLASTMODIFIED().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AUTINFO_KGIB_NEW.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "AUTINFO_KGIB_NEW"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CHECKFLAG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CHECKFLAG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMERNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CUSTOMERNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("a_ITEM01");
        elemField.setXmlName(new javax.xml.namespace.QName("", "A_ITEM01"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FROMIP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FROMIP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VERNO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VERNO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LASTMODIFIED");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LASTMODIFIED"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
