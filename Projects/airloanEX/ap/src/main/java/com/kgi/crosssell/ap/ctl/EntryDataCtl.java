package com.kgi.crosssell.ap.ctl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fuco.prod.common.util.HttpUtil2;
import com.kgi.crosssell.ap.service.EntryDataService;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.json.JSONObject;
@Controller
@RequestMapping("/Entry")
public class EntryDataCtl {
	@Autowired
	EntryDataService entryService;
	@RequestMapping(value = "/saveEntryData", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	@ResponseBody
	public void saveIndexClickRecord(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// 將取得的request轉為 json
		try {
			//[2019.10.31 GaryLiu] ==== START : 白箱 : 修改 Parameter Tampering ====
			String stringForRequest = HttpUtil2.getPostData(request, "UTF-8");
			ESAPI.validator().isValidInput("", stringForRequest, "SafeJson", 10000, false);
			JSONObject req = JSONObject.fromObject(stringForRequest);
			//[2019.10.31 GaryLiu] ====  END  : 白箱 : 修改 Parameter Tampering ====
//			System.out.println("saveEntryData: request = " + req.toString());
			String uniqId = req.getString("UniqId");
			String uniqType = req.getString("UniqType");
			String entry = req.optString("Entry", "");
			String member = req.optString("Member", "");
			String process = req.optString("Process", "");
			String browser = req.optString("Browser", "");
			String platform = req.optString("Platform", "");
			String os = req.optString("OS", "");
			String other = req.optString("Other","");
			
			//[2019.10.01 GaryLiu] ==== START : 修正Parameter Tampering ====
			uniqId = ESAPI.validator().getValidInput("", uniqId, "AntiXSS", Integer.MAX_VALUE, false);
			//[2019.10.01 GaryLiu] ====  END  : 修正Parameter Tampering ====
			
			// 寫入log檔
			//先刪除相同uniqid再新增
			entryService.deleteEntryData(uniqId);
			entryService.saveEntryData(uniqId, uniqType, entry, member, process, browser, platform, os, other);
		} catch (Exception e) {
			return;
		}
	}
}
