package com.kgi.airloanex.ap.dao;

import static com.kgi.airloanex.common.PlContractConst.KB_9743_DEPART_ID;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.kgi.airloanex.common.dto.db.QR_ChannelDepartList;
import com.kgi.airloanex.common.dto.db.QR_ChannelDepartListEOP;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class QR_ChannelDepartListDao extends CRUDQDao<QR_ChannelDepartList> {
    public static final String NAME = "QR_ChannelDepartList";

    @Override
    public int Create(QR_ChannelDepartList fullItem) {
        return 0;
    }

    @Override
    public QR_ChannelDepartList Read(QR_ChannelDepartList keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE ChannelId = ? AND DepartId = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_ChannelDepartList.class),
                    new Object[] { keyItem.getChannelId(), keyItem.getDepartId() });
        } catch (DataAccessException ex) {
            System.out.println("QR_ChannelDepartList查無資料");
            return null;
        }
    }

    @Override
    public int Update(QR_ChannelDepartList fullItem) {
        return 0;
    }

    @Override
    public int Delete(QR_ChannelDepartList keyItem) {
        return 0;
    }

    @Override
    public List<QR_ChannelDepartList> Query(QR_ChannelDepartList keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE ChannelId = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getChannelId() },
                    new BeanPropertyRowMapper<>(QR_ChannelDepartList.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }

    public String copyChannelDepartListToEOP() {
        String preSql = "Select count(*) as serial from " + NAME;
        Map<String, Object> tempMap = this.getJdbcTemplate().queryForMap(preSql);
        int maxSerial = Integer.valueOf(String.valueOf(tempMap.get("serial")));
        int min = 0;
        while (true) {
            int max = min + 500 < maxSerial ? min + 500 : maxSerial;
            String sql = "SELECT ChannelID,DepartID from " + NAME;
            List<Map<String, Object>> maps = this.getJdbcTemplate().queryForList(sql);
            List<QR_ChannelDepartListEOP> qr_ChannelDepartListEOPs = new ArrayList<>();
            maps.forEach((map) -> {
                QR_ChannelDepartListEOP qr_ChannelDepartListEOP = new QR_ChannelDepartListEOP();
                qr_ChannelDepartListEOP.setChannelId(String.valueOf(map.get("ChannelID")));
                qr_ChannelDepartListEOP.setDepartId(String.valueOf(map.get("DepartID")));
                qr_ChannelDepartListEOPs.add(qr_ChannelDepartListEOP);
            });
            int[] ints = this.batchCreate(qr_ChannelDepartListEOPs);
            List<Integer> collect = Arrays.stream(ints).boxed().collect(Collectors.toList());
            System.out.println(collect);

            if (maxSerial == max) {
                break;
            }
            min = max;
            break;
        }

        return "工作完成";
    }

    public int[] batchCreate(List<QR_ChannelDepartListEOP> items) {
        String sql = "INSERT INTO QR_ChannelDepartListEOP (ChannelId, DepartId, PromoDepart1, PromoMember1, PromoDepart2, PromoMember2, AssignDepart) "
                + " VALUES (?, ?, ?, ?, ?, ?, ?);";
        return this.getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, items.get(i).getChannelId());
                ps.setString(2, items.get(i).getDepartId());
                ps.setString(3, items.get(i).getPromoDepart1());
                ps.setString(4, items.get(i).getPromoMember1());
                ps.setString(5, items.get(i).getPromoDepart2());
                ps.setString(6, items.get(i).getPromoMember2());
                ps.setString(7, items.get(i).getAssignDepart());
            }

            @Override
            public int getBatchSize() {
                return items.size();
            }
        });
    }

    public QR_ChannelDepartList likeDepartId(QR_ChannelDepartList keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE ChannelId = ? AND DepartId like ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_ChannelDepartList.class),
                    new Object[] { keyItem.getChannelId(), keyItem.getDepartId() + "%" });
        } catch (DataAccessException ex) {
            System.out.println("QR_ChannelDepartList查無資料");
            return null;
        }
    }

    public String getSendAddress(String channelId, String departId) {
        String sql = "";
        if (StringUtils.equals("974", departId)) {
            departId = KB_9743_DEPART_ID;
            sql = "SELECT * FROM " + NAME + " WHERE ChannelId = ? AND LEFT(DepartId, 4) = ?";
        } else if (StringUtils.equals("962", departId)) {
            departId = "9621";
            sql = "SELECT * FROM " + NAME + " WHERE ChannelId = ? AND LEFT(DepartId, 4) = ?";
        } else {
            sql = "SELECT * FROM " + NAME + " WHERE ChannelId = ? AND LEFT(DepartId, 3) = ?";
        }

        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(QR_ChannelDepartList.class),
                    new Object[] { channelId, departId }).getSendAddress();
        } catch (Exception e) {
            System.out.println("QR_ChannelDepartList查無資料");
        }
        return null;
    }

    // DepartId
    // 以四碼查詢資料，回傳單筆資料，四碼查「無資料」時，以三碼查詢，回傳單位最小一筆資料
    // 以三碼查詢，判斷有無 departId 相同資料，有：回傳 departId 單筆資料，無：回傳單位最小一筆資料
    public QR_ChannelDepartList queryChannelIdAndDepartId(String channelId, String departId) {
        String sql = "SELECT * FROM " + NAME + " WHERE ChannelId = ? AND LEFT(DepartId, 3) = LEFT(?,3)";
        try {
            List<QR_ChannelDepartList> list =  this.getJdbcTemplate().query(sql, new BeanPropertyRowMapper<>(QR_ChannelDepartList.class),
                new Object[] { channelId, departId });
            QR_ChannelDepartList QRlist = null;
            for(QR_ChannelDepartList QR : list){
                if(StringUtils.equals(QR.getDepartId(),departId)){
                    QRlist = QR;
                }
            }
            if(QRlist != null){
                return QRlist;
            } else {
                return list.get(0);
            }
        } catch (Exception e) {
            System.out.println("QR_ChannelDepartList查無資料");
        }
        return null;
    }

    public QR_ChannelDepartList queryByChannelIdAndDepartId(QR_ChannelDepartList keyItem) {
        String sql = "SELECT * FROM QR_ChannelDepartList WHERE ChannelId = ? AND DepartId LIKE ? ";
        try {
            return this.getJdbcTemplate().queryForObject(sql,
                    new Object[] { keyItem.getChannelId(), keyItem.getDepartId() + "%" },
                    new BeanPropertyRowMapper<>(QR_ChannelDepartList.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }

    public List<QR_ChannelDepartList> findByKB() {
        String sql = "SELECT * FROM QR_ChannelDepartList where enable=1 AND ChannelId = 'KB' order by DepartId ";
        try {
            return this.getJdbcTemplate().query(sql, new BeanPropertyRowMapper<>(QR_ChannelDepartList.class));
        } catch (DataAccessException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
