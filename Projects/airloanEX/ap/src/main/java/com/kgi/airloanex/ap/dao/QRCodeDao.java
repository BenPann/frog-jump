package com.kgi.airloanex.ap.dao;

import static com.kgi.airloanex.common.PlContractConst.UNIQ_TYPE_02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kgi.airloanex.common.dto.db.ChannelData;
import com.kgi.airloanex.common.dto.db.CreditCaseDataExtend;
import com.kgi.eopend3.ap.dao.BaseDao;
import com.kgi.eopend3.common.dto.db.QR_ShortUrlCaseMatch;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class QRCodeDao extends BaseDao {
    public ChannelData getChannelData(String shortUrl) {
        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        try {

            String sql = "SELECT surl.ShortUrl,surl.ChannelId, surl.DepartId,surl.Member,surl.PProductType,surl.PProductId, surl.entry,"
                    + "cl.ChannelType,cl.DomainUrl,cl.MBCFlag,cl.ThemePath,"
                    + "cdl.PromoDepart,cdl.PromoDepart1,cdl.PromoDepart2,cdl.PromoMember,cdl.PromoMember1,cdl.PromoMember2,cdl.TransDepart,cdl.TransMember,"
                    + "pptl.Url " + "from QR_ShortUrl surl "
                    + "left join QR_ChannelList cl on cl.ChannelId = surl.ChannelId and cl.Enable = '1' "
                    + "left join QR_ChannelDepartList cdl on cdl.ChannelId = cl.ChannelId and cdl.DepartId = surl.DepartId and cdl.Enable = '1' "
                    + "inner join QR_PProductTypeList pptl on pptl.PProductType = surl.PProductType and pptl.Enable = '1' "
                    + "where ShortUrl = ?;";
            return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(ChannelData.class),
                    new Object[] { shortUrl });
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
	}
	
	public ChannelData getChannelDataByUniqId(String uniqId) {
		try {
			String sql = "SELECT su2.ShortUrl,su2.ChannelId, su2.DepartId,su2.Member,su2.PProductType,su2.PProductId, su2.entry, "
					+ "cdp.ChannelType,cdp.DomainUrl,cdp.MBCFlag,cdp.ThemePath, "
					+ "cdl.PromoDepart,cdl.PromoDepart1,cdl.PromoDepart2,cdl.PromoMember,cdl.PromoMember1,cdl.PromoMember2,cdl.TransDepart,cdl.TransMember, "
					+ "pptl.Url from QR_ShortUrlCaseMatch  su "
					+ "INNER JOIN QR_ShortUrl su2 on su.ShortUrl = su2.ShortUrl "
					+ "LEFT JOIN QR_ChannelList cdp on cdp.ChannelId = su2.ChannelId "
					+ "LEFT JOIN QR_ChannelDepartList cdl on cdl.ChannelId = su2.ChannelId and cdl.DepartId = su2.DepartId "
					+ "LEFT JOIN QR_PProductTypeList pptl on pptl.PProductType = su2.PProductType "
					+ "where su.UniqId = ?;";
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(ChannelData.class),
                    new Object[] { uniqId });
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
	}
	
    public void updateShortUrlCount(String shortUrl) {
        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        jdbcTemplate.update("update QR_ShortUrl set Count = Count + 1 where ShortUrl = ?;", new Object[] { shortUrl });
    }

    public void createUrlCaseMatch(String shortUrl, String uniqId, String uniqType) {
        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        //先刪除再新增，避免重複INSERT 出現的錯誤。
        jdbcTemplate.update("DELETE FROM QR_ShortUrlCaseMatch WHERE ShortUrl=? AND UniqId=?",new Object[]{shortUrl,uniqId});
        System.out.println( "#### DELETE FROM QR_ShortUrlCaseMatch WHERE ShortUrl=? AND UniqId=? <---(" + shortUrl + "," +  uniqId + ")");

        jdbcTemplate.update("INSERT INTO QR_ShortUrlCaseMatch VALUES(?,?,?)",
                new Object[] { shortUrl, uniqId, uniqType });
        System.out.println( "#### INSERT INTO QR_ShortUrlCaseMatch VALUES(?,?,?) <---(" + shortUrl + "," + uniqId + "," + uniqType + ")");
    }
    
    //
    public List<Map<String,Object>> getChannelList(String[] cangen) {
        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        StringBuilder sb = new StringBuilder();
		 sb.append("select ChannelId ,ChannelName FROM QR_ChannelList where CanGen in (");
		  for (int i = 0; i < cangen.length; i++) {
		      if (i + 1 == cangen.length) {
		          sb.append("?");
		      } else {
		          sb.append("?,");
		      }
		  }
		  sb.append(") order by sort ;");
		  List<String> params = new ArrayList<>(Arrays.asList(cangen));
		  try {
		      return jdbcTemplate.queryForList(sb.toString(), params.toArray());
		  } catch (Exception e) {
//		      e.printStackTrace();
		      return null;
		  }

    }
    
    public List<Map<String,Object>> getChannelPProductTypeMatch() {
        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        String sql = "SELECT a.ChannelId,a.PProductType,l.PProductTypeName FROM QR_ChannelPProductTypeMatch a INNER JOIN QR_PProductTypeList l ON a.PProductType = l.PProductType AND l.Enable = '1' ";
    	return jdbcTemplate.queryForList(sql);
    }
    
    public List<Map<String,Object>> getEmployeeLogic() {
        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        String sql = "select ChannelId ,IdLogic +'-'+IdLength as IdLogic FROM QR_ChannelList;";
    	return  jdbcTemplate.queryForList(sql);
    }
    
    public List<Map<String,Object>> getDepartID(String[] enable) {
        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        
        StringBuilder sb = new StringBuilder();
		 sb.append("SELECT ChannelId,DepartId FROM QR_ChannelDepartList where Enable in (");
		  for (int i = 0; i < enable.length; i++) {
		      if (i + 1 == enable.length) {
		          sb.append("?");
		      } else {
		          sb.append("?,");
		      }
		  }
		  sb.append(");");
		  List<String> params = new ArrayList<>(Arrays.asList(enable));
		  try {
		      return jdbcTemplate.queryForList(sb.toString(), params.toArray());
		  } catch (Exception e) {
//		      e.printStackTrace();
		      return null;
		  }
        
        
        
       // String sql = "SELECT ChannelId,DepartId FROM QR_ChannelDepartList where Enable = ;";
    	//return  jdbcTemplate.queryForList(sql);
    }
    
    public List<Map<String,Object>> getPProductTypeList(String channelId) {
        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        String sql = "select b.PProductType,b.PProductTypeName from QR_ChannelPProductTypeMatch a "
					+ "inner join QR_PProductTypeList b on a.PProductType = b.PProductType where a.ChannelId = ? and b.Enable = '1' order by b.sort;";
			return  jdbcTemplate.queryForList(sql,
	                new Object[] { channelId });
    }
    
    public List<Map<String,Object>> getPProductList(String channelId) {
        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        try {
			String sql = "select distinct a.PProductType, c.PProductId, c.PProductName,c.Sort from QR_ChannelPProductTypeMatch a "
					+ "inner join QR_PProductTypeProductMatch b on a.PProductType = b.PProductType " 
					+  "inner join QR_PProductList c on b.PProductId = c.PProductId and ChannelId = ? and c.Enable = '1' order by c.Sort ";
			return  jdbcTemplate.queryForList(sql,
	                new Object[] { channelId });
		} catch (DataAccessException e) {
			return null;
		}
    }
    
    public List<Map<String,Object>> getPProductListCount(String channelId) {
        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        try {
			String sql = "select a.PProductType,COUNT(a.PProductType) PProductTypeCount from QR_ChannelPProductTypeMatch a "
					+ "inner join QR_PProductTypeProductMatch b on a.PProductType = b.PProductType " 
					+  "inner join QR_PProductList c on b.PProductId = c.PProductId and ChannelId = ? and c.Enable = '1' group by a.PProductType ";
			return  jdbcTemplate.queryForList(sql,
	                new Object[] { channelId });
		} catch (DataAccessException e) {
			return null;
		}
    }

	public boolean isShortURL(String shortUrl) {
        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        boolean check = false;
        String sql = "select COUNT(*) as count FROM QR_ShortUrl "
        		+ " where ShortUrl=?";
        int checkNunber = 0;
        checkNunber = jdbcTemplate.queryForObject
        		(sql, new Object[] {shortUrl}, Integer.class);
        if (checkNunber != 0) check = true;
		return check;
	}

	public boolean isOver3Time(String part, String publicName) {
		
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		boolean isOver3Time = false;
		String sql = "select COUNT(*) as count FROM QR_ShortUrl "
				+ " where UpdateTime BETWEEN DATEADD(day,DATEDIFF(day,0,GETDATE()),0) and GETDATE()"
				+ " and DepartId=? and Member =?";
        int checkNunber = 0;
        checkNunber = jdbcTemplate.queryForObject
        		(sql, new Object[] {part , publicName}, Integer.class);
        if (checkNunber >= 3) isOver3Time = true;
		return isOver3Time;

	}

	public List<Map<String, Object>> getPProductTypeProductMatch() {
		
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		String sql = " select PProductType,STUFF((SELECT '-'+b.PProductId"
				+ " FROM QR_PProductTypeProductMatch b where b.PProductType  = a.PProductType FOR XML PATH(''))"
				+ " ,1,1,'') as PProductId"
				+ " FROM QR_PProductTypeProductMatch a group by PProductType";
		
		return  jdbcTemplate.queryForList(sql);

	}

	public boolean isProducdId(String st) {
		
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        boolean isProducdId = false;
        String sql = "select COUNT(*) as count FROM QR_PProductList "
        		+ " where PProductId=? and Enable='1'";
        int checkNunber = 0;
        checkNunber = jdbcTemplate.queryForObject
        		(sql, new Object[] {st}, Integer.class);
        if (checkNunber != 0) isProducdId = true;
        return isProducdId;
	}
	
	
	public boolean isTransDepart(String transDepart) {
        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        boolean check = false;
        String sql = " select count(*) as count FROM QR_ChannelDepartList a "
        		   + " where DepartId=? and a.Enable='1' ";
        int checkNunber = 0;
        checkNunber = jdbcTemplate.queryForObject
        		(sql, new Object[] {transDepart}, Integer.class);
        if (checkNunber != 0) check = true;
		return check;
	}
	
	public Map<String, Object> getTransMemberLogic(String transDepart) {
		
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		String sql = " select b.IdLogic ,b.IdLength FROM QR_ChannelDepartList a "
					+" join QR_ChannelList b on b.ChannelId=a.ChannelId"
					+" where a.DepartId=? and a.Enable='1' ";

        Map<String, Object> getTransMemberLogic ;
        getTransMemberLogic = jdbcTemplate.queryForMap
        		(sql, new Object[] {transDepart});

//        System.out.println("getTransMemberLogic:"+getTransMemberLogic.toString());
		return getTransMemberLogic;
	}

	public String setTransDepart(String transDepart, String transMember, String promoDepart,
			String promoMember, String caseNo) {

        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		String sql = "INSERT INTO QR_PromoCase (UniqId, UniqType, TransDepart, TransMember, PromoDepart,"
			      + " PromoMember, PromoDepart1, PromoMember1, PromoDepart2, PromoMember2) "
				   + "VALUES(?,?,?,?,?,?,?,?,?,?)";
		String result = "false";
		//先刪除 在寫入
		jdbcTemplate.update("DELETE FROM QR_PromoCase WHERE UniqId=?",new Object[]{caseNo});
		int i = jdbcTemplate.update(sql, new Object[] { caseNo, "02", transDepart, transMember,
				promoDepart, promoMember, "", "", "", "" });
		if(i > 0) result = "true";
		return result;
	}
	
	public ChannelData getPromote(String UniqId){
		
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		// ChannelData cd = null;
        String sql = " select c.ChannelName, d.TransDepart,d.TransMember, d.PromoDepart,"
        		   + " d.PromoMember, d.PromoDepart1, d.PromoMember1, d.PromoDepart2, d.PromoMember2" 
				   + " from QR_ShortUrlCaseMatch a "
				   + " left join QR_ShortUrl b on b.ShortUrl = a.ShortUrl"
				   + " left join QR_ChannelList c on c.ChannelId = b.ChannelId"
				   + " left join QR_PromoCase d on d.UniqId = a.UniqId"
				   + " where a.UniqId = ? ";
        try{
        	return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(ChannelData.class),
                    new Object[]{UniqId});
        }catch (EmptyResultDataAccessException e) {
            return null;
        }
	}

	public ChannelData getPromoCase(String uniqId) {
		
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		String sql = " SELECT * FROM QR_PromoCase WHERE UniqId = ? ";
		try {
			return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(ChannelData.class),
					new Object[] { uniqId });
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	public Map<String, Object> getPromoteForLoan(String caseNo) {

		
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		String sql = " select TransDepart, TransMember, PromoDepart, PromoMember "
				+ " from dbo.QR_PromoCase where UniqId=? ";
		List<Map<String, Object>> certs = jdbcTemplate.queryForList(sql, new Object[] { caseNo });

		Map<String, Object> PromoteForLoan = new HashMap<>();
		PromoteForLoan.put("TransDepart", "");
		PromoteForLoan.put("TransMember", "");
		PromoteForLoan.put("PromoDepart", "");
		PromoteForLoan.put("PromoMember", "");

		if (certs.size() == 0) {
			return PromoteForLoan;
		} else {
			return certs.get(0);
		}

	}
	
	
	
	// test only 
    public CreditCaseDataExtend getCreditDataById(String UniqId) {
        
        JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
        String sql = "SELECT * FROM CreditCaseData WHERE UniqId = ?;";
        try{
        	return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(CreditCaseDataExtend.class),
                    new Object[]{UniqId});
        }catch (EmptyResultDataAccessException e) {
            return null;
        }
    }


	public String getPProductTypeMatchUrl(String productType) {
		
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		String sql = "SELECT Url FROM QR_PProductTypeList WHERE PProductType = ?;";
		try{
			return jdbcTemplate.queryForList(sql,new Object[]{productType}).get(0).get("Url").toString();
		}catch(Exception e){
			return null;
		}
	}
	
	public String getDomainUrl(String shortUrl) {
		
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		String sql = "SELECT DomainUrl from QR_ShortUrl su inner join QR_ChannelList cl on su.ChannelId = cl.ChannelId where su.ShortUrl = ? ;";
		try{
			return jdbcTemplate.queryForList(sql,new Object[]{shortUrl}).get(0).get("DomainUrl").toString();
		}catch(Exception e){
			return null;
		}
	}

	public int saveChannel(String uniqId, ChannelData cd) {
		int result = 0;
		
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		String sql = " INSERT INTO QR_PromoCase( UniqId, UniqType, TransDepart, TransMember, PromoDepart, PromoMember, "
			       + " PromoDepart1, PromoMember1, PromoDepart2, PromoMember2) "
				   + " VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
		try {
			//先刪除 在寫入
			// 非商銀的話TransDepart 要寫入什麼?
			jdbcTemplate.update("DELETE FROM QR_PromoCase WHERE UniqId=? AND 1=1",new Object[]{uniqId});
			result = jdbcTemplate.update(sql,
			        new Object[]{ 
			        		uniqId, UNIQ_TYPE_02, cd.getTransDepart(), cd.getTransMember(), 
			        		cd.getPromoDepart(), cd.getPromoMember(),  cd.getPromoDepart1(), cd.getPromoMember1(),
			        		cd.getPromoDepart2(), cd.getPromoMember2()
							});
			return result;
		} catch (DataAccessException e) {
//			e.printStackTrace();
			return result;
		}
	}

	public Map<String, Object> getChannelDepartList(String transDepart) {
		
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		String sql = " select TransDepart,TransMember,PromoDepart,PromoMember from QR_ChannelDepartList where DepartId = ? ";
		List<Map<String, Object>> certs  = jdbcTemplate.queryForList(sql,  new Object[]{transDepart});
		
		if( certs.size() > 0) {
			return certs.get(0);
		} 
		
		Map<String, Object> noFind = new HashMap<>();
		noFind.put("TransDepart", "");
		noFind.put("TransMember", "");
		noFind.put("PromoDepart", "");
		noFind.put("PromoMember", "");
		return noFind;
		
	}

	    
    /**
     *	查詢是否有重複
     * @param uniqId
     * @return
     */
    public List<QR_ShortUrlCaseMatch> queryQR_ShortUrlCaseMatchByUniqId(String uniqId) {
    	try {
    		String sql="SELECT * FROM QR_ShortUrlCaseMatch WHERE UniqId =? ";
    		List<String> params = new ArrayList<>();
            params.add(uniqId);
            return this.getJdbcTemplate().query(sql, params.toArray(),new BeanPropertyRowMapper<>(QR_ShortUrlCaseMatch.class));
    	} catch (DataAccessException ex) {
            System.out.println("queryQR_ShortUrlCaseMatchByUniqId 無資料");
            return null;
        }
    }
}
