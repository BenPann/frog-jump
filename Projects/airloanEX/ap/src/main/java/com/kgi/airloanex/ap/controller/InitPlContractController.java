package com.kgi.airloanex.ap.controller;

import static com.kgi.airloanex.common.PlContractConst.LOAN0000000000000;

import javax.servlet.http.HttpServletRequest;

import com.kgi.airloanex.ap.service.*;
import com.kgi.eopend3.ap.controller.base.BaseController;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.dto.KGIHeader;
import com.kgi.eopend3.common.dto.WebResult;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * /stp
 * <h3>立約流程</h3>
 * <ol>
 * <li>/initPlContract 立約登入</li>
 * <li>/prepareDgtConst 預載方案別選項</li>
 * <li>/chosenDgtConst 選擇方案別選項</li>
 * <li>/agreeDownloadPdf</li>
 * <li>/agreePlContract</li>
 * <li>/infoPlContract EDDA驗身</li>
 * <li>/getAutInfo 共銷</li>
 * <li>/updateDgtConst 立約完成</li>
 * </ol>
 *
 * <h3>申請流程</h3>
 * <ol>
 * <li>/applyStart 申請登入</li>
 * <li>/applyInfo 更新申請的表單內容</li>
 * <li>/applyVerify 即OTP 1</li>
 * <li>/applyPreAudit 取得預核名單資料</li>
 * <li>/applyLoan 開始填寫申請書</li>
 * <li>/applyPerson 個人資料</li>
 * <li>/applyWork 工作資料</li>
 * <li>/applyOther 其他資料</li>
 * <li>/applyPrepareConfirm 預載申請貸款確認頁資料</li>
 * <li>/applyConfirm 送出申請貸款確認頁資料</li>
 * <li>/applyConfirmToOtp2 確認頁完成直接OTP2 不驗 pcode2566</li>
 * <li>/applyIdentified PCODE2566及OTP驗身完成，產製PDF</li>
 * <li>/mustUploadIdcard 是否需要進入身份證上傳頁</li>
 * <li>/mustUploadFinpf 是否需要進入財力證明上傳頁</li>
 * <li>/applyVerify2 即OTP2</li>
 * <li>/applyUploadIdcard 確認上傳身份證正反面</li>
 * <li>/applyUploadFinpf 確認上傳財力證明</li>
 * <li>/applyRelationDegree 確認上傳同一關係人表</li>
 * <li>/applyStepok 確認完成申請</li>
 * <li>/applyNewCreditcard 申請信用卡</li>
 * <li>/applyQryQa 額度體驗題目</li>
 * <li>/applySendAws 額度體驗評分</li>
 * <li>/applyPLCUs 專人聯絡</li>
 * <li>/applyPaper 紙本申請</li>
 * <li>/applyLaterAddon 稍後補件上傳</li>
 * </ol>
 * 
 * <p>
 * 預覽契約內容(preview Html) 
 * 產製 pdf xml image 上傳至 ftp 的部份見 ContractController
 * </p>
 * 
 * <p>
 * OTP(立約及申請共用)、PCODE2566(立約)、EDDA(立約)驗身見 ValidationCtrl
 * </p>
 * 
 * <p>
 * PCODE2566(申請)驗身見 BankController
 * </p>
 * 
 * <p>
 * 上傳身份證正反面及財力證明見 UserPhotoController
 * </p>
 * 
 * @see com.kgi.airloanex.ap.controller.ContractController
 * @see com.kgi.airloanex.ap.controller.ValidationCtrl
 * @see com.kgi.airloanex.ap.controller.BankController
 * @see com.kgi.airloanex.ap.controller.UserPhotoController
 */
@RestController
@RequestMapping("/stp")
public class InitPlContractController extends BaseController {

    private static String context = "InitPlContractController";

    @Autowired
    private InitPlContractService initPlContractService;

    @Autowired
    private LogonService logonService;

    @Autowired
    private ApplyService applyService;

    @Autowired
    private AutInfoService autInfoService;

    @Autowired
    private SrnoService snSer;

    // ----------------------------------------------------------------------------------------------
    // // 登入
    @PostMapping("/logon")
    public String logon(@RequestBody String reqBody) {
        logger.info("logon 前台輸入的資料" + reqBody);
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return logonService.logon(reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }
    // ----------------------------------------------------------------------------------------------
    // 立約流程

    @PostMapping("/initPlContract")
    public String initPlContract(@RequestBody String reqBody) {
        logger.info("5前台輸入的資料" + reqBody);
        // accountInfoService.getEDDA0000100MDetail(null, null, null);
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return initPlContractService.initPlContract(reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    // 20201120 如果只有PL及有代償 status=0 否則status=99
    @PostMapping("/initPlPayBank")
    public String initPlPayBank(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("### 判斷力約案件及有無代償" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        String idno = header.getIdno();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return initPlContractService.initPlPayBank(uniqId, idno);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/prepareDgtConst")
    public String prepareDgtConst(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("6前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        String idno = header.getIdno();
        logger.info("InitPlContractController,prepareDgtConst,150 uniqId = " + uniqId );
        logger.info("InitPlContractController,prepareDgtConst,151 idno = " + idno );
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return initPlContractService.getDgtConst(uniqId, idno, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/chosenDgtConst")
    public String chosenDgtConst(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("7前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        String idno = header.getIdno();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return initPlContractService.chosenDgtConst(uniqId, idno, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/agreeDownloadPdf")
    public String agreeGetPdf(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("選擇產品代碼 : " + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return initPlContractService.agreeDownloadPdf(uniqId, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/agreePlContract")
    public String agreePlContract(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("8前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return initPlContractService.agreePlContract(uniqId, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    /**
     * 20201119 Ben提出新增邏輯 目前只有PL需要做此判斷
     * a. 如果有代償，且撥款日已過，則請新增提示(代償立約的指定撥款日已逾期，請重新立約)，並跳回登入
     * b. 如果無代償，pay_date如果為空，就預設今天
     * c. 如果無代償，pay_date有值小於今日，則改為今日，反之則以pay_date為主         
     */
    @PostMapping("/judgeExceedPayDate")
    public String judgeExceedPayDate(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("判斷撥繳款邏輯" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        String idno = header.getIdno();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return initPlContractService.judgeExceedPayDate(uniqId, idno);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/judgeExceedPayDateByExpBank")
    public String judgeExceedPayDateByExpBank(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("判斷撥繳款邏輯" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return initPlContractService.judgeExceedPayDateByExpBank(uniqId, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/infoPlContract")
    public String infoPlContract(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("繳款日期" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return initPlContractService.infoPlContract(uniqId, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/getAutInfo") // 取得該客戶是否勾選過條款
    public String updateAutInfo(HttpServletRequest request, @RequestBody String reqBody) {
        try {
            KGIHeader header = this.getHeader(request);
            String idno = header.getIdno();
            String uniqId = header.getUniqId();
            // String ip = header.getIpAddress();
            return autInfoService.getAutInfo(reqBody, idno, uniqId);
        } catch (ErrorResultException e) {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping(value = "/updateDgtConst")
    public String updateDgtConst(HttpServletRequest request, @RequestBody String reqBody) throws Exception {
        logger.info("選擇產品代碼 : " + reqBody);
        try {
            KGIHeader header = this.getHeader(request);
            String uniqId = header.getUniqId();
            if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
                return initPlContractService.updateDgtConst(uniqId, reqBody);
            }

        } catch (ErrorResultException e) {
            return WebResult.GetFailResult();
        }
        return null;
    }
    // ----------------------------------------------------------------------------------------------
    // 申請流程

    @PostMapping("/applyInfo")
    public String applyInfo(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("3前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        System.out.println("InitPlContractController,applyInfo,273 = "+header);
        System.out.println("InitPlContractController,applyInfo,274 = "+uniqId);
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyInfo(uniqId, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/applyStart")
    public String applyStart(@RequestBody String reqBody) {
        logger.info("1前台輸入的資料" + reqBody);
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyStart(reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/applyVerify")
    public String applyVerify(HttpServletRequest request, @RequestBody String reqBody) throws Exception {
        logger.info("2前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyVerify(uniqId);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("applyPreAudit")
    public String applyPreAudit(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("取得預核名單資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyPreAudit(uniqId);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/applyLoan")
    public String applyLoan(HttpServletRequest request, @RequestBody String reqBody) throws Exception {
        logger.info("前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyLoan(uniqId);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping(value = "/applyPrepareConfirm")
    public String applyPrepareConfirm(HttpServletRequest request, @RequestBody String reqBody) throws Exception {
        logger.info("前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyPrepareConfirm(uniqId);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/applyConfirm")
    public String applyConfirm(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("4前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        String idno = header.getIdno();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyConfirm(uniqId, idno, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    /**
     * 確認頁完成直接OTP2 不驗 pcode2566
     */
    @PostMapping("/applyConfirmToOtp2")
    public String applyConfirmToOtp2(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        String idno = header.getIdno();
        String ipAddress = header.getIpAddress();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyConfirmToOtp2(uniqId, idno, ipAddress);
        } else {
            return WebResult.GetFailResult();
        }
    }

    /**
     * 已完成貸款申請書填寫含驗身，即產製貸款申請書 HTML PDF，待上傳身份證正反面及財力證明
     */
    @PostMapping("/applyIdentified")
    public String applyIdentified(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        String idno = header.getIdno();
        String ipAddress = header.getIpAddress();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyIdentified(uniqId, idno, ipAddress);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/mustUploadIdcard")
    public String mustUploadIdcard(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.mustUploadIdcard(uniqId);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/mustUploadFinpf")
    public String mustUploadFinpf(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.mustUploadFinpf(uniqId);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/applyUploadIdcard")
    public String applyUploadIdcard(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        String idno = header.getIdno();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyUploadIdcard(uniqId, idno, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/applyUploadFinpf")
    public String applyUploadFinpf(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyUploadFinpf(uniqId);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("applyRelationDegree")
    public String applyRelationDegree(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("6前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyRelationDegree(uniqId, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("applyStepok")
    public String applyStepok(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyStepok(uniqId);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("applyNewCreditcard")
    public String applyNewCreditcard(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("7前台輸入的資料" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        String ipAddress = header.getIpAddress();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyNewCreditcard(uniqId, ipAddress, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    // @PostMapping("applyCreateNewCase")
    // public String applyAddLoanCase(HttpServletRequest request, @RequestBody String reqBody) {
    //     logger.info("前台輸入的資料" + reqBody);
    //     KGIHeader header = this.getHeader(request);
    //     String uniqId = header.getUniqId();
    //     String idno = header.getIdno();
    //     String ipAddress = header.getIpAddress();
    //     if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
    //         return applyService.applyCreateNewCase(uniqId);
    //     } else {
    //         return WebResult.GetFailResult();
    //     }
    // }
    // ----------------------------------------------------------------------------------------------

    @PostMapping("/applyQryQa")
    public String applyQryQa(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("額度體驗題目" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        String ipAddress = header.getIpAddress();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.getQryQa(uniqId, ipAddress);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/applyGetTestRplScore")
    public String applySendAwsRPL(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("RPL 額度體驗評分" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        String ipAddress = header.getIpAddress();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyGetTestRplScore(uniqId, ipAddress, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/applySendAws")
    public String applySendAws(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("PL 額度體驗評分" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        String ipAddress = header.getIpAddress();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.getSendAsw(uniqId, ipAddress, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/applyPLCUs")
    public String applyPLCUs(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("專人聯絡" + reqBody);
        
        String uniqId = snSer.getCTACId();
        
        KGIHeader header = null;
        try {
            header = this.getHeader(request);
            uniqId = header.getUniqId();
        } catch(Exception e) {
            logger.error("Fail to find KgiHeader(not login).");
        }
        
        // String ipAddress = header.getIpAddress();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.getPLCUs(uniqId, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/applyPaper")
    public String applyPaper(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("紙本申請" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        // String ipAddress = header.getIpAddress();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyPaper(uniqId);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/applyLaterAddon")
    public String applyLaterAddon(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("稍後補件上傳" + reqBody);
        KGIHeader header = this.getHeader(request);
        String uniqId = header.getUniqId();
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyLaterAddon(uniqId, reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/applyCalResult")
    public String applyCalResult(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("額度利率體驗傳送方案" + reqBody);
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return applyService.applyCalResult(reqBody);
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/applyGetCSMemoData")
    public String applyGetCSMemoData(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("取得跨售資料" + reqBody);
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            String result = applyService.applyGetCSMemoData(reqBody);
            return result;
        } else {
            return WebResult.GetFailResult();
        }
    }

    @PostMapping("/applyGetThemePath")
    public String applyGetThemePath(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("取得申辦css主題檔" + reqBody);
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            String result = applyService.applyGetThemePath(reqBody);
            return result;
        } else {
            return WebResult.GetFailResult();
        }
    }
}
