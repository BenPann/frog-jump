package com.kgi.airloanex.ap.service;

import com.ibm.tw.commons.net.mq.MQReceiver;
import com.ibm.tw.commons.net.mq.MQSender;
import com.ibm.tw.commons.util.StringUtils;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.ContractApiLogDao;
import com.kgi.eopend3.ap.service.ESBService;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountInfoService {

    @Autowired
    private AirloanEXConfig globalConfig;

    @Autowired
    private ContractApiLogDao contractApiLogDao;
    
    @Autowired
	private ESBService esbService;

	@Autowired
	private SrnoService srnoService;
	    
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    // private List<EDDA0000100MDetail> getEDDA0000100MDetail(String idno, String bankId, String account) {
	public String getEDDA0000100MDetail(String uniqId, String idno, String eddaBankId, String eddaAccountNo) {
    	String startTime = "", endTime = "" ;
		String req = "", res = "";
		String result = "0"; 
		try {
			req = xmlEDDA0000100M(idno, eddaBankId, eddaAccountNo);
			MQSender sender = esbService.getALNSender();
			MQReceiver receiver = esbService.getALNReceiver();
			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			logger.info("StartTime --->" + startTime);
			res = esbService.sendMessageQueue(req, sender, receiver);
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			logger.info("EndTime --->" + endTime);
			// EDDA0000100M edda0000100M = new EDDA0000100M() ;
			// return edda0000100M.parseXml(res) ;
            logger.info(res);
            if(res.contains("<StatusCode>0<")) {
				result = "1";
			}
			
			return result;
		} catch (Exception e) {
			logger.error("未知錯誤", e);

			res = e.getMessage() ;

			return result;
		} finally {
			contractApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, "EDDA0000100M", req, res, startTime, endTime);
		}
    }

    private String xmlEDDA0000100M(String idno, String eddaBankId, String eddaAccountNo) {
	    if(eddaAccountNo.length() < 14) {
			eddaAccountNo = StringUtils.leftPad(eddaAccountNo.toString(), 14, "0");
		}
		logger.info("eddaAccountNo[ " + eddaAccountNo + " ]");

		String clientID = globalConfig.ALN_ClientId();
		String clientPAZZD = globalConfig.ALN_ClientPAZZD();
		String timeNow = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		clientPAZZD = DigestUtils.md5Hex(clientPAZZD + timeNow);
		StringBuffer sb = new StringBuffer("");

		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:esb=\"http://www.cosmos.com.tw/esb\" xmlns:cics=\"http://www.cosmos.com.tw/cics\" xmlns:bill=\"http://www.cosmos.com.tw/bill\" xmlns:nefx=\"http://www.cosmos.com.tw/nefx\" xmlns:cactx=\"http://www.cosmos.com.tw/cactx\" xmlns:bns=\"http://www.cosmos.com.tw/bns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("  <SOAP-ENV:Header />");
		sb.append("  <SOAP-ENV:Body>");
		sb.append("    <esb:MsgRq>");
		sb.append("      <Header>");
		sb.append("        <ClientId>").append(clientID).append("</ClientId>"); // ALNWEB
		sb.append("        <ClientPwd>").append(clientPAZZD).append("</ClientPwd>");
		sb.append("        <ClientDt>").append(timeNow).append("</ClientDt>");
		sb.append("        <TxnId>EDDA0000100M</TxnId>");
		sb.append("        <UUID>").append(clientID + DateUtil.GetDateFormatString("yyyyMMddHHmmssSSS")).append("</UUID>");
		sb.append("      </Header>");
        sb.append("      <SvcRq xsi:type=\"bns:EDDA0000100MSvcRqType\">");
        sb.append("        <DateTime>").append(DateUtil.GetDateFormatString("yyyyMMddHHmmss")).append("</DateTime>");
        sb.append("        <Stan>").append(srnoService.getEddaSrno()).append("</Stan>");   // EDDA交易序號必須為唯一值
        sb.append("        <InBankId>").append("8090000").append("</InBankId>");           // 凱基銀行代號 '8090000'
        sb.append("        <OutBankId>").append(eddaBankId + "0000").append("</OutBankId>");// 提回行金融機構代號 "8120000"
        sb.append("        <SenderIdNo>").append("86517321").append("</SenderIdNo>");      // 86517321
        sb.append("        <TxId>").append("824").append("</TxId>");                       // 票交所交易代號三碼
        sb.append("        <UserNo>").append(idno).append("</UserNo>");                    // 用戶號碼
        sb.append("        <UserId>").append(idno).append("</UserId>");                    // 用戶身分證/統編 個人戶英文字母請大寫
        sb.append("        <OutIdNo>").append(idno).append("</OutIdNo>");                  // 委託授權扣款存戶之營利事業統一編號或身分證字號
        sb.append("        <OutAcctNo>").append(eddaAccountNo).append("</OutAcctNo>");        // 委託授權扣款存戶於扣款行帳號，位數不足十四位數時，右靠左補零。
        sb.append("        <TxType>").append("A").append("</TxType>");                     // A:發動行新增授權扣款 M:發動行新增或異動扣款限額 D:發動行取消授權扣款 I:驗證服務
        sb.append("        <Currency>").append("TWD").append("</Currency>");               // TWD
        sb.append("        <LimitAmt>").append("00000000").append("</LimitAmt>");          // 00000000
        sb.append("        <Action>").append("I").append("</Action>");                     // I
        // sb.append("        <CustID>").append().append("</CustID>"); // 
        // sb.append("        <BankNo>").append().append("</BankNo>"); // 
        // sb.append("        <AcctNo>").append().append("</AcctNo>"); // 
        // sb.append("        <OwnershipFlag>").append().append("</OwnershipFlag>"); // 
		sb.append("      </SvcRq>");
		sb.append("    </esb:MsgRq>");
		sb.append("  </SOAP-ENV:Body>");
		sb.append("</SOAP-ENV:Envelope>");

		// logger.info(sb.toString());
		
		return sb.toString();
	}
	@SuppressWarnings("unused")
	private String xmlEDDA0000100M_sample(String idno, String eddaBankId, String eddaAccountNo) {
	    if(eddaAccountNo.length() < 14) {
			eddaAccountNo = StringUtils.leftPad(eddaAccountNo.toString(), 14, "0");
		}
		logger.info("eddaAccountNo[ " + eddaAccountNo + " ]");

		String clientID = globalConfig.ALN_ClientId();
		String clientPAZZD = globalConfig.ALN_ClientPAZZD();
		String timeNow = DateUtil.GetDateFormatString("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		clientPAZZD = DigestUtils.md5Hex(clientPAZZD + timeNow);
		StringBuffer sb = new StringBuffer("");

		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:esb=\"http://www.cosmos.com.tw/esb\" xmlns:cics=\"http://www.cosmos.com.tw/cics\" xmlns:bill=\"http://www.cosmos.com.tw/bill\" xmlns:nefx=\"http://www.cosmos.com.tw/nefx\" xmlns:cactx=\"http://www.cosmos.com.tw/cactx\" xmlns:bns=\"http://www.cosmos.com.tw/bns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("  <SOAP-ENV:Header />");
		sb.append("  <SOAP-ENV:Body>");
		sb.append("    <esb:MsgRq>");
		sb.append("      <Header>");
		sb.append("        <ClientId>").append(clientID).append("</ClientId>"); // ALNWEB
		sb.append("        <ClientPwd>").append(clientPAZZD).append("</ClientPwd>");
		sb.append("        <ClientDt>").append(timeNow).append("</ClientDt>");
		sb.append("        <TxnId>EDDA0000100M</TxnId>");
		sb.append("        <UUID>").append(clientID + DateUtil.GetDateFormatString("yyyyMMddHHmmssSSS")).append("</UUID>");
		sb.append("      </Header>");
        sb.append("      <SvcRq xsi:type=\"bns:EDDA0000100MSvcRqType\">");
        sb.append("        <DateTime>").append(DateUtil.GetDateFormatString("yyyyMMddHHmmss")).append("</DateTime>");
        sb.append("        <Stan>").append(srnoService.getEddaSrno()).append("</Stan>");   // EDDA交易序號必須為唯一值
        sb.append("        <InBankId>").append("8090000").append("</InBankId>");           // 凱基銀行代號 '8090000'
        sb.append("        <OutBankId>").append("8120000").append("</OutBankId>");// 提回行金融機構代號 "8120000"
        sb.append("        <SenderIdNo>").append("86517321").append("</SenderIdNo>");      // 86517321
        sb.append("        <TxId>").append("824").append("</TxId>");                       // 票交所交易代號三碼
        sb.append("        <UserNo>").append("Y163524550").append("</UserNo>");                    // 用戶號碼
        sb.append("        <UserId>").append("Y163524550").append("</UserId>");                    // 用戶身分證/統編 個人戶英文字母請大寫
        sb.append("        <OutIdNo>").append("Y163524550").append("</OutIdNo>");                  // 委託授權扣款存戶之營利事業統一編號或身分證字號
        sb.append("        <OutAcctNo>").append("20010100503470").append("</OutAcctNo>");        // 委託授權扣款存戶於扣款行帳號，位數不足十四位數時，右靠左補零。
        sb.append("        <TxType>").append("A").append("</TxType>");                     // A:發動行新增授權扣款 M:發動行新增或異動扣款限額 D:發動行取消授權扣款 I:驗證服務
        sb.append("        <Currency>").append("TWD").append("</Currency>");               // TWD
        sb.append("        <LimitAmt>").append("00000000").append("</LimitAmt>");          // 00000000
        sb.append("        <Action>").append("I").append("</Action>");                     // I
        // sb.append("        <CustID>").append().append("</CustID>"); // 
        // sb.append("        <BankNo>").append().append("</BankNo>"); // 
        // sb.append("        <AcctNo>").append().append("</AcctNo>"); // 
        // sb.append("        <OwnershipFlag>").append().append("</OwnershipFlag>"); // 
		sb.append("      </SvcRq>");
		sb.append("    </esb:MsgRq>");
		sb.append("  </SOAP-ENV:Body>");
		sb.append("</SOAP-ENV:Envelope>");

		logger.info(sb.toString());
		
		return sb.toString();
	}
}