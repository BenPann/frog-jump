package com.kgi.airloanex.ap.controller;

import javax.servlet.http.HttpServletRequest;

import com.kgi.airloanex.ap.service.ChatBotPageTeTriggerService;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/chatbot")
public class ChatbotController {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ChatBotPageTeTriggerService chatBotPageTeTriggerService;

    /**
     * We send JSON and We accept plain text as response.
     * INPUT: json
     * OUTPUT: string
     */
    @PostMapping("/trigger")
	public String trigger(HttpServletRequest request, @RequestBody String reqBody) {
        // logger.info("前台輸入的資料" + reqBody);
        if(ESAPI.validator().isValidInput("ChatbotController", reqBody, "SafeJson", Integer.MAX_VALUE, false)){
            String str = chatBotPageTeTriggerService.chatbotUrl(reqBody);
            return str;
		} else {
		 	return "";
		}
	}
}
