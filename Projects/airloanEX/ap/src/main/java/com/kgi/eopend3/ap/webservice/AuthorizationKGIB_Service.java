/**
 * AuthorizationKGIB_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.kgi.eopend3.ap.webservice;

public interface AuthorizationKGIB_Service extends javax.xml.rpc.Service {
    public java.lang.String getDominoAddress();

    public com.kgi.eopend3.ap.webservice.AuthorizationKGIB_PortType getDomino() throws javax.xml.rpc.ServiceException;

    public com.kgi.eopend3.ap.webservice.AuthorizationKGIB_PortType getDomino(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
