package com.kgi.airloanex.ap.controller;

import javax.servlet.http.HttpServletRequest;

import com.kgi.airloanex.ap.service.SmsService;
import com.kgi.eopend3.ap.controller.base.BaseController;
import com.kgi.eopend3.common.dto.KGIHeader;
import com.kgi.eopend3.common.dto.WebResult;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sms")
public class SmsController extends BaseController {

    private static String context = "SmsController";

    @Autowired
    private SmsService smsService;
    
    @PostMapping("/sessiontimeout")
    public String sessiontimeout(HttpServletRequest request, @RequestBody String reqBody) {
        logger.info("操作逾時 前台輸入的資料" + reqBody);
        try {
            KGIHeader header = this.getHeader(request);
            String uniqId = header.getUniqId();
            if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
                return smsService.sessiontimeout(uniqId);
            } else {
                throw new Exception();
            }
        } catch(Exception e) {
            return WebResult.GetFailResult();
        }
    }
}
