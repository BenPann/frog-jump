package com.kgi.airloanex.ap.pdf;

import static com.kgi.airloanex.common.PlContractConst.DOCUMENT_TYPE_CONTRACT_ACH_PDF;
import static com.kgi.airloanex.common.PlContractConst.KB_9743_CHANNEL_ID;
import static com.kgi.airloanex.common.PlContractConst.UNIQ_TYPE_03;
import static com.kgi.airloanex.common.PlContractConst.isPL;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.CaseDocumentDao;
import com.kgi.airloanex.ap.dao.ContractDocDao;
import com.kgi.airloanex.ap.dao.ContractMainActInfoDao;
import com.kgi.airloanex.ap.dao.ContractMainDao;
import com.kgi.airloanex.ap.dao.QR_ChannelDepartListDao;
import com.kgi.airloanex.common.domain.ContractAchPDFDomain;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDto;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoConstData;
import com.kgi.airloanex.common.dto.db.CaseDocument;
import com.kgi.airloanex.common.dto.db.ContractMain;
import com.kgi.airloanex.common.dto.db.ContractMainActInfo;
import com.kgi.airloanex.common.util.ExportUtil;
import com.kgi.eopend3.ap.config.GlobalConfig;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ContractAchPDF extends BasePDFDocument {

    private String _uniqId;
	private String templateVersion = "";
	private String sendAddress = "";

	@Autowired
	private GlobalConfig global;

    @Autowired
	private AirloanEXConfig globalConfig;

    @Autowired
	private ContractMainDao contDao;

	@Autowired
	private ContractMainActInfoDao contractMainActInfoDao;

    @Autowired
	private ContractDocDao contractDao;

    @Autowired
	private PageFooter footer;

    @Autowired
	private CaseDocumentDao caseDocumentDao;

    @Autowired
	private QR_ChannelDepartListDao qr_ChannelDepartListDao;

    private static final Logger logger = LogManager.getLogger(ContractPDF.class.getName());

    /** 主檔 - 案件契約資料表(ContractMain) */
	private ContractMain contractMain;
	/** 立約帳戶驗身資料表(ContractMainActInfo) */
	private ContractMainActInfo contractMainActInfo;
    
    public ContractAchPDF(ApplicationContext appContext) {
		super(appContext);
	}

	/** 設定初始化參數
	 * @param params 第一個參數 UniqId 第二個參數 是否要使用公司章
	 */
	public void init(String... params) throws IOException {
		this._uniqId = params[0];
		// 是否加密
		this.setUseEncrepyt(true);
		// this.setEncryptPassword(idno);
		// 是否使用浮水印
		this.setUseWaterMark(true);
		this.setWaterMarkText(globalConfig.IMG_WATER_FONT());
		this.setWaterMarkFont(globalConfig.PDF_FONT_WATER_TTF());
		// 是否要PDF轉成圖片
		this.setPdfToImage(false);
		// 是否存DB
		this.setUseSaveToDB(true);
		// 是否發信
		this.setUseSendEMail(false);
	}

    @Override
	public void beforePdfProcess() {
		try {
			//List<Map<String, Object>> list = contDao.getConstData(_uniqId);
			this.contractMain = contDao.ReadByUniqId(_uniqId);
			this.contractMainActInfo = contractMainActInfoDao.ReadByUniqId(_uniqId);

			this.setEncryptPassword(contractMain.getIdno());
			// (PL) CONTRACT_PRODUCT_ID_TYPE_LOAN
			// (CASHCARD) CONTRACT_PRODUCT_ID_TYPE_CASH
			// (RPL) CONTRACT_PRODUCT_ID_TYPE_ELOAN
			String productId = contractMain.getProductId();

			// PageFooter footer = new PageFooter();
			footer.productId = productId;
			this.setPageEvent(footer);

			// 取得Html的方法
			String StaticFilePath = global.StaticFilePath;
			logger.info("### StaticFilePath => " + StaticFilePath);
			final String htmlUrl = StaticFilePath + globalConfig.PDF_Contract_PL_ACH_TemplateFilePath();
			// CaseAuth caseAuth = caseAuthDao.Read(new CaseAuth(_uniqId));
			templateVersion = globalConfig.findContract_VerNo(productId);

			logger.info("### htmlUrl => " + htmlUrl);
			InputStream input = new FileInputStream(new File(htmlUrl));
			this.setRawHtml(this.getContentFromFilePath(input));
		} catch (RuntimeException rex) {
			throw rex;
		} catch (Exception ex) {
//			ex.printStackTrace();
		}
	}

    @Override
	public void afterPdfProcess() {
		// 3.存入撥款日期、合約日期

	}

    @Override
	public String replaceField(String raw) throws Exception {
		System.out.println("### Begin - replaceField");
        String str = "";
        String productId = contractMain.getProductId();
        if (isPL(productId)) { // PL 才有 ACH
            GetDgtConstRspDto apsConstData = new Gson().fromJson(contractMain.getApsConstData(), GetDgtConstRspDto.class);
            GetDgtConstRspDtoConstData constData = apsConstData.findOneConstData();
    
            // 利用 Branch_id 得知要寄送的地址
            sendAddress = getSendAddress(KB_9743_CHANNEL_ID, constData.getBranch_id());
    
            str = raw;
            ContractAchPDFDomain domain = new ContractAchPDFDomain();
            domain.make(        
            contractMainActInfo,
            sendAddress);
    
            // 取代html中的變數為內容值
            Map<String, String> map = domain.getMap();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                str = str.replace(entry.getKey(), entry.getValue());
            }
            System.out.println("### END - replaceField");
            System.out.println("### previewHtml.length()=" + str.length());
    
            // HTML檔 - @export 儲存至檔案系統
            if (globalConfig.isPDF_Export()) {
                ExportUtil.export(global.PdfDirectoryPath + "/" + "ContractAchPDF.previewHtml.html", str);
            }
        }
		return str;
    }

	@Override
	public void saveToDB(byte[] pdfByte) {

		// Save bytes to TABLE: CaseDocument
		CaseDocument cd = new CaseDocument(this._uniqId, UNIQ_TYPE_03, DOCUMENT_TYPE_CONTRACT_ACH_PDF);
		// 會有預覽資料 所以先讀取
		cd = caseDocumentDao.Read(cd); // TODO: Charles
		// 真的查不到 就塞回預設值吧
		if(cd == null){
			cd = new CaseDocument(this._uniqId, UNIQ_TYPE_03, DOCUMENT_TYPE_CONTRACT_ACH_PDF);
		}
        cd.setContent(pdfByte);
        cd.setVersion(templateVersion);
		caseDocumentDao.CreateOrUpdate(cd); // TODO: Charles
		
		// Save string base64 to TABLE: CaseDoc
		String str = Base64.encodeBase64String(pdfByte);
		contractDao.updateContractPdf(_uniqId, str);
		System.out.println("Save Pdf To DB"); // TODO: Charles

		// PDF檔 - @export 儲存至檔案系統
		if (globalConfig.isPDF_Export()) {
			ExportUtil.export(global.PdfDirectoryPath + "/" + _uniqId + ".pdf", pdfByte);
		}
	}

    @Override
	public void pdfToImage(List<byte[]> byteList) {
		System.out.println("pdfToImage");
		System.out.println(byteList.size());
	}

	@Override
	public void sendEmail(byte[] pdfByte) {

	}

    /**
	 * 
	 * 利用利用 Branch_id 得知要寄送的地址
	 * @param channelId (KB)
	 * @param departId APS 銀行代碼(GET_DGT_CONST 來)
	 */
	public String getSendAddress(String channelId, String departId) {
		return qr_ChannelDepartListDao.getSendAddress(channelId, departId);
	}
}
