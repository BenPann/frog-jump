package com.kgi.airloanex.ap.service;

import com.google.gson.Gson;
import com.kgi.eopend3.ap.dao.BrowseLogDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.db.BrowseLog;
import com.kgi.eopend3.common.dto.view.PageView;
import com.kgi.eopend3.common.util.CheckUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BrowseService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    BrowseLogDao browseLogDao;

    public String setPage(String reqJson, String uniqId, String uniqType, String ipAddress) {
        try {
            Gson gson = new Gson();
            PageView view = gson.fromJson(reqJson, PageView.class);
            if (!CheckUtil.check(view)) {
                throw new ErrorResultException(1, "參數錯誤", null);
            } else {
                BrowseLog browselog = new BrowseLog();
                browselog.setUniqId(uniqId);
                browselog.setUniqType(uniqType);
                browselog.setAction(view.getAction());
                browselog.setPage("/" + view.getPageName());
                browselog.setIPAddress(ipAddress);
                browseLogDao.Create(browselog);
                return WebResult.GetResultString(0, "成功", "");
            }
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, "系統錯誤", null);
        }
    }
}