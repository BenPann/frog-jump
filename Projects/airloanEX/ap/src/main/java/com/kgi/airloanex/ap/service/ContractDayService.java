package com.kgi.airloanex.ap.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.ContractApiLogDao;
import com.kgi.airloanex.ap.dao.OracleWdDao;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.customDto.GetExceedStatusRspDto;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 取營業日
 * 
 * 取立約日之撥款日(下一營業日)
 */
@Service
public class ContractDayService {

    private static final Logger logger = LogManager.getLogger(ContractDayService.class.getName());

	@Autowired
	private AirloanEXConfig globalConfig;

    @Autowired
	private OracleWdDao oracleWdDao;

    @Autowired
    private ContractApiLogDao contractApiLogDao;

    @Autowired
    private KgiService kgiService;

	/**
	 * 取營業日
	 * @param Date yyyyMMdd
	 */
	public String getPayDate(String Date) throws Exception {
		return oracleWdDao.getNextWorkDay(Date);
	}

    // 取立約日之撥款日(下一營業日)
	public String getContractPayDate(String uniqId, String caseNoWeb, String cDate, String productId, String payBank, String expBankNo, String expBankAcctNo, String exceed_flg) throws Exception {
		String qryName = "getContractPayDate";
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("cDate", cDate); 
		map.put("productId", productId); 
		map.put("payBank", payBank); 
		map.put("expBankNo", expBankNo); 
		map.put("expBankAcctNo", expBankAcctNo); 
		map.put("exceed_flg", exceed_flg);
		String queryJson = new Gson().toJson(map);
        String response = "";
        String startTime = "";
        String endTime = "";

		try {

			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");

			System.out.println(">>>>>>>>>>>>>>  getContractPayDate Start");
			String paydate; // HHmmss
	
			// 立約日如為14點之前
			// 信貸的立約時間點 改為15點之前 20190212 CR提出的
			// 立約時間點調整 exp_bank_no撥款銀行 自行改為21點之前 它行改為15點之前 如果有代償銀行資料 改為 15點之前   20201103 Ben提出調整
			System.out.println("※※※TEST※※..cDate ==> " + cDate.substring(8, 14));
	
			// GET_EXCEED_STATUS expBankNo, expBankAcctNo
			boolean isTomorrow = isTomorrow(uniqId, caseNoWeb, cDate, productId, payBank, expBankNo, expBankAcctNo, exceed_flg);
			paydate = getContractPayDate(cDate, isTomorrow);
			System.out.println("###END---getContractPayDate  ==> " + paydate);
			System.out.println("<<<<<<<<<<<<<<<  getContractPayDate End ");
	
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
	
			response = paydate;
		} catch(Exception e) {
			logger.error("Fail to getContractPayDate 取立約日之撥款日(下一營業日)", e);
		} finally {
			if (uniqId != null) {
				contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, uniqId, qryName, queryJson, response, startTime, endTime);
			}
		}
		return response;
	}

	/**
	 * 取營業日
	 * 若 isTomorrow =  true, 根據當日(cDate    )，取營業日。
	 * 若 isTomorrow = false, 根據隔日(cDate + 1)，取營業日。
	 */
	public String getContractPayDate(String cDate, boolean isTomorrow) throws Exception {
		String paydate;
		if (isTomorrow) {			
			// 拿明天的日期 取得下個工作日
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Calendar cd = Calendar.getInstance();
			cd.setTime(sdf.parse(cDate.substring(0, 8)));
			cd.add(Calendar.DATE, 1);// 增加一天
			paydate = getPayDate(sdf.format(cd.getTime())); // yyyyMMdd
		} else {
			paydate = getPayDate(cDate.substring(0, 8)); // yyyyMMdd
		}
		return paydate;
	}

    /**
	 * 判斷是否為隔日：
	 * 若false，則為當日(取營業日)。
	 * 若 true，則為隔日(取營業日)。
	 */
	private boolean isTomorrow(String uniqId, String caseNoWeb, String cDate, String productId, String payBank, String expBankNo, String expBankAcctNo, String exceed_flg) {
		
		boolean is_GET_EXCEED_STATUS_and_gt_payDateTime_exceed = false;
		boolean is_PL_lt_payDateTime_pl_pay_bank_NotPayBank = false;
		boolean is_PL_lt_payDateTime_pl_exp_bank_other = false;
		boolean is_PL_lt_payDateTime_pl_exp_bank_809 = false;
		boolean is_RPL_lt_payDateTime_rpl = false;
		boolean is_CASHCARD_lt_payDateTime_cash = false;
		boolean isExceed = false;
		String HHmmss = cDate.substring(8, 14);
		
		String payDateTime_pl_pay_bank = globalConfig.payDateTime_pl_pay_bank();
		String payDateTime_pl_exp_bank_other = globalConfig.payDateTime_pl_exp_bank_other();
		String payDateTime_pl_exp_bank_809 = globalConfig.payDateTime_pl_exp_bank_809();
		String payDateTime_rpl = globalConfig.payDateTime_rpl();
		String payDateTime_cash = globalConfig.payDateTime_cash();
		String payDateTime_exceed_flg = globalConfig.payDateTime_exceed_flg();

        String startTime = "";
        String endTime = "";
		String response = "";

		try {
			startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			is_GET_EXCEED_STATUS_and_gt_payDateTime_exceed = is_GET_EXCEED_STATUS_and_gt_payDateTime_exceed(uniqId, caseNoWeb, cDate, productId, expBankNo, expBankAcctNo);
			is_PL_lt_payDateTime_pl_pay_bank_NotPayBank = 
				StringUtils.equals(productId, PlContractConst.CONTRACT_PRODUCT_ID_TYPE_LOAN) // 是PL(1) pay_bank代償銀行有值 時間小於15點前
				&& Integer.valueOf(HHmmss) < Integer.valueOf(payDateTime_pl_pay_bank)
				&& StringUtils.isNotBlank(payBank);
	
			is_PL_lt_payDateTime_pl_exp_bank_other = 
				StringUtils.equals(productId, PlContractConst.CONTRACT_PRODUCT_ID_TYPE_LOAN) // 是PL(1) exp_bank_no撥款銀行為他行 時間小於15點前
				&& Integer.valueOf(HHmmss) < Integer.valueOf(payDateTime_pl_exp_bank_other)
				&& StringUtils.isNotBlank(expBankNo)           // 撥款銀行 需要以最終寫入contractMainInfo的signBankId欄位為主
				&& !StringUtils.equals("809", expBankNo.substring(0, 3));
	
			is_PL_lt_payDateTime_pl_exp_bank_809 =
				StringUtils.equals(productId, PlContractConst.CONTRACT_PRODUCT_ID_TYPE_LOAN) // 是PL(1) exp_bank_no撥款銀行為自行 時間小於21點前
				&& Integer.valueOf(HHmmss) < Integer.valueOf(payDateTime_pl_exp_bank_809)
				&& StringUtils.isNotBlank(expBankNo)           // 撥款銀行 需要以最終寫入contractMainInfo的signBankId欄位為主
				&& StringUtils.equals("809", expBankNo.substring(0, 3));
	
			is_RPL_lt_payDateTime_rpl = 
				StringUtils.equals(productId, PlContractConst.CONTRACT_PRODUCT_ID_TYPE_ELOAN) // 是RPL(3) 目前以當天為主 20201201提出要求也要設定參數
				&& Integer.valueOf(HHmmss) < Integer.valueOf(payDateTime_rpl);
			
			is_CASHCARD_lt_payDateTime_cash = 
				StringUtils.equals(productId, PlContractConst.CONTRACT_PRODUCT_ID_TYPE_CASH)  // 是cashCard(2) 目前以當天為主 20201201提出要求也要設定參數
				&& Integer.valueOf(HHmmss) < Integer.valueOf(payDateTime_cash);
	
			// 查詢立約案件熔斷狀態(客戶自行填寫撥款資訊時查詢)
			if (is_GET_EXCEED_STATUS_and_gt_payDateTime_exceed) {
				response = String.valueOf(true);
				return true;
			}
			System.out.println("※※※payBank=" + payBank);
			// 沒過設定時間--->當日
			if (is_PL_lt_payDateTime_pl_pay_bank_NotPayBank) {
				System.out.println("※※※TEST※※..==> payDateTime.pl.pay_bank");
				// return false;
				isExceed = isExceed(cDate, exceed_flg);
				response = String.valueOf(isExceed);
				return isExceed;
			} else if (is_PL_lt_payDateTime_pl_exp_bank_other) {
				System.out.println("※※※TEST※※..==> payDateTime.pl.exp_bank_other");
				// return false;
				isExceed = isExceed(cDate, exceed_flg);
				response = String.valueOf(isExceed);
				return isExceed;
			} else if (is_PL_lt_payDateTime_pl_exp_bank_809) {
				System.out.println("※※※TEST※※..==> payDateTime.pl.exp_bank_809");
				// return false;
				isExceed = isExceed(cDate, exceed_flg);
				response = String.valueOf(isExceed);
				return isExceed;
			} else if (is_RPL_lt_payDateTime_rpl) {
				System.out.println("※※※TEST※※..==> payDateTime.rpl");
				// return false;
				isExceed = isExceed(cDate, exceed_flg);
				response = String.valueOf(isExceed);
				return isExceed;
			} else if (is_CASHCARD_lt_payDateTime_cash) {
				System.out.println("※※※TEST※※..==> payDateTime.cash");
				// return false;
				isExceed = isExceed(cDate, exceed_flg);
				response = String.valueOf(isExceed);
				return isExceed;
			} else { // 超過設定時間--->切下一營業日
				// 拿明天的日期 取得下個工作日
				System.out.println("※※※TEST※※..==> 拿明天的日期 取得下個工作日");
				response = String.valueOf(true);
				return true;
			}
		} catch(Exception e) {
			throw e;
		} finally {
			String qryName = "isTomorrow";
			Map<String, String> map = new LinkedHashMap<String, String>();
			map.put("is_GET_EXCEED_STATUS_and_gt_payDateTime_exceed", is_GET_EXCEED_STATUS_and_gt_payDateTime_exceed ? "1" : "0"); 
			map.put("is_PL_lt_payDateTime_pl_pay_bank_NotPayBank", is_PL_lt_payDateTime_pl_pay_bank_NotPayBank ? "1" : "0"); 
			map.put("is_PL_lt_payDateTime_pl_exp_bank_other", is_PL_lt_payDateTime_pl_exp_bank_other ? "1" : "0"); 
			map.put("is_PL_lt_payDateTime_pl_exp_bank_809", is_PL_lt_payDateTime_pl_exp_bank_809 ? "1" : "0"); 
			map.put("is_RPL_lt_payDateTime_rpl", is_RPL_lt_payDateTime_rpl ? "1" : "0"); 
			map.put("is_CASHCARD_lt_payDateTime_cash", is_CASHCARD_lt_payDateTime_cash ? "1" : "0"); 
			map.put("isExceed",isExceed ? "1" : "0");
			map.put("HHmmss", HHmmss);
			map.put("payDateTime_pl_pay_bank", payDateTime_pl_pay_bank);
			map.put("payDateTime_pl_exp_bank_other", payDateTime_pl_exp_bank_other);
			map.put("payDateTime_pl_exp_bank_809", payDateTime_pl_exp_bank_809);
			map.put("payDateTime_rpl", payDateTime_rpl);
			map.put("payDateTime_cash", payDateTime_cash);
			map.put("payDateTime_exceed_flg", payDateTime_exceed_flg);
			String queryJson = new Gson().toJson(map);			
			endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
			if (uniqId != null) {
				contractApiLogDao.Create(PlContractConst.APILOG_TYPE_CALLOUT, uniqId, qryName, queryJson, response, startTime, endTime);
			}
		}
	}

    /**
	 * GET_EXCEED_STATUS 的 exceed_flg<>N，且超過熔斷的時點，切換至隔日
	 */
	private boolean is_GET_EXCEED_STATUS_and_gt_payDateTime_exceed(String uniqId, String caseNoWeb, String cDate, String productId, String expBankNo, String expBankAcctNo) {
		boolean isExceed = is_GET_EXCEED_STATUS(uniqId, caseNoWeb, productId, expBankNo, expBankAcctNo) && isPayDateTime_exceed(cDate);
		if (isExceed) {
			System.out.println("※※※TEST※※..==> GET_EXCEED_STATUS exceed_flg<>N 且 超過熔斷設定營業時間--->切下一營業日");
			return true;
		} else {
			System.out.println("※※※TEST※※..==> GET_EXCEED_STATUS 沒過熔斷設定營業時間--->當日");
			return false;
		}
	}

    /**
	 * 查詢立約案件熔斷狀態(客戶自行填寫撥款資訊時查詢) GET_EXCEED_STATUS
	 */
	private boolean is_GET_EXCEED_STATUS(String uniqId, String caseNoWeb, String productId, String expBankNo, String expBankAcctNo) {
		// call GET_EXCEED_STATUS
		if (StringUtils.isNotBlank(expBankNo) && StringUtils.isNotBlank(expBankAcctNo)) {
			if (StringUtils.equals(productId, PlContractConst.CONTRACT_PRODUCT_ID_TYPE_LOAN)) {
				try {
					GetExceedStatusRspDto rsp = kgiService.call_GET_EXCEED_STATUS(uniqId, caseNoWeb, expBankNo, expBankAcctNo);
					if (StringUtils.equals("", rsp.getCHECK_CODE())) {
						System.out.println("※※※TEST※※..==> GET_EXCEED_STATUS 的 exceed_flg=" + rsp.getExceed_flg());
						boolean isTomorrow = !StringUtils.equals("N", rsp.getExceed_flg());
						if (isTomorrow) {
							return true;
						}
					}
				} catch(Exception e) {
					logger.error("GET_EXCEED_STATUS failed.", e);
				}
			}
		}
		return false;
	}

	/**
	 * 判斷熔斷時點是否為隔日：
	 * 若isExceed=false，則為當日(取營業日)。
	 * 若isExceed= true，則為隔日(取營業日)。
	 * 
	 * exceed_flg<>N 且 超過熔斷設定營業時間--->切下一營業日
	 */
	private boolean isExceed(String cDate, String exceed_flg) {
		// exceed_flg：熔斷註記(N/E1/E2/E3/E4/E5/E6/E7/E8)，N-非熔斷
		boolean isExceedFlg = !StringUtils.equals("N", exceed_flg);
		if (isExceedFlg && isPayDateTime_exceed(cDate)) {
			System.out.println("※※※TEST※※..==> exceed_flg<>N 且 超過熔斷設定營業時間--->切下一營業日");
			return true;
		} else {
			System.out.println("※※※TEST※※..==> 沒過熔斷設定營業時間--->當日");
			return false;
		}
	}

    /**
	 * 是否超過熔斷的時點 airloanEX.payDateTime.exceed_flg 格式 HHmmss
	 * 
	 * @param cDate yyyyMMddHHmmss
	 * @return
	 */
	private boolean isPayDateTime_exceed(String cDate) {
		return Integer.valueOf(cDate.substring(8, 14)) > Integer.valueOf(globalConfig.payDateTime_exceed_flg());
	}

    /**
	 * 貸款期間邏輯處理
	 */
    public String loanPeriod(String uniqId, String caseNoWeb, String dealPaydate, String contractdate, String productId, String payBank, String expBankNo, String expBankAcctNo, String exceed_flg) throws Exception {
		String payDate = "";
        if (StringUtils.isBlank(dealPaydate)) {
        	// aps_constdata.pay_date 是 空值 拿當天contractdate
        	// payDate = getContractPayDate(contractdate, productId, constData);
        	payDate = getContractPayDate(uniqId, caseNoWeb, contractdate, productId, payBank, expBankNo, expBankAcctNo, exceed_flg);
        } else {
        	if (Integer.valueOf(contractdate.substring(0, 8)) < Integer.valueOf(dealPaydate)) {
        		// 拿aps_constdata.pay_date 取得下個工作日
        		payDate = getPayDate(dealPaydate);
        	} else {
        		// 拿aps_constdata.pay_date 是 空值 拿當天contractdate
        		// payDate = getContractPayDate(contractdate, productId, constData);
        		payDate = getContractPayDate(uniqId, caseNoWeb, contractdate, productId, payBank, expBankNo, expBankAcctNo, exceed_flg);
        	}
		}
		return payDate;
	}
}
