package com.kgi.airloanex.ap.service;

import static com.kgi.airloanex.ap.service.CaseDataService.isMobilePhone;
import static com.kgi.airloanex.ap.service.CaseDataService.splitAddress;
import static com.kgi.airloanex.common.PlContractConst.CaseDataJobStatus.CASE_STATUS_01_JobStatus_040;
import static com.kgi.airloanex.common.PlContractConst.CaseDataJobStatus.CASE_STATUS_01_JobStatus_900;
import static com.kgi.airloanex.common.PlContractConst.CaseDataSubStatus.CASE_STATUS_01_SubStatus_900;
import static com.kgi.airloanex.common.PlContractConst.CaseDataSubStatus.CASE_STATUS_01_SubStatus_910;
import static com.kgi.airloanex.common.PlContractConst.CaseDataSubStatus.CASE_STATUS_01_SubStatus_911;

import java.util.List;
import java.util.stream.Collectors;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.AuditDataDao;
import com.kgi.airloanex.ap.dao.CaseAuthDao;
import com.kgi.airloanex.ap.dao.CaseDataDao;
import com.kgi.airloanex.common.dto.customDto.CreatenewCaseStep2RspDto;
import com.kgi.airloanex.common.dto.customDto.GetDgtcaseInfoRspDto;
import com.kgi.airloanex.common.dto.customDto.GetKycDebtInfoRspDtoDebtInfo;
import com.kgi.airloanex.common.dto.customDto.UpdateCaseInfoRsqDto;
import com.kgi.airloanex.common.dto.db.CaseAuth;
import com.kgi.airloanex.common.dto.db.CaseData;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JcicService { 
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private CaseDataDao caseDataDao;

    @Autowired
    private JobLogService jobLogService;

    @Autowired
    private KgiService kgiService;

    @Autowired
    private AuditDataDao auditDataDao;

    @Autowired
    CaseAuthDao caseAuthDao;

    @Autowired
    private CaseDataService caseDataService;

    @Autowired
    private AirloanEXConfig globalConfig;
	
	public void dumpLog(String inStr){
		System.out.println(inStr);
		logger.info(inStr);
	}
	
	
    public void sendJcic() {
        List<CaseData> caseDatas = caseDataDao.queryWaitJcic();
        sendJcicStep2(caseDatas);
        sendJcicStep3(caseDatas);
    }

    /** 排程 step2 */
    public void sendJcicStep2(List<CaseData> caseDatas) {

        List<String> uniqIds = caseDatas.parallelStream().map(caseData -> {
            String uniqId = caseData.getCaseNo();
            String createNewCaseCaseNo = caseData.getCaseNoWeb();
			
			dumpLog("######## [排程] sendJcicStep2 處理 " + uniqId + " 中.....");
			
            try {
                
                // @setup caseData.SubStatus 01 900
                caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_900, CASE_STATUS_01_JobStatus_900);

                // 1. CREATE_NEW_CASE_STEP2 發查聯徵
                createNewCaseStep2(caseData);
                // 2. GET_DGTCASE_INFO 取得案件申請評分等..資訊
                step2GetDgtcaseInfo(uniqId, createNewCaseCaseNo);
                // 3. GET_KYC_DEBT_INFO
                // @call 20. 呼叫外部負債資料查詢
                checkDebtInfo(caseData);

            } catch (Exception e) {
                logger.error("sendJcicStep2. CaseData's CaseNo:" + uniqId, e);
                
                // @setup caseData.SubStatus 01 911
                caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_911, CASE_STATUS_01_JobStatus_040);
                jobLogService.AddSendDataConnOutLog("發查聯徵一般進件案件".concat(uniqId).concat("失敗"));
            }
			
			dumpLog("######## [排程] sendJcicStep2 處理 " + uniqId + " 完成.");			
			
            return uniqId;
        }).collect(Collectors.toList());

        // logger.info("### 排程 sendJcic - uniqIds=" + uniqIds);

    } // end sendJcicStep2

    /** 第二次產製信貸申請書時 step3 */
    public void sendJcicStep3(List<CaseData> caseDatas) {

        List<String> uniqIds = caseDatas.parallelStream().map(caseData -> {
            String uniqId = caseData.getCaseNo();
            String createNewCaseCaseNo = caseData.getCaseNoWeb();
			
			dumpLog("######## [排程] sendJcicStep3 處理 " + uniqId + " 中.....");
            
			try {
                // 1. UPDATE_CASE_INFO
                updateCaseInfo(caseData);
                // 2. GET_DGTCASE_INFO 取得案件申請評分等..資訊
                step3GetDgtcaseInfo(uniqId, createNewCaseCaseNo);

            // @setup caseData.SubStatus 01 910
            caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_910, CASE_STATUS_01_JobStatus_040);
			
            jobLogService.AddSendDataConnOutLog("發查聯徵一般進件案件 ".concat(uniqId).concat(" 成功"));

            } catch (Exception e) {
                logger.error("sendJcicStep3 Failed. CaseData's CaseNo:" + uniqId, e);
                
                // @setup caseData.SubStatus 01 911
                caseDataDao.updateSubStatusByCaseNo(uniqId, CASE_STATUS_01_SubStatus_911, CASE_STATUS_01_JobStatus_040);
                jobLogService.AddSendDataConnOutLog("發查聯徵一般進件案件".concat(uniqId).concat("失敗"));
            }
			
			dumpLog("######## [排程] sendJcicStep3 處理 " + uniqId + " 完成.");	
			
            return uniqId;
        }).collect(Collectors.toList());

        // logger.info("### 排程 sendJcic - uniqIds=" + uniqIds);

    } // end sendJcicStep3

    /**
     * 
     */
    public void updateCaseInfo(CaseData caseData) throws Exception {
        String uniqId = caseData.getCaseNo();
        String createNewCaseCaseNo = caseData.getCaseNoWeb();

        String COMP_CASE = caseDataService.makeCOMP_CASE(uniqId);

        // @call 23. UPDATE_CASE_INFO 更新案件資訊(STP)
        String HOUSE_REG_CITY_CODE = caseData.getResAddrZipCode();
        String HOUSE_REG_ADDRESS = splitAddress(caseData.getResAddr());

        String resTelArea = caseData.getResTelArea();
        String resTel = caseData.getResTel();
        if (isMobilePhone(resTel)) {
            resTelArea = "00";
        }
        String HOUSE_REG_AREA = resTelArea;
        String HOUSE_REG_TEL = resTel;
        
		
		String JOBTITLE = caseData.getTitle();
		String OCCUPATION = caseData.getOccupation();
		
		String AML_FLG = "";
		if (StringUtils.equals("負責人", JOBTITLE) && StringUtils.equals("1", StringUtils.substring(OCCUPATION, 0,1))){
			AML_FLG = "Y";
		}else{
			AML_FLG = "N";
		}
		
		
        String AML_RESULT = caseData.getAmlResult();
		if ( StringUtils.equals("9", AML_RESULT) || StringUtils.equals("99", AML_RESULT) || StringUtils.equals("", AML_RESULT)){
			AML_RESULT = "O";
		}
		
		
        
        // if (StringUtils.length(AML_RESULT) == 1) {
            // if (StringUtils.equals("N", AML_RESULT)) {
				// if (StringUtils.equals("9", AML_RESULT)){
					
				// }
            // } else {
                // AML_FLG = "Y"; // (Y/V/N/O/P) Y-ID命中禁制名單、V-疑似高 風險名單
            // }
        // } else {
            // AML_RESULT = "O";
        // }
		
		
		
        // 1.當主表 (HasQryIdInfo='01' and OldIdImgNoList<>'' and HasUploadId=00) 時，FAST_ID_FLG 帶入 Y，其餘都帶入 N
        String FAST_ID_FLG = 
        (StringUtils.equals("01", caseData.getHasQryIdInfo()) 
        && StringUtils.isNotBlank(caseData.getOldIdImgNoList()) 
        && StringUtils.equals("00", caseData.getHasUploadId())) ? "Y" : "N";

        UpdateCaseInfoRsqDto uci = kgiService.call_UPDATE_CASE_INFO(uniqId, createNewCaseCaseNo, HOUSE_REG_CITY_CODE, HOUSE_REG_ADDRESS, HOUSE_REG_AREA, HOUSE_REG_TEL, AML_FLG, AML_RESULT, COMP_CASE, FAST_ID_FLG);
    } // end updateCaseInfo

    /** 排程 step2 */
    public void step2GetDgtcaseInfo(String uniqId, String caseNoWeb) throws Exception {
        int loop = Integer.valueOf(globalConfig.loop_GET_DGTCASE_INFO());
        int wait = Integer.valueOf(globalConfig.wait_GET_DGTCASE_INFO());
		
		dumpLog("######## [排程] sendJcicStep2 處理 " + uniqId + " GetDgtcaseInfo 等待中....");	
		
        for (int i = 0; i < loop; i++) {
            
            GetDgtcaseInfoRspDto rsp = kgiService.call_GET_DGTCASE_INFO(uniqId, caseNoWeb);
            if (StringUtils.equals("", rsp.getCHECK_CODE())) {
                String caseFlagStep2 = rsp.getCASE_FLAG_STEP2(); // Y/N
                // String caseStatus = rsp.getCASE_STATUS(); // 1.正常執行完畢2.系統發生錯誤,空字串:系統逾時
                // 0329-001.根據週五理債調整規格通知，於問題 0324-002 中，呼叫GET_DGTCASE_INFO後，改依據回傳的新增參數 CASE_FLAG來決定查詢聯徵作業是否已完成，已完成則停止30秒重新呼叫GET_DGTCASE_INFO，並記錄CASE_STATUS=1 or 2，並更新主表ProjectCode欄位、TABLE:AUDITDATA也要新增一筆紀錄
                if (StringUtils.equals("Y", caseFlagStep2)) {
                    // Update CaseData DgtcaseResult&DgtcaseTime
                    // caseDataDao.updateDgtcaseResult(uniqId, caseStatus, DateUtil.GetDateFormatString("yyyy-MM-dd HH:mm:ss.SSS"));
                    return;
                } else {
                    Thread.sleep(wait);
                }

            } // end if 
            
        } // end for
    }

    /** 第二次產製信貸申請書時 step3 */
    public void step3GetDgtcaseInfo(String uniqId, String caseNoWeb) throws Exception {
        
        int loop = Integer.valueOf(globalConfig.loop_GET_DGTCASE_INFO());
        int wait = Integer.valueOf(globalConfig.wait_GET_DGTCASE_INFO());
		
		dumpLog("######## [排程] sendJcicStep3 處理 " + uniqId + " GetDgtcaseInfo 等待中....");	
		
        for (int i = 0; i < loop; i++) {
            
            GetDgtcaseInfoRspDto rsp = kgiService.call_GET_DGTCASE_INFO(uniqId, caseNoWeb);
            if (StringUtils.equals("", rsp.getCHECK_CODE())) {
                String caseFlagStep3 = rsp.getCASE_FLAG_STEP3(); // Y/N
                String caseStatus = rsp.getCASE_STATUS(); // 1.正常執行完畢2.系統發生錯誤,空字串:系統逾時
                // 0329-001.根據週五理債調整規格通知，於問題 0324-002 中，呼叫GET_DGTCASE_INFO後，改依據回傳的新增參數 CASE_FLAG來決定查詢聯徵作業是否已完成，已完成則停止30秒重新呼叫GET_DGTCASE_INFO，並記錄CASE_STATUS=1 or 2，並更新主表ProjectCode欄位、TABLE:AUDITDATA也要新增一筆紀錄
                if (StringUtils.equals("Y", caseFlagStep3)) {

                    // Update PROJECT_CODE to CaseData prj_code
                    String projectCode = rsp.getPROJECT_CODE();
                    if (StringUtils.isNotBlank(projectCode)) {
                        caseDataDao.updatePrj_code(uniqId, projectCode);
                    }

                    // Update GMCARD_NO to CaseData GmCardNo
                    String gmcardNo = rsp.getGMCARD_NO();
                    if (StringUtils.isNotBlank(gmcardNo)) {
                        caseDataDao.updateGmCardNo(uniqId, gmcardNo);
                    }

                    // Update CaseData DgtcaseResult&DgtcaseTime
                    caseDataDao.updateDgtcaseResult(uniqId, caseStatus, DateUtil.GetDateFormatString("yyyy-MM-dd HH:mm:ss.SSS"));

                    // insert AUDITDATA
                    CaseAuth auth = caseAuthDao.findByUniqId(uniqId);
                    String uniqType = auth.getUniqType();
                    auditDataDao.createAuditData(uniqId, uniqType, rsp.getPROJECT_CODE(), rsp.getCREDIT_LINE(), rsp.getINTEREST_RATE(), rsp.getRANK(), rsp.getSCORE(), rsp.getSHOW_SCORE());

                    return;
                } else {
                    Thread.sleep(wait);
                }

            } // end if 
            
        } // end for
    }

    /**
     * CREATE_NEW_CASE 發查聯徵
     */
    private void createNewCaseStep2(CaseData caseData) throws Exception {
        String uniqId = caseData.getCaseNo();
        String caseNoWeb = caseData.getCaseNoWeb();
        CreatenewCaseStep2RspDto rsp = kgiService.call_CREATE_NEW_CASE_STEP2(uniqId, caseNoWeb);

        if (StringUtils.equals("", rsp.getCHECK_CODE())) {
            // 發查聯徵成功
            // 發查聯徵 更新CaseData欄位 JcicOk, JcicTime
            caseDataDao.updateJcic(uniqId, "Y");
        } else {
            // 發查聯徵失敗
            // 發查聯徵 更新CaseData欄位 JcicOk, JcicTime
            caseDataDao.updateJcic(uniqId, "N");
        }
        return;
    }

    /**
     * CHECK_DEBT_INFO 呼叫外部負債資料查詢
     */
    private void checkDebtInfo(CaseData caseData) throws Exception {
        String uniqId = caseData.getCaseNo();
        String decisionCaseNo = caseData.getDecisionCaseNo();
        String productId = caseData.getProductId();
        String Z07Date = DateUtil.GetDateFormatString("yyyyMMdd");
        GetKycDebtInfoRspDtoDebtInfo debtInfo = caseDataService.checkDebtInfo(uniqId, decisionCaseNo, productId, Z07Date);
        return;
    }
}
