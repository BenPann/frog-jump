package com.kgi.airloanex.ap.pdf;

import static com.kgi.airloanex.common.PlContractConst.UNIQ_TYPE_03;
import static com.kgi.airloanex.common.PlContractConst.DOCUMENT_TYPE_CONTRACT_PDF;
import static com.kgi.airloanex.common.PlContractConst.KB_9743_CHANNEL_ID;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.CaseAuthDao;
import com.kgi.airloanex.ap.dao.CaseDocumentDao;
import com.kgi.airloanex.ap.dao.ContractDocDao;
import com.kgi.airloanex.ap.dao.ContractMainActInfoDao;
import com.kgi.airloanex.ap.dao.ContractMainDao;
import com.kgi.airloanex.ap.dao.QR_ChannelDepartListDao;
import com.kgi.airloanex.ap.service.APSPlContractService;
import com.kgi.airloanex.ap.service.ContractDayService;
import com.kgi.airloanex.ap.service.SendMailHunterService;
import com.kgi.airloanex.common.domain.ContractPDFDomain;
import com.kgi.airloanex.common.domain.UpdateDgtConstDomain;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDto;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoConstData;
import com.kgi.airloanex.common.dto.db.CaseAuth;
import com.kgi.airloanex.common.dto.db.CaseDocument;
import com.kgi.airloanex.common.dto.db.ContractMain;
import com.kgi.airloanex.common.dto.db.ContractMainActInfo;
import com.kgi.airloanex.common.dto.db.DropdownData;
import com.kgi.airloanex.common.util.ExportUtil;
import com.kgi.eopend3.ap.config.GlobalConfig;
import com.kgi.eopend3.ap.dao.DropdownDao;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ContractPDF extends BasePDFDocument {

	private Boolean replaceBankSeal = false;
	private String _uniqId;
	public String templateVersion = "";
	private String sendAddress = "";
	
	@Autowired
	private GlobalConfig global;

	@Autowired
	private AirloanEXConfig globalConfig;

	@Autowired
	private CaseAuthDao caseAuthDao;

	@Autowired
	private ContractMainDao contDao;

	@Autowired
	private ContractMainActInfoDao contractMainActInfoDao;

	@Autowired
	private ContractDocDao contractDao;

    @Autowired
    private DropdownDao dropwDownDao;

    @Autowired
    private APSPlContractService apsService;

	@Autowired
	private PageFooter footer;

	@Autowired
	private CaseDocumentDao caseDocumentDao;
	
	@Autowired
	private SendMailHunterService sendMailHunterService;

	@Autowired
	private QR_ChannelDepartListDao qr_ChannelDepartListDao;

	@Autowired
	private ContractDayService contractDayService;

	private static final Logger logger = LogManager.getLogger(ContractPDF.class.getName());

	/** 主檔 - 案件契約資料表(ContractMain) */
	private ContractMain contractMain;
	/** 立約帳戶驗身資料表(ContractMainActInfo) */
	private ContractMainActInfo contractMainActInfo;
	
	public ContractPDF(ApplicationContext appContext) {
		super(appContext);
	}

	public Boolean getReplaceBankSeal() {
		return replaceBankSeal;
	}

	public void setReplaceBankSeal(Boolean replaceBankSeal) {
		this.replaceBankSeal = replaceBankSeal;
	}

	/** 設定初始化參數
	 * @param params 第一個參數 UniqId 第二個參數 是否要使用公司章
	 */
	public void init(String... params) throws IOException {
		this._uniqId = params[0];
		// 是否加密
		this.setUseEncrepyt(true);
		// this.setEncryptPassword(idno);
		// 是否使用浮水印
		this.setUseWaterMark(true);
		this.setWaterMarkText(globalConfig.IMG_WATER_FONT());
		this.setWaterMarkFont(globalConfig.PDF_FONT_WATER_TTF());
		// 是否要PDF轉成圖片
		this.setPdfToImage(false);
		// 是否存DB
		this.setUseSaveToDB(true);
		// 是否發信
		this.setUseSendEMail(false);

		if(params[1] != null){
			this.setReplaceBankSeal(Boolean.valueOf(params[1]));
		}
	}

	@Override
	public void beforePdfProcess() {
		try {
			//List<Map<String, Object>> list = contDao.getConstData(_uniqId);
			this.contractMain = contDao.ReadByUniqId(_uniqId);
			this.contractMainActInfo = contractMainActInfoDao.ReadByUniqId(_uniqId);

			this.setEncryptPassword(contractMain.getIdno());
			// (PL) CONTRACT_PRODUCT_ID_TYPE_LOAN
			// (CASHCARD) CONTRACT_PRODUCT_ID_TYPE_CASH
			// (RPL) CONTRACT_PRODUCT_ID_TYPE_ELOAN
			String productId = contractMain.getProductId();

			// PageFooter footer = new PageFooter();
			footer.productId = productId;
			this.setPageEvent(footer);

			// 取得Html的方法
			String StaticFilePath = global.StaticFilePath;
			logger.info("### StaticFilePath => " + StaticFilePath);
			final String htmlUrl = StaticFilePath + globalConfig.findContract_TemplateFilePath(productId);
			// CaseAuth caseAuth = caseAuthDao.Read(new CaseAuth(_uniqId));
			templateVersion = globalConfig.findContract_VerNo(productId);

			logger.info("### htmlUrl => " + htmlUrl);
			InputStream input = new FileInputStream(new File(htmlUrl));
			this.setRawHtml(this.getContentFromFilePath(input));
		} catch (RuntimeException rex) {
			throw rex;
		} catch (Exception ex) {
//			ex.printStackTrace();
		}
	}

	@Override
	public void afterPdfProcess() {
		// 3.存入撥款日期、合約日期

	}

	@Override
	public String replaceField(String raw) throws Exception {
		System.out.println("### Begin - replaceField");
		String str = raw;
		String productId = contractMain.getProductId();		
		String verifyType = apsService.transAuthType(apsService.externalCheckTypeForAddNewCase(this._uniqId)) ;
		
		// 如果是中華電信的 用中華電信的時間
		CaseAuth caseAuth = new CaseAuth(this._uniqId);
		CaseAuth read = caseAuthDao.Read(caseAuth);

		GetDgtConstRspDto apsConstData = new Gson().fromJson(contractMain.getApsConstData(), GetDgtConstRspDto.class);
		GetDgtConstRspDtoConstData constData = apsConstData.findOneConstData();

		boolean isReplaceBankSeal = this.getReplaceBankSeal();
		String contentBase64 = findBankSeal();

		// 判斷後正式 最終撥款日期 finalPayDate
		String finalPayDateString = "";
		// 1.貸款期間解析
		String contractdate = DateUtil.GetDateFormatString(); // yyyyMMddHHmmss
		String signTime = contractMain.getSignTime();
		if (StringUtils.isNotBlank(signTime)) {
			signTime = StringUtils.replace(signTime, "-", "");
			signTime = StringUtils.replace(signTime, ":", "");
			signTime = StringUtils.replace(signTime, ".", "");
			signTime = StringUtils.replace(signTime, " ", "");
			signTime = StringUtils.substring(signTime, 0, 14); // yyyyMMddHHmmss
			contractdate = signTime;
		}
		// APS PAYDAY
		String dealPaydate = constData.getPay_date();

		// 貸款期間邏輯處理
		String expBankNo; // 撥款銀行代碼
		String signBankId = this.contractMainActInfo.getSignBankId();
		String signBranchId = this.contractMainActInfo.getSignBranchId();
		if (StringUtils.isBlank(signBranchId)) {
			expBankNo = signBankId + "0000";
		} else {
			expBankNo = signBankId + signBranchId;
		}
		String expBankAcctNo = this.contractMainActInfo.getSignAccountNo(); // 撥款銀行帳戶
		finalPayDateString = contractDayService.loanPeriod(this._uniqId, contractMain.getCaseNoWeb(), dealPaydate, contractdate, productId, constData.getPay_bank(), expBankNo, expBankAcctNo, constData.getExceed_flg());

		// 2019/5/21 拿aps_constdata.prjectcode 串 dropdowndata cl.casepriority
		// 有串到 就直接拿 pay_date
		String projectCode = constData.getProject_code();
//			System.out.println("projectCode:"+projectCode);
		DropdownData dropdown = new DropdownData();
		dropdown.setName("CL.CasePriority");
		List<DropdownData> dropdownList = dropwDownDao.Query(dropdown);
		for(int i = 0 ; i < dropdownList.size() ; i++) {
			if(StringUtils.equals(projectCode, dropdownList.get(i).getDataKey())) {
				finalPayDateString = contractDayService.getPayDate(dealPaydate);
				break;
			}
		}
		System.out.println("### finalPayDateString ==>" + finalPayDateString);
		// 利用 Branch_id 得知要寄送的地址
		sendAddress = getSendAddress(KB_9743_CHANNEL_ID, constData.getBranch_id());

        UpdateDgtConstDomain dgt = new UpdateDgtConstDomain();
		String const_sign_no = globalConfig.findContract_VerDate(productId);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date finalPayDate = sdf.parse(finalPayDateString);
        dgt.make(contractMain, contractMainActInfo, const_sign_no, DateUtil.GetDateFormatString(finalPayDate, "yyyy-MM-dd HH:mm:ss.SSS"));

		ContractPDFDomain domain = new ContractPDFDomain();
		domain.make(        
		_uniqId,
        contractMain, 
        contractMainActInfo,
        verifyType,
        contractdate,
        dealPaydate,
        finalPayDateString,
        read,
        isReplaceBankSeal,
		contentBase64,
		sendAddress,
		dgt);

		// 取代html中的變數為內容值
		Map<String, String> map = domain.getMap();
		for (Map.Entry<String, String> entry : map.entrySet()) {
			str = str.replace(entry.getKey(), entry.getValue());
		}

		saveContractDate(_uniqId, dealPaydate, finalPayDateString, contractdate);

		System.out.println("### END - replaceField");
		System.out.println("### previewHtml.length()=" + str.length());

		// HTML檔 - @export 儲存至檔案系統
		if (globalConfig.isPDF_Export()) {
			ExportUtil.export(global.PdfDirectoryPath + "/" + "ContractPDF.previewHtml.html", str);
		}
		
		return str;
	}

	public String findBankSeal() {
		try {
			String StaticFilePath = global.StaticFilePath;
			String imageUrl = StaticFilePath + "/image/BankSeal.base64"; // FIXME
			File file = new File(imageUrl);
			String contentBase64 = new String(Files.readAllBytes(file.toPath()));		
			return contentBase64;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public String replaceBankSeal(String raw) {
		String str = "";
		String contentBase64 = findBankSeal();
		System.out.println("### contentBase64.length()=" + contentBase64.length());
		System.out.println();
		str = raw.replace("<div style=\"display: none;\">[Bank_seal]</div>",
				"<img src=\"data:image/jpeg;base64," + contentBase64 + "\" " + "alt=\"\" style=\"width:120px;\" />");
		return str;
	}

	public String replaceBankSealToBlank(String raw) {
		String str = raw.replace("<div style=\"display: none;\">[Bank_seal]</div>",
							"<div style=\"display: none;\">&nbsp;</div>");
		return str;
	}
	
	/**
	 * 
	 * @param uniqId
	 * @param dealPayDay APS 撥款日期(GET_DGT_CONST 來)
	 * @param finalPayDay 最終撥款日期(計算過後的 PayDay)
	 * @param contractDay 立約日期(立約當日)
	 * @throws Exception
	 */
	public void saveContractDate(String uniqId, String dealPayDay, String finalPayDay, String contractDay)
		throws Exception {
		// 立約日轉db存檔格式
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		Date cdate = formatter.parse(contractDay);
		SimpleDateFormat dbFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String dbcontractDay = dbFormat.format(cdate);
		contDao.updateContractPayDay(uniqId, dealPayDay, finalPayDay, dbcontractDay);
	}

	/**
	 * Save bytes to TABLE: CaseDocument
	 * Save string base64 to TABLE: ContractDoc
	 */
	@Override
	public void saveToDB(byte[] pdfByte) {

		// Save bytes to TABLE: CaseDocument
		CaseDocument cd = new CaseDocument(this._uniqId, UNIQ_TYPE_03, DOCUMENT_TYPE_CONTRACT_PDF);
		// 會有預覽資料 所以先讀取
		cd = caseDocumentDao.Read(cd);
		// 真的查不到 就塞回預設值吧
		if(cd == null){
			cd = new CaseDocument(this._uniqId, UNIQ_TYPE_03, DOCUMENT_TYPE_CONTRACT_PDF);
		}
        cd.setContent(pdfByte);
        cd.setVersion(templateVersion);
		caseDocumentDao.CreateOrUpdate(cd); // TABLE: CaseDocument
		
		// Save string base64 to TABLE: CaseDoc
		String str = Base64.encodeBase64String(pdfByte);
		contractDao.updateContractPdf(_uniqId, str); // TABLE: ContractDoc
		System.out.println("Save Pdf To DB");

		// PDF檔 - @export 儲存至檔案系統
		if (globalConfig.isPDF_Export()) {
			ExportUtil.export(global.PdfDirectoryPath + "/" + _uniqId + ".pdf", pdfByte);
		}
	}

	@Override
	public void pdfToImage(List<byte[]> byteList) {
		System.out.println("pdfToImage");
		System.out.println(byteList.size());
	}

	@Override
	public void sendEmail(byte[] pdfByte) {
		//ContractData contractData = (ContractData) JSONObject.toBean(JSONObject.fromObject(_contractData), ContractData.class);
		//sendMailHunterService.sendContractMailHunter(_uniqId, contractMain.getEmail(), pdfByte);
	}

     /**
	 * 
	 * 利用利用 Branch_id 得知要寄送的地址
	 * @param channelId (KB)
	 * @param departId APS 銀行代碼(GET_DGT_CONST 來)
	 */
	public String getSendAddress(String channelId, String departId) {
		return qr_ChannelDepartListDao.getSendAddress(channelId, departId);
	}
}