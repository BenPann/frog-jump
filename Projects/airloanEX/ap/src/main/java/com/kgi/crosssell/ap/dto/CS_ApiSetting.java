package com.kgi.crosssell.ap.dto;

public class CS_ApiSetting {

	private String ChannelId = "";
	private String DomainUrl = "";
	private String ApiUrl = "";

	public CS_ApiSetting() {
	};

	public CS_ApiSetting(String ChannelId) {
		this.ChannelId = ChannelId;
	}

	public String getChannelId() {
		return ChannelId;
	}

	public String getDomainUrl() {
		return DomainUrl;
	}

	public String getApiUrl() {
		return ApiUrl;
	}

	public void setChannelId(String channelId) {
		ChannelId = channelId;
	}

	public void setDomainUrl(String domainUrl) {
		DomainUrl = domainUrl;
	}

	public void setApiUrl(String apiUrl) {
		ApiUrl = apiUrl;
	}

}
