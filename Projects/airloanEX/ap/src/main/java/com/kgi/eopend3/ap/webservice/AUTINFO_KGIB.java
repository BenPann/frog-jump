/**
 * AUTINFO_KGIB.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.kgi.eopend3.ap.webservice;
@SuppressWarnings("all")
public class AUTINFO_KGIB  implements java.io.Serializable {
    private java.lang.String CHECKFLAG;

    private java.lang.String ID;

    private java.lang.String CUSTOMERNAME;

    private java.lang.String a_ITEM01;

    private java.lang.String AUTHORIZETOALLCORP;

    private java.lang.String TOCORP01;

    private java.lang.String TOCORP13;

    private java.lang.String TOCORP22;

    private java.lang.String TOCORP23;

    private java.lang.String TOCORP24;

    private java.lang.String TOCORP25;

    private java.lang.String TOCORP26;

    private java.lang.String TOCORP27;

    private java.lang.String TOCORP29;

    private java.lang.String FROMIP;

    private java.lang.String VERNO;

    private java.lang.String LASTMODIFIED;

    public AUTINFO_KGIB() {
    }

    public AUTINFO_KGIB(
           java.lang.String CHECKFLAG,
           java.lang.String ID,
           java.lang.String CUSTOMERNAME,
           java.lang.String a_ITEM01,
           java.lang.String AUTHORIZETOALLCORP,
           java.lang.String TOCORP01,
           java.lang.String TOCORP13,
           java.lang.String TOCORP22,
           java.lang.String TOCORP23,
           java.lang.String TOCORP24,
           java.lang.String TOCORP25,
           java.lang.String TOCORP26,
           java.lang.String TOCORP27,
           java.lang.String TOCORP29,
           java.lang.String FROMIP,
           java.lang.String VERNO,
           java.lang.String LASTMODIFIED) {
           this.CHECKFLAG = CHECKFLAG;
           this.ID = ID;
           this.CUSTOMERNAME = CUSTOMERNAME;
           this.a_ITEM01 = a_ITEM01;
           this.AUTHORIZETOALLCORP = AUTHORIZETOALLCORP;
           this.TOCORP01 = TOCORP01;
           this.TOCORP13 = TOCORP13;
           this.TOCORP22 = TOCORP22;
           this.TOCORP23 = TOCORP23;
           this.TOCORP24 = TOCORP24;
           this.TOCORP25 = TOCORP25;
           this.TOCORP26 = TOCORP26;
           this.TOCORP27 = TOCORP27;
           this.TOCORP29 = TOCORP29;
           this.FROMIP = FROMIP;
           this.VERNO = VERNO;
           this.LASTMODIFIED = LASTMODIFIED;
    }


    /**
     * Gets the CHECKFLAG value for this AUTINFO_KGIB.
     * 
     * @return CHECKFLAG
     */
    public java.lang.String getCHECKFLAG() {
        return CHECKFLAG;
    }


    /**
     * Sets the CHECKFLAG value for this AUTINFO_KGIB.
     * 
     * @param CHECKFLAG
     */
    public void setCHECKFLAG(java.lang.String CHECKFLAG) {
        this.CHECKFLAG = CHECKFLAG;
    }


    /**
     * Gets the ID value for this AUTINFO_KGIB.
     * 
     * @return ID
     */
    public java.lang.String getID() {
        return ID;
    }


    /**
     * Sets the ID value for this AUTINFO_KGIB.
     * 
     * @param ID
     */
    public void setID(java.lang.String ID) {
        this.ID = ID;
    }


    /**
     * Gets the CUSTOMERNAME value for this AUTINFO_KGIB.
     * 
     * @return CUSTOMERNAME
     */
    public java.lang.String getCUSTOMERNAME() {
        return CUSTOMERNAME;
    }


    /**
     * Sets the CUSTOMERNAME value for this AUTINFO_KGIB.
     * 
     * @param CUSTOMERNAME
     */
    public void setCUSTOMERNAME(java.lang.String CUSTOMERNAME) {
        this.CUSTOMERNAME = CUSTOMERNAME;
    }


    /**
     * Gets the a_ITEM01 value for this AUTINFO_KGIB.
     * 
     * @return a_ITEM01
     */
    public java.lang.String getA_ITEM01() {
        return a_ITEM01;
    }


    /**
     * Sets the a_ITEM01 value for this AUTINFO_KGIB.
     * 
     * @param a_ITEM01
     */
    public void setA_ITEM01(java.lang.String a_ITEM01) {
        this.a_ITEM01 = a_ITEM01;
    }


    /**
     * Gets the AUTHORIZETOALLCORP value for this AUTINFO_KGIB.
     * 
     * @return AUTHORIZETOALLCORP
     */
    public java.lang.String getAUTHORIZETOALLCORP() {
        return AUTHORIZETOALLCORP;
    }


    /**
     * Sets the AUTHORIZETOALLCORP value for this AUTINFO_KGIB.
     * 
     * @param AUTHORIZETOALLCORP
     */
    public void setAUTHORIZETOALLCORP(java.lang.String AUTHORIZETOALLCORP) {
        this.AUTHORIZETOALLCORP = AUTHORIZETOALLCORP;
    }


    /**
     * Gets the TOCORP01 value for this AUTINFO_KGIB.
     * 
     * @return TOCORP01
     */
    public java.lang.String getTOCORP01() {
        return TOCORP01;
    }


    /**
     * Sets the TOCORP01 value for this AUTINFO_KGIB.
     * 
     * @param TOCORP01
     */
    public void setTOCORP01(java.lang.String TOCORP01) {
        this.TOCORP01 = TOCORP01;
    }


    /**
     * Gets the TOCORP13 value for this AUTINFO_KGIB.
     * 
     * @return TOCORP13
     */
    public java.lang.String getTOCORP13() {
        return TOCORP13;
    }


    /**
     * Sets the TOCORP13 value for this AUTINFO_KGIB.
     * 
     * @param TOCORP13
     */
    public void setTOCORP13(java.lang.String TOCORP13) {
        this.TOCORP13 = TOCORP13;
    }


    /**
     * Gets the TOCORP22 value for this AUTINFO_KGIB.
     * 
     * @return TOCORP22
     */
    public java.lang.String getTOCORP22() {
        return TOCORP22;
    }


    /**
     * Sets the TOCORP22 value for this AUTINFO_KGIB.
     * 
     * @param TOCORP22
     */
    public void setTOCORP22(java.lang.String TOCORP22) {
        this.TOCORP22 = TOCORP22;
    }


    /**
     * Gets the TOCORP23 value for this AUTINFO_KGIB.
     * 
     * @return TOCORP23
     */
    public java.lang.String getTOCORP23() {
        return TOCORP23;
    }


    /**
     * Sets the TOCORP23 value for this AUTINFO_KGIB.
     * 
     * @param TOCORP23
     */
    public void setTOCORP23(java.lang.String TOCORP23) {
        this.TOCORP23 = TOCORP23;
    }


    /**
     * Gets the TOCORP24 value for this AUTINFO_KGIB.
     * 
     * @return TOCORP24
     */
    public java.lang.String getTOCORP24() {
        return TOCORP24;
    }


    /**
     * Sets the TOCORP24 value for this AUTINFO_KGIB.
     * 
     * @param TOCORP24
     */
    public void setTOCORP24(java.lang.String TOCORP24) {
        this.TOCORP24 = TOCORP24;
    }


    /**
     * Gets the TOCORP25 value for this AUTINFO_KGIB.
     * 
     * @return TOCORP25
     */
    public java.lang.String getTOCORP25() {
        return TOCORP25;
    }


    /**
     * Sets the TOCORP25 value for this AUTINFO_KGIB.
     * 
     * @param TOCORP25
     */
    public void setTOCORP25(java.lang.String TOCORP25) {
        this.TOCORP25 = TOCORP25;
    }


    /**
     * Gets the TOCORP26 value for this AUTINFO_KGIB.
     * 
     * @return TOCORP26
     */
    public java.lang.String getTOCORP26() {
        return TOCORP26;
    }


    /**
     * Sets the TOCORP26 value for this AUTINFO_KGIB.
     * 
     * @param TOCORP26
     */
    public void setTOCORP26(java.lang.String TOCORP26) {
        this.TOCORP26 = TOCORP26;
    }


    /**
     * Gets the TOCORP27 value for this AUTINFO_KGIB.
     * 
     * @return TOCORP27
     */
    public java.lang.String getTOCORP27() {
        return TOCORP27;
    }


    /**
     * Sets the TOCORP27 value for this AUTINFO_KGIB.
     * 
     * @param TOCORP27
     */
    public void setTOCORP27(java.lang.String TOCORP27) {
        this.TOCORP27 = TOCORP27;
    }


    /**
     * Gets the TOCORP29 value for this AUTINFO_KGIB.
     * 
     * @return TOCORP29
     */
    public java.lang.String getTOCORP29() {
        return TOCORP29;
    }


    /**
     * Sets the TOCORP29 value for this AUTINFO_KGIB.
     * 
     * @param TOCORP29
     */
    public void setTOCORP29(java.lang.String TOCORP29) {
        this.TOCORP29 = TOCORP29;
    }


    /**
     * Gets the FROMIP value for this AUTINFO_KGIB.
     * 
     * @return FROMIP
     */
    public java.lang.String getFROMIP() {
        return FROMIP;
    }


    /**
     * Sets the FROMIP value for this AUTINFO_KGIB.
     * 
     * @param FROMIP
     */
    public void setFROMIP(java.lang.String FROMIP) {
        this.FROMIP = FROMIP;
    }


    /**
     * Gets the VERNO value for this AUTINFO_KGIB.
     * 
     * @return VERNO
     */
    public java.lang.String getVERNO() {
        return VERNO;
    }


    /**
     * Sets the VERNO value for this AUTINFO_KGIB.
     * 
     * @param VERNO
     */
    public void setVERNO(java.lang.String VERNO) {
        this.VERNO = VERNO;
    }


    /**
     * Gets the LASTMODIFIED value for this AUTINFO_KGIB.
     * 
     * @return LASTMODIFIED
     */
    public java.lang.String getLASTMODIFIED() {
        return LASTMODIFIED;
    }


    /**
     * Sets the LASTMODIFIED value for this AUTINFO_KGIB.
     * 
     * @param LASTMODIFIED
     */
    public void setLASTMODIFIED(java.lang.String LASTMODIFIED) {
        this.LASTMODIFIED = LASTMODIFIED;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AUTINFO_KGIB)) return false;
        AUTINFO_KGIB other = (AUTINFO_KGIB) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CHECKFLAG==null && other.getCHECKFLAG()==null) || 
             (this.CHECKFLAG!=null &&
              this.CHECKFLAG.equals(other.getCHECKFLAG()))) &&
            ((this.ID==null && other.getID()==null) || 
             (this.ID!=null &&
              this.ID.equals(other.getID()))) &&
            ((this.CUSTOMERNAME==null && other.getCUSTOMERNAME()==null) || 
             (this.CUSTOMERNAME!=null &&
              this.CUSTOMERNAME.equals(other.getCUSTOMERNAME()))) &&
            ((this.a_ITEM01==null && other.getA_ITEM01()==null) || 
             (this.a_ITEM01!=null &&
              this.a_ITEM01.equals(other.getA_ITEM01()))) &&
            ((this.AUTHORIZETOALLCORP==null && other.getAUTHORIZETOALLCORP()==null) || 
             (this.AUTHORIZETOALLCORP!=null &&
              this.AUTHORIZETOALLCORP.equals(other.getAUTHORIZETOALLCORP()))) &&
            ((this.TOCORP01==null && other.getTOCORP01()==null) || 
             (this.TOCORP01!=null &&
              this.TOCORP01.equals(other.getTOCORP01()))) &&
            ((this.TOCORP13==null && other.getTOCORP13()==null) || 
             (this.TOCORP13!=null &&
              this.TOCORP13.equals(other.getTOCORP13()))) &&
            ((this.TOCORP22==null && other.getTOCORP22()==null) || 
             (this.TOCORP22!=null &&
              this.TOCORP22.equals(other.getTOCORP22()))) &&
            ((this.TOCORP23==null && other.getTOCORP23()==null) || 
             (this.TOCORP23!=null &&
              this.TOCORP23.equals(other.getTOCORP23()))) &&
            ((this.TOCORP24==null && other.getTOCORP24()==null) || 
             (this.TOCORP24!=null &&
              this.TOCORP24.equals(other.getTOCORP24()))) &&
            ((this.TOCORP25==null && other.getTOCORP25()==null) || 
             (this.TOCORP25!=null &&
              this.TOCORP25.equals(other.getTOCORP25()))) &&
            ((this.TOCORP26==null && other.getTOCORP26()==null) || 
             (this.TOCORP26!=null &&
              this.TOCORP26.equals(other.getTOCORP26()))) &&
            ((this.TOCORP27==null && other.getTOCORP27()==null) || 
             (this.TOCORP27!=null &&
              this.TOCORP27.equals(other.getTOCORP27()))) &&
            ((this.TOCORP29==null && other.getTOCORP29()==null) || 
             (this.TOCORP29!=null &&
              this.TOCORP29.equals(other.getTOCORP29()))) &&
            ((this.FROMIP==null && other.getFROMIP()==null) || 
             (this.FROMIP!=null &&
              this.FROMIP.equals(other.getFROMIP()))) &&
            ((this.VERNO==null && other.getVERNO()==null) || 
             (this.VERNO!=null &&
              this.VERNO.equals(other.getVERNO()))) &&
            ((this.LASTMODIFIED==null && other.getLASTMODIFIED()==null) || 
             (this.LASTMODIFIED!=null &&
              this.LASTMODIFIED.equals(other.getLASTMODIFIED())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCHECKFLAG() != null) {
            _hashCode += getCHECKFLAG().hashCode();
        }
        if (getID() != null) {
            _hashCode += getID().hashCode();
        }
        if (getCUSTOMERNAME() != null) {
            _hashCode += getCUSTOMERNAME().hashCode();
        }
        if (getA_ITEM01() != null) {
            _hashCode += getA_ITEM01().hashCode();
        }
        if (getAUTHORIZETOALLCORP() != null) {
            _hashCode += getAUTHORIZETOALLCORP().hashCode();
        }
        if (getTOCORP01() != null) {
            _hashCode += getTOCORP01().hashCode();
        }
        if (getTOCORP13() != null) {
            _hashCode += getTOCORP13().hashCode();
        }
        if (getTOCORP22() != null) {
            _hashCode += getTOCORP22().hashCode();
        }
        if (getTOCORP23() != null) {
            _hashCode += getTOCORP23().hashCode();
        }
        if (getTOCORP24() != null) {
            _hashCode += getTOCORP24().hashCode();
        }
        if (getTOCORP25() != null) {
            _hashCode += getTOCORP25().hashCode();
        }
        if (getTOCORP26() != null) {
            _hashCode += getTOCORP26().hashCode();
        }
        if (getTOCORP27() != null) {
            _hashCode += getTOCORP27().hashCode();
        }
        if (getTOCORP29() != null) {
            _hashCode += getTOCORP29().hashCode();
        }
        if (getFROMIP() != null) {
            _hashCode += getFROMIP().hashCode();
        }
        if (getVERNO() != null) {
            _hashCode += getVERNO().hashCode();
        }
        if (getLASTMODIFIED() != null) {
            _hashCode += getLASTMODIFIED().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AUTINFO_KGIB.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "AUTINFO_KGIB"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CHECKFLAG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CHECKFLAG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMERNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CUSTOMERNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("a_ITEM01");
        elemField.setXmlName(new javax.xml.namespace.QName("", "A_ITEM01"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AUTHORIZETOALLCORP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AUTHORIZETOALLCORP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOCORP01");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TOCORP01"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOCORP13");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TOCORP13"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOCORP22");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TOCORP22"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOCORP23");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TOCORP23"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOCORP24");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TOCORP24"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOCORP25");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TOCORP25"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOCORP26");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TOCORP26"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOCORP27");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TOCORP27"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOCORP29");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TOCORP29"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FROMIP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FROMIP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VERNO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VERNO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LASTMODIFIED");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LASTMODIFIED"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
