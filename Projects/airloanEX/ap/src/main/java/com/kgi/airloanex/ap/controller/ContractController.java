package com.kgi.airloanex.ap.controller;

import javax.servlet.http.HttpServletRequest;

import com.kgi.airloanex.ap.service.ApplyService;
import com.kgi.airloanex.ap.service.ContractDocService;
import com.kgi.airloanex.ap.service.OrbitService;
import com.kgi.eopend3.ap.controller.base.BaseController;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.dto.KGIHeader;
import com.kgi.eopend3.common.dto.WebResult;

import org.owasp.esapi.ESAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
// import com.kgi.airloanex.ap.dao.ContractMainDao;
// import com.kgi.airloanex.common.PlContractConst;
// import com.google.gson.Gson;
// import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDto;
// import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoConstData;
// import com.kgi.airloanex.common.dto.db.ContractMain;
// import com.kgi.airloanex.common.dto.db.QR_ChannelDepartList;
// import com.kgi.eopend3.common.dto.view.ContractHtmlView;
// import com.kgi.eopend3.common.util.DateUtil;
// import com.kgi.airloanex.ap.dao.QR_ChannelDepartListDao;
// import com.kgi.airloanex.ap.service.KgiService;
// import com.kgi.airloanex.ap.service.PdfService;

/**
 * <ol>
 *  <li>1. 立約流程 - 預覽契約內容(preview Html)</li>
 *  <li>2. 立約流程 - 產製 pdf xml image 上傳至 ftp</li>
 *  <li>3. 申請流程 - 產製申請書html</li>
 *  <li>4. 申請流程 - 產製 pdf xml image 上傳至 ftp</li>
 * </ol>
 */
@RestController
@RequestMapping("/contract")
public class ContractController extends BaseController {
    @SuppressWarnings("unused")
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static String context = "ContractController";

    @Autowired
    private ContractDocService contractDocService;

    @Autowired
    private ApplyService applyService;
    // @Autowired
    // private PdfService pdfService;

    // @Autowired
    // private KgiService kgiService;

    // @Autowired
    // private QR_ChannelDepartListDao qr_ChannelDepartListDao;
    
    // @Autowired
    // private ContractMainDao contractMainDao;


    @Autowired
    private OrbitService orbitService;
    
    /**
     * 1. 立約流程 - 預覽契約內容(preview Html)
     * 
     * @param request
     * @param reqBody
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/previewContract")
	public String previewContract(
        HttpServletRequest request, @RequestBody String reqBody
        // String UniqId, String name, String readDate, String notify, String court,
        // String idno, String productId, String datause, String ipAddress, String verNo
            ) throws Exception {
        
        String previewcontract = "";
        try {
            KGIHeader header = this.getHeader(request);
            String uniqId = header.getUniqId();
            if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
                // 更新立約資料(狀態為立約中)
                // contractService.updateContractData(UniqId, idno, name, readDate, notify, court, datause, ipAddress);

                // 更新共銷同意註記
                // autInfoService.updateAutInfo(idno, name, datause, ipAddress, verNo);

                // 預覽用契約書套版
                // String previewcontract = contractDocService.createContractHtml(UniqId, idno, ipAddress, name);
                previewcontract = contractDocService.generatePreviewContractFinaleHtml(uniqId);
                // 儲存預覽用契約書到DB
                contractDocService.saveContractDoc(uniqId, previewcontract);
                // 產製 pdf
                /*ContractHtmlView view = new ContractHtmlView();
                ContractMain contractMain = contractMainDao.ReadByUniqId(uniqId);
                GetDgtConstRspDto apsConstData = new Gson().fromJson(contractMain.getApsConstData(), GetDgtConstRspDto.class);
                GetDgtConstRspDtoConstDa
                ta constData = apsConstData.findOneConstData();
                
                view.setFILE_NAME(uniqId + "_" + DateUtil.GetDateFormatString("HHmmss") + ".pdf");
                view.setFILE_PATH("C:/mapower/_temp"); // FIXME
                view.setFILE_PW(null); // FIXME
                view.setIMG_CONVERT("Y");

                QR_ChannelDepartList keyItem = new QR_ChannelDepartList();
                keyItem.setChannelId("KB");
                logger.info("branch_id=" + constData.getBranch_id());
                keyItem.setDepartId(constData.getBranch_id().substring(0, 3));
                QR_ChannelDepartList qr = qr_ChannelDepartListDao.likeDepartId(keyItem);
                view.setBRANCH_ID(qr.getAssignDepart());
                view.setEMAIL(contractMain.getEmail());
                view.setCUSTOMER_ID(contractMain.getIdno());
                String productType = "PL";
                if ("1".equals(contractMain.getProductId())) {
                    productType = "PL";
                } else if ("3".equals(contractMain.getProductId())) {
                    productType = "RPL";
                } else if ("2".equals(contractMain.getProductId())) {
                    productType = "GM";
                }
                view.setPRODUCT_TYPE(productType);
                view.setAPS_SEQ_NO(constData.getAPSSeqNo());
                view.setHTML_CONTENT(previewcontract);
                pdfService.htmlToPdf(uniqId, view);
                // // 產生契約書PDF寄送EMAIL給客戶並儲存到DB
    			// contractDocService.generateContractPDF(uniqId);
                // // 更新立約狀態為立約完成
                // contractMainDao.updateContractStatus(uniqId, PlContractConst.CASE_STATUS_WRITE_SUCCESS);
                // // xml
                // orbitService.uploadContractDataToOrbit();*/

                // 產生契約書PDF寄送EMAIL給客戶並儲存到DB
                contractDocService.generateContractFinalePDF(uniqId);
                // 更新立約狀態為立約完成
                // contractMainDao.updateContractStatus(uniqId, PlContractConst.CASE_STATUS_WRITE_SUCCESS);
                
            }
        } catch (ErrorResultException e) {
            return WebResult.GetFailResult();
        }
        return WebResult.GetResultString(0, "成功", previewcontract);
    } // end previewContract

    /**
     * 2. 立約流程 - 產製 pdf xml image 上傳至 ftp
     * 
     * @param request
     * @param reqBody
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/uploadContract")
	public String uploadContract(HttpServletRequest request, @RequestBody String reqBody) throws Exception {
        try {
            // 4 處理線上立約資料打包後送
            orbitService.uploadContractDataToOrbit();
        } catch (ErrorResultException e) {
            return WebResult.GetFailResult();
        }
        return WebResult.GetResultString(0, "成功", null);
    } // end uploadContract
    
    // /**
    //  * 3. 申請流程 - 產製申請書html
    //  * 
    //  * @param request
    //  * @param reqBody
    //  * @return
    //  * @throws Exception
    //  */
    // @PostMapping(value = "/previewApplyLoan")
    // public String previewApplyLoan(HttpServletRequest request, @RequestBody String reqBody) throws Exception {
    //     try {
    //         KGIHeader header = this.getHeader(request);
    //         String uniqId = header.getUniqId();
    //         pdfService.pdfGeneratorActViewAndSendEmail(uniqId);
    //     } catch (ErrorResultException e) {
    //         return WebResult.GetFailResult();
    //     }
    //     return WebResult.GetResultString(0, "成功", null);
    // } // end previewApplyLoan

    @PostMapping(value = "/previewApplyCreditCard")
    public String previewApplyCreditCard(HttpServletRequest request, @RequestBody String reqBody) throws Exception {
        try {
            KGIHeader header = this.getHeader(request);
            String uniqId = header.getUniqId();
            applyService.previewApplyCreditCard(uniqId);
        } catch (ErrorResultException e) {
            return WebResult.GetFailResult();
        }
        return WebResult.GetResultString(0, "成功", null);
    } // end previewApplyCreditCard

    /**
     * 4. 申請流程 - 產製 pdf xml image 上傳至 ftp
     * 
     * @param request
     * @param reqBody
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/uploadApplyLoan")
    public String uploadApplyLoan(HttpServletRequest request, @RequestBody String reqBody) throws Exception {
        try {
            // 處理線上申請資料打包後送
            orbitService.uploadCaseDataToOrbit();
        } catch (ErrorResultException e) {
            return WebResult.GetFailResult();
        }
        return WebResult.GetResultString(0, "成功", null);
    } // end uploadApplyLoan
}