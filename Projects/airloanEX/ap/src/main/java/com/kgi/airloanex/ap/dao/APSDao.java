package com.kgi.airloanex.ap.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.kgi.eopend3.ap.dao.BaseDao;

import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

@Repository
public class APSDao extends BaseDao {
	
	private static final Logger logger = LogManager.getLogger(APSDao.class.getName());
	
	public String queryAPI(String ap, String linkName, String queryJson) throws IOException{
		//System.out.println("APS DAO - queryJson="+queryJson);
		//1 使用HTTP PATCH進行串接
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPatch httpPatch = new HttpPatch(ap.concat(linkName));
		StringEntity entity = new StringEntity(queryJson,"UTF-8");
		
		httpPatch.addHeader("Charset", "UTF-8");  
		httpPatch.addHeader("Accept-Charset", "UTF-8");    
		httpPatch.addHeader("Content-Type", "application/json");
		httpPatch.addHeader("Accept", "application/json");
		
		httpPatch.setEntity(entity);
		CloseableHttpResponse response = client.execute(httpPatch);
		//2. 檢查是否收到正常回傳,回傳資訊。
		StatusLine sl = response.getStatusLine();
        if(sl.getStatusCode() == 200) {
        	StringBuilder sb = new StringBuilder();  
    		BufferedReader br = new BufferedReader(
    				new InputStreamReader(response.getEntity().getContent(),"utf-8"));
    			String line = null;  
    		    while ((line = br.readLine()) != null) {  
    		        sb.append(line);  
    		    }
    		br.close();

    		//[2019.08.21 GaryLiu] ==== START : 移除Encode.forHtml ====
    		//[2019.08.26 GaeyLiu] ==== START : 增加ESAPI驗證 ====
    		try {
    			//[2019.08.28 GaryLiu] ==== START : 修改ESAPI ====
    			String result = sb.toString();
    			if(ESAPI.validator().isValidInput("", result, "Space", Integer.MAX_VALUE, false)) {
    				return result;
    			} else {
    				return "";
    			}
    			//[2019.08.28 GaryLiu] ====  END  : 修改ESAPI ====
        	} catch (IntrusionException e) {
            	logger.info("Exception ESAPI : ", e);
            	return "";
            } catch (Exception e) {
            	logger.info("Exception : ", e);
            	return "";
            }
    		//[2019.08.26 GaeyLiu] ====  END  : 增加ESAPI驗證 ====
    		//[2019.08.21 GaryLiu] ====  END  : 移除Encode.forHtml ====
        }else {
        	System.out.println("呼叫APS失敗!");
        	throw new RuntimeException("呼叫APS Server失敗!!");
        }
		
	}
	
	
	public String queryAPIPost(String ap, String linkName, String queryJson) throws IOException{
		//System.out.println("APS DAO - queryJson="+queryJson);
		//1 使用HTTP PATCH進行串接
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(ap.concat(linkName));
		//HttpPatch httpPatch = new HttpPatch(ap.concat(linkName));
		StringEntity entity = new StringEntity(queryJson,"UTF-8");
		
		httpPost.addHeader("Charset", "UTF-8");  
		httpPost.addHeader("Accept-Charset", "UTF-8");    
		httpPost.addHeader("Content-Type", "application/json");
		httpPost.addHeader("Accept", "application/json");
		
		httpPost.setEntity(entity);
		CloseableHttpResponse response = client.execute(httpPost);
		//2. 檢查是否收到正常回傳,回傳資訊。
		StatusLine sl = response.getStatusLine();
        if(sl.getStatusCode() == 200) {
        	StringBuilder sb = new StringBuilder();  
    		BufferedReader br = new BufferedReader(
    				new InputStreamReader(response.getEntity().getContent(),"utf-8"));
    			String line = null;  
    		    while ((line = br.readLine()) != null) {  
    		        sb.append(line);  
    		    }
    		br.close();

    		return sb.toString();
        }else {
        	System.out.println("呼叫APS失敗!");
        	throw new RuntimeException("呼叫APS Server失敗!!");
        }
		
	}

	@Async("threadPoolTaskExecutor")
	public void insertApiLog(String apiName, String request, String response,String startTime,String endTime)  {
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		
		ArrayList<Object> paramList = new ArrayList<Object>();
			//建立寫入SQL

		String sql =  "INSERT INTO ApiLog"
					+ "(ApiName, Request, Response, StartTime, EndTime, UTime) VALUES"
					+ "(?,?,?,?,?,GETDATE())";

		paramList.add(apiName);
		paramList.add(request);
		paramList.add(response);
		paramList.add(startTime);
		paramList.add(endTime);
		try {
			jdbcTemplate.update(sql, paramList.toArray());
		}catch (DataAccessException e)
		{
//		    e.printStackTrace();
		}
		System.out.println("*****************APILOG INSERT END**************");
	}
}
