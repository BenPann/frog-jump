package com.kgi.airloanex.ap.dao;

import java.util.List;
import java.util.Map;

import com.kgi.airloanex.common.dto.db.AuditData;
import com.kgi.eopend3.ap.dao.BaseDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class AuditDataDao extends BaseDao { 

	public static final String NAME = "AuditData" ;

	public void createAuditData(String uniqId, String uniqType, String projectCode, String creditLine,
			String intrestRate, String rank, String score, String showScore) throws Exception {
		JdbcTemplate jdbctemp = this.getJdbcTemplate();
		jdbctemp.update("DELETE AuditData WHERE UniqId = ?", new Object[] { uniqId });
		jdbctemp.update(
				"INSERT INTO AuditData (UniqId, UniqType, ProjectCode, CreditLine, IntrestRate, Rank, Score, ShowScore ,UTime) VALUES(?,?,?,?,?,?,?,?,GETDATE());",
				new Object[] { uniqId, uniqType, projectCode, creditLine, intrestRate, rank, score, showScore });
	}

	public List<Map<String, Object>> getAuditData(String uniqId) {
		JdbcTemplate jdbctemp = this.getJdbcTemplate();
		String sql = "SELECT ProjectCode, CreditLine ,IntrestRate ,Rank, Score, ShowScore FROM AuditData WHERE UniqId = ? AND 1=1;";
		return jdbctemp.queryForList(sql, new Object[] { uniqId });
	}

	public AuditData findByUniqId(String uniqId) {
        String sql = "SELECT * FROM AuditData WHERE UniqId = ? ";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(AuditData.class), new Object[] { uniqId });
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料");           
            return null;
        }
    }
}
