package com.kgi.airloanex.ap.dao;

import java.util.ArrayList;
import java.util.List;

import com.kgi.airloanex.common.dto.db.ContractMainActInfo;
import com.kgi.eopend3.ap.dao.CRUDQDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class ContractMainActInfoDao extends CRUDQDao<ContractMainActInfo> {
    public static final String NAME = "ContractMainActInfo";

    @Override
    public int Create(ContractMainActInfo fullItem) {
        String sql = "INSERT INTO " + NAME
                + " (UniqId, AuthPhone, AuthBankId, AuthAccountNo, AuthStatus, AuthErrCount, AuthTime, SignPhone, SignBankId, SignAccountNo, SignBranchId, SignStatus, SignErrCount, SignTime, EddaBankId, EddaAccountNo, EddaStatus, EddaErrCount, EddaTime, CreateTime, UpdateTime"
                + " ) "
                + " VALUES (:UniqId, :AuthPhone, :AuthBankId, :AuthAccountNo, :AuthStatus, :AuthErrCount, :AuthTime, :SignPhone, :SignBankId, :SignAccountNo, :SignBranchId, :SignStatus, :SignErrCount, :SignTime, :EddaBankId, :EddaAccountNo, :EddaStatus, :EddaErrCount, :EddaTime, getdate(), getdate() )";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public ContractMainActInfo Read(ContractMainActInfo keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE UniqId = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(ContractMainActInfo.class),
                    new Object[] { keyItem.getUniqId() });
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料");
            throw new ErrorResultException(9, NAME + "查無資料", ex);
        }
    }

    public ContractMainActInfo ReadByUniqId(String uniqId) {
        ContractMainActInfo contractMain = new ContractMainActInfo(uniqId);
        return this.Read(contractMain);
    }

    @Override
    public int Update(ContractMainActInfo fullItem) {
        String sql = "UPDATE " + NAME
                + " SET AuthPhone = :AuthPhone, AuthBankId = :AuthBankId, AuthBankName = :AuthBankName, AuthAccountNo = :AuthAccountNo, AuthStatus = :AuthStatus, AuthErrCount = :AuthErrCount, AuthTime = :AuthTime, SignPhone = :SignPhone, SignBankId = :SignBankId, SignBankName = :SignBankName, SignAccountNo = :SignAccountNo, SignBranchId = :SignBranchId, SignStatus = :SignStatus, SignErrCount = :SignErrCount, SignTime = :SignTime, EddaBankId = :EddaBankId, EddaBankName = :EddaBankName, EddaAccountNo = :EddaAccountNo, EddaStatus = :EddaStatus, EddaErrCount = :EddaErrCount, EddaTime = :EddaTime,  UpdateTime= getdate() "
                + " WHERE UniqId=:UniqId ";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(ContractMainActInfo keyItem) {
        String sql = "DELETE FROM " + NAME + " WHERE UniqId = ?";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getUniqId() });
    }

    @Override
    public List<ContractMainActInfo> Query(ContractMainActInfo keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE AuthStatus = ? or SignStatus = ? or EddaStatus = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getAuthStatus(), keyItem.getSignStatus(), keyItem.getEddaStatus() },
                    new BeanPropertyRowMapper<>(ContractMainActInfo.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }

    public ContractMainActInfo touch(ContractMainActInfo keyItem) {

        // uniqId must not be blank
        if (keyItem == null || StringUtils.isBlank(keyItem.getUniqId())) {
            return null;
        }

        // find list
        List<ContractMainActInfo> list = new ArrayList<>();
        String sql = "SELECT * FROM " + NAME + " WHERE UniqId = ?";
        try {
            list = this.getJdbcTemplate().query(sql, new Object[] { keyItem.getUniqId() },
                    new BeanPropertyRowMapper<>(ContractMainActInfo.class));
        } catch (DataAccessException ex) {
            // ignore;
        }

        // create one if not found
        if (list.size() == 0) {
            keyItem.setAuthErrCount(0);
            keyItem.setSignErrCount(0);
            keyItem.setEddaErrCount(0);
            Create(keyItem);
            return Read(keyItem);
        } 
        // found one
        else if (list.size() == 1) {
            return list.get(0);
        } 
        // unexpected
        else {
            return null;
        }
    }
}