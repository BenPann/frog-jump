package com.kgi.airloanex.ap.service;

import com.kgi.airloanex.ap.dao.PlexpDao;
import com.kgi.airloanex.common.dto.db.Plexp;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlexpService {
    @Autowired
    private PlexpDao plexpdao;

    public Plexp findByCaseNo(String caseNo) {
        if (StringUtils.isNotBlank(caseNo)) {
            return plexpdao.findByCaseNo(caseNo);
        } else {
            return null;
        }
        
    }

    public int insert(Plexp fullItem) {
        return plexpdao.Create(fullItem);
    }

    public void updatePlexpCaseNoWeb(String expCaseNo, String uniqId) {
        Plexp plexp = plexpdao.findByCaseNo(expCaseNo);
        if (plexp != null) {
            plexp.setCaseNoWeb(uniqId);
            plexpdao.Update(plexp);
        }
    }

    /**
     * ExpProductType=PL/RPL
     * 
     * Default ExpProductType=PL if not available.
     * 
     * @param caseNo
     * @return
     */
    public String findExpProductTypeByCaseNo(String caseNo) {
        Plexp plexp = plexpdao.findByCaseNo(caseNo);
        if (plexp != null) {
            return plexp.getExpProductType();
        } else {
            return "PL";
        }
    }

    public void updatePlexpPhone(String expCaseNo, String phone){
        plexpdao.UpdateByCaseNoPhone(expCaseNo,phone);
    }
}
