package com.kgi.airloanex.ap.pdf;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPageEvent;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.HTML;
import com.itextpdf.tool.xml.html.TagProcessorFactory;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.rendering.PDFRenderer;

/** 
 * 產PDF文件輔助類別(Charles:相當於通用工廠類別(Factory Class))
 * <p>各邏輯分別放置於 BasePDFDocument 子類別，例如：ContractPDF.java, CasePDF.java</p>
 * PDFHelper 主邏輯放置為 PdfProcess()
 * <ol>
 * <li>this.pdfDocument.beforePdfProcess() 預先載入資料及樣板</li>
 * <li>generatePDF(...) 產生PDF</li>
 * <li>addWaterMark(...) 壓浮水印</li>
 * <li>this.pdfDocument.pdfToImage(...) 將PDF轉圖檔</li>
 * <li>encryptPDF(...) 將PDF加密</li>
 * <li>this.pdfDocument.saveToDB(...) 儲存至DB</li>
 * <li>this.pdfDocument.sendEmail(...) 發送Mail</li>
 * <li>this.pdfDocument.afterPdfProcess() 後續處理</li>
 * </ol>
 */
public class PDFHelper {
    private BasePDFDocument pdfDocument;

    public PDFHelper() {

    }

    public PDFHelper(BasePDFDocument doc) {
        this.pdfDocument = doc;
    }

    public BasePDFDocument getPDFDocument() {
        return this.pdfDocument;
    }

    public void setPDFDocument(BasePDFDocument doc) {
        this.pdfDocument = doc;
    }

    /**
     * 如果有特殊需求 將加密的byte傳進來這邊作解密PDF的動作
     * 
     * @param 加密的PDF資料
     * @param 解密密碼
     */
    public byte[] DecryptPDF(byte[] encryptPDFData, String encryptPassword) throws IOException, DocumentException {

        ByteArrayOutputStream outputStream = null;
        PdfReader reader = null;
        PdfStamper stamper = null;
        try {
            outputStream = new ByteArrayOutputStream();
            reader = new PdfReader(encryptPDFData, encryptPassword.getBytes());
            stamper = new PdfStamper(reader, outputStream);
            outputStream.flush();
        } finally {
            if (stamper != null)
                stamper.close();
            if (reader != null)
                reader.close();
            if (outputStream != null)
                outputStream.close();
        }
        return outputStream.toByteArray();
    }

    /**
     * 如果有特殊需求 將加密的byte傳進來這邊作解密PDF的動作
     * 
     * @param 加密的PDFBase64字串
     * @param 解密密碼
     */
    public byte[] DecryptPDF(String encryptPDFBase64Data, String encryptPassword)
            throws IOException, DocumentException {
        return DecryptPDF(java.util.Base64.getDecoder().decode(encryptPDFBase64Data), encryptPassword);
    }

    /**
     * 取得PDF的總頁數
     * 
     * @throws IOException
     * @throws InvalidPasswordException
     */
    public int GetDocumentPageCount(byte[] pdfData) throws InvalidPasswordException, IOException{
        PDDocument document = null;
        try {
            document = PDDocument.load(pdfData);
            return document.getNumberOfPages();
        } finally {
            if (document != null)
                document.close();
        }

    }

    /** 匯出轉換過的Html */
    public String ExportHtml() throws Exception {
        if (this.pdfDocument == null)
            throw new IOException("尚未傳入報表類別");
        // 取得Html
        String raw = this.pdfDocument.getRawHtml();
        if (raw == null || raw.trim().equals(""))
            throw new IOException("尚未傳入Html");
        System.out.println("### rawHtml.length()=" + raw.length());
        // Replace
        return this.pdfDocument.replaceField(raw.trim());
    }

    /**
     * 此方法會將傳入類別的的資料 轉成pdf 以及所設定的相關動作
     * 
     * @throws org.ghost4j.document.DocumentException
     * @return PDF的byte陣列
     * @throws Exception
     */
    public byte[] PdfProcess() throws Exception {
        if (this.pdfDocument == null)
            throw new IOException("尚未傳入報表類別");
        ByteArrayOutputStream pdfStream = null;
        try {
            this.pdfDocument.beforePdfProcess();
            // 產生暫存檔
            pdfStream = new ByteArrayOutputStream();
            // 產生PDF generatePDF
            generatePDF(pdfStream, this.ExportHtml(), this.pdfDocument.getPageEvent());

            // 壓浮水印 addWaterMark
            if (this.pdfDocument.getUseWaterMark()) {
                addWaterMark(pdfStream, this.pdfDocument.getWaterMarkText(), this.pdfDocument.getWaterMarkFont());
            }

            // 將PDF轉圖檔(如果有需要 加密前先做轉圖片) pdfToImage
            if (this.pdfDocument.getPdfToImage()) {
                this.pdfDocument.pdfToImage(this.getImageList(pdfStream.toByteArray()));
            }

            // 將PDF加密 encryptPDF
            if (this.pdfDocument.getUseEncrepyt()) {
                encryptPDF(pdfStream, this.pdfDocument.getEncryptPassword());
            }

            // 儲存至DB saveToDB
            if (this.pdfDocument.getUseSaveToDB()) {
                this.pdfDocument.saveToDB(pdfStream.toByteArray());
            }

            // 發送Mail sendEmail
            if (this.pdfDocument.getUseSendEMail()) {
                this.pdfDocument.sendEmail(pdfStream.toByteArray());
            }

            this.pdfDocument.afterPdfProcess();

        } finally {
            if (pdfStream != null)
                pdfStream.close();
        }

        return pdfStream.toByteArray();

    }

    public void generatePDF(ByteArrayOutputStream pdfFile, String exporthtml, PdfPageEvent event)
            throws DocumentException, UnsupportedEncodingException, IOException {
        Document document = null;
        try {
            // step 1
            document = new Document();
            // step 2
            PdfWriter writer = PdfWriter.getInstance(document, pdfFile);
            if (event != null) {
                writer.setPageEvent(event);
            }
            System.out.println("### exporthtml.length()=" + exporthtml.length());
            // step 3
            document.open();
            // step 4

            // CSS
            CSSResolver cssResolver = XMLWorkerHelper.getInstance().getDefaultCssResolver(true);

            TagProcessorFactory tagProcessorFactory = Tags.getHtmlTagProcessorFactory();
            tagProcessorFactory.removeProcessor(HTML.Tag.IMG);
            tagProcessorFactory.addProcessor(new ImageTagProcessor(), HTML.Tag.IMG);

            // HTML
            HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
            htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
            htmlContext.setImageProvider(new Base64ImageProvider());

            // Pipelines
            PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
            HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
            CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);

            // XML Worker
            XMLWorker worker = new XMLWorker(css, true);
            XMLParser p = new XMLParser(worker);

            // 避免 contract_onlineLoan.html  無法正常產出 的特殊處理
            exporthtml = exporthtml.replaceAll("<br>", "<br/>");
            exporthtml = exporthtml.replaceAll("<BR>", "<br/>");
            exporthtml = exporthtml.replaceAll("/></img>", "></img>");
			
        try {
            p.parse(new ByteArrayInputStream(exporthtml.getBytes("UTF-8")), Charset.forName("UTF-8"));
        } catch (Exception e) {
            System.out.println("### 轉至XML錯誤");
            e.printStackTrace();
        }
            

        } finally {
            if (document != null)
                document.close();
        }

    }

    public void addWaterMark(ByteArrayOutputStream pdfFile, String waterMarkText, String waterMarkFont)
            throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        ByteArrayOutputStream outputStream = null;
        try {
            outputStream = new ByteArrayOutputStream();
            PdfReader pdfReader = new PdfReader(pdfFile.toByteArray());
            PdfStamper pdfStamper = new PdfStamper(pdfReader, outputStream);

            for (int i = 1, pdfPageSize = pdfReader.getNumberOfPages() + 1; i < pdfPageSize; i++) {
                PdfContentByte pageContent = pdfStamper.getOverContent(i);
                BaseFont bfc = BaseFont.createFont(waterMarkFont, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

                // 設定透明度
                PdfGState gstate = new PdfGState();
                gstate.setFillOpacity(0.5f);
                gstate.setStrokeOpacity(1f);
                pageContent.setGState(gstate);
                pageContent.beginText();
                pageContent.setFontAndSize(bfc, 32);
                pageContent.setColorFill(BaseColor.GRAY);
                pageContent.showTextAligned(Element.ALIGN_CENTER, waterMarkText, document.getPageSize().getWidth() / 2,
                        document.getPageSize().getHeight() / 2, -45); // 浮水印角度 45
                pageContent.endText();
            }
            pdfStamper.close();

            pdfFile.reset();
            pdfFile.write(outputStream.toByteArray());
            pdfFile.flush();
        } finally {
            if (outputStream != null)
                outputStream.close();
        }
    }

    public void encryptPDF(ByteArrayOutputStream pdfFile, String encryptPassword)
            throws DocumentException, IOException {
        ByteArrayOutputStream outputStream = null;
        try {
            outputStream = new ByteArrayOutputStream();
            PdfReader reader = new PdfReader(pdfFile.toByteArray());
            PdfStamper stamper = new PdfStamper(reader, outputStream);
            stamper.setEncryption(encryptPassword.getBytes(), encryptPassword.getBytes(),
                    PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_256);
            stamper.close();
            pdfFile.reset();
            pdfFile.write(outputStream.toByteArray());
            pdfFile.flush();
        } finally {
            if (outputStream != null)
                outputStream.close();
        }
    }

    /**
     * 取得PDF轉成圖片的列表
     * 
     * @throws IOException
     */
    public List<byte[]> getImageList(byte[] pdfData) throws IOException {
        List<byte[]> listByte = new ArrayList<byte[]>();
        PDDocument document = null;
        try {
            document = PDDocument.load(pdfData);
            PDFRenderer pdfRenderer = new PDFRenderer(document);
            for (int page = 0; page < document.getNumberOfPages(); ++page) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {
                    BufferedImage bimg = pdfRenderer.renderImageWithDPI(page, 300);
                    ImageIO.write(bimg, "jpg", bos);
                    listByte.add(bos.toByteArray());
                } finally {
                    if (bos != null)
                        bos.close();
                }
            }
        } finally {
            document.close();
        }
        return listByte;
    }
}