package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.ContractMainPdf;
import com.kgi.eopend3.ap.dao.CRUDQDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;

public class ContractMainPdfDao extends CRUDQDao<ContractMainPdf> {
    public static final String NAME = "ContractMainPdf";

    @Override
    public int Create(ContractMainPdf fullItem) {
        String sql = "INSERT INTO " + NAME
                + " (UniqId, Version, PdfContent) "
                + " VALUES (:UniqId, :Version, :PdfContent)";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public ContractMainPdf Read(ContractMainPdf keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE UniqId = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(ContractMainPdf.class),
                    new Object[] { keyItem.getUniqId() });
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料");
            throw new ErrorResultException(9, NAME + "查無資料", ex);
        }
    }

    public ContractMainPdf ReadByUniqId(String uniqId) {
        ContractMainPdf contractMain = new ContractMainPdf(uniqId);
        return this.Read(contractMain);
    }

    @Override
    public int Update(ContractMainPdf fullItem) {
        String sql = "UPDATE " + NAME
                + " SET Version = :Version, PdfContent = :PdfContent "
                + " WHERE UniqId=:UniqId ";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(ContractMainPdf keyItem) {
        String sql = "DELETE FROM " + NAME + " WHERE UniqId = ?";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getUniqId() });
    }

    @Override
    public List<ContractMainPdf> Query(ContractMainPdf keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE UniqId = ? and Version = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getUniqId(), keyItem.getVersion()},
                    new BeanPropertyRowMapper<>(ContractMainPdf.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }
}