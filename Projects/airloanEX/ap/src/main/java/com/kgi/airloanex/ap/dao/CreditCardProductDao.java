package com.kgi.airloanex.ap.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.kgi.airloanex.common.dto.db.CreditCardProduct;
import com.kgi.airloanex.common.dto.response.CreditCardListResp;
import com.kgi.airloanex.common.dto.response.GiftListResp;
import com.kgi.eopend3.ap.dao.CRUDQDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class CreditCardProductDao extends CRUDQDao<CreditCardProduct> {
    
    @Override
    public int Create(CreditCardProduct fullItem) {
        return 0;
    }

    @Override
    public CreditCardProduct Read(CreditCardProduct keyItem) {
        return null;
    }

    @Override
    public int Update(CreditCardProduct fullItem) {
        return 0;
    }

    @Override
    public int Delete(CreditCardProduct keyItem) {
        return 0;
    }

    @Override
    public List<CreditCardProduct> Query(CreditCardProduct keyItem) {
        return null;        
    }

    /*public List<CreditCardListResp> getCreditCardListByProductId(String[] productId) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT CardSerial,CardType,ProductId,CardTitle,CardImage,CardContent from CreditCardProduct where ProductId IN (");
        for (int i = 0; i < productId.length; i++) {
            if (i + 1 == productId.length) {
                sb.append("?");
            } else {
                sb.append("?,");
            }
        }
        sb.append(") ");
        sb.append("AND CardStatus in ('04','05') AND StartDate <= getdate() AND EndDate >= getdate() ORDER BY StartDate");
        List<String> params = new ArrayList<>(Arrays.asList(productId));
        try {
            return this.getJdbcTemplate().query(sb.toString(),params.toArray(),new BeanPropertyRowMapper<>(CreditCardListResp.class));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }*/

    public List<CreditCardListResp> getCreditCardList(String[] productId,List<String> applyedCardType) {
        StringBuilder sb = new StringBuilder();
        List<String> params = new ArrayList<>();
        sb.append("SELECT CardSerial,ProductId,CardTitle,CardImage,CardContent from CreditCardProduct where CardStatus in ('04','05') ");
        sb.append(" AND StartDate <= getdate() AND EndDate >= getdate() ");    
        for (int i = 0; i < applyedCardType.size(); i++) {
            if(i == 0 ){
                sb.append(" AND CardType Not IN ( ?, ");
            }else if(i + 1 == applyedCardType.size()) {
                sb.append("? )");
            } else {
                sb.append("?,");
            }
        }
        if(applyedCardType.size()>0){
            params.addAll(applyedCardType);
        }
        if(productId!=null){
            for (int i = 0; i < productId.length; i++) {
                if(i == 0 ){
                    sb.append(" AND ProductId IN ( ?, ");
                }else if(i + 1 == productId.length) {
                    sb.append("? )");
                } else {
                    sb.append("?,");
                }
            }
            params.addAll(Arrays.asList(productId));
        }       
        sb.append(" ORDER BY StartDate");
        try {
            return this.getJdbcTemplate().query(sb.toString(),params.toArray(),new BeanPropertyRowMapper<>(CreditCardListResp.class));
        } catch (Exception e) {
            e.printStackTrace();
            throw new ErrorResultException(10, "取得信用卡列表失敗", e);
        }
    }

    public List<GiftListResp> findGiftListByUniqId(String uniqId) {
        String sql = "SELECT GiftTitle, GiftCode, GiftImage, GiftNote " +
                "FROM CreditCardGift ccg " +
                "         inner join GiftList gl on gl.GiftSerial = ccg.GiftSerial " +
                "         inner join CreditCardProduct ccp on ccp.CardSerial = gl.CardSerial " +
                "         inner join CreditCaseData ccd on ccd.ProductId = ccp.ProductId " +
                "where " +
                "      ccd.UniqId = ? " +
                "  AND ccp.StartDate <= getdate() " +
                "  AND ccp.EndDate >= getdate() " +
                "  AND CardStatus in ('04', '05') " +
                "  AND GiftStatus = '04'";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { uniqId },
                    new BeanPropertyRowMapper<>(GiftListResp.class));
        } catch (DataAccessException ex) {
            ex.printStackTrace();
            throw new ErrorResultException(9, "取得首刷禮列表失敗", ex);
        }
    }

    public List<GiftListResp> findGiftListByProductId(String productId) {
        String sql = " SELECT GiftTitle, GiftCode, GiftImage, GiftNote " +
        "  FROM CreditCardGift ccg " +
        "        inner join GiftList gl on gl.GiftSerial = ccg.GiftSerial " +
        "        inner join CreditCardProduct ccp on ccp.CardSerial = gl.CardSerial " +
        " WHERE ccp.ProductId = ? " +
        "   AND ccp.StartDate <= getdate() " +
        "   AND ccp.EndDate >= getdate() " +
        "   AND CardStatus in ('04', '05') " +
        "   AND GiftStatus = '04'";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { productId },
                    new BeanPropertyRowMapper<>(GiftListResp.class));
        } catch (DataAccessException ex) {
            ex.printStackTrace();
            throw new ErrorResultException(9, "取得首刷禮列表失敗", ex);
        }
    }
}
