package com.kgi.eopend3.ap.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = { "file:${FUCO_CONFIG_PATH}/DB.properties" }, encoding = "UTF-8")
public class OracleDwstConfig implements IBaseDBConfig {
    @SuppressWarnings("unused")
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${OracleDwstConfig.username}")
    private String username;
    @Value("#{new String(T(com.kgi.eopend3.common.util.crypt.FucoAESUtil).getInstance('${AbbeyRye}','${RayakRoe}').decrypt('${OracleDwstConfig.password}'),'UTF-8')}")
    private String pazzd;
    @Value("${OracleDwstConfig.url}")
    private String url;
    @Value("${OracleDwstConfig.driverclassname}")
    private String driverclassname;
    @Value("${OracleDwstConfig.dbName}")
    private String dbName;

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPazzd() {
        return pazzd;
    }

    @Override
    public void setPazzd(String pazzd) {
        this.pazzd = pazzd;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getDriverClassName() {
        return driverclassname;
    }

    @Override
    public void setDriverClassName(String driverClassName) {
        this.driverclassname = driverClassName;
    }
}