package com.kgi.airloanex.ap.service;

import static com.kgi.airloanex.common.PlContractConst.KB_9743_CHANNEL_ID;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.kgi.airloanex.ap.dao.QR_ChannelListDao;
import com.kgi.airloanex.common.dto.db.QR_ChannelDepartList;
import com.kgi.airloanex.common.dto.db.QR_ChannelList;
import com.kgi.eopend3.ap.dao.EntryDataDao;
import com.kgi.eopend3.common.dto.db.EntryData;
import com.kgi.eopend3.common.util.CommonFunctionUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SmtpContentService {

    @Autowired
    private ChannelDepartService channelDepartService;

    @Autowired
    private EntryDataDao entryDataDao;

    @Autowired
    private QR_ChannelListDao channelListDao;
    
    //2019/1/16 要求統一EMAIL格式，移除其它，只留下唯一格式
    public String getSMTPContent(String uniqId, String uniqType, String serial, String applyDate, String applyTime, String name,
                                 String idno, String gender, String phone, String contacTime, String status,
                                 String prjCode, String product, String promo1, String member1, String promo2, String member2, String email, String note) {
        StringBuilder sb = new StringBuilder();
        String seq = getTimeFormatSequence();
        if (null == serial)
            serial = seq;
        sb.append("<html><body>");
        sb.append("<table border=\"1\">");
        sb.append("<tr><td>拋轉序號</td><td>" + serial + "</td></tr>");
//        sb.append("<tr><td>申請日期</td><td>" + applyDate + "</td></tr>");
        sb.append("<tr><td>案件編號</td><td>" + uniqId + "</td></tr>");
        sb.append("<tr><td>時間</td><td>" + applyTime + "</td></tr>");
        sb.append("<tr><td>姓名</td><td>" + name + "</td></tr>");
        idno = idno != null ? idno : "";
        idno = CommonFunctionUtil.hiddenUserDataWithStar(idno, 4, 3);
        sb.append("<tr><td>身分證字號</td><td>" + idno + "</td></tr>");
        
        //[2020.01.08 GaryLiu] ==== START : 修改性別判斷處理方式 ====
        if(gender != null) {
        	if(gender.equals("1")) {
        		gender = "男";
        	} else if (gender.equals("2")) {
        		gender = "女";
        	}
        } else {
    		gender = "";
    	}
        
        //gender = gender != null ? (gender.equals("1") ? "男" : "女") : "";
        //[2020.01.08 GaryLiu] ====  END  : 修改性別判斷處理方式 ====

        if (idno.length() >= 3) {
            gender = idno.substring(1, 2).equals("1") ? "男" : "女";
        }

        sb.append("<tr><td>性別</td><td>" + gender + "</td></tr>");
        phone = phone != null ? phone : "";
        sb.append("<tr><td>連絡電話</td><td>" + phone + "</td></tr>");
        email = email != null ? email : "";
        sb.append("<tr><td>email</td><td>" + email + "</td></tr>");
//		sb.append("<tr><td>email</td><td>" + email != null ? email : "" + "</td></tr>");
        contacTime = contacTime != null ? contacTime : "";
        sb.append("<tr><td>方便聯絡時間</td><td>" + contacTime + "</td></tr>");
        product = product != null ? product : "";
        sb.append("<tr><td>申請產品別</td><td>" + product + "</td></tr>");

        status = status != null ? status : "";
        sb.append("<tr><td>狀態別</td><td>" + status + "</td></tr>");

        prjCode = prjCode != null ? prjCode : "";
        sb.append("<tr><td>卡片名稱/專案別</td><td>" + prjCode + "</td></tr>");

        //[2020.01.09 GaryLiu] ==== START : 新增[意見或問題]欄位 ====
        note = note != null ? note : "";
        if(note != null) {
        	if(note.length() > 0) {
        		sb.append("<tr><td>意見或問題</td><td>" + note + "</td></tr>");
        	}
        }
        //[2020.01.09 GaryLiu] ====  END  : 新增[意見或問題]欄位 ====
//        creditLine = creditLine > 0 ? creditLine : 0;
//        if (creditLine > 0) {
//            sb.append("<tr><td>申請金額</td><td>" + creditLine + "萬</td></tr>");
//        } else {
//            sb.append("<tr><td>申請金額</td><td>   </td></tr>");
//        }
//        if (intrestRate != null) {
//            sb.append("<tr><td>網頁顯示利率</td><td>" + intrestRate + "%</td></tr>");
//        } else {
//            sb.append("<tr><td>網頁顯示利率</td><td>評分未有結果</td></tr>");
//        }
//        if (paymoney != null) {
//            sb.append("<tr><td>網頁顯示金額</td><td>" + paymoney + "%</td></tr>");
//        } else {
//            sb.append("<tr><td>網頁顯示金額</td><td>評分未有結果</td></tr>");
//        }
//        if (creditLine > 0) {
//            sb.append("<tr><td>貸款期間期間</td><td></td></tr>");
//        }

        //增加推廣欄位

        addPromoCase(sb, uniqId, uniqType, promo1, member1, promo2, member2);


        sb.append("</table>");
        sb.append("</body></html>");

        return sb.toString();

    }

    private String getTimeFormatSequence() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
        Date date = new Date();
        String seq = dateFormat.format(date);
        return seq;
    }

    private void addPromoCase(StringBuilder sb, String uniqId, String uniqType, String promo1, String member1, String promo2, String member2) {

        QR_ChannelDepartList channelDepart = null;
        try {

            if (uniqType.equals("05")) {
                channelDepart = channelDepartService.getCreditChannelDepart(uniqId);
                promo2 = channelDepart.getPromoDepart2();
                member2 = channelDepart.getPromoMember2();
                promo1 = channelDepart.getPromoDepart1();
                member1 = channelDepart.getPromoMember1();
            }else if(uniqType.equals("12")){
                //薪轉申辦信用卡，所以沒有將原邏輯 推廣1>轉介，改成推廣1>推薦人員
                channelDepart = channelDepartService.getCreditChannelDepart(uniqId);
                promo1 = channelDepart.getPromoDepart2();
                member1 = channelDepart.getPromoMember2();
                promo2 = channelDepart.getPromoDepart1();
                member2 = channelDepart.getPromoMember1();
            } else {
                channelDepart = channelDepartService.getLoanChannelDepart(uniqId);
                promo2 = channelDepart.getPromoDepart();
                member2 = channelDepart.getPromoMember();
                promo1 = channelDepart.getTransDepart();
                member1 = channelDepart.getTransMember();
            }


        } catch (NullPointerException ex) {
            // 可能取不到channeldepart資料，會回傳一個NULL 或是可能crash
        }
        String channelName = "";
        if (channelDepart != null && !channelDepart.getChannelId().equals("")) {
            String channelId = channelDepart.getChannelId();
            if (channelId.equals("-")) {
                channelId = KB_9743_CHANNEL_ID;
            }
            QR_ChannelList channelList = channelListDao.Read(new QR_ChannelList(channelId));
            if (channelList != null) {
                channelName = channelList.getChannelName();
            }
        } else {
            EntryData entry = entryDataDao.Read(new EntryData(uniqId));
            String channelId = entry != null && entry.getEntry().length() >= 2 ? entry.getEntry().substring(0, 2) : "";
            QR_ChannelList channelList = channelListDao.Read(new QR_ChannelList(channelId));
            channelName = channelList != null ? channelList.getChannelName() : "";
        }
        sb.append("<tr><td>通路來源</td><td>" + channelName + "</td></tr>");
        sb.append("<tr><td>推薦單位</td><td>" + promo2 + "</td></tr>");
        sb.append("<tr><td>推薦人員</td><td>" + member2 + "</td></tr>");
        sb.append("<tr><td>轉介單位</td><td>" + promo1 + "</td></tr>");
        sb.append("<tr><td>轉介人員</td><td>" + member1 + "</td></tr>");

    }
}
