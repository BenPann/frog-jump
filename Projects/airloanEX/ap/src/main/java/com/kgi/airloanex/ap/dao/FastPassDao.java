package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.FastPass;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class FastPassDao extends CRUDQDao<FastPass> {

    @Override
    public int Create(FastPass fullItem) {
        String sql = "INSERT INTO FastPass (CaseNo, AML, AddPhoto, AMLRequest, AMLResponse, EndTime, DGTType, CreateTime, UpdateTime) values(?,?,?,?,?,?,?,GETDATE(),GETDATE());";
        return this.getJdbcTemplate().update(sql,
                new Object[] { 
                    fullItem.getCaseNo(), 
                    fullItem.getAML(),
                    fullItem.getAddPhoto(), 
                    fullItem.getAMLRequest(), 
                    fullItem.getAMLResponse(), 
                    fullItem.getEndTime(),
                    fullItem.getDGTType() });
    }

    @Override
    public FastPass Read(FastPass keyItem) {
        String sql = "SELECT * FROM FastPass WHERE CaseNo=?;";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(FastPass.class),
                    new Object[] { keyItem.getCaseNo() });
        } catch (DataAccessException ex) {
			System.out.println("FastPass查無資料");            
            return null;
        }

    }

    @Override
    public int Update(FastPass fullItem) {
        String sql = "UPDATE FastPass SET AddPhoto = ?, AML = ?, AMLRequest = ?, AMLResponse = ?, EndTime = ?, DGTType = ?, UpdateTime = GETDATE() WHERE CaseNo = ?";
        return this.getJdbcTemplate().update(sql,
                new Object[] { 
                    fullItem.getAddPhoto(), 
                    fullItem.getAML(), 
                    fullItem.getAMLRequest(),
                    fullItem.getAMLResponse(), 
                    fullItem.getEndTime(), 
                    fullItem.getDGTType(),
                    fullItem.getCaseNo() });
    }

    @Override
    public int Delete(FastPass keyItem) {
        String sql = "DELETE FastPass WHERE CaseNo = ? AND 1=1";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getCaseNo() });
    }

    @Override
    public List<FastPass> Query(FastPass keyItem) {
        String sql = "SELECT * FROM FastPass WHERE AML = ?";
        return this.getJdbcTemplate().query(sql, new BeanPropertyRowMapper<>(FastPass.class),
                new Object[] { keyItem.getAML() });
    }

    public int UpdateDGTType(String caseNo, String dgtType) {
        String sql = "UPDATE FastPass SET DGTType = ?, UpdateTime = GETDATE() WHERE CaseNo = ?";
        return this.getJdbcTemplate().update(sql,
                new Object[] { dgtType, caseNo });
    }
}