package com.kgi.airloanex.ap.dao;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Iterator;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.service.JobLogService;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.ap.dao.HttpDao;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.XMLType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.w3c.dom.NodeList;

@Repository
public class SOAPDao {

    @Autowired
    HttpDao httpDao;

    @Autowired
    APSDao apsDao;

    @Autowired
    JobLogService jobLogService;

    @Autowired
    ConfigDao configDao;

    @Autowired
    AirloanEXConfig globalConfig;
    
    @SuppressWarnings("unused")
    private static final Logger logger = LogManager.getLogger(SOAPDao.class.getName());

    private final static QName ServiceBody_QNAME = new QName("http://tempuri.org/", "SendMessageResponse");

    public String getCMBProgress(String CMBRequestStr) throws Exception {
        try {
			String result = "";

			String esbUrl = globalConfig.CMBSendMsg_SOAP_URL();
//        String rsp = HttpUtil2.doSoapPostRequest(esbUrl, CMBRequestStr);
			String rsp = ESAPI.validator().getValidInput("", httpDao.doSoapPostRequest(esbUrl, CMBRequestStr), "SafeJson", 10000, false);
			result = processResponse(rsp);
			jobLogService.AddSendDataInfoLog("發送訊息平台訊息：".concat(CMBRequestStr).concat("回傳結果:").concat(result));

			return result;
		} catch (Exception e) {
			return "input validation failed";
		}
    }

    public String getCreditCardStatus(String creditNoAps,String idno,boolean isJob) {
        String res = "";
        String startTime = "";
        String endTime = "";
        Service service = new Service();
        String namespace = "http://tempuri.org/";
        String actionUri = "GetDATA";
        String url = configDao.ReadConfigValue("Api.GetCreditCardStatus",
                "http://10.86.18.104:7777/QUERY/WQ/WQ_CASE_DATA.asmx");
        String op = "GetDATA"; // 要調用的方法名
        String reqXML = "";
        creditNoAps = creditNoAps == null ? "" : creditNoAps;
        idno = idno == null ? "" : idno;
        try {
            reqXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DATA><QRY_PARAM><CASE_ID>"+ creditNoAps +"</CASE_ID><P_ID>"
                   +idno+ "</P_ID><RESULT></RESULT><ERR_MSG></ERR_MSG></QRY_PARAM></DATA>";
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(new java.net.URL(url));
            call.setUseSOAPAction(true);
            call.setSOAPActionURI(namespace + actionUri); // action uri
            call.setOperationName(new QName(namespace, op));// 設置要調用哪個方法
            // 設置參數名稱，具體參照從瀏覽器中看到的
            call.addParameter(new QName(namespace, "xml"), XMLType.XSD_STRING, ParameterMode.IN); // 設置請求參數及類型
            call.setReturnType(org.apache.axis.encoding.XMLType.XSD_STRING);// 設置結果返回類型
            Object[] params = new Object[] { reqXML };
            res = (String) call.invoke(params); // 方法執行後的返回值
            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
            // TODO: 拆解回應 並回傳物件的列表
        } catch (Exception e) {
//            e.printStackTrace();
        } finally {
        	String apiname = "GetCreditCardStatus";
        	if(isJob) {
        		apiname += "/Job";
            }
            // 寫ApiLog
            apsDao.insertApiLog(apiname, reqXML, res, startTime, endTime);
        }
        return res;
    }

    private String processResponse(String soapStr) {
        String result = "0,系統發生錯誤。";
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(soapStr.getBytes())) {
            SOAPMessage response = MessageFactory.newInstance(SOAPConstants.DEFAULT_SOAP_PROTOCOL).createMessage(null,
                    byteArrayInputStream);
            SOAPBody soapBody = response.getSOAPBody();

            Iterator<?> itServiceBody = soapBody.getChildElements(ServiceBody_QNAME);
            if (itServiceBody.hasNext()) {
                SOAPElement emtServiceBody = (SOAPBodyElement) itServiceBody.next();
                NodeList nodelist = (NodeList) emtServiceBody.getChildNodes();
                Node node = getNode("SendMessageResult", nodelist);
                if (node != null) {
                    result = node.getValue();
                } else {
                    result = "0,KGI SOAP解析時無值";
                }
                // System.out.println("SOAP STATUS:" + node.getValue()); //1:成功, 0:失敗
            } else {
                result = "0,KGI SOAP no data";
            }

        } catch (SOAPException e) {
            // e.printStackTrace();
        } catch (IOException e) {
            // e.printStackTrace();
        }
        return result;
    }

    private static Node getNode(String nodeName, NodeList nodelist) {
        Node result = null;

        for (int i = 0; i < nodelist.getLength(); i++) {
            // return tag
            Node childNode = (Node) nodelist.item(i);
            if (nodeName.equalsIgnoreCase(childNode.getNodeName())) {
                result = childNode;
            }
        }
        return result;
    }
}
