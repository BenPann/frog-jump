package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.CaseDataKycDebtInfo;
import com.kgi.eopend3.ap.dao.CRUDQDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class CaseDataKycDebtInfoDao extends CRUDQDao<CaseDataKycDebtInfo> {
    public static final String NAME = "CaseDataKycDebtInfo" ;

    public CaseDataKycDebtInfo findByUniqId(String uniqId) {
        CaseDataKycDebtInfo keyItem = new CaseDataKycDebtInfo();
        keyItem.setUniqId(uniqId);
        return Read(keyItem);
    }

    @Override
    public int Create(CaseDataKycDebtInfo fullItem) {
        String sql = "INSERT INTO " + NAME 
                + " ( UniqId, MORTAGAGE_FLAG, MORTAGAGE_AMT, MORTAGAGE_PAY, CAR_LOAN_FLAG, CAR_LOAN_AMT, CAR_LOAN_PAY, PLOAN_FLAG, PLOAN_AMT, PLOAN_PAY, CREDIT_CARD_FLAG, CREDIT_CARD_AMT, CREDIT_CARD_PAY_KIND, CREDIT_CARD_CYCLE_AMT, CASH_CARD_FLAG, CASH_CARD_AMT )" 
        + " VALUES ( :UniqId,:MORTAGAGE_FLAG,:MORTAGAGE_AMT,:MORTAGAGE_PAY,:CAR_LOAN_FLAG,:CAR_LOAN_AMT,:CAR_LOAN_PAY,:PLOAN_FLAG,:PLOAN_AMT,:PLOAN_PAY,:CREDIT_CARD_FLAG,:CREDIT_CARD_AMT,:CREDIT_CARD_PAY_KIND,:CREDIT_CARD_CYCLE_AMT,:CASH_CARD_FLAG,:CASH_CARD_AMT )";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public CaseDataKycDebtInfo Read(CaseDataKycDebtInfo keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE UniqId = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CaseDataKycDebtInfo.class),
                    new Object[] { keyItem.getUniqId() });
        } catch (DataAccessException ex) {
            logger.error(NAME+"查無資料");           
            return null;
        }
    }

    @Override
    public int Update(CaseDataKycDebtInfo fullItem) {
        String sql = "UPDATE " + NAME + " SET MORTAGAGE_FLAG=:MORTAGAGE_FLAG, MORTAGAGE_AMT=:MORTAGAGE_AMT, MORTAGAGE_PAY=:MORTAGAGE_PAY, CAR_LOAN_FLAG=:CAR_LOAN_FLAG, CAR_LOAN_AMT=:CAR_LOAN_AMT, CAR_LOAN_PAY=:CAR_LOAN_PAY, PLOAN_FLAG=:PLOAN_FLAG, PLOAN_AMT=:PLOAN_AMT, PLOAN_PAY=:PLOAN_PAY, CREDIT_CARD_FLAG=:CREDIT_CARD_FLAG, CREDIT_CARD_AMT=:CREDIT_CARD_AMT, CREDIT_CARD_PAY_KIND=:CREDIT_CARD_PAY_KIND, CREDIT_CARD_CYCLE_AMT=:CREDIT_CARD_CYCLE_AMT, CASH_CARD_FLAG=:CASH_CARD_FLAG, CASH_CARD_AMT=:CASH_CARD_AMT "
        + " WHERE UniqId=:UniqId ";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(CaseDataKycDebtInfo keyItem) {
        String sql = "DELETE FROM " + NAME + " WHERE UniqId = ?";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getUniqId() });
    }

    @Override
    public List<CaseDataKycDebtInfo> Query(CaseDataKycDebtInfo keyItem) {
		String sql = "SELECT * FROM " + NAME + " WHERE UniqId = ?";					
		try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getUniqId() }, new BeanPropertyRowMapper<>(CaseDataKycDebtInfo.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }
    
}
