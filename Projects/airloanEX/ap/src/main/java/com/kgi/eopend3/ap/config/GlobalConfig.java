package com.kgi.eopend3.ap.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = { "file:${FUCO_CONFIG_PATH}/AP.properties" }, encoding = "UTF-8")
public class GlobalConfig {

    @Value("${AbbeyRye}")
    public String AbbeyRye; 

    @Value("${RayakRoe}")
    public String RayakRoe;

    @Value("${StaticFilePath}")
    public String StaticFilePath;
  
    @Value("${PdfDirectoryPath}")
    public String PdfDirectoryPath;
}