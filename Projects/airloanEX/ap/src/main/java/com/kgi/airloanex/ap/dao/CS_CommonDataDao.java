package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.CS_CommonData;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class CS_CommonDataDao extends CRUDQDao<CS_CommonData> {
    @Override
    public int Create(CS_CommonData fullItem) {
        String sql = "INSERT INTO CS_CommonData Values(?,?,?,?,?,GETDATE(),GETDATE());";
        return this.getJdbcTemplate().update(sql, new Object[] { fullItem.getUniqId(), fullItem.getUniqType(),
                fullItem.getCSType(),fullItem.getChannelId(),fullItem.getResultData() });
    }

    @Override
    public CS_CommonData Read(CS_CommonData keyItem) {
        String sql = "SELECT * FROM CS_CommonData WHERE UniqId = ?;";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CS_CommonData.class),
                    new Object[] { keyItem.getUniqId() });
        } catch (DataAccessException ex) {
            System.out.println("CS_CommonData查無資料");
            return null;
        }
    }

    @Override
    public int Update(CS_CommonData fullItem) {
        String sql = "UPDATE CS_CommonData SET ChannelId = ?, ResultData = ?, UpdateTime = GETDATE()  WHERE UniqId = ?;";
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getChannelId(), fullItem.getResultData(), fullItem.getUniqId() });
    }

    @Override
    public int Delete(CS_CommonData keyItem) {
        String sql = "DELETE CS_CommonData WHERE UniqId = ?;";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getUniqId() });
    }

    @Override
    public List<CS_CommonData> Query(CS_CommonData keyItem) {
        return null;
    }

}
