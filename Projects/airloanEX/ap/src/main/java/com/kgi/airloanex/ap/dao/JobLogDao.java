package com.kgi.airloanex.ap.dao;

import java.util.ArrayList;

import com.kgi.eopend3.ap.dao.BaseDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class JobLogDao extends BaseDao {

	public void AddJobLog(String jobName, int level, String msg) {
		ArrayList<Object> paramList = new ArrayList<Object>();
		//建立寫入SQL
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
			String sql =  "INSERT INTO JobLog"
					+ "(JobName, Level, Message, UTime) VALUES"
					+ "(?,?,?,GETDATE())";

			paramList.add(jobName);
			paramList.add(level);
			paramList.add(msg);
		try {	
			jdbcTemplate.update(sql, paramList.toArray());
		} catch (DataAccessException e) {
//		    e.printStackTrace();
		}
	}
}
