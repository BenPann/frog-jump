package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.ContractApiLog;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class ContractApiLogDao extends CRUDQDao<ContractApiLog> {
    public static final String NAME = "ContractApiLog";

    @Override
    public int Create(ContractApiLog fullItem) {
        String sql = "INSERT INTO " + NAME
                + " (Type, KeyInfo, ApiName, Request, Response, StartTime, EndTime, Used, CreateTime) VALUES "
                + "(:Type,:KeyInfo,:ApiName,:Request,:Response,:StartTime,:EndTime, :Used, GETDATE())";
            return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));

    }

    public int Create(String type, String keyInfo, String apiName, String req, String rep, String startTime,
            String endTime) {
        ContractApiLog dto = ContractApiLog.builder().Type(type).KeyInfo(keyInfo).ApiName(apiName).Request(req)
                .Response(rep).StartTime(startTime).EndTime(endTime).build();
        return this.Create(dto);
    }

    @Override
    public ContractApiLog Read(ContractApiLog keyItem) {
        return null;
    }

    @Override
    public int Update(ContractApiLog fullItem) {
        return 0;
    }

    @Override
    public int Delete(ContractApiLog keyItem) {
        return 0;
    }

    @Override
    public List<ContractApiLog> Query(ContractApiLog keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE KeyInfo = ? AND ApiName = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getKeyInfo(), keyItem.getApiName() },
                    new BeanPropertyRowMapper<>(ContractApiLog.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }

    public List<ContractApiLog> findByKeyInfoAndApiName(String KeyInfo, String ApiName) {
        ContractApiLog log = new ContractApiLog();
        log.setKeyInfo(KeyInfo);
        log.setApiName(ApiName);
        List<ContractApiLog> list = Query(log);
        list.removeIf(c -> c.isUsed() == true);
        return list;
    }

    public int updateUsed(int Serial) {
        String sql = "UPDATE " + NAME
                + " SET Used = :Used "
                + " WHERE Serial = :Serial ";
        ContractApiLog fullItem = new ContractApiLog();
        fullItem.setUsed(true);
        fullItem.setSerial(Serial);
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    // 白名單模擬走updateDgt 讓contractApiLog變為無產品
    public int UpdateUsedWhite(String uniqId, String productId) {
        String sql = "UPDATE contractApiLog SET Response = '{\"CHECK_CODE\": \"99\",\"ERR_MSG\": \"查無待立約資料\",\"CONST_DATA\": \"\",\"DEP_DATA\": \"\"}' " +
                     "WHERE KeyInfo = :KeyInfo AND Request like :Request";
        ContractApiLog fullItem = new ContractApiLog();    
        fullItem.setKeyInfo(uniqId);
        fullItem.setRequest("%DATA_TYPE\":\"" + productId + "%");
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }
}