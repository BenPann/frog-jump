package com.kgi.airloanex.ap.service;

import com.kgi.airloanex.ap.dao.JobLogDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JobLogService {
	
	@Autowired
	private JobLogDao jobLogDao;
	
	/**
	 * @param msg
	 * 客戶勾選與我聯絡Job 寫一般info log
	 */
	public void AddCaseContactMeInfoLog(String msg) {
	    jobLogDao.AddJobLog("CaseContactMe", 1, msg);
	}
	/**
	 * @param msg
	 * 客戶勾選與我聯絡Job 寫遇到異常值發生問題 log
	 */
	public void AddCaseContactMeChkfailLog(String msg) {
	    jobLogDao.AddJobLog("CaseContactMe", 2, msg);
	}
	/**
	 * @param msg
	 * 客戶勾選與我聯絡Job 寫連線遇到問題 log
	 */
	public void AddCaseContactMeConnOutLog(String msg) {
	    jobLogDao.AddJobLog("CaseContactMe", 3, msg);
	}
	/**
	 * @param msg
	 * 客戶勾選與我聯絡Job 寫發生重大問題 log
	 */
	public void AddCaseContactMeErrorLog(String msg) {
	    jobLogDao.AddJobLog("CaseContactMe", 4, msg);
	}
	/**
	 * @param msg
	 * 專人與我聯絡Job 寫一般info log
	 */
	public void AddContactUsInfoLog(String msg) {
	    jobLogDao.AddJobLog("ContactUs", 1, msg);
	}
	/**
	 * @param msg
	 * 專人與我聯絡Job 寫遇到異常值發生問題 log
	 */
	public void AddContactUsChkfailLog(String msg) {
	    jobLogDao.AddJobLog("ContactUs", 2, msg);
	}
	/**
	 * @param msg
	 * 專人與我聯絡Job 寫連線遇到問題 log
	 */
	public void AddContactUsConnOutLog(String msg) {
	    jobLogDao.AddJobLog("ContactUs", 3, msg);
	}
	/**
	 * @param msg
	 * 專人與我聯絡Job 寫發生重大問題 log
	 */
	public void AddContactUsErrorLog(String msg) {
	    jobLogDao.AddJobLog("ContactUs", 4, msg);
	}
	/**
	 * @param msg
	 * 案件打包送件Job 寫一般info log
	 */
	public void AddSendDataInfoLog(String msg) {
	    jobLogDao.AddJobLog("SendData", 1, msg);
	}
	/**
	 * @param msg
	 * 案件打包送件Job 寫遇到異常值發生問題 log
	 */
	public void AddSendDataChkfailLog(String msg) {
	    jobLogDao.AddJobLog("SendData", 2, msg);
	}
	/**
	 * @param msg
	 * 案件打包送件Job 寫連線遇到問題 log
	 */
	public void AddSendDataConnOutLog(String msg) {
	    jobLogDao.AddJobLog("SendData", 3, msg);
	}
	/**
	 * @param msg
	 * 案件打包送件Job 寫發生重大問題 log
	 */
	public void AddSendDataErrorLog(String msg) {
	    jobLogDao.AddJobLog("SendData", 4, msg);
	}
	/**
	 * @param msg
	 * Job 寫一般info log
	 */
	public void AddInfoLog(String jobName,String msg) {
	    jobLogDao.AddJobLog(jobName, 1, msg);
	}
	/**
	 * @param msg
	 * Job 寫遇到異常值發生問題 log
	 */
	public void AddIsPreAuditChkfailLog(String msg) {
	    jobLogDao.AddJobLog("IsPreAudit", 2, msg);
	}
	/**
	 * @param msg
	 * Job 寫連線遇到問題 log
	 */
	public void AddIsPreAuditConnOutLog(String msg) {
	    jobLogDao.AddJobLog("IsPreAudit", 3, msg);
	}
	/**
	 * @param msg
	 * 案件打包送件Job 寫發生重大問題 log
	 */
	public void AddIsPreAuditErrorLog(String msg) {
	    jobLogDao.AddJobLog("IsPreAudit", 4, msg);
	}



	/**
	 * @param msg
	 * Job 寫連線遇到問題 log
	 */
	public void AddConnOutLog(String msg,String title) {
		jobLogDao.AddJobLog(title, 3, msg);
	}
	
	
	/**
	 * @param msg
	 * Job 寫一般info log
	 */
	public void AddSendAMLInfoLog(String msg) {
		jobLogDao.AddJobLog("SendAML", 1, msg);
	}
	
	/**
	 * @param msg
	 * Job 寫遇到異常值發生問題 log
	 */
	public void AddSendAMLChkfailLog(String msg) {
	    jobLogDao.AddJobLog("SendAML", 2, msg);
	}
	
	/**
	 * @param msg
	 * Job 寫連線遇到問題 log
	 */
	public void AddSendAMLConnOutLog(String msg) {
		jobLogDao.AddJobLog("SendAML", 3, msg);
	}
	
	/**
	 * @param msg
	 * Job 寫發生重大問題 log
	 */
	public void AddSendAMLErrorLog(String msg) {
		jobLogDao.AddJobLog("SendAML", 4, msg);
	}

}
