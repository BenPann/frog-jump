package com.kgi.airloanex.ap.job;

import com.kgi.airloanex.ap.service.JcicService;
import com.kgi.airloanex.ap.service.JobLogService;
import com.kgi.airloanex.ap.service.OrbitService;
import com.kgi.airloanex.ap.service.PhotoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = {"file:${FUCO_CONFIG_PATH}/ScheduledJob.properties"}, encoding = "UTF-8")
public class SendPDFJob {

    @Autowired
    private JcicService jcicService;

    @Autowired
    private JobLogService jobLogService;

    @Autowired
    private OrbitService orbitService;

    @Autowired
    private PhotoService photoService;

    @Value("${SendPDFJobSwitch}")
    private String SendPDFJobSwitch;
    
    @Scheduled(cron = "${SendPDFJobSchedule}")
    public void run() {

        if(SendPDFJobSwitch.equals("0")) {
			return;
        }
        
        int err = 0;
        
        //System.out.println("************  airloanEX AP 排程執行中  ************");
        // ---------------------------------------------------------------
        // 0. 發查聯徵
        try {
            err = 0;
            jcicService.sendJcic(); // 發查聯徵, do step-2 & step-3，找 STATUS_01_JobStatus_030 的
        } catch (Exception e) {
            jobLogService.AddSendDataErrorLog(err + " " + e.getMessage());
        }
        // ---------------------------------------------------------------
        // 1. 第一次開始產生申請書PDF(貸款打包案件給影像系統)
        try {
            err = 1;
            orbitService.uploadCaseDataToOrbit(); // 第一次開始產生申請書PDF，找 CASE_STATUS_01_JobStatus_010 案件
            // photoService.uploadCreditCaseDataToOrbit();	// 信用卡
            // occreditService.autoFinishCreditDataFromED3CreditData();

            // orbitService.uploadCaseKycToOrbit();
        } catch (Exception e) {
            jobLogService.AddSendDataErrorLog(err + " " + e.getMessage());
        }
        // ---------------------------------------------------------------
        try {
            err = 2;
            orbitService.uploadIdcardOrFinpfToOrbit();
        } catch (Exception e) {
            jobLogService.AddSendDataErrorLog(err + " " + e.getMessage());
        }
        // ---------------------------------------------------------------
        // 2. 第二次開始產生申請書PDF(含ID財力圖檔)
        try {
            err = 3;
			// 第二次開始產生申請書PDF(含ID財力圖檔)，找[Status JobStatus]=01 040
            orbitService.uploadCaseDataAndKycAndPhotoAddon(); 
            // orbitService.uploadAddPhotoOrbitData(); // 第二次開始產生申請書PDF(含ID財力圖檔) + online補件資料上傳ftp
            // photoService.uploadAddPhotoOrbitData(); // 信用卡
        } catch (Exception e) {
            e.printStackTrace();
            jobLogService.AddSendDataErrorLog(err + " " + e.getMessage());
        }
        // ---------------------------------------------------------------
        // 3. 處理拒貸資料打包後送
        try {
            err = 4;
            // FIXME:orbitService.uploadRejectCase();
        } catch (Exception e) {
            jobLogService.AddSendDataErrorLog(err + " " + e.getMessage());
        }
        // ---------------------------------------------------------------
        // 4. 處理線上立約資料打包後送
        try {
            err = 5;
            orbitService.uploadContractDataToOrbit();
        } catch (Exception e) {
            jobLogService.AddSendDataErrorLog(err + " " + e.getMessage());
        }
        // ---------------------------------------------------------------
    }
}
