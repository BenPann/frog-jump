package com.kgi.crosssell.ap.service;

import java.io.IOException;

import com.fuco.prod.common.util.GlobalProp;
import com.kgi.airloanex.ap.dao.APSDao;
import com.kgi.airloanex.ap.dao.CaseAuthDao;
import com.kgi.airloanex.ap.dao.WhiteListDao;
import com.kgi.airloanex.ap.dao.WhiteListDataDao;
import com.kgi.eopend3.common.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.json.JSONObject;

@Service
public class APSTmpService {

    @Autowired
    private APSDao apsDao;

    @Autowired
    CaseAuthDao casAuthDao;
    @Autowired
    WhiteListDao WhiteListDao;
    @Autowired
    WhiteListDataDao WhiteListdataDao;

    private String queryAPS(String qryName, String queryJson) throws IOException {
        return queryAPS(qryName, queryJson, true);
    }

    private String queryAPS(String apsUrl, String qryName, String queryJson, Boolean writeLog) throws IOException {
        // 1. 組合出 request 的 url
        String response = "";
        String startTime = "";
        String endTime = "";
        int iLenForShow = 10;// default
        int iLenForCheck = 52;

        try {

            // 2. 發送 request
            startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");

            /// KGI/GET_CUST_INFO比較久，先記LOG
            if (qryName.equals("/KGI/GET_CUST_INFO")) {
                apsDao.insertApiLog(qryName, queryJson, "-開始-", startTime, startTime);
            }

            if (queryJson.length() >= iLenForCheck) {
                iLenForShow = iLenForCheck;
            } else {
                iLenForShow = queryJson.length();
            }

            System.out.println(
                    "#### 呼叫 APS-API (" + qryName + ")....Start :" + queryJson.substring(0, iLenForShow) + ".....(略)");

            response = apsDao.queryAPI(apsUrl, qryName, queryJson);

            if (response.length() >= iLenForCheck) {
                iLenForShow = iLenForCheck;
            } else {
                iLenForShow = response.length();
            }

            System.out.println(
                    "#### 呼叫 APS-API (" + qryName + ")....End :" + response.substring(0, iLenForShow) + ".....(略)");

            endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
        } finally {
            // 3. 寫apilog
            if (writeLog) {
                if (endTime == "")
                    endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
                apsDao.insertApiLog(qryName, queryJson, response, startTime, endTime);
            }
        }

        // 4. 返回結果
        return response;
    }

    private String queryAPS(String qryName, String queryJson, Boolean writeLog) throws IOException {
        String apsUrl = GlobalProp.getInstance().getString("APSServerHost");
        // System.out.println(apsUrl);
        return queryAPS(apsUrl, qryName, queryJson, writeLog);
    }

    public String getDGTCaseInfo(String caseNoWeb) throws IOException {
        JSONObject r = new JSONObject();
        r.put("CASE_NO", caseNoWeb);
        return queryAPS("/KGI/GET_DGTCASE_INFO", r.toString());
    }
}
