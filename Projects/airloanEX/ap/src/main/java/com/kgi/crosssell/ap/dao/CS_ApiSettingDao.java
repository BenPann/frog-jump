package com.kgi.crosssell.ap.dao;

import java.util.List;

import com.kgi.crosssell.ap.dto.CS_ApiSetting;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class CS_ApiSettingDao extends CRUDQDao<CS_ApiSetting> {

	@Override
	public int Create(CS_ApiSetting fullItem) {
		return 0;
	}

	@Override
	public CS_ApiSetting Read(CS_ApiSetting keyItem) {
		String sql = "SELECT * FROM CS_ApiSetting WHERE ChannelId = ? AND 1=1";
		try {
			return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CS_ApiSetting.class),
					new Object[] { keyItem.getChannelId() });
		} catch (DataAccessException ex) {
			System.out.println("#### " + keyItem.getChannelId() + " 在 CS_ApiSetting 查無對應資料.");
			return null;
		}
	}

	@Override
	public int Update(CS_ApiSetting fullItem) {
		return 0;
	}

	@Override
	public int Delete(CS_ApiSetting keyItem) {
		return 0;
	}

	@Override
	public List<CS_ApiSetting> Query(CS_ApiSetting keyItem) {
		return null;
	}

}
