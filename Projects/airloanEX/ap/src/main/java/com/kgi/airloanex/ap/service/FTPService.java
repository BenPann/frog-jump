package com.kgi.airloanex.ap.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.kgi.airloanex.ap.dao.FTPDao;
import com.kgi.airloanex.ap.config.AirloanEXConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sun.misc.BASE64Decoder;

@Service
public class FTPService {
	@Autowired
	FTPDao ftpdao;
	
	@Autowired
	AirloanEXConfig globalConfig;

	InputStream inputStream = null;
	
	public void toInputStream(byte[] bytes) {
		inputStream = new ByteArrayInputStream(bytes);
	}

	public void Base64ToInputStream(String fileInBase64Str) throws Exception{
		if (fileInBase64Str != null) { //處理檔案為空             
		//Base64解碼     
		BASE64Decoder decoder = new BASE64Decoder();
		
	 		@SuppressWarnings("restriction")
			byte[] b = decoder.decodeBuffer(fileInBase64Str);             
	 		for(int i=0;i<b.length;++i)             
	 		{                 
	 			if(b[i]<0)                 
	 			{//調整異常數據                     
	 				b[i]+=256;                
	 			}            
	 		}            
	 		//生成圖片            
	 		inputStream = new ByteArrayInputStream(b);
		}	
	}
	public void fileToInputStream(byte[] file) throws Exception{
		if (file != null) { //處理檔案為空

			for(int i=0;i<file.length;++i)
			{
				if(file[i]<0)
				{//調整異常數據
					file[i]+=256;
				}
			}
			//生成圖片
			inputStream = new ByteArrayInputStream(file);
		}
	}



	
	public boolean uploadImgBase64File(String filename) {
		String serverAdd = globalConfig.Img_FTPServer_Address();
		String ftpUserId = globalConfig.Img_FTPServer_Userid();
		String ftppazz = globalConfig.Img_FTPServer_PWD();
		String path = globalConfig.Img_FTPServer_Path();
		return ftpdao.upload(serverAdd, 21, ftpUserId, ftppazz, path, filename, inputStream);
	}
	
	public boolean uploadImgBase64File(String filename,byte[] bs) {
		String serverAdd = globalConfig.Img_FTPServer_Address();
		String ftpUserId = globalConfig.Img_FTPServer_Userid();
		String ftppazz = globalConfig.Img_FTPServer_PWD();
		String path = globalConfig.Img_FTPServer_Path();
		inputStream = new ByteArrayInputStream(bs);
		return ftpdao.upload(serverAdd, 21, ftpUserId, ftppazz, path, filename, inputStream);
	}
	
	public boolean uploadImgXml(String filename, InputStream is) {
		String serverAdd = globalConfig.Img_FTPServer_Address();
		String ftpUserId = globalConfig.Img_FTPServer_Userid();
		String ftppazz = globalConfig.Img_FTPServer_PWD();
		String path = globalConfig.Img_FTPServer_Path();
		return ftpdao.upload(serverAdd, 21, ftpUserId, ftppazz, path, filename, is);
	}
	
	
	public boolean uploadMailHunter(String ftppath, String filename, InputStream is) {
		String serverAdd = globalConfig.MailHunter_FTPServer_Address();
		String ftpUserId = globalConfig.MailHunter_FTPServer_Userid();
		String ftppazz = globalConfig.MailHunter_FTPServer_PAZZD();
		
		return ftpdao.upload(serverAdd, 21, ftpUserId, ftppazz, ftppath, filename, is);
	}
	
	/**
	 * @param filename
	 * 上傳已產好整批pdf檔
	 */
	public boolean uploadMailHunterPdfBase64File(String filename) {
		String serverAdd = globalConfig.MailHunter_FTPServer_Address();
		String ftpUserId = globalConfig.MailHunter_FTPServer_Userid();
		String ftppazz = globalConfig.MailHunter_FTPServer_PAZZD();

		return ftpdao.upload(serverAdd, 21, ftpUserId, ftppazz, "File", filename, inputStream);
	}
}
