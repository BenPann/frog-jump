package com.kgi.eopend3.ap.dao;

import java.util.List;

public abstract class CRUDQDao<T> extends BaseDao {

    public abstract int Create(T fullItem);

    public abstract T Read(T keyItem);

    public abstract int Update(T fullItem);

    public abstract int Delete(T keyItem);

    public abstract List<T> Query(T keyItem);

    public boolean Exist(T keyItem) {
        return (Read(keyItem) != null);
    }
   
    /** 新增或更新 (會先判斷資料是否存在) */
    public int CreateOrUpdate(T fullItem) {
        if (Exist(fullItem)) {
            return Update(fullItem);
        } else {
            return Create(fullItem);
        }
    }
}