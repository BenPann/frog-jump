package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.ContractWhiteList;
import com.kgi.eopend3.ap.dao.CRUDQDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class ContractWhiteListDao extends CRUDQDao<ContractWhiteList> {
    public static final String NAME = "ContractWhiteList";

    @Override
    public int Create(ContractWhiteList fullItem) {
        return 0;
    }

    @Override
    public ContractWhiteList Read(ContractWhiteList keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE ProductId = ? AND Status = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(ContractWhiteList.class),
                    new Object[] { keyItem.getProductId(), keyItem.getStatus() });
        } catch (Exception e) {
            logger.error(NAME + "查無資料");
            throw new ErrorResultException(9, NAME + "查無資料", "查無資料", e);
        }
    }

    @Override
    public int Update(ContractWhiteList fullItem) {
        return 0;
    }

    @Override
    public int Delete(ContractWhiteList keyItem) {
        return 0;
    }

    @Override
    public List<ContractWhiteList> Query(ContractWhiteList keyItem) {
        return null;
    }

    public String getStatus(String idno, String productId) {
        String status = "";
        String sql = "SELECT * FROM ContractWhiteList WHERE Idno = ? AND ProductId = ?";
        try {
             ContractWhiteList resultData =  this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(ContractWhiteList.class),
                    new Object[] { idno, productId });
             status = resultData.getStatus();
        } catch (Exception e) {
            logger.error(NAME + "查無資料");
            throw new ErrorResultException(9, NAME + "查無資料", "查無資料", e);
        }
        return status;
    }
}
