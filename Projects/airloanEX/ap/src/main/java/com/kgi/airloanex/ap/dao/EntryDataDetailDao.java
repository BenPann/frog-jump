package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.EntryDataDetail;
import com.kgi.eopend3.ap.dao.CRUDQDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class EntryDataDetailDao extends CRUDQDao<EntryDataDetail> {
    public static final String NAME = "EntryDataDetail";

    @Override
    public int Create(EntryDataDetail fullItem) {
        String sql = "INSERT INTO " + NAME
                + " (UniqId, UserAgent) "
                + " VALUES (:UniqId,  :UserAgent)";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public EntryDataDetail Read(EntryDataDetail keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE UniqId = ?";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(EntryDataDetail.class),
                    new Object[] { keyItem.getUniqId() });
        } catch (DataAccessException ex) {
            logger.error(NAME + "查無資料");
            throw new ErrorResultException(9, NAME + "查無資料", ex);
        }
    }

    public EntryDataDetail ReadByUniqId(String uniqId) {
        EntryDataDetail contractMain = new EntryDataDetail(uniqId);
        return this.Read(contractMain);
    }

    @Override
    public int Update(EntryDataDetail fullItem) {
        String sql = "UPDATE " + NAME
                + " SET UserAgent = :UserAgent "
                + " WHERE UniqId=:UniqId ";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(EntryDataDetail keyItem) {
        String sql = "DELETE FROM " + NAME + " WHERE UniqId = ?";
        return this.getJdbcTemplate().update(sql, new Object[] { keyItem.getUniqId() });
    }

    @Override
    public List<EntryDataDetail> Query(EntryDataDetail keyItem) {
        String sql = "SELECT * FROM " + NAME + " WHERE UniqId = ?";
        try {
            return this.getJdbcTemplate().query(sql, new Object[] { keyItem.getUniqId() },
                    new BeanPropertyRowMapper<>(EntryDataDetail.class));
        } catch (DataAccessException ex) {
            return null;
        }
    }
}