package com.kgi.airloanex.ap.dao;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * -- EACHDAY: eeeMMdd 民國年月日
 * -- CODE1: [工作日 CODE1=0, 假日 CODE1=1]
 * -- WHATDAY: [星期一 WHATDAY=1 ... 星期日 WHATDAY=7]
 */
@Repository
public class OracleWdDao {
    
    @Autowired
	@Qualifier("oracleWD")
    private DataSource oracleWD;
    
    // 判斷當日是否為營業日
    public boolean checkWorkDay(String date){
		JdbcTemplate jdbcTemplate = new JdbcTemplate(oracleWD);
		String sql = "SELECT EACHDAY,WHATDAY,CODE1 From lbdate where EACHDAY = ? ";
		int ckDate = Integer.valueOf(date) - 19110000;
		boolean workday = true;
		List<Map<String,Object>> list = jdbcTemplate.queryForList(sql, String.valueOf(ckDate));
		if(list.size() > 0){
			if(list.get(0).get("CODE1").toString().trim().equals("1")){
				workday = false;
			}
		}
		return workday;
    }
    
    // 取得銀行營業日檔下一營業日
	public String getNextWorkDay(String date) throws Exception {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(oracleWD);
        String sql = "SELECT EACHDAY,WHATDAY,CODE1 From lbdate where EACHDAY >= ? and CODE1 = 0 and rownum = 1";
        int ckDate = Integer.valueOf(date) - 19110000;
        String eachDate = "";
        List<Map<String,Object>> list = jdbcTemplate.queryForList(sql, String.valueOf(ckDate));
        if(list.size() > 0){
            eachDate = String.valueOf(Integer.valueOf(list.get(0).get("EACHDAY").toString().trim()) + 19110000);
        }
        return eachDate;
    }
}