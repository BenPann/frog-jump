package com.kgi.airloanex.ap.controller;

import java.util.Map;

import com.kgi.airloanex.ap.service.AMLService;
import com.kgi.eopend3.ap.controller.base.BaseController;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/aml")
public class AMLController extends BaseController {
    
    private static String context = "AMLController";

    @Autowired
    private AMLService amlService;
    
    // AML驗證身分證
    @PostMapping("/GET_AML_XML_DATA")
    public Map<String, String> getAmlXmlData(@RequestBody String reqBody) {
        logger.info("AML前台輸入的資料" + reqBody);
        if (ESAPI.validator().isValidInput(context, reqBody, "SafeJson", Integer.MAX_VALUE, false)) {
            return amlService.processAml(reqBody);
        } else {
            return null;
        }
    }
}
