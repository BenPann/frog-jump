package com.kgi.airloanex.ap.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.BadPdfFormatException;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.ContractApiLogDao;
import com.kgi.airloanex.ap.pdf.BasePDFDocument;
import com.kgi.airloanex.ap.pdf.CaseFinalePDF;
import com.kgi.airloanex.ap.pdf.CaseKycPDF;
import com.kgi.airloanex.ap.pdf.CasePDF;
import com.kgi.airloanex.ap.pdf.CreditCardPDF;
import com.kgi.airloanex.ap.pdf.PDFHelper;
import com.kgi.eopend3.ap.dao.HttpDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class PdfService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
    private AirloanEXConfig globalConfig;

    @Autowired
    private HttpDao httpDao ;
    
    @Autowired
    private ContractApiLogDao contractApiLogDao;
    
    @Autowired
	private ApplicationContext appContext;
    
    /** Charles: 以下程式碼是 Ben 用 nodejs 產製 pdf 的作法，但是後來未採用了。 */
    // public Html2PdfResp htmlToPdf(String uniqId, ContractHtmlView view) {
	// 	String startTime = "";
    //     String endTime = "";
    //     String logReq = "", logRes = "" ;
        
    //     String jsonStr = new Gson().toJson(view);
    //     startTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
    //     HttpResp response = httpDao.postJson(globalConfig.PDF_PdfServerUrl() + PlContractConst.API_HTML2PDF, jsonStr);
    //     endTime = DateUtil.GetDateFormatString("yyyy/MM/dd HH:mm:ss.SSS");
    //     logRes = response.getResponse() ;
    //     logger.info("### logRes" + logRes);
    //     Html2PdfResp html2PdfResp = new Gson().fromJson(response.getResponse(), Html2PdfResp.class) ;
    //     contractApiLogDao.Create(SystemConst.APILOG_TYPE_CALLOUT, uniqId, PlContractConst.API_HTML2PDF, logReq, logRes, startTime, endTime);
    //     return html2PdfResp;
    // }

    /**
	 * CasePDF PDF套表流程新版
     * 
     * 信用貸款申請書
	 */
	public void pdfGeneratorActViewAndSendEmail(String caseNo) throws Exception {
		CasePDF pdf = new CasePDF(appContext);
		pdf.init(caseNo);
		PDFHelper helper = new PDFHelper(pdf);
		helper.PdfProcess();
    }
    
    /**
     * CreditCardPDF 產生pdf 儲存 並寄送
     * 
     * 信用卡申請書
     */
    public void pdfGeneratorCreditCardPDF(String uniqId) throws Exception {
        CreditCardPDF creditCardPDF = new CreditCardPDF(appContext);
        creditCardPDF.init(uniqId);
        PDFHelper helper = new PDFHelper(creditCardPDF);
        helper.PdfProcess();
    }

    public void pdfGeneratorCaseKycPDF(String uniqId) throws Exception {
        CaseKycPDF caseKycPDF = new CaseKycPDF(appContext);
        caseKycPDF.init(uniqId);
        PDFHelper helper = new PDFHelper(caseKycPDF);
        helper.PdfProcess();
    }

    /**
     * CasePDF + CaseKycPDF
     * 
     * 第二次開始產生申請書PDF
     */
    public void pdfGeneratorCaseFinalePDF(String uniqId) throws Exception {
        // 1. 寄送，貸款申請書給客戶
        // Charles:這裡 pdfGeneratorActViewAndSendEmail 的主要目的是「寄送 email 給客戶」，且其 email 內容僅有貸款申請書，不可包含 KYC表 及 同一關係人表
        pdfGeneratorActViewAndSendEmail(uniqId);

        // 2. 產製，3合1，包含貸款申請書、KYC表 及 同一關係人表
        CaseFinalePDF caseFinalePDF = new CaseFinalePDF(appContext);
        caseFinalePDF.init(uniqId);
        PDFHelper helper = new PDFHelper(caseFinalePDF);
        // helper.PdfProcess();
        PdfProcessCaseFinalePDF(helper, caseFinalePDF);
    }

    private byte[] PdfProcessCaseFinalePDF(PDFHelper helper, CaseFinalePDF caseFinalePDF) throws Exception {
        BasePDFDocument pdfDocument = helper.getPDFDocument();
        if (pdfDocument == null)
        throw new IOException("尚未傳入報表類別");
        logger.info("### 開始 PdfProcessCaseFinalePDF");
        ByteArrayOutputStream pdfStream = new ByteArrayOutputStream();
        try {
            boolean needRelationDegreeReport = caseFinalePDF.needRelationDegreeReport();
            BasePDFDocument pdfDocumentCasePDF = caseFinalePDF.getCasePDF();
            BasePDFDocument pdfDocumentCaseKycPDF = caseFinalePDF.getCaseKycPDF();
            BasePDFDocument pdfDocumentCaseRelationDegreePDF = caseFinalePDF.getCaseRelationDegreePDF();
            logger.info("1.beforePdfProcess - pdfDocumentCasePDF");
            pdfDocumentCasePDF.beforePdfProcess();
            logger.info("2.beforePdfProcess - pdfDocumentCaseKycPDF");
            pdfDocumentCaseKycPDF.beforePdfProcess();
            if (needRelationDegreeReport) {
                logger.info("3.beforePdfProcess - pdfDocumentCaseRelationDegreePDF");
                pdfDocumentCaseRelationDegreePDF.beforePdfProcess();
            }

            // 產生暫存檔
            ByteArrayOutputStream pdfStreamCasePDF = new ByteArrayOutputStream();
            ByteArrayOutputStream pdfStreamCaseKycPDF = new ByteArrayOutputStream();
            ByteArrayOutputStream pdfStreamCaseRelationDegreePDF = new ByteArrayOutputStream();

            // 產生PDF generatePDF
            logger.info("1.generatePDF - pdfStreamCasePDF");
            helper.generatePDF(pdfStreamCasePDF, ExportHtml(pdfDocumentCasePDF), pdfDocument.getPageEvent());
            logger.info("2.generatePDF - pdfStreamCaseKycPDF");
            helper.generatePDF(pdfStreamCaseKycPDF, ExportHtml(pdfDocumentCaseKycPDF), pdfDocument.getPageEvent());
            if (needRelationDegreeReport) {
                logger.info("3.generatePDF - pdfStreamCaseRelationDegreePDF");
                helper.generatePDF(pdfStreamCaseRelationDegreePDF, ExportHtml(pdfDocumentCaseRelationDegreePDF), pdfDocument.getPageEvent());
            }

            if (pdfDocumentCasePDF.getUseWaterMark()) {
                logger.info("1.addWaterMark - pdfStreamCasePDF");
                helper.addWaterMark(pdfStreamCasePDF, pdfDocumentCasePDF.getWaterMarkText(), pdfDocumentCasePDF.getWaterMarkFont());
            }
            if (pdfDocumentCaseKycPDF.getUseWaterMark()) {
                logger.info("2.addWaterMark - pdfStreamCaseKycPDF");
                helper.addWaterMark(pdfStreamCaseKycPDF, pdfDocumentCaseKycPDF.getWaterMarkText(), pdfDocumentCaseKycPDF.getWaterMarkFont());
            }
            if (pdfDocumentCaseRelationDegreePDF.getUseWaterMark() && needRelationDegreeReport) {
                logger.info("3.addWaterMark - pdfStreamCaseRelationDegreePDF");
                helper.addWaterMark(pdfStreamCaseRelationDegreePDF, pdfDocumentCaseRelationDegreePDF.getWaterMarkText(), pdfDocumentCaseRelationDegreePDF.getWaterMarkFont());
            }

            Document document = new Document();
            PdfCopy copy = new PdfCopy(document, pdfStream);
            document.open();
            logger.info("1.addPage - pdfReaderCasePDF");
            PdfReader pdfReaderCasePDF = new PdfReader(pdfStreamCasePDF.toByteArray());
            addPage(copy, pdfReaderCasePDF);
            logger.info("2.addPage - pdfReaderCaseKycPDF");
            PdfReader pdfReaderCaseKycPDF = new PdfReader(pdfStreamCaseKycPDF.toByteArray());
            addPage(copy, pdfReaderCaseKycPDF);
            logger.info("3.addPage - pdfReaderCaseRelationDegreePDF");
            if (needRelationDegreeReport) {
                PdfReader pdfReaderCaseRelationDegreePDF = new PdfReader(pdfStreamCaseRelationDegreePDF.toByteArray());
                addPage(copy, pdfReaderCaseRelationDegreePDF);
            }
            document.close();

            // 壓浮水印 addWaterMark
            // if (pdfDocument.getUseWaterMark()) {
            //     helper.addWaterMark(pdfStream, pdfDocument.getWaterMarkText(), pdfDocument.getWaterMarkFont());
            // }

            // 將PDF轉圖檔(如果有需要 加密前先做轉圖片) pdfToImage
            if (pdfDocument.getPdfToImage()) {
                pdfDocument.pdfToImage(helper.getImageList(pdfStream.toByteArray()));
            }

            // 將PDF加密 encryptPDF
            if (pdfDocument.getUseEncrepyt()) {
                helper.encryptPDF(pdfStream, pdfDocument.getEncryptPassword());
            }

            // 儲存至DB saveToDB
            if (pdfDocument.getUseSaveToDB()) {
                pdfDocument.saveToDB(pdfStream.toByteArray());
            }

            // 發送Mail sendEmail
            if (pdfDocument.getUseSendEMail()) {
                pdfDocument.sendEmail(pdfStream.toByteArray());
            }

            pdfDocument.afterPdfProcess();

        } finally {
            if (pdfStream != null)
                pdfStream.close();
        }
        logger.info("### 結束 PdfProcessCaseFinalePDF");
        return pdfStream.toByteArray();
    }

    /** 匯出轉換過的Html */
    public String ExportHtml(BasePDFDocument pdfDocument) throws Exception {
        if (pdfDocument == null)
            throw new IOException("尚未傳入報表類別");
        // 取得Html
        String raw = pdfDocument.getRawHtml();
        if (raw == null || raw.trim().equals(""))
            throw new IOException("尚未傳入Html");
        System.out.println("### rawHtml.length()=" + raw.length());
        // Replace
        return pdfDocument.replaceField(raw.trim());
    }

    private void addPage(PdfCopy copy, PdfReader pdfReader) throws BadPdfFormatException, IOException {
        int pages = pdfReader.getNumberOfPages();
        for (int i = 0; i < pages; i++) {
            logger.info("### pdf - loop page=" + (i + 1));
            copy.addPage(copy.getImportedPage(pdfReader, i + 1));
        }
    }
}