package com.kgi.airloanex.ap.pdf.dto;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class PDFCaseData {
	public String CaseNo;			//案件編號	

	public String UserType;
	public String OldFinaImgNoList;
	public String OldIdImgNoList;

	public Integer p_period;			//貨款期間
	public Integer p_purpose;			//資金用途
	public String p_purpose_name;	//資金用途說明
	
	public Integer p_apy_amount;		//貨款申請金額
	
	public String Occupation;       //職業類別
	public String customer_name; 	//姓名
	public String gender;			//性別
	public String corp_name;		//公司名稱
	public String birthday;			//生日
	public String marriage;			//婚姻狀況	
	public String corp_tel_area; 	//公司電話區碼
	public String corp_tel;			//公司電話
	public String corp_tel_exten;	//公司分機
	public String idno;				//身分證字號
	public String corp_city_code;	//公司郵遞區碼
	public String corp_address;     //公司地址
	public String education;		//教育程度
	public Integer yearly_income;		//年收入
	public String on_board_date;	//到職日
	public String email_address;	//EMAIL地址
	public String title;			//職務名稱
	public String estate_type;		//不動產狀況
	public String mobile_tel;		//行動電話
	public String house_city_code;	//居住地郵遞區碼
	public String house_address;	//居住地址
	public String house_tel_area;	//住宅電話區碼
	public String house_tel;		//住宅電話
	public String prj_code;			//專案代號
	public String PJ;
	
	public String productId;		//產品代號
	public String ip_address;		//ip address
	public boolean applycc;
	public String applyccUtime;
	public String resAddrZipCode;	//戶籍地郵遞區碼
	public String resAddr;			//戶籍地址
	public String resTelArea;		//戶籍地址電話區碼
	public String resTel;			//戶籍地址電話

	public String Z07Html;         //發查Z07 回傳聯徵資料 html
	public String Z07Result;       //發查Z07 null, Y：是(有通報案件記錄), N：否
	public String Z07Time;         //發查Z07的時間 yyyy-MM-dd HH:mm:ss.SSS

	public String CddRateRisk;     //風險評級:高 中 低
    
	public String AmlResult; // 發查AML成功:N 身分為正常 Y 身分為黑名單 V 身分為疑似有異常 or 99
	public String AmlTime; // 發查AML的時間

	public String CheckRelpResult; // 發查利害關係人: Y：利害關係人，N：非利害關係人
	public String CheckRelpTime; // 發查利害關係人的時間: yyyy-MM-dd HH:mm:ss.SSS

	public String CaseNoApsPrev;
	
	public PDFCaseData(){

	}
	public PDFCaseData(Map<String, Object> map) {
		// System.out.println(map);
		this.CaseNo = trimToEmpty(map.get("CaseNo"));

		this.UserType = trimToEmpty(map.get("UserType"));
		this.OldFinaImgNoList = trimToEmpty(map.get("OldFinaImgNoList"));
		this.OldIdImgNoList = trimToEmpty(map.get("OldIdImgNoList"));

		this.p_period = map.get("p_period") == null ? 0 : (Integer)map.get("p_period");
		this.p_purpose = (Integer)map.get("p_purpose");
		this.p_purpose_name = trimToEmpty(map.get("p_purpose_name"));
		this.p_apy_amount = (Integer)map.get("p_apy_amount");
		
		this.Occupation = trimToEmpty(map.get("Occupation"));
		this.customer_name = trimToEmpty(map.get("customer_name"));
		this.gender = trimToEmpty(map.get("gender"));
		this.corp_name = trimToEmpty(map.get("corp_name"));
		this.birthday = trimToEmpty(map.get("birthday"));
		this.marriage = trimToEmpty(map.get("marriage"));
		this.corp_tel_area = trimToEmpty(map.get("corp_tel_area"));
		this.corp_tel = trimToEmpty(map.get("corp_tel"));
		this.corp_tel_exten = trimToEmpty(map.get("corp_tel_exten"));
		this.idno = trimToEmpty(map.get("idno"));
		
		this.corp_city_code = trimToEmpty(map.get("corp_city_code"));
		this.corp_address = trimToEmpty(map.get("corp_address")).replace(";", "");
		this.education = trimToEmpty(map.get("education"));
		this.yearly_income = (Integer)map.get("yearly_income");
		this.on_board_date = trimToEmpty(map.get("on_board_date"));
		this.email_address = trimToEmpty(map.get("email_address"));
		this.title = trimToEmpty(map.get("title"));
		this.estate_type = trimToEmpty(map.get("estate_type"));
		this.mobile_tel = trimToEmpty(map.get("mobile_tel"));
		
		this.house_city_code = trimToEmpty(map.get("house_city_code"));
		this.house_address = trimToEmpty(map.get("house_address")).replace(";", "");
		this.house_tel_area = trimToEmpty(map.get("house_tel_area"));
		this.house_tel = trimToEmpty(map.get("house_tel"));
		this.prj_code = trimToEmpty(map.get("prj_code"));
		this.PJ = trimToEmpty(map.get("PJ"));
		
		//產品代號
		this.productId = trimToEmpty(map.get("ProductId"));
		//處理ip 判斷是否有含「:」的ip號碼  並只取不含:後number的ip address
		String ipAddr =	trimToEmpty(map.get("ip_address"));
		if(ipAddr.indexOf(":") > -1) {
			ipAddr = ipAddr.substring(0, ipAddr.indexOf(":"));
		}
		
		this.ip_address = ipAddr;
		String applyccUtime = trimToEmpty(map.get("applyccUtime").toString());

		boolean hasNoCreditVerify = (map.get("UniqId") == null);

		if(hasNoCreditVerify) {
			this.applycc = false;
			this.applyccUtime = "";
		}else {
			this.applycc = true;
			this.applyccUtime = StringUtils.substring(applyccUtime, 0, 4)
					            +StringUtils.substring(applyccUtime, 5, 7)
					            +StringUtils.substring(applyccUtime, 8, 10);
		}
		
		this.resAddrZipCode = trimToEmpty(map.get("resAddrZipCode"));	//戶籍地郵遞區碼
		this.resAddr = trimToEmpty(map.get("resAddr")).replace(";", "");		//戶籍地址
		this.resTelArea = trimToEmpty(map.get("resTelArea"));				//戶籍地址電話區碼
		this.resTel = trimToEmpty(map.get("resTel"));							//戶籍地址電話

		this.Z07Html = trimToEmpty(map.get("Z07Html")); 
		this.Z07Result = trimToEmpty(map.get("Z07Result"));
		this.Z07Time = trimToEmpty(map.get("Z07Time"));

		this.CddRateRisk = trimToEmpty(map.get("CddRateRisk"));

		this.AmlResult = trimToEmpty(map.get("AmlResult"));
		this.AmlTime = trimToEmpty(map.get("AmlTime"));
		
		this.CheckRelpResult = trimToEmpty(map.get("CheckRelpResult"));
		this.CheckRelpTime = trimToEmpty(map.get("CheckRelpTime"));

		this.CaseNoApsPrev = trimToEmpty(map.get("CaseNoApsPrev"));
	}

	public void setRejectPDFCaseData(Map<String, Object> map) {
		this.CaseNo = (String) map.get("CaseNo").toString();
		this.customer_name = (String) map.get("customer_name").toString();
		this.birthday = (String) map.get("birthday").toString();
		this.idno = (String) map.get("idno").toString();
		this.mobile_tel = (String) map.get("mobile_tel").toString();
		this.p_apy_amount = (Integer)map.get("p_apy_amount");
		// 產品代號
		this.productId = (String) map.get("ProductId").toString();
		// 處理ip 判斷是否有含「:」的ip號碼 並只取不含:後number的ip address
		String ipAddr = (String) map.get("ip_address").toString();
		if (ipAddr.indexOf(":") > -1) {
			ipAddr = ipAddr.substring(0, ipAddr.indexOf(":"));
		}
		this.ip_address = ipAddr;

		boolean hasNoCreditVerify = (map.get("UniqId") == null);

		if(hasNoCreditVerify) {
			this.applycc = false;
			this.applyccUtime = "";
		}else {
			this.applycc = true;
			this.applyccUtime = trimToEmpty(map.get("applyccUtime").toString());
		}
	}

	private String trimToEmpty(Object obj) {
		if (obj == null) {
			return "";
		}
		return StringUtils.trimToEmpty(String.valueOf(obj));
	}
}
