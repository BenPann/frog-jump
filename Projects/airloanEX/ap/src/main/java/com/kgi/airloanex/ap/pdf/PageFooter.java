package com.kgi.airloanex.ap.pdf;

import java.io.IOException;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.ap.config.AirloanEXConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @see http://hk.uwenku.com/question/p-epwhcoqq-e.html
 */
@Component
public class PageFooter extends PdfPageEventHelper {

	@Autowired
	private AirloanEXConfig globalConfig;

    public String productId = "";
    public PdfTemplate total;

    public void onEndPage(PdfWriter writer, Document document) {
        Font cFont = new Font(Font.FontFamily.UNDEFINED, 10, Font.ITALIC);
        PdfPTable table = new PdfPTable(4);
        try {
            table.setWidths(new int[] { 10, 2, 2, 10 });
            table.setTotalWidth(512);
            table.getDefaultCell().setFixedHeight(20);
            table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

            String ttfPath = globalConfig.PDF_FONT_WATER_TTF();
            BaseFont bfc;
            try {
                bfc = BaseFont.createFont(ttfPath, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                cFont = new Font(bfc, 10, Font.NORMAL);
            } catch (IOException e1) {
//                e1.printStackTrace();
            }

            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell("");
            String footerKind = globalConfig.findContract_Footer(productId);
            Phrase footer = new Phrase(footerKind, cFont);
            
            table.addCell(String.format(" %d /", writer.getPageNumber()));
            PdfPCell cell = new PdfPCell(Image.getInstance(total));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            table.addCell(footer);
            table.writeSelectedRows(0, -1, 34, 30, writer.getDirectContent());
        } catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }

    /** 
     * Creates the PdfTemplate that will hold the total number of pages. 
     * @see com.itextpdf.text.pdf.PdfPageEventHelper#onOpenDocument(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document) 
     */ 
    public void onOpenDocument(PdfWriter writer, Document document) {
        total = writer.getDirectContent().createTemplate(30, 16);
    }

    /** 
     * Fills out the total number of pages before the document is closed. 
     * @see com.itextpdf.text.pdf.PdfPageEventHelper#onCloseDocument(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document) 
     */ 
    public void onCloseDocument(PdfWriter writer, Document document) {
        ColumnText.showTextAligned(total, Element.ALIGN_LEFT, new Phrase(String.valueOf(writer.getPageNumber())), 2, 2, 0);
    }
}