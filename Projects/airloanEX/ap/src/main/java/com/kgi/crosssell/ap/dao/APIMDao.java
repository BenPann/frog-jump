package com.kgi.crosssell.ap.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.kgi.airloanex.ap.config.AirloanEXConfig;

import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class APIMDao {

    @Autowired
    AirloanEXConfig globalConfig;

	// 特殊案例 有空再整理
	public String queryAPIM(String ap, String linkName, String queryJson, Map<String, String> optionMap)
			throws IOException {
		CloseableHttpClient client = HttpClients.createDefault();
		String url = ap.concat(linkName);
		if (optionMap != null) {
			// Url特殊處理 不經過加密
			int i = 0;
			for (Map.Entry<String, String> entry : optionMap.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();
				if (i == 0) {
					url = url.concat("?");
				} else {
					url = url.concat("&");
				}
				url = url.concat(key).concat("=").concat(value);
				i++;
			}
		}

		HttpPost clientPost = new HttpPost(url);
		StringEntity entity = new StringEntity(queryJson, "UTF-8");

		// 文字內容先做MD5 + base64
		String md5Content = base64(md5(queryJson));

		System.out.println("#### AWSinfo - url = " + url);
		System.out.println("#### AWSinfo - 原始資料(in UTF-8) = " + queryJson);
		System.out.println("#### AWSinfo - md5Content = base64(md5(queryJson))) = " + md5Content);

		// clientPost.addHeader("Charset", "UTF-8");
		// clientPost.addHeader("Accept-Charset", "UTF-8");
		clientPost.addHeader("Content-Type", "application/json");
		clientPost.addHeader("Accept", "application/json");
		clientPost.addHeader("Content-MD5", md5Content);

		// 取特殊日期
		Date current = new Date();
		// SimpleDateFormat sdf = new java.text.SimpleDateFormat("E, dd MMM yyyy
		// HH:mm:ss Z", Locale.UK);
		// sdf.setTimeZone(TimeZone.getTimeZone("Europe/Ghana"));
		SimpleDateFormat sdf = new java.text.SimpleDateFormat("E, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
		String date = sdf.format(current);
		clientPost.addHeader("Date", date);

		System.out.println("#### AWSinfo - linkName = " + linkName);
		System.out.println("#### AWSinfo - Date = " + date);

		String signature = Encryption(md5Content, linkName, date);

		System.out.println("#### AWSinfo - signature = base64(hmacSha1(post, secret)) = " + signature);

		String apiKey = globalConfig.APIM_CrossSell_ApiKey();

		System.out.println("#### AWSinfo - apiKey = " + apiKey);

		String authorization = "AWS " + apiKey + ":" + signature;

		System.out.println("#### AWSinfo - authorization = " + authorization);

		clientPost.addHeader("Authorization", authorization);

		System.out.println("#### AWSinfo clientPost.addHeader \"Content-Type\", \"application/json\"");
		System.out.println("#### AWSinfo clientPost.addHeader \"Accept\", \"application/json\"");
		System.out.println("#### AWSinfo clientPost.addHeader \"Content-MD5\"," + md5Content);
		System.out.println("#### AWSinfo clientPost.addHeader \"Date\", " + date);
		System.out.println("#### AWSinfo clientPost.addHeader \"Authorization\", " + authorization);

		clientPost.setEntity(entity);

		CloseableHttpResponse response = client.execute(clientPost);

		// 2. 檢查是否收到正常回傳,回傳資訊。
		StatusLine sl = response.getStatusLine();

		System.out.println("#### getStatusCode()=" + sl.getStatusCode());

		if (sl.getStatusCode() == 200) {
			StringBuilder sb = new StringBuilder();

			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));

			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();

			return sb.toString();
		} else {
			System.out.println("呼叫失敗! Status Code : " + sl.getStatusCode());
			throw new RuntimeException("呼叫 Server失敗!!");
		}
	}

	// 1. 連線用
	public String queryAPI(String ap, String linkName, String queryJson) throws IOException {
		return queryAPIM(ap, linkName, queryJson, null);
	}

	// 2. 加密用
	public String Encryption(String content, String linkName, String date) {
		// base64(hmac-sha1(VERB + "\n"
		// + CONTENT-MD5 + "\n"
		// + CONTENT-TYPE + "\n"
		// + DATE + "\n"
		// + CanonicalizedAmzHeaders + "\n"
		// + CanonicalizedResource))
		// Thu, 17 Nov 2005 18:49:58

		String post = "POST\n" + content + "\n" + "application/json" + "\n" + date + "\n" + linkName;

		String secret = globalConfig.APIM_CrossSell_SecretKey();

		System.out.println("#### AWSinfo - post = \n" + post);
		System.out.println("#### AWSinfo - secret = " + secret);

		// 先做hmacSha1再轉base64
		return base64(hmacSha1(post, secret));
	}

	// HMAC-SHA1
	public byte[] hmacSha1(String value, String key) {
		try {
			// Get an hmac_sha1 key from the raw key bytes
			byte[] keyBytes = key.getBytes();
			SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");

			Mac mac = Mac.getInstance("HmacSHA1");
			mac.init(signingKey);
			return mac.doFinal(value.getBytes());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public byte[] md5(String str) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			return md.digest(str.getBytes()); // 將 byte 陣列加密
		} catch (Exception ex) {
			System.out.println("錯誤的驗算法名稱");
			return null;
		}
	}

	public String base64(byte[] source) {
		Base64.Encoder encoder = Base64.getEncoder();
		return encoder.encodeToString(source);
	}

}
