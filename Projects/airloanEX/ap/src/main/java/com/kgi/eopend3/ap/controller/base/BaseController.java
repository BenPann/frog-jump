package com.kgi.eopend3.ap.controller.base;

import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;
import com.kgi.eopend3.ap.exception.ErrorResultException;
import com.kgi.eopend3.common.SystemConst;
import com.kgi.eopend3.common.dto.KGIHeader;
import org.owasp.esapi.AccessReferenceMap;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;
import org.owasp.esapi.reference.RandomAccessReferenceMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseController {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    public KGIHeader getHeader(HttpServletRequest request) {
        Gson gson = new Gson();
        String header1 = request.getHeader(SystemConst.KGIHEADER);
        logger.info("### header1 = " + header1);
        AccessReferenceMap<String> map = new RandomAccessReferenceMap();
        String indirect_apiName = map.addDirectReference(header1);
        try {
            header1 = map.getDirectReference(indirect_apiName);
        } catch (AccessControlException e) {
            e.printStackTrace();
            header1 = "";
        }
        KGIHeader header = null;
        if (ESAPI.validator().isValidInput("BaseController", header1, "SafeJson", Integer.MAX_VALUE, false)) {
            header = gson.fromJson(header1, KGIHeader.class);            
            return header;
        }else{
            throw new ErrorResultException(9, "Invalid Input", null);
        }
    }

}