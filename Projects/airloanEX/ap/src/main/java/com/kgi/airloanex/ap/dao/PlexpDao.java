package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.Plexp;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class PlexpDao extends CRUDQDao<Plexp> {
	public static final String NAME = "PLExp" ;

    public Plexp findByCaseNo(String caseNo) {
        Plexp rtn;
		String sql = "SELECT * FROM " + NAME + " WHERE CaseNo = ?";					
		try {
            List<Plexp> list = this.getJdbcTemplate().query(sql, new Object[] { caseNo }, new BeanPropertyRowMapper<>(Plexp.class));
            if (list != null && list.size() > 0) {
                rtn = list.get(0);
            } else {
                rtn = null;
            }
        } catch (DataAccessException ex) {
            rtn = null;
        }
        return rtn;
    }

    public void UpdateByCaseNoPhone(String caseNo,String phone){
        String sql = "UPDATE " + NAME + " SET Phone= :phone WHERE CaseNo= :caseNo ";
        System.out.println("PlexpDao,UpdateByCaseNoPhone,35 = "+ sql);
        Plexp plexp =new Plexp();
        plexp.setPhone(phone);
        plexp.setCaseNo(caseNo);
        this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(plexp));
    }

    @Override
    public int Create(Plexp fullItem) {
        String sql = "INSERT INTO "
                + NAME + "( ExpNo, Phone, Date, Name, CaseNoWeb, CaseNo, QVer, AnsList, ProjectCode, CreditLine, IntrestRate, Rank, Score, UTime,      ExpProductType,  Sex,  age,  edu,  marriage, jobType,   workyear,  yearincome,  IPAddress)"
                + " VALUES(:ExpNo, '' ,   :Date, '',  :CaseNoWeb,:CaseNo, '',  :AnsList,:ProjectCode,:CreditLine,:IntrestRate,:Rank,:Score, getdate(), :ExpProductType, :Sex, :age, :edu, :marriage, :jobType, :workyear, :yearincome, :IPAddress)";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public Plexp Read(Plexp keyItem) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int Update(Plexp fullItem) {
        String sql = "UPDATE " + NAME + " SET Phone=:Phone,Date=:Date,Name=:Name,CaseNoWeb=:CaseNoWeb,CaseNo=:CaseNo,QVer=:QVer,AnsList=:AnsList,ProjectCode=:ProjectCode,CreditLine=:CreditLine,IntrestRate=:IntrestRate,Rank=:Rank,Score=:Score,ExpProductType=:ExpProductType,Sex=:Sex,age=:age,edu=:edu,marriage=:marriage,jobType=:jobType,workyear=:workyear,yearincome=:yearincome,IPAddress=:IPAddress WHERE ExpNo=:ExpNo ";
        return this.getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(fullItem));
    }

    @Override
    public int Delete(Plexp keyItem) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public List<Plexp> Query(Plexp keyItem) {
        // TODO Auto-generated method stub
        return null;
    }

}
