package com.kgi.airloanex.ap.service;

import com.kgi.airloanex.ap.dao.SOAPDao;
import com.kgi.airloanex.common.dto.db.MailHistory;
import com.kgi.airloanex.common.dto.soap.SendMsgSoapParameter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SOAPService {
    private final String MAILTYPE = "3";
    @Autowired
    SOAPDao SOAPDao;

    /**
     * @param idNo 預核-進件訊息
     */
    public MailHistory getApplyLoanCMBProgress(String idno, String cname, String hasdata, int creditLine, String plusMessage,String branchId) throws Exception {

        //組訊息參數值
//        String cTobranch = "9743";
        String cTobranch = branchId;
        String cTitle = "數位進件";
        String iMessageColor = "1";
        // \n為訊息制定欄位換行符號
        String cMessage = "ID:".concat(idno).concat("\n").concat("姓名:").concat(cname);
        if(plusMessage !=null && !plusMessage.equals("")){
            cMessage= cMessage.concat(plusMessage);
        }
        if (creditLine <= 0) {
            cMessage = cMessage.concat("\n").concat("APS可貸金額為0");
        }
        if (hasdata.equals("1")) {
            cMessage = cMessage.concat("[中華電信專案]");
        }
        SendMsgSoapParameter soapParameter = new SendMsgSoapParameter(cTobranch, cTitle, iMessageColor, cMessage);
        String CMBRequestStr = getCMBRequestStr(soapParameter);
        MailHistory mailHistory = setSoapMailHistory(cMessage, CMBRequestStr);


        return mailHistory;
    }

//	public String getApplyLoanNoCreditLineCMBProgress(String idno, String cname,String hasdata) throws Exception{
//
//		//組訊息參數值
//		String cTobranch = "9743";
//	    String cTitle = "數位進件";
//	    String iMessageColor = "1";
//	 // \n為訊息制定欄位換行符號
//	    String cMessage = "ID:".concat(idno).concat("\n").concat("姓名:").concat(cname).concat("\n").concat("APS可貸金額為0");
//	    if(hasdata.equals("1")){
//	    	cMessage =cMessage.concat("[中華電信專案]");
//	    }
//	    SendMsgSoapParameter soapParameter = new SendMsgSoapParameter(cTobranch, cTitle, iMessageColor, cMessage);
//		String CMBRequestStr = getCMBRequestStr(soapParameter);
//		return SOAPDao.getCMBProgress(CMBRequestStr);
//    }

    public MailHistory getContractCMBProgress(String idno, String cname, String branchId) throws Exception {

        //組訊息參數值
        // String cTobranch = "9743";
        String cTobranch = branchId;
        String cTitle = "數位進件";
        String iMessageColor = "1";
        // \n為訊息制定欄位換行符號
        String cMessage = "ID:".concat(idno).concat("\n").concat("姓名:").concat(cname).concat("\n契約書");
        SendMsgSoapParameter soapParameter = new SendMsgSoapParameter(cTobranch, cTitle, iMessageColor, cMessage);
        String CMBRequestStr = getCMBRequestStr(soapParameter);
        MailHistory mailHistory = setSoapMailHistory(cMessage, CMBRequestStr);

        return mailHistory;
    }

    /**
     * @param idNo 非預核-拒貸訊息
     */
    public MailHistory getRejectCaseCMBProgress(String idno, String cname,String branchId) throws Exception {

        //組訊息參數值
//        String cTobranch = "9743";
        String cTobranch = branchId;
        String cTitle = "數位進件";
        String iMessageColor = "1";
        // \n為訊息制定欄位換行符號
        String cMessage = "拒貸".concat("\n").concat("ID:").concat(idno).concat("\n").concat("姓名:").concat(cname);
        SendMsgSoapParameter soapParameter = new SendMsgSoapParameter(cTobranch, cTitle, iMessageColor, cMessage);
        String CMBRequestStr = getCMBRequestStr(soapParameter);

        MailHistory mailHistory = setSoapMailHistory(cMessage, CMBRequestStr);
        return mailHistory;
    }

    /**
     * @param idNo 預核-補件訊息
     */
    public MailHistory getAddPhotoCMBProgress(String idno, String cname, String branchID) throws Exception {

        //組訊息參數值
        String cTobranch = branchID;
        String cTitle = "數位補件";
        String iMessageColor = "1";
        // \n為訊息制定欄位換行符號
        String cMessage = "ID:".concat(idno).concat("\n").concat("姓名:").concat(cname).concat("\n").concat("文件類別:新進件補件");
        SendMsgSoapParameter soapParameter = new SendMsgSoapParameter(cTobranch, cTitle, iMessageColor, cMessage);
        String CMBRequestStr = getCMBRequestStr(soapParameter);
        MailHistory mailHistory = setSoapMailHistory(cMessage, CMBRequestStr);
        return mailHistory;
    }

    /**
     * 信用卡補件
     * @param idno
     * @param cname
     * @param branchId
     * @return
     * @throws Exception
     */
    public MailHistory getCreditAddPhotoCMBProgress(String idno, String cname, String branchId) throws Exception {

        //組訊息參數值
        String cTobranch = branchId;
        String cTitle = "信用卡申請";
        String iMessageColor = "1";
        // \n為訊息制定欄位換行符號
        String cMessage = "ID:".concat(idno).concat("\n").concat("姓名:").concat(cname).concat("\n").concat("文件類別:新進件補件");
        SendMsgSoapParameter soapParameter = new SendMsgSoapParameter(cTobranch, cTitle, iMessageColor, cMessage);

        String CMBRequestStr = getCMBRequestStr(soapParameter);
        MailHistory mailHistory = setSoapMailHistory(cMessage, CMBRequestStr);
        return mailHistory;
    }


    /**
     * 信用卡送件
     * @param idno
     * @param cname
     * @param branchId
     * @return
     * @throws Exception
     */
    public MailHistory getCreditCMBProgress(String idno, String cname, String branchId,String plusMessage) throws Exception {

        //組訊息參數值
        // String cTobranch = "9743";
        String cTobranch = branchId;
        String cTitle = "信用卡申請";
        String iMessageColor = "1";
        // \n為訊息制定欄位換行符號
        String cMessage = "ID:".concat(idno).concat("\n").concat("姓名:").concat(cname);
        if(plusMessage!=null && !plusMessage.equals("")){
            cMessage = cMessage.concat(plusMessage);
        }

        SendMsgSoapParameter soapParameter = new SendMsgSoapParameter(cTobranch, cTitle, iMessageColor, cMessage);

        String CMBRequestStr = getCMBRequestStr(soapParameter);
        MailHistory mailHistory = setSoapMailHistory(cMessage, CMBRequestStr);
        return mailHistory;
    }


    private String getCMBRequestStr(SendMsgSoapParameter soapParameter) {

        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        sb.append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        sb.append("<soap:Body>");
        sb.append("<SendMessage xmlns=\"http://tempuri.org/\">");
        sb.append("<cToBranch>" + soapParameter.cTobranch + "</cToBranch>");
        sb.append("<cTitle>" + soapParameter.cTitle + "</cTitle>");
        sb.append("<iMessageColor>" + soapParameter.iMessageColor + "</iMessageColor>");
        sb.append("<cMessage>" + soapParameter.cMessage + "</cMessage>");
        sb.append("</SendMessage>");
        sb.append("</soap:Body>");
        sb.append("</soap:Envelope>");
        return sb.toString();
    }

    private MailHistory setSoapMailHistory(String cMessage, String CMBRequestStr) {
        MailHistory mailHistory = MailHistory.getInstance().setMailType(MAILTYPE).setTitle(cMessage).setContent(CMBRequestStr).setStatus("0");
        return mailHistory;
    }

    public String sendSoap(MailHistory mailHistory) throws Exception {
        String reuslt = SOAPDao.getCMBProgress(mailHistory.getContent());
        return reuslt;
    }
}
