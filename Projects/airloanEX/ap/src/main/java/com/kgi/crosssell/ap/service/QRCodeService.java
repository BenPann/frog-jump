package com.kgi.crosssell.ap.service;

import java.util.List;

import com.kgi.airloanex.ap.dao.QRCodeDao;
import com.kgi.airloanex.ap.dao.QR_PromoCaseUserInputDao;
import com.kgi.airloanex.ap.dao.QR_ShortUrlDao;
import com.kgi.airloanex.common.dto.db.ChannelData;
import com.kgi.airloanex.common.dto.db.QR_PromoCaseUserInput;
import com.kgi.eopend3.common.dto.db.QR_ShortUrl;
import com.kgi.eopend3.common.dto.db.QR_ShortUrlCaseMatch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QRCodeService {
	
	@Autowired
	private QRCodeDao qrCodeDao;

	@Autowired
	private QR_ShortUrlDao qrShortDao;
	
	@Autowired
	private QR_PromoCaseUserInputDao promoCaseUserInputDao;

	public ChannelData getChannelData(String shortUrl) {
		return qrCodeDao.getChannelData(shortUrl);
	}

	public void updateShortUrlCount(String shortUrl) {
		qrCodeDao.updateShortUrlCount(shortUrl);
	}

	public boolean isProducdId(String st) {
		// TODO Auto-generated method stub
		return qrCodeDao.isProducdId(st);
	}
	
	public void createUrlCaseMatch(String shortUrl, String uniqId, String uniqType) {
		
		List<QR_ShortUrlCaseMatch> check =  qrCodeDao.queryQR_ShortUrlCaseMatchByUniqId(uniqId);
		if(check ==null || check.size() ==0  ) {
			qrCodeDao.createUrlCaseMatch(shortUrl, uniqId, uniqType);
		}
		// 3/12 因應斷點報表修正 當qrcode進入時 且 departId 非空值 先寫 QR_promocase和userinputtdas
		QR_ShortUrl url = new QR_ShortUrl(shortUrl); 
		url = qrShortDao.Read(url);
		if(url!=null&&!url.getDepartId().equals("")) {
			QR_PromoCaseUserInput input = new QR_PromoCaseUserInput(uniqId);
	        input.setUniqType(uniqType);
	        input.setPromoDepart(url.getDepartId());
	        input.setPromoMember(url.getMember());
	        input.setChannelId(url.getChannelId());
	        QR_PromoCaseUserInput inputReading = promoCaseUserInputDao.Read(input);
	        if(null == inputReading)
	        {
	        	promoCaseUserInputDao.Create(input);
	        }
		}		
	}

}
