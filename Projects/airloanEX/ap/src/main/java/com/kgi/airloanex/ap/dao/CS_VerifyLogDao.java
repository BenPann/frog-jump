package com.kgi.airloanex.ap.dao;

import java.util.List;

import com.kgi.airloanex.common.dto.db.CS_VerifyLog;
import com.kgi.eopend3.ap.dao.CRUDQDao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class CS_VerifyLogDao extends CRUDQDao<CS_VerifyLog> {

	@Override
	public int Create(CS_VerifyLog fullItem) {
		String sql = "INSERT INTO CS_VerifyLog VALUES(?,?,?,?,?,?,?,getdate())";		
        return this.getJdbcTemplate().update(sql,
                new Object[] { fullItem.getUniqId(), fullItem.getUniqType(), fullItem.getIdno(),
                        fullItem.getIpAddress(), fullItem.getTime(), fullItem.getMessageSerial(), 
                        fullItem.getContractVersion() });
	}

	@Override
	public CS_VerifyLog Read(CS_VerifyLog keyItem) {
		String sql = "SELECT * FROM CS_VerifyLog WHERE UniqId = ? AND 1=1;";
        try {
            return this.getJdbcTemplate().queryForObject(sql, new BeanPropertyRowMapper<>(CS_VerifyLog.class),
                    new Object[] { keyItem.getUniqId() });
        } catch (DataAccessException ex) {
            System.out.println("CS_VerifyLog查無資料");            
            return null;
        }
	}

	@Override
	public int Update(CS_VerifyLog fullItem) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int Delete(CS_VerifyLog keyItem) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<CS_VerifyLog> Query(CS_VerifyLog keyItem) {
		// TODO Auto-generated method stub
		return null;
	}

}
