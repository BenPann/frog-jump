package com.kgi.airloanex.ap.controller;

import com.kgi.airloanex.ap.service.ChannelDepartService;
import com.kgi.eopend3.ap.controller.base.BaseController;
import com.kgi.eopend3.common.dto.WebResult;

import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/channel")
public class ChannelDepartController extends BaseController{
    
    @Autowired
    ChannelDepartService channelDepartService;

    @PostMapping("/qrshorturl")
    public String qrcode(@RequestBody String reqBody) {
        logger.info("前台輸入的資料" + reqBody);
        if(ESAPI.validator().isValidInput("ChannelDepartController", reqBody, "SafeJson", Integer.MAX_VALUE, false)){
            return channelDepartService.qrshorturl(reqBody);
        }else{
            return WebResult.GetFailResult();
        }
    }

}
