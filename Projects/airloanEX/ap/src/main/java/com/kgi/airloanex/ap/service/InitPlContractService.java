package com.kgi.airloanex.ap.service;

import static com.kgi.airloanex.common.PlContractConst.CASE_STATUS_WRITE_SUCCESS;
import static com.kgi.airloanex.common.PlContractConst.CASE_STATUS_WRITING;
import static com.kgi.airloanex.common.PlContractConst.CONT;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.kgi.airloanex.ap.config.AirloanEXConfig;
import com.kgi.airloanex.ap.dao.ContractApiLogDao;
import com.kgi.airloanex.ap.dao.ContractMainActInfoDao;
import com.kgi.airloanex.ap.dao.ContractMainDao;
import com.kgi.airloanex.ap.dao.WhiteListDao;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.PlContractConst.RouteGo;
import com.kgi.airloanex.common.domain.UpdateDgtConstDomain;
import com.kgi.airloanex.common.dto.customDto.GetCifInfoRspDto;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDto;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoConstData;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoDepData;
import com.kgi.airloanex.common.dto.customDto.GetDgtConstRspDtoRateData;
import com.kgi.airloanex.common.dto.customDto.GetExceedStatusRspDto;
import com.kgi.airloanex.common.dto.customDto.GetOtpTelRspDto;
import com.kgi.airloanex.common.dto.customDto.GetOtpTelStpRspDto;
import com.kgi.airloanex.common.dto.customDto.GetOtpTelStpRspDtoOtpData;
import com.kgi.airloanex.common.dto.customDto.UpdateDgtConstReqDto;
import com.kgi.airloanex.common.dto.customDto.UpdateDgtConstRspDto;
import com.kgi.airloanex.common.dto.db.ContractMain;
import com.kgi.airloanex.common.dto.db.ContractMainActInfo;
import com.kgi.airloanex.common.dto.db.WhiteList;
import com.kgi.airloanex.common.dto.response.CaseResp;
import com.kgi.airloanex.common.dto.response.ChosenDgtConstResp;
import com.kgi.airloanex.common.dto.response.GetCifInfoResp;
import com.kgi.airloanex.common.dto.response.GetDgtConstResp;
import com.kgi.airloanex.common.dto.response.GetOtpTelResp;
import com.kgi.airloanex.common.dto.response.InitBankInfo;
import com.kgi.airloanex.common.dto.response.KgiBankInfo;
import com.kgi.airloanex.common.dto.response.LoginResp;
import com.kgi.airloanex.common.dto.response.PrepareDgtConstResp;
import com.kgi.airloanex.common.dto.response.RateData;
import com.kgi.airloanex.common.dto.view.AgreePlContractView;
import com.kgi.airloanex.common.dto.view.ChosenDgtConstView;
import com.kgi.airloanex.common.dto.view.ExpBankView;
import com.kgi.airloanex.common.dto.view.InfoPlContractView;
import com.kgi.airloanex.common.dto.view.LoginView;
import com.kgi.eopend3.ap.dao.ConfigDao;
import com.kgi.eopend3.ap.dao.EOP_IPLogDao;
import com.kgi.eopend3.ap.dao.EntryDataDao;
import com.kgi.eopend3.ap.exception.ErrorResultException;
//import com.kgi.eopend3.ap.service.AccountInfoService;
import com.kgi.eopend3.ap.util.GsonUtil;
import com.kgi.eopend3.common.dto.WebResult;
import com.kgi.eopend3.common.dto.db.EOP_IPLog;
import com.kgi.eopend3.common.dto.db.EntryData;
import com.kgi.eopend3.common.util.CheckUtil;
import com.kgi.eopend3.common.util.DateUtil;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InitPlContractService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ContractMainDao contractMainDao;

    @Autowired
    private ContractMainActInfoDao contractMainActInfoDao;

    @Autowired
    private SrnoService snSer;

    @Autowired
    private EntryDataDao entryDataDao;

    @Autowired
    private EOP_IPLogDao eopIPLogDao;

    @Autowired
    private WhiteListDao whiteListDao;

    // @Autowired
    // private AccountInfoService accountInfoService;

    @Autowired
    private KgiService kgiService;

    @Autowired
    private ConfigDao configDao;

    @Autowired
    private AirloanEXConfig globalConfig;

    @Autowired
    private ContractApiLogDao contractApiLogDao;

    @Autowired
	private ContractDayService contractDayService;

    private String isWhite = "";

    public String initPlContract(String reqJson) {
        try {
            LoginView login = new Gson().fromJson(reqJson, LoginView.class);

            if (!CheckUtil.check(login)) {
                throw new ErrorResultException(1,
                        configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
            } else {
                String uniqType = login.getUniqType();
                String entry = login.getEntry();
                String entryOther = login.getEntryOther();
                String idno = login.getIdno();
                String ipAddress = login.getIpAddress();
                String birthday = login.getBirthday();
                String browser = login.getBrowser();
                String platform = login.getPlatform();
                String os = login.getOs();

                RouteGo routeGo = RouteGo.initPlContract;
                /**/

                // 創立案件編號
                String uniqId = snSer.getUniqId();
                //
                LoginResp loginResp = new LoginResp();
                loginResp.setUniqId(uniqId);
                loginResp.setUniqType(uniqType);
                loginResp.setIdno(idno);
                loginResp.setIpAddress(ipAddress);

                // 寫入entry資料
                EntryData entryData = new EntryData(uniqId, uniqType, entry, CONT, browser, platform, os);
                entryData.setOther(entryOther);
                entryDataDao.Create(entryData);

                // 白名單
                int pType = 0;
                int sType = 0;
                WhiteList whiteList = new WhiteList();
                whiteList.setID(idno);
                List<WhiteList> list = whiteListDao.Query(whiteList);
                for (WhiteList resultWhiteList : list) {
                    pType = resultWhiteList.getPType();
                    sType = resultWhiteList.getSType();
                    logger.info("### pType = > " + pType + "  ;sType = > " + sType);
                }

                // [判斷一]尚未驗身前呼叫取得立約電文API-01：
                // 1. GET_DGT_CONST（ID,1） 是否有個人信貸（PL）簽約資訊
                // 2. GET_DGT_CONST（ID,3） 是否有循環信貸（RPL）簽約資訊
                // 3. GET_DGT_CONST（ID,2） 是否有現金卡簽約資訊

                GetDgtConstRspDto getDgtConstRspDto1;
                GetDgtConstRspDto getDgtConstRspDto3;
                GetDgtConstRspDto getDgtConstRspDto2;
                logger.info("### isWhite >>" + isWhite);
                // 白名單 pType == 2 立約
                if (pType == 2 && sType == 1) {
                    isWhite = "Y";
                    logger.info("### idno = > " + idno + "  此ID為白名單; isWhite >>" + isWhite);
                    getDgtConstRspDto1 = kgiService.getWhiteDgtConst(uniqId, idno, "1");
                    getDgtConstRspDto2 = kgiService.getWhiteDgtConst(uniqId, idno, "2");
                    getDgtConstRspDto3 = kgiService.getWhiteDgtConst(uniqId, idno, "3");
                } else {
                    isWhite = "N";
                    logger.info("### idno = > " + idno + "  call_GET_DGT_CONST; isWhite>>" + isWhite);
                    getDgtConstRspDto1 = kgiService.call_GET_DGT_CONST(uniqId, idno, "1");
                    getDgtConstRspDto3 = kgiService.call_GET_DGT_CONST(uniqId, idno, "3");
                    getDgtConstRspDto2 = kgiService.call_GET_DGT_CONST(uniqId, idno, "2");
                }

                // GetCifInfoResp getCifInfoResp = new GetCifInfoResp();
                GetOtpTelResp getOtpTelResp = new GetOtpTelResp();
                InitBankInfo initBankInfo = new InitBankInfo();

                // 如果上述三者皆回無簽約資訊，則跳出MSG model提示：「您無任何可供線上簽約之案件或申請案件尚未達立約階段」，並停留於登入頁即可。
                // 如果有任一可簽約資訊，則接[判斷二]。
                if (!StringUtils.equals("", getDgtConstRspDto1.getCHECK_CODE())
                        && !StringUtils.equals("", getDgtConstRspDto3.getCHECK_CODE())
                        && !StringUtils.equals("", getDgtConstRspDto2.getCHECK_CODE())) {
                    routeGo = RouteGo.initPlContract;
                    CaseResp resp = new CaseResp(loginResp, routeGo, initBankInfo, new ArrayList<>());
                    return WebResult.GetResultString(-90,
                            configDao.ReadConfigValue("CONT_Err_GET_DGT_CONST", "系統問題，如有任何需要協助，請洽客服人員"), resp);
                }
                // --------------------------------------------------------------------
                // [判斷二]驗身判斷新舊戶並取得OTP TEL：
                // "0":驗證失敗，"":驗證成功(舊戶)，"10":新戶
                // @call 1. GET_OTP_TEL_STP
                // GetOtpTelStpRspDto getOtpTelRspDto = null;

                // try {
                //     getOtpTelRspDto = kgiService.call_GET_OTP_TEL_STP(uniqId, idno, birthday);
                // } catch (Exception e) {
                //     logger.error("未處理的錯誤", e);
                //     return WebResult.GetResultString(99, "系統問題，如有任何需要協助，請洽客服人員(GET_OTP_TEL_STP)", null);
                // }

                // // logger.info("### GET_OTP_TEL_STP: " + new Gson().toJson(getOtpTelRspDto));

                // boolean isNewOne = false; // 新戶
                // boolean isOldOne = false; // 既有戶
                // // "0":驗證失敗
                // if (StringUtils.equals("0", getOtpTelRspDto.getCHECK_CODE())) {
                //     logger.info("### GET_OTP_TEL_STP 驗證失敗");
                //     return WebResult.GetResultString(99, getOtpTelRspDto.getERR_MSG(), null);
                // } else { // "":驗證成功(舊戶)
                //     if (StringUtils.equals("", getOtpTelRspDto.getCHECK_CODE())) {
                //         if (has_OTP_TEL(getOtpTelRspDto.getOTP_DATA())) {
                //             isOldOne = true;
                //         } else {
                //             isNewOne = true;    
                //         }
                //     } // "10":新戶
                //     else if (StringUtils.equals("10", getOtpTelRspDto.getCHECK_CODE())) {
                //         isNewOne = true;
                //     } else {
                //         logger.info("### 異常不是新戶或既有戶");
                //         return WebResult.GetResultString(99, configDao.ReadConfigValue("CONT_Err_GETAPI", "系統問題，如有任何需要協助，請洽客服人員"), null);
                //     }
                // }
                // logger.info(isNewOne ? "### 新戶" : "");
                // logger.info(isOldOne ? "### 既有戶" : "");
                // String otpTel = find_OTP_TEL(getOtpTelRspDto.getOTP_DATA());
                // --------------------------------------------------------------------
                // [判斷二]驗身判斷新舊戶並取得OTP TEL：
                // GET_OTP_TEL GET_CIF_INFO 客戶類型
                // A. CheckCode=’’ and 手機號碼為空 不用呼叫 既有戶，MSG model提示訊息「未於本行留存有效手機」
                // B. CheckCode=’’ and 有回手機號碼 要呼叫取得留存Email 既有戶，往後進入OTP驗證
                // C. CheckCode=’10’ 不用呼叫 新戶，往後進入驗身選擇頁
                // otpTel資料
                GetOtpTelRspDto getOtpTelRspDto = new GetOtpTelRspDto();
                getOtpTelRspDto = kgiService.call_GET_OTP_TEL(uniqId, idno, birthday);
                String otpTel = getOtpTelRspDto.getOTP_TEL();
                // --------------------------------------------------------------------
                getOtpTelResp.setOtpTel(otpTel);

                // cif資料
                GetCifInfoResp cifInfoResp = null;
                // 戶別
                String userType = null; // 0: 新戶 1:信用卡戶 2:存款戶 3:純貸款戶
                //
                Integer status;
                String message;

                // A. CheckCode=’’ 既有戶
                if (StringUtils.equals("", getOtpTelRspDto.getCHECK_CODE())) {
                    // A.1 既有戶 手機號碼為空，停留於init頁面 MSG model提示訊息「未於本行留存有效手機」
                    if (StringUtils.isBlank(otpTel)) {

                        userType = "2"; // 2:存款戶

                        // goto init頁
                        routeGo = RouteGo.initPlContract;

                        status = 99;
                        message = configDao.ReadConfigValue("", "於行內未留存有效手機號碼");
                    }
                    // A.2 既有戶 有回手機號碼 要呼叫取得留存Email 既有戶，往後進入OTP驗證
                    else {

                        userType = "2"; // 2:存款戶

                        // 取得cif資料 最主要目的是要知道身分別 既有卡戶等
                        cifInfoResp = getCifInfo(uniqId, idno, birthday); // 主要目標：取得EMAIL
                        // goto OTP頁
                        routeGo = RouteGo.otpPlContract;

                        status = 0;
                        message = "成功";
                    }
                }
                // B. CheckCode=’10’ 新戶，往後進入驗身選擇頁
                else if (StringUtils.equals("10", getOtpTelRspDto.getCHECK_CODE())) {

                    userType = "0"; // 0: 新戶

                    // goto 驗身選擇頁
                    // 20210506: 現在只有一種驗身方式，故不用讓使用者選擇，直接導頁 By Kenny
                    routeGo = RouteGo.identifyByBankAccountPlContract;
//                    routeGo = RouteGo.chooseIdentificationStylePlContract;

                    status = 0;
                    message = "成功";
                }
                // CheckCode=’20’ 既有戶，系統回傳的手機號碼格式有誤
                else if (StringUtils.equals("20", getOtpTelRspDto.getCHECK_CODE())) {

                    userType = "2"; // 2:存款戶

                    // goto init頁
                    routeGo = RouteGo.initPlContract;

                    status = 99;
                    message = configDao.ReadConfigValue("", "於行內未留存有效手機號碼");
                } else if (StringUtils.equals("0", getOtpTelRspDto.getCHECK_CODE())) {

                    userType = null; // 無法判斷戶別

                    // goto init頁
                    routeGo = RouteGo.initPlContract;

                    status = 99;
                    message = configDao.ReadConfigValue("", "生日檢核不相符");
                }
                // C. CheckCode 不包含空值及10 ，停留於init頁面
                else {

                    userType = null; // 無法判斷戶別

                    // goto 登入頁
                    routeGo = RouteGo.initPlContract;

                    status = 99;
                    message = configDao.ReadConfigValue("CONT_Err_GETAPI", "系統問題，如有任何需要協助，請洽客服人員");
                }

                if (StringUtils.isNotBlank(otpTel)) {
                    initBankInfo.setPhone(otpTel);
                }

                if (cifInfoResp != null && StringUtils.isNotBlank(cifInfoResp.getEmail())) {
                    initBankInfo.setEmail(cifInfoResp.getEmail());
                }

                // 從GET_DGT_CONST取撥款及繳款的銀行代碼跟帳號

                GetDgtConstRspDto getDgtConst = null;
                if (StringUtils.equals("", getDgtConstRspDto1.getCHECK_CODE())) {
                    getDgtConst = getDgtConstRspDto1;
                } else if (StringUtils.equals("", getDgtConstRspDto2.getCHECK_CODE())) {
                    getDgtConst = getDgtConstRspDto2;
                } else if (StringUtils.equals("", getDgtConstRspDto3.getCHECK_CODE())) {
                    getDgtConst = getDgtConstRspDto3;
                }

                // initBankInfo, initRateDatas
                if (getDgtConst != null) {
                    initBankInfo(uniqId, initBankInfo, getDgtConst);
                }
                List<RateData> rateDatas = initRateDatas(getDgtConst);

                // if (StringUtils.isNotBlank(initBankInfo.getPayBankNo()) &&
                // StringUtils.isNotBlank(initBankInfo.getPayAccount()) &&
                // StringUtils.isNotBlank(getOtpTelResp.getOtpTel())) {
                // //
                // routeGo = RouteGo.identifyByBankAccountPlContract;
                // }

                // add ContractMain
                ContractMain contractMain = new ContractMain(uniqId);
                // contractMain.setGender(idno.substring(1, 2));
                contractMain.setStatus(PlContractConst.CASE_STATUS_INIT);
                // 撥款為本行他行 0:本行 1:它行 null:它行
                contractMain.setPayBankOption(initBankInfo.getPayBankOption());
                // contractMain.setAccountType(bns85081.getAcctType());
                // contractMain.setSubCategory(bns85081.getIntCat());
                contractMain = GsonUtil.merge(contractMain, login, ContractMain.class, true);
                contractMain.setUserType(userType); // 0: 新戶 1:信用卡戶 2:存款戶 3:純貸款戶
                contractMain.setEmail((cifInfoResp != null) ? cifInfoResp.getEmail() : null);
                contractMain.setNotify("01"); // 01:簡訊 02:書面
                contractMain.setCourt("臺北");
                contractMainDao.Create(contractMain);

                // add ContractMainActInfo
                contractMainActInfoDao.touch(new ContractMainActInfo(uniqId));

                // EOP_IPLog
                EOP_IPLog eop_IPLog = new EOP_IPLog(uniqId, ipAddress);
                eopIPLogDao.Create(eop_IPLog);

                //
                CaseResp resp = new CaseResp(loginResp, routeGo, initBankInfo, rateDatas);

                return WebResult.GetResultString(status, message, resp);

                //
                // LoginResp loginResp = new LoginResp();
                // loginResp.setUniqId(uniqId);
                // loginResp.setUniqType(uniqType);
                // loginResp.setIdno(idno);
                // loginResp.setIpAddress(ipAddress);
                // CaseResp resp = new CaseResp(routeGo, loginResp, cifInfoResp, getOtpTelResp,
                // initBankInfo);

                // // EOP_IPLog
                // EOP_IPLog eop_IPLog = new EOP_IPLog(uniqId, ipAddress);
                // eopIPLogDao.Create(eop_IPLog);

                // return WebResult.GetResultString(0, "成功", resp);
                // */
                /*
                 * GetCifInfoResp getCifInfoResp = new GetCifInfoResp(); GetOtpTelResp
                 * getOtpTelResp = new GetOtpTelResp();
                 * 
                 * getCifInfoResp.setEmail("ooo.xxx@frog-jump.com");
                 * getOtpTelResp.setOtpTel("0929888111");
                 * 
                 * // 創立案件編號 String uniqId = snSer.getUniqId(); // LoginResp loginResp = new
                 * LoginResp(); loginResp.setUniqId(uniqId); loginResp.setUniqType(uniqType);
                 * loginResp.setIdno(idno); loginResp.setIpAddress(ipAddress); // ContractMain
                 * contractMain = new ContractMain(uniqId);
                 * contractMain.setStatus(SystemConst.CASE_STATUS_INIT); contractMain =
                 * GsonUtil.merge(contractMain, login, ContractMain.class, true);
                 * contractMainDao.Create(contractMain);
                 * 
                 * // 寫入entry資料 EntryData entryData = new EntryData(uniqId, uniqType, entry,
                 * CONT, browser, platform, os); entryDataDao.Create(entryData);
                 * 
                 * // EOP_IPLog EOP_IPLog eop_IPLog = new EOP_IPLog(uniqId, ipAddress);
                 * eopIPLogDao.Create(eop_IPLog);
                 * 
                 * // goto 登入頁(查無簽約案件) // routeGo = RouteGo.initPlContract; // CaseResp resp =
                 * new CaseResp(routeGo, loginResp, getCifInfoResp, getOtpTelResp,
                 * initBankInfo); // return WebResult.GetResultString(-90,
                 * "您無任何可供線上簽約之案件或申請案件尚未達立約階段", resp);
                 * 
                 * // goto 驗身選擇頁A. CheckCode=’’ and 手機號碼為空 不能呼叫 既有戶「未於本行留存有效手機」 routeGo =
                 * RouteGo.chooseIdentificationStylePlContract; CaseResp resp = new
                 * CaseResp(routeGo, loginResp, getCifInfoResp, getOtpTelResp, initBankInfo);
                 * return WebResult.GetResultString(0, "未於本行留存有效手機", resp);
                 * 
                 * // goto OTP頁B. CheckCode=’’ and 有回手機號碼 既有戶，往後進入OTP驗證 // routeGo =
                 * RouteGo.otpPlContract; // CaseResp resp = new CaseResp(routeGo, loginResp,
                 * getCifInfoResp, getOtpTelResp, initBankInfo); // return
                 * WebResult.GetResultString(0, "成功", resp);
                 * 
                 * // goto 驗身選擇頁C. CheckCode=’10’ 不用呼叫 新戶，往後進入驗身選擇頁 // routeGo =
                 * RouteGo.chooseIdentificationStylePlContract; // CaseResp resp = new
                 * CaseResp(routeGo, loginResp, getCifInfoResp, getOtpTelResp, initBankInfo); //
                 * return WebResult.GetResultString(0, "成功", resp);
                 */
            }
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end initPlContract

    /** 是否有行內留存手機 */
    private boolean has_OTP_TEL(List<GetOtpTelStpRspDtoOtpData> otpDatas) {
        return find_OTP_TEL(otpDatas) != null;
    }

    /** 取得一行內留存手機 */
    private String find_OTP_TEL(List<GetOtpTelStpRspDtoOtpData> otpDatas) {
        if (otpDatas != null && otpDatas.size() > 0) {
            for (GetOtpTelStpRspDtoOtpData otpData : otpDatas) {
                String format = otpData.getFORMAT();
                String otpTel = otpData.getOTP_TEL();
                if (StringUtils.equals("Y", format) && StringUtils.length(otpTel) == 10) {
                    return otpTel;
                }
            }
        }
        return null;
    }

    /**
     * 
     * @param uniqId
     * @param idno
     * @param birthday
     * @return
     * @throws ErrorResultException
     * @throws Exception
     */
    private GetCifInfoResp getCifInfo(String uniqId, String idno, String birthday)
            throws ErrorResultException, Exception {
        try {
            GetCifInfoResp resp = new GetCifInfoResp();
            GetCifInfoRspDto cif = kgiService.call_GET_CIF_INFO(uniqId, idno, birthday);

            String email = "";
            // 空白代表沒有錯誤
            if (cif.getCHECK_CODE().equals("")) {
                email = cif.getEML_ADDR();
            }
            // // 『10』代表查無資料
            // else if (cif.getCHECK_CODE().equals("10")) {
            // // ignore;
            // }
            // // 『99』代表系統錯誤，錯誤訊息請參照ERR_MSG
            // else if (cif.getCHECK_CODE().equals("99")) {
            // throw new ErrorResultException(98, "系統錯誤", null);
            // }
            // // 『0』檢核資料錯誤
            // else {
            // throw new ErrorResultException(97, cif.getERR_MSG(), null);
            // }

            if (cif.getCHECK_CODE().equals("")) {
                resp.setEmail(email);
            }
            /*
             * if (cif.getCHECK_CODE().equals("")) { resp.setChtName(cif.getNAME());
             * resp.setEngName(cif.getENG_NAME());
             * resp.setResAddrZipCode(cif.getADR_ZIP_1()); Dropdown resAddrDropDown =
             * dropdownDao.readByNameAndDataName("zipcodemapping",cif.getADR_ZIP_1());
             * resp.setResAddr(cif.getADR_1().replace(resAddrDropDown.getDataKey(),
             * "").replaceAll("\\s", "")); resp.setCommAddrZipCode(cif.getADR_ZIP());
             * Dropdown commonAddrDropDown =
             * dropdownDao.readByNameAndDataName("zipcodemapping",cif.getADR_ZIP());
             * resp.setCommAddr(cif.getADR().replace(commonAddrDropDown.getDataKey(),
             * "").replaceAll("\\s", "")); resp.setMarriage(cif.getMARRIAGE());
             * resp.setEmail(cif.getEML_ADDR());
             * resp.setResTelArea(cif.getTEL_NO_AREACODE_1());
             * resp.setResTel(cif.getTEL_NO_1());
             * resp.setHomeTelArea(cif.getTEL_NO_AREACODE());
             * resp.setHomeTel(cif.getTEL_NO()); resp.setEstateType(cif.getHOUSE_OWNER());
             * resp.setCorpName(cif.getCOMPANY_NAME());
             * resp.setCorpTelArea(cif.getCOMPANY_TEL_AREACODE());
             * resp.setCorpTel(cif.getCOMPANY_TEL());
             * resp.setCorpAddrZipCode(cif.getCOMPANY_ADDR_ZIP()); Dropdown corpAddrDropDown
             * = dropdownDao.readByNameAndDataName("zipcodemapping",cif.getADR_ZIP_1());
             * resp.setCorpAddr(cif.getCOMPANY_ADDR().replace(corpAddrDropDown.getDataKey(),
             * "").replaceAll("\\s", "")); resp.setYearlyIncome(cif.getINCOME());
             * resp.setOnBoardDate(cif.getON_BOARD_DATE()); }
             */
            return resp;
        } catch (ErrorResultException e) {
            throw e;
        } catch (Exception ex) {
            throw ex;
        }
    } // end getCifInfo

    private void initBankInfo(String uniqId, InitBankInfo initBankInfo, GetDgtConstRspDto getDgtConst) {

        GetDgtConstRspDtoConstData o = getDgtConst.findOneConstData();
        GetDgtConstRspDtoDepData dep = getDgtConst.findOneDepData();
        List<GetDgtConstRspDtoDepData> deps = getDgtConst.getDepDataList();

        if (StringUtils.isNotBlank(o.getMobile_tel()) && o.getMobile_tel().length() == 10
                && o.getMobile_tel().substring(0, 2).equals("09")) {
            initBankInfo.setPhone(o.getMobile_tel());
        }
        if (StringUtils.isNotBlank(o.getEmail_address())) {
            initBankInfo.setEmail(o.getEmail_address());
        }

        initBankInfo.setExpBankNo(o.getExp_bank_no()); // exp_bank_no：撥款銀行代號
        // PayAccount
        if (StringUtils.isNotBlank(o.getExp_account_no())) {
            initBankInfo.setExpAccount(o.getExp_account_no()); // exp_account_no：撥款銀行帳號
        } else if (StringUtils.isNotBlank(o.getPay_account())) {
            initBankInfo.setExpAccount(o.getPay_account()); // pay_account：代償銀行帳號
        }

        if (StringUtils.isNotBlank(o.getPay_kind_day()) && !o.getPay_kind_day().equals("0")) { // Pay_kind_day : 繳款日
            initBankInfo.setPayKindDay(o.getPay_kind_day());
        }

        if (StringUtils.isNotBlank(o.getSord_bank_no()) && StringUtils.isNotBlank(o.getSord_acct_no())) {
            initBankInfo.setSordBankNo(o.getSord_bank_no()); // Charles 因為 GET_DGT_CONST.sord_bank_no 有 " " 值，所以需要做
                                                             // workaround
            initBankInfo.setSordAcctNo(o.getSord_acct_no()); // Charles 因為 GET_DGT_CONST.sord_acct_no 有 " " 值，所以需要做
                                                             // workaround
        }

        if (StringUtils.isNotBlank(o.getPay_bank())) {
            initBankInfo.setPayBank(o.getPay_bank());
        }

        if (dep != null) {

            for (GetDgtConstRspDtoDepData d : deps) {
                KgiBankInfo k = new KgiBankInfo();
                k.setKgiBankNo("809");
                k.setKgiAcctNo(d.getMembCustAc());
                k.setKgiAcctName(d.getShortName());
                initBankInfo.getKgiBankInfos().add(k);
            }

            initBankInfo.setPayBankOption("0"); // 0:本行
            initBankInfo.setHasPayBankOption(true);
            initBankInfo.setSordBankOption("0"); // 0:本行
            initBankInfo.setHasSordBankOption(true);
        } else {
            initBankInfo.setPayBankOption("1"); // 1:它行
            initBankInfo.setHasPayBankOption(false);
            initBankInfo.setSordBankOption("1"); // 1:它行
            initBankInfo.setHasSordBankOption(false);
        }

        initBankInfo.setRate(o.getRate());
        initBankInfo.setAprove_period(o.getAprove_period());
        initBankInfo.setApy_fee(o.getApy_fee());
        initBankInfo.setB_range(o.getB_range());
        initBankInfo.setRate_desc(o.getRate_desc());
        initBankInfo.setP_prj_code(o.getP_prj_code());
        initBankInfo.setInt_type(o.getInt_type());
        initBankInfo.setP_now_rate(o.getP_now_rate());
        initBankInfo.setInt_pre_1(o.getInt_pre_1());
        initBankInfo.setFix_rate1(o.getFix_rate1());
        initBankInfo.setTbA_aprove_amount(o.getTbA_aprove_amount());
        initBankInfo.setSum_rate(o.getSum_rate());
        initBankInfo.setSession_id(uniqId);
    }

    public List<RateData> initRateDatas(GetDgtConstRspDto getDgtConst) {
        List<RateData> rtn = new ArrayList<>();
        List<GetDgtConstRspDtoRateData> list = getDgtConst.getRateDataList();
        for (int i = 0; list != null && i < list.size(); i++) {
            RateData rateData = new RateData();
            BeanUtils.copyProperties(list.get(i), rateData);
            rtn.add(rateData);
        }
        return rtn;
    }

    public String getDgtConst(String uniqId, String idno, String reqJson) {
        PrepareDgtConstResp prepareDgtConstResp = new PrepareDgtConstResp();
        ContractMain contractMain = contractMainDao.ReadByUniqId(uniqId);
        prepareDgtConstResp.setPaymentDay(contractMain.getPaymentDay());
		logger.info("#### " + uniqId + " 要顯示的 FinalPaymentDay = " + contractMain.getPaymentDay());
        // 查詢繳款日期回給前端
        try {
            // [判斷一]尚未驗身前呼叫取得立約電文API-01：
            // 1. GET_DGT_CONST（ID,1） 是否有個人信貸（PL）簽約資訊
            // 2. GET_DGT_CONST（ID,3） 是否有循環信貸（RPL）簽約資訊
            // 3. GET_DGT_CONST（ID,2） 是否有現金卡簽約資訊
            GetDgtConstRspDto getDgtConstRspDto1 = kgiService.touch_GET_DGT_CONST(uniqId, idno, "1");
            GetDgtConstRspDto getDgtConstRspDto3 = kgiService.touch_GET_DGT_CONST(uniqId, idno, "3");
            GetDgtConstRspDto getDgtConstRspDto2 = kgiService.touch_GET_DGT_CONST(uniqId, idno, "2");

            if (!StringUtils.equals("", getDgtConstRspDto1.getCHECK_CODE())
                    && !StringUtils.equals("", getDgtConstRspDto3.getCHECK_CODE())
                    && !StringUtils.equals("", getDgtConstRspDto2.getCHECK_CODE())) {
                return WebResult.GetResultString(-90, "查無案件資料", prepareDgtConstResp);
            } else {
                if (StringUtils.equals("", getDgtConstRspDto1.getCHECK_CODE())) {
                    GetDgtConstRspDtoConstData constData1 = getDgtConstRspDto1.findOneConstData();
                    GetDgtConstResp getDgtConstResp1 = new GetDgtConstResp();
                    BeanUtils.copyProperties(constData1, getDgtConstResp1);
                    prepareDgtConstResp.setGetDgtConstResp1(getDgtConstResp1);
                }
                if (StringUtils.equals("", getDgtConstRspDto3.getCHECK_CODE())) {
                    GetDgtConstRspDtoConstData constData3 = getDgtConstRspDto3.findOneConstData();
                    GetDgtConstResp getDgtConstResp3 = new GetDgtConstResp();
                    BeanUtils.copyProperties(constData3, getDgtConstResp3);
                    prepareDgtConstResp.setGetDgtConstResp3(getDgtConstResp3);
                }
                if (StringUtils.equals("", getDgtConstRspDto2.getCHECK_CODE())) {
                    GetDgtConstRspDtoConstData constData2 = getDgtConstRspDto2.findOneConstData();
                    GetDgtConstResp getDgtConstResp2 = new GetDgtConstResp();
                    BeanUtils.copyProperties(constData2, getDgtConstResp2);
                    prepareDgtConstResp.setGetDgtConstResp2(getDgtConstResp2);
                }
            }
            /*
             * PrepareDgtConstResp prepareDgtConstResp = new PrepareDgtConstResp();
             * GetDgtConstResp getDgtConstResp1 = new GetDgtConstResp(); GetDgtConstResp
             * getDgtConstResp3 = new GetDgtConstResp(); GetDgtConstResp getDgtConstResp2 =
             * new GetDgtConstResp();
             * prepareDgtConstResp.setGetDgtConstResp1(getDgtConstResp1);
             * prepareDgtConstResp.setGetDgtConstResp3(getDgtConstResp3);
             * prepareDgtConstResp.setGetDgtConstResp2(getDgtConstResp2);
             */

            return WebResult.GetResultString(0, "成功", prepareDgtConstResp);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end getDgtConst

    // 20201120 如果只有PL及有代償 status=0 否則status=99
    public String initPlPayBank(String uniqId, String idno) {
        String result = "";
        try {
            GetDgtConstRspDto getDgtConstRspDto1 = kgiService.touch_GET_DGT_CONST(uniqId, idno, "1");
            GetDgtConstRspDto getDgtConstRspDto3 = kgiService.touch_GET_DGT_CONST(uniqId, idno, "3");
            GetDgtConstRspDto getDgtConstRspDto2 = kgiService.touch_GET_DGT_CONST(uniqId, idno, "2");

            GetDgtConstRspDtoConstData constData = getDgtConstRspDto1.findOneConstData();

            if (StringUtils.equals("", getDgtConstRspDto1.getCHECK_CODE())
                    && !StringUtils.equals("", getDgtConstRspDto2.getCHECK_CODE())
                    && !StringUtils.equals("", getDgtConstRspDto3.getCHECK_CODE())
                    && !StringUtils.equals("", constData.getPay_bank())) {
                        System.out.println("※※※唯獨pl有待償");
                       result = WebResult.GetResultString(0, "", "");
                    } else {
                        result = WebResult.GetResultString(99, "", "");
                    }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  result;
    }

    public String judgeExceedPayDateByExpBank(String uniqId, String reqJson) {
        try {
            String contractdate = DateUtil.GetDateFormatString(); // 目前的時間 yyyyMMddHHmmss
            String resultPayDate = "";// 經由營業日，計算出最終撥款日期
            ExpBankView expBank = new Gson().fromJson(reqJson, ExpBankView.class);
            ContractMain contractMain = contractMainDao.ReadByUniqId(uniqId);
            String productId = contractMain.getProductId();
            if (StringUtils.equals(productId, PlContractConst.CONTRACT_PRODUCT_ID_TYPE_LOAN)) {
                GetExceedStatusRspDto rsp = kgiService.call_GET_EXCEED_STATUS(uniqId, contractMain.getCaseNoWeb(), expBank.getExpBankCode(), expBank.getExpBankAcctNo());
                if (StringUtils.equals("", rsp.getCHECK_CODE())) {
                    boolean isTomorrow = !StringUtils.equals("N", rsp.getExceed_flg());
                    resultPayDate = contractDayService.getContractPayDate(contractdate, isTomorrow);
                }
            }
            return WebResult.GetResultString(0, "", resultPayDate);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.GetResultString(99, configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    /**
     * 取得經由營業日，計算出最終撥款日期 resultPayDate
     * 
     * 20201119 Ben提出新增邏輯 目前只有PL需要做此判斷
     * a. 如果有代償，且撥款日已過，則請新增提示(代償立約的指定撥款日已逾期，請重新立約)，並跳回登入
     * b. 如果無代償，pay_date如果為空，就預設今天
     * c. 如果無代償，pay_date有值小於今日，則改為今日，反之則以pay_date為主         
     */
    public String judgeExceedPayDate(String uniqId, String idno) {
        try {
            String contractdate = ""; // 目前的時間 yyyyMMddHHmmss
            int intContractDate;      // 將時間轉成int yyyyMMdd
            int intPayDate;           // 撥款日期
            String resultPayDate = "";// 經由營業日，計算出最終撥款日期
            String msg = "";          // 撥款日期逾期 丟出錯誤訊息至前端
            int status = 0;

            contractdate = DateUtil.GetDateFormatString(); 
            intContractDate = Integer.valueOf(contractdate.substring(0, 8));
            ContractMain contractMain = contractMainDao.ReadByUniqId(uniqId);
            String productId = contractMain.getProductId();
            productId = StringUtils.isNotBlank(productId) ? productId : PlContractConst.CONTRACT_PRODUCT_ID_TYPE_LOAN;

            GetDgtConstRspDto getDgtConstRspDto = kgiService.touch_GET_DGT_CONST(uniqId, idno, productId);
            // logger.info("### getDgtConstRspDto=" + new Gson().toJson(getDgtConstRspDto));
            GetDgtConstRspDtoConstData constData = getDgtConstRspDto.findOneConstData();
            // logger.info("### constData=" + constData);
            if (StringUtils.isBlank(constData.getPay_date())) {
                intPayDate = Integer.valueOf(DateUtil.GetDateFormatString("yyyyMMdd"));
            } else {
                intPayDate = Integer.valueOf(constData.getPay_date());
            }

            // 有待償逾期丟回錯誤訊息
            if (StringUtils.isNotBlank(constData.getPay_bank())) {
                if (intContractDate > intPayDate) {
                    status = 99;
                    msg = configDao.ReadConfigValue("CONT_Err_PayDateExpired", "代償案件需於預定撥款日下午3點前完成線上簽約。");
                    return WebResult.GetResultString(status, msg, resultPayDate);
                }
            }
            System.out.println("※※※TEST※※..intContractDate==> " + intContractDate);
            System.out.println("※※※TEST※※..intPayDate==> " + intPayDate);
            
            System.out.println("※※※ pay_kind_day==>" + constData.getPay_kind_day());

            // RPL 預設無待償
            if (StringUtils.equals(PlContractConst.CONTRACT_PRODUCT_ID_TYPE_ELOAN, productId)) {
                // 無待償
                if (intContractDate >= intPayDate) {
                    resultPayDate = contractDayService.getContractPayDate(uniqId, contractMain.getCaseNoWeb(), contractdate, productId, constData.getPay_bank(), constData.getExp_bank_no(), null, constData.getExceed_flg()).substring(6, 8);
                    System.out.println("※※※TEST※※..55==> ");
                } else if (intContractDate < intPayDate) {
                    resultPayDate = String.valueOf(intPayDate).substring(6, 8);
                    System.out.println("※※※TEST※※..66==> ");
                }
            } else if (StringUtils.isBlank(constData.getPay_kind_day()) || StringUtils.equals("0", constData.getPay_kind_day())) {
                // pay_kind_day 繳款沒有值或者為0
                System.out.println("※※※TEST※※..00==> pay_kind_day 繳款沒有值或者為0");
                // 有代償(未逾期) 就一定會有撥款日期
                if (StringUtils.isNotBlank(constData.getPay_bank())) {
                    System.out.println("※※※TEST※※..00==> 有代償(未逾期) 就一定會有撥款日期");
                    if (intContractDate == intPayDate) {
                        resultPayDate = contractDayService.getContractPayDate(uniqId, contractMain.getCaseNoWeb(), contractdate, productId, constData.getPay_bank(), constData.getExp_bank_no(), null, constData.getExceed_flg()).substring(6, 8);           
                        System.out.println("※※※TEST※※..11==> ");
                    } else if (intContractDate < intPayDate) {
                        resultPayDate = String.valueOf(intPayDate).substring(6, 8);
                        System.out.println("※※※TEST※※..22==> ");
                    }
                // 無待償
                } else { 
                    if (StringUtils.isNotBlank(constData.getPay_date())) { // 有撥款日期
                        System.out.println("※※※TEST※※..00==> 無待償 有撥款日期");
                        if (intContractDate >= intPayDate) {
                            resultPayDate = contractDayService.getContractPayDate(uniqId, contractMain.getCaseNoWeb(), contractdate, productId, constData.getPay_bank(), constData.getExp_bank_no(), null, constData.getExceed_flg()).substring(6, 8);
                            System.out.println("※※※TEST※※..33==> ");
                        } else if (intContractDate < intPayDate) {
                            resultPayDate = String.valueOf(intPayDate).substring(6, 8);
                            System.out.println("※※※TEST※※..44==> ");
                        }
                    } else {
                        System.out.println("※※※TEST※※..77==> 無待償 無撥款日期");
                        resultPayDate = contractDayService.getContractPayDate(uniqId, contractMain.getCaseNoWeb(), contractdate, productId, constData.getPay_bank(), constData.getExp_bank_no(), null, constData.getExceed_flg()).substring(6, 8);
                    }
                }
            }
            if (resultPayDate.length() == 2 && StringUtils.startsWith(resultPayDate, "0")) {
                resultPayDate = resultPayDate.substring(1, 2);
            }
            System.out.println("※※※resultPayDate ==>" + resultPayDate);
            return WebResult.GetResultString(status, msg, resultPayDate);
        } catch (Exception e) {
            e.printStackTrace();
            return WebResult.GetResultString(99, configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    public String chosenDgtConst(String uniqId, String idno, String reqJson) {
        try {
            ChosenDgtConstView chosen = new Gson().fromJson(reqJson, ChosenDgtConstView.class);
            GetDgtConstRspDto getDgtConstRspDto = kgiService.touch_GET_DGT_CONST(uniqId, idno, chosen.getContract()); // chosen.getContract() 1, 3, 2
            String respString = kgiService.touchString_GET_DGT_CONST(uniqId, idno, chosen.getContract()); // chosen.getContract() 1, 3, 2

            System.out.println(uniqId);
            ContractMain contractMain = contractMainDao.ReadByUniqId(uniqId);
            contractMain.setProductId(chosen.getContract());
            GetDgtConstRspDtoConstData one = getDgtConstRspDto.findOneConstData();
            contractMain.setCaseNoWeb(one.getCase_no());
            contractMain.setGmCardNo(one.getGmcard_no());
            contractMain.setChtName(one.getCustomer_name());
            contractMain.setEmail(one.getEmail_address());
            contractMain.setDealPayDay(one.getPay_date());
            contractMain.setApsConstData(respString);

            GetDgtConstResp getDgtConstResp = null;
            if (StringUtils.equals("", getDgtConstRspDto.getCHECK_CODE())) {
                GetDgtConstRspDtoConstData constData = getDgtConstRspDto.findOneConstData();
                getDgtConstResp = new GetDgtConstResp();
                BeanUtils.copyProperties(constData, getDgtConstResp); // 取 input_mode
            }

            contractMainDao.Update(contractMain);

            InitBankInfo initBankInfo = new InitBankInfo();
            if (getDgtConstRspDto != null) {
                initBankInfo(uniqId, initBankInfo, getDgtConstRspDto);
            }

            ChosenDgtConstResp chosenResp = new ChosenDgtConstResp(getDgtConstResp, initBankInfo, initRateDatas(getDgtConstRspDto));

            return WebResult.GetResultString(0, "成功", chosenResp);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    } // end chosenDgtConst

    public String agreeDownloadPdf(String uniqId, String reqJson) {
        String pdfUrl = "";
        logger.info("### 契約書樣板產品 => " + reqJson);
        try {
            switch (reqJson) {
                case PlContractConst.CONTRACT_PRODUCT_ID_TYPE_LOAN:
                    pdfUrl = configDao.ReadConfigValue("airloanEX.PDF.PLoan.TemplateFilePath",
                            "https://www.kgibank.com/cosmos/about_us/L04/docs/PL002-1xWebx201806.pdf");
                    break;
                case PlContractConst.CONTRACT_PRODUCT_ID_TYPE_CASH:
                    pdfUrl = configDao.ReadConfigValue("airloanEX.PDF.GMCashLoan.TemplateFilePath",
                            "https://www.kgibank.com/about_us/L04/docs/contract04_2.pdf");
                    break;
                case PlContractConst.CONTRACT_PRODUCT_ID_TYPE_ELOAN:
                    pdfUrl = configDao.ReadConfigValue("airloanEX.PDF.RPLoan.TemplateFilePath",
                            "https://www.kgibank.com/cosmos/about_us/L04/docs/exRPL002-1xWebx-x107.06x.pdf");
                    break;
                default:
                    throw new RuntimeException("錯誤的產品ID");
            }
            logger.info("### agreeDownloadPdf pdfUrl => " + pdfUrl);
            return WebResult.GetResultString(0, "成功", pdfUrl);
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    public String agreePlContract(String uniqId, String reqJson) {
        try {
            AgreePlContractView agree = new Gson().fromJson(reqJson, AgreePlContractView.class);
            ContractMain contractMain = contractMainDao.ReadByUniqId(uniqId);

            contractMain.setEmail(agree.getEmail());
            contractMain.setReadDate(agree.getReviewDate());
            contractMain.setPaymentDay(agree.getPayMentDay());
            logger.info("### agree.getAuthInfo()=" + agree.getAuthInfo());
            contractMain.setDataUse(StringUtils.equals("true", agree.getAuthInfo()) ? "01" : "02"); // 同意資料使用(共銷) 01:同意
                                                                                                    // 02:不同意
            contractMainDao.Update(contractMain);
            return WebResult.GetResultString(0, "成功", null);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99,
                    configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    public String infoPlContract(String uniqId, String reqJson) {
        try {
            InfoPlContractView infoView = new Gson().fromJson(reqJson, InfoPlContractView.class);
            ContractMain contractMain = contractMainDao.ReadByUniqId(uniqId);
            contractMain.setPaymentDay(infoView.getPayMentDay());
            contractMain.setPayBankOption(infoView.getPayBankOption());
            contractMain.setSordBankOption(infoView.getSordBankOption());
            contractMainDao.Update(contractMain);
            return WebResult.GetResultString(0, "成功", null);
        } catch (ErrorResultException e) {
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }

    /**
     * 準備 UPDATE_DGT_CONST 的資料
     * 
     * @param contractMain
     * @param actInfo
     * @return
     * @throws Exception
     */
    public UpdateDgtConstReqDto prepareUpdateDgtConst(ContractMain contractMain, ContractMainActInfo actInfo) throws Exception {
        
        String productId = contractMain.getProductId();
        String const_sign_no = globalConfig.findContract_VerDate(productId);
        
        UpdateDgtConstDomain domain = new UpdateDgtConstDomain();
        domain.make(contractMain, actInfo, const_sign_no, contractMain.getFinalPayDay());
        UpdateDgtConstReqDto o = new UpdateDgtConstReqDto();

        o.setCASE_NO(domain.getCASE_NO());
        o.setCITY_CODE(domain.getCITY_CODE());
        o.setADDRESS(domain.getADDRESS());
        o.setPay_date(domain.getPay_date());
        o.setPay_desc_type(domain.getPay_desc_type());
        o.setPay_bank(domain.getPay_bank());
        o.setPay_bank_no(domain.getPay_bank_no());
        o.setPay_bank_branchId(domain.getPay_bank_branchId());
        o.setPay_account(domain.getPay_account());
        o.setPay_kind_type(domain.getPay_kind_type());
        o.setPay_kind_day(domain.getPay_kind_day());
        o.setPay_way_type(domain.getPay_way_type());
        o.setSord_bank_no(domain.getSord_bank_no());
        o.setSord_bank_name(domain.getSord_bank_name());
        o.setSord_acct_no(domain.getSord_acct_no());
        o.setEdda_check_time(domain.getEdda_check_time());
        o.setConst_sign_no(domain.getConst_sign_no());
        o.setAPSSeqNo(domain.getAPSSeqNo());
        o.setExceed_flg(domain.getExceed_flg());

        return o;
    } // end prepareUpdateDgtConst

    public String updateDgtConst(String uniqId, String reqBody) {
        logger.info("######## 最後契約簽屬UPDATE_DGT_CONST");
        try {
            
            // 簽名時間(對保時間)
            contractMainDao.updateSignTime(uniqId);
            
            if (StringUtils.equals("Y",isWhite)) {
                logger.info("### isWhite = > " + isWhite);
                contractApiLogDao.UpdateUsedWhite(uniqId, reqBody);
                contractMainDao.updateContractStatusWhenStatusEq(uniqId, CASE_STATUS_WRITE_SUCCESS, CASE_STATUS_WRITING);
                ContractMain contractMain = contractMainDao.ReadByUniqId(uniqId);
                ContractMainActInfo actInfo = contractMainActInfoDao.ReadByUniqId(uniqId);
                UpdateDgtConstReqDto reqDto = prepareUpdateDgtConst(contractMain, actInfo);
                System.out.println("####撥款日期 ==> " + reqDto.getPay_date());
                return WebResult.GetResultString(0, "成功", "");
            } else {
                logger.info("### notWhite = > " + isWhite);
                ContractMain contractMain = contractMainDao.ReadByUniqId(uniqId);

                // Status 01 -> 02
                if (StringUtils.equals(CASE_STATUS_WRITING, contractMain.getStatus())) {
                    ContractMainActInfo actInfo = contractMainActInfoDao.ReadByUniqId(uniqId);
                    UpdateDgtConstReqDto reqDto = prepareUpdateDgtConst(contractMain, actInfo);
                    String productId = contractMain.getProductId();
        
                    //BeanUtils.copyProperties(view, reqDto);
                    String reqString = new Gson().toJson(reqDto);
                    logger.info("### UPDATE_DGT_CONST:" + reqString);
                    UpdateDgtConstRspDto rspDto = kgiService.call_UPDATE_DGT_CONST(uniqId, reqString);
                    if (StringUtils.equals("", rspDto.getCHECK_CODE())) {
                        kgiService.updateDgtConstIsUsed(uniqId, contractMain.getIdno(), productId); // 2020-09-03 demand by Ben
                        contractMainDao.updateContractStatusWhenStatusEq(uniqId, CASE_STATUS_WRITE_SUCCESS, CASE_STATUS_WRITING);
                        return WebResult.GetResultString(0, "成功", rspDto);
                    } else {
                        return WebResult.GetResultString(99, rspDto.getERR_MSG(), null);
                    }
                } else {
                    return WebResult.GetResultString(99, configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
                }
            }
        } catch (ErrorResultException e) {
            e.printStackTrace();
            return WebResult.GetResultString(e.getStatus(), e.getMessage(), e.getResult());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("未處理的錯誤", e);
            return WebResult.GetResultString(99, configDao.ReadConfigValue("CONT_Err_Unexpected", "系統問題，如有任何需要協助，請洽客服人員"), null);
        }
    }
    
}