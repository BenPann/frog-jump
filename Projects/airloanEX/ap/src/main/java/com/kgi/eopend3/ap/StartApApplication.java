package com.kgi.eopend3.ap;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;
// import java.util.HashMap;
// import java.util.List;
// import java.util.Map;
// import java.util.Properties;
// import com.kgi.eopend3.ap.config.AirloandbConfig;
// import com.kgi.eopend3.ap.dao.ConfigDao;
// import com.kgi.eopend3.common.dto.db.Config;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.context.event.EventListener;
// import org.springframework.core.env.ConfigurableEnvironment;
// import org.springframework.core.env.PropertiesPropertySource;
// import org.springframework.boot.context.event.ApplicationReadyEvent;


// @SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
@Configuration
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
@ComponentScan({"com.kgi.eopend3.ap","com.kgi.airloanex.ap","com.kgi.crosssell.ap"})

@EnableScheduling
@EnableRetry
public class StartApApplication extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(StartApApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(StartApApplication.class);
		// springApplication.addListeners(new SpringBuiltInEventsListener());
		springApplication.run(args);
	}

	/*// Example of Load Properties From Config and Create a PropertiesPropertySource name "airloandbConfig"
	@Autowired
    ConfigDao configDao;
	@EventListener(ApplicationReadyEvent.class)
	public void onApplicationReadyEvent(ApplicationReadyEvent event) {
		
		// Load Properties From Config and Create a PropertiesPropertySource name "airloandbConfig"
		final ConfigurableEnvironment env = event.getApplicationContext().getEnvironment();
		final Properties props = loadPropertiesFromDatabase();
		final PropertiesPropertySource source = new PropertiesPropertySource("airloandbConfig", props);

		// 
		env.getPropertySources().addFirst(source);
		System.out.println(env.getPropertySources().get("airloandbConfig")); // DEBUG
		System.out.println("KeyValue=" + configDao.ReadConfigValue("AML.AMLEnd"));
		
		AirloandbConfig a = new AirloandbConfig(event.getApplicationContext());
		System.out.println(a.AML_AMLEnd);
	}
	private Properties loadPropertiesFromDatabase() {
		List<Config> configs = configDao.findAll();
        Map<String, String> map = new HashMap<>();
        for (Config config : configs) {
            map.put(config.getKeyName(), config.getKeyValue());
		}
		Properties properties = new Properties();
		properties.putAll(map);
        return properties;
    }*/
}
