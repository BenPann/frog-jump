package com.kgi.airloanex.ap.dao;

import java.util.List;
import java.util.Map;

import com.kgi.eopend3.ap.dao.BaseDao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class EopQueryHelper extends BaseDao {
    
    public List<Map<String, Object>> getPolicyImageByCreditUniqId(String uniqid) {
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		String sql = "SELECT * FROM CreditCaseData ccd "
				+ "                          inner join ED3_EX_CreditCaseData ed3CCD on ccd.UniqId = ed3CCD.AirLoanUniqId "
				+  "                         inner join  ED3_CLPolicyInfo policy on policy.UniqId = ed3CCD.UniqId  "
				+ "  WHERE ed3CCD.AirLoanUniqId = ? and policy.ActionType = '1' "
				+ " ORDER by policy.serial desc"
				;
		return jdbcTemplate.queryForList(sql,new Object[] { uniqid });
	}
}
