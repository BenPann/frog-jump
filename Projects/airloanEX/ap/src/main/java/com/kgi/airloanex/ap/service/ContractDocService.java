package com.kgi.airloanex.ap.service;

import static com.kgi.airloanex.common.PlContractConst.DOCUMENT_TYPE_CONTRACT_PDF;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.kgi.airloanex.ap.dao.CaseDocumentDao;
import com.kgi.airloanex.ap.dao.ContractDocDao;
import com.kgi.airloanex.ap.dao.ContractMainDao;
import com.kgi.airloanex.ap.dao.OracleWdDao;
import com.kgi.airloanex.ap.pdf.ContractAchPDF;
import com.kgi.airloanex.ap.pdf.ContractFinalePDF;
import com.kgi.airloanex.ap.pdf.ContractPDF;
import com.kgi.airloanex.ap.pdf.HTML2PDF;
import com.kgi.airloanex.ap.pdf.PDFHelper;
import com.kgi.airloanex.common.PlContractConst;
import com.kgi.airloanex.common.dto.db.CaseDocument;
import com.kgi.airloanex.common.dto.db.ContractMain;
import com.kgi.eopend3.ap.config.GlobalConfig;
import com.kgi.eopend3.common.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
public class ContractDocService {

	@Autowired
	private ContractMainDao contractMainDao;

	@Autowired
	private ContractDocDao contractDao;

	@Autowired
	private ContractMainDao contDao;

	@Autowired
	private OracleWdDao oracleWdDao;

	@Autowired
	private ApplicationContext appContect;
	
	@Autowired
	private CaseDocumentDao caseDocumentDao;

	@Autowired
	private GlobalConfig global;

	@Autowired
	private HTML2PDF html2PDF;

	public String createContractHtml(String UniqId, String idno, String ipAddress, String name) throws Exception {
		String Previewcontract = "";
		String ProductId = "";
		
		ContractMain contractMain = contractMainDao.ReadByUniqId(UniqId);
		ProductId = contractMain.getProductId();
		switch (ProductId) {
		case PlContractConst.CONTRACT_PRODUCT_ID_TYPE_LOAN:
			Previewcontract = getALoanContractHtml(UniqId, idno, ipAddress,
					contractMain.getReadDate(), contractMain.getNotify(),
					contractMain.getCourt(), contractMain.getApsConstData(),
					contractMain.getDocAddr(), name);
			break;
		case PlContractConst.CONTRACT_PRODUCT_ID_TYPE_CASH:
			Previewcontract = getCashCardContractHtml(UniqId, idno, ipAddress,
					contractMain.getReadDate(), contractMain.getNotify(),
					contractMain.getCourt(), contractMain.getApsConstData(),
					contractMain.getDocAddr(), name);
			break;
		case PlContractConst.CONTRACT_PRODUCT_ID_TYPE_ELOAN:
			Previewcontract = getELoanContractHtml(UniqId, idno, ipAddress,
					contractMain.getReadDate(), contractMain.getNotify(),
					contractMain.getCourt(), contractMain.getApsConstData(),
					contractMain.getDocAddr(), name);
			break;
		default:
			break;
		}
		return Previewcontract;
	}

	// PLOAN
	public String getALoanContractHtml(String conNo, String idno, String ipAddress, String readDate, String notify,
			String court, String apsConstData, String docAddr,String name) throws Exception {
		String StaticFilePath = global.StaticFilePath;
		String htmlStr = StaticFilePath+"/html/contract_onlineLoan.html";
		InputStream input = new FileInputStream(new File(htmlStr));
		htmlStr = HTML2PDF.readHtmlFromFilePath(input);
		return pocessContractHtml(conNo, idno, ipAddress, readDate, notify, court, docAddr, apsConstData, htmlStr,name,
				PlContractConst.CONTRACT_PRODUCT_ID_TYPE_LOAN);
	}

	// e貸寶
	public String getELoanContractHtml(String conNo, String idno, String ipAddress, String readDate, String notify,
			String court, String apsConstData, String docAddr,String name) throws Exception {
		String StaticFilePath = global.StaticFilePath;
		String htmlStr = StaticFilePath+"/html/contract_E_Loan.html";
		InputStream input = new FileInputStream(new File(htmlStr));
		htmlStr = HTML2PDF.readHtmlFromFilePath(input);
		return pocessContractHtml(conNo, idno, ipAddress, readDate, notify, court, docAddr, apsConstData, htmlStr,name,
				PlContractConst.CONTRACT_PRODUCT_ID_TYPE_ELOAN);
	}

	// 現金卡
	public String getCashCardContractHtml(String conNo, String idno, String ipAddress, String readDate, String notify,
			String court, String apsConstData, String docAddr,String name) throws Exception {
		String StaticFilePath = global.StaticFilePath;
		String htmlStr = StaticFilePath+"/html/contract_CashCard.html";
		InputStream input = new FileInputStream(new File(htmlStr));
		htmlStr = HTML2PDF.readHtmlFromFilePath(input);
		return pocessContractHtml(conNo, idno, ipAddress, readDate, notify, court, docAddr, apsConstData, htmlStr, name,
				PlContractConst.CONTRACT_PRODUCT_ID_TYPE_CASH);
	}

	// 組成契約書html
	private String pocessContractHtml(String conNo, String idno, String ipAddress, String readDate, String notify,
			String court, String docAddr, String apsConstData, String contractHtml,String name, String productId) {
		System.out.println("PLAP - ContractDocService - pocessContractHtml");
		try {
			JSONObject Contract = JSONObject.fromObject(apsConstData);

			if (!Contract.getString("CONST_DATA").equals("") && !Contract.getString("CONST_DATA").equals("[]")) {
				JSONArray payjarr = new JSONArray();
				JSONArray constArr = Contract.getJSONArray("CONST_DATA");
				if (constArr.size() > 0) {
					Contract = constArr.getJSONObject(0);
					// System.out.println(constArr);
					for (int i = 0; i < constArr.size(); i++) {
						JSONObject r = new JSONObject();
						JSONObject Cont = constArr.getJSONObject(i);
						r.put("pay_account", Cont.optString("pay_account"));
						r.put("pay_bank", Cont.optString("pay_bank"));
						payjarr.add(r);
					}
					// 1.貸款期間解析
					String contractdate = DateUtil.GetDateFormatString();
					// APS PAYDAY
					String dealPaydate = Contract.optString("pay_date", contractdate.substring(0, 8));
					// 判斷後正式PAYDAY
					String paydate = "";
//					System.out.println("ApsConstData ContractTime:" + contractdate);

					// 貸款期間邏輯處理
					if (dealPaydate.equals("")) {
						paydate = getContractPayDate(contractdate, productId);
					} else {
						if (Integer.valueOf(contractdate.substring(0, 8)) < Integer.valueOf(dealPaydate)) {
							paydate = getPayDate(dealPaydate);
						} else {
							paydate = getContractPayDate(contractdate, productId);
						}
					}
//					System.out.println("getPayDate - PayDate:" + paydate);
					// System.out.println("paybank:"+payjarr.toString());
					// 2.組成合約書內容
					Contract.put("contractDate", contractdate);
					Contract.put("paybank", payjarr.toString());
					Contract.put("pay_date", paydate);
					Contract.put("idNo", idno);
					Contract.put("readDate", readDate);
					Contract.put("ipAddress", ipAddress);
					Contract.put("notify", notify);
					Contract.put("districtCourt", court);
					Contract.put("docAddr", docAddr);

					Contract.put("productId", productId);
					Contract.put("name", name);
					contractHtml = html2PDF.getConractHtml(contractHtml, Contract.toString());

					// 3.存入撥款日期、合約日期
					saveContractDate(conNo, dealPaydate, paydate, contractdate);
				}
				return contractHtml;
			} else {
				return "99";
			}

		} catch (Exception e) {
//			e.printStackTrace();
			return "99";
		}
	}

	// 取營業日
	public String getPayDate(String Date) throws Exception {
		return oracleWdDao.getNextWorkDay(Date);
	}

	// 取立約日之撥款日(下一營業日)
	public String getContractPayDate(String cDate, String productId) throws Exception {
		String paydate;
		// 立約日如為14點之前

		if (Integer.valueOf(cDate.substring(8, 14)) < Integer.valueOf("135959")
				&& productId.equals(PlContractConst.CONTRACT_PRODUCT_ID_TYPE_LOAN)) {
			paydate = getPayDate(cDate.substring(0, 8));
		} else if (Integer.valueOf(cDate.substring(8, 14)) < Integer.valueOf("155959")
				&& productId.equals(PlContractConst.CONTRACT_PRODUCT_ID_TYPE_ELOAN)) {
			paydate = getPayDate(cDate.substring(0, 8));
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Calendar cd = Calendar.getInstance();
			cd.setTime(sdf.parse(cDate.substring(0, 8)));
			cd.add(Calendar.DATE, 1);// 增加一天
			paydate = getPayDate(sdf.format(cd.getTime()));
		}

		return paydate;
	}

	public int saveContractDoc(String UniqId, String previewcontract) throws Exception {
		
		// add CaseDocument
		CaseDocument cd = new CaseDocument(UniqId, "03", PlContractConst.DOCUMENT_TYPE_CONTRACT_PDF); 
		// 空白的
		byte[] b = new byte[1];
		cd.setContent(b);
		cd.setOther(previewcontract);
		caseDocumentDao.CreateOrUpdate(cd);
		
		// add ContractDoc
		contractDao.createContractDoc(UniqId);
		contractDao.insertContractDoc(UniqId, previewcontract);
		
		// update ContractMain.ResultHtml
		ContractMain contractMain = contractMainDao.ReadByUniqId(UniqId);
		contractMain.setResultHtml(previewcontract);
		int cnt = contractMainDao.Update(contractMain);

		return cnt;
	}

	public void saveContractDate(String uniqId, String dealPayDay, String finalPayDay, String contractDay)
			throws Exception {
		// 立約日轉db存檔格式
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		Date cdate = formatter.parse(contractDay);
		SimpleDateFormat dbFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String dbcontractDay = dbFormat.format(cdate);

		contDao.updateContractPayDay(uniqId, dealPayDay, finalPayDay, dbcontractDay);
	}

	public String getPreviewContent(String uniqId) throws Exception {
		List<Map<String, Object>> list = contractDao.getContractDoc(uniqId);
		String previewContent = "";
		if (list.size() > 0) {
			previewContent = list.get(0).get("PreviewContent").toString();
		}
		return previewContent;
	}

	public String downloadContract(String uniqId) throws Exception {
		List<Map<String, Object>> list = contractDao.getContractDoc(uniqId);
		String PdfContent = "";
		if (list.size() > 0) {
			PdfContent = list.get(0).get("PdfContent").toString();
		}
		return PdfContent;
	}

	public void uploadSign(String uniqId, String sign) {
		contractDao.uploadSign(uniqId, sign);
	}

	// /** 產生立約的預覽用PDF */
	// public String generatePreviewContractHtml(String uniqId) throws Exception {
	// 	// 此部分為特殊作法 只需要替換完成的HTML
	// 	ContractPDF pdf = new ContractPDF(appContect);
	// 	pdf.init(uniqId, "false");
	// 	pdf.beforePdfProcess();
	// 	PDFHelper helper = new PDFHelper(pdf);
	// 	return helper.ExportHtml();
	// }

	public void generateContractPDF(String uniqId) throws Exception {
		ContractPDF pdf = new ContractPDF(appContect);
		pdf.init(uniqId, "true");
		PDFHelper helper = new PDFHelper(pdf);
		helper.PdfProcess();
	}

	public void generateContractAchPDF(String uniqId) throws Exception {
		ContractAchPDF pdf = new ContractAchPDF(appContect);
		pdf.init(uniqId, "true");
		PDFHelper helper = new PDFHelper(pdf);
		helper.PdfProcess();
	}

	/** 產生立約的預覽用PDF */
	public String generatePreviewContractFinaleHtml(String uniqId) throws Exception {
		// 此部分為特殊作法 只需要替換完成的HTML
		ContractFinalePDF pdf = new ContractFinalePDF(appContect);
		pdf.init(uniqId, "false");
		pdf.beforePdfProcess();
		PDFHelper helper = new PDFHelper(pdf);
		return helper.ExportHtml();
	}

	public ContractFinalePDF generateContractFinalePDF(String uniqId) throws Exception {
		ContractFinalePDF pdf = new ContractFinalePDF(appContect);
		pdf.init(uniqId, "true");
		PDFHelper helper = new PDFHelper(pdf);
		helper.PdfProcess();
		return pdf;
	}

	// public void proessContractPdf(String UniqId, String idno) throws Exception {

	// 	List<Map<String, Object>> contractUser = contDao.getConstData(UniqId);

	// 	List<Map<String, Object>> contractdoc = contractDao.getContractDoc(UniqId);
	// 	String previewContent = "";
	// 	if (contractdoc.size() > 0) {
	// 		previewContent = contractdoc.get(0).get("PreviewContent").toString();
	// 		// 原本簽名改成直接用名字帶入
	// 		String userSign = "";
	// 		userSign = contractUser.get(0).get("CustomerName").toString();
	// 		String email = contractdoc.get(0).get("EMail").toString();
	// 		String productId = (String) contractUser.get(0).get("ProductId");
	// 		pdfService.pdfContractActView(UniqId, idno, previewContent, userSign, email, productId);
	// 	}

	// }
}
