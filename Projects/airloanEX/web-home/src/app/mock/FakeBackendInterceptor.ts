import {Injectable} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {mergeMap} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  // 回應正常訊息
  regularResponseList = [
    {url: 'api/apply/finish', method: 'GET'},   // 完成頁面
  ];

  constructor() {
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return of(null).pipe(
      mergeMap(() => {
        // 產品模式不顯示此段LOG
        if (!environment.production) {
          //  console.log('FakeBackendInterceptor request:', request);
        }

        if (environment.useFakeOtp) {

          if (
            request.url.endsWith('api/validate/nccc') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {
                ncccCheckCode: '0',
                ncccMessage: '',
                errorCount: 0,
                ncccErrorCount: 0,
                pcode2566ErrorCount: 0
              }
            };
            return of(new HttpResponse({status: 200, body}));
          }

          if (
            (
              request.url.endsWith('api/validate/pcode2566') ||
              request.url.endsWith('api/validate/pcode2566/auth') ||
              request.url.endsWith('api/validate/pcode2566/sign')
            ) &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {
                pcode2566CheckCode: '0',
                pcode2566Message: '',
                // ed3
                errorCount: 0,
                ncccErrorCount: 0,
                pcode2566ErrorCount: 0,
                // airloanEX
                authErrCount: 0,
                signErrCount: 0,
                eddaErrCount: 0,
              }
            };
            return of(new HttpResponse({status: 200, body}));
          }

          if (
            request.url.endsWith('api/validate/sendOTP') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '',
              result: {
                code: '0000',
                message: '',
                sk: 'sk',
                txnID: 'txn',
                txnDate: 'date'
              }
            };

            return of(new HttpResponse({status: 200, body}));
          }

          // 驗證OTP
          if (
            request.url.endsWith('api/validate/checkOTP') &&
            request.method === 'POST'
          ) {
            const body = {
              status: 0,
              message: '',
              result: {
                code: '0000',
                message: ''
              }
            };

            return of(new HttpResponse({status: 200, body}));
          }

        } // end if (environment.useFakeOtp) {

        // 是否使用假的的API
        if (environment.useFakeKgiApi) {
          if (
            this.regularResponseList.find(
              value =>
                value.method === request.method &&
                request.url.endsWith(value.url)
            )
          ) {
            const body = {
              status: 0,
              message: '',
              result: ''
            };
            return of(new HttpResponse({status: 200, body}));
          }

          // TODO: stp/applyStart
        }

        // 是否使用假的的API
        if (environment.useFakeBackendApi) {
          // 取得下拉選單
          if (
            request.url.endsWith('api/data/getDropDown') &&
            request.method === 'POST'
          ) {
            let body: any[];
            // console.log(request.body);
            switch (request.body.name) {
              case 'estate':
                body = [
                  {dataKey: '1', dataName: '本人所有'},
                  {dataKey: '2', dataName: '配偶所有'},
                  {dataKey: '3', dataName: '家族所有'},
                  {dataKey: '4', dataName: '無'}
                ];
                break;
              case 'eopeducation':
                body = [
                  {dataKey: '1', dataName: '博士'},
                  {dataKey: '2', dataName: '碩士'},
                  {dataKey: '3', dataName: '大學'},
                  {dataKey: '4', dataName: '大專'},
                  {dataKey: '5', dataName: '高中(職)'},
                  {dataKey: '6', dataName: '國中'},
                  {dataKey: '7', dataName: '其他'}
                ];
                break;
              case 'eopbillType':
                body = [
                  {dataKey: '2', dataName: '電子帳單'},
                  {dataKey: '3', dataName: '郵寄帳單'}
                ];
                break;
              case 'eoppurpose':
                body = [
                  {dataKey: '1', dataName: '爽開就開'},
                  {dataKey: '2', dataName: '老媽叫我開'},
                  {dataKey: '3', dataName: '我也不知道為什麼要開'}
                ];
                break;
              case 'eopoccupation':
                body = [
                  {dataKey: '030100', dataName: '軍公教'},
                  {dataKey: '016499', dataName: '金融保險業'},
                  {dataKey: '014290', dataName: '營造/土木工程'},
                  {dataKey: '018610', dataName: '醫療院所'},
                  {dataKey: '013399', dataName: '製造業'},
                  {dataKey: '014990', dataName: '運輸業'},
                  {dataKey: '014832', dataName: '通訊/科技業'},
                  {dataKey: '1_019690', dataName: '服務業'},
                  {dataKey: '2_019690', dataName: '家管'},
                  {dataKey: '016812', dataName: '不動產經紀業'},
                  {dataKey: '1_014745', dataName: '藝術品或骨董等專拍賣'},
                  {dataKey: '016911', dataName: '法律服務業'},
                  {dataKey: '016920', dataName: '會計服務業'},
                  {dataKey: '999690', dataName: '軍火業'},
                  {dataKey: '019200', dataName: '博弈業'},
                  {dataKey: '016419', dataName: '虛擬貨幣'},
                  {dataKey: '016496', dataName: '典當業'},
                  {dataKey: '2_014745', dataName: '珠寶、銀樓業'},
                  {
                    dataKey: '019323',
                    dataName:
                      '特殊行業(視廳歌唱業、理髮業、三溫暖業、舞廳業、酒吧業、特種咖啡茶室及電子遊藝業)'
                  }
                ];
                break;
              case 'eopjobtitle':
                body = [
                  {dataKey: '1', dataName: '董監事/股東'},
                  {dataKey: '2', dataName: '負責人'},
                  {dataKey: '3', dataName: '高階管理職務人員'},
                  {dataKey: '4', dataName: '一般主管'},
                  {dataKey: '5', dataName: '工程師'},
                  {dataKey: '6', dataName: '業務人員'},
                  {dataKey: '7', dataName: '一般職員'},
                  {dataKey: '8', dataName: '約聘人員'},
                  {dataKey: '9', dataName: '老師'},
                  {dataKey: '10', dataName: '其他'}
                ];
                break;
              case 'idcardlocation':
                body = [
                  {dataKey: '北縣', dataName: '北縣'},
                  {dataKey: '宜縣', dataName: '宜縣'},
                  {dataKey: '桃縣', dataName: '桃縣'},
                  {dataKey: '竹縣', dataName: '竹縣'},
                  {dataKey: '苗縣', dataName: '苗縣'},
                  {dataKey: '中縣', dataName: '中縣'},
                  {dataKey: '彰縣', dataName: '彰縣'},
                  {dataKey: '投縣', dataName: '投縣'},
                  {dataKey: '雲縣', dataName: '雲縣'},
                  {dataKey: '嘉縣', dataName: '嘉縣'},
                  {dataKey: '南縣', dataName: '南縣'},
                  {dataKey: '高縣', dataName: '高縣'},
                  {dataKey: '屏縣', dataName: '屏縣'},
                  {dataKey: '東縣', dataName: '東縣'},
                  {dataKey: '花縣', dataName: '花縣'},
                  {dataKey: '澎縣', dataName: '澎縣'},
                  {dataKey: '基市', dataName: '基市'},
                  {dataKey: '竹市', dataName: '竹市'},
                  {dataKey: '嘉市', dataName: '嘉市'},
                  {dataKey: '連江', dataName: '連江'},
                  {dataKey: '金門', dataName: '金門'},
                  {dataKey: '北市', dataName: '北市'},
                  {dataKey: '高市', dataName: '高市'},
                  {dataKey: '新北市', dataName: '新北市'},
                  {dataKey: '中市', dataName: '中市'},
                  {dataKey: '南市', dataName: '南市'},
                  {dataKey: '桃市', dataName: '桃市'}
                ];
                break;
              case 'pCode2566Bank':
                body = [
                  {dataKey: '004', dataName: '台灣銀行'},
                  {dataKey: '009', dataName: '彰化銀行'},
                  {dataKey: '012', dataName: '台北富邦'},
                  {dataKey: '016', dataName: '高雄銀行'},
                  {dataKey: '017', dataName: '兆豐商銀'},
                  {dataKey: '048', dataName: '王道銀行'},
                  {dataKey: '052', dataName: '渣打商銀'},
                  {dataKey: '054', dataName: '京城商銀'},
                  {dataKey: '101', dataName: '瑞興銀行'},
                  {dataKey: '103', dataName: '臺灣新光銀行'},
                  {dataKey: '108', dataName: '陽信銀行'},
                  {dataKey: '118', dataName: '板信銀行'},
                  {dataKey: '162', dataName: '彰化六信'},
                  {dataKey: '216', dataName: '花蓮二信'},
                  {dataKey: '600', dataName: '農金資中心'},
                  {dataKey: '803', dataName: '聯邦銀行'},
                  {dataKey: '805', dataName: '遠東銀行'},
                  {dataKey: '806', dataName: '元大銀行'},
                  {dataKey: '808', dataName: '玉山銀行'},
                  {dataKey: '809', dataName: '凱基銀行'},
                  {dataKey: '812', dataName: '台新銀行'}
                ];
                break;
              default:
                break;
            }
            if (body) {
              return of(
                new HttpResponse({
                  status: 200,
                  body: {
                    status: 0,
                    message: '',
                    result: body
                  }
                })
              );
            }
          }

          if (
            request.url.includes('/publicApi/Terms/') &&
            request.method === 'GET'
          ) {
            return of(new HttpResponse({status: 200, body: '我是條款'}));
          }

          if (
            request.url.endsWith('api/apply/accountType') &&
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {
                ed3OfflineContent: '非本人帳戶無轉帳限制<br>可使用約定轉帳',
                ed3OnlineContent: '非本人帳戶無轉帳限制:<br>1萬/單筆、3萬/每日、5萬/每月<br>無約定轉帳'
              }
            };
            return of(new HttpResponse({status: 200, body}));
          }

          if (
            request.url.endsWith('api/apply/product') &&
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {
                ed3CCAProdContent: '每月享跨提12次免手續費<br>新戶享首刷禮',
                ed3AProdContent: '每月享跨提5次免手續費'
              }
            };
            return of(new HttpResponse({status: 200, body}));
          }

          if (
            request.url.endsWith('/publicApi/channel/getChannelList') &&
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: [
                {
                  channelID: 'KB',
                  channelName: '凱基商銀',
                  idLength: 'Hello, world!',
                  idLogic: '0'
                }
              ]
            };
            return of(new HttpResponse({status: 200, body}));
          }

          if (
            request.url.includes('/publicApi/terms/getTermsList') &&
            request.method === 'POST'
          ) {
            let body: any[];
            switch (request.body.url) {
              case 'cont-init':
              case 'cont-initPlContract': // add by Charles
                body = [
                  {
                    termTitle: '個人資料運用告知聲明使用者條款及約定',
                    termName: 'EOPPersonalDataUsing',
                    type: '2',
                  },
                  {
                    termTitle: '利率費用告知事項',
                    termName: 'EOPRateFee',
                    type: '2',
                  },
                  {
                    termTitle: '非代辦宣告',
                    termName: 'EOPNoAgency',
                    type: '2',
                  },
                  {
                    termTitle: '電話/網路/行動銀行約定條款',
                    termName: 'EOPBankPromise',
                    type: '1',
                  }
                ];
                break;
            }


            return of(new HttpResponse({status: 200, body}));
          }

          if (
            request.url.endsWith('/api/apply/getAuthInfo') &&
            request.method === 'GET'
          ) {
            const body = {
              status: 0,
              message: '成功',
              result: {authorizeToallCorp: 'N', verNo: '1.0'}
            };
            return of(new HttpResponse({status: 200, body}));
          }

        } // end if (environment.useFakeBackendApi)

        return next.handle(request);
      })
    );
    // .pipe(materialize())
    // .pipe(delay(500))
    // .pipe(dematerialize());
  } // end intercept
} // end FakeBackendInterceptor

export let fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
