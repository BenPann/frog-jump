import { TestBed } from '@angular/core/testing';

import { CalTokenGuard } from './cal-token.guard';

describe('CalTokenGuard', () => {
  let guard: CalTokenGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CalTokenGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
