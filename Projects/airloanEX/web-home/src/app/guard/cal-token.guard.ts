import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserDataService } from '../service/user-data/user-data.service';
import { ModalFactoryService } from '../shared/service/common/modal-factory.service';
import { RouterHelperService } from '../shared/service/common/router-helper.service';

@Injectable({
  providedIn: 'root'
})
export class CalTokenGuard implements CanActivate {
  constructor(
    private userDataService: UserDataService,
    private routerHelperService: RouterHelperService,
    private modalFactoryService: ModalFactoryService
  ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (!environment.production) {
      return true;
    } else if (this.userDataService.calToken) {
      return true;
    } else {
      this.routerHelperService.navigate(['home']);
      return false;
    }
  }
}
