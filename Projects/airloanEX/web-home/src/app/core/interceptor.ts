import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { SpinnerOverlayService } from '../shared/service/common/spinner-overlay.service';
import { catchError, finalize } from 'rxjs/operators';
import { ModalFactoryService } from '../shared/service/common/modal-factory.service';
import { UserDataService } from '../service/user-data/user-data.service';
import { RouterHelperService } from '../shared/service/common/router-helper.service';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';

@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {
  private count = 0;
  private modalCount = 0;

  constructor(
    private userDataService: UserDataService,
    private spinnerOverlayService: SpinnerOverlayService,
    private routerHelperService: RouterHelperService,
    private modalFactoryService: ModalFactoryService,
    private logger: ApplyLoggerService
  ) {
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.count++;
    if (this.count === 1) {
      this.spinnerOverlayService.show();
    }

    const token = this.userDataService.token;
    const calToken = this.userDataService.calToken;

    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer' + token
        },
        body: request.body
      });
    } else if (calToken) {
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer' + calToken
        },
        body: request.body
      });
    }
    return next.handle(request).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse) {

          if (error.status === 401 || error.status === 504) {
            /*this.modalCount++;
            if (this.modalCount === 1) {
              // this.routerHelperService.navigate(['init']);
              this.routerHelperService.navigate(['home']).then(() => {
                this.modalFactoryService.modalDefault({
                  message: '驗證逾期，請重新輸入資料'
                }).result.then((v => {
                  this.modalCount = 0;
                }));
              });
            }*/

            const data = {status: 9999, message: '系統問題，如有任何需要協助，請洽客服人員', result: null};
            this.logger.showMsg(data);
            this.modalCount = 0;
          } else {
            console.warn('其他錯誤', error);
          }
        }
        return throwError(error);
      }),
      finalize(() => {
        this.count--;
        if (this.count === 0) {
          this.spinnerOverlayService.hide();
        }
      })
    );
  }
}
