export class WebResult {
  status: number;
  message: string;
  result: any;
  constructor(status: number, message: string, result: any) {
    this.status = status;
    this.message = message;
    this.result = result;
  }
}

export interface ModalSetting {
  title?: string;
  message: string;
  showClose?: boolean;
  showConfirm?: boolean;
  subTitle?: string;
  btnOK?: string;
  btnCancel?: string;
  btnForEdda?: boolean;
}

export interface DocumentResult {
  name: string;
  image: string | ArrayBuffer;
  title: string;
  css?: any;
  serial?: string;
}

export interface TermsView {
  termName: string;
  check: string;
}

export interface TermsInput {
  termTitle: string;
  termName: string;
  type: string;
}

export interface ProductList {
  ed3CommProdTitle: string;
  ed3CommSubTitle: string;
  ed3CommDesc: string;
  ed3CommDisplayDefault: string;
  ed3CommDisplayOption: string;
}

export interface ProductView {
  userType: string;
  productType: string;
  caseSource: string;
}

export interface DropdownItem {
  dataKey: string;
  dataName: string;
}

export interface TermsInput {
  termTitle: string;
  termName: string;
  type: string;
}
