import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ShowMessageComponent } from './shared/component/show-message/show-message.component';
import { ShowImageComponent } from './shared/component/show-image/show-image.component';
import { ShowHtmlComponent } from './shared/component/show-html/show-html.component';
import { HttpTokenInterceptor } from './core/interceptor';
import { SpinnerOverlayComponent } from './shared/component/spinner-overlay/spinner-overlay.component';
import { SpinnerOverlayService } from './shared/service/common/spinner-overlay.service';
import { OverlayModule } from '@angular/cdk/overlay';
import { AngularCropperjsModule } from 'angular-cropperjs';

import { NgxCleaveDirectiveModule } from 'ngx-cleave-directive';

import { fakeBackendProvider } from './mock/FakeBackendInterceptor';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './component/home/home.component';
import { ApplyLayoutComponent } from './component/apply-layout/apply-layout.component';
import { ApplyHeaderComponent } from './component/apply-header/apply-header.component';
import { ApplyFooterComponent } from './component/apply-footer/apply-footer.component';
import { CommComponent } from './component/comm/comm.component';

// 註冊語系資料
import { registerLocaleData } from '@angular/common';
import localeZhHant from '@angular/common/locales/zh-Hant';
import localeZhHantExtra from '@angular/common/locales/extra/zh-Hant';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalFactoryService } from './shared/service/common/modal-factory.service';

import { LeaveGuard } from './guard/leave.guard';
import { TokenGuard } from './guard/token.guard';

import { RouterHelperService } from './shared/service/common/router-helper.service';
import { ChatbotComponent } from './component/chatbot/chatbot.component';
import { InputMaskDirective } from './service/cleave/input-mask.directive';
import { ApplyLoggerComponent } from './component/apply-logger/apply-logger.component';

const entryComp = [
  ShowMessageComponent,
  SpinnerOverlayComponent,
  // UploadCropperComponent,
  ShowImageComponent,
  ShowHtmlComponent
];
registerLocaleData(localeZhHant, localeZhHantExtra);
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ApplyLayoutComponent,
    ApplyHeaderComponent,
    ApplyFooterComponent,
    ChatbotComponent,
    ApplyLoggerComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    AngularCropperjsModule,
    HttpClientModule,
    ReactiveFormsModule,
    CommonModule,
    OverlayModule,
    NgbModule,
    SharedModule,
    NgxCleaveDirectiveModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'zh-Hant' },
    SpinnerOverlayService,
    ModalFactoryService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpTokenInterceptor,
      multi: true
    },
    RouterHelperService,
    fakeBackendProvider,
    InputMaskDirective,
    LeaveGuard,
    TokenGuard
  ],
  entryComponents: [...entryComp],
  bootstrap: [AppComponent]
})
export class AppModule { }
