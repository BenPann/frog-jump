import { ChatbotComponent } from './component/chatbot/chatbot.component';
import { CommComponent } from './component/comm/comm.component';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ApplyLayoutComponent } from './component/apply-layout/apply-layout.component';
import { HomeComponent } from './component/home/home.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  // { path: '', component: PageNotFoundComponent },
  // apply 申請流程
  { path: 'home', component: HomeComponent },
  { path: 'chatbot', component: ChatbotComponent },
  {
    path: '',
    component: ApplyLayoutComponent,
    children: [
      { path: 'apply', loadChildren: () => import('./component/apply/apply.module').then(m => m.ApplyModule) },
      { path: 'CAD', loadChildren: () => import('./component/cad/cad.module').then(m => m.CADModule) },
      { path: 'CAL', loadChildren: () => import('./component/cal/cal.module').then(m => m.CALModule) },
      { path: 'SGN', loadChildren: () => import('./component/sgn/sgn.module').then(m => m.SGNModule) },
      { path: 'UPL', loadChildren: () => import('./component/upl/upl.module').then(m => m.UPLModule) },
    ]
  },
  // init 立約流程
  // {
  //   path: '',
  //   component: InitLayoutComponent,
  //   children: [
  //     { path: RouteGo.initPlContract.path, component: InitPlContractComponent },
  //     {
  //       path: 'o',
  //       loadChildren: () =>
  //         import('./o-user/o-user.module').then(m => m.OUserModule)
  //     },
  //     {
  //       path: 'n',
  //       loadChildren: () =>
  //         import('./n-user/n-user.module').then(m => m.NUserModule)
  //     },
  //   ]
  // },
  // { path: 'error', component: ErrorPageComponent },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'top' }),
    FormsModule
  ],
  exports: [RouterModule, FormsModule]
})
export class AppRoutingModule { }
