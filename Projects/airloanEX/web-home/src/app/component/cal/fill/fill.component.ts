import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, forkJoin, of } from 'rxjs';
import { delay, takeWhile, switchMap, tap } from 'rxjs/operators';
import { CalMain, CalMainModel } from 'src/app/component/apply/apply.interface';
import { closeDropdown, slideMove } from 'src/app/js/CAL.fill.page.js';
import { closeAllDialog, initBlueButton, initCloseButtons, initTextInputsBlur, showDialog, closeDialog } from 'src/app/js/start.page.js';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { ProductService } from 'src/app/service/product/product.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ValidateService } from 'src/app/service/validate/validate.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';

import { QryQa, QA_DATA, SEL_DATA } from 'src/app/component/apply/apply.interface';

@Component({
  selector: 'app-fill',
  templateUrl: './fill.component.html',
  styleUrls: ['./fill.component.scss']
})
export class FillComponent implements OnInit {
  private alive = true;
  uniqType = '02'; // 申請

  questList = '';
  answerList = '';
  applyQryQa: QryQa;
  loadingFunction;
  
  // /KGI/QRY_QA
  // 1. 稱呼？先生/小姐
  // 2. 年齡？n歲
  // 4. 婚姻狀態？已婚/單身
  // 3. 最高學歷？
  // 7. 工作多久了呢？
  // 6. 工作性質是…？
  // 8. 您的年收入大概有？n萬
  questAnswer = new Map([[1, ''], [2, ''], [3, ''], [4, ''], [5, ''], [6, ''], [7, ''], [8, ''], [9, ''], [10, ''], [11, ''], [12, ''], ]);
  genderOptions = [
    {name: 'gender', id: 'gender1', label: '先生', closeDropdownId: '#question1', slideMove: 2, q: '1', a: '1'},
    {name: 'gender', id: 'gender2', label: '小姐', closeDropdownId: '#question1', slideMove: 2, q: '1', a: '2'},
  ];
  marriageOptions = [
    {name: 'marriage', id: 'marriage1', label: '單身', closeDropdownId: '#question3', slideMove: 4, q: '4', a: '1'},
    {name: 'marriage', id: 'marriage2', label: '已婚', closeDropdownId: '#question3', slideMove: 4, q: '4', a: '2'},
  ];
  // schoolOptions = [
    // {name: 'school', id: 'school1', label: '國中及以下', closeDropdownId: '#question4', slideMove: 5, q: '3', a: '1'},
    // {name: 'school', id: 'school2', label: '高中/高職', closeDropdownId: '#question4', slideMove: 5, q: '3', a: '1'},
    // {name: 'school', id: 'school3', label: '大專', closeDropdownId: '#question4', slideMove: 5, q: '3', a: '2'},
    // {name: 'school', id: 'school4', label: '大學', closeDropdownId: '#question4', slideMove: 5, q: '3', a: '3'},
    // {name: 'school', id: 'school5', label: '研究所以上', closeDropdownId: '#question4', slideMove: 5, q: '3', a: '4'},
  // ];
  schoolOptions = [
    {name: 'school', id: 'school2', label: '高中/高職', closeDropdownId: '#question4', slideMove: 5, q: '3', a: '1'},
    {name: 'school', id: 'school3', label: '大專', closeDropdownId: '#question4', slideMove: 5, q: '3', a: '2'},
    {name: 'school', id: 'school4', label: '大學', closeDropdownId: '#question4', slideMove: 5, q: '3', a: '3'},
    {name: 'school', id: 'school5', label: '研究所以上', closeDropdownId: '#question4', slideMove: 5, q: '3', a: '4'},
  ];
  workOptions = [
    {name: 'work', id: 'work1', label: '未滿半年', closeDropdownId: '#question5', slideMove: 6, q: '7', a: '0.5'},
    {name: 'work', id: 'work2', label: '半年到 1 年', closeDropdownId: '#question5', slideMove: 6, q: '7', a: '1'},
    {name: 'work', id: 'work3', label: '1 - 4 年', closeDropdownId: '#question5', slideMove: 6, q: '7', a: '3'},
    {name: 'work', id: 'work4', label: '5 - 9 年', closeDropdownId: '#question5', slideMove: 6, q: '7', a: '7'},
    {name: 'work', id: 'work5', label: '10 - 15 年', closeDropdownId: '#question5', slideMove: 6, q: '7', a: '12'},
    {name: 'work', id: 'work6', label: '15 年以上', closeDropdownId: '#question5', slideMove: 6, q: '7', a: '16'},
  ];
  typeOptions = [
    {name: 'type', id: 'type1', label: '政府機關', closeDropdownId: '#question5', slideMove: 7, q: '6', a: '1'},
    {name: 'type', id: 'type2', label: '教育機關', closeDropdownId: '#question5', slideMove: 7, q: '6', a: '2'},
    {name: 'type', id: 'type3', label: '上市/上櫃/知名大企業', closeDropdownId: '#question5', slideMove: 7, q: '6', a: '3'},
    {name: 'type', id: 'type4', label: '一般企業', closeDropdownId: '#question5', slideMove: 7, q: '6', a: '4'},
    {name: 'type', id: 'type5', label: '小型/獨資公司', closeDropdownId: '#question5', slideMove: 7, q: '6', a: '5'},
    {name: 'type', id: 'type6', label: '軍職', closeDropdownId: '#question5', slideMove: 7, q: '6', a: '6'},
  ];

  ProductType = '';

  applyForm: FormGroup;

  initForm() {
    this.applyForm = this.fb.group({
      gender: ['', [Validators.required]],
      age: ['', [Validators.required, Validators.maxLength(2)]],
      marriage: ['', [Validators.required]],
      school: ['', [Validators.required]],
      work: ['', [Validators.required]],
      type: ['', [Validators.required]],
      annualIncome: ['', [Validators.required, Validators.maxLength(4)]],

    });
  }
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private productService: ProductService,
    private applyService: ApplyApiService,
    private commonDataService: CommonDataService,
    private validateService: ValidateService,
    private logger: ApplyLoggerService,

  ) { }

  ngOnInit(): void {

    this.ProductType = 'PL';
    this.userDataService.calProductType = this.ProductType;

    const logonReq = {
      idno: '',
      birthday: '',
      entry: this.activatedRoute.snapshot.queryParams.entry ? this.activatedRoute.snapshot.queryParams.entry : '',
      uniqType: this.uniqType,
    };

    this.initForm();

    this.applyService.logon(logonReq)
    .pipe(
      takeWhile(() => this.alive),
      switchMap(logonData => {
        // console.log(logonData);
        if (logonData.status === 0) {
          // @save to sessionStorage
          this.userDataService.calToken = logonData.result.token; // 'calToken'
        }
        return of(logonData);
      })
    ).subscribe(logonData => {
      initTextInputsBlur();
      initBlueButton();
      initCloseButtons();
    });

    this.userDataService.subPageName = 'Gender';

    // 送出後在後端做處理  取得EXPNO
    // this.applyService.applyQryQa({})
    // .pipe(takeWhile(() => this.alive))
    // .subscribe(data => {
    //   if (data.status === 0) {
    //     this.applyQryQa = data.result;
    //     this.applyQryQa.QA_DATA = JSON.parse(data.result.QA_DATA);
    //     this.applyQryQa.SEL_DATA = JSON.parse(data.result.SEL_DATA);
    //   }
    // });
  }

  showDialog(target) {
    showDialog(target);
  }

  closeDropdown(target, label, th) {
    closeDropdown(target, label, th.target);
  }

  slideMove(index, th) {
    slideMove(index, th.target);
  }

  cancelCalculating() {
    clearTimeout(this.loadingFunction);
  }


  keydownTel($event, name) {
    // if ($event.keyCode === 13 && name === 'question2') {
    //   this.closeDropdown('#question2', $event.target.value + '歲', $event);
    //   $event.target.blur();
    //   this.slideMove(3, $event);
    // }

    if ($event.keyCode === 13 && name === 'question7') {
      // this.closeDropdown('#question7', $event.target.value + '萬', $event);
      $event.target.blur();
      // showDialog('#dialog1');

      // 跳頁延長時間
      // this.loadingFunction = setTimeout(() => {
      closeAllDialog();
      this.onSubmit();
      // }, 4000);
    }

  }

  listQuest_Answer(quest: string, sel: string) {

    let value = sel;

    switch (quest) {
      case '2':
        // 年齡
        if (+sel >= 20 && +sel <= 25) {
          value = '23';
        } else if (+sel >= 26 && +sel <= 30) {
          value = '27';
        } else if (+sel >= 31 && +sel <= 35) {
          value = '33';
        } else if (+sel >= 36 && +sel <= 40) {
          value = '39';
        } else if (+sel >= 41 && +sel <= 45) {
          value = '43';
        } else if (+sel >= 46 && +sel <= 50) {
          value = '47';
        } else if (+sel >= 50 && +sel <= 65) {
          value = '54';
        }
        break;
      case '8':
        // 年收入
        if (+sel >= 25 && +sel <= 30) {
          value = '27';
        } else if (+sel >= 31 && +sel <= 40) {
          value = '35';
        } else if (+sel >= 41 && +sel <= 50) {
          value = '45';
        } else if (+sel >= 51 && +sel <= 60) {
          value = '55';
        } else if (+sel >= 61 && +sel <= 70) {
          value = '65';
        } else if (+sel >= 71 && +sel <= 80) {
          value = '75';
        } else if (+sel >= 81 && +sel <= 90) {
          value = '85';
        } else if (+sel >= 91 && +sel <= 100) {
          value = '95';
        } else if (+sel >= 101 && +sel <= 150) {
          value = '125';
        } else if (+sel >= 151 && +sel <= 200) {
          value = '175';
        } else if (+sel >= 201 && +sel <= 300) {
          value = '250';
        } else if (+sel >= 300) {
          value = '300';
        }
        break;
      default:
        break;
    }
    this.questAnswer.set(+quest, value);

    // this.questList += ';';
    // this.answerList += ';';
  }

  isBadAge(ans_2: string) {
    if (!ans_2 || +ans_2 < 20) {
      this.logger.showMsg({status: 99, message: '您的年齡須大於20歲', result: ''});
      return true;
    } else if (+ans_2 > 65) {
      this.logger.showMsg({status: 99, message: '您的年齡須小於65歲', result: ''});
      return true;
    }
    return false;
  }

  isGoodAge(ans_2: string) {
    return !this.isBadAge(ans_2);
  }
  
  // 1. 稱呼？先生/小姐
  changeGender($event, item) {
    this.closeDropdown(item.closeDropdownId, item.label, $event);
    this.slideMove(item.slideMove, $event);
    this.listQuest_Answer(item.q, item.a);
    this.userDataService.subPageName = 'Age';
  }
  // 2. 年齡？n歲
  changeAge($event) {
    const ans_2 = $event.target.value;
    if (this.isGoodAge(ans_2)) {
      this.closeDropdown('#question2', $event.target.value+'歲', $event);
      this.slideMove(3, $event);
      this.listQuest_Answer('2',$event.target.value);
      this.userDataService.subPageName = 'Marital_Status';
    }
  }
  // 4. 婚姻狀態？已婚/單身
  changeMarital_Status($event, item) {
    this.closeDropdown(item.closeDropdownId, item.label, $event);
    this.slideMove(item.slideMove, $event);
    this.listQuest_Answer(item.q, item.a);
    this.userDataService.subPageName = 'Education';
  }
  // 3. 最高學歷？
  changeEducation($event, item) {
    this.closeDropdown(item.closeDropdownId, item.label, $event);
    this.slideMove(item.slideMove, $event);
    this.listQuest_Answer(item.q, item.a);
    this.userDataService.subPageName = 'Job_Tenure';
  }
  // 7. 工作多久了呢？
  changeJob_Tenure($event, item) {
    this.closeDropdown(item.closeDropdownId, item.label, $event);
    this.slideMove(item.slideMove, $event);
    this.listQuest_Answer(item.q, item.a)
    this.userDataService.subPageName = 'Occupation';
  }
  // 6. 工作性質是…？
  changeOccupation($event, item) {
    this.closeDropdown(item.closeDropdownId, item.label, $event);
    this.slideMove(item.slideMove, $event);
    this.listQuest_Answer(item.q, item.a);
    this.userDataService.subPageName = 'Annual_Income';
  }
  // 8. 您的年收入大概有？n萬
  changeAnnual_Income($event) {
    this.closeDropdown('#question7', $event.target.value+'萬', $event);
    this.listQuest_Answer('8',$event.target.value)
  }

  onSubmit() {

    const ans_2 = this.questAnswer.get(2); // 年齡
    // console.log('ans_2' + +ans_2);
    if (this.isBadAge(ans_2)) {
      return;
    }

    const ans_8 = this.questAnswer.get(8); // 年收入
    if (!ans_8 || +ans_8 < 25) {
      this.logger.showMsg({status: 99, message: '您的年收入須大於25萬', result: ''});
      return;
    }
    
    this.questAnswer.forEach((value, key) => {
      this.questList += key + ';';
      this.answerList += value + ';';
    });

    const req = {
      // CASE_NO: 'EXP' + 'this.applyQryQa.CASE_NO',
      QUEST_LIST: this.questList,
      ANSWER_LIST: this.answerList,
      ProductType: this.ProductType,
    };
    
    this.applyService.applySendAws(req)
    .pipe(
      takeWhile(() => this.alive),
      tap(_ => showDialog('#dialog1')),
      delay(7000),
      tap(_ => closeDialog()),
    ).subscribe(data => {
        // console.log(data);
        if (data.status === 0) {
          
          this.userDataService.subPageName = ''; // reset subPageName

          let main: CalMain;
          if (this.userDataService.calMain) {
            main = this.userDataService.calMain;
          } else {
            main = new CalMainModel();
          }
          Object.assign(main, data.result);
          this.userDataService.calMain = main;
          this.routerHelperService.navigate(['/CAL/result']);
        } else {
          this.logger.showMsg(data);
        }
      });
  } // end onSubmit
}
