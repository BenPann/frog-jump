import { Router } from '@angular/router';
import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { initTextInputsBlur, initBlueButton, initAllCloseButtons } from 'src/app/js/start.page.js';
import { rangeMoving, initRange } from 'src/app/js/advance.page.js';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { CalcIRRQuick } from 'src/app/js/calcIRR.js';
import {UserDataService} from "../../../service/user-data/user-data.service";

@Component({
  selector: 'app-advance',
  templateUrl: './advance.component.html',
  styleUrls: ['./advance.component.scss']
})
export class AdvanceComponent implements OnInit {

  @Input() inputData;
  @Output() subClose = new EventEmitter();

  creditLine = 0;
  interestRate = 0;
  payValue = '';
  year = 0;
  
  applyForm: FormGroup;
  initForm() {
    this.applyForm = this.fb.group({
      amount: ['70'],
      year: ['7'],
    });
  }

  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
  ) { }

  ngOnInit(): void {
    this.initForm();
    initTextInputsBlur();
    initBlueButton();
    initAllCloseButtons();

    this.creditLine = this.inputData.creditLine;
    this.interestRate = this.inputData.interestRate;
    this.payValue = this.inputData.payValue;
    this.year = this.inputData.year;

    setTimeout(() => {
      initRange();
    }, 1);
  }

  initRange() {
    initRange();
    const quick = CalcIRRQuick(this.creditLine, this.year, this.year * 12, this.interestRate, 0, 8000);
    this.payValue = Math.round(+quick.Period1MonthlyPay).toLocaleString();
  }

  close() {
    const a = {
      sub: false,
      creditLine: this.creditLine,
      year: this.year,
    };
    this.subClose.emit(a);
  }

  onSubmit() {
    this.userDataService.subPageName="";
    this.routerHelperService.navigate(['/apply/start']);
  }
}
