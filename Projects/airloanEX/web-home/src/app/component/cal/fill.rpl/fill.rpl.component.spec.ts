import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillrplComponent } from './fill.rpl.component';

describe('FillrplComponent', () => {
  let component: FillrplComponent;
  let fixture: ComponentFixture<FillrplComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillrplComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FillrplComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
