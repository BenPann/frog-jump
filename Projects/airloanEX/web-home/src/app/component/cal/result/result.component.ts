import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { initTextInputsBlur, initBlueButton, initCloseButtons, showDialog, initCleaveNumber, initAllCloseButtons, initCleaveMoney } from 'src/app/js/start.page.js';
import { controlFloating, fillAmount, loanRange } from 'src/app/js/result.page.js';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { ProductService } from 'src/app/service/product/product.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ValidateService } from 'src/app/service/validate/validate.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { CalMain } from 'src/app/component/apply/apply.interface';
import { CalcIRRQuick } from 'src/app/js/calcIRR.js';

import { takeWhile } from 'rxjs/operators';
@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  private alive = true;
  
  sub = false; // 開啟進階試算

  hideTarget = true;
  textContent = '查看方案詳情';
  year = 7;

  payValue = '';
  viewData = {};
  
  CASE_NO = '';
  creditLine = 0;
  interestRate = 0;

  terms = [];
  dialogTitle: string;
  dialogData: string;

  applyForm: FormGroup;
  initForm() {
    this.applyForm = this.fb.group({
      phone: ['', [Validators.required, Validators.maxLength(10), Validators.pattern(/^09\d{8}$/)]],
    });
  }

  constructor(
    public cleave: CleaveService,
    private fb: FormBuilder,
    private router: Router,
    private routerHelperService: RouterHelperService,
    public userDataService: UserDataService,
    private productService: ProductService,
    private applyService: ApplyApiService,
    private commonDataService: CommonDataService,
    private validateService: ValidateService,
  ) { }

  ngOnInit(): void {
    this.initForm();
    // initTextInputsBlur();
    // initBlueButton();
    initAllCloseButtons();
    initCloseButtons();
    initCleaveNumber();
    initCleaveMoney();

    const calMain: CalMain = this.userDataService.calMain;
    this.userDataService.subPageName="Est_Processing";
    this.CASE_NO = calMain.CASE_NO;
    this.creditLine = +calMain.CREDIT_LINE;
    this.interestRate = +calMain.INTEREST_RATE;
    this.loanRange();

    this.initTerms();
  }

  initTerms(): void {
    this.applyService.getTermList().subscribe(data => {
      if (data) {
        data.forEach(value => {
          this.terms[value.type] = { name: value.termName, title: value.termTitle };
        });
      }
    });
  }

  // 取得條款內容
  showTerms(target: string, termsName) {
    this.applyService
      .getTerms(termsName.name)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        try {
          // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
          const json = JSON.parse(data);
          this.dialogTitle = '查詢條款失敗';
          this.dialogData = '';
        } catch (error) {
          this.dialogTitle = termsName.title;
          this.dialogData = data;
          showDialog(target);
        }
      });
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  showDialog(target) {
    showDialog(target);
  }

  controlFloating(th) {
    controlFloating(th);
  }

  sendSms() {
    const phone = this.applyForm.get('phone').value;
    this.applyService.applyCalResult(phone, this.CASE_NO)
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      if (data.status === 0) {
        showDialog('#dialog3')
      }
    });
  }

  detailsToggle() {
    if (this.hideTarget) {
      this.hideTarget = false;
      this.textContent = '隱藏方案';
    } else {
      this.hideTarget = true;
      this.textContent = '查看方案詳情';

    }
  }

  loanRange() {
    const quick = CalcIRRQuick(this.creditLine, this.year, this.year * 12, this.interestRate, 0, 8000);
    this.payValue = Math.round(+quick.Period1MonthlyPay).toLocaleString();
  }

  setViewData() {
    return this.viewData = {
      creditLine: this.creditLine,
      interestRate: this.interestRate,
      year: this.year,
      payValue: this.payValue,
    };
  }

  setMode(event) {
    this.sub = event.sub;
    if(this.sub === false){ this.userDataService.subPageName="Est_Processing" };
    this.creditLine = event.creditLine;
    this.year = event.year;
    this.loanRange();

  }
  onSubmit() {
    // [routerLink]="['/apply/start']" [queryParams]="{ id:CASE_NO }"
    // this.routerHelperService.navigate(['/apply/start'], {queryParams: {id: this.CASE_NO}});
    this.userDataService.subPageName = "";
    this.routerHelperService.navigate(['/apply/start']);
  }

}
