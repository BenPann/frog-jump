import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancerplComponent } from './advancerpl.component';

describe('AdvancerplComponent', () => {
  let component: AdvancerplComponent;
  let fixture: ComponentFixture<AdvancerplComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvancerplComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancerplComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
