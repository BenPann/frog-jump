import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { initRange, rangeMoving } from 'src/app/js/advance.page.js';
import { initTextInputsBlur, initBlueButton, initAllCloseButtons } from 'src/app/js/start.page.js';
import {UserDataService} from "../../../service/user-data/user-data.service";

@Component({
  selector: 'app-advancerpl',
  templateUrl: './advancerpl.component.html',
  styleUrls: ['./advancerpl.component.scss']
})
export class AdvancerplComponent implements OnInit {

  @Input() inputData;
  @Output() subClose = new EventEmitter();

  creditLine = 0;
  interestRate = 0;
  totalPay = 0;

  applyForm: FormGroup;
  initForm() {
    this.applyForm = this.fb.group({
      amount: ['1'],
      rpldays: ['1'],

    });
  }

  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
  ) { }

  ngOnInit(): void {
    this.initForm();
    initTextInputsBlur();
    initBlueButton();
    initAllCloseButtons();

    this.applyForm.get('amount').setValue(this.inputData.creditLine);
    this.applyForm.get('rpldays').setValue(this.inputData.rpldays);
    this.interestRate = this.inputData.interestRate;

    setTimeout(() => {
      initRange();
    }, 1);

    this.calc();
  }

  rangeMoving(th) {
    rangeMoving(th);    
    this.calc();
  }

  calc() {
    this.totalPay = this.precisionRoundMod(((this.applyForm.get('amount').value * 10000) * (this.interestRate/100) / 365 * this.applyForm.get('rpldays').value), 1); // 1.6是參數，計算方式=10,000*12.99%(依借款利率)/365 (四捨五入小數第一位顯示)
  }

  precisionRoundMod(number, precision) {
    var factor = Math.pow(10, precision);
    var n = precision < 0 ? number : 0.01 / factor + number;
    return Math.round( n * factor) / factor;
  }

  close() {
    const a = {
      sub: false
    };
    this.subClose.emit(a);
  }

  onSubmit() {
    this.userDataService.subPageName="";
    this.routerHelperService.navigate(['/apply/start']);
  }

}
