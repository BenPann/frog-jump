import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CALRoutingModule } from './cal-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FillComponent } from './fill/fill.component';
import { FillrplComponent } from './fill.rpl/fill.rpl.component';
import { ResultComponent } from './result/result.component';
import { ResultrplComponent } from './result.rpl/resultrpl.component';
import { AdvanceComponent } from './advance/advance.component';
import { AdvancerplComponent } from './advancerpl/advancerpl.component';
import { NgxCleaveDirectiveModule } from 'ngx-cleave-directive';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    FillComponent,
    FillrplComponent,
    ResultComponent,
    AdvanceComponent,
    ResultrplComponent,
    AdvancerplComponent,
  ],
  imports: [
    CommonModule,
    CALRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCleaveDirectiveModule,
    SharedModule,
  ]
})
export class CALModule { }
