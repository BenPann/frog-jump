import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultrplComponent } from './resultrpl.component';

describe('ResultrplComponent', () => {
  let component: ResultrplComponent;
  let fixture: ComponentFixture<ResultrplComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultrplComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultrplComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
