import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { controlFloating, fillAmount } from 'src/app/js/result.page.js';
import { initTextInputsBlur, initBlueButton, initAllCloseButtons, initCloseButtons, initCleaveNumber, showDialog } from 'src/app/js/start.page.js';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { ProductService } from 'src/app/service/product/product.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ValidateService } from 'src/app/service/validate/validate.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { CalMain } from 'src/app/component/apply/apply.interface';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-resultrpl',
  templateUrl: './resultrpl.component.html',
  styleUrls: ['./resultrpl.component.scss']
})
export class ResultrplComponent implements OnInit {

  private alive = true;

  sub = false; // 開啟進階試算

  viewData = {};

  applyForm: FormGroup;

  hideTarget = true;
  textContent = '查看方案詳情';
  CASE_NO = '';
  amountValue = 0;
  interestRate = 0;
  rpldays = 1;
  dailyPay = 0;

  terms = [];
  dialogTitle: string;
  dialogData: string;

  initForm() {
    this.applyForm = this.fb.group({
      phone: ['', [Validators.required, Validators.maxLength(10), Validators.pattern(/^09\d{8}$/)]],
      amountInput: [this.amountValue, [Validators.required, Validators.maxLength(3)]],
    });
  }

  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private routerHelperService: RouterHelperService,
    public userDataService: UserDataService,
    private productService: ProductService,
    private applyService: ApplyApiService,
    private commonDataService: CommonDataService,
    private validateService: ValidateService,
  ) { }

  ngOnInit(): void {

    const calMain: CalMain = this.userDataService.calMain;
    this.userDataService.subPageName="Est_Processing";
    this.CASE_NO = calMain.CASE_NO;
    this.amountValue = +calMain.CREDIT_LINE;
    this.interestRate = +calMain.INTEREST_RATE;
    this.dailyPay = this.precisionRoundMod(10000 * (this.interestRate/100) / 365, 1); // 1.6是參數，計算方式=10,000*12.99%(依借款利率)/365 (四捨五入小數第一位顯示)

    this.initForm();

    this.initTerms();
  }

  ngAfterViewInit(): void {
    // initTextInputsBlur();
    // initBlueButton();
    initAllCloseButtons();
    initCloseButtons();
    initCleaveNumber();
  }

  initTerms(): void {
    this.applyService.getTermList().subscribe(data => {
      console.log(data);
      if (data) {
        data.forEach(value => {
          this.terms[value.type] = { name: value.termName, title: value.termTitle };
        });
      }
    });
  }

  // 取得條款內容
  showTerms(target: string, termsName) {
    this.applyService
      .getTerms(termsName.name)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        try {
          // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
          const json = JSON.parse(data);
          this.dialogTitle = '查詢條款失敗';
          this.dialogData = '';
        } catch (error) {
          this.dialogTitle = termsName.title;
          this.dialogData = data;
          showDialog(target);
        }
      });
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  precisionRoundMod(number, precision) {
    var factor = Math.pow(10, precision);
    var n = precision < 0 ? number : 0.01 / factor + number;
    return Math.round( n * factor) / factor;
  }

  showDialog(target) {
    showDialog(target);
  }

  controlFloating(th) {
    controlFloating(th);
  }

  sendSms() {
    const phone = this.applyForm.get('phone').value;
    this.applyService.applyCalResult(phone, this.CASE_NO)
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      if (data.status === 0) {
        showDialog('#dialog3')
      }
    });
  }

  detailsToggle() {
    if (this.hideTarget) {
      this.hideTarget = false;
      this.textContent = '隱藏方案';
    } else {
      this.hideTarget = true;
      this.textContent = '查看方案詳情';
    }
  }

  setViewData() {
    return this.viewData = {
      creditLine: this.amountValue,
      interestRate: this.interestRate,
      rpldays: this.rpldays,
    };
  }

  setMode(event) {
    this.sub = event.sub;
    if(this.sub === false){ this.userDataService.subPageName="Est_Processing" };
  }

  fillAmount() {
    this.applyForm.get('amountInput').patchValue(this.amountValue);
  }
  onSubmit() {
    this.userDataService.subPageName = "";
    this.routerHelperService.navigate(['/apply/start'], {queryParams: {PT: '3'}});
  }

}
