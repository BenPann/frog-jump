import { AdvancerplComponent } from './advancerpl/advancerpl.component';
import { AdvanceComponent } from './advance/advance.component';
import { ResultComponent } from './result/result.component';
import { ResultrplComponent } from './result.rpl/resultrpl.component';
import { FillComponent } from './fill/fill.component';
import { FillrplComponent } from './fill.rpl/fill.rpl.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CalTokenGuard } from 'src/app/guard/cal-token.guard';
import { LeaveGuard } from 'src/app/guard/leave.guard';

const routes: Routes = [
  {
    path: 'fill',
    component: FillComponent,
    // canActivate: [CalTokenGuard],
    // canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'fill.rpl',
    component: FillrplComponent,
    // canActivate: [CalTokenGuard],
    // canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'result',
    component: ResultComponent,
    canActivate: [CalTokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
    },
  {
    path: 'result.rpl',
    component: ResultrplComponent,
    canActivate: [CalTokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
    },
  {
    path: 'advance',
    component: AdvanceComponent,
    canActivate: [CalTokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
    },
  {
    path: 'advance.rpl',
    component: AdvancerplComponent,
    canActivate: [CalTokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CALRoutingModule { }
