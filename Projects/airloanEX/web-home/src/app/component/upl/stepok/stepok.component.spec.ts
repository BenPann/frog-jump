import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepokComponent } from './stepok.component';

describe('StepokComponent', () => {
  let component: StepokComponent;
  let fixture: ComponentFixture<StepokComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepokComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepokComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
