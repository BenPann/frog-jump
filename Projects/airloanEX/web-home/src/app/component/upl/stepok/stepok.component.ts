import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';

@Component({
  selector: 'app-stepok',
  templateUrl: './stepok.component.html',
  styleUrls: ['./stepok.component.scss']
})
export class StepokComponent implements OnInit {
  private alive = true;
  private ref;

  applyForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private routerHelperService: RouterHelperService,
    private logger: ApplyLoggerService,
    private applyService: ApplyApiService,
    private userDataService: UserDataService
  ) { }

  initForm() {
    this.applyForm = this.fb.group({
      email_address: [''],
    });
  }

  ngOnInit(): void {
    this.initForm();
    const main = this.userDataService.applyMain;
    this.applyForm.patchValue(main);

    console.log("ok this.logger.isCSRT = " + this.logger.isCSRT);
    if(this.logger.isCSRT){
      this.ref = setInterval(() => { window.location.href = sessionStorage.getItem("rtRedirectUrl"); }, 1000 * 5);
    }
  }

  clickSGNback() {
    this.routerHelperService.navigate(['/SGN/back']);
  }

  onSubmit() {
  }
}
