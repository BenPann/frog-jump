import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { initTextInputsBlur, initBlueButton, initCloseButtons, showDialog } from 'src/app/js/start.page.js';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class Step2Component implements OnInit {

  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
  ) { }

  ngOnInit(): void {
  }

  clickUploadLater() {
    this.routerHelperService.navigate(['/UPL/later']);
  }

  onSubmit() {
    this.routerHelperService.navigate(['/UPL/step3']);
  }
}
