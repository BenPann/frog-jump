import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { takeWhile } from 'rxjs/operators';
import { initTextInputsBlur, initBlueButton, initCloseButtons, showDialog } from 'src/app/js/start.page.js';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';

@Component({
  selector: 'app-later',
  templateUrl: './later.component.html',
  styleUrls: ['./later.component.scss']
})
export class LaterComponent implements OnInit {

  private alive = true;

  phone: string;

  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private logger: ApplyLoggerService,
    private applyService: ApplyApiService,
  ) { }

  ngOnInit(): void {
    const main = this.userDataService.applyMain;
    this.phone = main.mobile_tel;
  }

  ngAfterViewInit(): void {
    initTextInputsBlur();
    initBlueButton();
    initCloseButtons();
  }

  showDialog(target) {
    this.applyService.applyLaterAddon(this.phone)
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      if (data.status === 0) {
        showDialog(target);
      } else {
        this.logger.showMsg(data.result);
      }
    });
  }

  onSubmit() {
    this.routerHelperService.navigate(['/UPL/step1']);
  }
}
