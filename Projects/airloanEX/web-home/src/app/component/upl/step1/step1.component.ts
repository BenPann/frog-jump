import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';
import { DocumentResult } from 'src/app/interface/resultType';
import { cityjson, countiesOptions } from 'src/app/js/person.page.js';
import { initAllCloseButtons, initCloseButtons, showDialog } from 'src/app/js/start.page.js';
import { cropper, fillImage, removeImage, showConfDialog, showCropDialog } from 'src/app/js/step1.page.js';
import { PhotoView } from 'src/app/service/apply-account/apply-account.interface';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { IDCardResponse, OtherDocumentResponse } from 'src/app/shared/service/document/document.interface';
import { DocumentService } from 'src/app/shared/service/document/document.service';

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss']
})
export class Step1Component implements OnInit {

  private alive = true;
  applyForm: FormGroup;

  // @ViewChild('counties', {static: false}) counties: ElementRef;
  // @ViewChild('township', {static: false}) township: ElementRef;

  name = ''; // 姓名
  marriage = '未婚';

  isFront = true;
  imagePath = null;

  imageFrontIdcard: string; // base64
  imageBackIdcard: string; // base64

  // 要傳入app-upload的身分證正面資料
  frontIdDoc: DocumentResult = {
    name: 'frontId',
    image: '',
    title: '',
    serial: '',
  };
  // 要傳入app-upload的身分證反面資料
  backIdDoc: DocumentResult = {
    name: 'backId',
    image: '',
    title: '',
    serial: '',
  };
  // 要傳入app-upload的第二證件資料
  secCardIdDoc: DocumentResult = {
    name: 'secCardId',
    image: '',
    title: '',
    serial: '',
  };

  cityOption = [
    { dataKey: '', dataName: '請選擇' },
    { dataKey: '北縣', dataName: '北縣' },
    { dataKey: '宜縣', dataName: '宜縣' },
    { dataKey: '桃縣', dataName: '桃縣' },
    { dataKey: '竹縣', dataName: '竹縣' },
    { dataKey: '苗縣', dataName: '苗縣' },
    { dataKey: '中縣', dataName: '中縣' },
    { dataKey: '彰縣', dataName: '彰縣' },
    { dataKey: '投縣', dataName: '投縣' },
    { dataKey: '雲縣', dataName: '雲縣' },
    { dataKey: '嘉縣', dataName: '嘉縣' },
    { dataKey: '南縣', dataName: '南縣' },
    { dataKey: '高縣', dataName: '高縣' },
    { dataKey: '屏縣', dataName: '屏縣' },
    { dataKey: '東縣', dataName: '東縣' },
    { dataKey: '花縣', dataName: '花縣' },
    { dataKey: '澎縣', dataName: '澎縣' },
    { dataKey: '基市', dataName: '基市' },
    { dataKey: '竹市', dataName: '竹市' },
    { dataKey: '嘉市', dataName: '嘉市' },
    { dataKey: '連江', dataName: '連江' },
    { dataKey: '金門', dataName: '金門' },
    { dataKey: '北市', dataName: '北市' },
    { dataKey: '高市', dataName: '高市' },
    { dataKey: '新北市', dataName: '新北市' },
    { dataKey: '中市', dataName: '中市' },
    { dataKey: '南市', dataName: '南市' },
    { dataKey: '桃市', dataName: '桃市' },
  ];

  idcardCRecordOption = [
    { dataKey: '', dataName: '請選擇' },
    { dataKey: '1', dataName: '初發' },
    { dataKey: '2', dataName: '補發' },
    { dataKey: '3', dataName: '換發' },
  ];

  options = countiesOptions;
  cityOption2 = [{ dataKey: '', dataName: '鄉鎮市區' }];


  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService,
    private documentService: DocumentService,
    private applyService: ApplyApiService,
    private userDataService: UserDataService,
    private commonDataService: CommonDataService,
    private logger: ApplyLoggerService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  ngAfterViewInit(): void {
    // initTextInputsBlur();
    // initBlueButton();
    initCloseButtons();
    initAllCloseButtons();
  }

  initForm() {
    this.applyForm = this.fb.group({
      idcardDate: ['', [Validators.required, Validators.maxLength(9), verifyIdcardDate]],
      idcardLocation: [''],
      idcardCRecord: [''],
      counties: [''],
      township: [''],
      address: [''],
    });
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  clickUploadLater() {
    this.routerHelperService.navigate(['/UPL/later']);
  }

  cropper() {
    cropper.rotate(-90);
  }

  showDialog(target) {
    showDialog(target);
  }

  removeImage(th) {
    removeImage(th);
  }

  showCropDialog(target, th) {
    showCropDialog(target, th);
  }

  showConfDialog(th, img?) {
    showConfDialog(this.isFront);
    if (this.isFront) {
      this.getFrontIdImg(fillImage(th), img);
    } else {
      this.getBackIdImg(fillImage(th));
    }
  }

  makeDistrict(value) {

    if (cityjson[value] === undefined) {
      this.applyForm.get('counties').patchValue('');
    }

    this.cityOption2 = [{ dataKey: '', dataName: '鄉鎮市區' }];
    if (value && cityjson[value]) {
      this.cityOption2 = this.cityOption2.concat(cityjson[value]);
    } else {
      this.applyForm.get('township').patchValue('');
    }
  }

  // 取得身分證正面
  getFrontIdImg(base64Image, img?): void {
    img.src = base64Image;
    const photoView: PhotoView = {
      sType: '1',
      base64Image,
      subSerial: this.frontIdDoc.serial
    };
    this.documentService
      .upload(photoView)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          const result: IDCardResponse = data.result;
          this.frontIdDoc = {
            name: 'frontId',
            image: result.waterMarkImage,
            title: '身分證正面',
            serial: result.subSerial
          };
          let date = result.idCard_Issue_DT;
          if (data && date.length === 7) {
            if (date.startsWith('0')) { // 民國:#ee/MM/dd
              date = ('' + date.substr(1, 2) + '/' + date.substr(3, 2) + '/' + date.substr(5, 2));
            } else {
              date = ('' + date.substr(0, 3) + '/' + date.substr(3, 2) + '/' + date.substr(5, 2)); 
            }
          } else {
            date = ('');
          }
          this.applyForm.get('idcardDate').patchValue(date);
          this.applyForm.get('idcardLocation').patchValue(result.idCard_Issue_City);
          this.applyForm.get('idcardCRecord').patchValue(result.idCard_Issue_Type);
        } else {
          // TODO: show 錯誤訊息
          //若身分證辨識後為空值，或是要顯示MODEL時訊息為空，則一律顯示訊息[身分證無法辨識，請重新上傳較為清晰之身分證圖片]
          if(data.message === "" || data.message === null){
            alert("身分證無法辨識，請重新上傳較為清晰之身分證圖片");
          }else{
            alert(data.message);
          }
        }
      });
    this.applyForm.get('idcardDate').markAsTouched();
    this.applyForm.get('idcardLocation').markAsTouched();
    this.applyForm.get('idcardCRecord').markAsTouched();
  } // end getFrontIdImg

  // 取得身分證反面
  getBackIdImg(base64Image): void {
    const photoView: PhotoView = {
      sType: '2',
      base64Image,
      subSerial: this.backIdDoc.serial
    };


    this.documentService
      .upload(photoView)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          const result: IDCardResponse = data.result;
          this.backIdDoc = {
            name: 'backId',
            image: result.waterMarkImage,
            title: '身分證反面',
            serial: result.subSerial
          };
          this.marriage = result.marriage;
          let address: string = result.householdAddr1 + result.householdAddr2 + result.householdAddr3 + result.householdAddr4;
          address = address.replace(/[A-z]/g, '');
          // 取 [縣|市] 位址
          const counties_index = address.search(/[ '縣' || '市']/) + 1;
          // 取 [鄉|市|鎮|區|臺] 位址
          const township_index = address.substring(counties_index).search(/[ '鄉' || '市' || '鎮' || '區' || '臺']/) + 1;
          // 是否有 [縣|市｜鄉|市|鎮|區|臺]
          if (counties_index + township_index === -1) {
            // 地址資料
            this.applyForm.get('address').patchValue(address);
          } else {
            // 地址資料
            this.applyForm.get('address').patchValue(address.substring(counties_index + township_index));
            // 缺割出 [鄉|市|鎮|區|臺] 資料
            const township = address.substring(counties_index, counties_index + township_index);
            // 依照 [鄉|市|鎮|區|臺] 找出 [縣|市] 資料
            Object.values(cityjson).filter((m, i) => {
              const name = m.filter(f => f.dataName === township);
              if (name.length) {
                // [鄉|市|鎮|區|臺] 資料
                this.applyForm.get('township').patchValue(name[0].dataKey);
                const countiesValue = Object.keys(cityjson)[i];
                // [縣|市] 資料
                this.applyForm.get('counties').patchValue(countiesValue);
                this.makeDistrict(countiesValue);
                return m;
              }
            });
          }
        } else {
            // 上傳失敗
            this.logger.showMsg(data);
        }
      });
    this.applyForm.get('township').markAsTouched();
    this.applyForm.get('counties').markAsTouched();
    this.applyForm.get('address').markAsTouched();
  } // end getBackIdImg

  // 取得第二證件反面
  getSecCardImg(e: DocumentResult): void {
    const photoView: PhotoView = {
      sType: '3',
      base64Image: e.image,
      subSerial: this.secCardIdDoc.serial
    };
    this.documentService
      .upload(photoView)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          const result: OtherDocumentResponse = data.result;
          this.secCardIdDoc = {
            name: 'secCardId',
            image: result.waterMarkImage,
            title: '第二證件',
            serial: result.subSerial
          };
        } else {
            // 上傳失敗
            this.logger.showMsg(data);
        }
      });
  } // end getSecCardImg

  // /**
  //  * 上傳檔案
  //  */
  // saveFile(index: number, target) {
  //   if (target.files.length > 0) {
  //     const file = target.files[0];
  //     this.service_saveFile(file).subscribe(
  //       result => {
  //         description: result;
  //       }
  //     );
  //   }
  // }

  // service_saveFile(file: File): Observable<any> {
  //   const reader = new FileReader();
  //   reader.readAsDataURL(file);
  //   return new Observable(observer => {
  //     reader.onloadend = () => {
  //       let text = '';
  //       if (reader.result) {
  //         const array = reader.result.toString().split(',');
  //         if (array.length > 1) { text = array[1]; }
  //       }
  //       observer.next(text);
  //       observer.complete();
  //     };
  //   });
  // }

  onSubmit() {

    // const countiesEle = this.counties.nativeElement;
    // const townshipEle = this.township.nativeElement;
    // // 縣市名稱，例如：台北市
    // const countiesName = countiesEle.options[countiesEle.options.selectedIndex].text;
    // // 鄉鎮市區名稱，例如：大同區
    // const townshipName = townshipEle.options[countiesEle.options.selectedIndex].text;
    const ts = this.applyForm.get('township').value;
    const townshipObject = this.cityOption2.filter(v => +v.dataKey === +ts)[0];
    const townshipName = townshipObject ? townshipObject.dataName : '';

    // eeeMMdd
    let idcardDateString: string = this.applyForm.get('idcardDate').value;
    idcardDateString = idcardDateString.replace('民國', '').replace(/\//gi, '').trim();
    idcardDateString = idcardDateString.length === 6 ? '0' + idcardDateString : idcardDateString;

    const req = {
      idcardDate : idcardDateString,
      idcardLocation : this.applyForm.get('idcardLocation').value,
      idcardCRecord : this.applyForm.get('idcardCRecord').value,
      countiesName: this.applyForm.get('counties').value,
      townshipName,
      township : this.applyForm.get('township').value,
      address : this.applyForm.get('address').value,
      marriage : this.marriage
    };
    
    // 上傳身份證正反及發查戶役政
    this.applyService.applyUploadIdcard(req)
        .pipe(takeWhile(() => this.alive))
        .subscribe(data => {
			
			console.log(data);
			
			if (data.status === 0) {

			  if (this.logger.isCSRT && sessionStorage.getItem('prjCode') === "D00050"){
				
				this.applyService.applyStepok()
				.pipe(takeWhile(() => this.alive))
				.subscribe(data3 => {
				  if (data3.status === 0) {
					this.routerHelperService.navigate(['/UPL/stepok']);
				  } else {
					this.logger.showMsg(data3.result); 
				  }
				});

			  } else {

				this.applyService.mustUploadFinpf()
				.pipe(takeWhile(() => this.alive))
				.subscribe(data2 => {
				  if (data2.status === 0 && data2.result) {
					if (data2.result.mustUploadFinpf) {
					  this.routerHelperService.navigate(['/UPL/step2']);
					} else {
					  this.applyService.applyStepok()
					  .pipe(takeWhile(() => this.alive))
					  .subscribe(data3 => {
						if (data3.status === 0) {
						  this.routerHelperService.navigate(['/UPL/stepok']);
						} else {
						  this.logger.showMsg(data3.result); 
						}
					  });
					}
				  } else {
					this.routerHelperService.navigate(['/UPL/step2']);
				  }
				});

			  }
			} else {
			  this.logger.showMsg(data.result);
			}
		});
  }

}

/**
 * 檢核身分證換發日期 不得於未來日期 一些基本檢核
 */
export function verifyIdcardDate(control: FormControl): ValidationErrors {
    const birthday = control;
    const birthdayTxyArray = birthday.value.split('/');
    if (birthday && birthday.value) {
        console.log(birthday.value.split('/').length);
        if (birthdayTxyArray.length === 3) {
            try {
                const changeDate = new Date( (birthdayTxyArray[0] * 1) + 1911, birthdayTxyArray[1] - 1, birthdayTxyArray[2]);
                console.log('changeDaete' + changeDate);
                const date = new Date();

                console.log('date:' + date);
                if (changeDate.getTime() > date.getTime()) {
                    console.log('changeDateError');
                    return {forbiddenName: {value: birthday.value}, error: true};
                }
            } catch (e) {
                console.log('e' + e);
                return {forbiddenName: {value: birthday.value}, error: true};
            }

            if (birthdayTxyArray[1] > 12 || birthdayTxyArray[1] < 1 || birthdayTxyArray[2] < 1 || birthdayTxyArray[2] > 31) {
                return {forbiddenName: {value: birthday.value}, error: true};
            }

            return null;
        } else {
            console.log('changeDateError');
            return {forbiddenName: {value: birthday.value}, error: true};
        }

    } else {
        console.log('changeDateError');
        return {forbiddenName: {value: birthday.value}, error: true};
    }
}
