import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';
import { initTextInputsBlur, initBlueButton, initCloseButtons, showDialog, initCleaveNumber, initCleaveUpper } from 'src/app/js/start.page.js';
import { idValidate, dataToggle, selectActive } from 'src/app/js/step4.page.js';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { DropdownItem } from 'src/app/interface/resultType';
import { ApplyMain, ApplyMainModel } from 'src/app/component/apply/apply.interface';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';

@Component({
  selector: 'app-step4',
  templateUrl: './step4.component.html',
  styleUrls: ['./step4.component.scss']
})
export class Step4Component implements OnInit {

  private alive = true;
  applyForm: FormGroup;

  /*relationshipOptions = [
    { key: '', value: '請選擇' },
    { key: '配偶', value: '配偶' },
    { key: '子女', value: '子女' },
    { key: '父親', value: '父親' },
    { key: '母親', value: '母親' },
  ];
  newTypeOption = [
    { key: '', value: '請選擇' },
    { key: '本人擔任負責人之企業資料', value: '本人擔任負責人之企業資料' },
    { key: '配偶擔任負責人之企業資料', value: '配偶擔任負責人之企業資料' },
  ];*/
  relationDegreeOptions: DropdownItem[] = [{ dataKey: '', dataName: '請選擇' }];
  relationDegreeCorpOptions: DropdownItem[] = [{ dataKey: '', dataName: '請選擇' }];

  terms = [];
  dialogTitle: string;
  dialogData: string;

  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private applyService: ApplyApiService,
    private commonDataService: CommonDataService,
    private logger: ApplyLoggerService
  ) { }

  ngOnInit(): void {
    this.initForm();

    const main: ApplyMain = this.userDataService.applyMain;
    // init 二親等以內血親
    this.addSecondDegreeDatas();
    if (main.addSecondDegreeData) {
      for (let i = 0; i < main.addSecondDegreeData.length - 1; i++) {
        this.addSecondDegreeDatas();
      }
    }

    // init 企業負責人
    this.addDatas();
    if (main.addData) {
      for (let i = 0; i < main.addData.length - 1; i++) {
        this.addDatas();
      }
    }

    this.applyForm.patchValue(main);

    // 二親等以內血親
    this.commonDataService.getRelationDegree()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        data.forEach(value => {
          this.relationDegreeOptions.push(value);
        });
      });

    // 企業負責人
    this.commonDataService.getRelationDegreeCorp()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        data.forEach(value => {
          this.relationDegreeCorpOptions.push(value);
        });
      });

  } // end ngOnInit

  ngAfterViewInit(): void {
    // initTextInputsBlur();
    // initBlueButton();
    initCleaveNumber();
    initCleaveUpper();
    initCloseButtons();
  }

  getFormArray(target: string): FormArray {
    return this.applyForm.get(target) as FormArray;
  }

  addSecondDegreeDatas() {
    this.getFormArray('addSecondDegreeData').push(this.newSecondDegreeData);
    initCleaveUpper();
  }

  removeSecondDegreeDatas() {
    const array = this.getFormArray('addSecondDegreeData');
    const length = array.length;
    array.removeAt(array.length - 1);
    if (length == 1) { 
      this.addSecondDegreeDatas();
    }
  }

  addDatas() {
    this.getFormArray('addData').push(this.newData);
    initCleaveUpper();
  }

  removeDatas() {
    const array = this.getFormArray('addData');
    const length = array.length;
    array.removeAt(array.length - 1);
    if (length == 1) { 
      this.addDatas();
    }
  }

  initForm() {
    this.applyForm = this.fb.group({
      idno: [{ value: '', disabled: true }, [Validators.required, Validators.minLength(10), idValidate]],
      customer_name: [{ value: '', disabled: true }, [Validators.required]],

      // relationship: ['', [Validators.required]],
      // relationshipName: ['', [Validators.required]],
      // relationshipId: ['', [Validators.required, Validators.minLength(10), idValidate]],

      addSecondDegreeData: this.fb.array([]), // init 二親等以內血親
      addData: this.fb.array([]), // init 企業負責人
    });
  }

  get newSecondDegreeData() {
    return this.fb.group({
      relationship: ['', [Validators.required]],
      relationshipName: ['', [Validators.required]],
      relationshipId: ['', [Validators.required, Validators.minLength(10), idValidate]],
    });
  }

  get newData() {
    return this.fb.group({
      corpRelationDegree: [''], // corpRelationDegree
      corpName: [''], // corpName
      corpnumber: [''], // corpnumber
      corpJobTitle: [''], // corpJobTitle
      memo: [''], // memo

      // kinship: [''],
      // kinshipName: [''],
      // kinshipId: ['', [Validators.minLength(10), idValidate]],
    });
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  errorMessageOfSecondDegreeData(i: number, id: string, errorName?: string) {
    const ctrl = ((<FormArray>this.applyForm.get('addSecondDegreeData')).controls[i] as FormGroup).controls[id];
    if (errorName) {
      return ctrl.invalid && ctrl.touched && ctrl.hasError(errorName);
    } else {
      return ctrl.invalid && ctrl.touched;
    }
  }

  showDialog(target) {
    showDialog(target);
  }

  idValidate(th) {
    const form = this.applyForm.get(th.id);
    form.patchValue(th.value);
  }

  dataToggle(th) {
    dataToggle(th);
  }

  selectActive(th) {
    selectActive(th);
  }

  // cloneAddData(target) {
  //   cloneAddData(target);
  // }

  onSubmit() {

    const req = this.applyForm.getRawValue();

    this.applyService.applyRelationDegree(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          // @save to sessionStorage
          const main = this.userDataService.applyMain;
          Object.assign(main, req);
          this.userDataService.applyMain = main;
          
          this.applyService.applyStepok()
          .pipe(takeWhile(() => this.alive))
          .subscribe(data1 => {
            if (data1.status === 0) {
              this.routerHelperService.navigate(['/UPL/stepok']);
            } else {
              this.logger.showMsg(data1.result);
            }
          });

        } else {
          this.logger.showMsg(data);
        }
      });
  }

}
