import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UPLRoutingModule } from './upl-routing.module';
import { LaterComponent } from './later/later.component';
import { Step0Component } from './step0/step0.component';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Step3Component } from './step3/step3.component';
import { Step4Component } from './step4/step4.component';
import { StepokComponent } from './stepok/stepok.component';
import { ExampleComponent } from './example/example.component';
import { NgxCleaveDirectiveModule } from 'ngx-cleave-directive';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    LaterComponent,
    Step0Component,
    Step1Component,
    Step2Component,
    Step3Component,
    Step4Component,
    StepokComponent,
    ExampleComponent,
  ],
  imports: [
    CommonModule,
    UPLRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCleaveDirectiveModule,
    SharedModule,
  ]
})
export class UPLModule { }
