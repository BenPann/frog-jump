import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';
import { initTextInputsBlur, initBlueButton, initCloseButtons, initCleaveNumber, showDialog } from 'src/app/js/start.page.js';
import { initRapidForm } from 'src/app/js/verify.page.js';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';

@Component({
  selector: 'app-step0',
  templateUrl: './step0.component.html',
  styleUrls: ['./step0.component.scss']
})
export class Step0Component implements OnInit {

  private alive = true;

  imagePath = null;

  mustUploadIdcard = true;
  mustUploadFinpf = true;

  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
    private applyService: ApplyApiService,
    private userDataService: UserDataService,
    private logger: ApplyLoggerService
  ) {}

  ngOnInit(): void {
    this.applyService.mustUploadIdcard()
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      if (data.status === 0 && data.result) {
          this.mustUploadIdcard = data.result.mustUploadIdcard;
          this.mustUploadFinpf = data.result.mustUploadFinpf;
      }
    
      if(this.logger.isCSRT){
        const prjCode = sessionStorage.getItem('prjCode');
        if(prjCode === "D00050"){
          this.mustUploadFinpf = false;
        }
      }
    });
  
  }

  clickUploadLater() {
    this.routerHelperService.navigate(['/UPL/later']);
  }
  
  onSubmit() {
    if (this.mustUploadIdcard) {
      this.routerHelperService.navigate(['/UPL/step1']);
    } else if (this.mustUploadFinpf) {
      this.routerHelperService.navigate(['/UPL/step3']);
    } else {
      this.applyService.applyStepok()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.routerHelperService.navigate(['/UPL/stepok']);
        } else {
          this.logger.showMsg(data.result);
        }
      });
    }
  }

}
