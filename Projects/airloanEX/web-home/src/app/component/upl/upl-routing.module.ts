import { StepokComponent } from './stepok/stepok.component';
import { ExampleComponent } from './example/example.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Step0Component } from './step0/step0.component';
import { Step1Component } from './step1/step1.component';
import { LaterComponent } from './later/later.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';
import { Step4Component } from './step4/step4.component';

import { TokenGuard } from 'src/app/guard/token.guard';
import { LeaveGuard } from 'src/app/guard/leave.guard';

const routes: Routes = [
  { path: 'example', component: ExampleComponent },
  { path: 'later', component: LaterComponent },
  {
    path: 'step0',
    component: Step0Component,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'step1',
    component: Step1Component,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'step2',
    component: Step2Component,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'step3',
    component: Step3Component,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'step4',
    component: Step4Component,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'stepok',
    component: StepokComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UPLRoutingModule { }
