import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { initTextInputsBlur, initBlueButton, initCloseButtons, showDialog } from 'src/app/js/start.page.js';
import { filterSample, collapseIt, fillImage, checkButtonDisable } from 'src/app/js/step3.page.js';
import { Observable, forkJoin } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { PhotoView } from 'src/app/service/apply-account/apply-account.interface';
import { DocumentService } from 'src/app/shared/service/document/document.service';
import { OtherDocumentResponse } from 'src/app/shared/service/document/document.interface';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { WebResult } from 'src/app/interface/resultType';

@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss']
})
export class Step3Component implements OnInit {
  private alive = true;
  imageDisabled = true;

  imgArr: string[] = [''];
  uploadedDocument: any[] = [];

  isShowRTMsg = false;
  enterPoint ;//用於財力上傳頁面，區分申請進入點是PL還是RPL
  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
    private documentService: DocumentService,
    private logger: ApplyLoggerService,
    private applyService: ApplyApiService,
    private userDataService: UserDataService,

  ) { }

  ngOnInit(): void {
    initTextInputsBlur();
    initBlueButton();
    this.enterPoint = sessionStorage.getItem('enterPoint'); //用於財力上傳頁面，區分申請進入點是PL還是RPL
    if(this.logger.isCSRT){
      const prjCode = sessionStorage.getItem('prjCode');
      if(prjCode === "D00051"){
        this.isShowRTMsg = true;
      }
    }

    const main = this.userDataService.applyMain;
    if (main.hasOldFinaImg === 'Y') {
      this.next();
    }

  }

  clickUploadLater() {
    this.routerHelperService.navigate(['/UPL/later']);
  }

  checkSrc(item) {
    return item !== '' ? item : 'assets/inc/default/img/demo/blank.png';
  }
  checkClass(item) {
    return item !== '' ? 'file-box form-item has-image' : 'file-box form-item';
  }

  removeImage(i) {
    this.imageDisabled = true;
    this.imgArr.splice(i, 1);
    // console.log(this.imgArr);
  }

  fillImage(th: any) {
    fillImage(th).pipe(takeWhile(() => this.alive)).subscribe(base64Image => {
      // console.log('base64Image=' + base64Image);
      this.imgArr.unshift(base64Image as string);
      this.imageDisabled = checkButtonDisable();
    });
    th.value = ''; // reset input file
  }

  filterSample(th) {
    filterSample(th);
  }

  collapseIt(th) {
    collapseIt(th);
  }

  next() { // 下一頁
    const main = this.userDataService.applyMain;
    // console.log('+main.p_apy_amount=' + +main.p_apy_amount);
    if (+main.p_apy_amount > 100) {
      if (main.marriage === '2') { // 已婚
        // 貸款金額大於100萬須填寫同一關係人表(step4)
        this.routerHelperService.navigate(['/UPL/step4']);
      } else { // 單身
        const req =
        {
          idno: main.idno,
          customer_name: main.customer_name,
          relationship: '',
          relationshipName: '',
          relationshipId: '',
          addData: []
        };
        this.applyService.applyRelationDegree(req)
          .pipe(takeWhile(() => this.alive))
          .subscribe(data => {
            if (data.status === 0) {
              // @save to sessionStorage
              const main = this.userDataService.applyMain;
              Object.assign(main, req);
              this.userDataService.applyMain = main;
              
              this.applyService.applyStepok()
              .pipe(takeWhile(() => this.alive))
              .subscribe(data1 => {
                if (data1.status === 0) {
                  this.routerHelperService.navigate(['/UPL/stepok']);
                } else {
                  this.logger.showMsg(data1.result);
                }
              });

            } else {
              this.logger.showMsg(data);
            }
          });
      }
    } else {
      this.applyService.applyStepok()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.routerHelperService.navigate(['/UPL/stepok']);
        } else {
          this.logger.showMsg(data.result);
        }
      });
    }
  }

  onSubmit() {
    const base64Images = [...this.imgArr]; // clone array
    // base64Images = base64Images.splice(1); // remove first one
    // console.log(base64Images);
    let uploadFinpfs: Observable<WebResult>[] = [];
    for (const base64Image of base64Images) {
      if (base64Image.length) {
        const photoView: PhotoView = {
          sType: '99',
          base64Image,
          subSerial: ''
        };
        uploadFinpfs.push(this.documentService.upload(photoView));
      } // end if
    }// end for

    // console.log('uploadFinpfs', uploadFinpfs);
    forkJoin(uploadFinpfs).subscribe(arrayOfResponse => {
      // console.log('arrayOfResponse', arrayOfResponse);
      // loop for
      for (const data of arrayOfResponse) {
        // console.log('forkJoin data', data);
          if (data.status === 0) {
            const res: OtherDocumentResponse = data.result;
            this.uploadedDocument.push({
              subSerial: res.subSerial,
              // docName: docName,
              // title: '財力證明',
              waterMarkImage: res.waterMarkImage
            });
          } else {
            // 上傳失敗
            this.logger.showMsg(data);
          }
      } // end for

      this.applyService.applyUploadFinpf()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.next();
        } else {
          this.logger.showMsg(data.result);
        }
      });

    }); // end forkJoin
  }

}
