import { Component, OnInit } from '@angular/core';
import { filterSample, collapseIt } from 'src/app/js/step3.page.js';
import { Router } from '@angular/router';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { initTextInputsBlur, initBlueButton } from 'src/app/js/start.page.js';

@Component({
  selector: 'app-example',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.scss']
})
export class ExampleComponent implements OnInit {

  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
  ) { }

  ngOnInit(): void {
    initTextInputsBlur();
    initBlueButton();
  }

  filterSample(th) {
    filterSample(th);
  }

  collapseIt(th) {
    collapseIt(th);
  }

  onSubmit() {
    this.routerHelperService.navigate(['/UPL/step4']);
  }

}
