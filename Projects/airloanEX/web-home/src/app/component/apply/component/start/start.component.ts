import { FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import {
  initCleaveDate, initCleaveUpper, initCleaveNumber, initCleaveMoney,
  initTextInputsBlur, initCloseButtons, initAllCloseButtons,
  closeDialog,
  showDialog
} from 'src/app/js/start.page.js';
import { Router } from '@angular/router';
import { idValidate } from 'src/app/js/step4.page.js';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ApplyMain, ApplyMainModel, CalMain } from 'src/app/component/apply/apply.interface';
import { ModalFactoryService } from 'src/app/shared/service/common/modal-factory.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { WebResult } from 'src/app/interface/resultType';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnInit {
  private alive = true;
  uniqType = '02'; // 申請

  applyForm: FormGroup;

  result: any;

  productId = '2'; // 1 PA, (預設)2 PL, 3 RPL, 4 GM
  
  uniqId: string;
  channelId: string;

  exp: string;
  entry: string;
  entryOther: string;
  entryName: string;

  terms = [];
  dialogTitle: string;
  dialogData: string;

  idDisabled: boolean;

  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private applyService: ApplyApiService,
    private commonDataService: CommonDataService,
    private modalService: ModalFactoryService,
    public logger: ApplyLoggerService,
  ) {
    this.activatedRoute.queryParamMap.subscribe((params: ParamMap) => {
      this.exp = params.get('EXP'); 
      const pt = params.get('PT');
      const entry = params.get('entry');
      const qr = params.get('qr');
      const cs = params.get('CS');
      this.channelId = params.get('channelId');
      const entryOther = []; // EntryData.other
      const keys = params.keys;

      if (qr) {
        this.commonDataService.getAgentNo(qr).subscribe(data => {
          const agentno = data.result?.Member;
          const control = this.applyForm.get('business');
          if (data.status === 0 && agentno) {
            control.setValue(agentno);
            control.disable();
          } else {
            control.enable();
          }
        });
      }

      for (const key of keys) {
        if (key !== 'PT' && key !== 'entry') { // exclude param: PT, entry
          const o = new Object();
          o[key] = params.get(key);
          entryOther.push(o);
        }
      }
      // console.log('pt=', pt);
      // console.log('entry=', entry);
      // console.log('entryOther=', entryOther);

      if (pt) {
        this.productId = pt;
      } else {
        this.productId = '2'; // 2 PL
      }

      // 1 PA 預核名單preAudit(快速申辦不查聯徵)
      // 2 PL
      // 3 RPL
      // 4 GM(CASHCARD)
      if (this.applyService.isPA(this.productId)) { // 1 PA

      } else if (this.applyService.isPL(this.productId)) { // 2 PL
        this.entry = entry ? entry : '';
        this.entryName = '個人信用貸款';
        this.entryOther = JSON.stringify(entryOther);
        sessionStorage.setItem("enterPoint","PL");
      } else if (this.applyService.isRPL(this.productId)) { // 3 RPL
        this.entry = entry ? entry : '';
        this.entryName = '額度型貸款';
        this.entryOther = JSON.stringify(entryOther);
        sessionStorage.setItem("enterPoint","RPL");
      } else if (this.applyService.isGM(this.productId)) { // 4 GM
        this.entry = entry ? entry : '';
        this.entryName = 'GM';
        this.entryOther = JSON.stringify(entryOther);
      }

      if(cs){
        this.uniqId = cs;
        this.applyService.applyGetCSMemoData(cs).subscribe(data => {
          const csData = data.result;

          if(csData){
            
            sessionStorage.setItem('channelId', csData.channelId);
            sessionStorage.setItem('creditLimit', csData.additionalData.creditLimit);
            sessionStorage.setItem('prjCode', csData.additionalData.prjCode);
            sessionStorage.setItem('rtRedirectUrl', csData.additionalData.redirectUrlAfterfinish);

            const id = this.applyForm.get('id');
            const phone = this.applyForm.get('phone');
            if(csData.idno){
              id.setValue(csData.idno);
            }
            console.log("csData.mobile : " + csData.mobile);
            if(csData.mobile){
              phone.setValue(csData.mobile);
            }

            if(this.channelId){
              if("RT" === this.channelId){
                logger.isCSRT = true;
              }else{
                logger.isCSRT = false;
              }
              this.applyService.applyGetThemePath(this.channelId).subscribe(data => {
                const themePath = data.result;
      
                if(themePath){
                  require("style-loader!src/assets/inc/default/css/" + themePath);
                }
              });
            }
          }
        });
      }else {
        logger.isCSRT = false;
      }

      // console.log('productId=' + this.productId);
      // console.log('productType=' + this.entryName);
    });
  }

  initForm() {
    this.applyForm = this.fb.group({
      id: ['', [Validators.required, Validators.maxLength(10), idValidate]],
      birthday: ['', [Validators.required, Validators.maxLength(10), Validators.pattern(/^\d{4}\/\d{2}\/\d{2}$/), LoanAge]],
      phone: ['', [Validators.required, Validators.maxLength(10), Validators.pattern(/^09\d{8}$/)]],
      // business: ['', [Validators.maxLength(6), Validators.pattern(/^\d{6}$/)]],
      business: ['', [Validators.maxLength(10)]],
      ch1: [false, [Validators.required]],
      ch2: [false, []],
    });
  }

  ngOnInit(): void {

    this.userDataService.welcomeReset();

    this.initForm();

    // dialog-IBM
    // showDialog('#dialog0');
    // dialog-bootstrap
    /*this.modalService.modalDefault({
      showConfirm: true,
      showClose: false,
      message: '您有案件尚未完成請問是否接續申請？',
      btnCancel: '接續申請',
      btnOK: '重新申請',
      btnForEdda: true,
    })
    .result.then(v => {
      if (v === 'cancel') { // 接續申請

      } else if (v === 'ok') { // 重新申請

      }
    });*/

    this.initTerms();
  } // end ngOnInit

  ngAfterViewInit(): void {
    initCleaveDate();
    initCleaveUpper();
    initCleaveNumber();
    initCleaveMoney();
    initCloseButtons();
    initAllCloseButtons();
  }

  initTerms(): void {
    this.applyService.getTermList().subscribe(data => {
      if (data) {
        data.forEach(value => {
          this.terms[value.type] = { name: value.termName, title: value.termTitle };
        });
      }
    });
  }

  // 取得條款內容
  showTerms(target: string, termsName) {
    this.applyService
      .getTerms(termsName.name)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        try {
          // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
          const json = JSON.parse(data);
          this.dialogTitle = '查詢條款失敗';
          this.dialogData = '';
        } catch (error) {
          this.dialogTitle = termsName.title;
          this.dialogData = data;
          showDialog(target);
        }
      });
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  // 問題：欄位crtl + X 剪下後，不會改變Form內容
  // 解覺：離開時修改內容
  idValidate(value): any {
    this.applyForm.patchValue({ id: value });
  }

  doLogin(req) {
    return this.applyService.applyStart(req);
  }

  makeLoginReq(forceNewApply, productId, uniqId, entry, entryOther, idno, birthday, phone, agentNo, expCaseNo, uniqType, ch1, ch2) {
    const req = {
      productId,
      uniqId,
      entry,
      entryOther,
      idno,
      birthday,
      phone,
      agentNo,
      forceNewApply,
      expCaseNo,
      uniqType, 
      ch1,
      ch2
    };
    return req;
  }

  onSubmit(): void {
    // this.modalService.modalDefault({ message: '安心旅遊補助' });
    const productId: string = this.productId;
    const uniqId: string = this.uniqId;
    const entry: string = this.entry;
    const entryOther: string = this.entryOther;
    const idno: string = this.applyForm.get('id').value;
    const birthday: string = this.applyForm.get('birthday').value;
    const phone: string = this.applyForm.get('phone').value;
    const agentNo: string = this.applyForm.get('business').value;
    const ch1: string = this.applyForm.get('ch1').value;
    const ch2: string = this.applyForm.get('ch2').value;
    const calMain: CalMain = this.userDataService.calMain;
    let expCaseNo = "";
    if (this.exp) {
      expCaseNo = this.exp;
    } else if (calMain?.CASE_NO) {
      expCaseNo = calMain.CASE_NO;
    }
    const uniqType = this.uniqType;

    const req = this.makeLoginReq(null, productId, uniqId, entry, entryOther, idno, birthday, phone, agentNo, expCaseNo, uniqType, ch1, ch2);

    this.applyService.applyStart(req).pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        console.log(data);
        if (data.status === 0) {
          // @save to sessionStorage
          this.userDataService.token = data.result.token; // 'kgiToken'
          const main: ApplyMain = new ApplyMainModel();
          this.userDataService.doStartData(main, req);
          if (data.result.caseDataResp) {
            Object.assign(main,  data.result.caseDataResp);
            this.userDataService.applyMain = main;
          }
          this.breakPointAlert(data.result);
        } else {
          if (this.channelId === "RT"){
            this.logger.showMsgForRT(data, "https://web.pchomepay.com.tw/kgi/loan/success?token=c99f212bc97c3741bbab08f2205d250d1ccb903bce5abfceb226cc40e5c2826eeea92abad3f7502f10cdca3f13fe04d9ffbd2d7d5482e145b9272e50fdf2956d");
          } else {
            this.logger.showMsg(data);
          }
        }
      });
    // this.router.navigate(['/apply/verify']);
  }

  /*breakPointAlert(result: any): void {

    const idno: string = this.applyForm.get('id').value;
    const birthday: string = this.applyForm.get('birthday').value;
    const phone: string = this.applyForm.get('phone').value;
    const agentNo: string = this.applyForm.get('business').value;

    const req = {
      idno,
      birthday,
      phone,
      agentNo,
      forceNewApply: 'Y',
      entry: this.activatedRoute.snapshot.queryParams.entry ? this.activatedRoute.snapshot.queryParams.entry : '',
      uniqType: this.uniqType,
    };

    const breakPoint: string = result.breakPoint;
    const caseDataResp: any = result.caseDataResp;
    if (breakPoint) {
      this.modalService.modalDefault({
        showConfirm: true,
        showClose: false,
        message: '您有案件尚未完成請問是否接續申請？',
        btnCancel: '接續申請',
        btnOK: '重新申請',
        btnForEdda: true,
      })
      .result.then(v => {
        if (v === 'cancel') { // 接續申請
          const main = this.userDataService.applyMain;
          this.userDataService.doPersonData(main, caseDataResp);
          this.userDataService.applyMain = main;
          this.routerHelperService.navigate([breakPoint]);
        } else if (v === 'ok') { // 重新申請
          this.applyService.applyStart(req)
          .pipe(takeWhile(() => this.alive))
          .subscribe(data => {
            console.log(data);
            if (data.status === 0) {
              // @save to sessionStorage
              this.userDataService.token = data.result.token; // 'kgiToken'
              const main: ApplyMain = new ApplyMainModel();
              this.userDataService.doStartData(main, req);
              this.userDataService.applyMain = main;
              this.routerHelperService.navigate(['/apply/verify']);
            }
          });
        }
      });
    } else {
      this.routerHelperService.navigate(['/apply/verify']);
    }
  } // end breakPointAlert*/

  autoCheck(checked: boolean) {
    if (checked) {
      //改為不要自動勾選
	  //this.applyForm.controls['ch2'].patchValue(true);
    }
  }

  showDialog(target) {
    showDialog(target);
  }

  closeDialog() {
    closeDialog();
  }

  breakPointAlert(result: any): void {
    this.result = result;
    const breakPoint: string = result.breakPoint;
    if (breakPoint) {
      showDialog('#dialog0');
    } else {
      this.routerHelperService.navigate(['/apply/verify']);
    }
  } // end breakPointAlert

  clickResume(): void {
    this.clickApply('N');
  } // end clickResume

  clickNewApply(): void {
    this.clickApply('Y');
  } // end clickNewApply  

  clickApply(forceNewApply: 'Y'|'N'): void {
    const productId: string = this.productId;
    const uniqId: string = this.uniqId;
    const entry: string = this.entry;
    const entryOther: string = this.entryOther;
    const idno: string = this.applyForm.get('id').value;
    const birthday: string = this.applyForm.get('birthday').value;
    const phone: string = this.applyForm.get('phone').value;
    const agentNo: string = this.applyForm.get('business').value;
    const uniqType = this.uniqType;
    const ch1: string = this.applyForm.get('ch1').value;
    const ch2: string = this.applyForm.get('ch2').value;
    const calMain: CalMain = this.userDataService.calMain;
    const expCaseNo = calMain?.CASE_NO;
    
    const req = this.makeLoginReq(forceNewApply, productId, uniqId, entry, entryOther, idno, birthday, phone, agentNo, expCaseNo, uniqType, ch1, ch2);
    
    this.applyService.applyStart(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        console.log(data);
        if (data.status === 0) {
          // @save to sessionStorage
          this.userDataService.token = data.result.token; // 'kgiToken'
          const main: ApplyMain = new ApplyMainModel();
          this.userDataService.doStartData(main, req);
          this.userDataService.doResume(main, data.result.caseDataResp);
          Object.assign(main,  data.result.caseDataResp);
          this.userDataService.applyMain = main;

          if (forceNewApply === 'Y') { // 重新申請
            this.userDataService.breakPoint = ''; 
            this.routerHelperService.navigate(['/apply/verify']);
          } else if (forceNewApply === 'N') { // 接續斷點
            const breakPoint = data.result.breakPoint;
            if (breakPoint === '/apply/start') {
              this.userDataService.breakPoint = '';
            } else {
              this.userDataService.breakPoint = breakPoint;
            }
            this.routerHelperService.navigate(['/apply/verify']);
          }
          
        } else {
          this.logger.showMsg(data);
        }
      });
  } // end clickApply
} // end StartComponent

/**
 * age=-20: 不可小於20歲
 * age= 65: 不可大於65歲
 * @param yyyy_mm_dd 
 * @param age 
 */
function checkAge(yyyy_mm_dd: string, age: number): boolean {
  if (!yyyy_mm_dd || yyyy_mm_dd.length != 10) {
    return;
  }
  const date = new Date();

  // 今天日期減20年
  date.setFullYear(date.getFullYear() - Math.abs(age));
  const validate = new Date(date).getTime();
  const birthday = new Date(yyyy_mm_dd).getTime();
  
  if (age < 0) {
    // console.log(`${validate >= birthday} validate:${date.toISOString().substring(0,10)} ${age < 0 ? '>=':'<='} birthday:${yyyy_mm_dd}`);
    return validate >= birthday;
  } else {
    // console.log(`${validate <= birthday} validate:${date.toISOString().substring(0,10)} ${age < 0 ? '>=':'<='} birthday:${yyyy_mm_dd}`);
    return validate <= birthday;
  }
  
}

/**
 * 檢核貸款年齡: 不可小於20歲 不可大於65歲
 */
export function LoanAge(control: FormControl): ValidationErrors {
  const birthday = control;
  if (birthday && birthday.value && birthday.value.length == 10) {
    const gt20 = checkAge(birthday.value, -20);
    const lt65 = checkAge(birthday.value, 65);
    // console.log(`gt20:${gt20} lt65:${lt65}`);
    if (!gt20) {
      return {forbiddenName: {value: birthday.value}, underAge20: true}
    }
    if (!lt65) {
      return {forbiddenName: {value: birthday.value}, overAge65: true}
    }
  }
  return null;
}