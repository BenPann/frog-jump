import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserDataService } from 'src/app/service/user-data/user-data.service';

@Component({
  selector: 'app-paper',
  templateUrl: './paper.component.html',
  styleUrls: ['./paper.component.scss']
})
export class PaperComponent implements OnInit {
  private alive = true;

  applyForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
  ) { }

  initForm() {
    this.applyForm = this.fb.group({
      email_address: [''],
    });
  }

  ngOnInit(): void {

    this.initForm();

    const main = this.userDataService.applyMain;
    this.applyForm.patchValue(main);
  }
}
