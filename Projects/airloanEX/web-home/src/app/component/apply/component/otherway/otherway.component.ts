import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';

@Component({
  selector: 'app-otherway',
  templateUrl: './otherway.component.html',
  styleUrls: ['./otherway.component.scss']
})
export class OtherwayComponent implements OnInit {
  private alive = true;

  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
    private logger: ApplyLoggerService,
    private applyService: ApplyApiService,
  ) { }

  ngOnInit(): void {
  }

  clickApplyCreditcard() {
    this.routerHelperService.navigate(['/CAD/gift']);
  }

  clickApplyPaper() {
    this.applyService.applyPaper()
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      if (data.status === 0) {
        this.routerHelperService.navigate(['/apply/paper']);
      } else {
        this.logger.showMsg(data.result);
      }
    });
  }

  clickApplyIdentify() {
    this.routerHelperService.navigate(['/apply/identify']);
  }

  onSubmit() {
    this.routerHelperService.navigate(['/apply/verify2']);
  }

}
