import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { initTextInputsBlur, initBlueButton, initCleaveMoney, initCloseButtons, showDialog } from 'src/app/js/start.page.js';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
import { DropdownItem } from 'src/app/interface/resultType';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';

@Component({
  selector: 'app-loancsrt',
  templateUrl: './loancsrt.component.html',
  styleUrls: ['./loancsrt.component.scss']
})
export class LoancsrtComponent implements OnInit {

  private alive = true;
  applyForm: FormGroup;

  periodOptions = [
    { key: '', value: '請選擇' },
    { key: '2', value: '2年' },
    { key: '3', value: '3年' },
    { key: '4', value: '4年' },
    { key: '5', value: '5年' },
    { key: '6', value: '6年' },
    { key: '7', value: '7年' },
  ];

  loanPurposeOptions: DropdownItem[] = [{ dataKey: '', dataName: '請選擇' }];
  // purposeOptions = [
  //   { key: '', value: '請選擇' },
  //   { key: '投資', value: '1' },
  //   { key: '賭博', value: '2' },
  //   { key: '慈善', value: '3' },
  // ];

  p_apy_amount: string;

  initForm() {
    this.applyForm = this.fb.group({});

    this.p_apy_amount = sessionStorage.getItem('creditLimit');
  }
  constructor(
    public cleave: CleaveService,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private applyService: ApplyApiService,
    private commonDataService: CommonDataService,
    public logger: ApplyLoggerService
  ) { }

  ngOnInit(): void {
    this.initForm();

    const main = this.userDataService.applyMain;
    this.applyForm.patchValue(main);
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  showDialog(target) {
    showDialog(target);
  }

  onSubmit() {
    const applyAmount = this.p_apy_amount + "0000";
    const req = {
      p_apy_amount: applyAmount
    };

    // 更新狀態 01 000
    this.applyService.applyLoan()
    .pipe(takeWhile(() => this.alive))
    .subscribe();

    // console.log(req)
    this.applyService.applyInfo(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          // @save to sessionStorage
          const main = this.userDataService.applyMain;
          Object.assign(main, req);
          // this.userDataService.doLoanData(main, req);
          this.userDataService.applyMain = main;
          if (this.applyService.isOldOne(main.UserType)) {
            this.routerHelperService.navigate(['/apply/confirm']);
          } else { // 新戶
            this.routerHelperService.navigate(['/apply/person']);
          }
        } else {
          this.logger.showMsg(data);
        }
      });
  }

}
