import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanoldComponent } from './loanold.component';

describe('LoanoldComponent', () => {
  let component: LoanoldComponent;
  let fixture: ComponentFixture<LoanoldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanoldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanoldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
