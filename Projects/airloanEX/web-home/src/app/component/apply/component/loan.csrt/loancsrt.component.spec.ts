import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoancsrtComponent } from './loancsrt.component';

describe('LoancsrtComponent', () => {
  let component: LoancsrtComponent;
  let fixture: ComponentFixture<LoancsrtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoancsrtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoancsrtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
