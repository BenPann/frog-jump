import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanrplComponent } from './loanrpl.component';

describe('LoanrplComponent', () => {
  let component: LoanrplComponent;
  let fixture: ComponentFixture<LoanrplComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanrplComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanrplComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
