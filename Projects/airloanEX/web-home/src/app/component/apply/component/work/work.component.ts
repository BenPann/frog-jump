import { formatDate } from '@angular/common';
import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { filter, switchMap, takeWhile, tap } from 'rxjs/operators';
import { DropdownItem } from 'src/app/interface/resultType';
import { candidate, cityjson, countiesOptions, telValidate } from 'src/app/js/person.page.js';
import { initCleaveNumber } from 'src/app/js/start.page.js';
import { dedicateCompany, phonejson } from 'src/app/js/work.page.js';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { SpinnerOverlayService } from 'src/app/shared/service/common/spinner-overlay.service';

@Component({
  selector: 'app-work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.scss']
})
export class WorkComponent implements OnInit {
  private alive = true;

  work: string;
  applyForm: FormGroup;
  corpNameBL = true;
  options = countiesOptions;
  cityOption = [{ dataKey: '', dataName: '鄉鎮市區' }];
  phonejson = phonejson;
  occupationOptions: DropdownItem[] = [{ dataKey: '', dataName: '請選擇' }];
  yearOption = [{ key: '', value: '請選擇' }, ];
  monthOption = [
    { key: '', value: '請選擇' },
    { key: '01', value: '01月' },
    { key: '02', value: '02月' },
    { key: '03', value: '03月' },
    { key: '04', value: '04月' },
    { key: '05', value: '05月' },
    { key: '06', value: '06月' },
    { key: '07', value: '07月' },
    { key: '08', value: '08月' },
    { key: '09', value: '09月' },
    { key: '10', value: '10月' },
    { key: '11', value: '11月' },
    { key: '12', value: '12月' },
  ];
  companys = null;
  companyplaceholder = "請輸入公司名稱或統編";

  initForm() {
    this.applyForm = this.fb.group({
      corp_name: ['', [Validators.required]],
      Occupation: ['', [Validators.required]],
      title: ['', [Validators.required]],
      year: ['', [Validators.required, CheckOnBoardDate]],
      month: ['', [Validators.required, CheckOnBoardDate]],
      on_board_date: [''],
      companyCounties: ['', [Validators.required]],
      companyTownship: ['', [Validators.required]],
      corp_city_code: [''],
      companyAddress: ['', [Validators.required]], 
      companyPhone: ['', [Validators.required, Validators.maxLength(11), Validators.pattern(/(^!09|^(08)-?\d{7,8}$|^(07)-?\d{7,8}$|^(06)-?\d{7,8}$|^(05)-?\d{7,8}$|^(04)-?\d{7,8}$|^(03)-?\d{7,8}$|^(02)-?\d{7,8}$|^(0826)-?\d{5,6}$|^(0836)-?\d{5,6}$|^(082)-?\d{6,7}$|^(089)-?\d{6,7}$|^(049)-?\d{6,7}$|^(037)-?\d{6,7}$)/)]], 
      corp_tel_area: [''],
      corp_tel: [''],
      corp_tel_exten: ['', [Validators.maxLength(8), Validators.pattern(/^\d{0,8}$/)]],
    });
  }
  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private elme: ElementRef,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService,
    private applyService: ApplyApiService,
    private userDataService: UserDataService,
    private commonDataService: CommonDataService,
    private logger: ApplyLoggerService,
    private spinner: SpinnerOverlayService,
  ) { 
    if (logger.isCSRT){
      const channelId = sessionStorage.getItem('channelId');
      this.applyService.applyGetThemePath(channelId).subscribe(data => {
        const themePath = data.result;

        if(themePath){
          require("style-loader!src/assets/inc/default/css/" + themePath);
          this.companyplaceholder = "請輸入露天賣家名稱";
        }
      });
    }
  }

  ngOnInit(): void {
    this.initForm();
    initCleaveNumber();
    candidate();
    this.selectOptions();

    const main = this.userDataService.applyMain;
    main.companyPhone = main.companyPhone ? main.companyPhone : '';
    this.applyForm.patchValue(main);
    this.makeDistrict(main.companyCounties);
    this.initYearList(main.birthday);

    // 取消loging 畫面
    this.spinner.spinnerBL = false;
    this.applyForm.get('corp_name').valueChanges.pipe(
        tap(() => {candidate(); this.companys = null; }),
        filter( value => this.corpNameBL && value && (value.trim()).length >= 2),
        switchMap(value => {
          if ( isNaN(+value) ) {
            return this.applyService.getFullName({ShortName: value});
          } else if (+value.length === 8){
            return this.applyService.getinfo({ CompanyId: +value });
          } else {
            return of();
          }
        }),
    ).subscribe((data: any) => {
      if (data.status === 0 && data.result.Code === '200') {
        this.companys = data.result.Data;
        if (this.companys instanceof Array){
          setTimeout(() => {
            dedicateCompany(this.applyForm.get('corp_name').value, this.elme, this.companys);
          }, 1);
        } else {
          this.applyForm.get('corp_name').patchValue(this.companys.CompanyName);
        }
      }
    });
  }

  checkCandidate(item) {
    // console.log('item',item)
    this.applyForm.get('corp_name').patchValue(item);
  }

  selectOptions() {
    this.commonDataService.getOccupation()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        data.forEach(value => {
          this.occupationOptions.push(value);
        });
      });
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  makeDistrict(value) {
    // 鄉鎮市區選項
    this.cityOption = [{ dataKey: '', dataName: '鄉鎮市區' }];
    if (value) {
      this.cityOption = this.cityOption.concat(cityjson[value]);
    } else {
      this.applyForm.get('companyTownship').patchValue('');
    }
    // 區碼設定
    const phone = this.applyForm.get('companyPhone');
    if (!phone.value && value) {
      phone.patchValue(phonejson[value]);
    }

  }

  initYearList(birthday: string) {
    const day = +birthday.substring(0, 4) + 15;
    const yearDay = formatDate(new Date(), 'yyyy', 'zh-Hant');
    this.yearOption = [];
    for (let i = day; i <= +yearDay; i++) {
      this.yearOption.unshift({ key: i.toString(), value: i + '年' });
    }
    this.yearOption.unshift({ key: '', value: '請選擇' });
  }

  telValidate(th) {
    this.applyForm.get(th.id).patchValue(telValidate(th));
  }

  onSubmit() {
    // 打開 loging 畫面
    this.spinner.spinnerBL = true;

    const req = {
      corp_name: this.applyForm.get('corp_name').value,
      Occupation: this.applyForm.get('Occupation').value,
      title: this.applyForm.get('title').value,
      year: this.applyForm.get('year').value,
      month: this.applyForm.get('month').value,
      on_board_date: '',
      companyCounties: this.applyForm.get('companyCounties').value,
      companyTownship: this.applyForm.get('companyTownship').value,
      corp_city_code: '',
      companyAddress: this.applyForm.get('companyAddress').value,
      corp_address: '',
      companyPhone: this.applyForm.get('companyPhone').value,
      corp_tel_area: '',
      corp_tel: '',
      corp_tel_exten: this.applyForm.get('corp_tel_exten').value,
    };

    req.on_board_date = (req.year ? req.year : '') + (req.month ? req.month : '') + '28';
    req.corp_city_code = this.cityOption.filter(v => v.dataName === req.companyTownship)[0].dataKey;
    req.corp_address = req.companyCounties + req.companyTownship + ';' + req.companyAddress;
    if (req.companyPhone.indexOf('-') !== -1) {
      req.corp_tel_area = req.companyPhone.split('-')[0];
      req.corp_tel = req.companyPhone.split('-')[1];
    } else {
      req.corp_tel = req.companyPhone;
    }

    // applyInfo 將資料存入DB（CaseData）
    this.applyService.applyInfo(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          // @save to sessionStorage
          const main = this.userDataService.applyMain;
          Object.assign(main, req);
          // this.userDataService.doWorkData(main, req);
          this.userDataService.applyMain = main;
          this.routerHelperService.navigate(['/apply/other']);
        } else {
          this.logger.showMsg(data);
        }
      });
  }

} // end WorkComponent

/**
 * 到職年月：應檢核不可大於當月
 */
export function CheckOnBoardDate(control: FormControl): ValidationErrors {
  const fg = control.parent as FormGroup;
  if (fg) {
    const year = fg.get('year');
    const month = fg.get('month');

    if (year === control) {
      month.updateValueAndValidity();
      return null;
    }
    
    if (year && year.value && month && month.value) {
      const today = new Date();
      const onBoardDate = new Date(+year.value, (+month.value-1), 1); // 預設 yyyy-MM-01 但是寫入時是 yyyy-MM-28
      // console.log(today);
      // console.log(onBoardDate);
      if (onBoardDate.getTime() > today.getTime()) {
        return {forbiddenName: {value: month.value}, overMonth: true}
      }
    }
  }
  return null;
}
