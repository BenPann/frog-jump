import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Verify2Component } from './verify2.component';

describe('Verify2Component', () => {
  let component: Verify2Component;
  let fixture: ComponentFixture<Verify2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Verify2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Verify2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
