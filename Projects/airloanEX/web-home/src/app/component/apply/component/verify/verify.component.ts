import { FormGroup, FormBuilder } from '@angular/forms';
import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { initAllCloseButtons, initCleaveNumber, initCloseButtons, showDialog } from 'src/app/js/start.page.js';
import { initRapidForm } from 'src/app/js/verify.page.js';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';

import { takeWhile, tap } from 'rxjs/operators';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { interval } from 'rxjs';
import { OtpService } from '../../../../service/otp/otp.service';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit, OnDestroy {
  private alive = true;
  private resetTimer = true;

  applyForm: FormGroup;

  // 計秒
  secText = 60;
  userPhoneNum = '';
  sendSuccess = false;
  datatypeName = '';

  // 彈跳視窗訊息
  dialogData: string;

  initForm() {
    this.applyForm = this.fb.group({
      tel1: [''],
      tel2: [''],
      tel3: [''],
      tel4: [''],
      tel5: [''],
      tel6: [''],
    });
  }

  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private elem: ElementRef,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private logger: ApplyLoggerService,
    private otpService: OtpService,
    private applyService: ApplyApiService
  ) { }

  ngAfterViewInit(): void {
    
    initCleaveNumber();
    initRapidForm(this.elem);
    initCloseButtons();
    initAllCloseButtons();

    setTimeout(() => this.sendOtp());
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  ngOnInit() {
    // 取客戶類別及前一頁行動電話
    this.getDataTypeAndMobile_tel();

    this.initForm();
  }

  checkValue(name: string, value: string) {
    if (value) {
      if (!/[0-9]$/.test(value)) {
        this.applyForm.get(name).patchValue('');
      } else if (this.applyForm.valid) {
        this.onSubmit();
      }
    }
  }

  // 取客戶類別及前一頁行動電話
  getDataTypeAndMobile_tel() {
    const main = this.userDataService.applyMain;
    this.userPhoneNum = main.mobile_tel;

    switch (main.stempDataType) {
      case '1':
        this.datatypeName = '申辦本行信用卡服務之';
        break;
      case '2':
        this.datatypeName = '申辦本行現金卡服務之';
        break;
      case '3':
        this.datatypeName = '申辦本行存款服務之';
        break;
      case '4':
        this.datatypeName = '申辦本行貸款服務之';
        break;
      case '5':
        this.datatypeName = 'VIP';
        break;
      default:
        this.datatypeName = '';
        break;
    }
  }

  errorForm() {
    document.forms[0].classList.add('error-form');
  }

  // 重領驗證碼
  sendSMS(): void {
    // 清除欄位號碼
    this.applyForm.reset();
    this.sendOtp();
  }

  sendOtp() {
    this.otpService
      .sendOtp(this.userPhoneNum)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          if (data.result.code === '0000') {
            showDialog('#dialog0');
            this.dialogData = '已發送簡訊至您的手機號碼，請輸入驗證碼進行最後確認';
            this.sendSuccess = true;
            // 計時
            this.setTime(60);
          } else {
            this.secText = 0;
          }
        } else {
          showDialog('#dialog0');
          this.dialogData = '發送簡訊驗證碼失敗，請稍後再試，或洽客服人員';
          this.secText = 0;
        }
      });
  }

  // 計時
  setTime(tempT: number): void {
    this.resetTimer = true;
    let total = tempT;
    this.secText = total;
    interval(1000)
      .pipe(
        takeWhile(() => total-- !== 0),
        takeWhile(() => this.resetTimer),
        takeWhile(() => this.alive),
        tap(data => (this.secText = total))
      )
      .subscribe();
  }

  onSubmit(): void {
    const form = this.applyForm;

    const req = form.get('tel1').value + form.get('tel2').value + form.get('tel3').value
              + form.get('tel4').value + form.get('tel5').value + form.get('tel6').value;
    // @call 6. OTP
    this.otpService
      .checkOtp(this.userPhoneNum, req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {

          // 更新狀態 00 010
          this.applyService.applyVerify()
          .pipe(takeWhile(() => this.alive))
          .subscribe();

          const breakPoint = this.userDataService.breakPoint;
          if (breakPoint && breakPoint !== '/apply/verify'
            && !breakPoint.startsWith('/CAL')) {
            this.userDataService.breakPoint = '';
            this.routerHelperService.navigate([breakPoint]);
          } else {

            const main = this.userDataService.applyMain;

            this.applyService.applyPrepareConfirm()
            .pipe(takeWhile(() => this.alive))
            .subscribe(data => {
              if (data.status === 0) {
                const caseDataResp: any = data.result.caseDataResp;
                // @save to sessionStorage
                this.userDataService.doResume(main, caseDataResp);
                this.userDataService.applyMain = main;
              }
            });

            if (this.logger.isCSRT){
              this.routerHelperService.navigate(['apply', 'loan.csrt']);
            } else{
              // 1 PA 預核名單preAudit(快速申辦不查聯徵)
              // 2 PL
              // 3 RPL
              // 4 GM(CASHCARD)
              const productId = this.userDataService.applyMain.ProductId;
              const userType =  this.userDataService.applyMain.UserType;
              // console.log('productId=' + productId);
              if (this.applyService.isPA(productId)) { // 預核
                this.routerHelperService.navigate(['apply', 'loan.old']);
              } else if (this.applyService.isPL(productId)) { // PL
                this.routerHelperService.navigate(['apply', 'loan']);
              } else if (this.applyService.isRPL(productId)) { // RPL
                this.routerHelperService.navigate(['apply', 'loan.rpl']);
              }
            }
          } // end else
        } else {
          this.logger.showMsg(data);
        }
      });
  }
}
