import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherwayComponent } from './otherway.component';

describe('OtherwayComponent', () => {
  let component: OtherwayComponent;
  let fixture: ComponentFixture<OtherwayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherwayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherwayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
