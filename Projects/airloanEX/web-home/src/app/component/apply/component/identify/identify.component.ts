import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';
import { DropdownItem } from 'src/app/interface/resultType';
import { initCleaveNumber, initCloseButtons, showDialog } from 'src/app/js/start.page.js';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { ProductService } from 'src/app/service/product/product.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { PCODE2566Req, PCode2566Resp } from 'src/app/service/validate/validate.interface';
import { ValidateService } from 'src/app/service/validate/validate.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';


@Component({
  selector: 'app-identify',
  templateUrl: './identify.component.html',
  styleUrls: ['./identify.component.scss']
})
export class IdentifyComponent implements OnInit {

  private alive = true;
  // dropdownData
  pcode2566BankList: DropdownItem[];

  applyForm: FormGroup;

  // bankOption = [
  //   { key: '', value: '請選擇' },
  //   { key: '004-臺灣銀行', value: '004-臺灣銀行' },
  //   { key: '005-土地銀行', value: '005-土地銀行' },
  //   { key: '006-合庫商銀', value: '006-合庫商銀' },
  //   { key: '007-第一銀行', value: '007-第一銀行' },
  //   { key: '008-華南銀行', value: '008-華南銀行' },
  //   { key: '009-彰化銀行', value: '009-彰化銀行' },
  //   { key: '011-上海銀行', value: '011-上海銀行' },
  //   { key: '012-台北富邦', value: '012-台北富邦' },
  //   { key: '013-國泰世華', value: '013-國泰世華' },
  //   { key: '016-高雄銀行', value: '016-高雄銀行' },
  //   { key: '017-兆豐商銀', value: '017-兆豐商銀' },
  //   { key: '021-花旗銀行', value: '021-花旗銀行' },
  //   { key: '050-臺灣企銀', value: '050-臺灣企銀' },
  //   { key: '081-匯豐銀行', value: '081-匯豐銀行' },
  //   { key: '083-渣打銀行', value: '083-渣打銀行' },
  //   { key: '087-標旗銀行', value: '087-標旗銀行' },
  //   { key: '102-華泰銀行', value: '102-華泰銀行' },
  //   { key: '108-陽信銀行', value: '108-陽信銀行' },
  //   { key: '700-中華郵政', value: '700-中華郵政' },
  //   { key: '803-聯邦銀行', value: '803-聯邦銀行' },
  //   { key: '805-遠東銀行', value: '805-遠東銀行' },
  //   { key: '808-玉山銀行', value: '808-玉山銀行' },
  //   { key: '812-台新銀行', value: '812-台新銀行' },
  //   { key: '815-日盛銀行', value: '815-日盛銀行' },
  //   { key: '816-安泰銀行', value: '816-安泰銀行' },
  //   { key: '822-中國信託', value: '822-中國信託' },
  // ];
  initForm() {
    this.applyForm = this.fb.group({
      bank: ['', [Validators.required]],
      bankAccount: ['', [Validators.required, Validators.maxLength(16)]],
      phone: ['', [Validators.required, Validators.maxLength(10), Validators.pattern(/^09\d{8}$/)]],
    });
  }
  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private productService: ProductService,
    private applyService: ApplyApiService,
    private commonDataService: CommonDataService,
    private validateService: ValidateService,
    private logger: ApplyLoggerService
  ) { }

  ngOnInit(): void {
    this.initForm();

    // 取得銀行別
    this.commonDataService
      .getPcode2566BankList()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        this.pcode2566BankList = data;
      });

    // 取得NCCC和PCode2566最大驗身次數 getMaxVerifyCount()
    // this.productService
    // .getMaxVerifyCount()
    // .pipe(takeWhile(() => this.alive))
    // .subscribe(data => {
    //   if (data.status === 0) {
    //     this.userDataService.maxVerifyCount = data.result;
    //   } else {
    //     // TODO: 提示錯誤
    //   }
    // });
    const main = this.userDataService.applyMain;
    this.applyForm.patchValue(main);
    this.applyForm.get('phone').setValue(main.mobile_tel);
  }

  ngAfterViewInit(): void {
    initCleaveNumber();
    initCloseButtons();
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  // showDialog(target) {
  //   showDialog(target);
  // }

  clickApplyOtherway() {
    this.routerHelperService.navigate(['/apply/otherway']);
  }

  changeInput() {
    const formVal = this.applyForm.getRawValue(); // get disabled controls
    const main = this.userDataService.applyMain;
    main.pcode2566_phone = formVal.phone;
    main.bank = formVal.bank;
    main.bankAccount = formVal.bankAccount;
    this.userDataService.applyMain = main;
  }

  onSubmit() {
    const req = {
      bank: this.applyForm.get('bank').value,
      bankAccount: this.applyForm.get('bankAccount').value,
    };

    this.applyService.applyInfo(req)
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      if (data.status === 0) {
        // @save to sessionStorage
        const main = this.userDataService.applyMain;
        Object.assign(main, req);
        // this.userDataService.doLoanData(main, req);
        this.userDataService.applyMain = main;
      }
    });

    const formVal = this.applyForm.getRawValue(); // get disabled controls
    const reqPCODE2566: PCODE2566Req = {
      phone: formVal.phone,
      bank: formVal.bank,
      account: formVal.bankAccount,
      branchId: '',
    };

    // 他行存戶驗身
    // @call 7. PCODE2566
    this.validateService
      .setPCode2566(reqPCODE2566)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        console.log('他行存戶驗身');
        console.log(data);
        if (data.status === 0) {

          // @save to sessionStorage
          const main = this.userDataService.applyMain;
          main.pcode2566_phone = this.applyForm.get('phone').value;
          this.userDataService.applyMain = main;

          const r2: PCode2566Resp = data.result;
          if (r2.pcode2566CheckCode === '0') { // 驗身通過
            this.routerHelperService.navigate(['/apply/verify2']);
          } else {
            alert('驗身失敗');
          }
        } else {
          this.logger.showMsg(data);
        } // end if
      });
  }

}
