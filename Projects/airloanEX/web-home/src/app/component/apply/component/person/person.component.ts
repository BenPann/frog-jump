import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';

import {
  initCleaveNumber,
  initTextInputsBlur,
  initBlueButton,
} from 'src/app/js/start.page.js';
import {
  dedicateMail,
  candidate,
  telValidate,
  cityjson,
  countiesOptions
} from 'src/app/js/person.page.js';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {
  private alive = true;
  
  email_address: string;

  applyForm: FormGroup;
  corpNameBL = true;

  options = countiesOptions;
  cityOption = [{ dataKey: '', dataName: '鄉鎮市區' }];

  @ViewChild('homePhone', {static: false}) homePhone: ElementRef;
  @ViewChild('domicilePhone', {static: false}) domicilePhone: ElementRef;

  initForm() {
    this.applyForm = this.fb.group({
      customer_name: ['', [Validators.required]],
      email_address: ['', [Validators.required, Validators.email]],
      // email_address: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
      counties: ['', [Validators.required]],
      township: ['', [Validators.required]],
      house_city_code: [''],
      address: ['', [Validators.required]],
      homePhone: ['', [Validators.required, Validators.pattern(/(^(09)\d{8}$|^(07)-?\d{7,8}$|^(06)-?\d{7,8}$|^(05)-?\d{7,8}$|^(04)-?\d{7,8}$|^(03)-?\d{7,8}$|^(02)-?\d{7,8}$|^(0826)-?\d{5,6}$|^(0836)-?\d{5,6}$|^(082)-?\d{6,7}$|^(089)-?\d{6,7}$|^(049)-?\d{6,7}$|^(037)-?\d{6,7}$)/)]],
      house_tel_area: [''],
      house_tel: [''],
      domicilePhone: ['', [Validators.required, Validators.pattern(/(^(09)\d{8}$|^(07)-?\d{7,8}$|^(06)-?\d{7,8}$|^(05)-?\d{7,8}$|^(04)-?\d{7,8}$|^(03)-?\d{7,8}$|^(02)-?\d{7,8}$|^(0826)-?\d{5,6}$|^(0836)-?\d{5,6}$|^(082)-?\d{6,7}$|^(089)-?\d{6,7}$|^(049)-?\d{6,7}$|^(037)-?\d{6,7}$)/)]],
      ResTelArea: [''],
      ResTel: [''],
      ch1: [''],
    });
  }
  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private elme: ElementRef,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private applyService: ApplyApiService,
    private logger: ApplyLoggerService
  ) { }

  ngOnInit(): void {
    this.initForm();
    candidate();
    initCleaveNumber();

    const main = this.userDataService.applyMain;
    main.homePhone = main.homePhone ? main.homePhone : '';
    main.domicilePhone = main.domicilePhone ? main.domicilePhone : '';
    // console.log(main.counties);
    // console.log(main.township);
    this.applyForm.patchValue(main);
    this.makeDistrict(main.counties);
  }

  ngAfterViewInit(): void {
    // 0312345678 轉換 03-12345678
    const homePhoneText = telValidate(this.homePhone.nativeElement);
    const domicilePhoneText = telValidate(this.domicilePhone.nativeElement)
    this.applyForm.get('homePhone').patchValue(homePhoneText);
    this.applyForm.get('domicilePhone').patchValue(domicilePhoneText);
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  dedicateMail($event) {
    if (this.corpNameBL) {
      dedicateMail($event, this.elme);
    } else {
      this.corpNameBL = true;
    }
  }

  makeDistrict(value) {

    if (cityjson[value] === undefined) {
      this.applyForm.get('counties').patchValue('');
    }

    this.cityOption = [{ dataKey: '', dataName: '鄉鎮市區' }];
    if (value && cityjson[value]) {
      this.cityOption = this.cityOption.concat(cityjson[value]);
    } else {
      this.applyForm.get('township').patchValue('');
    }
  }

  telValidate(th) {
    this.applyForm.get(th.id).patchValue(telValidate(th));
  }


  phoneAutoFill(th) {
    const homePhone = this.applyForm.get('homePhone');
    const domicilePhone = this.applyForm.get('domicilePhone');
    homePhone.markAsTouched();
    if (th && !this.errorMessage('homePhone')) { // 勾選同住家
      domicilePhone.patchValue(homePhone.value);
      homePhone.disable();
      domicilePhone.disable();
    } else {
      homePhone.enable();
      domicilePhone.enable();
    }
  }

  onSubmit() {

    const req = {
      customer_name: this.applyForm.get('customer_name').value,
      email_address: this.applyForm.get('email_address').value,
      counties: this.applyForm.get('counties').value,
      township: this.applyForm.get('township').value,
      house_city_code: '',
      address: this.applyForm.get('address').value,
      house_address: '',
      homePhone: this.applyForm.get('homePhone').value,
      house_tel_area: '',
      house_tel: '',
      domicilePhone: this.applyForm.get('domicilePhone').value,
      ResTelArea: '',
      ResTel: '',
    };

    req.house_city_code = this.cityOption.filter(v => v.dataName === req.township)[0].dataKey;
    req.house_address = req.counties + req.township + ';' + req.address;
    if (req.homePhone.indexOf('-') !== -1) {
      req.house_tel_area = req.homePhone.split('-')[0];
      req.house_tel = req.homePhone.split('-')[1];
    } else {
      req.house_tel = req.homePhone;
    }
    if (req.domicilePhone.indexOf('-') !== -1) {
      req.ResTelArea = req.domicilePhone.split('-')[0];
      req.ResTel = req.domicilePhone.split('-')[1];
    } else {
      req.ResTel = req.domicilePhone;
    }

    this.applyService.applyInfo(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        console.log(data);
        if (data.status === 0) {
          // @save to sessionStorage
          const main = this.userDataService.applyMain;
          Object.assign(main, req);
          // this.userDataService.doPersonData(main, req);
          this.userDataService.applyMain = main;
          this.routerHelperService.navigate(['/apply/work']);
        } else {
          this.logger.showMsg(data);
        }
      });
  }
}
