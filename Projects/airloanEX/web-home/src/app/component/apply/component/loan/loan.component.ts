import { FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { DropdownItem } from 'src/app/interface/resultType';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { initTextInputsBlur, initBlueButton, initCleaveMoney, showDialog, initCloseButtons } from 'src/app/js/start.page.js';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.scss']
})
export class LoanComponent implements OnInit {

  private alive = true;
  isPL = false;
  applyForm: FormGroup;
  loanStatus = 'PL';
  loanTitle = '開始填寫申請書';
  loanAmountLabel = '貸款金額';
  checkAmountErr = false;

  periodOptions = [
    { key: '', value: '請選擇' },
    { key: '2', value: '2年' },
    { key: '3', value: '3年' },
    { key: '4', value: '4年' },
    { key: '5', value: '5年' },
    { key: '6', value: '6年' },
    { key: '7', value: '7年' },
  ];

  loanPurposeOptions: DropdownItem[] = [{ dataKey: '', dataName: '請選擇' }];
  // purposeOptions = [
  //   { key: '', value: '請選擇' },
  //   { key: '投資', value: '1' },
  //   { key: '賭博', value: '2' },
  //   { key: '慈善', value: '3' },
  // ];
  initForm() {
    this.applyForm = this.fb.group({
      p_apy_amount: ['', [Validators.required, Validators.maxLength(3)]],
      p_period: ['', [Validators.required]],
      p_purpose: ['', [Validators.required]],
      p_purpose_name: [''],
    });
  }
  constructor(
    public cleave: CleaveService,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private applyService: ApplyApiService,
    private commonDataService: CommonDataService,
    private logger: ApplyLoggerService,
  ) { }

  ngOnInit(): void {
    this.initForm();

    this.selectList();
    this.demolink();

    const main = this.userDataService.applyMain;
    this.applyForm.patchValue(main);
    
    this.isPL = this.applyService.isPL(main.ProductId);

  }

  ngAfterViewInit(): void {
    initCleaveMoney();
    initCloseButtons();
  }

  selectList() {
    this.commonDataService.getLoanPurposeOptions()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        data.forEach(value => {
          this.loanPurposeOptions.push(value);
        });
      });
  }

  demolink() {
    this.loanStatus = 'PL';
    this.loanTitle = '開始填寫申請書';
    this.loanAmountLabel = '貸款金額';
    this.applyForm.get('p_period').setValidators([Validators.required]);
    this.applyForm.get('p_purpose').setValidators([Validators.required]);
    this.applyForm.updateValueAndValidity();
    this.applyForm.reset();
    this.applyForm.patchValue({
      p_period: '',
      p_purpose: '',
    });
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  showDialog(target) {
    showDialog(target);
  }

  onPhonepress(value) {
    const v = parseInt(value);
    if (v === NaN) {
      this.checkAmountErr = false;
    } else if (v >= 3 && v <= 600) {
      // "申請金額必須介於3~600萬"
      this.checkAmountErr = false;
    } else {
      this.checkAmountErr = true;
    }
  }

  /**
   * 檢核若貸款用途為其它，則須填寫貸款用途
   */
  onPurpose16() {
    const p1 = this.applyForm.get('p_purpose');
    const p2 = this.applyForm.get('p_purpose_name');
    if (p1.value === '16') { // 其它
      p2.setValidators([Validators.required, , Validators.maxLength(50)]);
    } else {
      p2.clearValidators();
    }
    p2.updateValueAndValidity();
  }

  onSubmit() {
    const req = {
      p_apy_amount: this.applyForm.get('p_apy_amount').value,
      p_period: this.applyForm.get('p_period').value,
      p_purpose: this.applyForm.get('p_purpose').value,
      p_purpose_name: this.applyForm.get('p_purpose_name').value,
    };

    // 更新狀態 01 000
    this.applyService.applyLoan()
    .pipe(takeWhile(() => this.alive))
    .subscribe();

    // console.log(req)
    this.applyService.applyInfo(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          // @save to sessionStorage
          const main = this.userDataService.applyMain;
          Object.assign(main, req);
          // this.userDataService.doLoanData(main, req);
          this.userDataService.applyMain = main;
          if (this.applyService.isOldOne(main.UserType)) {
            this.routerHelperService.navigate(['/apply/confirm']);
          } else { // 新戶
            this.routerHelperService.navigate(['/apply/person']);
          }
        } else {
          this.logger.showMsg(data);
        }
      });
  }
}