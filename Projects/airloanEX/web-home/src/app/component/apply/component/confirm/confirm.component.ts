import { formatDate } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { switchMap, takeWhile } from 'rxjs/operators';
import { DropdownItem } from 'src/app/interface/resultType';
import { initClickDialog } from 'src/app/js/confirm.page.js';
import { cityjson, countiesOptions, telValidate } from 'src/app/js/person.page.js';
import { initTextInputsBlur, initBlueButton, initCleaveNumber, showDialog, initCloseButtons, initCleaveDate, initCleaveMoney } from 'src/app/js/start.page.js';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  private alive = true;

  applyForm: FormGroup;
  ch1 = false;
  ch2 = false;
  options = countiesOptions;

  lblCompanyName = "公司名稱";
  lblJobStartTime = "到職年月";

  cityOption = [{ dataKey: '', dataName: '鄉鎮市區' }];
  cityOption2 = [{ dataKey: '', dataName: '鄉鎮市區' }];

  occupationOptions: DropdownItem[] = [{ dataKey: '', dataName: '請選擇' }];
  // industryOption = [
  //   { dataKey: '', dataName: '請選擇' },
  //   { dataKey: '農業', dataName: '農業' },
  //   { dataKey: '林業', dataName: '林業' },
  //   { dataKey: '漁業', dataName: '漁業' },
  //   { dataKey: '畜牧業', dataName: '畜牧業' },
  //   { dataKey: '服務業', dataName: '服務業' },
  //   { dataKey: '金融業', dataName: '金融業' },
  //   { dataKey: '金融服務業', dataName: '金融服務業' },
  //   { dataKey: '科技業', dataName: '科技業' },
  // ];
  positionOption = [
    { dataKey: '', dataName: '請選擇' },
    { dataKey: '課長', dataName: '課長' },
    { dataKey: '科長', dataName: '科長' },
    { dataKey: '班長', dataName: '班長' },
    { dataKey: '股長', dataName: '股長' },
    { dataKey: '助理', dataName: '助理' },
    { dataKey: '秘書', dataName: '秘書' },
    { dataKey: '上市或上櫃白領職員', dataName: '上市或上櫃白領職員' },
    { dataKey: '總經理', dataName: '總經理' },
  ];
  yearOption = [{ key: '', value: '請選擇' }, ];
  monthOption = [
    { key: '', value: '請選擇' },
    { key: '01', value: '01月' },
    { key: '02', value: '02月' },
    { key: '03', value: '03月' },
    { key: '04', value: '04月' },
    { key: '05', value: '05月' },
    { key: '06', value: '06月' },
    { key: '07', value: '07月' },
    { key: '08', value: '08月' },
    { key: '09', value: '09月' },
    { key: '10', value: '10月' },
    { key: '11', value: '11月' },
    { key: '12', value: '12月' },
  ];
  educationOptions: DropdownItem[] = [{ dataKey: '', dataName: '請選擇' }];
  // educationOption = [
  //   { dataKey: '', dataName: '請選擇' },
  //   { dataKey: '小學', dataName: '小學' },
  //   { dataKey: '國中', dataName: '國中' },
  //   { dataKey: '高中', dataName: '高中' },
  //   { dataKey: '專科', dataName: '專科' },
  //   { dataKey: '大學', dataName: '大學' },
  //   { dataKey: '研究所', dataName: '研究所' },
  // ];
  marriageOptions: DropdownItem[] = [{ dataKey: '', dataName: '請選擇' }];
  // maritalOption = [
  //   { dataKey: '', dataName: '請選擇' },
  //   { dataKey: '已婚', dataName: '已婚' },
  //   { dataKey: '未婚', dataName: '未婚' },
  //   { dataKey: '離婚', dataName: '離婚' },
  // ];
  estateOptions: DropdownItem[] = [{ dataKey: '', dataName: '請選擇' }];
  // realEstateOption = [
  //   { dataKey: '', dataName: '請選擇' },
  //   { dataKey: '持有', dataName: '持有' },
  //   { dataKey: '未持有', dataName: '未持有' },
  //   { dataKey: '配偶所有', dataName: '配偶所有' },
  // ];

  // 畫面顯示用
  data = null;
  // birthday = null;

  terms = [];
  dialogTitle: string;
  dialogData: string;

  @ViewChild('homePhone', {static: false}) homePhone: ElementRef;
  @ViewChild('domicilePhone', {static: false}) domicilePhone: ElementRef;
  @ViewChild('companyPhone', {static: false}) companyPhone: ElementRef;

  // dialog 使用
  initForm() {
    this.applyForm = this.fb.group({
      customer_name: ['', [Validators.required]],
      email_address: ['', [Validators.required, Validators.pattern(/^.*[@].*\..*$/)]],
      // birthday: ['', [Validators.required, Validators.maxLength(10), Validators.pattern(/^\d{4}\/\d{2}\/\d{2}$/)]],
      counties: ['', [Validators.required]],
      township: ['', [Validators.required]],
      address: ['', [Validators.required]],
      homePhone: ['', [Validators.required, Validators.maxLength(11), Validators.pattern(/(^(09)\d{8}$|^(07)-?\d{7,8}$|^(06)-?\d{7,8}$|^(05)-?\d{7,8}$|^(04)-?\d{7,8}$|^(03)-?\d{7,8}$|^(02)-?\d{7,8}$|^(0826)-?\d{5,6}$|^(0836)-?\d{5,6}$|^(082)-?\d{6,7}$|^(089)-?\d{6,7}$|^(049)-?\d{6,7}$|^(037)-?\d{6,7}$)/)]],
      domicilePhone: ['', [Validators.required, Validators.maxLength(11), Validators.pattern(/(^(09)\d{8}$|^(07)-?\d{7,8}$|^(06)-?\d{7,8}$|^(05)-?\d{7,8}$|^(04)-?\d{7,8}$|^(03)-?\d{7,8}$|^(02)-?\d{7,8}$|^(0826)-?\d{5,6}$|^(0836)-?\d{5,6}$|^(082)-?\d{6,7}$|^(089)-?\d{6,7}$|^(049)-?\d{6,7}$|^(037)-?\d{6,7}$)/)]],
      corp_name: ['', [Validators.required]],
      Occupation: ['', [Validators.required]],
      title: ['', [Validators.required]],
      year: ['', [Validators.required, CheckOnBoardDate]],
      month: ['', [Validators.required, CheckOnBoardDate]],
      companyCounties: ['', [Validators.required]],
      companyTownship: ['', [Validators.required]],
      companyAddress: ['', [Validators.required]],
      companyPhone: ['', [Validators.required, Validators.maxLength(11), Validators.pattern(/(^!09|^(08)-?\d{7,8}$|^(07)-?\d{7,8}$|^(06)-?\d{7,8}$|^(05)-?\d{7,8}$|^(04)-?\d{7,8}$|^(03)-?\d{7,8}$|^(02)-?\d{7,8}$|^(0826)-?\d{5,6}$|^(0836)-?\d{5,6}$|^(082)-?\d{6,7}$|^(089)-?\d{6,7}$|^(049)-?\d{6,7}$|^(037)-?\d{6,7}$)/)]], 
      corp_tel_exten: ['', [Validators.maxLength(8), Validators.pattern(/^\d{0,8}$/)]],
      education: ['', [Validators.required]],
      yearly_income: ['', [Validators.required, Validators.maxLength(5), Under25W]],
      marriage: ['', [Validators.required]],
      estate_type: ['', [Validators.required]],
      ch1: [''],
      ch2: [''],
    });

    if(this.logger.isCSRT){
      this.lblCompanyName = "露天賣家名稱";
      this.lblJobStartTime = "支付連帳號註冊時間";
    }
  }

  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService,
    private applyService: ApplyApiService,
    private userDataService: UserDataService,
    private commonDataService: CommonDataService,
    private logger: ApplyLoggerService,
  ) { }

  ngOnInit(): void {
    this.initForm();
  } // end ngOnInit

  ngAfterViewInit(): void {

    initCloseButtons();
    initClickDialog();
    initCleaveDate();
    initCleaveNumber();
    initCleaveMoney();

    this.applyService.applyPrepareConfirm()
      .pipe(
        takeWhile(() => this.alive),
        switchMap(data => {
          const main = this.userDataService.applyMain;
          if (data.status === 0) {
            const caseDataResp: any = data.result.caseDataResp;
            // @save to sessionStorage
            this.userDataService.doResume(main, caseDataResp);
            this.userDataService.applyMain = main;
          }
          return of(main);
        }),        
      )
      .subscribe(main => {
        if (main) {
          this.applyForm.patchValue(main);
          this.data = main;
          // this.birthday = new Date(main.birthday.substring(0, 4) + '-' + main.birthday.substring(4, 6) + '-' + main.birthday.substring(6, 8));
        }
        this.makeDistrict(this.applyForm.get('counties').value);
        this.makeDistrictCompany(this.applyForm.get('companyCounties').value);
        this.initYearList(main.birthday);
        this.selectOptions();
        this.initTerms();

        // 0312345678 轉換 03-12345678
        const homePhoneText = telValidate(this.homePhone.nativeElement);
        const domicilePhoneText = telValidate(this.domicilePhone.nativeElement)
        const companyPhoneText = telValidate(this.companyPhone.nativeElement);
        this.applyForm.get('homePhone').patchValue(homePhoneText);
        this.applyForm.get('domicilePhone').patchValue(domicilePhoneText);
        this.applyForm.get('companyPhone').patchValue(companyPhoneText);
        this.data.homePhone = homePhoneText;
        this.data.domicilePhone = domicilePhoneText;
        this.data.companyPhone = companyPhoneText;
      });
  }

  initYearList(birthday: string) {
    const day = +birthday.substring(0, 4) + 15;
    const yearDay = formatDate(new Date(), 'yyyy', 'zh-Hant');
    this.yearOption = [];
    for (let i = day; i <= +yearDay; i++) {
      this.yearOption.unshift({ key: i.toString(), value: i + '年' });
    }
    this.yearOption.unshift({ key: '', value: '請選擇' });
  }

  initTerms(): void {
    this.applyService.getTermList().subscribe(data => {
      if (data) {
        data.forEach(value => {
          this.terms[value.type] = { name: value.termName, title: value.termTitle };
        });
      }
    });
  }


  // 取得條款內容
  showTerms(target: string, termsName) {
    this.applyService
      .getTerms(termsName.name)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        try {
          // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
          const json = JSON.parse(data);
          this.dialogTitle = '查詢條款失敗';
          this.dialogData = '';
        } catch (error) {
          this.dialogTitle = termsName.title;
          this.dialogData = data;
          showDialog(target);
        }
      });
  }

  getOption(name, value) {
    if (this[name].filter(v => +v.dataKey === +value).length) {
      const dataName =  this[name].filter(v => +v.dataKey === +value)[0].dataName;
      if (dataName === '請選擇') {
        return ''; // 請填寫
      } else {
        return dataName;
      }
    }
  }

  selectOptions() {
    this.commonDataService.getOccupation()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        data.forEach(value => {
          this.occupationOptions.push(value);
        });
      });

    this.commonDataService.getEducation()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        data.forEach(value => {
          this.educationOptions.push(value);
        });
      });
    this.commonDataService.getMarriage()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        data.forEach(value => {
          this.marriageOptions.push(value);
        });
      });

    this.commonDataService.getEstate()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        data.forEach(value => {
          this.estateOptions.push(value);
        });
      });
  }

  /**
   * 判斷 dialog 確定取消功能
   */
  confirm_cancel(bl: boolean) {
    if (bl) {
      Object.assign(this.data, this.applyForm.getRawValue());
    } else {
      this.applyForm.patchValue(this.data);
    }
  }

  /**
   * 顯示 CSS 調整，空值時紅字
   */
  classname(id: string) {
    // this.data, this.applyForm
    if (this.data) {
      const value = this.data[id];
      if (!value || value === '請填寫' || !this.applyForm.get(id) || this.applyForm.get(id).invalid) {
        return 'txt-red';
      }
    }
  }

  /**
   * 欄位條件判斷
   */
  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  showDialog(target) {
    showDialog(target);
  }

  makeDistrict(value) {

    if (cityjson[value] === undefined) {
      this.applyForm.get('counties').patchValue('');
    }

    // 鄉鎮市區選項
    this.cityOption = [{ dataKey: '', dataName: '鄉鎮市區' }];
    if (value && cityjson[value]) {
      this.cityOption = this.cityOption.concat(cityjson[value]);
    } else {
      this.applyForm.get('township').patchValue('');
    }
  }

  makeDistrictCompany(value) {

    if (cityjson[value] === undefined) {
      this.applyForm.get('companyCounties').patchValue('');
    }

    // 鄉鎮市區選項
    this.cityOption2 = [{ dataKey: '', dataName: '鄉鎮市區' }];
    if (value && cityjson[value]) {
      this.cityOption2 = this.cityOption2.concat(cityjson[value]);
    } else {
      this.applyForm.get('companyTownship').patchValue('');
    }
  }

  telValidate(th) {
    this.applyForm.get(th.id).patchValue(telValidate(th));
  }

  sameTel(tel?: string) {
    const homePhone     = this.applyForm.get('homePhone');
    const domicilePhone = this.applyForm.get('domicilePhone');
    
    const ch2 = this.applyForm.get('ch2');
    // console.log('tel=', tel);
    // console.log('ch2.value=', ch2.value);
    if (ch2.value === true) {
      if (tel) {
        homePhone.patchValue(tel);
        domicilePhone.patchValue(tel);
      } else {
        domicilePhone.patchValue(homePhone.value);
      }
    }

    homePhone.markAsTouched();
    domicilePhone.markAsTouched();
  }

  onSubmit() {
    const req = {
      ch1: this.ch1,
      ch2: this.ch2,
      customer_name: this.applyForm.get('customer_name').value,
      // birthday: this.applyForm.get('birthday').value,
      email_address: this.applyForm.get('email_address').value,
      counties: this.applyForm.get('counties').value,
      township: this.applyForm.get('township').value,
      house_city_code: '',
      address: this.applyForm.get('address').value,
      house_address: '',
      homePhone: this.applyForm.get('homePhone').value,
      house_tel_area: '',
      house_tel: '',
      domicilePhone: this.applyForm.get('domicilePhone').value,
      ResTelArea: '',
      ResTel: '',
      corp_name: this.applyForm.get('corp_name').value,
      Occupation: this.applyForm.get('Occupation').value,
      title: this.applyForm.get('title').value,
      year: this.applyForm.get('year').value,
      month: this.applyForm.get('month').value,
      on_board_date: '',
      companyCounties: this.applyForm.get('companyCounties').value,
      companyTownship: this.applyForm.get('companyTownship').value,
      corp_city_code: '',
      companyAddress: this.applyForm.get('companyAddress').value,
      corp_address: '',
      companyPhone: this.applyForm.get('companyPhone').value,
      corp_tel_area: '',
      corp_tel: '',
      corp_tel_exten: this.applyForm.get('corp_tel_exten').value,
      education: this.applyForm.get('education').value,
      yearly_income: this.applyForm.get('yearly_income').value.replace(',', ''),
      marriage: this.applyForm.get('marriage').value,
      estate_type: this.applyForm.get('estate_type').value,

    };

    req.house_city_code = this.cityOption.filter(v => v.dataName === req.township)[0].dataKey;
    req.house_address = req.counties + req.township + ';' + req.address;
    if (req.homePhone.indexOf('-') !== -1) {
      req.house_tel_area = req.homePhone.split('-')[0];
      req.house_tel = req.homePhone.split('-')[1];
    } else {
      req.house_tel = req.homePhone;
    }
    if (req.domicilePhone.indexOf('-') !== -1) {
      req.ResTelArea = req.domicilePhone.split('-')[0];
      req.ResTel = req.domicilePhone.split('-')[1];
    } else {
      req.ResTel = req.domicilePhone;
    }

    req.on_board_date = (req.year ? req.year : '') + (req.month ? req.month : '') + '28';
    req.corp_city_code = this.cityOption2.filter(v => v.dataName === req.companyTownship)[0].dataKey;
    req.corp_address = req.companyCounties + req.companyTownship + ';' + req.companyAddress;
    if (req.companyPhone.indexOf('-') !== -1) {
      req.corp_tel_area = req.companyPhone.split('-')[0];
      req.corp_tel = req.companyPhone.split('-')[1];
    } else {
      req.corp_tel = req.companyPhone;
    }

    this.applyService.applyConfirm(req)
      .pipe(
        takeWhile(() => this.alive),
        switchMap(data => {
          const main = this.userDataService.applyMain;
          if (data.status === 0) {
            // @save to sessionStorage
            Object.assign(main, req);
            Object.assign(main, data.result);
            // this.userDataService.doPersonData(main, req);
            this.userDataService.applyMain = main;
            return of(true); // confirmed = true;
          } else {
            this.logger.showMsg(data);
            return of(false); // confirmed = false;
          }
        }),      
      )
      .subscribe(confirmed => {
          this.applyService.applyConfirmToOtp2()
          .pipe(takeWhile(() => this.alive))
          .subscribe(data2 => {
            if (data2.status === 0 && data2.result && data2.result.toOtp2) {
              // OTP 2 驗身
              // this.routerHelperService.navigate(['/apply/verify2']);
              // 產製PDF
              this.applyService.applyIdentified()
              .pipe(takeWhile(() => this.alive))
              .subscribe(data3 => {
                if (data3.status === 0) {
                  // @save to sessionStorage
                  const main = this.userDataService.applyMain;
                  Object.assign(main, req);
                  Object.assign(main, data3.result);
                  // this.userDataService.doPersonData(main, req);
                  this.userDataService.applyMain = main;
                  this.routerHelperService.navigate(['/UPL/step0']);
                } else {
                  this.logger.showMsg(data3);
                }
              });
            } else {
              // PCODE2566 他行帳戶驗身
              this.routerHelperService.navigate(['/apply/identify']);
            }
          });
      });
  }
} // end ConfirmComponent

/**
 * 到職年月：應檢核不可大於當月
 */
export function CheckOnBoardDate(control: FormControl): ValidationErrors {
  const fg = control.parent as FormGroup;
  if (fg) {
    const year = fg.get('year');
    const month = fg.get('month');

    if (year === control) {
      month.updateValueAndValidity();
      return null;
    }
    
    if (year && year.value && month && month.value) {
      const today = new Date();
      const onBoardDate = new Date(+year.value, (+month.value-1), 1); // 預設 yyyy-MM-01 但是寫入時是 yyyy-MM-28
      // console.log(today);
      // console.log(onBoardDate);
      if (onBoardDate.getTime() > today.getTime()) {
        return {forbiddenName: {value: month.value}, overMonth: true}
      }
    }
  }
  return null;
}

/**
 * 年收入：應檢核不可少於25萬
 */
export function Under25W(control: FormControl): ValidationErrors {
  if (control && control.value) {
    const w = control.value.replace(',', '');
    if (w < 25) {
      return {forbiddenName: {value: w}, under25W: true}
    }
  }
  return null;
}