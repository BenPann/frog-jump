import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { closeDialog, initAllCloseButtons, initCleaveNumber, initCloseButtons, showDialog } from 'src/app/js/start.page.js';
import { initRapidForm } from 'src/app/js/verify.page.js';
import { UserDataService } from 'src/app/service/user-data/user-data.service';

import { takeWhile, tap } from 'rxjs/operators';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { interval } from 'rxjs';
import { OtpService } from '../../../../service/otp/otp.service';

@Component({
  selector: 'app-verify2',
  templateUrl: './verify2.component.html',
  styleUrls: ['./verify2.component.scss']
})
export class Verify2Component implements OnInit, OnDestroy {
  private alive = true;
  private resetTimer = true;

  applyForm: FormGroup;

  // 計秒
  secText = 60;
  userPhoneNum = '';
  sendSuccess = false;

  // 彈跳視窗訊息
  dialogData: string;

  initForm() {
    this.applyForm = this.fb.group({
      tel1: [''],
      tel2: [''],
      tel3: [''],
      tel4: [''],
      tel5: [''],
      tel6: [''],
    });
  }

  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private elem: ElementRef,
    private routerHelperService: RouterHelperService,
    private applyService: ApplyApiService,
    private userDataService: UserDataService,
    private logger: ApplyLoggerService,
    private otpService: OtpService,
  ) { }

  ngAfterViewInit(): void {
    
    initCleaveNumber();
    initRapidForm(this.elem);
    initCloseButtons();
    initAllCloseButtons();

    setTimeout(() => this.sendOtp());
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  ngOnInit() {
    // 取前一頁面的行動電話
    const pcode2566_phone = this.userDataService.applyMain.pcode2566_phone;
    const mobile_tel = this.userDataService.applyMain.mobile_tel;
    if (pcode2566_phone) {
      this.userPhoneNum = pcode2566_phone;
    } else if (mobile_tel) {
      this.userPhoneNum = mobile_tel;
    }

    this.initForm();
  }

  checkValue(name: string, value: string) {
    if (value) {
      if (!/[0-9]$/.test(value)) {
        this.applyForm.get(name).patchValue('');
      } else if (this.applyForm.valid) {
        this.onSubmit();
      }
    }
  }

  errorForm() {
    document.forms[0].classList.add('error-form');
  }

  // 重領驗證碼
  sendSMS(): void {
    // 清除欄位號碼
    this.applyForm.reset();
    this.sendOtp();
  }

  sendOtp() {
    this.otpService
      .sendOtp(this.userPhoneNum)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          if (data.result.code === '0000') {
            showDialog('#dialog0');
            this.dialogData = '已發送簡訊至您的手機號碼，請輸入驗證碼進行最後確認';
            this.sendSuccess = true;
            // 計時
            this.setTime(60);
          } else {
            this.secText = 0;
          }
        } else {
          showDialog('#dialog0');
          this.dialogData = '發送簡訊驗證碼失敗，請稍後再試，或洽客服人員';
          this.secText = 0;
        }
      });
  }

  // 計時
  setTime(tempT: number): void {
    this.resetTimer = true;
    let total = tempT;
    this.secText = total;
    interval(1000)
      .pipe(
        takeWhile(() => total-- !== 0),
        takeWhile(() => this.resetTimer),
        takeWhile(() => this.alive),
        tap(data => (this.secText = total))
      )
      .subscribe();
  }

  onSubmit(): void {
    const form = this.applyForm;

    const req = form.get('tel1').value + form.get('tel2').value + form.get('tel3').value
              + form.get('tel4').value + form.get('tel5').value + form.get('tel6').value;
    // @call 8. OTP 2
    this.otpService
      .checkOtp(this.userPhoneNum, req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {

          // 產製PDF
          this.applyService.applyIdentified()
          .pipe(takeWhile(() => this.alive))
          .subscribe(data2 => {
            if (data2.status === 0) {
              // @save to sessionStorage
              const main = this.userDataService.applyMain;
              Object.assign(main, req);
              Object.assign(main, data2.result);
              // this.userDataService.doPersonData(main, req);
              this.userDataService.applyMain = main;
              this.routerHelperService.navigate(['/UPL/step0']);
            } else {
			  //create new case fail or timeout
              //this.logger.showMsg(data2);
			  this.routerHelperService.navigate(['/UPL/step0']);
            }
          });
		  
        } else {
			
		  //check otp fail
          this.logger.showMsg(data);
		  
        }
      });
  }
}
