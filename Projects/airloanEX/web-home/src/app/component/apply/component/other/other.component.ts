import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { initBlueButton, initTextInputsBlur, initCleaveNumber, initCleaveMoney } from 'src/app/js/start.page.js';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { DropdownItem } from 'src/app/interface/resultType';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';

@Component({
  selector: 'app-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.scss']
})
export class OtherComponent implements OnInit {
  private alive = true;

  applyForm: FormGroup;
  educationOptions: DropdownItem[] = [{ dataKey: '', dataName: '請選擇' }];
  //   { dataKey: '小學', dataName: '小學' },
  //   { dataKey: '國中', dataName: '國中' },
  //   { dataKey: '高中', dataName: '高中' },
  //   { dataKey: '專科', dataName: '專科' },
  //   { dataKey: '大學', dataName: '大學' },
  //   { dataKey: '研究所', dataName: '研究所' },
  marriageOptions: DropdownItem[] = [{ dataKey: '', dataName: '請選擇' }];
  // { key: '單身', value: '單身' },
  // { key: '已婚', value: '已婚' },
  estateOptions: DropdownItem[] = [{ dataKey: '', dataName: '請選擇' }];
  // { key: '本人所有', value: '本人所有' },
  // { key: '家族所有', value: '家族所有' },
  // { key: '配偶所有', value: '配偶所有' },
  // { key: '無', value: '無' },

  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private elme: ElementRef,
    private routerHelperService: RouterHelperService,
    private applyService: ApplyApiService,
    private userDataService: UserDataService,
    private commonDataService: CommonDataService,
    private logger: ApplyLoggerService
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.selectOptions();
    initCleaveNumber();
    initCleaveMoney();

    const main = this.userDataService.applyMain;
    this.applyForm.patchValue(main);
  }

  initForm() {
    this.applyForm = this.fb.group({
      education: ['', [Validators.required]],
      yearly_income: ['', [Validators.required, Validators.maxLength(5), Under25W]],
      marriage: ['', [Validators.required]],
      estate_type: ['', [Validators.required]],
    });
  }

  selectOptions() {
    this.commonDataService.getEducation()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        data.forEach(value => {
          this.educationOptions.push(value);
        });
      });
    this.commonDataService.getMarriage()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        data.forEach(value => {
          this.marriageOptions.push(value);
        });
      });

    this.commonDataService.getEstate()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        data.forEach(value => {
          this.estateOptions.push(value);
        });
      });
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  onSubmit() {
    const req = {
      education: this.applyForm.get('education').value,
      yearly_income: this.applyForm.get('yearly_income').value.replace(',', ''),
      marriage: this.applyForm.get('marriage').value,
      estate_type: this.applyForm.get('estate_type').value,
    };

    this.applyService.applyInfo(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        // console.log(data);
        if (data.status === 0) {
          // @save to sessionStorage
          const main = this.userDataService.applyMain;
          Object.assign(main, req);
          // this.userDataService.doPersonData(main, req);
          this.userDataService.applyMain = main;
          this.routerHelperService.navigate(['/apply/confirm']);
        } else {
          this.logger.showMsg(data);
        }
      });
  }
}

/**
 * 年收入：應檢核不可少於25萬
 */
export function Under25W(control: FormControl): ValidationErrors {
  if (control && control.value) {
    const w = control.value.replace(',', '');
    if (w < 25) {
      return {forbiddenName: {value: w}, under25W: true}
    }
  }
  return null;
}
