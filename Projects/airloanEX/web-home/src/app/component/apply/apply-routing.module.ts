import { PaperComponent } from './component/paper/paper.component';
import { OtherwayComponent } from './component/otherway/otherway.component';
import { Verify2Component } from './component/verify2/verify2.component';
import { IdentifyComponent } from './component/identify/identify.component';
import { ConfirmComponent } from './component/confirm/confirm.component';
import { OtherComponent } from './component/other/other.component';
import { WorkComponent } from './component/work/work.component';
import { PersonComponent } from './component/person/person.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartComponent } from './component/start/start.component';
import { VerifyComponent } from './component/verify/verify.component';
import { LoanComponent } from './component/loan/loan.component';
import { LoanoldComponent } from './component/loan.old/loanold.component';
import { LoanrplComponent } from './component/loan.rpl/loanrpl.component';
import { LoancsrtComponent } from './component/loan.csrt/loancsrt.component';

import { TokenGuard } from 'src/app/guard/token.guard';
import { LeaveGuard } from 'src/app/guard/leave.guard';

const routes: Routes = [
  {
    path: 'start',
    component: StartComponent
  },
  {
    path: 'verify',
    component: VerifyComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    // data: { browseLog: true }
  },
  {
    path: 'loan',
    component: LoanComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'loan.rpl',
    component: LoanrplComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'loan.old',
    component: LoanoldComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'loan.csrt',
    component: LoancsrtComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'person',
    component: PersonComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'work',
    component: WorkComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'other',
    component: OtherComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'confirm',
    component: ConfirmComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'identify',
    component: IdentifyComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'verify2',
    component: Verify2Component,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    // data: { browseLog: true }
  },
  {
    path: 'otherway',
    component: OtherwayComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'paper',
    component: PaperComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplyRoutingModule { }
