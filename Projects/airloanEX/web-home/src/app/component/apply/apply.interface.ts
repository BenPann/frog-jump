export interface LogonRequest {
    idno?: string;
    birthday?: string;
    entry: string;
    uniqType: string;
}

export interface ApplyStartRequest {
    idno: string;
    birthday: string;
    phone: string;
    agentNo: string;
    forceNewApply?: string;
    entry: string;
    uniqType: string;
}

export interface ApplyInfoRequest {
    p_apy_amount?: string;
    p_period?: string;
    p_purpose?: string;
    // 個人資料
    customer_name?: string;
    email_address?: string;
    house_city_code?: string;
    house_address?: string;
    house_tel_area?: string;
    house_tel?: string;
    ResTelArea?: string;
    ResTel?: string;
    // 工作資料
    corp_name?: string;
    Occupation?: string;
    title?: string;
    on_board_date?: string;
    counties?: string;
    township?: string;
    corp_city_code?: string;
    corp_address?: string;
    corp_tel_area?: string;
    corp_tel?: string;
    corp_tel_exten?: string;
    // 其他資料
    education?: string;
    yearly_income?: string;
    marriage?: string;
    estate_type?: string;

    // 用他行帳號驗證您的身分 未比對DB名稱
    // 銀行別
    bank?: string;
    // 銀行帳號
    bankAccount?: string;
    // 手機號碼
    phone?: string;
    // 請登入查看核貸內容
    idno?: string;
    birthday?: string;
    // 填寫撥款及繳款資訊
    grantBank?: string;
    appropriationBranch?: string;
    grantAccount?: string;
    paymentDate?: string;
    payingBank?: string;
    paymentAccount?: string;
} // end ApplyInfoRequest

export interface ApplyUploadIdcard {
    idcardDate: string; // 發證日期
    idcardLocation: string; // 發證地點
    idcardCRecord: string; // 發證紀錄
    countiesName: string; // 縣市
    townshipName: string; // 鄉鎮市區
    township: string; // zipcode
    address: string; // 地址
    marriage: string; // 婚姻
}
/** 同一關係人表資料 */
export interface ApplyRelationDegreeRequest {
    idno: string;
    customer_name: string;

    relationship: string;
    relationshipName: string;
    relationshipId: string;

    addData: ApplyRelationDegreeCorp[];
}

/** 信用卡新卡申請流程 */
export interface ApplyNewCreditcardRequest {
    nitoCard: string;
    englishName: string;
    billingType: string;
    mailingAddress: string;
    counties: string;
    township: string;
    address: string;
    city_code: string;
    fullAddress: string;
    workPhone: string;
    extension: string;
    tel_area: string;
    tel: string;
    ch1: string;
    ch2: string;
}
/** 二親等以內血親 */
export interface ApplySecondDegree {
    relationship: string;
    relationshipName: string;
    relationshipId: string;
}
/** 若您或您的配偶擔任「企業負責人」,請填寫 */
export interface ApplyRelationDegreeCorp {
    corpRelationDegree: string;
    corpName: string;
    corpnumber: string;
    corpJobTitle: string;
    memo: string;
}
/** 信貸申請流程 */
export interface ApplyMain {
    UserType: string
    CaseNo: string;
    ProductId: string;
    entry: string;
    entryOther: string;

    // 基本資料
    idno: string;
    birthday: string; // yyyyMMdd
    phone: string;    // 用戶輸入的行動電話
    agentNo: string;

    // 申請書
    // 貸款金額
    p_apy_amount: string;
    // 貸款期間
    p_period: string;
    // 貸款用途
    p_purpose: string;
    // 填寫貸款用途
    p_purpose_name: string;

    // 個人資料
    // 姓名
    customer_name: string;
    // 電子信箱
    email_address: string;
    // 居住地址
    counties?: string;
    township?: string;
    address?: string;
    house_city_code: string;
    house_address: string;
    // 住家市話
    homePhone?: string;
    house_tel_area: string;
    house_tel: string;
    // 戶籍市話
    domicilePhone?: string;
    ResTelArea: string;
    ResTel: string;
    // 工作資料
    // 公司名稱
    corp_name: string;
    // 行業類別
    Occupation: string;
    // 職稱
    title: string;
    // 到職年月
    year?: string;
    month?: string;
    on_board_date: string;
    // 公司地址
    companyCounties?: string;
    companyTownship?: string;
    companyAddress?: string;
    corp_city_code: string;
    corp_address: string;
    // 公司電話
    companyPhone?: string;
    corp_tel_area: string;
    corp_tel: string;
    corp_tel_exten: string;

    // 其他資料
    // 學歷
    education: string;
    // 年收入(萬元)
    yearly_income: string;
    // 婚姻狀況
    marriage: string;
    // 不動產狀態
    estate_type: string;
    // 用他行帳號驗證您的身分
    bank?: string;
    bankAccount?: string;
    // phone?: string;
    // 關係人列表
    relationship: string;
    relationshipName: string;
    relationshipId: string;

    addSecondDegreeData: ApplySecondDegree[];
    addData: ApplyRelationDegreeCorp[];

    grantBank?: string;
    appropriationBranch?: string;
    grantAccount?: string;
    bankPhone?: string;
    paymentDate?: string;
    payingBank?: string;
    paymentAccount?: string;

    mobile_tel: string; // 打otp的行動電話
    pcode2566_phone: string;
    stempDataType: string; // DATA_TYPE 客戶類型(1.信用卡 2.現金卡(靈活卡) 3.存款 4.貸款) 目的是給apply verify 簡訊驗證顯示用戶於本行有效行動電話之服務名稱

    hasOldIdImg;
    hasOldFinaImg;
}

export class ApplyMainModel implements ApplyMain {
    UserType: string;
    CaseNo: string;
    ProductId: string;
    entry: string;
    entryOther: string;

    // 基本資料
    idno: string;
    birthday: string; // yyyyMMdd
    phone: string;    // 用戶輸入的行動電話
    agentNo: string;

    // 申請書
    // 貸款金額
    p_apy_amount: string;
    // 貸款期間
    p_period: string;
    // 貸款用途
    p_purpose: string;
    // 填寫貸款用途
    p_purpose_name: string;

    // 個人資料
    // 姓名
    customer_name: string;
    // 電子信箱
    email_address: string;
    house_city_code: string;
    house_address: string;
    house_tel_area: string;
    house_tel: string;
    ResTelArea: string;
    ResTel: string;

    // 工作資料
    // 公司名稱
    corp_name: string;
    // 行業類別
    Occupation: string;
    // 職稱
    title: string;
    // 到職年月
    on_board_date: string;
    // 公司地址
    corp_city_code: string;
    corp_address: string;
    // 公司電話
    corp_tel_area: string;
    corp_tel: string;
    corp_tel_exten: string;

    // 其他
    // 學歷
    education: string;
    // 年收入(萬元)
    yearly_income: string;
    // 婚姻狀況
    marriage: string;
    // 不動產狀態
    estate_type: string;

    // 關係人列表
    relationship: string;
    relationshipName: string;
    relationshipId: string;

    addSecondDegreeData: ApplySecondDegree[];
    addData: ApplyRelationDegreeCorp[];

    mobile_tel: string; // 打otp的行動電話
    pcode2566_phone: string;
    stempDataType: string; // DATA_TYPE 客戶類型(1.信用卡 2.現金卡(靈活卡) 3.存款 4.貸款) 目的是給apply verify 簡訊驗證顯示用戶於本行有效行動電話之服務名稱

    hasOldIdImg;
    hasOldFinaImg;
}
/** 額度利率體驗 */
export interface CalMain {
    CASE_NO: string;
    EXP_NO: string;
    CREDIT_LINE: string;
    INTEREST_RATE: string;
}

export class CalMainModel implements CalMain {
    CASE_NO: string;
    EXP_NO: string;
    CREDIT_LINE: string;
    INTEREST_RATE: string;
}

export interface NCCCReq {
    pan: string;
    expDate: string;
    extNo: string;
}

export interface QryQa {
  CHECK_CODE: string;
  ERR_MSG: string;
  CASE_NO: string;
  QA_DATA: QA_DATA[];
  SEL_DATA: SEL_DATA[];
}
export interface QA_DATA {
  var_code: string;
  quest_no: string;
  quest_desc: string;
  quest_kind: string;
  quest_kind2: string;
  quest_length: string;
  quest_type: string;
}
export interface SEL_DATA {
  var_code: string;
  quest_no: string;
  quest_desc: string;
  quest_kind: string;
  quest_type: string;

  sel_no: string;
  sel_desc: string;
  sel_value: string;
}