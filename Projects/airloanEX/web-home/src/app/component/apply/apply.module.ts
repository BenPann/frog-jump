import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApplyRoutingModule } from './apply-routing.module';
import { StartComponent } from './component/start/start.component';
import { VerifyComponent } from './component/verify/verify.component';
import { LoanComponent } from './component/loan/loan.component';
import { PersonComponent } from './component/person/person.component';
import { WorkComponent } from './component/work/work.component';
import { OtherComponent } from './component/other/other.component';
import { ConfirmComponent } from './component/confirm/confirm.component';
import { IdentifyComponent } from './component/identify/identify.component';
import { OtherwayComponent } from './component/otherway/otherway.component';
import { Verify2Component } from './component/verify2/verify2.component';
import { PaperComponent } from './component/paper/paper.component';
import { LoanoldComponent } from './component/loan.old/loanold.component';
import { LoanrplComponent } from './component/loan.rpl/loanrpl.component';
import { LoancsrtComponent } from './component/loan.csrt/loancsrt.component';
import { NgxCleaveDirectiveModule } from 'ngx-cleave-directive';
import { SharedModule } from 'src/app/shared/shared.module';
@NgModule({
  declarations: [
    StartComponent,
    VerifyComponent,
    LoanComponent,
    LoanrplComponent,
    LoancsrtComponent,
    LoanoldComponent,
    PersonComponent,
    WorkComponent,
    OtherComponent,
    ConfirmComponent,
    IdentifyComponent,
    OtherwayComponent,
    Verify2Component,
    PaperComponent,

  ],
  imports: [
    CommonModule,
    ApplyRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCleaveDirectiveModule,
    SharedModule,
  ],
})
export class ApplyModule { }
