import { of } from 'rxjs';
import { Component, OnInit, OnDestroy, AfterContentInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { fillProject, desktopSlide, homePage, initSlides } from '../../js/home.page.js.js';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterContentInit, OnDestroy {

  constructor(
    // private router: Router,
    // private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    console.log('htom');
    document.body.className = 'all-white';

    initSlides();
    homePage();
  }
  ngAfterContentInit(): void {

  }

  ngOnDestroy(): void {
    document.body.className = '';
  }

  fillProject(th: HTMLElement): void {
    return fillProject(th);
  }

  desktopSlide(direction: number) {
    return desktopSlide(direction);
  }
}

