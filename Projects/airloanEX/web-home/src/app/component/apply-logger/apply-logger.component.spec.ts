import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyLoggerComponent } from './apply-logger.component';

describe('ApplyLoggerComponent', () => {
  let component: ApplyLoggerComponent;
  let fixture: ComponentFixture<ApplyLoggerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplyLoggerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyLoggerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
