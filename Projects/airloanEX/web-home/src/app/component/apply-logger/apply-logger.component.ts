import { Component, OnInit } from '@angular/core';
import {initCloseButtons, initAllCloseButtons, showDialog, closeDialog} from 'src/app/js/start.page.js';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';

@Component({
  selector: 'app-apply-logger',
  templateUrl: './apply-logger.component.html',
  styleUrls: ['./apply-logger.component.scss']
})
export class ApplyLoggerComponent {

  constructor(public logger: ApplyLoggerService) { }

  closeDialog(){
    closeDialog();
  }

  redirectRT(){
    const rtRedirectUrl = sessionStorage.getItem("rtRedirectUrl");    
    window.location.href = rtRedirectUrl;
  }
}
