import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { initTextInputsBlur, initBlueButton, showDialog, initCloseButtons, initCleaveNumber } from 'src/app/js/start.page.js';
import {
  telValidate,
  cityjson, countiesOptions
} from 'src/app/js/person.page.js';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { CreditcardApiService } from 'src/app/service/creditcard/creditcard-api.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { takeWhile } from 'rxjs/operators';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';

import { ApplyNewCreditcardRequest } from 'src/app/component/apply/apply.interface';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';

@Component({
  selector: 'app-gift',
  templateUrl: './gift.component.html',
  styleUrls: ['./gift.component.scss']
})
export class GiftComponent implements OnInit {
  private alive = true;

  myAddressHidden = true;
  giftInfo = true;
  applyForm: FormGroup;

  nitoCardOptions = [{ giftCode: '', giftTitle: '請選擇', giftImage: '', giftNote: '' }];
  options = countiesOptions;
  cityOption = [{ dataKey: '', dataName: '鄉鎮市區' }];

  billTypeOptions = [{ dataKey: '', dataName: '請選擇' }];

  public giftInfoImage = '';
  public giftInfoNote = '';

  terms = [];
  dialogTitle: string;
  dialogData: string;

  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private applyService: ApplyApiService,
    private commonDataService: CommonDataService,
    private creditcardApiService: CreditcardApiService,
    private logger: ApplyLoggerService
  ) { }

  ngOnInit(): void {
    this.initForm();

    this.initNitoCardOptions();
    this.initBillTypeOptions();

    this.initTerms();
  } // end ngOnInit

  ngAfterViewInit(): void {
    // initTextInputsBlur();
    // initBlueButton();
    initCloseButtons();
    initCleaveNumber();
  }

  initTerms(): void {
    this.applyService.getTermList().subscribe(data => {
      if (data) {
        data.forEach(value => {
          this.terms[value.type] = { name: value.termName, title: value.termTitle };
        });
      }
    });
  }

  // 取得條款內容
  showTerms(target: string, termsName) {
    this.applyService
      .getTerms(termsName.name)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        try {
          // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
          const json = JSON.parse(data);
          this.dialogTitle = '查詢條款失敗';
          this.dialogData = '';
        } catch (error) {
          this.dialogTitle = termsName.title;
          this.dialogData = data;
          showDialog(target);
        }
      });
  }

  initForm() {
    this.applyForm = this.fb.group({
      nitoCard: ['', [Validators.required]],
      englishName: ['', [Validators.required]],
      billingType: ['', [Validators.required]],
      mailingAddress: ['', [Validators.required]],
      counties: [''],
      township: [''],
      address: [''],
      workPhone: ['', [Validators.required, Validators.maxLength(11), Validators.pattern(/^\d{2}-?\d{8}$|^\d{2}-?\d{7}$|^\d{3}-?\d{6}$|^\d{3}-?\d{7}$|^\d{4}-?\d{5}$|^\d{4}-?\d{5}$|^09\d{2}-?\d{6}$/)]],
      extension: ['', [, Validators.maxLength(8)]],
      ch1: [false],
      ch2: [false],

    });
  } // end initForm

  initNitoCardOptions() {
    // 信用卡首刷禮
    this.creditcardApiService.firstGiftList()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        console.log(data);
        if (data.status === 0) {
          data.result.forEach(value => {
            this.nitoCardOptions.push(
              {
                giftCode: value.giftCode,
                giftTitle: value.giftTitle,
                giftImage: value.giftImage,
                giftNote: value.giftNote
              });
          });
        }
      });
  } // end selectOptions

  initBillTypeOptions() {
    this.commonDataService.getBillType()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        data.forEach(value => {
          this.billTypeOptions.push(value);
        });
      });
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  showDialog(target) {
    showDialog(target);
  }

  telValidate(th) {
    this.applyForm.get(th.id).patchValue(telValidate(th));
  }

  makeDistrict(value) {

    if (cityjson[value] === undefined) {
      this.applyForm.get('counties').patchValue('');
    }

    this.cityOption = [{ dataKey: '', dataName: '鄉鎮市區' }];
    if (value && cityjson[value]) {
      this.cityOption = this.cityOption.concat(cityjson[value]);
    } else {
      this.applyForm.get('township').patchValue('');
    }
  }

  addressHidden(bl: boolean) {
    // addressHidden(bl);
    this.myAddressHidden = !bl;
    if (bl) {
      ['counties', 'township', 'address'].forEach(value => {
        this.applyForm.get(value).setValidators([Validators.required]);
        this.applyForm.get(value).updateValueAndValidity();
      });
    } else {
      ['counties', 'township', 'address'].forEach(value => {
        this.applyForm.patchValue({
          counties: '',
          township: '',
          address: '',
        });
        this.applyForm.get(value).clearValidators();
        this.applyForm.get(value).updateValueAndValidity();
      });
    }
  }

  clickGift(nitoCard) {
    this.nitoCardOptions.forEach(o => {
      if (nitoCard === o.giftCode) {
        this.giftInfoImage = o.giftImage;
        this.giftInfoNote = o.giftNote;
      }
    });
  }

  onSubmit() {
    const req: ApplyNewCreditcardRequest = {
      nitoCard: this.applyForm.get('nitoCard').value,
      englishName: this.applyForm.get('englishName').value,
      billingType: this.applyForm.get('billingType').value,
      mailingAddress: this.applyForm.get('mailingAddress').value,
      counties: this.applyForm.get('counties').value,
      township: this.applyForm.get('township').value,
      address: this.applyForm.get('address').value,
      city_code: '',
      fullAddress: '',
      workPhone: this.applyForm.get('workPhone').value,
      extension: this.applyForm.get('extension').value,
      tel_area: '',
      tel: '',
      ch1: this.applyForm.get('ch1').value,
      ch2: this.applyForm.get('ch2').value,
    };

    if (!this.myAddressHidden) {
      req.city_code = this.cityOption.filter(v => v.dataName === req.township)[0].dataKey;
      req.fullAddress = req.counties + req.township + req.address;
    }
    if (req.workPhone.indexOf('-') !== -1) {
      req.tel_area = req.workPhone.split('-')[0];
      req.tel = req.workPhone.split('-')[1];
    } else {
      req.tel = req.workPhone;
    }

    this.applyService.applyNewCreditcard(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        console.log(data);
        if (data.status === 0) {
          this.routerHelperService.navigate(['/CAD/verify']);
        } else {
          this.logger.showMsg(data);
        }
      });
  }
}
