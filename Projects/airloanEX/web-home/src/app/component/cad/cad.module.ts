import { VerifyComponent } from './verify/verify.component';
import { NewcardComponent } from './newcard/newcard.component';
import { GiftComponent } from './gift/gift.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CADRoutingModule } from './cad-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxCleaveDirectiveModule } from 'ngx-cleave-directive';
import { InputMaskDirective } from 'src/app/service/cleave/input-mask.directive';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    GiftComponent,
    NewcardComponent,
    VerifyComponent,
    InputMaskDirective,

  ],
  imports: [
    CommonModule,
    CADRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCleaveDirectiveModule,
    SharedModule,
  ]
})
export class CADModule { }
