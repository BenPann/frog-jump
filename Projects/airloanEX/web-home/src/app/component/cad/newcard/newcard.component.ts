import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';
import { initTextInputsBlur, initBlueButton, showDialog, initCloseButtons } from 'src/app/js/start.page.js';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';

@Component({
  selector: 'app-newcard',
  templateUrl: './newcard.component.html',
  styleUrls: ['./newcard.component.scss']
})
export class NewcardComponent implements OnInit {
  private alive = true;

  ch1 = false;
  ch2 = false;

  terms = [];
  dialogTitle: string;
  dialogData: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private routerHelperService: RouterHelperService,
    private applyService: ApplyApiService,

  ) { }

  ngOnInit(): void {
    this.initTerms();
  } // end ngOnInit

  ngAfterViewInit(): void {
    // initTextInputsBlur();
    // initBlueButton();
    initCloseButtons();
  }

  initTerms(): void {
    this.applyService.getTermList().subscribe(data => {
      if (data) {
        data.forEach(value => {
          this.terms[value.type] = { name: value.termName, title: value.termTitle };
        });
      }
    });
  }

  // 取得條款內容
  showTerms(target: string, termsName) {
    this.applyService
      .getTerms(termsName.name)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        try {
          // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
          const json = JSON.parse(data);
          this.dialogTitle = '查詢條款失敗';
          this.dialogData = '';
        } catch (error) {
          this.dialogTitle = termsName.title;
          this.dialogData = data;
          showDialog(target);
        }
      });
  }


  showDialog(target) {
    showDialog(target);
  }

  onSubmit() {
    this.routerHelperService.navigate(['/CAD/gift']);
  }

}
