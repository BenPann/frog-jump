import { VerifyComponent } from './verify/verify.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewcardComponent } from './newcard/newcard.component';
import { GiftComponent } from './gift/gift.component';

import { TokenGuard } from 'src/app/guard/token.guard';
import { LeaveGuard } from 'src/app/guard/leave.guard';

const routes: Routes = [
  {
    path: 'newcard',
    component: NewcardComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'gift',
    component: GiftComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'verify',
    component: VerifyComponent,
    canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CADRoutingModule { }
