import { Component, OnInit, ElementRef, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { initTextInputsBlur, initBlueButton, initCloseButtons, showDialog, initCleaveNumber } from 'src/app/js/start.page.js';
import { initRapidForm } from 'src/app/js/verify.page.js';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { NCCCReq } from 'src/app/service/validate/validate.interface';
import { takeWhile } from 'rxjs/operators';
import { ValidateService } from 'src/app/service/validate/validate.service';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {
  constructor(
    public cleave: CleaveService,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private elem: ElementRef,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private applyService: ApplyApiService,
    private validateService: ValidateService,
    private logger: ApplyLoggerService
  ) { }

  price = 10;
  private alive = true;

  applyForm: FormGroup;

  onChange(event) {
    console.log(event.target);
  }
  initForm() {
    this.applyForm = this.fb.group({
      cardNumber1: ['', [Validators.required, Validators.minLength(4), Validators.pattern(/^\d{4}$/)]],
      cardNumber2: ['', [Validators.required, Validators.minLength(4), Validators.pattern(/^\d{4}$/)]],
      cardNumber3: ['', [Validators.required, Validators.minLength(4), Validators.pattern(/^\d{4}$/)]],
      cardNumber4: ['', [Validators.required, Validators.minLength(4), Validators.pattern(/^\d{4}$/)]],
      month: ['', [Validators.required, Validators.minLength(2), Validators.pattern(/^(0[1-9]|1[0-2])$/)]],
      Year: ['', [Validators.required, Validators.minLength(2), Validators.pattern(/^\d{2}$/)]],
      cardBehind: ['', [Validators.required, Validators.minLength(3), Validators.pattern(/^\d{3}$/)]],
    });
  }

  ngOnInit(): void {
    this.initForm();
    initRapidForm(this.elem);
  }

  ngAfterViewInit(): void {
    // initTextInputsBlur();
    // initBlueButton();
    initCloseButtons();
    initCleaveNumber();
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  showDialog(target) {
    showDialog(target);
  }

  onSubmit() {
    const req: NCCCReq = {
      pan:
        this.applyForm.get('cardNumber1').value
      + this.applyForm.get('cardNumber2').value
      + this.applyForm.get('cardNumber3').value
      + this.applyForm.get('cardNumber4').value,
      expDate: this.applyForm.get('month').value + this.applyForm.get('Year').value,
      extNo: this.applyForm.get('cardBehind').value,
    };
    this.validateService.setNCCC(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.result.statusCode === '') {
          this.applyService.previewApplyCreditCard()
            .pipe(takeWhile(() => this.alive))
            .subscribe(data2 => {
              if (data2.status === 0) {
                this.routerHelperService.navigate(['/UPL/step0']);
              } else {
                // 產製失敗
                this.logger.showMsg(data2);
              }
            });
        } else {
          // 信用卡驗證失敗
          this.logger.showMsg(data);
        }
      });
  }

}
