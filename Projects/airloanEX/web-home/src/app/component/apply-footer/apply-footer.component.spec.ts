import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyFooterComponent } from './apply-footer.component';

describe('ApplyFooterComponent', () => {
  let component: ApplyFooterComponent;
  let fixture: ComponentFixture<ApplyFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplyFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
