import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyHeaderComponent } from './apply-header.component';

describe('ApplyHeaderComponent', () => {
  let component: ApplyHeaderComponent;
  let fixture: ComponentFixture<ApplyHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplyHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
