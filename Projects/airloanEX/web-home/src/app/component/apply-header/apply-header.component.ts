import { Component, OnInit } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { ActivatedRoute, NavigationEnd, ParamMap, Router } from "@angular/router";
import { Observable, of } from 'rxjs';
import { filter, map, switchMap, catchError } from 'rxjs/operators';
import { ApplyMain, CalMain } from 'src/app/component/apply/apply.interface';
import { showChatBot, closeChatBot, closeDialog, showDialog } from "src/app/js/start.page.js";
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { UserDataService } from "../../service/user-data/user-data.service";

@Component({
  selector: "app-apply-header",
  templateUrl: "./apply-header.component.html",
  styleUrls: ["./apply-header.component.scss"],
})
export class ApplyHeaderComponent implements OnInit {
  page = '';

  reqTrigger = {productType: '', flowType: '', pageName: '', subPageName: ''}; // input json
  
  chatbotUrl$: Observable<string>;
  chatbotUrl: string;
  videoUrl;
  // dangerousVideoUrl = "http://172.16.253.124/chat-room/index.html?userId=&feedback=true&welcome=true&te_trigger=RPL_Application_Start&dimension=PL&session_id=";

  productId = '2'; // 1 PA, (預設)2 PL, 3 RPL, 4 GM

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private userDataService: UserDataService,
    private routerHelperService: RouterHelperService,
    private applyService: ApplyApiService,
  ) {
    // angular 10 calling component function from iframe
    // angular 10 iframe parent javascript function call
    (<any>window).showContactUsDialog = this.showContactUsDialog.bind(this);
    (<any>window).closeChatBot = this.closeChatBot.bind(this);
    (<any>window).sessiontimeout = this.sessiontimeout.bind(this);

    // console.log('### begin');
    this.router.events
    .pipe(
      filter(event => event instanceof NavigationEnd),
      map((evnets: NavigationEnd) => {
        const n1 = this.activatedRoute?.firstChild;
        const n2 = n1?.firstChild;
        return {
          // route: evnets,
          n1,
          n2
        };
      }),
      switchMap(item => {
        const n1 = item.n1;
        const n2 = item.n2;
        let flowType: string;
        let pageName: string; 
        if (n1) {
          this.page += n1.snapshot.url.toString();
          flowType = n1.snapshot.url.toString();
        }
        if (n2) {
          this.page += n2.snapshot.url.toString();
          pageName = n2.snapshot.url.toString();
        }
    
        const main = this.userDataService.applyMain;
        const ProductId = main?.ProductId;
        let productType = this.applyService.makeProductType(ProductId);
        // 測試程式碼
        // ProductType = 'PL';
        // flowType = 'apply';
        // pageName = 'start';
    
        const req = {productType, flowType, pageName, subPageName: ''};
        return of(req);
      }),
      switchMap(req => {
        if (req.productType === '') {
          this.activatedRoute.queryParamMap.subscribe((params: ParamMap) => {
            const pt = params.get('PT');
            
            if (pt) {
              this.productId = pt;
            } else {
              this.productId = '2'; // 2 PL
            }
            const productType = this.applyService.makeProductType(this.productId);
            req.productType = productType;
            // console.log('productId=' + this.productId);
            // console.log('productType=' + productType);
          });
        }
        return of(req);
      })
    ).subscribe(req => { 
      this.reqTrigger = req;
      
      this.applyService.getTrigger(this.reqTrigger.productType, this.reqTrigger.flowType, this.reqTrigger.pageName).subscribe(data => {
        if (data.status === 0 && data.result) {
          this.chatbotUrl = data.result.chatbotUrl;
          this.chatbotUrl = this.chatbotUrl.replace('${welcome}', 'true');
          this.chatbotUrl = this.chatbotUrl.replace('${session_id}', '');
          // console.log('### this.chatbotUrl=', this.chatbotUrl);
          this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.chatbotUrl);
          // console.log(this.reqTrigger); // 例如： {productType: "PL", flowType: "apply", pageName: "start"}
          // console.log(this.chatbotUrl); // 例如： http://172.16.253.124/chat-room/index.html?userId=&feedback=true&welcome=true&te_trigger=PL_Application_Start&dimension=PL&session_id=
        }
      });
    });
    // console.log('### end');
  }

  ngOnInit() {
    // this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.dangerousVideoUrl);
  }

  alterChatBotUrl() {
    const calProductType = this.userDataService.calProductType;
    const subPageName = this.userDataService.subPageName;
    const welcome = this.userDataService.welcome;
    if (calProductType) {
      this.reqTrigger.productType = calProductType;
    }
    if (subPageName) {
      this.reqTrigger.subPageName = subPageName;
    }
    console.log('### welcome=', welcome);    
    console.log('### this.reqTrigger=', this.reqTrigger);
    this.chatbotUrl$ = this.applyService.getTrigger(this.reqTrigger.productType, this.reqTrigger.flowType, this.reqTrigger.pageName, this.reqTrigger.subPageName).pipe(
      switchMap(data => {
      if (data.status === 0 && data.result) {
        this.chatbotUrl = data.result.chatbotUrl;
        this.chatbotUrl = this.chatbotUrl.replace('${welcome}', welcome);
        const calMain: CalMain = this.userDataService.calMain;
        const main: ApplyMain = this.userDataService.applyMain;
        if (main && main.CaseNo) {
          this.chatbotUrl = this.chatbotUrl.replace('${session_id}', main.CaseNo);
        } else if (calMain && calMain.EXP_NO) {
          this.chatbotUrl = this.chatbotUrl.replace('${session_id}', calMain.EXP_NO);
        } else {
          this.chatbotUrl = this.chatbotUrl.replace('${session_id}', '');
        }
        console.log('### this.chatbotUrl=', this.chatbotUrl);
        this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.chatbotUrl); 
      }
      return of(this.videoUrl);
    }));
  }

  showChatBot() {
    showChatBot();
  }

  showDialog(target) {
    showDialog(target); 
  }

  closeDialog() {
    closeDialog();
  }

  closeChatBot() {
    closeChatBot();
  }

  sessiontimeout() {
    this.applyService.sessiontimeout().pipe(
      catchError(error => {
        return of({status: 0, message: '', result: {}});
      })
    ).subscribe(data => {
      if (data.status === 0) {
      } else {
      }
    });
  }

  showContactUsDialog() {
    closeChatBot();
    showDialog('#dialogContactMethod');	
  }

  // videoUrlLink() {
  //   this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.chatbotUrl);
  //   return this.videoUrl;
  // }

  routerlink() {
    // this.router.navigate(["/SGN/client"]); Charles:連結無效
    this.routerHelperService.navigate(['/SGN/client']);
  }
}
