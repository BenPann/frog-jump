import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { closeChatBot, showContactUsDialog} from 'src/app/js/header.page.js';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.scss']
})
export class ChatbotComponent implements OnInit {

  constructor(
  ) { }

  ngOnInit(): void {
  }

  closeChatBot() {
    closeChatBot();
  }

  showContactUsDialog(){
    showContactUsDialog();
  }
}
