import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {

  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
  ) { }

  ngOnInit(): void {
  }


  onSubmit(from) {
    this.routerHelperService.navigate(['/']);
  }
}
