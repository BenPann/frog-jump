import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { initTextInputsBlur, initBlueButton } from 'src/app/js/start.page.js';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';

@Component({
  selector: 'app-cashcardold',
  templateUrl: './cashcardold.component.html',
  styleUrls: ['./cashcardold.component.scss']
})
export class CashcardoldComponent implements OnInit {

  ch1 = false;

  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
  ) { }

  ngOnInit(): void {
    // initTextInputsBlur();
    // initBlueButton();
  }


  onSubmit() {
    this.routerHelperService.navigate(['/SGN/verify2']);
  }

}
