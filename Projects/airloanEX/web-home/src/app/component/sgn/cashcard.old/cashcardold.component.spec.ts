import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashcardoldComponent } from './cashcardold.component';

describe('CashcardoldComponent', () => {
  let component: CashcardoldComponent;
  let fixture: ComponentFixture<CashcardoldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashcardoldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashcardoldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
