import { Component, OnInit } from '@angular/core';
import { initTextInputsBlur, initBlueButton } from 'src/app/js/start.page.js';
import { Router } from '@angular/router';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss']
})
export class ContractComponent implements OnInit {
  contractStatus = 'PL';
  ch1 = false;

  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
  ) { }

  ngOnInit(): void {
    // initTextInputsBlur();
    // initBlueButton();
  }

  onSubmit() {
    this.routerHelperService.navigate(['/SGN/verify2']);
  }
}
