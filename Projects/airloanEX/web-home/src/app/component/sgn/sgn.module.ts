import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SGNRoutingModule } from './sgn-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BackComponent } from './back/back.component';
import { SelectComponent } from './select/select.component';
import { AccountComponent } from './account/account.component';
import { VerifyComponent } from './verify/verify.component';
import { Verify2Component } from './verify2/verify2.component';
import { ContentComponent } from './content/content.component';
import { ContentrplComponent as contentRpl } from './content.rpl/contentrpl.component';
import { ClientComponent } from './client/client.component';
import { FillComponent } from './fill/fill.component';
import { StatementComponent } from './statement/statement.component';
import { StatementrplComponent as statementRpl } from './statement.rpl/statementrpl.component';
import { ContractComponent } from './contract/contract.component';
import { ContractrplComponent as contractRpl } from './contract.rpl/contractrpl.component';
import { CashcardoldComponent as cashcardOld } from './cashcard.old/cashcardold.component';
import { CompleteComponent } from './complete/complete.component';
import { CompletemoreComponent as completeMore } from './complete.more/completemore.component';
import { CompleterplComponent as completeRpl } from './complete.rpl/completerpl.component';
import { ContractgmComponent } from './contractgm/contractgm.component';
import { StatementgmComponent } from './statementgm/statementgm.component';
import { NgxCleaveDirectiveModule } from 'ngx-cleave-directive';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    BackComponent,
    SelectComponent,
    AccountComponent,
    VerifyComponent,
    Verify2Component,
    ContentComponent,
    ClientComponent,
    FillComponent,
    StatementComponent,
    ContractComponent,
    contractRpl,
    cashcardOld,
    CompleteComponent,
    completeRpl,
    completeMore,
    statementRpl,
    contentRpl,
    ContractgmComponent,
    StatementgmComponent,
  ],
  imports: [
    CommonModule,
    SGNRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCleaveDirectiveModule,
    SharedModule,
  ]
})
export class SGNModule { }
