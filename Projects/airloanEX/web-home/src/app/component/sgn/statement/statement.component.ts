import { Component, OnInit } from '@angular/core';
import { showDialog, initTextInputsBlur, initBlueButton, initCloseButtons } from 'src/app/js/start.page.js';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ModalFactoryService } from 'src/app/shared/service/common/modal-factory.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';

@Component({
  selector: 'app-statement',
  templateUrl: './statement.component.html',
  styleUrls: ['./statement.component.scss']
})
export class StatementComponent implements OnInit {
  private alive = true;

  statementStatus = 'PL';
  applyForm: FormGroup;
  dataOptions = [
    { key: '每月5日', value: '每月5日' },
    { key: '每月10日', value: '每月10日' },
    { key: '每月25日', value: '每月25日' },
    { key: '每月20日', value: '每月20日' },
    { key: '每月25日', value: '每月25日' },
  ];
  options = [
    { key: '2020/05/17', value: '2020/05/17' },
    { key: '2020/05/16', value: '2020/05/16' },
    { key: '2020/05/15', value: '2020/05/15' },
    { key: '2020/05/14', value: '2020/05/14' },
  ];

  terms = [];
  dialogTitle: string;
  dialogData: string;

  initForm() {
    this.applyForm = this.fb.group({
      date: ['每月5日'],
      ch1: ['', [Validators.required]],
      date2: ['2020/05/17', [Validators.required]],
      rd: ['', [Validators.required]],

    });
  }

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private applyService: ApplyApiService,
    private modalService: ModalFactoryService,
    private logger: ApplyLoggerService,
  ) { }

  ngOnInit(): void {
    this.initForm();

    this.initTerms();
  } // end ngOnInit

  ngAfterViewInit(): void {
    // initTextInputsBlur();
    // initBlueButton();
    initCloseButtons();
  }

  initTerms(): void {
    this.applyService.getTermList().subscribe(data => {
      if (data) {
        console.log(data);
        data.forEach(value => {
          this.terms[value.type] = { name: value.termName, title: value.termTitle };
        });
      }
    });
  }

  // 取得條款內容
  showTerms(target: string, termsName) {
    this.applyService
      .getTerms(termsName.name)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        try {
          // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
          const json = JSON.parse(data);
          this.dialogTitle = '查詢條款失敗';
          this.dialogData = '';
        } catch (error) {
          this.dialogTitle = termsName.title;
          this.dialogData = data;
          showDialog(target);
        }
      });
  }

  showDialog(target) {
    showDialog(target);
  }

  onSubmit() {
    this.routerHelperService.navigate(['/SGN/contract']);
  }
}
