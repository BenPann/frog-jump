import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { initTextInputsBlur, initBlueButton, initCloseButtons, showDialog } from 'src/app/js/start.page.js';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.routerHelperService.navigate(['/SGN/fill']);
  }

}
