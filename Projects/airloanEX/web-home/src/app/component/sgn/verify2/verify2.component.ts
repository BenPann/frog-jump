import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { initCleaveNumber } from 'src/app/js/start.page.js';
import { initRapidForm } from 'src/app/js/verify.page.js';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';

@Component({
  selector: 'app-verify2',
  templateUrl: './verify2.component.html',
  styleUrls: ['./verify2.component.scss']
})
export class Verify2Component implements OnInit {

  countTextHidden = false;
  resendTextHidden = true;

  applyForm: FormGroup;
  initForm() {
    this.applyForm = this.fb.group({
      tel1: [''],
      tel2: [''],
      tel3: [''],
      tel4: [''],
      tel5: [''],
      tel6: [''],

    });
  }
  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private routerHelperService: RouterHelperService,
    private elem: ElementRef,
  ) { }



  ngOnInit() {
    this.initForm();
    initCleaveNumber();
    initRapidForm(this.elem);


  }

  checkValue(name: string, value: string) {
    if (value) {
      if (!/[0-9]$/.test(value)) {
        this.applyForm.get(name).patchValue('');
      } else if (this.applyForm.valid) {
        this.onSubmit();
      }
    }
  }

  setHidden(booleanText: boolean) {
    // countText.hidden=false ; resendText.hidden=true
    // countText.hidden=true  ; resendText.hidden=false
    this.countTextHidden = !booleanText;
    this.resendTextHidden = booleanText;
  }

  errorForm() {
    document.forms[0].classList.add('error-form');
  }

  onSubmit(): void {
    this.routerHelperService.navigate(['/SGN/complete']);
  }
}
