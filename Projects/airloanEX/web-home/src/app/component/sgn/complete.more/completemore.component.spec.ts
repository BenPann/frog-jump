import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletemoreComponent } from './completemore.component';

describe('CompletemoreComponent', () => {
  let component: CompletemoreComponent;
  let fixture: ComponentFixture<CompletemoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletemoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletemoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
