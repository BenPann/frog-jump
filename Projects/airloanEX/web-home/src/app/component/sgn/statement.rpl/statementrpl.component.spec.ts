import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatementrplComponent } from './statementrpl.component';

describe('StatementrplComponent', () => {
  let component: StatementrplComponent;
  let fixture: ComponentFixture<StatementrplComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatementrplComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatementrplComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
