import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { initCleaveNumber } from 'src/app/js/start.page.js';
import { initRapidForm } from 'src/app/js/verify.page.js';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';

import { takeWhile, tap } from 'rxjs/operators';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';
import { interval } from 'rxjs';
import { OtpService } from '../../../service/otp/otp.service';
@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit, OnDestroy {
  private alive = true;
  private resetTimer = true;

  applyForm: FormGroup;

  // 計秒
  secText = 60;
  userPhoneNum = '';
  sendSuccess = false;

  initForm() {
    this.applyForm = this.fb.group({
      tel1: [''],
      tel2: [''],
      tel3: [''],
      tel4: [''],
      tel5: [''],
      tel6: [''],
    });
  }

  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private elem: ElementRef,
    private routerHelperService: RouterHelperService,
    private applyService: ApplyApiService,
    private userDataService: UserDataService,
    private logger: ApplyLoggerService,
    private otpService: OtpService,
  ) { }

  ngAfterViewInit(): void {
    setTimeout(() => this.sendOtp());
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  ngOnInit() {
    // 取前一頁面的電話號碼
    this.userPhoneNum = this.userDataService.applyMain.mobile_tel;

    this.initForm();
    initCleaveNumber();
    initRapidForm(this.elem);
  }

  checkValue(name: string, value: string) {
    if (value) {
      if (!/[0-9]$/.test(value)) {
        this.applyForm.get(name).patchValue('');
      } else if (this.applyForm.valid) {
        this.onSubmit();
      }
    }
  }

  errorForm() {
    document.forms[0].classList.add('error-form');
  }

  // 重領驗證碼
  sendSMS(): void {
    // 清除欄位號碼
    this.applyForm.reset();
    this.sendOtp();
  }

  sendOtp() {
    this.otpService
      .sendOtp(this.userPhoneNum)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          if (data.result.code === '0000') {
            // this.modalFactoryService.modalDefault({
            //   message: '已發送簡訊至您的手機號碼，請輸入驗證碼進行最後確認'
            // });
            this.sendSuccess = true;
            // 計時
            this.setTime(60);
          } else {
            // this.modalFactoryService.modalDefault({
            //   message: data.result.message
            // });
            this.secText = 0;
          }
        } else {
          // this.modalFactoryService.modalDefault({
          //   message: data.message
          // });
          this.secText = 0;
        }
      });
  }

  // 計時
  setTime(tempT: number): void {
    this.resetTimer = true;
    let total = tempT;
    this.secText = total;
    interval(1000)
      .pipe(
        takeWhile(() => total-- !== 0),
        takeWhile(() => this.resetTimer),
        takeWhile(() => this.alive),
        tap(data => (this.secText = total))
      )
      .subscribe();
  }

  onSubmit(): void {
    const form = this.applyForm;

    const req = form.get('tel1').value + form.get('tel2').value + form.get('tel3').value
              + form.get('tel4').value + form.get('tel5').value + form.get('tel6').value;

    this.otpService
      .checkOtp(this.userPhoneNum, req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          this.routerHelperService.navigate(['/SGN/content']);
        } else {
          this.logger.showMsg(data);
        }
      });
  }
}
