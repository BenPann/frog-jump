import { CompleteComponent } from './complete/complete.component';
import { CompletemoreComponent as completeMore } from './complete.more/completemore.component';
import { CompleterplComponent as completeRpl } from './complete.rpl/completerpl.component';
import { ContractComponent } from './contract/contract.component';
import { ContractrplComponent as contractRpl } from './contract.rpl/contractrpl.component';
import { ContractgmComponent as contractGm } from './contractgm/contractgm.component';
import { CashcardoldComponent as cashcardOld } from './cashcard.old/cashcardold.component';
import { StatementComponent } from './statement/statement.component';
import { StatementrplComponent as statementRpl } from './statement.rpl/statementrpl.component';
import { FillComponent } from './fill/fill.component';
import { ClientComponent } from './client/client.component';
import { ContentComponent } from './content/content.component';
import { ContentrplComponent as contentRpl } from './content.rpl/contentrpl.component';
import { VerifyComponent } from './verify/verify.component';
import { AccountComponent } from './account/account.component';
import { SelectComponent } from './select/select.component';
import { BackComponent } from './back/back.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Verify2Component } from './verify2/verify2.component';

import { TokenGuard } from 'src/app/guard/token.guard';
import { LeaveGuard } from 'src/app/guard/leave.guard';

const routes: Routes = [
  {
    path: 'back',
    component: BackComponent,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'select',
    component: SelectComponent,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'account',
    component: AccountComponent,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'verify',
    component: VerifyComponent,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'verify2',
    component: Verify2Component,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'content',
    component: ContentComponent,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'content.rpl',
    component: contentRpl,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'client',
    component: ClientComponent,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    // data: { browseLog: true }
  },
  {
    path: 'fill',
    component: FillComponent,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'statement',
    component: StatementComponent,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'statement.rpl',
    component: statementRpl,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'contract',
    component: ContractComponent,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'contract.rpl',
    component: contractRpl,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'contract.gm',
    component: contractGm,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'cashcard.old',
    component: cashcardOld,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'complete',
    component: CompleteComponent,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'complete.rpl',
    component: completeRpl,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },
  {
    path: 'complete.more',
    component: completeMore,
    // canActivate: [TokenGuard],
    canDeactivate: [LeaveGuard],
    data: { browseLog: true }
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SGNRoutingModule { }
