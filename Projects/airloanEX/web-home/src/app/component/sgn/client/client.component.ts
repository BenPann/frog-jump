import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { initTextInputsBlur, initBlueButton, initCloseButtons, showDialog, initCleaveNumber } from 'src/app/js/start.page.js';
import { dedicateMail, candidate } from 'src/app/js/person.page.js';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { ProductService } from 'src/app/service/product/product.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { ValidateService } from 'src/app/service/validate/validate.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { takeWhile } from 'rxjs/operators';
import { DropdownItem } from 'src/app/interface/resultType';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  private alive = true;

  email: string;
  applyForm: FormGroup;
  corpNameBL = true;
  departIdOptions: DropdownItem[] = [{ dataKey: '', dataName: '請選擇' }];
  productIdOptions: DropdownItem[] = [
    { dataKey: '', dataName: '請選擇產品' },
    { dataKey: '素還金', dataName: '素還金' },
    { dataKey: '振興金', dataName: '振興金' },
    { dataKey: '樂透金', dataName: '樂透金' },
  ];

  terms = [];
  dialogTitle: string;
  dialogData: string;
  
  initForm() {
    this.applyForm = this.fb.group({
      Name: ['', [Validators.required]],
      ProductId: ['', []],
      Phone: ['', [Validators.required, Validators.minLength(10), Validators.pattern(/^09\d{8}$/)]],
      EMail: ['', [Validators.email]],
      DepartId: ['', []],
      Member: [''],
      ContactTime: ['', []],
      Note: [''],
    });
  }

  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private elme: ElementRef,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private productService: ProductService,
    private applyService: ApplyApiService,
    private commonDataService: CommonDataService,
    private validateService: ValidateService,
    private logger: ApplyLoggerService
  ) { }

  ngOnInit(): void {
    this.initForm();

    this.selectList();

    this.initTerms();
  } // end ngOnInit

  ngAfterViewInit(): void {
    // initTextInputsBlur();
    // initBlueButton();
    initCleaveNumber();
    initCloseButtons();
    candidate();
  }

  initTerms(): void {
    this.applyService.getTermList().subscribe(data => {
      if (data) {
        // console.log(data);
        data.forEach(value => {
          this.terms[value.type] = { name: value.termName, title: value.termTitle };
        });
      }
    });
  }

  // 取得條款內容
  showTerms(target: string, termsName) {
    this.applyService
      .getTerms(termsName.name)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        try {
          // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
          const json = JSON.parse(data);
          this.dialogTitle = '查詢條款失敗';
          this.dialogData = '';
        } catch (error) {
          this.dialogTitle = termsName.title;
          this.dialogData = data;
          showDialog(target);
        }
      });
  }

  selectList() {
    this.commonDataService.getQR_ChannelDepartType()
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        data.forEach(value => {
          this.departIdOptions.push(value);
        });
      });
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  showDialog(target) {
    showDialog(target);
  }

  dedicateMail($event) {
    if (this.corpNameBL) {
      dedicateMail($event, this.elme);
    } else {
      this.corpNameBL = true;
    }
  }

  onSubmit() {
    const req = this.applyForm.getRawValue();
    this.applyService.applyPLCUs(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        console.log(data);
        if (data.status === 0) {
          this.showDialog('#dialog2');
        } else {
          this.logger.showMsg(data);
        }
      });
  }
}
