import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { initTextInputsBlur, initBlueButton, initCloseButtons, showDialog, initCleaveNumber } from 'src/app/js/start.page.js';
import { dedicateCompany } from 'src/app/js/work.page.js';
import { candidate } from 'src/app/js/person.page.js';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { takeWhile } from 'rxjs/operators';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';

@Component({
  selector: 'app-fill',
  templateUrl: './fill.component.html',
  styleUrls: ['./fill.component.scss']
})
export class FillComponent implements OnInit {
  private alive = true;

  work: string;
  applyForm: FormGroup;
  corpNameBL = true;

  bankOption = [
    { key: '', value: '請選擇' },
    { key: '臺灣銀行', value: '臺灣銀行' },
    { key: '土地銀行', value: '土地銀行' },
    { key: '合庫商銀', value: '合庫商銀' },
    { key: '第一銀行', value: '第一銀行' },
    { key: '華南銀行', value: '華南銀行' },
    { key: '彰化銀行', value: '彰化銀行' },
    { key: '上海銀行', value: '上海銀行' },
    { key: '台北富邦', value: '台北富邦' },
    { key: '國泰世華', value: '國泰世華' },
    { key: '高雄銀行', value: '高雄銀行' },
    { key: '兆豐商銀', value: '兆豐商銀' },
    { key: '花旗銀行', value: '花旗銀行' },
    { key: '臺灣企銀', value: '臺灣企銀' },
    { key: '匯豐銀行', value: '匯豐銀行' },
    { key: '渣打銀行', value: '渣打銀行' },
    { key: '標旗銀行', value: '標旗銀行' },
    { key: '華泰銀行', value: '華泰銀行' },
    { key: '陽信銀行', value: '陽信銀行' },
    { key: '中華郵政', value: '中華郵政' },
    { key: '聯邦銀行', value: '聯邦銀行' },
    { key: '遠東銀行', value: '遠東銀行' },
    { key: '玉山銀行', value: '玉山銀行' },
    { key: '台新銀行', value: '台新銀行' },
    { key: '日盛銀行', value: '日盛銀行' },
    { key: '安泰銀行', value: '安泰銀行' },
    { key: '中國信託', value: '中國信託' },
  ];
  dateOption = [
    { key: '', value: '請選擇' },
    { kry: '每月5日', value: '每月5日' },
    { kry: '每月10日', value: '每月10日' },
    { kry: '每月15日', value: '每月15日' },
    { kry: '每月20日', value: '每月20日' },
    { kry: '每月25日', value: '每月25日' },
  ];


  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private elme: ElementRef,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private applyService: ApplyApiService,
    private commonDataService: CommonDataService,
    private logger: ApplyLoggerService
  ) { }

  ngOnInit(): void {
    this.initForm();

    const main = this.userDataService.applyMain;
    this.applyForm.patchValue(main);
  }

  ngAfterViewInit(): void {
    // initTextInputsBlur();
    // initBlueButton();
    initCloseButtons();
    initCleaveNumber();
    candidate();
  }

  initForm() {
    this.applyForm = this.fb.group({
      grantBank: ['', [Validators.required]],
      appropriationBranch: ['', [Validators.required]],
      grantAccount: ['', [Validators.required, Validators.minLength(16)]],
      bankPhone: ['', [Validators.required, Validators.minLength(10), Validators.pattern(/^09\d{8}$/)]],
      paymentDate: ['', [Validators.required]],
      payingBank: ['', [Validators.required]],
      paymentAccount: ['', [Validators.required, Validators.minLength(16)]],
    });
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  showDialog(target) {
    showDialog(target);
  }

  dedicateCompany(myvalue) {
    if (this.corpNameBL) {
      dedicateCompany(myvalue, this.elme);
    } else {
      this.corpNameBL = true;
    }
  }

  /***************
  繳款銀行同撥款帳戶
  ***************/
  bankAutoFill(th) {
    if (th.checked === true) {
      this.applyForm.patchValue({
        payingBank: this.applyForm.get('grantBank').value,
        paymentAccount: this.applyForm.get('grantAccount').value
      });
    }
  }

  onSubmit() {
    const req = {
      grantBank: this.applyForm.get('grantBank').value,
      appropriationBranch: this.applyForm.get('appropriationBranch').value,
      grantAccount: this.applyForm.get('grantAccount').value,
      bankPhone: this.applyForm.get('bankPhone').value,
      paymentDate: this.applyForm.get('paymentDate').value,
      payingBank: this.applyForm.get('payingBank').value,
      paymentAccount: this.applyForm.get('paymentAccount').value,
    };

    this.applyService.applyInfo(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          // @save to sessionStorage
          const main = this.userDataService.applyMain;
          Object.assign(main, req);
          // this.userDataService.doLoanData(main, req);
          this.userDataService.applyMain = main;
          this.routerHelperService.navigate(['/SGN/statement']);
        } else {
          this.logger.showMsg(data);
        }
      });
  }

}
