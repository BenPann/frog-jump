import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatementgmComponent } from './statementgm.component';

describe('StatementgmComponent', () => {
  let component: StatementgmComponent;
  let fixture: ComponentFixture<StatementgmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatementgmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatementgmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
