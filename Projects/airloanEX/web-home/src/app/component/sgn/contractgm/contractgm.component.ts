import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';

@Component({
  selector: 'app-contractgm',
  templateUrl: './contractgm.component.html',
  styleUrls: ['./contractgm.component.scss']
})
export class ContractgmComponent implements OnInit {
  contractStatus = 'GM';
  ch1 = false;

  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
  ) { }

  ngOnInit(): void {
    // initTextInputsBlur();
    // initBlueButton();
  }

  onSubmit() {
    this.routerHelperService.navigate(['/SGN/verify2']);
  }
}
