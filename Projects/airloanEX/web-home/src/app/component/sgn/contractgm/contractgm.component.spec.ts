import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractgmComponent } from './contractgm.component';

describe('ContractgmComponent', () => {
  let component: ContractgmComponent;
  let fixture: ComponentFixture<ContractgmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContractgmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractgmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
