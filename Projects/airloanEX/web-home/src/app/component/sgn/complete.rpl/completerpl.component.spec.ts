import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleterplComponent } from './completerpl.component';

describe('CompleterplComponent', () => {
  let component: CompleterplComponent;
  let fixture: ComponentFixture<CompleterplComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompleterplComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleterplComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
