import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentrplComponent } from './contentrpl.component';

describe('ContentrplComponent', () => {
  let component: ContentrplComponent;
  let fixture: ComponentFixture<ContentrplComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentrplComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentrplComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
