import { Component, ElementRef, OnInit } from '@angular/core';
import { initTextInputsBlur, initBlueButton, initCloseButtons, showDialog } from 'src/app/js/start.page.js';
import { Router } from '@angular/router';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
@Component({
  selector: 'app-contentrpl',
  templateUrl: './contentrpl.component.html',
  styleUrls: ['./contentrpl.component.scss']
})
export class ContentrplComponent implements OnInit {


  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
    private elem: ElementRef,
  ) { }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    initTextInputsBlur();
    initBlueButton();
    initCloseButtons();
  }

  showDialog(target) {
    showDialog(target);
  }

  setInfoText(name: string) {
    let iHTML = '';
    if (name === 'ra1') {
      iHTML = '200 元/次<br />最高收 3,000 元/年 ';
    } else {
      iHTML = '3,000元/年';
    }
    document.getElementById('infoText').innerHTML = iHTML;
  }

  onSubmit() {
    this.routerHelperService.navigate(['/SGN/statement.rpl']);
  }


}
