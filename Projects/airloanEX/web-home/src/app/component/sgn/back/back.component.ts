import { CleaveService } from 'src/app/service/cleave/cleave.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';
import { initTextInputsBlur, initBlueButton, initCloseButtons, showDialog, initCleaveUpper, initCleaveDate } from 'src/app/js/start.page.js';
import { idValidate } from 'src/app/js/step4.page.js';
import { ApplyApiService } from 'src/app/service/apply/apply-api.service';
import { UserDataService } from 'src/app/service/user-data/user-data.service';
import { CommonDataService } from 'src/app/shared/service/common/common-data.service';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';
import { ApplyLoggerService } from 'src/app/service/apply-logger/apply-logger.service';

@Component({
  selector: 'app-back',
  templateUrl: './back.component.html',
  styleUrls: ['./back.component.scss']
})
export class BackComponent implements OnInit {
  private alive = true;
  ch1 = false;
  applyForm: FormGroup;

  terms = [];
  dialogTitle: string;
  dialogData: string;
  initForm() {
    this.applyForm = this.fb.group({
      idno: ['', [Validators.required, Validators.maxLength(10), idValidate]],
      birthday: ['', [Validators.required, Validators.maxLength(10), Validators.pattern(/^\d{4}\/\d{2}\/\d{2}$/)]],
    });
  }
  constructor(
    private fb: FormBuilder,
    public cleave: CleaveService,
    private router: Router,
    private routerHelperService: RouterHelperService,
    private userDataService: UserDataService,
    private applyService: ApplyApiService,
    private commonDataService: CommonDataService,
    private logger: ApplyLoggerService
  ) { }

  ngOnInit(): void {
    this.initForm();

    this.showDialog('#dialogTips');
  
    this.initTerms();
  } // end ngOnInit

  ngAfterViewInit(): void {
    initTextInputsBlur();
    initBlueButton();
    initCloseButtons();
    initCleaveUpper();
    initCleaveDate();
  }

  initTerms(): void {
    this.applyService.getTermList().subscribe(data => {
      if (data) {
        data.forEach(value => {
          this.terms[value.type] = { name: value.termName, title: value.termTitle };
        });
      }
    });
  }

  // 取得條款內容
  showTerms(target: string, termsName) {
    this.applyService
      .getTerms(termsName.name)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        try {
          // 此段為檢查回傳的是否為JSON物件 JSON就是失敗
          const json = JSON.parse(data);
          this.dialogTitle = '查詢條款失敗';
          this.dialogData = '';
        } catch (error) {
          this.dialogTitle = termsName.title;
          this.dialogData = data;
          showDialog(target);
        }
      });
  }

  errorMessage(id: string, errorName?: string) {
    if (errorName) {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched && this.applyForm.controls[id].hasError(errorName);
    } else {
      return this.applyForm.controls[id].invalid && this.applyForm.controls[id].touched;
    }
  }

  showDialog(target) {
    showDialog(target);
  }

  idValidate(value) {
    this.applyForm.patchValue({ id: value });
  }

  onSubmit() {
    const req = {
      idno: this.applyForm.get('idno').value,
      birthday: this.applyForm.get('birthday').value,
    };

    this.applyService.applyInfo(req)
      .pipe(takeWhile(() => this.alive))
      .subscribe(data => {
        if (data.status === 0) {
          // @save to sessionStorage
          const main = this.userDataService.applyMain;
          Object.assign(main, req);
          // this.userDataService.doLoanData(main, req);
          this.userDataService.applyMain = main;
          this.routerHelperService.navigate(['/SGN/select']);
        } else {
          this.logger.showMsg(data);
        }
      });
    // this.router.navigate(['/SGN/select']);
  }

}
