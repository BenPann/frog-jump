import { Component, OnInit } from '@angular/core';
import { initTextInputsBlur, initBlueButton } from 'src/app/js/start.page.js';
import { Router } from '@angular/router';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';

@Component({
  selector: 'app-contractrpl',
  templateUrl: './contractrpl.component.html',
  styleUrls: ['./contractrpl.component.scss']
})
export class ContractrplComponent implements OnInit {
  contractStatus = 'RPL';
  ch1 = false;

  constructor(
    private router: Router,
    private routerHelperService: RouterHelperService,
  ) { }

  ngOnInit(): void {
    // initTextInputsBlur();
    // initBlueButton();
  }

  onSubmit() {
    this.routerHelperService.navigate(['/SGN/verify2']);
  }
}
