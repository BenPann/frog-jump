import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractrplComponent } from './contractrpl.component';

describe('ContractrplComponent', () => {
  let component: ContractrplComponent;
  let fixture: ComponentFixture<ContractrplComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractrplComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractrplComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
