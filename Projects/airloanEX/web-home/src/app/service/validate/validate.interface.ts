export interface NCCCReq {
  pan: string;
  expDate: string;
  extNo: string;
}

export interface NCCCReqPlus {
  nCCC: NCCCReq;
  phone: number;
}

export interface NCCCRes {
  ncccCheckCode: string;
  ncccMessage: string;
  errorCount: number;
  ncccErrorCount: number;
  pcode2566ErrorCount: number;
}

export interface PCODE2566Req {
  phone: string;
  bank: string;
  account: string;
  branchId: string;
}

export interface PCODE2566Res {
  pcode2566CheckCode: string;
  pcode2566Message: string;
  errorCount: number;
  ncccErrorCount: number;
  pcode2566ErrorCount: number;
}

export interface EddaReq {
  eddaBankId: string;
  eddaAccountNo: string;
}
export interface VerifyCountResult {
  errorCount: number;
  ncccErrorCount: number;
  pcode2566ErrorCount: number;
}

export interface MaxVerifyCount {
  ncccTotalCount: number;
  pcode2566TotalCount: number;
}

// airloanEX - replace ed3 PCODE2566Res
export class PCode2566Resp {
  pcode2566CheckCode: string;
  pcode2566Message: string;
  authErrCount: number;
  signErrCount: number;
  eddaErrCount: number;
}

// airloanEX - replace ed3 VerifyCountResult
export interface VerifyCountResp {
  authErrCount: number;
  signErrCount: number;
  eddaErrCount: number;
}
