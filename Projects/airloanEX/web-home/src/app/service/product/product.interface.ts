export interface SecuritiesBranch {
  securitiesCity: string;
  securitiesDist: string;
  securitiesAddrZipCode: string;
  securitiesCode: string;
  securitiesName: string;
}

export interface ChooseProductReq {
  orders: string[];
  creditType: string;
  twTaxpayer: string;
  agreement: string;
}

export interface BookBranchInfo {
  bookingBranchId: string;
  bookingDate: string;
  bookingTime: string;
}

export interface ChooseAccountReq {
  accountType: string;
  deliveryType: string;
  agreement: string;
  securitiesAddr: string;
  securitiesAddrZipCode: string;
  securitiesCode: string;
  securitiesName: string;
}

// tslint:disable-next-line: no-use-before-declare
export interface AppointmentInfo extends BranchBankInfo {
  phone: string;
  bookBranchInfo: BookBranchInfo;
}

export interface RecommendInfo {
  productName: string;
  refereeClass: string;
  orgID: string;
  empID: string;
}

export interface SecuritiesInfo {
  securitiesCity: string;
  securitiesDist: string;
  securitiesAddrZipCode: string;
  securitiesCode: string;
  securitiesName: string;
}

export interface BranchBankInfo {
  branchAddrZipCode: string;
  branchAddrCity: string;
  branchAddrDist: string;
  branchAddr: string;
  branchName: string;
  branchID: string;
}

export interface BankInfo {
  bankCity: string;
  bankDist: string;
  bankAddrZipCode: string;
  bankAddr: string;
  bankName: string;
  branchCode: string;
}
