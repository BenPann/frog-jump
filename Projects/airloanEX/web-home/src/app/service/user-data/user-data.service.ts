import { Injectable } from '@angular/core';
import { ApplyMain, CalMain } from 'src/app/component/apply/apply.interface';
import { NCCCReqPlus, PCODE2566Req, VerifyCountResp, MaxVerifyCount } from 'src/app/service/validate/validate.interface';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  public calProductType = '';
  public subPageName = '';

  public get calToken(): string {
    return sessionStorage.getItem('calToken');
  }

  public set calToken(token: string) {
    sessionStorage.setItem('calToken', token);
  }

  public get calMain(): CalMain {
    return this.getJSON('calMain');
  }

  public set calMain(data: CalMain) {
    sessionStorage.setItem('calMain', JSON.stringify(data));
  }

  public welcomeReset() {
    sessionStorage.removeItem("welcome");
  }

  public get welcome(): string {
    let welcome = sessionStorage.getItem("welcome");
        welcome = (welcome === null ? 'true' : 'false');
    this.welcome = welcome;
    return welcome;
  }

  public set welcome(data: string) {
    sessionStorage.setItem('welcome', data);
  }

  public get token(): string {
    return sessionStorage.getItem('kgiToken');
  }

  public set token(token: string) {
    sessionStorage.setItem('kgiToken', token);
  }

  public get applyMain(): ApplyMain {
    return this.getJSON('applyMain');
  }

  public set applyMain(data: ApplyMain) {
    sessionStorage.setItem('applyMain', JSON.stringify(data));
  }

  public get breakPoint(): string {
    return sessionStorage.getItem('breakPoint');
  }

  public set breakPoint(breakPoint: string) {
    sessionStorage.setItem('breakPoint', breakPoint);
  }

  public doResume(main: ApplyMain, data: any): void {
    main.UserType = data.UserType;
    main.idno = data.Idno;
    main.birthday = data.birthday;
    main.mobile_tel = data.mobile_tel;
    main.agentNo = data.agentNo;

    main.ProductId = data.ProductId;
    main.p_apy_amount = data.p_apy_amount;
    main.p_period = data.p_period;
    main.p_purpose = data.p_purpose;
    // 個人資料
    main.customer_name = data.customer_name;
    main.email_address = data.email_address;

    const house_address = data.house_address;
    if (house_address && house_address.indexOf(';') > 0) {
      main.counties = house_address.substring(0, 3); // 縣市
      const index = house_address.indexOf(';');
      main.township = house_address.substring(3, index); // 區
      main.address = house_address.substring(index !== -1 ? index + 1 : 6);
    } else {
      main.counties = '';
      main.township = '';
      main.address = house_address;
    }

    main.house_city_code = data.house_city_code;
    main.house_address = house_address;
    main.homePhone = data.house_tel_area + data.house_tel;
    main.house_tel_area = data.house_tel_area;
    main.house_tel = data.house_tel;
    main.domicilePhone = data.ResTelArea + data.ResTel;
    main.ResTelArea = data.ResTelArea;
    main.ResTel = data.ResTel;
    // 工作資料
    main.corp_name = data.corp_name;
    if (data.Occupation) {
      main.Occupation = data.Occupation.trim();
    }
    main.title = data.title;
    if (data.on_board_date) {
      main.year = data.on_board_date.substring(0, 4);
      main.month = data.on_board_date.substring(4, 6);
    }
    main.on_board_date = data.on_board_date;

    const corp_addres = data.corp_address;
    if (corp_addres && corp_addres.indexOf(';') > 0) {
      main.companyCounties = corp_addres.substring(0, 3);
      const index = corp_addres.indexOf(';');
      main.companyTownship = corp_addres.substring(3, index);
      main.companyAddress = corp_addres.substring(index !== -1 ? index + 1 : 6);
    } else {
      main.companyCounties = '';
      main.companyTownship = '';
      main.companyAddress = corp_addres;
    }

    main.corp_city_code = data.corp_city_code;
    main.corp_address = corp_addres;
    main.companyPhone = data.corp_tel_area + data.corp_tel;
    main.corp_tel_area = data.corp_tel_area;
    main.corp_tel = data.corp_tel;
    main.corp_tel_exten = data.corp_tel_exten;
    // 其他資料
    main.education = data.education;
    main.yearly_income = data.yearly_income;
    main.marriage = data.marriage;
    main.estate_type = data.estate_type;

    // 用他行帳號驗證您的身分 未比對DB名稱
    // 銀行別
    main.bank = data.bank;
    // 銀行帳號
    main.bankAccount = data.bankAccount;
    // 手機號碼
    main.phone = data.phone;
    // 請登入查看核貸內容
    // main.idno = data.idno;
    // main.birthday = data.birthday;
    // 填寫撥款及繳款資訊
    main.grantBank = data.grantBank;
    main.appropriationBranch = data.appropriationBranch;
    main.grantAccount = data.grantAccount;
    main.bankPhone = data.bankPhone;
    main.paymentDate = data.paymentDate;
    main.payingBank = data.payingBank;
    main.paymentAccount = data.paymentAccount;
  } // end doResume
  /**
   * 開始申請頁
   */
  public doStartData(main: ApplyMain, startData: any): void {
    if (startData) {
      main.ProductId = startData.ProductId;
      main.idno = startData.idno;
      main.birthday = startData.birthday;
      main.phone = startData.phone;
      main.agentNo = startData.agentNo;
    }
  } // end doStartData

  /**
   * 儲存，開始填寫申請書頁
   */
  public doLoanData(main: ApplyMain, loanData: any): void {
    if (loanData) {
      main.p_apy_amount = loanData.p_apy_amount;
      main.p_period = loanData.p_period;
      main.p_purpose = loanData.p_purpose;
    }
  } // end doLoanData

  /**
   * 儲存，個人資料頁
   */
  public doPersonData(main: ApplyMain, personData: any): void {
    if (personData) {
      main.customer_name = personData.customer_name;
      main.email_address = personData.email_address;
      main.house_city_code = personData.house_city_code;
      main.house_address = personData.house_address;
      main.house_tel_area = personData.house_tel_area;
      main.house_tel = personData.house_tel;
      main.ResTelArea = personData.ResTelArea;
      main.ResTel = personData.ResTel;
    }
  }

  // public doWorkData(main: ApplyMain, workData: any): void {
  //   if (workData) {
  //     main = workData;
  // main.corp_name = workData.corp_name;
  // main.Occupation = workData.Occupation;
  // main.title = workData.title;
  // main.on_board_date = workData.on_board_date;
  // main.corp_city_code = workData.corp_city_code;
  // main.corp_address = workData.corp_address;
  // main.corp_tel_area = workData.corp_tel_area;
  // main.corp_tel = workData.corp_tel;
  // main.corp_tel_exten = workData.corp_tel_exten;
  //   }
  // }

  public get maxVerifyCount(): MaxVerifyCount {
    return this.getJSON('verifyCount');
  }

  public set maxVerifyCount(req: MaxVerifyCount) {
    sessionStorage.setItem(
      'verifyCount',
      JSON.stringify(req)
    );
  }

  constructor() {
  }

  /**
   * 取JSON
   * return any
   */
  private getJSON(key: string): any {
    return sessionStorage.getItem(key)
      ? JSON.parse(sessionStorage.getItem(key))
      : undefined;
  }
}
