import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApplyInfoRequest, ApplyNewCreditcardRequest, ApplyRelationDegreeRequest, ApplyStartRequest, ApplyUploadIdcard, LogonRequest } from 'src/app/component/apply/apply.interface';
import { TermsInput, WebResult } from 'src/app/interface/resultType';
import { RouterHelperService } from 'src/app/shared/service/common/router-helper.service';

@Injectable({
  providedIn: 'root'
})
export class ApplyApiService {

  constructor(
    private httpClient: HttpClient,
    private routerHelperService: RouterHelperService
  ) { }

  /** 新戶 */
  isNewOne(userType: string) {
    return userType === '0';
  }

  /** 既有戶 */
  isOldOne(userType: string) {
    return userType !== '0';
  }

  /** 1 PA 預核名單preAudit(快速申辦不查聯徵) */
  isPA(productId: string) {
    return productId === '1';
  }

  /** 2 PL */
  isPL(productId: string) {
    return productId === '2';
  }

  /** 3 RPL */
  isRPL(productId: string) {
    return productId === '3';
  }

  /** 4 GM(CASHCARD) */
  isGM(productId: string) {
    return productId === '4';
  }

  /** PA/PL/RPL/GM */
  makeProductType(productId: string): string{
    let productType;
    if (productId && this.isPA(productId)) {
      productType = 'PA';
    } else if (productId && this.isPL(productId)) {
      productType = 'PL';
    } else if (productId && this.isRPL(productId)) {
      productType = 'RPL';
    } else if (productId && this.isGM(productId)) {
      productType = 'GM';
    } else {
      productType = '';
    }
    return productType;
  }

  logon(req: LogonRequest): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/logon', req);
  }

  applyStart(req: ApplyStartRequest): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyStart', req);
  }

  applyVerify(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyVerify', {});
  }

  applyPreAudit(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyPreAudit', {});
  }

  applyLoan(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyLoan', {});
  }

  applyInfo(req: ApplyInfoRequest): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyInfo', req);
  }

  applyConfirm(req: ApplyInfoRequest): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyConfirm', req);
  }

  applyPrepareConfirm(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyPrepareConfirm', {});
  }

  applyConfirmToOtp2(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyConfirmToOtp2', {});
  }

  applyIdentified(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyIdentified', {});
  }

  mustUploadIdcard(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/mustUploadIdcard', {});
  }

  mustUploadFinpf(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/mustUploadFinpf', {});
  }

  applyUploadIdcard(req: ApplyUploadIdcard): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyUploadIdcard', req);
  }

  applyUploadFinpf(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyUploadFinpf', {});
  }

  applyRelationDegree(req: ApplyRelationDegreeRequest): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyRelationDegree', req);
  }

  applyPaper(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyPaper', {});
  }

  applyLaterAddon(phone: string): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyLaterAddon', {phone: phone});
  }

  applyCalResult(phone: string, CASE_NO: string): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyCalResult', {phone: phone, CASE_NO: CASE_NO});
  }

  applyStepok(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyStepok', {});
  }

  applyNewCreditcard(req: ApplyNewCreditcardRequest): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyNewCreditcard', req);
  }

  applyQryQa(req): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyQryQa', req);
  }

  applyGetTestRplScore(req): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applyGetTestRplScore', req);
  }

  applySendAws(req): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/stp/applySendAws', req);
  }

  // previewApplyLoan(): Observable<WebResult> {
  //   return this.httpClient.post<WebResult>('../api/contract/previewApplyLoan', {});
  // }

  previewApplyCreditCard(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/contract/previewApplyCreditCard', {});
  }

  uploadApplyLoan(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/contract/uploadApplyLoan', {});
  }

  applyPLCUs(req): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../publicApi/stp/applyPLCUs', req);
  }

  // /**
  //  * We send JSON and We accept plain text as response.
  //  * INPUT: json
  //  * OUTPUT: string
  //  */
  // getTrigger(productType: string, flowType: string, pageName: string): Observable<any> {
  //   return this.httpClient.post<any>('../publicApi/chatbot/trigger', {productType, flowType, pageName}, 
  //   {
  //     headers: new HttpHeaders({
  //       'Accept': 'text/plain, */*',
  //       'Content-Type': 'application/json' // We send JSON
  //     }),
  //     responseType: 'text' as 'json' // We accept plain text as response.
  //     });
  // }

  getTrigger(productType: string, flowType: string, pageName: string, subPageName?: string): Observable<WebResult> {
    return this.httpClient.post<any>('../publicApi/chatbot/trigger', {productType, flowType, pageName, subPageName});
  }

  getTermList(url?: string): Observable<TermsInput[]> {
    const pageName = url ? url : this.routerHelperService.getRouterAddress();
    return this.httpClient.post<TermsInput[]>(
      '../publicApi/terms/getTermsList',
      {
        url: 'loan-' + pageName // FIXME: Charles: fix to ed3-init for STP. Now it is depend on FakeBackendInterceptor.ts
      }
    );
  }

  getTerms(termsName: string): Observable<any> {
    const headers = new HttpHeaders().set(
      'Content-Type',
      'text/plain; charset=utf-8'
    );
    return this.httpClient.get('../publicApi/terms/' + termsName, {
      headers,
      responseType: 'text'
    });
  }

  getFullName(req): Observable<any> {
    return this.httpClient.post<WebResult>('../api/company/full-name', req);
  }

  getinfo(req): Observable<any> {
    return this.httpClient.post<WebResult>('../api/company/info', req);
  }

  applyGetCSMemoData(uniqId: string): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../publicApi/stp/applyGetCSMemoData', { uniqId: uniqId });
  }

  applyGetThemePath(channelId: string): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../publicApi/stp/applyGetThemePath', { channelId: channelId });
  }

  sessiontimeout(): Observable<WebResult> {
    return this.httpClient.post<WebResult>('../api/sms/sessiontimeout', {});
  }
}
