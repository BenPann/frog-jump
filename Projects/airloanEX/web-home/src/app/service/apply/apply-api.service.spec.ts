import { TestBed } from '@angular/core/testing';

import { ApplyApiService } from './apply-api.service';

describe('ApplyApiService', () => {
  let service: ApplyApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApplyApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
