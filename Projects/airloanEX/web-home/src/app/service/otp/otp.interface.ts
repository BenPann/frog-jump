export interface OtpData {
  code: string;
  message: string;
  sk: string;
  txnID: string;
  txnDate: string;
}
