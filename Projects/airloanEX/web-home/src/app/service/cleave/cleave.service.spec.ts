import { TestBed } from '@angular/core/testing';

import { CleaveService } from './cleave.service';

describe('CleaveService', () => {
  let service: CleaveService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CleaveService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
