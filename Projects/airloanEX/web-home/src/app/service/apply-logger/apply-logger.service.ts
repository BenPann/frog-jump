import { Injectable, OnInit } from '@angular/core';
import { WebResult } from 'src/app/interface/resultType';
import { initAllCloseButtons, initCloseButtons, showDialog } from 'src/app/js/start.page.js';

@Injectable({
  providedIn: 'root'
})
export class ApplyLoggerService {
  dialogTitle: number;
  dialogContent: string;
  isCSRT: boolean;
  constructor() {   }

  showMsg(data: WebResult) {
    initCloseButtons();
    initAllCloseButtons();
    this.dialogTitle = data.status;
    this.dialogContent = data.message;
    showDialog('#dialog');
  }

  showMsgForRT(data: WebResult, redirectUrl: string) {
    initCloseButtons();
    initAllCloseButtons();
    this.dialogTitle = data.status;
    this.dialogContent = data.message;
    sessionStorage.setItem("rtRedirectUrl", redirectUrl);
    showDialog('#dialog');
  }
}
