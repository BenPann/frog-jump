import { TestBed } from '@angular/core/testing';

import { ApplyLoggerService } from './apply-logger.service';

describe('ApplyLoggerService', () => {
  let service: ApplyLoggerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApplyLoggerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
