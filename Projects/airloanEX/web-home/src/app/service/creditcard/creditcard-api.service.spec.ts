import { TestBed } from '@angular/core/testing';

import { CreditcardApiService } from './creditcard-api.service';

describe('CreditcardApiService', () => {
  let service: CreditcardApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreditcardApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
