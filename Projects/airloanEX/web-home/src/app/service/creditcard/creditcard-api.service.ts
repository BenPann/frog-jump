import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WebResult } from 'src/app/interface/resultType';
import { CreditCardProductRequest } from 'src/app/service/creditcard/creditcard.interface';


@Injectable({
  providedIn: 'root'
})
export class CreditcardApiService {

  constructor(private httpClient: HttpClient) { }

  firstGiftList(): Observable<WebResult> {
    return this.httpClient.get<WebResult>('../api/creditcard/firstGiftList');
  }
}
