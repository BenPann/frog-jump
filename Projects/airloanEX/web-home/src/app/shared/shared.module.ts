import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ShowMessageComponent } from './component/show-message/show-message.component';
import { SpinnerOverlayComponent } from './component/spinner-overlay/spinner-overlay.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HiddenStrPipe } from './pipe/hidden-str.pipe';
import { CommonDataService } from './service/common/common-data.service';
import { ModalFactoryService } from './service/common/modal-factory.service';
import { RouterHelperService } from './service/common/router-helper.service';
import { SpinnerOverlayService } from './service/common/spinner-overlay.service';
import { RouterModule } from '@angular/router';
import { ShowImageComponent } from './component/show-image/show-image.component';
import { ShowHtmlComponent } from './component/show-html/show-html.component';
import { SafePipe } from './pipe/safe.pipe';

const entryComp = [
  ShowMessageComponent,
  SpinnerOverlayComponent,
  ShowImageComponent,
  ShowHtmlComponent
];

const exportComp = [

];

const exportPipe = [
  HiddenStrPipe,
  SafePipe,
];

const service = [
  CommonDataService,
  ModalFactoryService,
  RouterHelperService,
  SpinnerOverlayService
];

@NgModule({
  declarations: [
    ...entryComp,
    ...exportComp,
    ...exportPipe,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule,
  ],
  exports: [
    ...entryComp,
    ...exportComp,
    ...exportPipe,
    NgbModule
  ],
  providers: [
  ]
})
export class SharedModule {}
