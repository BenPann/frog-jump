import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebResult } from 'src/app/interface/resultType';
import { filter, map } from 'rxjs/operators';

/**
 *共用資料的Service
 */
@Injectable({
  providedIn: 'root'
})
export class CommonDataService {
  constructor(private httpClient: HttpClient) {
  }

  /**
   * 取得下拉選單的資料
   * @param dropName 下拉選單的名字
   */
  public getDropdownData(dropName: string) {
    return this.httpClient
      .post<WebResult>('../api/data/getDropDown', { name: dropName })
      .pipe(
        filter(r => r.status === 0),
        map(r => r.result)
      );
  }

  public getPcode2566BankList() {
    return this.httpClient.post<WebResult>('../api/data/pcode2566BankList', {}).pipe(filter(r => r.status === 0), map(r => r.result));
  }

  public getEddaBankList() {
    return this.httpClient.post<WebResult>('../api/data/eddaBankList', {}).pipe(filter(r => r.status === 0), map(r => r.result));
  }
  /** 貸款PL資金用途清單 */
  public getLoanPurposeOptions() {
    return this.httpClient.post<WebResult>('../api/data/moneypurpose', {}).pipe(filter(r => r.status === 0), map(r => r.result));
  }
  /** 職業類別清單 */
  public getOccupation() {
    return this.httpClient.post<WebResult>('../api/data/occupation', {}).pipe(filter(r => r.status === 0), map(r => r.result));
  }
  /** 職務名稱 */
  public getOccupationtitle() {
    return this.httpClient.post<WebResult>('../api/data/occupationtitle', {}).pipe(filter(r => r.status === 0), map(r => r.result));
  }
  /** 帳單寄送方式清單 */
  public getBilltype() {
    return this.httpClient.post<WebResult>('../api/data/billtype', {}).pipe(filter(r => r.status === 0), map(r => r.result));
  }
  /** 貸款學歷清單 */
  public getEducation() {
    return this.httpClient.post<WebResult>('../api/data/education', {}).pipe(filter(r => r.status === 0), map(r => r.result));
  }
  /** 婚姻狀況 */
  public getMarriage() {
    return this.httpClient.post<WebResult>('../api/data/marriage', {}).pipe(filter(r => r.status === 0), map(r => r.result));
  }
  /** 不動產狀況 */
  public getEstate() {
    return this.httpClient.post<WebResult>('../api/data/estate', {}).pipe(filter(r => r.status === 0), map(r => r.result));
  }
  /** 身分證換補發縣市 */
  public getIdcardlocation() {
    return this.httpClient.post<WebResult>('../api/data/idcardlocation', {}).pipe(filter(r => r.status === 0), map(r => r.result));
  }
  /** 二親等以內血親 */
  public getRelationDegree() {
    return this.httpClient.post<WebResult>('../api/data/relationDegree', {}).pipe(filter(r => r.status === 0), map(r => r.result));
  }
  /** 企業負責人 */
  public getRelationDegreeCorp() {
    return this.httpClient.post<WebResult>('../api/data/relationDegreeCorp', {}).pipe(filter(r => r.status === 0), map(r => r.result));
  }
  /** 填寫信用卡申請書頁 - 帳單類別 */
  public getBillType() {
    return this.httpClient.post<WebResult>('../api/data/billType', {}).pipe(filter(r => r.status === 0), map(r => r.result));
  }

  /** 聯絡課服 - 聯絡單位 */
  public getQR_ChannelDepartType() {
    return this.httpClient.post<WebResult>('../api/data/QR_ChannelDepartType', {}).pipe(filter(r => r.status === 0), map(r => r.result));
  }

  /**
   * 取得信用卡列表資料
   */
  public getCards() {
    return this.httpClient.get<WebResult>('../api/credit/getCreditCardList');
  }

  public queryCommonData(types: string[]) {
    return this.httpClient.post('queryCommonData', {
      typeArray: types
    });
  }

  /**
   * 取得推廣單位列表
   */
  public getChannelList() {
    return this.httpClient.get<WebResult>('../publicApi/channel/getChannelList');
  }

  /**
   * 取得單位代號列表
   */
  public getDepartList(channelID: string) {
    return this.httpClient.post<WebResult>('../publicApi/channel/getChannelDepartList', { channelID });
  }

  /**
   * 取得業務員代號
   */
  public getAgentNo(shortUrl: string) {
    return this.httpClient.post<WebResult>('../publicApi/channel/qrshorturl', { shortUrl });
  }
}
