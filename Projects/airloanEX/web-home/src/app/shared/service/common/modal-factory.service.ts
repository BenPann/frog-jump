import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ShowMessageComponent } from 'src/app/shared/component/show-message/show-message.component';
import { ModalSetting } from '../../../interface/resultType';
import { ShowImageComponent } from '../../component/show-image/show-image.component';
import { ShowHtmlComponent } from '../../component/show-html/show-html.component';

@Injectable({
  providedIn: 'root'
})
export class ModalFactoryService {
  constructor(private modalService: NgbModal) {}

  public modalAjaxError() {
    const modalRef = this.modalService.open(ShowMessageComponent, {
      centered: true
    });
    modalRef.componentInstance.setting = {
      title: '錯誤',
      subTitle: '',
      message: '連線錯誤',
      showConfirm: false
    };
  }

  public modalDefault(setting: ModalSetting) {
    const modalRef = this.modalService.open(ShowMessageComponent, {
      centered: true
    });
    modalRef.componentInstance.setting = setting;
    return modalRef;
  }

  public modalImage(setting: ModalSetting) {
    const modalRef = this.modalService.open(ShowImageComponent, {
      centered: true
    });
    modalRef.componentInstance.setting = setting;
    return modalRef;
  }

  public modalHtml(setting: ModalSetting) {
    const modalRef = this.modalService.open(ShowHtmlComponent, {
      centered: true
    });
    modalRef.componentInstance.setting = setting;
    return modalRef;
  }
}
