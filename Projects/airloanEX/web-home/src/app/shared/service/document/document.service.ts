import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {WebResult} from 'src/app/interface/resultType';
import { PhotoView } from 'src/app/service/apply-account/apply-account.interface';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {
  constructor(private httpClient: HttpClient) {
  }

  upload(photoView: PhotoView) {
    return this.httpClient.post<WebResult>(
      '../api/photo/upload',
        photoView
    );
  }

  ocr(image: string | ArrayBuffer) {
    return this.httpClient.post<WebResult>('../api/photo/uploadBCard', {
      base64Image: image
    });
  }

  deletePhoto(serial: string) {
    return this.httpClient.post<WebResult>(
      '../api/photo/deletePhoto',
      {
        subSerial: serial
      }
    );
  }

  getPhotoList() {
    return this.httpClient.get<WebResult>('../api/photo/getPhotoList');
  }
}
