export interface UploadDocument {
  frontId: string;
  frontIdSerial: string;
  backId: string;
  backIdSerial: string;
  secCardId: string;
  secCardIdSerial: string;
}
