import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadCropperComponent } from './upload-cropper.component';

describe('UploadCropperComponent', () => {
  let component: UploadCropperComponent;
  let fixture: ComponentFixture<UploadCropperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadCropperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadCropperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
