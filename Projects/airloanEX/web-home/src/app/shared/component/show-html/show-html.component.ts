import { Component, OnInit } from '@angular/core';
import { ModalSetting } from 'src/app/interface/resultType';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-show-html',
  templateUrl: './show-html.component.html',
  styleUrls: ['./show-html.component.scss']
})
export class ShowHtmlComponent implements OnInit {
  public setting: ModalSetting;
  constructor(public modal: NgbActiveModal) {}

  public ngOnInit(): void {
    const defaultSetting: ModalSetting = {
      title: '',
      subTitle: '',
      showConfirm: false,
      message: '',
      btnOK: '確認',
      btnCancel: '取消',
    };
    this.setting = Object.assign(defaultSetting, this.setting);
  }

  notSubTitle(): string {
    if (this.setting.subTitle) {
      return '';
    } else {
      return 'w-100';
    }
  }
}
