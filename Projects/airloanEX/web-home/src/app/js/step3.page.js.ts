import { Observable } from 'rxjs';

// export function removeImage(th) {
// th.parentNode.remove();
// 檢查按鈕狀態
// checkButtonDisable();
// }

export function collapseIt(th) {
    if (th.classList.contains('collapse-open')) {
        th.classList.remove('collapse-open');
    } else {
        th.classList.add('collapse-open');
    }
}

export function filterSample(th) {
    const type = th.id;

    document.getElementById('filterArea').classList.add('show');
    setTimeout(function () {
        document.getElementById('filterArea').classList.remove('show');
    }, 500);

    const allItems = document.getElementById('filterArea').querySelectorAll('tr');
    for (let i = 0; i < allItems.length; i++) {
        allItems[i].hidden = true;
    }

    const targetItems = document.getElementById('filterArea').querySelectorAll('tr[data-type*="' + th.id + '"], tr[data-type*="' + th.id + '"]+tr');
    for (let i = 0; i < targetItems.length; i++) {
        // targetItems[i].hidden = false;
        // 移除 hidden 標籤
        targetItems[i].removeAttribute('hidden');
    }

}

export function fillImage(th) {
    // const fileBox = th.parentNode.parentNode.parentNode;

    const reader = new FileReader();
    reader.readAsDataURL(th.files[0]);
    const _ifJpgPngMatch = /.+(png|jpeg)/g.test(th.files[0].type);
    if (!_ifJpgPngMatch) {
        // title: '檔案名稱不符合',
        // message: '上傳檔案以JPG、PNG為限'
        alert('上傳檔案以JPG、PNG為限'); // FIXME: use dialog
        return new Observable(observer => { observer.complete(); });
    } else {
        return new Observable(observer => {
            reader.onloadend = () => {
                // fileBox.classList.add('has-image');
                // fileBox.children[1].src = reader.result;
                // console.log(reader.result.toString()); // data:image/png;base64,圖片字串
                const text = reader.result.toString();
                if (text.length > 6291456) {
                    // title: '檔案大小不符',
                    // message: '上傳檔案大小以6MB為限'
                    alert('上傳檔案大小以6MB為限'); // FIXME: use dialog
                } else {
                    observer.next(text);
                }
                observer.complete();
            };
        });
    }


    // if (fileBox.nextElementSibling.classList.contains('file-box') !== true) {
    //     // cloneFileBox(fileBox.nextElementSibling);
    //     return fileBox.children[1].src;
    // }
} // end fillImage

export function cloneFileBox(target) {
    const fileBoxClone = document.querySelector('.file-box').cloneNode(true);
    const tempClone = fileBoxClone.cloneNode(true);
    document.querySelector('form').insertBefore(tempClone, target);
}

export function checkButtonDisable() {
    const button = document.querySelector('form .btn-submit');
    if (document.querySelector('.has-image')) {
        return false;
    } else {
        return true;
    }
}
