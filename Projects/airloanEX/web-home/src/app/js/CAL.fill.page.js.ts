
export function closeDropdown(target, label, th) {
    if (th.checkValidity() === true) {
        // setTimeout(function () {
        //     const targetCheckbox = document.querySelector(target);
        //     targetCheckbox.checked = false;
        //     targetCheckbox.nextElementSibling.textContent = label;
        // }, 800)
    }
}

export function slideMove(index, th) {
    th.blur();

    if (th.checkValidity() === true) {
        const myslide = th.parentNode.parentNode;
        const bodyWrapper = document.querySelector('.body-wrapper');

        const nextSlideBlock: any = document.querySelectorAll('.slide-block')[index - 1];
        nextSlideBlock.hidden = false;

        setTimeout(function () {

            const id = setInterval(function () {
                let pos = 0;
                const initialPos = bodyWrapper.scrollTop;
                // 因為取得 bodyWrapper.scrollTop 不同裝置數值不同，也會有小數點，scrollHeight減 10 讓符合條件的寬一點
                // 所以可能無法相等，或是出現 clientHeight + scrollTop 數值就是比較小的情況
                if (bodyWrapper.scrollHeight - 10 <= bodyWrapper.clientHeight + bodyWrapper.scrollTop) {
                    bodyWrapper.scrollTop = bodyWrapper.scrollHeight - bodyWrapper.clientHeight;
                    clearInterval(id);
                } else {
                    pos += 50;
                    bodyWrapper.scrollTop = initialPos + pos;
                }
            }, 15);

            const progress: any = document.getElementById('progress');
            progress.value = index;

        }, 100);

        setTimeout(function () {
            myslide.classList.add('slide-collapsed');
            const textInput = nextSlideBlock.querySelector('input[type="tel"]');
            if (textInput) {
                textInput.focus();
            }
        }, 500)

    }
}

