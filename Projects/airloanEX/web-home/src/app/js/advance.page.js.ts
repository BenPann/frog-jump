
export function rangeMoving(rangeinput) {
    rangeinput.previousElementSibling.style.width = (rangeinput.value - rangeinput.min) / (rangeinput.max - rangeinput.min) * 100 + "%";
}

export function initRange() {
    const ranges = document.querySelectorAll('input[type="range"]');
    for (let i = 0; i < ranges.length; i++) {
        rangeMoving(ranges[i]);
    }
}

// 取網址參數 by KGI-IT Division
export function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

export function getParameter() {
    if (this.getParameterByName('PROD') === 'RPL') {

        const oTarget = document.querySelectorAll('#amount');
        console.log(oTarget);
        oTarget[0].setAttribute('max', '100');
        oTarget[0].setAttribute('min', '5');
        oTarget[0].setAttribute('value', '5');

        const oTarget2 = document.querySelectorAll('#year');
        console.log(oTarget);
        oTarget2[0].setAttribute('max', '12');
        oTarget2[0].setAttribute('min', '1');
        oTarget2[0].setAttribute('value', '12');
        oTarget2[0].setAttribute('oninput', 'rangeMoving(this);year.textContent=this.value+" 個月"');

        document.getElementById('amount').innerHTML = '5 萬';

        document.getElementById('year').innerHTML = '12 個月';


    };
}