import { closeCandidate } from './person.page.js';

// 公司名稱候選清單
export function dedicateCompany(myvalue, th, companys?) {
    const mylist = th.nativeElement.querySelector('.candidate');
    const mywrapper = mylist?.parentNode;
    if (/./.test(myvalue) && companys.length !== 0) {
        if (mywrapper) {
            mywrapper.classList.add('has-candidate');
        }
        // mylist.children[0].focus();
        // mylist.children[0].textContent = '凱基商業銀行';
        // mylist.children[1].textContent = '凱基證券';
        // mylist.children[2].textContent = '凱基信用卡';
        // mylist.children[3].textContent = '凱基超長超長超長超長超長超長超長超長超長超長超長超長超長超長';
        // mylist.children[3].textContent = '凱基福利社';
        // mylist.children[3].textContent = '凱基期貨';

        th.nativeElement.onkeydown = function (e) {
            if (e.keyCode === 40 && mylist.children[0]) {
                mylist.children[0].focus();
            }

            if (e.keyCode === 27) {
                closeCandidate(mywrapper);
            }
        };
    } else {
        if (mywrapper) {
            mywrapper.classList.remove('has-candidate');
        }
        th.onkeydown = '';
    }
}

/***************
選好縣市後自動帶入公司電話
***************/
export function prefixFill(th, target) {
    const targetEl = document.querySelector(target);
    if (targetEl.value !== '') {
        return;
    }
    targetEl.value = phonejson[th.target.value] + '-';
}

export const phonejson = {
    基隆市: '02',
    台北市: '02',
    新北市: '02',
    桃園市: '03',
    宜蘭縣: '03',
    新竹縣: '03',
    新竹市: '03',
    苗栗縣: '037',
    台中市: '04',
    南投縣: '049',
    彰化縣: '04',
    雲林縣: '05',
    嘉義縣: '05',
    嘉義市: '05',
    台南市: '06',
    高雄市: '07',
    屏東市: '08',
    花蓮縣: '03',
    台東縣: '089',
    澎湖縣: '06',
    金門縣: '0826',
    連江縣: '0836',
}