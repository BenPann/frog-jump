import { FormControl } from '@angular/forms';


// 非必填的下拉選單 請選擇Placeholder
export function selectActive(th) {
    th.classList.add('txt-black');
}

// 新增資料類型 切換動態
export function dataToggle(th) {
    const parent = th.parentNode.parentNode;

    const allDataTypeItem = parent.querySelectorAll('[data-type]');
    for (let i = 0; i < allDataTypeItem.length; i++) {
        allDataTypeItem[i].hidden = true;
    }

    const selectedDataTypeItem = parent.querySelectorAll('[data-type="' + th.value + '"]');
    for (let i = 0; i < selectedDataTypeItem.length; i++) {
        selectedDataTypeItem[i].hidden = false;
    }
}
export function cloneAddData(target) {
    // 新增一組複製動態
    const addDataClone = document.querySelector('.addData').cloneNode(true);
    const tempClone = addDataClone.cloneNode(true);
    document.querySelector('form').insertBefore(tempClone, target);
}

/***************
身分證末碼檢核

身分證欄位掛上.id-validate
***************/
export function idValidate(mobile: FormControl): any {
    // const a = this.applyForm;
    if (mobile.value) {
        if (checkID(mobile.value) === false) {
            return { 'errors': true };
        }
    } else {
        return null;
    }
}
// export function idValidate(th) {
//     if (checkID(th.value) === false) {
//         th.setCustomValidity('此身分證字號有誤');
//     } else {
//         th.setCustomValidity('');
//     }
// }

// 身分證檢查(長度、尾碼計算)
export function checkID(id) {

    const tab = "ABCDEFGHJKLMNPQRSTUVXYWZIO"

    const A1 = new Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3);
    const A2 = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5);
    const Mx = new Array(9, 8, 7, 6, 5, 4, 3, 2, 1, 1);

    if (id.length != 10) return false;
    const i = tab.indexOf(id.charAt(0));
    if (i == -1) return false;
    let sum = A1[i] + A2[i] * 9;

    for (let i = 1; i < 10; i++) {
        const v = parseInt(id.charAt(i));
        if (isNaN(v)) return false;
        sum = sum + v * Mx[i];
    }

    if (sum % 10 != 0) return false;
    return true;
}