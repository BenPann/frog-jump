import { showDialog } from './start.page.js';

// let tempSavedData = [
//     { title: '', value: [''] },
// ];

export function initClickDialog() {
    const allTableRows = document.querySelectorAll('tr');
    for (let i = 0; i < allTableRows.length; i++) {
        allTableRows[i].addEventListener('click', function() {
            showDialog('#dialog' + this.dataset.index);
            // dataFillDialog(this.dataset.index);
        });
    }
}

// export function dataFillTable(data?) {
//     if (data) {
//         tempSavedData = data;
//     }
//     const allTableRows = document.querySelectorAll('tr');
//     for (let i = 0; i < allTableRows.length; i++) {
//         const mytd = allTableRows[i].children[1];
//         let formattedValue = '';
//         if (i === 2) {
//             // 生日
//             const aaa = new Date(tempSavedData[i].value[0]);
//             formattedValue = aaa.toLocaleString('zh-TW', {
//                 // dateStyle: 'long',
//                 year: 'numeric',
//                 month: 'long',
//                 day: 'numeric',
//             });
//         } else if (i === 12) {
//             // 年收入
//             formattedValue = tempSavedData[i].value[0] + '萬';
//         } else if (i === 10) {
//             // 公司電話
//             formattedValue = tempSavedData[i].value[0] + '#' + tempSavedData[i].value[1];
//         } else {
//             // 其他狀況
//             formattedValue = tempSavedData[i].value.join('');
//         }


//         // 資料闕漏
//         if (tempSavedData[i].value[0] === '') {
//             formattedValue = '請填寫';
//             mytd.classList.add('txt-red');
//         } else {
//             mytd.classList.remove('txt-red');
//         }

//         mytd.textContent = formattedValue;


//     }
// }


// export function dataFillDialog(targetIndex) {
//     const matchData = tempSavedData[targetIndex];
//     const dialogForm = window['dialog' + targetIndex].children[0];

//     for (let i = 0; i < dialogForm.length - 2; i++) {
//         dialogForm.elements[i].value = matchData.value[i];
//     }
// }
// export function initDialogForm() {
//     const allDialogDataSubmits = document.querySelectorAll('.dialog form .btn-submit');
//     const allDialogDataSubmitsArray = Array.prototype.slice.call(allDialogDataSubmits);
//     allDialogDataSubmitsArray.forEach(function (item, idx) {
//         item.addEventListener('click', function () {
//             for (let k = 0; k < this.form.length - 2; k++) {
//                 tempSavedData[idx].value[k] = this.form.elements[k].value;
//             }
//             dataFillTable();
//         });
//     });
// }
