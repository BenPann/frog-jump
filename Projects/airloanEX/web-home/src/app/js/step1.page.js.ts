import { Cropper } from 'src/assets/inc/library/cropperjs/dist/cropper.js';
import { showDialog } from './start.page.js';

// Must declare
declare const Cropper: any;

let originImage;
export let cropper;
let nowFileBox;
let nowCropperData;


export function removeImage(th) {
    const myfilebox = th.parentNode;
    myfilebox.children[1].src = 'assets/inc/default/img/demo/blank.png';
    myfilebox.classList.remove('has-image');
}

export function showCropDialog(target, th) {
    nowFileBox = th.parentNode.parentNode.parentNode;
    const mydialog = document.querySelector(target);
    originImage = mydialog.querySelector('img');
    const reader = new FileReader();
    // if(th.files.length===0){
    // 	th.parentNode.parentNode.parentNode.children[1].src='../../inc/default/img/demo/blank.png';
    // 	th.parentNode.parentNode.parentNode.classList.remove('has-image');
    // 	return;
    // }
    reader.readAsDataURL(th.files[0]);
    reader.addEventListener('load', function() {
        const imageSource = reader.result;
        console.log('cropper' ,cropper);
        if (cropper) {
            cropper.replace(imageSource);
        } else {
            originImage.src = imageSource;
            cropper = new Cropper(originImage, {
                aspectRatio: 16 / 9,
                cropBoxMovable: false,
                cropBoxResizable: false,
                dragMode: 'move',
                ready() {
                    if (innerWidth < 1001) {
                        // 手機版切尺寸
                        const mywidth = innerWidth * 0.9;
                        const myleft = innerWidth * 0.05;
                        cropper.setCropBoxData({ width: mywidth, left: myleft });
                    } else {
                        // 桌機版切圖尺寸
                        cropper.setCropBoxData({ width: 480, left: 60, top: 40 });
                    }
                },
            });
        }

    });
    showDialog(target);
    th.value = '';
}

export function showConfDialog(isFront?: boolean) {
    nowCropperData = cropper.getCroppedCanvas().toDataURL('image/jpeg');

    if (isFront === true) {
        document.getElementById('sampleImageFront').setAttribute('src', nowCropperData);

        showDialog('#dialog2');
    } else {
        document.getElementById('sampleImageBack').setAttribute('src', nowCropperData);
        showDialog('#dialog3');
    }

}

export function fillImage(th) {
    nowFileBox.classList.add('has-image'); // <div class="file-box form-item">
    nowFileBox.children[1].src = nowCropperData; // <img>
    // console.log(nowFileBox);
    // console.log(nowFileBox.children[1]);
    console.log('nowCropperData.length=' + nowCropperData.length);
    return nowCropperData;
}
