import { element } from 'protractor';
import { Event } from '@angular/router';

/***************
      戶籍市話同住家
      ***************/
// export function phoneAutoFill(th, houseLivingPhone, familyAddressPhone) {
//     if (th.checked === true) {
//         const houseEl = document.querySelector(houseLivingPhone);
//         const familyEl = document.querySelector(familyAddressPhone);

//         familyEl.value = houseEl.value;
//         familyEl.focus();
//         familyEl.blur();
//     }
// }

// email候選清單
export function dedicateMail(myvalue, th) {
    const mylist = th.nativeElement.querySelector('.candidate');
    const mywrapper = mylist.parentNode;

    if (/@$/.test(myvalue)) {
        mywrapper.classList.add('has-candidate');
        // mylist.children[0].focus();
        // mylist.children[0].textContent = myvalue + 'gmail.com';
        // mylist.children[1].textContent = myvalue + 'yahoo.com.tw';
        // mylist.children[2].textContent = myvalue + 'hotmail.com';
        // mylist.children[3].textContent = myvalue + 'hinet.net';
        // 清空檢核紅字
        // th.nativeElement.nextElementSibling.textContent = '';

        th.nativeElement.onkeydown = function (e) {
            if (e.keyCode === 40) {
                mylist.children[0].focus();
            }

            if (e.keyCode === 27) {
                closeCandidate(mywrapper);
            }
        };
    } else {
        mywrapper.classList.remove('has-candidate');
        th.onkeydown = '';
    }
}

// 共用函式 關閉候選清單
export function closeCandidate(target) {
    const myinput = target.querySelector('input');
    target.classList.remove('has-candidate');
    myinput.focus();
    myinput.blur(); // 跑檢核
}

// 共用函式 帶入選項
export function importValue(th) {
    const mywrapper = th.parentNode.parentNode;
    if(mywrapper){
        const myinput = mywrapper.querySelector('input');
        myinput.value = th.textContent;
        closeCandidate(mywrapper);
    }
}


// email 需使用此方法才正常
export function candidate() {

    const candidateItems = document.querySelectorAll('.candidate .candidate-item');
    for (let i = 0; i < candidateItems.length; i++) {

        // 上下鍵選取、Enter鍵帶入、Esc鍵關閉候選清單
        candidateItems[i].addEventListener('keydown', function (e: any) {
            const nextEl = this.nextElementSibling;
            const prevEl = this.previousElementSibling;
            const mywrapper = this.parentNode.parentNode;

            if (e.keyCode === 27) {
                closeCandidate(mywrapper);
            }

            if (e.keyCode === 13) {
                importValue(this);
            }

            if (e.keyCode === 40) {
                if (nextEl) {
                    nextEl.focus();
                }
            }

            if (e.keyCode === 38) {
                if (prevEl) {
                    prevEl.focus();
                }
            }
        });

        // 滑鼠點選後帶入值
        candidateItems[i].addEventListener('click', function (e) {
            importValue(this);
        });
    }

    // 開啟候選清單後任意按畫面一個地方可以關掉它
    window.addEventListener('click', function (e) {
        const mywrapper = document.querySelector('.has-candidate');
        if (mywrapper && e.target !== mywrapper.querySelector('input')) {
            closeCandidate(mywrapper);
        }
    })
}


/***************
行政區下拉選單
***************/
// export function makeDistrict(th, districtSelect) {
//     const districtEl = document.querySelector(districtSelect);
//     const defaultOption = districtEl.children[0].cloneNode('deep');

//     districtEl.innerHTML = '';
//     districtEl.appendChild(defaultOption);
//     districtEl.classList.remove('need-valid');
//     if (th.value) {
//         for (let i = 0; i < cityjson[th.value].length; i++) {
//             const node = document.createElement('option');
//             node.value = cityjson[th.value][i].key;
//             node.textContent = cityjson[th.value][i].value;
//             districtEl.appendChild(node);
//         }
//     }

// }

export const countiesOptions = [
    {
        counties: '北部', items: [
            { dataKey: '基隆市', dataName: '基隆市' },
            { dataKey: '台北市', dataName: '台北市' },
            { dataKey: '新北市', dataName: '新北市' },
            { dataKey: '桃園市', dataName: '桃園市' },
            { dataKey: '宜蘭縣', dataName: '宜蘭縣' },
            { dataKey: '新竹縣', dataName: '新竹縣' },
            { dataKey: '新竹市', dataName: '新竹市' },
        ]
    },
    {
        counties: '中部', items: [
            { dataKey: '苗栗縣', dataName: '苗栗縣' },
            { dataKey: '台中市', dataName: '台中市' },
            { dataKey: '南投縣', dataName: '南投縣' },
            { dataKey: '彰化縣', dataName: '彰化縣' },
            { dataKey: '雲林縣', dataName: '雲林縣' },
        ]
    },
    {
        counties: '南部', items: [
            { dataKey: '嘉義縣', dataName: '嘉義縣' },
            { dataKey: '嘉義市', dataName: '嘉義市' },
            { dataKey: '台南市', dataName: '台南市' },
            { dataKey: '高雄市', dataName: '高雄市' },
            { dataKey: '屏東縣', dataName: '屏東縣' },
        ]
    },
    {
        counties: '東部與外島', items: [
            { dataKey: '花蓮縣', dataName: '花蓮縣' },
            { dataKey: '台東縣', dataName: '台東縣' },
            { dataKey: '澎湖縣', dataName: '澎湖縣' },
            { dataKey: '金門縣', dataName: '金門縣' },
            { dataKey: '連江縣', dataName: '連江縣' },
        ]
    },
];
export const cityjson = {
    '基隆市': [
        { dataKey: 200, dataName: '仁愛區' },
        { dataKey: 201, dataName: '信義區' },
        { dataKey: 202, dataName: '中正區' },
        { dataKey: 203, dataName: '中山區' },
        { dataKey: 204, dataName: '安樂區' },
        { dataKey: 205, dataName: '暖暖區' },
        { dataKey: 206, dataName: '七堵區' },
    ],

    '台北市': [
        { dataKey: 100, dataName: '中正區', },
        { dataKey: 103, dataName: '大同區', },
        { dataKey: 104, dataName: '中山區', },
        { dataKey: 105, dataName: '松山區', },
        { dataKey: 106, dataName: '大安區', },
        { dataKey: 108, dataName: '萬華區', },
        { dataKey: 110, dataName: '信義區', },
        { dataKey: 111, dataName: '士林區', },
        { dataKey: 112, dataName: '北投區', },
        { dataKey: 114, dataName: '內湖區', },
        { dataKey: 115, dataName: '南港區', },
        { dataKey: 116, dataName: '文山區', },
    ],


    '新北市': [
        { dataKey: 207, dataName: '萬里區', },
        { dataKey: 208, dataName: '金山區', },
        { dataKey: 220, dataName: '板橋區', },
        { dataKey: 221, dataName: '汐止區', },
        { dataKey: 222, dataName: '深坑區', },
        { dataKey: 223, dataName: '石碇區', },
        { dataKey: 224, dataName: '瑞芳區', },
        { dataKey: 226, dataName: '平溪區', },
        { dataKey: 227, dataName: '雙溪區', },
        { dataKey: 228, dataName: '貢寮區', },
        { dataKey: 231, dataName: '新店區', },
        { dataKey: 232, dataName: '坪林區', },
        { dataKey: 233, dataName: '烏來區', },
        { dataKey: 234, dataName: '永和區', },
        { dataKey: 235, dataName: '中和區', },
        { dataKey: 236, dataName: '土城區', },
        { dataKey: 237, dataName: '三峽區', },
        { dataKey: 238, dataName: '樹林區', },
        { dataKey: 239, dataName: '鶯歌區', },
        { dataKey: 241, dataName: '三重區', },
        { dataKey: 242, dataName: '新莊區', },
        { dataKey: 243, dataName: '泰山區', },
        { dataKey: 244, dataName: '林口區', },
        { dataKey: 247, dataName: '蘆洲區', },
        { dataKey: 248, dataName: '五股區', },
        { dataKey: 249, dataName: '八里區', },
        { dataKey: 251, dataName: '淡水區', },
        { dataKey: 252, dataName: '三芝區', },
        { dataKey: 253, dataName: '石門區', },
    ],

    '桃園市': [
        { dataKey: 320, dataName: '中壢區', },
        { dataKey: 324, dataName: '平鎮區', },
        { dataKey: 325, dataName: '龍潭區', },
        { dataKey: 326, dataName: '楊梅區', },
        { dataKey: 327, dataName: '新屋區', },
        { dataKey: 328, dataName: '觀音區', },
        { dataKey: 330, dataName: '桃園區', },
        { dataKey: 333, dataName: '龜山區', },
        { dataKey: 334, dataName: '八德區', },
        { dataKey: 335, dataName: '大溪區', },
        { dataKey: 336, dataName: '復興區', },
        { dataKey: 337, dataName: '大園區', },
        { dataKey: 338, dataName: '蘆竹區', },
    ],

    '宜蘭縣': [
        { dataKey: 260, dataName: '宜蘭市', },
        { dataKey: 261, dataName: '頭城鎮', },
        { dataKey: 262, dataName: '礁溪鄉', },
        { dataKey: 263, dataName: '壯圍鄉', },
        { dataKey: 264, dataName: '員山鄉', },
        { dataKey: 265, dataName: '羅東鎮', },
        { dataKey: 266, dataName: '三星鄉', },
        { dataKey: 267, dataName: '大同鄉', },
        { dataKey: 268, dataName: '五結鄉', },
        { dataKey: 269, dataName: '冬山鄉', },
        { dataKey: 270, dataName: '蘇澳鎮', },
        { dataKey: 272, dataName: '南澳鄉', },
        { dataKey: 290, dataName: '釣魚臺', },
    ],
    '新竹縣': [
        { dataKey: 302, dataName: '竹北市', },
        { dataKey: 303, dataName: '湖口鄉', },
        { dataKey: 304, dataName: '新豐鄉', },
        { dataKey: 305, dataName: '新埔鎮', },
        { dataKey: 306, dataName: '關西鎮', },
        { dataKey: 307, dataName: '芎林鄉', },
        { dataKey: 308, dataName: '寶山鄉', },
        { dataKey: 310, dataName: '竹東鎮', },
        { dataKey: 311, dataName: '五峰鄉', },
        { dataKey: 312, dataName: '橫山鄉', },
        { dataKey: 313, dataName: '尖石鄉', },
        { dataKey: 314, dataName: '北埔鄉', },
        { dataKey: 315, dataName: '峨眉鄉', },
    ],
    '新竹市': [
        { dataKey: 300, dataName: '東區', },
        { dataKey: 300, dataName: '北區', },
        { dataKey: 300, dataName: '香山區', },
    ],
    '苗栗縣': [
        { dataKey: 350, dataName: '竹南鎮', },
        { dataKey: 351, dataName: '頭份市', },
        { dataKey: 352, dataName: '三灣鄉', },
        { dataKey: 353, dataName: '南庄鄉', },
        { dataKey: 354, dataName: '獅潭鄉', },
        { dataKey: 356, dataName: '後龍鎮', },
        { dataKey: 357, dataName: '通霄鎮', },
        { dataKey: 358, dataName: '苑裡鎮', },
        { dataKey: 360, dataName: '苗栗市', },
        { dataKey: 361, dataName: '造橋鄉', },
        { dataKey: 362, dataName: '頭屋鄉', },
        { dataKey: 363, dataName: '公館鄉', },
        { dataKey: 364, dataName: '大湖鄉', },
        { dataKey: 365, dataName: '泰安鄉', },
        { dataKey: 366, dataName: '銅鑼鄉', },
        { dataKey: 367, dataName: '三義鄉', },
        { dataKey: 368, dataName: '西湖鄉', },
        { dataKey: 369, dataName: '卓蘭鎮', },
    ],
    '台中市': [
        { dataKey: 400, dataName: '中區', },
        { dataKey: 401, dataName: '東區', },
        { dataKey: 402, dataName: '南區', },
        { dataKey: 403, dataName: '西區', },
        { dataKey: 404, dataName: '北區', },
        { dataKey: 406, dataName: '北屯區', },
        { dataKey: 407, dataName: '西屯區', },
        { dataKey: 408, dataName: '南屯區', },
        { dataKey: 411, dataName: '太平區', },
        { dataKey: 412, dataName: '大里區', },
        { dataKey: 413, dataName: '霧峰區', },
        { dataKey: 414, dataName: '烏日區', },
        { dataKey: 420, dataName: '豐原區', },
        { dataKey: 421, dataName: '后里區', },
        { dataKey: 422, dataName: '石岡區', },
        { dataKey: 423, dataName: '東勢區', },
        { dataKey: 424, dataName: '和平區', },
        { dataKey: 426, dataName: '新社區', },
        { dataKey: 427, dataName: '潭子區', },
        { dataKey: 428, dataName: '大雅區', },
        { dataKey: 429, dataName: '神岡區', },
        { dataKey: 432, dataName: '大肚區', },
        { dataKey: 433, dataName: '沙鹿區', },
        { dataKey: 434, dataName: '龍井區', },
        { dataKey: 435, dataName: '梧棲區', },
        { dataKey: 436, dataName: '清水區', },
        { dataKey: 437, dataName: '大甲區', },
        { dataKey: 438, dataName: '外埔區', },
        { dataKey: 439, dataName: '大安區', },
    ],
    '南投縣': [
        { dataKey: 540, dataName: '南投市', },
        { dataKey: 541, dataName: '中寮鄉', },
        { dataKey: 542, dataName: '草屯鎮', },
        { dataKey: 544, dataName: '國姓鄉', },
        { dataKey: 545, dataName: '埔里鎮', },
        { dataKey: 546, dataName: '仁愛鄉', },
        { dataKey: 551, dataName: '名間鄉', },
        { dataKey: 552, dataName: '集集鎮', },
        { dataKey: 553, dataName: '水里鄉', },
        { dataKey: 555, dataName: '魚池鄉', },
        { dataKey: 556, dataName: '信義鄉', },
        { dataKey: 557, dataName: '竹山鎮', },
        { dataKey: 558, dataName: '鹿谷鄉', },
    ],
    '彰化縣': [
        { dataKey: 500, dataName: '彰化市', },
        { dataKey: 502, dataName: '芬園鄉', },
        { dataKey: 503, dataName: '花壇鄉', },
        { dataKey: 504, dataName: '秀水鄉', },
        { dataKey: 505, dataName: '鹿港鎮', },
        { dataKey: 506, dataName: '福興鄉', },
        { dataKey: 507, dataName: '線西鄉', },
        { dataKey: 508, dataName: '和美鎮', },
        { dataKey: 509, dataName: '伸港鄉', },
        { dataKey: 510, dataName: '員林市', },
        { dataKey: 511, dataName: '社頭鄉', },
        { dataKey: 512, dataName: '永靖鄉', },
        { dataKey: 513, dataName: '埔心鄉', },
        { dataKey: 514, dataName: '溪湖鎮', },
        { dataKey: 515, dataName: '大村鄉', },
        { dataKey: 516, dataName: '埔鹽鄉', },
        { dataKey: 520, dataName: '田中鎮', },
        { dataKey: 521, dataName: '北斗鎮', },
        { dataKey: 522, dataName: '田尾鄉', },
        { dataKey: 523, dataName: '埤頭鄉', },
        { dataKey: 524, dataName: '溪州鄉', },
        { dataKey: 525, dataName: '竹塘鄉', },
        { dataKey: 526, dataName: '二林鎮', },
        { dataKey: 527, dataName: '大城鄉', },
        { dataKey: 528, dataName: '芳苑鄉', },
        { dataKey: 530, dataName: '二水鄉', },
    ],

    '雲林縣': [
        { dataKey: 630, dataName: '斗南鎮', },
        { dataKey: 631, dataName: '大埤鄉', },
        { dataKey: 632, dataName: '虎尾鎮', },
        { dataKey: 633, dataName: '土庫鎮', },
        { dataKey: 634, dataName: '褒忠鄉', },
        { dataKey: 635, dataName: '東勢鄉', },
        { dataKey: 636, dataName: '臺西鄉', },
        { dataKey: 637, dataName: '崙背鄉', },
        { dataKey: 638, dataName: '麥寮鄉', },
        { dataKey: 640, dataName: '斗六市', },
        { dataKey: 643, dataName: '林內鄉', },
        { dataKey: 646, dataName: '古坑鄉', },
        { dataKey: 647, dataName: '莿桐鄉', },
        { dataKey: 648, dataName: '西螺鎮', },
        { dataKey: 649, dataName: '二崙鄉', },
        { dataKey: 651, dataName: '北港鎮', },
        { dataKey: 652, dataName: '水林鄉', },
        { dataKey: 653, dataName: '口湖鄉', },
        { dataKey: 654, dataName: '四湖鄉', },
        { dataKey: 655, dataName: '元長鄉', },
    ],
    '嘉義縣': [
        { dataKey: 602, dataName: '番路鄉', },
        { dataKey: 603, dataName: '梅山鄉', },
        { dataKey: 604, dataName: '竹崎鄉', },
        { dataKey: 605, dataName: '阿里山鄉', },
        { dataKey: 606, dataName: '中埔鄉', },
        { dataKey: 607, dataName: '大埔鄉', },
        { dataKey: 608, dataName: '水上鄉', },
        { dataKey: 611, dataName: '鹿草鄉', },
        { dataKey: 612, dataName: '太保市', },
        { dataKey: 613, dataName: '朴子市', },
        { dataKey: 614, dataName: '東石鄉', },
        { dataKey: 615, dataName: '六腳鄉', },
        { dataKey: 616, dataName: '新港鄉', },
        { dataKey: 621, dataName: '民雄鄉', },
        { dataKey: 622, dataName: '大林鎮', },
        { dataKey: 623, dataName: '溪口鄉', },
        { dataKey: 624, dataName: '義竹鄉', },
        { dataKey: 625, dataName: '布袋鎮', },
    ],
    '嘉義市': [
        { dataKey: 600, dataName: '東區', },
        { dataKey: 600, dataName: '西區', },
    ],
    '台南市': [
        { dataKey: 700, dataName: '中西區', },
        { dataKey: 701, dataName: '東區', },
        { dataKey: 702, dataName: '南區', },
        { dataKey: 704, dataName: '北區', },
        { dataKey: 708, dataName: '安平區', },
        { dataKey: 709, dataName: '安南區', },
        { dataKey: 710, dataName: '永康區', },
        { dataKey: 711, dataName: '歸仁區', },
        { dataKey: 712, dataName: '新化區', },
        { dataKey: 713, dataName: '左鎮區', },
        { dataKey: 714, dataName: '玉井區', },
        { dataKey: 715, dataName: '楠西區', },
        { dataKey: 716, dataName: '南化區', },
        { dataKey: 717, dataName: '仁德區', },
        { dataKey: 718, dataName: '關廟區', },
        { dataKey: 719, dataName: '龍崎區', },
        { dataKey: 720, dataName: '官田區', },
        { dataKey: 721, dataName: '麻豆區', },
        { dataKey: 722, dataName: '佳里區', },
        { dataKey: 723, dataName: '西港區', },
        { dataKey: 724, dataName: '七股區', },
        { dataKey: 725, dataName: '將軍區', },
        { dataKey: 726, dataName: '學甲區', },
        { dataKey: 727, dataName: '北門區', },
        { dataKey: 730, dataName: '新營區', },
        { dataKey: 731, dataName: '後壁區', },
        { dataKey: 732, dataName: '白河區', },
        { dataKey: 733, dataName: '東山區', },
        { dataKey: 734, dataName: '六甲區', },
        { dataKey: 735, dataName: '下營區', },
        { dataKey: 736, dataName: '柳營區', },
        { dataKey: 737, dataName: '鹽水區', },
        { dataKey: 741, dataName: '善化區', },
        { dataKey: 742, dataName: '大內區', },
        { dataKey: 743, dataName: '山上區', },
        { dataKey: 744, dataName: '新市區', },
        { dataKey: 745, dataName: '安定區', },
    ],
    '高雄市': [
        { dataKey: 800, dataName: '新興區', },
        { dataKey: 801, dataName: '前金區', },
        { dataKey: 802, dataName: '苓雅區', },
        { dataKey: 803, dataName: '鹽埕區', },
        { dataKey: 804, dataName: '鼓山區', },
        { dataKey: 805, dataName: '旗津區', },
        { dataKey: 806, dataName: '前鎮區', },
        { dataKey: 807, dataName: '三民區', },
        { dataKey: 811, dataName: '楠梓區', },
        { dataKey: 812, dataName: '小港區', },
        { dataKey: 813, dataName: '左營區', },
        { dataKey: 814, dataName: '仁武區', },
        { dataKey: 815, dataName: '大社區', },
        { dataKey: 817, dataName: '東沙群島', },
        { dataKey: 819, dataName: '南沙群島', },
        { dataKey: 820, dataName: '岡山區', },
        { dataKey: 821, dataName: '路竹區', },
        { dataKey: 822, dataName: '阿蓮區', },
        { dataKey: 823, dataName: '田寮區', },
        { dataKey: 824, dataName: '燕巢區', },
        { dataKey: 825, dataName: '橋頭區', },
        { dataKey: 826, dataName: '梓官區', },
        { dataKey: 827, dataName: '彌陀區', },
        { dataKey: 828, dataName: '永安區', },
        { dataKey: 829, dataName: '湖內區', },
        { dataKey: 830, dataName: '鳳山區', },
        { dataKey: 831, dataName: '大寮區', },
        { dataKey: 832, dataName: '林園區', },
        { dataKey: 833, dataName: '鳥松區', },
        { dataKey: 840, dataName: '大樹區', },
        { dataKey: 842, dataName: '旗山區', },
        { dataKey: 843, dataName: '美濃區', },
        { dataKey: 844, dataName: '六龜區', },
        { dataKey: 845, dataName: '內門區', },
        { dataKey: 846, dataName: '杉林區', },
        { dataKey: 847, dataName: '甲仙區', },
        { dataKey: 848, dataName: '桃源區', },
        { dataKey: 849, dataName: '那瑪夏區', },
        { dataKey: 851, dataName: '茂林區', },
        { dataKey: 852, dataName: '茄萣區', },
    ],
    '屏東縣': [
        { dataKey: 900, dataName: '屏東市', },
        { dataKey: 901, dataName: '三地門鄉', },
        { dataKey: 902, dataName: '霧臺鄉', },
        { dataKey: 903, dataName: '瑪家鄉', },
        { dataKey: 904, dataName: '九如鄉', },
        { dataKey: 905, dataName: '里港鄉', },
        { dataKey: 906, dataName: '高樹鄉', },
        { dataKey: 907, dataName: '鹽埔鄉', },
        { dataKey: 908, dataName: '長治鄉', },
        { dataKey: 909, dataName: '麟洛鄉', },
        { dataKey: 911, dataName: '竹田鄉', },
        { dataKey: 912, dataName: '內埔鄉', },
        { dataKey: 913, dataName: '萬丹鄉', },
        { dataKey: 920, dataName: '潮州鎮', },
        { dataKey: 921, dataName: '泰武鄉', },
        { dataKey: 922, dataName: '來義鄉', },
        { dataKey: 923, dataName: '萬巒鄉', },
        { dataKey: 924, dataName: '崁頂鄉', },
        { dataKey: 925, dataName: '新埤鄉', },
        { dataKey: 926, dataName: '南州鄉', },
        { dataKey: 927, dataName: '林邊鄉', },
        { dataKey: 928, dataName: '東港鎮', },
        { dataKey: 929, dataName: '琉球鄉', },
        { dataKey: 931, dataName: '佳冬鄉', },
        { dataKey: 932, dataName: '新園鄉', },
        { dataKey: 940, dataName: '枋寮鄉', },
        { dataKey: 941, dataName: '枋山鄉', },
        { dataKey: 942, dataName: '春日鄉', },
        { dataKey: 943, dataName: '獅子鄉', },
        { dataKey: 944, dataName: '車城鄉', },
        { dataKey: 945, dataName: '牡丹鄉', },
        { dataKey: 946, dataName: '恆春鎮', },
        { dataKey: 947, dataName: '滿州鄉', },
    ],
    '花蓮縣': [
        { dataKey: 970, dataName: '花蓮市', },
        { dataKey: 971, dataName: '新城鄉', },
        { dataKey: 972, dataName: '秀林鄉', },
        { dataKey: 973, dataName: '吉安鄉', },
        { dataKey: 974, dataName: '壽豐鄉', },
        { dataKey: 975, dataName: '鳳林鎮', },
        { dataKey: 976, dataName: '光復鄉', },
        { dataKey: 977, dataName: '豐濱鄉', },
        { dataKey: 978, dataName: '瑞穗鄉', },
        { dataKey: 979, dataName: '萬榮鄉', },
        { dataKey: 981, dataName: '玉里鎮', },
        { dataKey: 982, dataName: '卓溪鄉', },
        { dataKey: 983, dataName: '富里鄉', },
    ],
    '台東縣': [
        { dataKey: 950, dataName: '臺東市', },
        { dataKey: 951, dataName: '綠島鄉', },
        { dataKey: 952, dataName: '蘭嶼鄉', },
        { dataKey: 953, dataName: '延平鄉', },
        { dataKey: 954, dataName: '卑南鄉', },
        { dataKey: 955, dataName: '鹿野鄉', },
        { dataKey: 956, dataName: '關山鎮', },
        { dataKey: 957, dataName: '海端鄉', },
        { dataKey: 958, dataName: '池上鄉', },
        { dataKey: 959, dataName: '東河鄉', },
        { dataKey: 961, dataName: '成功鎮', },
        { dataKey: 962, dataName: '長濱鄉', },
        { dataKey: 963, dataName: '太麻里鄉', },
        { dataKey: 964, dataName: '金峰鄉', },
        { dataKey: 965, dataName: '大武鄉', },
        { dataKey: 966, dataName: '達仁鄉', },
    ],
    '澎湖縣': [
        { dataKey: 880, dataName: '馬公市', },
        { dataKey: 881, dataName: '西嶼鄉', },
        { dataKey: 882, dataName: '望安鄉', },
        { dataKey: 883, dataName: '七美鄉', },
        { dataKey: 884, dataName: '白沙鄉', },
        { dataKey: 885, dataName: '湖西鄉', },
    ],
    '金門縣': [
        { dataKey: 890, dataName: '金沙鎮', },
        { dataKey: 891, dataName: '金湖鎮', },
        { dataKey: 892, dataName: '金寧鄉', },
        { dataKey: 893, dataName: '金城鎮', },
        { dataKey: 894, dataName: '烈嶼鄉', },
        { dataKey: 896, dataName: '烏坵鄉', },
    ],
    '連江縣': [
        { dataKey: 209, dataName: '南竿鄉', },
        { dataKey: 210, dataName: '北竿鄉', },
        { dataKey: 211, dataName: '莒光鄉', },
        { dataKey: 212, dataName: '東引鄉', },
    ],
}





/***************
電話號碼檢核

1. 必填未填訊息改成「如無市話，請填寫手機號碼」
2. 抓出區碼自動帶「-」

※ telValidate的function一定要放在 blur簡單格式檢核 的上面（必須先觸發）
不然「如無市話請填寫手機號碼」會帶不進去
***************/
export function telValidate(th) {
    const value = th.value;

    // if (th.validity.valueMissing === true) {
    //     th.setCustomValidity('如無市話，請填寫手機號碼');
    // } else {
    //     th.setCustomValidity('');
    // }

    if (value.search('-') === -1) {
        let newValue;

        if (value.search('0826') === 0) {
            newValue = value.replace('0826', '0826-');
        } else if (value.search('0836') === 0) {
            newValue = value.replace('0836', '0836-');
        } else if (value.search('082') === 0) {
            newValue = value.replace('082', '082-');
        } else if (value.search('089') === 0) {
            newValue = value.replace('089', '089-');
        } else if (value.search('049') === 0) {
            newValue = value.replace('049', '049-');
        } else if (value.search('037') === 0) {
            newValue = value.replace('037', '037-');
        } else {
            newValue = value
                .replace(/^08/, '08-')
                .replace(/^07/, '07-')
                .replace(/^06/, '06-')
                .replace(/^05/, '05-')
                .replace(/^04/, '04-')
                .replace(/^03/, '03-')
                .replace(/^02/, '02-');
        }

        return newValue;
    } else {
        return value;
    }
}
