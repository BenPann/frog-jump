// fake 3->0
// 第1張->1
// 第2張->2
// 第3張->3
// fake 1->4
let slideIdx = 1;

// swipe
// const slides: any = document.querySelectorAll('.swiper-slide');
// const mycon: HTMLElement = document.querySelector('.swiper-container');
// const mydots: HTMLElement = document.querySelector('.swiper-dots');

function slideChange(idx: number, isTransition?: boolean) {
  const mycon: HTMLElement = document.querySelector('.swiper-container');
  if (isTransition === false) {
    mycon.style.transitionDuration = '';
  } else {
    mycon.style.transitionDuration = '0.3s';
  }

  mycon.style.transform = 'translate3d(-' + innerWidth * idx + 'px,0,0)';
  document.querySelector('.swiper-dot.actived').classList.remove('actived');

  const allDot = document.querySelectorAll('.swiper-dot');

  if (idx === mycon.children.length - 1) {
    setTimeout(() => {
      slideIdx = 1;
      mycon.style.transitionDuration = '';
      mycon.style.transform = 'translate3d(-' + innerWidth + 'px,0,0)';
      allDot[slideIdx - 1].classList.add('actived');
    }, 300);
  } else if (idx === 0) {
    setTimeout(() => {
      slideIdx = mycon.children.length - 2;
      mycon.style.transitionDuration = '';
      mycon.style.transform =
        'translate3d(-' + (mycon.children.length - 2) * innerWidth + 'px,0,0)';
      allDot[slideIdx - 1].classList.add('actived');
    }, 300);
  } else {
    allDot[slideIdx - 1].classList.add('actived');
  }
} // end slideChange

export function initSlides() {
  const slides: any = document.querySelectorAll('.swiper-slide');
  const mycon: HTMLElement = document.querySelector('.swiper-container');
  for (const slide of slides) {
    slide.press = false;
    // slide.lastX;
    // slide.lastY;
    // slide.movedX;
    // slide.movedY;

    slide.addEventListener('touchstart', function (e) {
      this.press = true;
      this.lastX = e.touches[0].clientX;
      this.lastY = e.touches[0].clientY;
      mycon.style.transitionDuration = '';
    });

    slide.addEventListener('touchleave', function () {
      this.press = false;
      slideChange(slideIdx);
    });

    slide.addEventListener('touchend', function () {
      this.press = false;

      if (Math.abs(this.movedX) > innerWidth / 4) {
        if (this.movedX < 0) {
          slideIdx += 1;
        } else {
          slideIdx -= 1;
        }
        slideChange(slideIdx);
      } else {
        slideChange(slideIdx);
      }
    });

    slide.addEventListener(
      'touchmove',
      function (e) {
        if (this.press === true) {
          this.movedX = e.touches[0].clientX - this.lastX;
          this.movedY = e.touches[0].clientY - this.lastY;
          if (Math.abs(this.movedY) > Math.abs(this.movedX)) {
            return;
          } else if (e.cancelable) {
            const nowposX = -(slideIdx * innerWidth) + this.movedX;
            mycon.style.transform = 'translate3d(' + nowposX + 'px,0,0)';
            e.preventDefault();
          }
        }
      },
      { passive: false }
    );
  } // end for
} // end initSlides

// menu
export function fillProject(th) {
  const menuPlaceholder: any = document.querySelector('#menuPlaceholder');
  const menuTrigger: any = document.querySelector('input[type="checkbox"]#menuTrigger');
  const line: HTMLElement = document.querySelector('#line');
  // 手機選取專案
  menuPlaceholder.textContent = th.value;
  menuTrigger.checked = false;

  // 電腦版nav橘色線拖動效果
  line.style.width = th.nextElementSibling.clientWidth + 'px';
  line.style.left = th.offsetLeft + 'px';
} // end fillProject

export function desktopSlide(direction: number) {
  slideIdx += direction;
  slideChange(slideIdx);
} // end desktopSlide

export function homePage() {

  const mycon: HTMLElement = document.querySelector('.swiper-container');
  const mydots: HTMLElement = document.querySelector('.swiper-dots');

  // 圖片預載優化
  const visualPictures = document.querySelectorAll('#visual picture');

  for (let i = 0; i < visualPictures.length; i++) {
    // 手機版
    const tempImgb = new Image(375, 630);
    tempImgb.src = 'assets/inc/default/img/demo/home' + [i + 1] + 'b.png';
    const realImgb = visualPictures[i].children[0];
    // 桌機版
    const tempImga = new Image(1440, 720);
    tempImga.src = 'assets/inc/default/img/demo/home' + [i + 1] + 'a.png';
    const realImga = visualPictures[i].children[1];

    aaa(tempImgb, realImgb);
    aaa(tempImga, realImga);
  } // end for

  function aaa(xxx: HTMLImageElement, zzz: any) {
    const id = window.setInterval(() => {
      if (xxx.complete === true) {
        zzz.src = xxx.src;
        zzz.srcset = xxx.src;
        clearInterval(id);
      }
    }, 1000);
  } // aaa

  function replaceSource(target) {
    // 假頂圖、假尾圖不做圖片預載
    const myimg = target.querySelector('img');
    const mysource = target.querySelector('source');
    const newimgsrc = myimg.src.replace('-min', '');

    myimg.src = newimgsrc;
    mysource.srcset = newimgsrc.replace('a.png', 'b.png');
  } // end replaceSource

  mycon.style.transform = 'translate3d(-' + innerWidth + 'px,0,0)';

  const firstSlide = mycon.firstElementChild.cloneNode(true);
  replaceSource(firstSlide);
  const lastSlide = mycon.lastElementChild.cloneNode(true);
  replaceSource(lastSlide);

  mycon.insertBefore(lastSlide, mycon.firstElementChild);
  mycon.insertBefore(firstSlide, mycon.lastChild);

  mycon.style.width = mycon.children.length + '00vw';

  for (let i = 0; i < mycon.children.length - 4; i++) {
    const mydot = mydots.lastElementChild.cloneNode(true);
    mydots.insertBefore(mydot, mydots.lastChild);
  }

  // 首頁的一屏高度不能用minheight所以自己算
  function mobileFixedHeight() {
    // 第一個條件：限小版
    // 第二個條件：限橫屏
    // 第三個條件：限手機畫面比圖小時（手雞比圖大例如w400×h1000）
    const header = document.querySelector('#header');
    const menu = document.querySelector('#menu');
    if (
      innerWidth < 481 &&
      innerHeight > innerWidth &&
      innerHeight - menu.clientHeight - header.clientHeight < mycon.children[0].clientHeight
    ) {
      mycon.style.height =
        innerHeight - header.clientHeight - menu.clientHeight + 'px';
    } else {
      mycon.style.height = '';
    }
  }
  mobileFixedHeight();

  window.addEventListener('resize', () => {
    slideChange(slideIdx, false);
    mobileFixedHeight();
  });
} // end homePage
