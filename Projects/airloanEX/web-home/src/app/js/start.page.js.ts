import { Cleave } from 'cleave.js';

// Must declare
declare const Cleave: any;

/***************
cleave.js格式檢核

1. 限輸數字（檔英文/注音/貼上）
2. 給數字套上格式
***************/

/*日期檢核 .cleave-date*/
export function initCleaveDate() {
  const cleaveDates: any = document.querySelectorAll('.cleave-date');
  for (const cleaveDate of cleaveDates) {
    const c = new Cleave(cleaveDate, {
      date: true,
      datePattern: ['Y', 'm', 'd'],
    });
  }
} // end initCleaveDate

/*小寫英文自動轉大寫 .cleave-upper*/
export function initCleaveUpper() {
  const cleaveUppers: any = document.querySelectorAll('.cleave-upper');
  for (const cleaveUpper of cleaveUppers) {
    const c = new Cleave(cleaveUpper, {
      uppercase: true,
      blocks: [9999],
    });
  }
} // end initCleaveUpper

/*僅輸入數字 .cleave-number*/
export function initCleaveNumber() {
  const cleaveNumbers: any = document.querySelectorAll('.cleave-number');
  for (const cleaveNumber of cleaveNumbers) {
    const c = new Cleave(cleaveNumber, {
      numericOnly: true,
      blocks: [9999],
    });
  }
} // end initCleaveNumber

/*僅輸入金額（會抓首碼是不是填0） .cleave-money*/
export function initCleaveMoney() {
  const cleaveMoneys: any = document.querySelectorAll('.cleave-money');
  for (const cleaveMoney of cleaveMoneys) {
    const c = new Cleave(cleaveMoney, {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
    });
  }
} // end initCleaveMoney

/***************
blur簡單格式檢核

1. blur後觸發
2. 檢核必填/簡單regex格式
3. 檢核input type
4. 錯誤訊息拿原生噴的

memo: checkbox/radio不用也不能做blur檢核
因為checkbox/radio沒勾的話「下一步」按鈕連案也不能案，所以不需要
***************/
export function initTextInputsBlur() {
  const textInputs: any = document.querySelectorAll('.form-item [required]:not([type="checkbox"]):not([type="radio"])');
  for (const textInput of textInputs) {
    textInput.addEventListener('blur', function () {
      this.classList.add('need-valid');
      this.nextElementSibling.textContent = this.validationMessage;
    });
  } // end for
} // end initTextInputsBlur


/***************
button由灰轉藍的效果
***************/
function checkValid(th: any) {
  const form = th.form;
  const button = form.querySelector('button.btn-submit');
  if (form.checkValidity() === true) {
    button.disabled = false;
  } else {
    button.disabled = true;
  }
}
export function initBlueButton() {
  const allInputs: any = document.querySelectorAll('.form-item [required]');
  for (const allInput of allInputs) {


    allInput.addEventListener('change', function () {
      const that = this;
      setTimeout(() => {
        checkValid(that); // IE下拉選單會有延遲問題
      }, 300);
    });

    allInput.addEventListener('input', function () {
      checkValid(this);
    });

    allInput.addEventListener('blur', function () {
      checkValid(this);
    });

  } // end for
} // end initBlueButton

/***************
Dialog Popup
***************/
// var contactScrollBarWidth=window.innerWidth-contact.clientWidth-side.clientWidth;
let nowOpenedDialog;
/*關閉Dialog：要關閉的組件套上.dialog-close*/
export function closeDialog() {
  const bodyWrapper: any = document.body.children[1];
  if (nowOpenedDialog) {
    nowOpenedDialog.classList.remove('dialog-show');
    const otherDialog = document.querySelector('.dialog-show');
    if (otherDialog) {
      nowOpenedDialog = otherDialog;
      return;
    } else {
      document.body.classList.remove('has-dialog');
      bodyWrapper.style.paddingRight = '';
    }
  } // end if
} // end closeDialog

export function closeAllDialog() {
  const bodyWrapper: any = document.body.children[1];
  document.body.classList.remove('has-dialog');
  bodyWrapper.style.paddingRight = '';
  const nowDialogs: any = document.querySelectorAll('.dialog-show');
  for (const nowDialog of nowDialogs) {
    nowDialog.classList.remove('dialog-show');
  }
} // end closeAllDialog

export function initCloseButtons() {
  const closeButtons: any = document.querySelectorAll('.dialog-close');
  for (const closeButton of closeButtons) {
    closeButton.addEventListener('click', closeDialog);
  }
} // end initCloseButtons

// 關閉全部
export function initAllCloseButtons() {
  const allCloseButtons: any = document.querySelectorAll('.dialog-close-all');
  for (const allCloseButton of allCloseButtons) {
    allCloseButton.addEventListener('click', closeAllDialog);
  }
} // end initAllCloseButtons

/*呼叫Dialog：套上onclick="showDialog('#dialog1')"*/
export function showDialog(target) {
  const bodyWrapper: any = document.body.children[1];
  const bodyScrollBarWidth = innerWidth - bodyWrapper.clientWidth; // body scroll bar
  nowOpenedDialog = document.querySelector(target);
  bodyWrapper.style.paddingRight = bodyScrollBarWidth + 'px';
  document.body.classList.add('has-dialog');
  nowOpenedDialog.classList.add('dialog-show');

  window.addEventListener('keydown', (e) => {
    if (e.keyCode === 27) {
      closeDialog();
    }
  });
} // end showDialog

var G_CHATBOT_STATUS = "close";

export function showChatBot(){
	
	if (G_CHATBOT_STATUS=="close"){
		//open it
		document.getElementById("SpiritAI").style.display = "block";
		//document.getElementById("talk-triangle-top").style.display = "block";
		//document.getElementById("talk-triangle-base").style.display = "block";
		G_CHATBOT_STATUS = "open";
	}else{
		
		if (G_CHATBOT_STATUS=="open"){
			//open it
			document.getElementById("SpiritAI").style.display = "none";
			//document.getElementById("talk-triangle-top").style.display = "none";
			//document.getElementById("talk-triangle-base").style.display = "none";
			G_CHATBOT_STATUS = "close";
		}		
		
	}
}


export function closeChatBot(){
	
	document.getElementById("SpiritAI").style.display = "none";
	//document.getElementById("talk-triangle-top").style.display = "none";
	//document.getElementById("talk-triangle-base").style.display = "none";
	G_CHATBOT_STATUS = "close";

}
