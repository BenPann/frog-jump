// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  turnOnRouterGuard: false,
  // 是否使用假的凱基的API(就是忽略會發電文的API)
  useFakeKgiApi: false,
  // 是否使用假的後端的API(就是呼叫後端的API都用mock取代)
  useFakeBackendApi: false,
  useFakeOtp: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
