---------------------------------------------------------------------------------------------------------------------
cleave.js格式檢核 
	start.page.js.ts

電話號碼檢核 
	person.page.js.ts

身分證末碼檢核 
	step4.page.js

blur簡單格式檢核 
	start.page.js.ts

button由灰轉藍的效果 
	start.page.js.ts

簡訊驗證碼 輸入完畢後自動focus到下一個 
	verify.page.js.ts

Dialog Popup
	start.page.js.ts
	confirm.page.js.ts

行政區下拉選單
	person.page.js.ts

選好縣市後自動帶入公司電話
	work.page.js.ts

輸入框候選清單 操作動態
	person.page.js.ts

email候選清單
	person.page.js.ts

公司名稱候選清單
	work.page.js.ts

LINE Browser 輸入框收回後頁面回不來的bug
	N/A

身分證檢查(長度、尾碼計算)
	step4.page.js.ts

禁止回上頁
	N/A

手機因為CSS 100vh不會減去上下方瀏覽器導覽頁的高度導致東西有一點被切掉，改用innerHeight處理
	N/A
---------------------------------------------------------------------------------------------------------------------
開發進程

新增如下，
home
apply-layout
apply-header
apply-footer
apply
	ng g module component/apply --module app --route apply

		C:\Users\CHARLES\git\stp\Projects\airloanEX>cd web-home

		C:\Users\CHARLES\git\stp\Projects\airloanEX\web-home>ng g module component/apply --module app --route apply
		CREATE src/app/component/apply/apply-routing.module.ts (340 bytes)
		CREATE src/app/component/apply/apply.module.ts (342 bytes)
		CREATE src/app/component/apply/apply.component.html (20 bytes)
		CREATE src/app/component/apply/apply.component.spec.ts (621 bytes)
		CREATE src/app/component/apply/apply.component.ts (265 bytes)
		CREATE src/app/component/apply/apply.component.css (0 bytes)
		UPDATE src/app/app-routing.module.ts (764 bytes)
start
	ng g component component/apply/component/start --module component/apply

assets/inc/default/css/font.css

移除 index.html 的 <link rel="stylesheet" href="assets/inc/default/css/style.css" />
重構 style.css 至 styles.scss，修正 css 的 url(assets/絕對路徑)

	/*其他字型、icon*/
	@import 'assets/inc/default/css/font.css';

	/*手機版*/
	@import url('assets/inc/default/css/mobile.css') screen and (max-width: 1000px);

經重構後，localhost:4200/apply/start 才能正常動作。

導入 cleave js
	https://www.npmjs.com/package/ngx-cleave-directive

	cd web-home
	npm i -S cleave.js ngx-cleave-directive
		C:\Users\CHARLES\git\stp\Projects\airloanEX>cd web-home

		C:\Users\CHARLES\git\stp\Projects\airloanEX\web-home>npm i -S cleave.js ngx-cleave-directive
		npm WARN karma-jasmine-html-reporter@1.5.4 requires a peer of jasmine-core@>=3.5 but none is installed. You must install peer dependencies yourself.
		npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.13 (node_modules\@angular\compiler-cli\node_modules\fsevents):
		npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.13: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})
		npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.13 (node_modules\watchpack-chokidar2\node_modules\fsevents):
		npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.13: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})
		npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.13 (node_modules\karma\node_modules\fsevents):
		npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.13: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})
		npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@2.1.3 (node_modules\fsevents):
		npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.1.3: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})
		npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.13 (node_modules\webpack-dev-server\node_modules\fsevents):
		npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.13: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})

		+ cleave.js@1.6.0
		+ ngx-cleave-directive@1.1.3
		added 2 packages from 2 contributors and audited 1095 packages in 11.425s

		22 packages are looking for funding
		  run `npm fund` for details

		found 13 vulnerabilities (5 low, 3 moderate, 5 high)
		  run `npm audit fix` to fix them, or `npm audit` for details

新增 apply-api.service.ts ApplyService
	C:\Users\CHARLES\git\stp\Projects\airloanEX\web-home>ng g service service/apply/apply-api
	CREATE src/app/service/apply/apply-api.service.spec.ts (368 bytes)
	CREATE src/app/service/apply/apply-api.service.ts (137 bytes)

新增 FakeBackendInterceptor.ts

於 app.module.ts 新增 HttpTokenInterceptor 及 fakeBackendProvider
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpTokenInterceptor,
      multi: true
    },
    fakeBackendProvider,

須新增 HttpClientModule,

新增 proxy.conf.json

新增 shared

新增 user-data.service.ts

npm install --save @angular/cdk

npm install --save bootstrap
npm install --save @ng-bootstrap/ng-bootstrap

npm install --save cropperjs
npm install --save angular-cropperjs

npm install --save angular-cleave