USE [AIRLOANDB]
GO
/****** Object:  Table [dbo].[ApiLog]    Script Date: 2020/4/30 上午 11:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApiLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[ApiName] [nvarchar](200) NULL,
	[Request] [nvarchar](max) NOT NULL,
	[Response] [nvarchar](max) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_KGI_ApiLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AuditData]    Script Date: 2020/4/30 上午 11:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditData](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[ProjectCode] [nvarchar](20) NULL,
	[CreditLine] [nvarchar](10) NULL,
	[IntrestRate] [nvarchar](20) NULL,
	[Rank] [varchar](50) NULL,
	[Score] [nvarchar](20) NULL,
	[ShowScore] [char](1) NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_AuditData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BreakPoint]    Script Date: 2020/4/30 上午 11:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BreakPoint](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[MainPage] [nvarchar](100) NULL,
	[SubPage] [nvarchar](100) NULL,
	[IsEnd] [char](1) NULL,
	[TempData] [nvarchar](max) NULL,
	[UTime] [datetime] NOT NULL,
	[ColName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_BreakPoint] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BrowseLog]    Script Date: 2020/4/30 上午 11:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BrowseLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[Page] [nvarchar](200) NOT NULL,
	[Action] [nvarchar](200) NOT NULL,
	[UTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CardMenu]    Script Date: 2020/4/30 上午 11:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CardMenu](
	[CardSerial] [int] NOT NULL,
	[IdNumber] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK__CardMenu__4E8CDEC3B3EF7BC2] PRIMARY KEY CLUSTERED 
(
	[CardSerial] ASC,
	[IdNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CaseAuth]    Script Date: 2020/4/30 上午 11:51:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CaseAuth](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[AuthType] [varchar](20) NOT NULL,
	[EntType] [nvarchar](50) NOT NULL,
	[MBCValid] [varchar](20) NOT NULL,
	[MBCCheckCode] [varchar](20) NOT NULL,
	[MBCMessage] [nvarchar](500) NOT NULL,
	[ChtAgree] [varchar](20) NOT NULL,
	[ChtAgreeTime] [datetime] NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_UserAuth] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CaseData]    Script Date: 2020/4/30 上午 11:51:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CaseData](
	[CaseNo] [nvarchar](50) NOT NULL,
	[CaseNoWeb] [nvarchar](50) NULL,
	[CaseNoAps] [nvarchar](50) NULL,
	[GmCardNo] [nvarchar](50) NULL,
	[Idno] [nvarchar](50) NULL,
	[Status] [char](2) NOT NULL,
	[ApsStatus] [nvarchar](2) NULL,
	[IsPreAudit] [char](1) NOT NULL,
	[ProductId] [nvarchar](50) NULL,
	[Occupation] [char](3) NULL,
	[customer_name] [nvarchar](20) NULL,
	[birthday] [nchar](8) NULL,
	[gender] [char](1) NULL,
	[house_city_code] [nvarchar](6) NULL,
	[house_address] [nvarchar](150) NULL,
	[house_tel_area] [nvarchar](4) NULL,
	[house_tel] [nvarchar](30) NULL,
	[education] [char](1) NULL,
	[marriage] [char](1) NULL,
	[corp_name] [nvarchar](100) NULL,
	[corp_city_code] [nvarchar](6) NULL,
	[corp_address] [nvarchar](150) NULL,
	[corp_tel_area] [nvarchar](4) NULL,
	[corp_tel] [nvarchar](30) NULL,
	[corp_tel_exten] [nvarchar](6) NULL,
	[corp_class] [char](1) NULL,
	[corp_type] [char](4) NULL,
	[title] [nvarchar](60) NULL,
	[on_board_date] [char](8) NULL,
	[yearly_income] [int] NULL,
	[estate_type] [char](1) NULL,
	[p_apy_amount] [int] NULL,
	[mobile_tel] [nvarchar](14) NULL,
	[p_period] [int] NULL,
	[p_purpose] [int] NULL,
	[p_purpose_name] [nvarchar](50) NULL,
	[email_address] [varchar](120) NULL,
	[ip_address] [varchar](20) NULL,
	[prj_code] [varchar](20) NULL,
	[case_no_est] [varchar](20) NULL,
	[ContactMe] [char](1) NULL,
	[ContactMeFlag] [char](1) NULL,
	[AddPhotoFlag] [char](1) NULL,
	[OccupatioFlag] [char](1) NULL,
	[ImportantCust] [char](1) NULL,
	[AMLFlag] [char](1) NULL,
	[UTime] [datetime] NOT NULL,
	[UserType] [char](1) NULL,
	[ResAddrZipCode] [nvarchar](6) NULL,
	[ResAddr] [nvarchar](200) NULL,
	[ResTelArea] [nvarchar](4) NULL,
	[ResTel] [nvarchar](30) NULL,
 CONSTRAINT [PK_CaseData] PRIMARY KEY CLUSTERED 
(
	[CaseNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CaseDoc]    Script Date: 2020/4/30 上午 11:51:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CaseDoc](
	[CaseNo] [nvarchar](50) NOT NULL,
	[idno] [nvarchar](50) NOT NULL,
	[PdfContent] [varchar](max) NOT NULL,
	[Image] [varchar](max) NOT NULL,
 CONSTRAINT [PK_CaseDoc] PRIMARY KEY CLUSTERED 
(
	[CaseNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CaseDocument]    Script Date: 2020/4/30 上午 11:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CaseDocument](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[DocumentType] [nvarchar](50) NOT NULL,
	[Content] [varbinary](max) NOT NULL,
	[Version] [nvarchar](50) NOT NULL,
	[Other] [nvarchar](max) NULL,
	[UpdateTime] [datetime] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_CaseDocument] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[DocumentType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CaseEnd]    Script Date: 2020/4/30 上午 11:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CaseEnd](
	[CaseNo] [nvarchar](50) NOT NULL,
	[EndReasonId] [nvarchar](50) NULL,
	[EndReason] [nvarchar](200) NULL,
	[EndSystem] [nvarchar](50) NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_CaseEnd] PRIMARY KEY CLUSTERED 
(
	[CaseNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CG_RouteProduct]    Script Date: 2020/4/30 上午 11:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CG_RouteProduct](
	[ChooseProductId] [nvarchar](max) NULL,
	[ChannelId] [varchar](20) NULL,
	[RouteID] [varchar](20) NULL,
	[RouteType] [char](1) NULL,
	[RouteURL] [nvarchar](max) NULL,
	[ChooseCount] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CG_SelectProduct_Pproduct]    Script Date: 2020/4/30 上午 11:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CG_SelectProduct_Pproduct](
	[CGProductID] [varchar](20) NOT NULL,
	[PProductType] [varchar](20) NOT NULL,
	[ChannelId] [varchar](20) NOT NULL,
	[Select_Default] [int] NOT NULL,
	[Required] [int] NOT NULL,
	[SORT] [varchar](20) NOT NULL,
 CONSTRAINT [PK_CG_SelectProduct_Pproduct] PRIMARY KEY CLUSTERED 
(
	[CGProductID] ASC,
	[PProductType] ASC,
	[ChannelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CG_SelectProduct_UserType]    Script Date: 2020/4/30 上午 11:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CG_SelectProduct_UserType](
	[UserTypeID] [varchar](20) NULL,
	[CGProductID] [varchar](20) NULL,
	[PProductType] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CG_SelectProductList]    Script Date: 2020/4/30 上午 11:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CG_SelectProductList](
	[CGProductID] [nvarchar](20) NOT NULL,
	[CGProductName] [nvarchar](50) NOT NULL,
	[CGProductTitle] [nvarchar](max) NOT NULL,
	[CGProductSubTitle] [nvarchar](max) NOT NULL,
	[CGProductDesc] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_CG_SelectProductList] PRIMARY KEY CLUSTERED 
(
	[CGProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CG_UserType]    Script Date: 2020/4/30 上午 11:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CG_UserType](
	[UserTypeID] [varchar](20) NOT NULL,
	[UserTypeName] [nvarchar](20) NOT NULL,
	[UserTypeDef] [nvarchar](max) NULL,
 CONSTRAINT [PK_CG_UserType] PRIMARY KEY CLUSTERED 
(
	[UserTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CHT_CreditRating]    Script Date: 2020/4/30 上午 11:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CHT_CreditRating](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[CustomerPhone] [nvarchar](20) NULL,
	[CustomerIdcard] [nvarchar](20) NULL,
	[Processyyymm] [nvarchar](20) NULL,
	[IsOldCustomer] [nvarchar](20) NULL,
	[CreditValue] [nvarchar](20) NULL,
	[PaymentBehaviorBeyond14] [nvarchar](20) NULL,
	[PaymentBehaviorBeyond30] [nvarchar](20) NULL,
	[PaymentMethod] [nvarchar](20) NULL,
	[IsSuspended] [nvarchar](20) NULL,
	[IsRevoked] [nvarchar](20) NULL,
	[IsRevoked6mon] [nvarchar](20) NULL,
	[DemandLetter] [nvarchar](20) NULL,
	[DebtRank] [nvarchar](20) NULL,
	[HasDebt] [nvarchar](20) NULL,
	[IsRiskCustomer] [nvarchar](20) NULL,
	[CustomerType] [nvarchar](20) NULL,
	[HasDesignatedManager] [nvarchar](20) NULL,
	[MaxTenureMonth] [nvarchar](20) NULL,
	[DataUsage] [nvarchar](20) NULL,
	[VoiceCallCnt] [nvarchar](20) NULL,
	[VoiceCallMin] [nvarchar](20) NULL,
	[VoSendTotNum] [nvarchar](20) NULL,
	[WorkCountry] [nvarchar](20) NULL,
	[HomeCountry] [nvarchar](20) NULL,
	[HasBadRecord] [nvarchar](20) NULL,
	[HasData] [nvarchar](20) NULL,
	[CreateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CHT_PreFill]    Script Date: 2020/4/30 上午 11:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CHT_PreFill](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Sex] [nvarchar](50) NULL,
	[Birthday] [nvarchar](50) NULL,
	[HomeAddress] [nvarchar](200) NULL,
	[Mobile] [nvarchar](50) NULL,
	[EMail] [nvarchar](200) NULL,
	[LocalPhone] [nvarchar](50) NULL,
	[BillAddress] [nvarchar](200) NULL,
	[CreateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Config]    Script Date: 2020/4/30 上午 11:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Config](
	[KeyName] [nvarchar](50) NOT NULL,
	[KeyValue] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_Config] PRIMARY KEY CLUSTERED 
(
	[KeyName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContractData]    Script Date: 2020/4/30 上午 11:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractData](
	[ConNo] [nvarchar](50) NOT NULL,
	[CaseNoWeb] [nvarchar](50) NULL,
	[CaseNoAps] [nvarchar](50) NULL,
	[Status] [char](2) NOT NULL,
	[ProductId] [nvarchar](50) NOT NULL,
	[Idno] [nvarchar](50) NOT NULL,
	[CustomerName] [nvarchar](50) NOT NULL,
	[DealPayDay] [datetime] NOT NULL,
	[FinalPayDay] [datetime] NOT NULL,
	[ContractDay] [datetime] NOT NULL,
	[DocAddr] [nvarchar](200) NOT NULL,
	[ReadDate] [datetime] NOT NULL,
	[Notify] [char](2) NOT NULL,
	[Court] [nvarchar](20) NOT NULL,
	[EMail] [nvarchar](200) NOT NULL,
	[DataUse] [char](2) NOT NULL,
	[ApsConstData] [nvarchar](max) NOT NULL,
	[IpAddress] [nvarchar](50) NOT NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractData] PRIMARY KEY CLUSTERED 
(
	[ConNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContractDoc]    Script Date: 2020/4/30 上午 11:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractDoc](
	[ConNo] [nvarchar](50) NOT NULL,
	[Version] [nvarchar](50) NULL,
	[UserSign] [nvarchar](max) NULL,
	[PreviewContent] [nvarchar](max) NULL,
	[PdfContent] [nvarchar](max) NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractDoc] PRIMARY KEY CLUSTERED 
(
	[ConNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContractDocPhoto]    Script Date: 2020/4/30 上午 11:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractDocPhoto](
	[ConNo] [nvarchar](50) NOT NULL,
	[Page] [int] NOT NULL,
	[Image] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_ContractDocPhoto] PRIMARY KEY CLUSTERED 
(
	[ConNo] ASC,
	[Page] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CreditCardGift]    Script Date: 2020/4/30 上午 11:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreditCardGift](
	[GiftSerial] [int] IDENTITY(1,1) NOT NULL,
	[GiftTitle] [nvarchar](50) NULL,
	[GiftNote] [nvarchar](max) NULL,
	[GiftCode] [nvarchar](50) NULL,
	[GiftImage] [nvarchar](max) NULL,
	[GiftStatus] [char](2) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CreateUser] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateUser] [nvarchar](50) NULL,
	[UpdateTime] [datetime] NULL,
	[ApproveUser] [nvarchar](50) NULL,
	[ApproveTime] [datetime] NULL,
	[SuspendUser] [nvarchar](50) NULL,
	[SuspendTime] [datetime] NULL,
 CONSTRAINT [PK_CreditCardGift] PRIMARY KEY CLUSTERED 
(
	[GiftSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CreditCardProduct]    Script Date: 2020/4/30 上午 11:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreditCardProduct](
	[CardSerial] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [nvarchar](50) NULL,
	[CardType] [nvarchar](50) NULL,
	[CardTitle] [nvarchar](50) NULL,
	[CardContent] [nvarchar](max) NULL,
	[CardImage] [nvarchar](max) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CardStatus] [char](2) NULL,
	[CreateUser] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateUser] [nvarchar](50) NULL,
	[UpdateTime] [datetime] NULL,
	[ApproveUser] [nvarchar](50) NULL,
	[ApproveTime] [datetime] NULL,
 CONSTRAINT [PK_CreditCardProduct] PRIMARY KEY CLUSTERED 
(
	[CardSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CreditCaseApprovalLog]    Script Date: 2020/4/30 上午 11:51:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreditCaseApprovalLog](
	[UniqId] [nvarchar](50) NOT NULL,
	[Idno] [nvarchar](50) NOT NULL,
	[ProductId] [nvarchar](50) NOT NULL,
	[CreditApsNo] [nvarchar](50) NOT NULL,
	[ProductName] [nvarchar](50) NOT NULL,
	[SupNo] [nvarchar](50) NOT NULL,
	[SupChtName] [nvarchar](50) NOT NULL,
	[Status] [varchar](5) NOT NULL,
	[StatusDesc] [nvarchar](50) NOT NULL,
	[ProdFlg] [nvarchar](100) NOT NULL,
	[SalesMarket] [nvarchar](100) NOT NULL,
	[ChtName] [nvarchar](50) NOT NULL,
	[AP00Time] [datetime] NULL,
	[ApprovedTime] [datetime] NULL,
	[CrediteLimit] [nvarchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_CreditCaseApprovalLog] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CreditCaseData]    Script Date: 2020/4/30 上午 11:51:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreditCaseData](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[CreditNoAps] [nvarchar](50) NULL,
	[Status] [char](2) NOT NULL,
	[ApsStatus] [varchar](1) NOT NULL,
	[Idno] [nvarchar](50) NOT NULL,
	[UserType] [char](1) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[ProductId] [nvarchar](50) NOT NULL,
	[ChtName] [nvarchar](50) NOT NULL,
	[EngName] [nvarchar](100) NULL,
	[Birthday] [nvarchar](50) NOT NULL,
	[Gender] [nvarchar](50) NOT NULL,
	[IdCardDate] [datetime] NOT NULL,
	[IdCardLocation] [nvarchar](50) NOT NULL,
	[IdCardCRecord] [char](1) NOT NULL,
	[ResAddrZipCode] [nvarchar](6) NOT NULL,
	[ResAddr] [nvarchar](200) NOT NULL,
	[CommAddrZipCode] [nvarchar](6) NOT NULL,
	[CommAddr] [nvarchar](200) NOT NULL,
	[HomeAddrZipCode] [nvarchar](6) NOT NULL,
	[HomeAddr] [nvarchar](200) NOT NULL,
	[BillAddrZipCode] [nvarchar](6) NOT NULL,
	[BillAddr] [nvarchar](200) NOT NULL,
	[SendCardAddrZipCode] [nvarchar](6) NOT NULL,
	[SendCardAddr] [nvarchar](200) NOT NULL,
	[Education] [char](1) NOT NULL,
	[Marriage] [char](1) NOT NULL,
	[EmailAddress] [varchar](120) NOT NULL,
	[ResTelArea] [nvarchar](4) NOT NULL,
	[ResTel] [nvarchar](30) NOT NULL,
	[HomeTelArea] [nvarchar](4) NOT NULL,
	[HomeTel] [nvarchar](30) NOT NULL,
	[HomeType] [char](1) NOT NULL,
	[HomeOwner] [char](1) NOT NULL,
	[CorpName] [nvarchar](100) NOT NULL,
	[CorpTelArea] [nvarchar](4) NOT NULL,
	[CorpTel] [nvarchar](30) NOT NULL,
	[CorpTelExten] [nvarchar](6) NOT NULL,
	[CorpAddrZipCode] [nvarchar](6) NULL,
	[CorpAddr] [nvarchar](200) NOT NULL,
	[Occupation] [char](3) NOT NULL,
	[CorpDepart] [nvarchar](50) NOT NULL,
	[JobTitle] [nvarchar](100) NOT NULL,
	[YearlyIncome] [int] NOT NULL,
	[OnBoardDate] [char](6) NOT NULL,
	[SendType] [char](1) NOT NULL,
	[FirstPresent] [nvarchar](50) NOT NULL,
	[IpAddress] [varchar](20) NULL,
	[DataUse] [char](2) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_CreditCaseData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CreditCaseDoc]    Script Date: 2020/4/30 上午 11:51:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreditCaseDoc](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[idno] [nvarchar](50) NOT NULL,
	[PdfContent] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CreditCaseOrignal]    Script Date: 2020/4/30 上午 11:51:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreditCaseOrignal](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[CreditNoAps] [nvarchar](50) NULL,
	[Status] [char](2) NOT NULL,
	[ApsStatus] [varchar](1) NOT NULL,
	[Idno] [nvarchar](50) NOT NULL,
	[UserType] [char](1) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[ProductId] [nvarchar](50) NOT NULL,
	[ChtName] [nvarchar](50) NOT NULL,
	[EngName] [nvarchar](100) NULL,
	[Birthday] [nvarchar](50) NOT NULL,
	[Gender] [nvarchar](50) NOT NULL,
	[IdCardDate] [datetime] NOT NULL,
	[IdCardLocation] [nvarchar](50) NOT NULL,
	[IdCardCRecord] [char](1) NOT NULL,
	[ResAddrZipCode] [nvarchar](6) NOT NULL,
	[ResAddr] [nvarchar](200) NOT NULL,
	[CommAddrZipCode] [nvarchar](6) NOT NULL,
	[CommAddr] [nvarchar](200) NOT NULL,
	[HomeAddrZipCode] [nvarchar](6) NOT NULL,
	[HomeAddr] [nvarchar](200) NOT NULL,
	[BillAddrZipCode] [nvarchar](6) NOT NULL,
	[BillAddr] [nvarchar](200) NOT NULL,
	[SendCardAddrZipCode] [nvarchar](6) NOT NULL,
	[SendCardAddr] [nvarchar](200) NOT NULL,
	[Education] [char](1) NOT NULL,
	[Marriage] [char](1) NOT NULL,
	[EmailAddress] [varchar](120) NOT NULL,
	[ResTelArea] [nvarchar](4) NOT NULL,
	[ResTel] [nvarchar](30) NOT NULL,
	[HomeTelArea] [nvarchar](4) NOT NULL,
	[HomeTel] [nvarchar](30) NOT NULL,
	[HomeType] [char](1) NOT NULL,
	[HomeOwner] [char](1) NOT NULL,
	[CorpName] [nvarchar](100) NOT NULL,
	[CorpTelArea] [nvarchar](4) NOT NULL,
	[CorpTel] [nvarchar](30) NOT NULL,
	[CorpTelExten] [nvarchar](6) NOT NULL,
	[CorpAddrZipCode] [nvarchar](6) NULL,
	[CorpAddr] [nvarchar](200) NOT NULL,
	[Occupation] [char](3) NOT NULL,
	[CorpDepart] [nvarchar](50) NOT NULL,
	[JobTitle] [nvarchar](100) NOT NULL,
	[YearlyIncome] [int] NOT NULL,
	[OnBoardDate] [char](6) NOT NULL,
	[SendType] [char](1) NOT NULL,
	[FirstPresent] [nvarchar](50) NOT NULL,
	[IpAddress] [varchar](20) NULL,
	[DataUse] [char](2) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_CreditCaseDataOrignal] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CreditCaseStaging]    Script Date: 2020/4/30 上午 11:51:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreditCaseStaging](
	[UniqId] [nvarchar](50) NULL,
	[IsCLCreditCard] [char](2) NULL,
	[IsCheckCLInstallment] [char](2) NULL,
	[InstallmentMonth] [int] NULL,
	[InstallmentAgreeTime] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CreditVerify]    Script Date: 2020/4/30 上午 11:51:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreditVerify](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[VerifyStatus] [char](1) NOT NULL,
	[VerifyType] [char](1) NOT NULL,
	[CardNo] [varchar](20) NOT NULL,
	[CardTime] [varchar](8) NOT NULL,
	[BankId] [varchar](10) NOT NULL,
	[AccountNo] [varchar](20) NOT NULL,
	[ErrorCount] [int] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[NCCCErrorCount] [int] NOT NULL,
	[Pcode2566ErrorCount] [int] NOT NULL,
 CONSTRAINT [PK_CreditVerify] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CS_ApiSetting]    Script Date: 2020/4/30 上午 11:51:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CS_ApiSetting](
	[ChannelId] [nvarchar](50) NOT NULL,
	[DomainUrl] [nvarchar](100) NOT NULL,
	[ApiUrl] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_CS_ApiSetting] PRIMARY KEY CLUSTERED 
(
	[ChannelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CS_AumData]    Script Date: 2020/4/30 上午 11:51:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CS_AumData](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[Currency] [varchar](20) NOT NULL,
	[CurrentStock] [nvarchar](50) NULL,
	[FinancingBalance] [nvarchar](50) NULL,
	[BorrowStockBalance] [nvarchar](50) NULL,
	[TotalFinancial] [nvarchar](50) NULL,
	[ExchangeRate] [varchar](50) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[StockBaseDate] [nvarchar](20) NOT NULL,
	[StockPriceDate] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_CS_AumData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[Currency] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CS_CommonData]    Script Date: 2020/4/30 上午 11:51:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CS_CommonData](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[CSType] [nvarchar](2) NOT NULL,
	[ChannelId] [nvarchar](50) NOT NULL,
	[ResultData] [nvarchar](max) NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_CS_CommonData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CS_PersonalData]    Script Date: 2020/4/30 上午 11:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CS_PersonalData](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[HasAum] [char](1) NOT NULL,
	[UpdateAum] [char](1) NOT NULL,
	[UUId] [nvarchar](50) NOT NULL,
	[Idno] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Birthday] [nvarchar](50) NOT NULL,
	[Mobile] [nvarchar](50) NULL,
	[EMail] [nvarchar](50) NULL,
	[CommAddrZipCode] [nvarchar](6) NULL,
	[CommAddress] [nvarchar](200) NULL,
	[ResTelArea] [nvarchar](4) NULL,
	[ResPhone] [nvarchar](50) NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_CS_PersonalData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CS_VerifyLog]    Script Date: 2020/4/30 上午 11:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CS_VerifyLog](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[Idno] [nvarchar](50) NOT NULL,
	[IpAddress] [varchar](20) NOT NULL,
	[Time] [nvarchar](50) NOT NULL,
	[MessageSerial] [nvarchar](50) NOT NULL,
	[ContractVersion] [nvarchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_CS_VerifyLog] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DataQual]    Script Date: 2020/4/30 上午 11:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataQual](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[EMail] [int] NOT NULL,
	[Addr] [int] NOT NULL,
	[Phone] [int] NOT NULL,
	[Education] [int] NOT NULL,
	[Marriage] [int] NOT NULL,
	[Estate] [int] NOT NULL,
	[CompanyName] [int] NOT NULL,
	[CompanyAddr] [int] NOT NULL,
	[CompanyPhone] [int] NOT NULL,
	[Title] [int] NOT NULL,
	[OnBoardDate] [int] NOT NULL,
	[YearInCome] [int] NOT NULL,
	[OCRCompanyName] [int] NOT NULL,
	[OCRCompanyAddr] [int] NOT NULL,
	[OCRCompanyZip] [int] NOT NULL,
	[OCRCompanyPhone] [int] NOT NULL,
	[OCRTitle] [int] NOT NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_DataQual] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DataQual2]    Script Date: 2020/4/30 上午 11:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataQual2](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[DataName] [nvarchar](50) NOT NULL,
	[Qual] [int] NOT NULL,
 CONSTRAINT [PK_DataQual2] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[DataName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DropdownData]    Script Date: 2020/4/30 上午 11:51:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DropdownData](
	[Name] [nvarchar](50) NOT NULL,
	[DataKey] [nvarchar](50) NOT NULL,
	[DataName] [nvarchar](50) NOT NULL,
	[Enable] [int] NOT NULL,
	[Sort] [int] NOT NULL,
 CONSTRAINT [PK_DropdownData] PRIMARY KEY CLUSTERED 
(
	[Name] ASC,
	[DataKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DropDownTransForm]    Script Date: 2020/4/30 上午 11:51:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DropDownTransForm](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[DropDownDataKey] [nvarchar](50) NOT NULL,
	[Credit] [nvarchar](50) NOT NULL,
	[Loan] [nvarchar](50) NOT NULL,
	[Eop] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_AOResult]    Script Date: 2020/4/30 上午 11:51:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_AOResult](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[Status] [varchar](1) NULL,
	[OpenfailMsg] [nvarchar](200) NULL,
	[StatusTw] [varchar](1) NULL,
	[SalaryAccountTw] [nvarchar](16) NULL,
	[PassBookTw] [varchar](1) NULL,
	[StatusFore] [varchar](1) NULL,
	[SalaryAccountFore] [nvarchar](16) NULL,
	[PassBookFore] [varchar](1) NULL,
	[TrustStatus] [varchar](1) NULL,
	[D3Message] [nvarchar](max) NULL,
	[D3Base64PDF] [varbinary](max) NOT NULL,
	[FailedImage] [nvarchar](max) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NULL,
	[RegNo] [nvarchar](100) NULL,
	[RegDate] [varchar](10) NULL,
	[OpenTimeTw] [datetime] NULL,
	[OpenTimeFore] [datetime] NULL,
	[OpenTimeTrust] [datetime] NULL,
 CONSTRAINT [PK_ED3_AOResult] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_ApiLog]    Script Date: 2020/4/30 上午 11:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_ApiLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](2) NOT NULL,
	[KeyInfo] [nvarchar](200) NULL,
	[ApiName] [nvarchar](200) NOT NULL,
	[Request] [nvarchar](max) NOT NULL,
	[Response] [nvarchar](max) NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ED3_ApiLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_BranchList]    Script Date: 2020/4/30 上午 11:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_BranchList](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[BranchID] [nvarchar](16) NOT NULL,
	[BranchName] [nvarchar](50) NOT NULL,
	[BranchAddrZipCode] [nvarchar](10) NOT NULL,
	[BranchAddrCity] [nvarchar](50) NOT NULL,
	[BranchAddrDist] [nvarchar](50) NOT NULL,
	[BranchAddr] [nvarchar](200) NOT NULL,
	[Enable] [varchar](1) NOT NULL,
	[Sort] [int] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ED3_BranchList] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_CaseData]    Script Date: 2020/4/30 上午 11:52:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_CaseData](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [varchar](2) NULL,
	[UserType] [varchar](1) NULL,
	[Status] [varchar](2) NULL,
	[Process] [varchar](1) NULL,
	[Idno] [nvarchar](20) NULL,
	[Birthday] [varchar](8) NULL,
	[Phone] [nvarchar](50) NULL,
	[IsTwTaxResident] [varchar](1) NULL,
	[PromoChannelID] [varchar](2) NULL,
	[PromoDepart] [nvarchar](50) NULL,
	[PromoMember] [nvarchar](50) NULL,
	[SetSecuritiesDelivery] [varchar](1) NULL,
	[SecuritiesAddr] [nvarchar](50) NULL,
	[SecuritiesAddrZipCode] [varchar](5) NULL,
	[SecuritiesCode] [varchar](5) NULL,
	[SecuritiesName] [nvarchar](50) NULL,
	[ProductId] [varchar](2) NULL,
	[ChtName] [nvarchar](50) NULL,
	[EngName] [nvarchar](100) NULL,
	[Gender] [varchar](1) NULL,
	[IdCardDate] [datetime] NULL,
	[IdCardLocation] [nvarchar](50) NULL,
	[IdCardRecord] [varchar](2) NULL,
	[ResAddrZipCode] [nvarchar](6) NULL,
	[ResAddrArea] [nvarchar](200) NULL,
	[ResAddr] [nvarchar](200) NULL,
	[CommAddrZipCode] [nvarchar](6) NULL,
	[CommAddrArea] [nvarchar](200) NULL,
	[CommAddr] [nvarchar](200) NULL,
	[BirthPlace] [nvarchar](200) NULL,
	[Education] [varchar](1) NULL,
	[Marriage] [varchar](1) NULL,
	[EmailAddress] [varchar](120) NULL,
	[SendType] [varchar](1) NULL,
	[ResTelArea] [nvarchar](4) NULL,
	[ResTel] [nvarchar](30) NULL,
	[HomeTelArea] [nvarchar](4) NULL,
	[HomeTel] [nvarchar](30) NULL,
	[EstateType] [varchar](1) NULL,
	[Purpose] [varchar](2) NULL,
	[BranchID] [varchar](3) NULL,
	[AuthorizeToallCorp] [char](1) NULL,
	[AutVer] [char](20) NULL,
	[Occupation] [nvarchar](50) NULL,
	[CorpName] [nvarchar](100) NULL,
	[CorpTelArea] [nvarchar](4) NULL,
	[CorpTel] [nvarchar](30) NULL,
	[CorpAddrZipCode] [nvarchar](6) NULL,
	[CorpAddrArea] [nvarchar](200) NULL,
	[CorpAddr] [nvarchar](200) NULL,
	[JobTitle] [nvarchar](100) NULL,
	[YearlyIncome] [int] NULL,
	[OnBoardDate] [char](10) NULL,
	[Injury] [varchar](1) NULL,
	[BreakPointPage] [varchar](200) NULL,
	[AccountType] [varchar](4) NULL,
	[SubCategory] [varchar](4) NULL,
	[AMLResult] [varchar](1) NULL,
	[IpAddress] [varchar](20) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateTime] [datetime] NULL,
	[SendedTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[RouteID] [varchar](20) NULL,
	[ChannelStartPoint] [datetime] NULL,
	[CLToken] [nvarchar](200) NULL,
	[IsPayer] [char](1) NOT NULL,
	[IsTrust] [char](1) NULL,
 CONSTRAINT [PK_ED3_CaseData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_Casedata_CGproduct]    Script Date: 2020/4/30 上午 11:52:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_Casedata_CGproduct](
	[UniqId] [nvarchar](50) NOT NULL,
	[MainProduct] [varchar](20) NOT NULL,
	[SubProduct] [nvarchar](max) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_CaseDataTerms]    Script Date: 2020/4/30 上午 11:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_CaseDataTerms](
	[UniqId] [nvarchar](50) NOT NULL,
	[TermSerial] [int] NOT NULL,
	[IsAgree] [varchar](1) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ED3_CaseDataTerms] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[TermSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_CL_Fake_Idno]    Script Date: 2020/4/30 上午 11:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_CL_Fake_Idno](
	[Real_ID] [nvarchar](50) NOT NULL,
	[Fake_ID] [nvarchar](50) NOT NULL,
	[CreateDateTime] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_CLCaseSts]    Script Date: 2020/4/30 上午 11:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_CLCaseSts](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[PdctSts] [nvarchar](2) NOT NULL,
	[CLStatus] [nchar](1) NULL,
	[CLTime] [datetime] NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NULL,
	[StsCode] [nvarchar](100) NULL,
	[StsDesc] [nvarchar](max) NULL,
 CONSTRAINT [PK_ED3_CLCaseSts] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_CLCif]    Script Date: 2020/4/30 上午 11:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_CLCif](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[MsgId] [nvarchar](20) NOT NULL,
	[UniqId] [nvarchar](50) NULL,
	[StsCode] [nvarchar](5) NULL,
	[StsDesc] [nvarchar](50) NULL,
	[ShortUrl] [nvarchar](50) NULL,
	[Entry] [nvarchar](50) NULL,
	[CaseId] [nvarchar](50) NULL,
	[ApplNo] [nvarchar](50) NULL,
	[ApplyPdc] [nchar](1) NULL,
	[ApplyPdcRePmt] [nchar](1) NULL,
	[ApplyPmtEndDt] [nvarchar](23) NULL,
	[MsgSendDt] [nvarchar](23) NULL,
	[Token] [nvarchar](50) NULL,
	[TokenStrDt] [nvarchar](10) NULL,
	[TokenEndDt] [nvarchar](10) NULL,
	[IdNo] [nvarchar](10) NULL,
	[ChtName] [nvarchar](20) NULL,
	[EngName] [nvarchar](20) NULL,
	[PyName] [nvarchar](20) NULL,
	[BirthDt] [nvarchar](10) NULL,
	[Email] [nvarchar](50) NULL,
	[Marriage] [nchar](1) NULL,
	[MobNo] [nvarchar](20) NULL,
	[HomeTel] [nvarchar](20) NULL,
	[ComTel] [nvarchar](20) NULL,
	[ResAddr] [nvarchar](100) NULL,
	[ComAddr] [nvarchar](100) NULL,
	[HomeAddr] [nvarchar](100) NULL,
	[ApplType] [nchar](1) NULL,
	[ChanId] [nvarchar](10) NULL,
	[PromoDepart] [nvarchar](10) NULL,
	[PromoMmbId] [nvarchar](20) NULL,
	[PromoMmbName] [nvarchar](20) NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ED3_CLCif] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_CLPayerLog]    Script Date: 2020/4/30 上午 11:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_CLPayerLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[PolicySerial] [int] NOT NULL,
	[PayerStatus] [nvarchar](10) NULL,
	[PayerFailMsg] [nvarchar](1000) NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ED3_CLPayerLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_CLPolicyInfo]    Script Date: 2020/4/30 上午 11:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_CLPolicyInfo](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[MsgId] [nvarchar](20) NULL,
	[UniqId] [nvarchar](50) NULL,
	[ActionType] [nchar](1) NULL,
	[ApplNo] [nvarchar](50) NULL,
	[EffDt] [nvarchar](10) NULL,
	[CashValue] [int] NULL,
	[LoanAmt] [int] NULL,
	[RePremiu] [int] NULL,
	[RePmtMtd] [nvarchar](10) NULL,
	[OriPmtMtd] [nvarchar](10) NULL,
	[PolicyImage] [varbinary](max) NULL,
	[CalDate] [nvarchar](10) NULL,
	[QryDate] [nvarchar](10) NULL,
	[ApplSts] [nchar](1) NULL,
	[ApplStsUpdDate] [nvarchar](10) NULL,
	[MsgDate] [nvarchar](10) NULL,
	[PolicySts] [nchar](1) NULL,
	[PolicyStsUpdDate] [nvarchar](10) NULL,
	[AumStatus] [nchar](1) NULL,
	[AumTime] [datetime] NULL,
	[AumUuid] [nvarchar](50) NULL,
	[PayerStatus] [nchar](1) NULL,
	[PayerTime] [datetime] NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NULL,
 CONSTRAINT [PK_ED3_CLPolicyInfo] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_EX_CreditCaseData]    Script Date: 2020/4/30 上午 11:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_EX_CreditCaseData](
	[UniqId] [nvarchar](50) NOT NULL,
	[AirloanUniqId] [nvarchar](50) NULL,
	[UserType] [char](1) NULL,
	[CreditProductId] [nvarchar](50) NULL,
	[IdCardCRecord] [char](1) NULL,
	[Occupation] [char](3) NULL,
	[Education] [char](1) NULL,
	[Marriage] [char](1) NULL,
	[SendType] [char](1) NULL,
	[JobTitle] [nvarchar](50) NULL,
	[FirstPresent] [varchar](50) NULL,
	[DataUse] [char](2) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[SendedTime] [datetime] NULL,
 CONSTRAINT [PK_ED3_EX_CreditCaseData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_IdentityVerification]    Script Date: 2020/4/30 上午 11:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_IdentityVerification](
	[UniqId] [nvarchar](50) NOT NULL,
	[VerifyStatus] [char](1) NULL,
	[VerifyType] [char](1) NULL,
	[CardNo] [varchar](20) NULL,
	[CardTime] [varchar](8) NULL,
	[BankId] [varchar](10) NULL,
	[AccountNo] [varchar](20) NULL,
	[BookingBranchId] [varchar](3) NULL,
	[BookingDate] [varchar](8) NULL,
	[BookingTime] [nvarchar](2) NULL,
	[ErrorCount] [int] NULL,
	[NCCCErrorCount] [int] NULL,
	[Pcode2566ErrorCount] [int] NULL,
	[ErrorMsg] [nvarchar](200) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[MyCard] [char](1) NULL,
 CONSTRAINT [PK_ED3_IdentityVerification] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_JobLog]    Script Date: 2020/4/30 上午 11:52:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_JobLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[KeyInfo] [nvarchar](200) NULL,
	[JobName] [nvarchar](50) NULL,
	[Level] [int] NULL,
	[Message] [nvarchar](max) NULL,
	[UTime] [datetime] NULL,
 CONSTRAINT [PK_ED3_JobLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_RejectLog]    Script Date: 2020/4/30 上午 11:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_RejectLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NULL,
	[Idno] [nvarchar](20) NOT NULL,
	[IpAddress] [varchar](20) NOT NULL,
	[Phone] [nvarchar](50) NULL,
	[EmailAddress] [varchar](120) NULL,
	[RejectCode] [varchar](2) NULL,
	[CreateTime] [datetime] NOT NULL,
	[AccountType] [varchar](4) NULL,
	[SubCategory] [varchar](4) NULL,
 CONSTRAINT [PK_ED3_RejectLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_UserPhoto]    Script Date: 2020/4/30 上午 11:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_UserPhoto](
	[UniqId] [nvarchar](50) NOT NULL,
	[SubSerial] [int] NOT NULL,
	[Status] [nvarchar](20) NULL,
	[PType] [int] NULL,
	[SType] [int] NULL,
	[ImageBig] [varbinary](max) NULL,
	[ImageSmall] [varbinary](max) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateTime] [datetime] NULL,
	[SendedTime] [datetime] NULL,
 CONSTRAINT [PK_ED3_UserPhoto] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[SubSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ED3_VerifyLog]    Script Date: 2020/4/30 上午 11:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ED3_VerifyLog](
	[UniqId] [nvarchar](50) NOT NULL,
	[VerifyType] [nvarchar](20) NOT NULL,
	[Count] [int] NOT NULL,
	[Request] [nvarchar](max) NULL,
	[Response] [nvarchar](max) NULL,
	[CheckCode] [nvarchar](20) NULL,
	[AuthCode] [nvarchar](20) NULL,
	[Other] [nvarchar](1000) NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PK_ED3_VerifyLog] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[VerifyType] ASC,
	[Count] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EntryData]    Script Date: 2020/4/30 上午 11:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EntryData](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[Entry] [nvarchar](100) NOT NULL,
	[Member] [nvarchar](100) NULL,
	[Process] [nvarchar](50) NULL,
	[Browser] [nvarchar](50) NULL,
	[Platform] [nvarchar](50) NULL,
	[OS] [nvarchar](50) NULL,
	[Other] [nvarchar](1024) NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EntryData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_AccountList]    Script Date: 2020/4/30 上午 11:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_AccountList](
	[UniqId] [nvarchar](50) NOT NULL,
	[Account] [nvarchar](20) NOT NULL,
	[AccountType] [nvarchar](20) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EOP_AccountList] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[Account] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_AOResult]    Script Date: 2020/4/30 上午 11:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_AOResult](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[Status] [varchar](1) NULL,
	[OpenfailMsg] [nvarchar](max) NULL,
	[StatusTw] [varchar](1) NULL,
	[SalaryAccountTw] [nvarchar](16) NULL,
	[PassBookTw] [varchar](1) NULL,
	[StatusFore] [varchar](1) NULL,
	[SalaryAccountFore] [nvarchar](16) NULL,
	[PassBookFore] [varchar](1) NULL,
	[TrustStatus] [varchar](1) NULL,
	[D3Message] [nvarchar](max) NULL,
	[D3Base64PDF] [nvarchar](max) NOT NULL,
	[FailedImage] [nvarchar](max) NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EOP_AOResult] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_ApiLog]    Script Date: 2020/4/30 上午 11:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_ApiLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](2) NOT NULL,
	[KeyInfo] [nvarchar](200) NULL,
	[ApiName] [nvarchar](200) NOT NULL,
	[Request] [nvarchar](max) NOT NULL,
	[Response] [nvarchar](max) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EOP_ApiLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_ApplyItem]    Script Date: 2020/4/30 上午 11:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_ApplyItem](
	[UniqId] [nvarchar](50) NOT NULL,
	[ItemId] [nvarchar](20) NOT NULL,
	[Comment] [nvarchar](200) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EOP_ApplyItem] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_CaseData]    Script Date: 2020/4/30 上午 11:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_CaseData](
	[UniqId] [nvarchar](50) NOT NULL,
	[Status] [varchar](2) NULL,
	[Type] [varchar](1) NULL,
	[Process] [varchar](20) NULL,
	[SalaryAccountTw] [varchar](16) NULL,
	[SalaryAccountFore] [varchar](16) NULL,
	[IsTwTaxResident] [varchar](1) NULL,
	[Phone] [nvarchar](50) NULL,
	[SetSecuritiesDelivery] [varchar](1) NULL,
	[SecuritiesAddrZipCode] [varchar](5) NULL,
	[SecuritiesAddr] [nvarchar](50) NULL,
	[SecuritiesCode] [varchar](5) NULL,
	[SecuritiesName] [nvarchar](50) NULL,
	[ProductId] [varchar](2) NULL,
	[ChtName] [nvarchar](50) NULL,
	[EngName] [nvarchar](100) NULL,
	[Idno] [nvarchar](20) NULL,
	[Birthday] [varchar](8) NULL,
	[Gender] [varchar](1) NULL,
	[IdCardDate] [datetime] NULL,
	[IdCardLocation] [nvarchar](50) NULL,
	[IdCardRecord] [varchar](2) NULL,
	[ResAddrZipCode] [nvarchar](6) NULL,
	[ResAddr] [nvarchar](200) NULL,
	[CommAddrZipCode] [nvarchar](6) NULL,
	[CommAddr] [nvarchar](200) NULL,
	[Education] [varchar](1) NULL,
	[Marriage] [varchar](1) NULL,
	[EmailAddress] [varchar](120) NULL,
	[ResTelArea] [nvarchar](4) NULL,
	[ResTel] [nvarchar](30) NULL,
	[HomeTelArea] [nvarchar](4) NULL,
	[HomeTel] [nvarchar](30) NULL,
	[EstateType] [varchar](1) NULL,
	[PassBookTw] [varchar](1) NULL,
	[PassBookFore] [varchar](1) NULL,
	[CorpName] [nvarchar](100) NULL,
	[CorpTelArea] [nvarchar](4) NULL,
	[CorpTel] [nvarchar](30) NULL,
	[CorpAddrZipCode] [nvarchar](6) NULL,
	[CorpAddr] [nvarchar](200) NULL,
	[Occupation] [varchar](10) NULL,
	[JobTitle] [nvarchar](100) NULL,
	[YearlyIncome] [int] NULL,
	[OnBoardDate] [char](10) NULL,
	[IpAddress] [varchar](20) NULL,
	[SendType] [varchar](1) NULL,
	[BreakPointPage] [varchar](200) NULL,
	[D3AccountType] [varchar](4) NULL,
	[D3SubCategory] [varchar](4) NULL,
	[AMLResult] [varchar](1) NULL,
	[DataUse] [varchar](1) NULL,
	[EOPStatus] [varchar](2) NULL,
	[TrustStatus] [varchar](1) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateTime] [datetime] NULL,
	[SendedTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[ReceiveResultTime] [datetime] NULL,
 CONSTRAINT [PK_EOP_CaseData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_CaseDataTerms]    Script Date: 2020/4/30 上午 11:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_CaseDataTerms](
	[UniqId] [nvarchar](50) NOT NULL,
	[TermSerial] [int] NOT NULL,
	[IsAgree] [varchar](1) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EOP_CaseDataTerms] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[TermSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_EX_ChtTeleFeeData]    Script Date: 2020/4/30 上午 11:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_EX_ChtTeleFeeData](
	[UniqId] [nvarchar](50) NOT NULL,
	[SubSerial] [int] NOT NULL,
	[Status] [varchar](2) NOT NULL,
	[CampCode] [nvarchar](50) NOT NULL,
	[UserNo] [nvarchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NULL,
	[SendedTime] [datetime] NULL,
 CONSTRAINT [PK_EOP_EX_ChtTeleFeeData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[SubSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_EX_CreditCaseData]    Script Date: 2020/4/30 上午 11:52:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_EX_CreditCaseData](
	[UniqId] [nvarchar](50) NOT NULL,
	[AirloanUniqId] [nvarchar](50) NULL,
	[Status] [char](2) NULL,
	[Idno] [nvarchar](50) NULL,
	[UserType] [char](1) NULL,
	[Phone] [nvarchar](50) NULL,
	[ProductId] [nvarchar](50) NULL,
	[ChtName] [nvarchar](50) NULL,
	[EngName] [nvarchar](100) NULL,
	[Birthday] [nvarchar](50) NULL,
	[Gender] [nvarchar](50) NULL,
	[IdCardDate] [datetime] NULL,
	[IdCardLocation] [nvarchar](50) NULL,
	[IdCardCRecord] [char](1) NULL,
	[ResAddrZipCode] [nvarchar](6) NULL,
	[ResAddr] [nvarchar](200) NULL,
	[CommAddrZipCode] [nvarchar](6) NULL,
	[CommAddr] [nvarchar](200) NULL,
	[HomeAddrZipCode] [nvarchar](6) NULL,
	[HomeAddr] [nvarchar](200) NULL,
	[Education] [char](1) NULL,
	[Marriage] [char](1) NULL,
	[EmailAddress] [varchar](120) NULL,
	[ResTelArea] [nvarchar](4) NULL,
	[ResTel] [nvarchar](30) NULL,
	[HomeTelArea] [nvarchar](4) NULL,
	[HomeTel] [nvarchar](30) NULL,
	[HomeType] [char](1) NULL,
	[HomeOwner] [char](1) NULL,
	[CorpName] [nvarchar](100) NULL,
	[CorpTelArea] [nvarchar](4) NULL,
	[CorpTel] [nvarchar](30) NULL,
	[CorpTelExten] [nvarchar](6) NULL,
	[CorpAddrZipCode] [nvarchar](6) NULL,
	[CorpAddr] [nvarchar](200) NULL,
	[Occupation] [char](3) NULL,
	[CorpDepart] [nvarchar](50) NULL,
	[JobTitle] [nvarchar](100) NULL,
	[YearlyIncome] [int] NULL,
	[OnBoardDate] [char](10) NULL,
	[SendType] [char](1) NULL,
	[FirstPresent] [nvarchar](50) NULL,
	[IpAddress] [varchar](20) NULL,
	[QuickHandle] [varchar](1) NULL,
	[DataUse] [char](2) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateTime] [datetime] NULL,
	[SendedTime] [datetime] NULL,
 CONSTRAINT [PK_EOP_EX_CreditCaseData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_EX_DealAccountData]    Script Date: 2020/4/30 上午 11:52:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_EX_DealAccountData](
	[UniqId] [nvarchar](50) NOT NULL,
	[SubSerial] [int] NOT NULL,
	[Status] [varchar](2) NOT NULL,
	[BankCode] [varchar](3) NOT NULL,
	[BankName] [nvarchar](50) NOT NULL,
	[BankAccount] [varchar](16) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NULL,
	[SendedTime] [datetime] NULL,
 CONSTRAINT [PK_EOP_EX_DealAccountData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[SubSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_EX_ElectricityFeeData]    Script Date: 2020/4/30 上午 11:52:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_EX_ElectricityFeeData](
	[UniqId] [nvarchar](50) NOT NULL,
	[SubSerial] [int] NOT NULL,
	[Status] [varchar](2) NOT NULL,
	[AreaCode] [nvarchar](50) NOT NULL,
	[AreaM] [nvarchar](50) NOT NULL,
	[UserNo] [nvarchar](50) NOT NULL,
	[BranchNo] [nvarchar](50) NOT NULL,
	[CheckNo] [nvarchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NULL,
	[SendedTime] [datetime] NULL,
 CONSTRAINT [PK_EOP_EX_ElectricityFeeData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[SubSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_EX_GasFeeData]    Script Date: 2020/4/30 上午 11:52:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_EX_GasFeeData](
	[UniqId] [nvarchar](50) NOT NULL,
	[SubSerial] [int] NOT NULL,
	[Status] [varchar](2) NOT NULL,
	[GasCompany] [varchar](50) NOT NULL,
	[GasName] [nvarchar](200) NOT NULL,
	[UserNo] [varchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NULL,
	[SendedTime] [datetime] NULL,
 CONSTRAINT [PK_EOP_EX_GasFeeData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[SubSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_EX_TaipeiWaterFeeData]    Script Date: 2020/4/30 上午 11:52:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_EX_TaipeiWaterFeeData](
	[UniqId] [nvarchar](50) NOT NULL,
	[SubSerial] [int] NOT NULL,
	[Status] [varchar](2) NOT NULL,
	[AreaB] [nvarchar](50) NOT NULL,
	[AreaM] [nvarchar](50) NOT NULL,
	[AccountFeeNo] [nvarchar](50) NOT NULL,
	[CheckNo] [nvarchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NULL,
	[SendedTime] [datetime] NULL,
 CONSTRAINT [PK_EOP_EX_TaipeiWaterFeeData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[SubSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_EX_TaiwanWaterFeeData]    Script Date: 2020/4/30 上午 11:52:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_EX_TaiwanWaterFeeData](
	[UniqId] [nvarchar](50) NOT NULL,
	[SubSerial] [int] NOT NULL,
	[Status] [varchar](2) NOT NULL,
	[AreaB] [nvarchar](50) NOT NULL,
	[AreaM] [nvarchar](50) NOT NULL,
	[AccountFeeNo] [nvarchar](50) NOT NULL,
	[CheckNo] [nvarchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NULL,
	[SendedTime] [datetime] NULL,
 CONSTRAINT [PK_EOP_EX_TaiwanWaterFeeData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[SubSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_IdentityVerification]    Script Date: 2020/4/30 上午 11:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_IdentityVerification](
	[UniqId] [nvarchar](50) NOT NULL,
	[VerifyStatus] [char](1) NOT NULL,
	[VerifyType] [char](1) NOT NULL,
	[CardNo] [varchar](20) NOT NULL,
	[CardTime] [varchar](8) NOT NULL,
	[BankId] [varchar](10) NOT NULL,
	[AccountNo] [varchar](20) NOT NULL,
	[ErrorCount] [int] NULL,
	[NcccErrorCount] [int] NULL,
	[Pcode2566ErrorCount] [int] NULL,
	[ErrorMsg] [nvarchar](max) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EOP_IdentityVerificatio] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_IdMapping]    Script Date: 2020/4/30 上午 11:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_IdMapping](
	[UniqId] [nvarchar](50) NOT NULL,
	[Type] [varchar](2) NOT NULL,
	[BatchId] [nvarchar](20) NOT NULL,
	[Idno] [varchar](20) NOT NULL,
	[Birthday] [nvarchar](20) NOT NULL,
	[Other] [nvarchar](max) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EOP_IdMapping] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_IPLog]    Script Date: 2020/4/30 上午 11:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_IPLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[IPAddress] [nvarchar](200) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EOP_IPLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_JobLog]    Script Date: 2020/4/30 上午 11:52:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_JobLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[KeyInfo] [nvarchar](200) NULL,
	[JobName] [nvarchar](50) NOT NULL,
	[Level] [int] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EOP_JobLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_Organization]    Script Date: 2020/4/30 上午 11:52:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_Organization](
	[UserId] [nvarchar](50) NOT NULL,
	[Minister] [nvarchar](50) NULL,
	[Manager] [nvarchar](50) NULL,
 CONSTRAINT [EOP_Organization_pk] PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_ShortUrl]    Script Date: 2020/4/30 上午 11:52:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_ShortUrl](
	[ShortUrl] [nvarchar](50) NOT NULL,
	[BatchId] [nvarchar](20) NOT NULL,
	[Status] [varchar](2) NOT NULL,
	[DepartArea] [nvarchar](50) NOT NULL,
	[DepartId] [nvarchar](50) NOT NULL,
	[DepartName] [nvarchar](50) NOT NULL,
	[CheckList] [char](1) NOT NULL,
	[CheckListName] [nvarchar](50) NULL,
	[BusinessDepart] [nvarchar](50) NOT NULL,
	[BusinessMember] [nvarchar](50) NOT NULL,
	[CorpID] [nvarchar](50) NOT NULL,
	[CorpName] [nvarchar](50) NOT NULL,
	[CorpTelArea] [nvarchar](50) NOT NULL,
	[CorpTel] [nvarchar](50) NOT NULL,
	[CorpAddrZipCode] [nvarchar](50) NOT NULL,
	[CorpAddr] [nvarchar](50) NOT NULL,
	[Occupation] [varchar](10) NOT NULL,
	[TaiwanPassbook] [nvarchar](50) NOT NULL,
	[ShortUrlStartTime] [datetime] NOT NULL,
	[ShortUrlEndTime] [datetime] NOT NULL,
	[PayDate1] [nvarchar](20) NOT NULL,
	[PayDate2] [nvarchar](20) NOT NULL,
	[TwdRate] [nvarchar](20) NOT NULL,
	[TwdCashWdraw] [nvarchar](4) NOT NULL,
	[TwdTranSfer] [nvarchar](4) NOT NULL,
	[TwdWtdtr] [nvarchar](4) NOT NULL,
	[ChkTwdOffer] [nvarchar](200) NULL,
	[ForeignOffer] [nvarchar](50) NOT NULL,
	[TrustOffer] [nvarchar](50) NOT NULL,
	[CreditProductId] [nvarchar](200) NOT NULL,
	[HouseLoanRate] [nvarchar](20) NOT NULL,
	[LoanRate] [nvarchar](20) NOT NULL,
	[RejectReason] [nvarchar](200) NULL,
	[CreateUser] [nvarchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateUser] [nvarchar](50) NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[CheckUser] [nvarchar](50) NULL,
	[CheckTime] [datetime] NULL,
	[ApproveUser] [nvarchar](50) NULL,
	[ApproveTime] [datetime] NULL,
	[BatchOwner] [nvarchar](50) NOT NULL,
	[BatchOwnerChgTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EOP_ShortUrl] PRIMARY KEY CLUSTERED 
(
	[ShortUrl] ASC,
	[BatchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_ShortUrlChangeLog]    Script Date: 2020/4/30 上午 11:52:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_ShortUrlChangeLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[ShortUrl] [nvarchar](50) NOT NULL,
	[ColumnName] [nvarchar](100) NOT NULL,
	[ColumnBefore] [nvarchar](50) NOT NULL,
	[ColumnAfter] [nvarchar](50) NOT NULL,
	[CreateUser] [nvarchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_ShortUrlChangeOwner]    Script Date: 2020/4/30 上午 11:52:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_ShortUrlChangeOwner](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[ShortUrl] [nvarchar](50) NOT NULL,
	[LastOwner] [nvarchar](100) NOT NULL,
	[NewOwner] [nvarchar](100) NOT NULL,
	[Status] [nvarchar](2) NOT NULL,
	[CreateUser] [nvarchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateUser] [nvarchar](50) NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[ApproveUser] [nvarchar](50) NOT NULL,
	[ApproveTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EOP_ShortUrlChangeOwner] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_ShortUrlCheckList]    Script Date: 2020/4/30 上午 11:52:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_ShortUrlCheckList](
	[ShortUrl] [nvarchar](50) NOT NULL,
	[Idno] [nvarchar](100) NOT NULL,
	[ChtName] [nvarchar](100) NOT NULL,
	[Phone] [nvarchar](10) NOT NULL,
	[EmailAddress] [nvarchar](50) NOT NULL,
	[CreateUser] [nvarchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EOP_ShortUrlCheckList] PRIMARY KEY CLUSTERED 
(
	[ShortUrl] ASC,
	[Idno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_UserPhoto]    Script Date: 2020/4/30 上午 11:52:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_UserPhoto](
	[UniqId] [nvarchar](50) NOT NULL,
	[SubSerial] [int] NOT NULL,
	[Status] [nvarchar](20) NOT NULL,
	[ProdId] [nvarchar](50) NULL,
	[PType] [int] NOT NULL,
	[SType] [int] NOT NULL,
	[ImageBig] [varbinary](max) NOT NULL,
	[ImageSmall] [varbinary](max) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[SendTime] [datetime] NULL,
 CONSTRAINT [PK_EOP_UserPhoto] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[SubSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EOP_VerifyLog]    Script Date: 2020/4/30 上午 11:52:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EOP_VerifyLog](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NULL,
	[VerifyType] [nvarchar](20) NOT NULL,
	[Count] [int] NOT NULL,
	[Request] [nvarchar](max) NOT NULL,
	[Response] [nvarchar](max) NOT NULL,
	[CheckCode] [nvarchar](20) NOT NULL,
	[AuthCode] [nvarchar](20) NOT NULL,
	[Other] [nvarchar](1000) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EOP_VerifyLog] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[VerifyType] ASC,
	[Count] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FastPass]    Script Date: 2020/4/30 上午 11:52:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FastPass](
	[CaseNo] [nvarchar](50) NOT NULL,
	[AML] [char](1) NOT NULL,
	[AddPhoto] [char](1) NOT NULL,
	[AMLRequest] [nvarchar](max) NOT NULL,
	[AMLResponse] [nvarchar](max) NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[DGTType] [char](1) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK__FastPass__6CAEAA276A800602] PRIMARY KEY CLUSTERED 
(
	[CaseNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FucoApiLog]    Script Date: 2020/4/30 上午 11:52:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FucoApiLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[ApiName] [nvarchar](50) NOT NULL,
	[Request] [nvarchar](max) NOT NULL,
	[Response] [nvarchar](max) NOT NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_KGI_FucoApiLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GiftList]    Script Date: 2020/4/30 上午 11:52:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiftList](
	[CardSerial] [int] NULL,
	[GiftSerial] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IndexClick]    Script Date: 2020/4/30 上午 11:52:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IndexClick](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[Entry] [nvarchar](100) NULL,
	[Browser] [nvarchar](50) NULL,
	[Platform] [nvarchar](50) NULL,
	[OS] [nvarchar](50) NULL,
	[Action] [nvarchar](50) NOT NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_IndexClick] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JobLog]    Script Date: 2020/4/30 上午 11:52:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[JobName] [nvarchar](50) NOT NULL,
	[Level] [int] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_JobLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KGI_PLoanProd]    Script Date: 2020/4/30 上午 11:52:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KGI_PLoanProd](
	[ProdKey] [nvarchar](50) NOT NULL,
	[ProdName] [nvarchar](50) NOT NULL,
	[Enable] [int] NOT NULL,
	[Sort] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProdKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KGI_Unit]    Script Date: 2020/4/30 上午 11:52:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KGI_Unit](
	[UnitKey] [nvarchar](50) NOT NULL,
	[UnitName] [nvarchar](50) NOT NULL,
	[Enable] [int] NOT NULL,
	[Sort] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UnitKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KGITerms]    Script Date: 2020/4/30 上午 11:52:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KGITerms](
	[Name] [nvarchar](50) NOT NULL,
	[Page] [nvarchar](50) NOT NULL,
	[Type] [varchar](1) NOT NULL,
	[Sort] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoanProduct]    Script Date: 2020/4/30 上午 11:52:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoanProduct](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](50) NOT NULL,
	[Version] [nvarchar](20) NOT NULL,
	[ListViewImage] [nvarchar](max) NOT NULL,
	[MainViewImage] [nvarchar](max) NOT NULL,
	[MainViewMobileImage] [nvarchar](max) NULL,
	[MainContent] [nvarchar](max) NOT NULL,
	[ApplyUrl] [nvarchar](300) NOT NULL,
	[Status] [nvarchar](20) NOT NULL,
	[AddedTime] [datetime] NOT NULL,
	[CreateUser] [nvarchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateUser] [nvarchar](50) NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[ApproveUser] [nvarchar](50) NULL,
	[ApproveTime] [datetime] NULL,
 CONSTRAINT [PK_LoanProduct] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoanProductDetail]    Script Date: 2020/4/30 上午 11:52:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoanProductDetail](
	[MainSerial] [int] NOT NULL,
	[DetailSerial] [int] NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[ViewImage] [nvarchar](max) NULL,
	[Title] [nvarchar](200) NULL,
	[Content] [nvarchar](300) NULL,
	[CreateUser] [nvarchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_LoanProductDetail] PRIMARY KEY CLUSTERED 
(
	[MainSerial] ASC,
	[DetailSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MailHistory]    Script Date: 2020/4/30 上午 11:52:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MailHistory](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[MailType] [nvarchar](20) NOT NULL,
	[Status] [nvarchar](20) NOT NULL,
	[ErrorMessage] [nvarchar](1000) NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Content] [nvarchar](max) NOT NULL,
	[TemplateId] [nvarchar](20) NOT NULL,
	[Attatchment] [varbinary](max) NOT NULL,
	[EMailAddress] [nvarchar](max) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_MailHistory] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MailHunterHistory]    Script Date: 2020/4/30 上午 11:52:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MailHunterHistory](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NULL,
	[TemplateId] [nvarchar](20) NOT NULL,
	[ChtName] [nvarchar](50) NULL,
	[Idno] [nvarchar](50) NULL,
	[EMailAddress] [nvarchar](max) NULL,
	[Status] [nvarchar](20) NULL,
	[ErrorMessage] [nvarchar](1000) NULL,
	[Title] [nvarchar](200) NULL,
	[Content] [nvarchar](max) NULL,
	[ProjectCode] [nvarchar](20) NULL,
	[OwnerID] [nvarchar](20) NULL,
	[ProductCode] [nvarchar](20) NULL,
	[Attatchment] [varbinary](max) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_MailHunterHistory] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MailTemplate]    Script Date: 2020/4/30 上午 11:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MailTemplate](
	[Type] [nvarchar](50) NOT NULL,
	[TagName] [nvarchar](50) NOT NULL,
	[Content] [nvarchar](max) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_MailTemplate] PRIMARY KEY CLUSTERED 
(
	[Type] ASC,
	[TagName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MBC_CL_Fake_Idno]    Script Date: 2020/4/30 上午 11:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MBC_CL_Fake_Idno](
	[Real_ID] [nvarchar](50) NOT NULL,
	[Fake_ID] [nvarchar](50) NOT NULL,
	[CreateDateTime] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MBC_WhichSystem]    Script Date: 2020/4/30 上午 11:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MBC_WhichSystem](
	[UniqId] [nvarchar](50) NOT NULL,
	[WhichSystem] [varchar](6) NOT NULL,
	[verNo] [varchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NULL,
 CONSTRAINT [PK_MBC_WhichSystem] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MemoData]    Script Date: 2020/4/30 上午 11:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemoData](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[MemoName] [nvarchar](50) NOT NULL,
	[Memo] [nvarchar](max) NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_MemoData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[MemoName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[menu_permission]    Script Date: 2020/4/30 上午 11:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[menu_permission](
	[menuId] [int] NOT NULL,
	[permissionId] [int] NOT NULL,
 CONSTRAINT [PK_menu_permission] PRIMARY KEY CLUSTERED 
(
	[menuId] ASC,
	[permissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[menus]    Script Date: 2020/4/30 上午 11:52:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[menus](
	[menuId] [int] NOT NULL,
	[menuName] [nvarchar](50) NOT NULL,
	[URL] [nvarchar](500) NULL,
	[parentId] [int] NULL,
	[itype] [int] NULL,
 CONSTRAINT [PK_menus] PRIMARY KEY CLUSTERED 
(
	[menuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[oplog]    Script Date: 2020/4/30 上午 11:52:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oplog](
	[opName] [nvarchar](50) NOT NULL,
	[opStatus] [nvarchar](50) NOT NULL,
	[opAccount] [nvarchar](20) NOT NULL,
	[http_client_ip] [nvarchar](50) NOT NULL,
	[http_x_forwarded_for] [nvarchar](50) NOT NULL,
	[remote_addr] [nvarchar](50) NOT NULL,
	[http_header] [nvarchar](max) NOT NULL,
	[opDescribe] [nvarchar](max) NOT NULL,
	[opClientDatetime] [datetime] NOT NULL,
	[modifyDatetime] [datetime] NOT NULL,
 CONSTRAINT [pk_oplog] PRIMARY KEY CLUSTERED 
(
	[opName] ASC,
	[opStatus] ASC,
	[opAccount] ASC,
	[opClientDatetime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OTPData]    Script Date: 2020/4/30 上午 11:52:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OTPData](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[PhoneNum] [nvarchar](20) NOT NULL,
	[OTPSource] [char](1) NOT NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_OTPData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[permissions]    Script Date: 2020/4/30 上午 11:52:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[permissions](
	[permissionId] [int] NOT NULL,
	[permissionName] [nvarchar](50) NOT NULL,
	[permissionKey] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[permissions_func]    Script Date: 2020/4/30 上午 11:52:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[permissions_func](
	[permissionId] [int] NOT NULL,
	[roleId] [int] NOT NULL,
	[depart] [int] NOT NULL,
	[hidden] [int] NOT NULL,
	[download] [int] NOT NULL,
 CONSTRAINT [PK_permission_func] PRIMARY KEY CLUSTERED 
(
	[permissionId] ASC,
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PLContactUs]    Script Date: 2020/4/30 上午 11:52:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PLContactUs](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[EMail] [nvarchar](100) NOT NULL,
	[ContactTime] [nvarchar](50) NOT NULL,
	[Note] [nvarchar](1000) NOT NULL,
	[ProductId] [nvarchar](50) NULL,
	[DepartId] [nvarchar](50) NULL,
	[Member] [nvarchar](50) NULL,
	[PrevPage] [nvarchar](50) NULL,
	[ContactFlag] [char](1) NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_PLContactUs_newTest] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PLExp]    Script Date: 2020/4/30 上午 11:52:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PLExp](
	[ExpNo] [nvarchar](50) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[Date] [nchar](8) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CaseNoWeb] [nvarchar](50) NOT NULL,
	[CaseNo] [nvarchar](50) NOT NULL,
	[QVer] [nvarchar](50) NOT NULL,
	[AnsList] [nvarchar](50) NOT NULL,
	[ProjectCode] [nvarchar](4) NOT NULL,
	[CreditLine] [int] NOT NULL,
	[IntrestRate] [decimal](6, 3) NOT NULL,
	[Rank] [int] NOT NULL,
	[Score] [decimal](8, 3) NOT NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_KGI_PLExp] PRIMARY KEY CLUSTERED 
(
	[ExpNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProcessPageTime]    Script Date: 2020/4/30 上午 11:52:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProcessPageTime](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[Page] [nvarchar](50) NOT NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ProcessPageTime] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProcessTime]    Script Date: 2020/4/30 上午 11:52:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProcessTime](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ProcessTime] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QR_ChannelDataUrl]    Script Date: 2020/4/30 上午 11:52:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QR_ChannelDataUrl](
	[ChannelId] [nvarchar](50) NOT NULL,
	[UrlNameType] [nvarchar](20) NOT NULL,
	[SubChannelId] [nvarchar](20) NOT NULL,
	[Enable] [char](1) NOT NULL,
	[UrlType] [char](1) NOT NULL,
	[Url] [nvarchar](2000) NULL,
	[UrlStartTime] [datetime] NULL,
	[UrlEndTime] [datetime] NULL,
 CONSTRAINT [PK_QR_ChannelDataUrl] PRIMARY KEY CLUSTERED 
(
	[ChannelId] ASC,
	[UrlNameType] ASC,
	[SubChannelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QR_ChannelDepartList]    Script Date: 2020/4/30 上午 11:52:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QR_ChannelDepartList](
	[ChannelId] [nvarchar](50) NOT NULL,
	[DepartId] [nvarchar](50) NOT NULL,
	[DepartName] [nvarchar](200) NOT NULL,
	[Enable] [char](1) NOT NULL,
	[EMail] [nvarchar](1000) NOT NULL,
	[TransDepart] [nvarchar](50) NOT NULL,
	[TransMember] [nvarchar](50) NOT NULL,
	[PromoDepart] [nvarchar](50) NOT NULL,
	[PromoMember] [nvarchar](50) NOT NULL,
	[AssignDepart] [nvarchar](50) NOT NULL,
	[PromoDepart1] [nvarchar](50) NOT NULL,
	[PromoMember1] [nvarchar](50) NOT NULL,
	[PromoDepart2] [nvarchar](50) NOT NULL,
	[PromoMember2] [nvarchar](50) NOT NULL,
	[AssignDepartExist] [nvarchar](50) NOT NULL,
	[AssignDepartNew] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_QR_ChannelDepartList] PRIMARY KEY CLUSTERED 
(
	[ChannelId] ASC,
	[DepartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QR_ChannelDepartListEOP]    Script Date: 2020/4/30 上午 11:52:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QR_ChannelDepartListEOP](
	[ChannelId] [nvarchar](50) NOT NULL,
	[DepartId] [nvarchar](50) NOT NULL,
	[PromoDepart1] [nvarchar](50) NOT NULL,
	[PromoMember1] [nvarchar](50) NOT NULL,
	[PromoDepart2] [nvarchar](50) NOT NULL,
	[PromoMember2] [nvarchar](50) NOT NULL,
	[AssignDepart] [nvarchar](50) NOT NULL,
	[EMail] [nvarchar](1000) NOT NULL,
 CONSTRAINT [PK_QR_ChannelDepartListEOP] PRIMARY KEY CLUSTERED 
(
	[ChannelId] ASC,
	[DepartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QR_ChannelList]    Script Date: 2020/4/30 上午 11:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QR_ChannelList](
	[ChannelId] [nvarchar](50) NOT NULL,
	[ChannelName] [nvarchar](200) NOT NULL,
	[ChannelType] [char](1) NOT NULL,
	[Enable] [char](1) NOT NULL,
	[ThemePath] [nvarchar](200) NULL,
	[DomainUrl] [nvarchar](200) NULL,
	[MBCFlag] [char](1) NOT NULL,
	[IdLogic] [char](1) NOT NULL,
	[IdLength] [varchar](3) NOT NULL,
	[Sort] [nvarchar](50) NOT NULL,
	[CanGen] [char](1) NOT NULL,
	[GetCif] [char](1) NULL,
	[UrlType] [char](1) NULL,
	[CifUrl] [nvarchar](2000) NULL,
	[MemoUrl] [nvarchar](2000) NULL,
	[GetCifStartTime] [datetime] NULL,
	[GetCifEndTime] [datetime] NULL,
	[DisPlayMode] [char](1) NULL,
 CONSTRAINT [PK_QR_ChannelList] PRIMARY KEY CLUSTERED 
(
	[ChannelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QR_ChannelPProductTypeMatch]    Script Date: 2020/4/30 上午 11:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QR_ChannelPProductTypeMatch](
	[ChannelId] [nvarchar](50) NOT NULL,
	[PProductType] [varchar](20) NOT NULL,
 CONSTRAINT [PK_QR_ChannelPProductTypeMatch] PRIMARY KEY CLUSTERED 
(
	[ChannelId] ASC,
	[PProductType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QR_NotCardPProductList]    Script Date: 2020/4/30 上午 11:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QR_NotCardPProductList](
	[PProductId] [nvarchar](20) NOT NULL,
	[PProductName] [nvarchar](200) NOT NULL,
	[Enable] [char](1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QR_PProductList]    Script Date: 2020/4/30 上午 11:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QR_PProductList](
	[PProductId] [nvarchar](20) NOT NULL,
	[PProductName] [nvarchar](200) NOT NULL,
	[Enable] [char](1) NOT NULL,
	[Sort] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_QR_PProductList] PRIMARY KEY CLUSTERED 
(
	[PProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QR_PProductTypeList]    Script Date: 2020/4/30 上午 11:52:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QR_PProductTypeList](
	[PProductType] [varchar](20) NOT NULL,
	[PProductTypeName] [nvarchar](200) NOT NULL,
	[Url] [nvarchar](200) NULL,
	[Enable] [char](1) NULL,
	[Sort] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_QR_PProductTypeList] PRIMARY KEY CLUSTERED 
(
	[PProductType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QR_PProductTypeProductMatch]    Script Date: 2020/4/30 上午 11:52:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QR_PProductTypeProductMatch](
	[PProductType] [varchar](20) NOT NULL,
	[PProductId] [varchar](20) NOT NULL,
 CONSTRAINT [PK_QR_PProductTypeProductMatch] PRIMARY KEY CLUSTERED 
(
	[PProductType] ASC,
	[PProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QR_PromoCase]    Script Date: 2020/4/30 上午 11:52:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QR_PromoCase](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[TransDepart] [nvarchar](50) NOT NULL,
	[TransMember] [nvarchar](50) NOT NULL,
	[PromoDepart] [nvarchar](50) NOT NULL,
	[PromoMember] [nvarchar](50) NOT NULL,
	[PromoDepart1] [nvarchar](50) NOT NULL,
	[PromoMember1] [nvarchar](50) NOT NULL,
	[PromoDepart2] [nvarchar](50) NOT NULL,
	[PromoMember2] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_QR_PromoCase] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QR_PromoCaseResult]    Script Date: 2020/4/30 上午 11:52:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QR_PromoCaseResult](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[PromoDepart1] [nvarchar](50) NOT NULL,
	[PromoMember1] [nvarchar](50) NOT NULL,
	[PromoDepart2] [nvarchar](50) NOT NULL,
	[PromoMember2] [nvarchar](50) NOT NULL,
	[AssignDepart] [nvarchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_QR_PromoCaseResult] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QR_PromoCaseUserInput]    Script Date: 2020/4/30 上午 11:52:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QR_PromoCaseUserInput](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[PromoDepart] [nvarchar](50) NOT NULL,
	[PromoMember] [nvarchar](50) NOT NULL,
	[ChannelId] [char](2) NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PK_QR_PromoCaseUserInput] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QR_ShortUrl]    Script Date: 2020/4/30 上午 11:52:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QR_ShortUrl](
	[ShortUrl] [nvarchar](50) NOT NULL,
	[ChannelId] [nvarchar](50) NOT NULL,
	[DepartId] [nvarchar](50) NOT NULL,
	[Member] [nvarchar](50) NOT NULL,
	[PProductType] [nvarchar](20) NOT NULL,
	[PProductId] [nvarchar](1000) NOT NULL,
	[Entry] [nvarchar](50) NOT NULL,
	[Count] [int] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_QR_ShortUrl_Entry] PRIMARY KEY CLUSTERED 
(
	[ShortUrl] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QR_ShortUrlCaseMatch]    Script Date: 2020/4/30 上午 11:52:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QR_ShortUrlCaseMatch](
	[ShortUrl] [nvarchar](50) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NULL,
 CONSTRAINT [PK_QR_ShortUrlCaseMatch] PRIMARY KEY CLUSTERED 
(
	[ShortUrl] ASC,
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RedirectMap]    Script Date: 2020/4/30 上午 11:52:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RedirectMap](
	[Name] [nvarchar](50) NOT NULL,
	[Url] [nvarchar](500) NOT NULL,
	[Description] [nvarchar](200) NULL,
 CONSTRAINT [PK_RedirectMap] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[role_permission]    Script Date: 2020/4/30 上午 11:52:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[role_permission](
	[roleId] [int] NOT NULL,
	[permissionId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[roles]    Script Date: 2020/4/30 上午 11:52:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[roles](
	[roleId] [int] NOT NULL,
	[roleName] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SN]    Script Date: 2020/4/30 上午 11:52:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SN](
	[Group] [nvarchar](50) NOT NULL,
	[Key] [nvarchar](50) NOT NULL,
	[Sn] [int] NOT NULL,
 CONSTRAINT [PK_SN] PRIMARY KEY CLUSTERED 
(
	[Group] ASC,
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Terms]    Script Date: 2020/4/30 上午 11:52:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Terms](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Version] [nvarchar](20) NULL,
	[Status] [nvarchar](20) NULL,
	[Content] [nvarchar](max) NULL,
	[AddedTime] [datetime] NULL,
	[CreateUser] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateUser] [nvarchar](50) NULL,
	[UpdateTime] [datetime] NULL,
	[ApproveUser] [nvarchar](50) NULL,
	[ApproveTime] [datetime] NULL,
 CONSTRAINT [PK_Terms] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Test]    Script Date: 2020/4/30 上午 11:52:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Test](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[MyId] [nchar](10) NULL,
	[Status] [nchar](10) NULL,
	[At] [datetime] NULL,
 CONSTRAINT [PK_Jimi] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_info]    Script Date: 2020/4/30 上午 11:52:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_info](
	[Userid] [nvarchar](50) NOT NULL,
	[ActionType] [char](10) NULL,
	[objectType] [char](10) NULL,
	[Role] [int] NULL,
	[UserPID] [nvarchar](50) NULL,
	[UserCName] [nvarchar](20) NULL,
	[UserEngName] [nvarchar](100) NULL,
	[UserDeptCode] [nvarchar](50) NULL,
	[UserDeptName] [nvarchar](20) NULL,
	[UserEmail] [varchar](120) NULL,
	[UserTitle] [nvarchar](20) NULL,
	[UserTel] [nvarchar](30) NULL,
	[UserTelEx] [nvarchar](6) NULL,
	[UserJoinDate] [datetime] NULL,
	[UserBirthDate] [nvarchar](50) NULL,
	[UserAccount] [nvarchar](50) NULL,
	[ExcuteUserId] [nvarchar](50) NULL,
 CONSTRAINT [user_info_pk] PRIMARY KEY NONCLUSTERED 
(
	[Userid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_role]    Script Date: 2020/4/30 上午 11:52:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_role](
	[userId] [nvarchar](50) NOT NULL,
	[roleId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserPhoto]    Script Date: 2020/4/30 上午 11:52:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPhoto](
	[UniqId] [nvarchar](50) NOT NULL,
	[Online] [char](1) NOT NULL,
	[SubSerial] [int] NOT NULL,
	[Status] [nvarchar](20) NOT NULL,
	[ProdType] [nvarchar](50) NOT NULL,
	[PType] [int] NOT NULL,
	[SType] [int] NOT NULL,
	[ProductId] [nvarchar](50) NOT NULL,
	[UnitId] [nvarchar](50) NOT NULL,
	[ImageBig] [varbinary](max) NOT NULL,
	[ImageSmall] [varbinary](max) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_UserPhoto] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[Online] ASC,
	[SubSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[users]    Script Date: 2020/4/30 上午 11:52:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[userId] [nvarchar](50) NOT NULL,
	[userName] [nvarchar](50) NOT NULL,
	[userPwd] [nvarchar](50) NOT NULL,
	[FailCnt] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VerifyLog]    Script Date: 2020/4/30 上午 11:52:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VerifyLog](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[VerifyType] [nvarchar](20) NOT NULL,
	[Count] [int] NOT NULL,
	[Request] [nvarchar](max) NOT NULL,
	[Response] [nvarchar](max) NOT NULL,
	[CheckCode] [nvarchar](20) NOT NULL,
	[AuthCode] [nvarchar](20) NOT NULL,
	[Other] [nvarchar](1000) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_VerifyLog] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[VerifyType] ASC,
	[Count] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WhiteList]    Script Date: 2020/4/30 上午 11:52:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WhiteList](
	[ID] [nvarchar](50) NOT NULL,
	[PType] [int] NOT NULL,
	[SType] [int] NOT NULL,
 CONSTRAINT [PK_WhiteList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[PType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BreakPoint] ADD  DEFAULT ('') FOR [ColName]
GO
ALTER TABLE [dbo].[CardMenu] ADD  CONSTRAINT [DF__CardMenu__CardSe__503BEA1C]  DEFAULT (NULL) FOR [CardSerial]
GO
ALTER TABLE [dbo].[CaseData] ADD  CONSTRAINT [DF__CaseData__gender__5535A963]  DEFAULT ((0)) FOR [gender]
GO
ALTER TABLE [dbo].[CreditVerify] ADD  DEFAULT ((0)) FOR [NCCCErrorCount]
GO
ALTER TABLE [dbo].[CreditVerify] ADD  DEFAULT ((0)) FOR [Pcode2566ErrorCount]
GO
ALTER TABLE [dbo].[CS_AumData] ADD  DEFAULT ('') FOR [StockBaseDate]
GO
ALTER TABLE [dbo].[CS_AumData] ADD  DEFAULT ('') FOR [StockPriceDate]
GO
ALTER TABLE [dbo].[ED3_CaseData] ADD  CONSTRAINT [DF_ED3_CaseData_IsPayer]  DEFAULT ((0)) FOR [IsPayer]
GO
ALTER TABLE [dbo].[EOP_EX_GasFeeData] ADD  CONSTRAINT [DF__EOP_EX_Ga__GasNa__24485945]  DEFAULT ('') FOR [GasName]
GO
ALTER TABLE [dbo].[oplog] ADD  DEFAULT (sysdatetime()) FOR [modifyDatetime]
GO
ALTER TABLE [dbo].[QR_ChannelDepartListEOP] ADD  DEFAULT ('') FOR [EMail]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WhiteList', @level2type=N'COLUMN',@level2name=N'ID'
GO
