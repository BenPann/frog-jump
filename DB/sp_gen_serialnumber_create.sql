USE [AIRLOANDB]
GO
/****** Object:  StoredProcedure [dbo].[sp_gen_serialnumber]    Script Date: 2020/6/17 下午 02:36:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_gen_serialnumber] 
	@Group nvarchar(50),
	@Key nvarchar(50)
AS
BEGIN
Declare @sn int
DECLARE @OutputTbl TABLE (ID INT)
IF NOT EXISTS (SELECT Sn FROM dbo.SN WHERE [Group] = @Group AND [Key] = @Key)
	BEGIN
		INSERT INTO dbo.SN VALUES ( @Group,@Key, 0 );
	END

UPDATE dbo.SN SET Sn = Sn+1
OUTPUT inserted.Sn into @OutputTbl(ID)
WHERE [Group] = @Group AND [Key] = @Key;
select @sn=ID from @OutputTbl;
return @sn;
END

