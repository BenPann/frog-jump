### 立約 APILog 資料表(ContractApiLog)

| PK   | NULL | IDENTITY | 欄位       | 型態          | 說明                    | 代碼說明 |
| ---- | ---- | -------- | ---------- | ------------- | ----------------------- | -------- |
| v    |      | v        | Serial     | int           | 序號, 由資料庫自動產生  |          |
|      |      |          | Type       | nvarchar(2)   | 1: 我方呼叫 2: 對方呼叫 |          |
|      |      |          | KeyInfo    | nvarchar(200) | 識別此交易是由誰發出去  | UniqId   |
|      |      |          | ApiName    | nvarchar(200) | 呼叫的 API 名稱         |          |
|      |      |          | Request    | nvarchar(max) | 呼叫 API 的需求電文     |          |
|      |      |          | Response   | nvarchar(max) | API 的回應電文          |          |
|      |      |          | StartTime  | datetime      | API 開始時間            |          |
|      |      |          | EndTime    | datetime      | API 結束時間            |          |
|      |      |          | CreateTime | datetime      | 新增時間                |          |
|      |      |          | Used       | bit           | 是否已完成交易          |          |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[ContractApiLog]    Script Date: 2020/9/3 下午 05:38:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ContractApiLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](2) NULL,
	[KeyInfo] [nvarchar](200) NULL,
	[ApiName] [nvarchar](200) NULL,
	[Request] [nvarchar](max) NOT NULL,
	[Response] [nvarchar](max) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Used] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_ContractApiLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
```
