### 白名單 Api Response(WhiteListData)

| PK  | NULL | 欄位     | 型態          | 說明                            | 代碼說明 |
| --- | ---- | -------- | ------------- | ------------------------------- | -------- |
| v   |      | ID       | nvarchar(50)  | 身分證字號                      |          |
| v   |      | PType    | int           | 主類型 - 流程(信用卡/貸款/立約) |          |
| v   |      | ApiName  | nvarchar(200) | API 名稱                        |          |
|     |      | Response | nvarchar(max) | API 回應內容                    |          |

#### 白名單 TYPE 定義

| PTYPE | 白名單內容 |
| ----- | ---------- |
| 0     | 信用卡     |
| 1     | 貸款       |
| 2     | 立約       |

## Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[WhiteListData]    Script Date: 2020/7/3 上午 11:44:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WhiteListData](
	[ID] [nvarchar](50) NOT NULL,
	[PType] [int] NOT NULL,
	[ApiName] [nvarchar](200) NOT NULL,
    [Response] [nvarchar](max) NULL,
 CONSTRAINT [PK_WhiteListData] PRIMARY KEY CLUSTERED
(
	[ID] ASC,
	[PType] ASC,
	[ApiName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
```
