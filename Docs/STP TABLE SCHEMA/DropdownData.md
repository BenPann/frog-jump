### 下拉選單資料表 (DropdownData)

| PK  | NULL | 欄位     | 型態         | 說明          | 代碼說明                                  |
| --- | ---- | -------- | ------------ | ------------- | ----------------------------------------- |
| v   |      | Name     | nvarchar(50) |               | 下拉選單群組名稱(如 Marriage)             |
| v   |      | DataKey  | nvarchar(50) | 選項的 key 值 | 要設定與要後送的 XML 中相同的欄位代碼一樣 |
|     |      | DataName | nvarchar(50) | 選項的中文名  |                                           |
|     |      | Enable   | int          | 是否顯示      | 0:不顯示 1:顯示                           |
|     |      | Sort     | int          | 排序用的值    |                                           |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[DropdownData]    Script Date: 2020/7/3 上午 11:28:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DropdownData](
	[Name] [nvarchar](50) NOT NULL,
	[DataKey] [nvarchar](50) NOT NULL,
	[DataName] [nvarchar](50) NOT NULL,
	[Enable] [int] NOT NULL,
	[Sort] [int] NOT NULL,
 CONSTRAINT [PK_DropdownData] PRIMARY KEY CLUSTERED
(
	[Name] ASC,
	[DataKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
```
