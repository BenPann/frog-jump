### 紀錄客戶勾選條款資料表(ContractMainTerms)

成功才寫

| PK   | NULL | 欄位       | 型態         | 說明            | 代碼說明               |
| ---- | ---- | ---------- | ------------ | --------------- | ---------------------- |
| v    |      | UniqId     | nvarchar(50) | 序號            |                        |
| v    |      | TermSerial | int          | 條款序號        | 跟 Terms Table 做 對應 |
|      |      | IsAgree    | varchar(1)   | Y:同意 N:不同意 |                        |
|      |      | CreateTime | datetime     | 新增時間        |                        |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[ContractMainTerms]    Script Date: 2020/7/7 上午 10:10:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractMainTerms](
	[UniqId] [nvarchar](50) NOT NULL,
	[TermSerial] [int] NOT NULL,
	[IsAgree] [varchar](1) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractMainTerms] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[TermSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
```
