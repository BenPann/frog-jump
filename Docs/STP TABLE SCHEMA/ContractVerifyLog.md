### 驗證紀錄 (ContractVerifyLog)

- 此 Table 為紀錄各個系統驗證結果以及驗證次數

| PK   | NULL | 欄位       | 型態           | 說明           | 代碼說明                                     |
| ---- | ---- | ---------- | -------------- | -------------- | -------------------------------------------- |
| v    |      | UniqId     | nvarchar(50)   | 案件編號       |                                              |
|      |      | UniqType   | char(2)        | 唯一代號類型   | 03:立約                                      |
| v    |      | VerifyType | nvarchar(20)   | 驗證類型       | Cht:中華電信 ,NCCC,PCode2566                 |
| v    |      | Count      | int            | 驗證次數       | 流水號數字 1,2,3 (錯誤次數就從 Count 去判斷) |
|      |      | Request    | nvarchar(max)  | 驗證傳遞參數   | 需求 RawData                                 |
|      |      | Response   | nvarchar(max)  | 驗證回應參數   | 回應 RawData                                 |
|      |      | CheckCode  | nvarchar(20)   | 回覆代碼       | 拆解 Response 的 CheckCode                   |
|      | v    | AuthCode   | nvarchar(20)   | 授權碼或驗證碼 | 目前是 NCCC 才有回應的授權碼                 |
|      | v    | Other      | nvarchar(1000) | 其他           | 如有其他欄位需求 先以 Json 方式塞進此欄位    |
|      | v    | Status     | nvarchar(2)    | 成功否         | 00 失敗 01 成功                              |
|      | v    | CreateTime | datetime       | 新增時間       | 就是驗證時間                                 |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[ContractVerifyLog]    Script Date: 2020/7/2 下午 01:37:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractVerifyLog](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL DEFAULT '03',
	[VerifyType] [nvarchar](20) NOT NULL,
	[Count] [int] NOT NULL,
	[Request] [nvarchar](max) NOT NULL,
	[Response] [nvarchar](max) NOT NULL,
	[CheckCode] [nvarchar](20) NOT NULL,
	[AuthCode] [nvarchar](20) NULL,
	[Other] [nvarchar](1000) NULL,
	[Status] [nvarchar](2) NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PK_ContractVerifyLog] PRIMARY KEY CLUSTERED
(
	[UniqId] ASC,
	[VerifyType] ASC,
	[Count] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
```
