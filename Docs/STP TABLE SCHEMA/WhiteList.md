### 白名單 (WhiteList)

| PK  | NULL | 欄位  | 型態         | 說明                            | 代碼說明                    |
| --- | ---- | ----- | ------------ | ------------------------------- | --------------------------- |
| v   |      | ID    | nvarchar(50) | 身分證字號                      |                             |
| v   |      | PType | int          | 主類型 - 流程(信用卡/貸款/立約) |                             |
|     |      | SType | int          | 次類型 - 戶況(新戶/既有戶)      | [詳見列表](#白名單type定義) |

#### 白名單 TYPE 定義

| PTYPE | STYPE | 白名單內容         |
| ----- | ----- | ------------------ |
| 0     | 1     | 信用卡(新戶)       |
| 0     | 2     | 信用卡(既有卡戶)   |
| 0     | 3     | 信用卡(既有貸款戶) |
| 1     | 1     | 貸款(預核)         |
| 1     | 2     | 貸款(非預核)       |
| 1     | 3     | 貸款(卡加貸的卡)   |
| 1     | 4     | 貸款(卡加貸的貸)   |
| 2     | 1     | 立約(新戶)         |
| 2     | 2     | 立約(既有戶)       |

## Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[WhiteList]    Script Date: 2020/7/3 上午 11:44:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WhiteList](
	[ID] [nvarchar](50) NOT NULL,
	[PType] [int] NOT NULL,
	[SType] [int] NOT NULL,
 CONSTRAINT [PK_WhiteList] PRIMARY KEY CLUSTERED
(
	[ID] ASC,
	[PType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WhiteList', @level2type=N'COLUMN',@level2name=N'ID'
GO
```
