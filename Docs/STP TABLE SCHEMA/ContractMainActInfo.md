### 立約帳戶驗身資料表(ContractMainActInfo)

| PK   | NULL | 欄位          | 型態         | 說明                                    | 代碼說明                                        |
| ---- | ---- | ------------- | ------------ | --------------------------------------- | ----------------------------------------------- |
| v    |      | UniqId        | nvarchar(50) | 立約編號                                | 範例: CONTYYYYMMDD00001                         |
|      | v    | AuthPhone     | nvarchar(15) | 登入驗身時的電話號碼                    | sendOTP 時就存 既有戶 GET_OTP_TEL               |
|      | v    | AuthBankId    | nvarchar(10) | 登入驗身時的 PCode2566 銀行代碼         | GET_DGT_CONST 來                                |
|      | v    | AuthBankName  | nvarchar(50) | 登入驗身時的 PCode2566 銀行名稱         | GET_DGT_CONST 來                                |
|      | v    | AuthAccountNo | nvarchar(50) | 登入驗身時的 PCode2566 銀行帳戶號碼     | GET_DGT_CONST 來                                |
|      | v    | AuthStatus    | nvarchar(2)  | 登入驗身時的 PCode2566/OTP 驗證狀態     | 0:驗證失敗 1:驗證成功 (含 AuthPhone 的驗證狀態) |
|      | v    | AuthErrCount  | int          | 登入驗身時的 PCode2566/OTP 驗證錯誤次數 | (含 AuthPhone 的錯誤次數)                       |
|      | v    | AuthTime      | datetime     | 登入驗身時的 PCode2566/OTP 驗證時間     |                                                 |
|      | v    | SignPhone     | nvarchar(15) | 最後立約時的電話號碼                    | 預設同 AuthPhone                                |
|      | v    | SignBankId    | nvarchar(10) | 授扣款時的 PCode2566 銀行代碼           | 預設同 AuthBankId                               |
|      | v    | SignBankName  | nvarchar(50) | 授扣款時的 PCode2566 銀行名稱           | 預設同 AuthBankName                             |
|      | v    | SignBranchId  | nvarchar(5)  | 授扣款時的 PCode2566 分行代碼           |                                                 |
|      | v    | SignAccountNo | nvarchar(50) | 授扣款時的 PCode2566 銀行帳戶號碼       | 預設同 AuthAccountNo                            |
|      | v    | SignStatus    | nvarchar(2)  | 授扣款時的 PCode2566/OTP 驗證狀態       | 0:驗證失敗 1:驗證成功 (含 SignPhone 的驗證狀態) |
|      | v    | SignErrCount  | int          | 授扣款時的 PCode2566/OTP 驗證錯誤次數   | (含 SignPhone 的錯誤次數)                       |
|      | v    | SignTime      | datetime     | 授扣款時的 PCode2566/OTP 驗證時間       |                                                 |
|      | v    | EddaBankId    | nvarchar(10) | 授扣款時的 eDDA 銀行代碼                | GET_DGT_CONST 來                                |
|      | v    | EddaBankName  | nvarchar(50) | 授扣款時的 eDDA 銀行名稱                | GET_DGT_CONST 來                                |
|      | v    | EddaAccountNo | nvarchar(50) | 授扣款時的 eDDA 銀行帳戶號碼            | GET_DGT_CONST 來                                |
|      | v    | EddaStatus    | nvarchar(2)  | 授扣款時的 eDDA 驗證狀態                | 0:驗證失敗 1:驗證成功                           |
|      | v    | EddaErrCount  | int          | 授扣款時的 eDDA 驗證錯誤次數            | 有總錯誤次數 可忽略不計 因此應該可以移除此欄    |
|      | v    | EddaTime      | datetime     | 驗證時間                                |                                                 |
|      |      | CreateTime    | datetime     | 建立時間                                |                                                 |
|      |      | UpdateTime    | datetime     | 更新時間                                |                                                 |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[ContractMainActInfo]    Script Date: 2020/7/2 下午 04:48:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractMainActInfo](
	[UniqId] [nvarchar](50) NOT NULL,
	[AuthPhone] [nvarchar](15) NULL,
	[AuthBankId] [nvarchar](10) NULL,
	[AuthBankName] [nvarchar](50) NULL,
	[AuthAccountNo] [nvarchar](50) NULL,
	[AuthStatus] [nvarchar](2) NULL,
	[AuthErrCount] [int] NULL,
	[AuthTime] [datetime] NULL,
	[SignPhone] [nvarchar](15) NULL,
	[SignBankId] [nvarchar](10) NULL,
	[SignBankName] [nvarchar](50) NULL,
	[SignBranchId] [nvarchar](5) NULL,
	[SignAccountNo] [nvarchar](50) NULL,
	[SignStatus] [nvarchar](2) NULL,
	[SignErrCount] [int] NULL,
	[SignTime] [datetime] NULL,
	[EddaBankId] [nvarchar](10) NULL,
	[EddaBankName] [nvarchar](50) NULL,
	[EddaAccountNo] [nvarchar](50) NULL,
	[EddaStatus] [nvarchar](2) NULL,
	[EddaErrCount] [int] NULL,
	[EddaTime] [datetime] NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractMainActInfo] PRIMARY KEY CLUSTERED
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
```
