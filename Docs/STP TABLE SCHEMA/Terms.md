### 條款內文(Terms)

| PK  | 欄位        | 型態          | 說明                                                   | 代碼說明 |
| --- | ----------- | ------------- | ------------------------------------------------------ | -------- |
| v   | Serial      | int           | 序號, 由資料庫自動產生                                 |          |
|     | Name        | nvarchar(50)  | 條款名稱                                               |          |
|     | Version     | nvarchar(20)  | 版本號                                                 |          |
|     | Status      | nvarchar(20)  | 條款狀態 (01:編輯中,02:送出待放行,03:放行完成,09:退件) |          |
|     | Content     | nvarchar(max) | 條款內文                                               |          |
|     | AddedTime   | datetime      | 上版時間                                               |          |
|     | CreateUser  | nvarchar(50)  | 新增的使用者                                           |          |
|     | CreateTime  | datetime      | 新增時間                                               |          |
|     | UpdateUser  | nvarchar(50)  | 更新的使用者                                           |          |
|     | UpdateTime  | datetime      | 更新時間                                               |          |
|     | ApproveUser | nvarchar(50)  | 放行的使用者                                           |          |
|     | ApproveTime | datetime      | 放行時間                                               |          |

## Note: 搜尋條件為 上版時間小於今天 排序 上版時間 放行時間 找第一筆

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[Terms]    Script Date: 2020/7/3 上午 11:38:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Terms](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Version] [nvarchar](20) NULL,
	[Status] [nvarchar](20) NULL,
	[Content] [nvarchar](max) NULL,
	[AddedTime] [datetime] NULL,
	[CreateUser] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateUser] [nvarchar](50) NULL,
	[UpdateTime] [datetime] NULL,
	[ApproveUser] [nvarchar](50) NULL,
	[ApproveTime] [datetime] NULL,
 CONSTRAINT [PK_Terms] PRIMARY KEY CLUSTERED
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
```
