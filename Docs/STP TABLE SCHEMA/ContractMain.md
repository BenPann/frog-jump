### 案件契約資料表(ContractMain)

| PK   | NULL | 欄位           | 型態          | 說明                   | 代碼說明                                                     |
| ---- | ---- | -------------- | ------------- | ---------------------- | ------------------------------------------------------------ |
| v    |      | UniqId         | nvarchar(50)  | 立約編號               | 範例: CONTYYYYMMDD00001                                      |
|      | v    | UserType       | nvarchar(2)   | 戶況(使用者使用者類別) | 0: 新戶 1:信用卡戶 2:存款戶 3:純貸款戶 (從 GET_CIF_INO 取得) |
|      | v    | CaseNoWeb      | nvarchar(50)  | 案件編號(APS)          | GET_DGT_CONST 來                                             |
|      | v    | GmCardNo       | nvarchar(50)  | 立約回傳編號           | GET_DGT_CONST 來                                             |
|      |      | Status         | nvarchar(2)   | 案件狀態               | [詳見列表](#Status案件主狀態)                                |
|      | v    | ProductId      | nvarchar(20)  | 產品代號               | 1、2、3 GET_DGT_CONST 代碼                                   |
|      |      | Idno           | nvarchar(50)  | 身分證字號             |                                                              |
|      | v    | ChtName        | nvarchar(50)  | 客戶姓名               | 新戶: GET_DGT_CONST 既有戶:GET_CIF_INFO                      |
|      |      | Birthday       | nvarchar(8)   | 生日                   | YYYYMMDD                                                     |
|      | v    | Email          | nvarchar(120) | 電子信箱               | GET_CIF_INFO 來                                              |
|      | v    | DealPayDay     | datetime      | APS 撥款日期           | GET_DGT_CONST 來                                             |
|      | v    | FinalPayDay    | datetime      | 最終撥款日期           | 計算過後的 PayDay                                            |
|      | v    | PayBankOption  | nvarchar(2)   | 本行或它行             | 0:本行 1:它行 null:它行                                      |
|      | v    | SordBankOption | nvarchar(2)   | 本行或它行             | 0:本行 1:它行 null:它行                                      |
|      | v    | ContractDay    | datetime      | 立約日期               | 立約當日                                                     |
|      | v    | PaymentDay     | nvarchar(2)   | 繳款日期               | 01~31                                                        |
|      | v    | DocAddr        | nvarchar(200) | 文件寄送地址           | 暫無                                                         |
|      | v    | ReadDate       | datetime      | 審閱日期               |                                                              |
|      | v    | Notify         | nvarchar(2)   | 告知方式               | 01:簡訊 02:書面                                              |
|      | v    | Court          | nvarchar(20)  | 法院名稱簡寫           |                                                              |
|      | v    | DataUse        | nvarchar(2)   | 同意資料使用(共銷)     | 01:同意 02:不同意                                            |
|      | v    | ApsConstData   | nvarchar(max) | APS 的立約資料         | GET_DGT_CONST                                                |
|      | v    | ResultHtml     | nvarchar(max) | nvarchar(max)          | 預覽用                                                       |
|      | v    | SignTime       | datetime      | 簽名時間               |                                                              |
|      | v    | SendTime       | datetime      | 送影像                 |                                                              |
|      |      | IpAddress      | nvarchar(20)  | IP                     |                                                              |
|      |      | CreateTime     | datetime      | 建立時間               |                                                              |
|      |      | UpateTime      | datetime      | 更新時間               |                                                              |











#### Status - 案件主狀態

| 代碼 | 中文                                |
| ---- | ----------------------------------- |
| 00   | 剛進入(沒立約資料)                  |
| 01   | 填寫中(OTP 驗身成功後)              |
| 02   | 填寫完(最後預覽 OTP 成功後)         |
| 03   | PDF 產生成功                        |
| 04   | PDF 產生失敗                        |
| 11   | 資料處理中(送影像 從 03 變成)       |
| 12   | 資料送出成功(送影像成功 從 11 變成) |
| 19   | 資料送出失敗(送影像失敗)            |
| 42   | pdf image xml upload                |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[ContractMain]    Script Date: 2020/7/2 下午 04:48:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractMain](
	[UniqId] [nvarchar](50) NOT NULL,
	[UserType] [nvarchar](2) NULL,
	[CaseNoWeb] [nvarchar](50) NULL,
	[GmCardNo] [nvarchar](50) NULL,
	[Status] [nvarchar](2) NOT NULL,
	[ProductId] [nvarchar](20) NULL,
	[Idno] [nvarchar](50) NOT NULL,
	[ChtName] [nvarchar](50) NULL,
	[Birthday] [varchar](8) NOT NULL,
	[Email] [nvarchar](120) NULL,
	[DealPayDay] [datetime] NULL,
	[FinalPayDay] [datetime] NULL,
    [PayBankOption] [nvarchar](2) NULL,
    [SordBankOption] [nvarchar](2) NULL,
	[ContractDay] [datetime] NULL,
	[PaymentDay] [nvarchar](2) NULL,
	[DocAddr] [nvarchar](200) NULL,
	[ReadDate] [datetime] NULL,
	[Notify] [nvarchar](2) NULL,
	[Court] [nvarchar](20) NULL,
	[DataUse] [nvarchar](2) NULL,
	[ApsConstData] [nvarchar](max) NULL,
	[ResultHtml] [nvarchar](max) NULL,
	[SignTime] [datetime] NULL,
	[SendTime] [datetime] NULL,
	[IpAddress] [nvarchar](20) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractMain] PRIMARY KEY CLUSTERED
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
```
