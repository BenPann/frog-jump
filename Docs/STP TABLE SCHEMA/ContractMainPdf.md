### 立約 PDF 資料表(ContractMainPdf)

成功才寫

| PK  | NULL | 欄位       | 型態           | 說明            | 代碼說明                |
| --- | ---- | ---------- | -------------- | --------------- | ----------------------- |
| v   |      | UniqID     | nvarchar(50)   | 立約編號        | 範例: CONTYYYYMMDD00001 |
|     | v    | Version    | nvarchar(50)   | 契約書版本      |                         |
|     |      | PdfContent | varbinary(max) | 契約書 PDF 內容 |                         |
|     |      | UTime      | datetime       | 建立時間        |                         |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[ContractMainPdf]    Script Date: 2020/7/2 下午 04:48:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractMainPdf](
    [UniqId] [nvarchar](50) NOT NULL,
    [Version] [nvarchar](50) NOT NULL,
    [PdfContent] [varbinary](max) NULL,
    [UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractMainPdf] PRIMARY KEY CLUSTERED
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
```
