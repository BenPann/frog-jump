### DB 設定檔 (Config)

- 此 Table 在系統內為唯讀

| PK  | NULL | 欄位        | 型態           | 說明       | 代碼說明 |
| --- | ---- | ----------- | -------------- | ---------- | -------- |
| v   |      | KeyName     | nvarchar(50)   | 設定檔名稱 |          |
|     |      | KeyValue    | nvarchar(1000) | 設定檔內容 |          |
|     | V    | Description | nvarchar(50)   | 設定檔描述 |          |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[Config]    Script Date: 2020/7/2 下午 02:03:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Config](
	[KeyName] [nvarchar](50) NOT NULL,
	[KeyValue] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_Config] PRIMARY KEY CLUSTERED
(
	[KeyName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
```
