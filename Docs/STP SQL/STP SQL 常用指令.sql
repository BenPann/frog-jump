USE [AIRLOANDB]
-- -------------------------------------------------------------------------------------
-- 172.31.2.36,2012
-- apuser
-- -------------------------------------------------------------------------------------
-- 開戶 eD3
select * from ED3_ApiLog;
select * from EOP_IPLog;
--

select * from EntryData;
select * from ContractData;
select top 10 * from ContractDoc;
select top 10 * from BrowseLog;
select top 10 * from APILog;
-- select * from CurrentLogin;
-- -------------------------------------------------------------------------------------
-- 條款一覽表
 select kgi.Name as termName ,substring(dd.DataName,charindex('-',dd.DataName)+1,len(dd.DataName)) as termTitle,kgi.Type
  from KGITerms kgi inner join DropdownData dd
  on kgi.Name = dd.DataKey and dd.Name = 'terms'
  -- where kgi.Page = 'ed3-init' 
  order by kgi.Type, kgi.Sort;

-- 頁面條款對應表
select * from KGITerms kgi order by kgi.Page, kgi.Type, kgi.Sort;
select * from KGITerms kgi where Page like '%fillData3' order by kgi.Page, kgi.Type, kgi.Sort;
select * from KGITerms kgi where Name = 'EOPAuthDataUsing' order by kgi.Page, kgi.Type, kgi.Sort; -- 共同行銷條款
-- 條款名稱
select * from DropdownData dd where dd.Name = 'terms';
-- and dd.DataKey = 'EOPPersonalDataUsing'; -- ed3-init
select * from DropdownData dd where dd.Name = 'terms' and dd.DataKey = 'EOPAuthDataUsing'; -- 共同行銷條款
-- airloanEX & ed3 已知條款
select * from Terms where Name in (
'ebankingDealItems', -- 電話/網路/行動銀行約定條款
'EOPPersonalDataUsing', -- 蒐集、處理及利用個人資料告知書
'pcode2566_creditcard_bank_list' -- 存款帳戶可驗證金融機構查詢
);

select * from ED3_CaseDataTerms;
-- -------------------------------------------------------------------------------------
-- airloanEX
-- "con-"
select * from KGITerms kgi where Page like 'con%' order by kgi.Page, kgi.Type, kgi.Sort;

-- select * from 
select * from EntryData;
-- select * from ContractData; -- 主檔，由ContractMain取代之
select * from ContractDoc;
select top 100 * from BrowseLog where Page like '/airloanEX%' order by UTime desc;

-- airloanEX TABLE
select * from ContractAPILog;
-- 主檔 ContractMain
select * from ContractMain order by CreateTime desc;
select * from CaseAuth;
select * from CreditVerify;

select * from ContractMainActInfo order by Creatime desc;
-- select * from ED3_IdentityVerification;

select * from ContractMainPdf;
select * from ContractMainTerms;

select * from ContractVerifyLog order by CreateTime desc;
-- select * from ED3_VerifyLog;

-- select * from EntryData;
select * from EntryDataDetail;
select * from WhiteList;
select * from WhiteListData;

-- WhiteListData
select * from WhiteList w left outer join WhiteListData d on w.ID = d.ID and w.PType = d.PType;

-- -------------------------------------------------------------------------------------

select * from config where KeyName in ('ED3VerifyBankAcct', 'ED3VerifyCreditCard', 'ED3VerifyOffline', 'ED3VerifyNextTime');

select * from DropdownData;
-- -------------------------------------------------------------------------------------
-- 舊專案 PL
-- ContractData
-- select top 10 * from ContractData; -- 主檔，由ContractMain取代之
select ApsConstData from ContractData; -- 主檔，由ContractMain取代之
select readdate, notify, court from ContractData;
select distinct court from ContractData;
select distinct notify from ContractData;
-- PDF
select top 1 * from ContractDoc where PdfContent is not null and UserSign = '';

select * from CaseDocument;

select * from CaseAuth;
select * from CreditVerify;

select * from CS_CommonData;
select * from ContractDocPhoto;

select * from Config where KeyName like 'PDF.Contract.%';

-- oracle
select * from lbdate;
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
-- airloanEX PDF
select * from ContractDoc where ConNo in ('CONT2020072900007') order by UTime desc;
select * from CaseDocument where UniqId in ('CONT2020072900007') order by CreateTime desc;
select * from CaseAuth order by CreateTime desc;
select * from CreditVerify order by CreateTime desc;
-- -------------------------------------------------------------------------------------

-- Most frequently use SQL
select * from ContractMain order by CreateTime desc;
select * from ContractMain where UniqId = 'CONT2020072900007';
select * from ContractDoc where ConNo in ('CONT2020072900007') order by UTime desc;
select * from CaseDocument where UniqId in ('CONT2020072900007') order by CreateTime desc;
-- -------------------------------------------------------------------------------------
-- PDF的圖片及xml產製及上傳
select top 20 * from JobLog where Message like '%con2020%';
-- -------------------------------------------------------------------------------------
select * from ContractMain where UniqId = 'CONT2020080400017';
-- PaymentDay, DocAddr, SignTime, SendTime
-- -------------------------------------------------------------------------------------
select UserId, process, BranchID from ed3_casedata where BranchID is not null;
select * from CreditCaseData;
-- ContractDataDao.queryWaitSendContract()
SELECT con.*,cd.CaseNo,cd.prj_code FROM ContractData con 
LEFT JOIN CaseData cd on ( con.CaseNoWeb = cd.CaseNoWeb and cd.Status <> '00' and cd.CaseNoWeb <> '' );

select top 1 * from contractdata;
select top 3 * from caseData;
-- -------------------------------------------------------------------------------------
-- Dataname=F055, Name=CL.CasePriority, DataKey=AHC
SELECT DataName, Name, DataKey FROM DropdownData where Name = 'CL.CasePriority';

select * from contractData;
select * from caseData;

select * from UserPhoto;
-- -------------------------------------------------------------------------------------
-- 分行別
select * from QR_ChannelDepartList where ChannelId = 'KB';-- and DepartId = '8220';
-- -------------------------------------------------------------------------------------
select * from dropdowndata where name = 'branchIdMapping';
select * from contractmain order by createTime desc;
-- CONT2020080600005
select * from contractmain where uniqId = 'CONT2020080600005';
select * from contractdoc where conno = 'CONT2020080600005';
select top 1 * from contractDoc where pdfcontent is not null;
select * from jobLog where Serial = 8583446 order by UTime desc; -- Serial=8583446, uniqId=CONT2020080600005
select * from jobLog order by UTime desc;
-- message like '%查無申請書無法上傳%' order by UTime desc;
-- -------------------------------------------------------------------------------------
select * from config where keyname='PDF.Contract.Loan.TemplateFilePath';
-- -------------------------------------------------------------------------------------
-- 案件契約ConNo:CONT2020080700003打包後送XML檔名:020200807140039739CONT2020080700003.xml XML資訊：<?xml version='1.0' encoding='UTF-8'?><Orbit><machineIP>10.86.19.232</machineIP><machineName>mweb</machineName><subcaseType></subcaseType><uuid></uuid><branchID>8220</branchID><userID>9999</userID><userName>webuser</userName><createTime>2020/08/07 14:00:39</createTime><email>charles.yen@frog-jump.com</email><notes></notes><attachment>CONT2020080700003.pdf</attachment><parentUUID></parentUUID><productTypeID></productTypeID><cMarketingProgramId></cMarketingProgramId><cSalesUnit></cSalesUnit><cSalesId></cSalesId><mApplyAmount></mApplyAmount><cReceiveBranch></cReceiveBranch><referralUnit></referralUnit><referralMemberCode></referralMemberCode><applicant></applicant><mobileNum></mobileNum><highRisk></highRisk><jobType></jobType><scanFiles><row fileName="CONT2020080700003_001.jpg" caseId="1" FMID="A001" covertPage="A001" productType="P003" branchType="B8220" casePriority="F032" documentType="C003" cardApplyType="" cardType="" idNumber="J121679844" cardNumber="" detailId="" /><row fileName="CONT2020080700003_002.jpg" caseId="" FMID="" covertPage="" productType="" branchType="" casePriority="" documentType="" cardApplyType="" cardType="" idNumber="" cardNumber="" detailId="" /><row fileName="CONT2020080700003_003.jpg" caseId="" FMID="" covertPage="" productType="" branchType="" casePriority="" documentType="" cardApplyType="" cardType="" idNumber="" cardNumber="" detailId="" /><row fileName="CONT2020080700003_004.jpg" caseId="" FMID="" covertPage="" productType="" branchType="" casePriority="" documentType="" cardApplyType="" cardType="" idNumber="" cardNumber="" detailId="" /><row fileName="CONT2020080700003_005.jpg" caseId="" FMID="" covertPage="" productType="" branchType="" casePriority="" documentType="" cardApplyType="" cardType="" idNumber="" cardNumber="" detailId="" /><row fileName="CONT2020080700003_006.jpg" caseId="" FMID="" covertPage="" productType="" branchType="" casePriority="" documentType="" cardApplyType="" cardType="" idNumber="" cardNumber="" detailId="" /><row fileName="CONT2020080700003_007.jpg" caseId="" FMID="" covertPage="" productType="" branchType="" casePriority="" documentType="" cardApplyType="" cardType="" idNumber="" cardNumber="" detailId="" /></scanFiles></Orbit>
select * from JobLog where Serial = '8593892';

select * from contractmain where uniqid = 'CONT2020080700003';
select * from contractmainactinfo where uniqid = 'CONT2020080700003';
select * from contractdoc where conno = 'CONT2020080700003';
-- -------------------------------------------------------------------------------------