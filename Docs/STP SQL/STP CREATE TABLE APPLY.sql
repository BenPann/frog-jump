USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[CaseDataKycInfo]    Script Date: 2020/12/3 上午 09:59:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CaseDataKycInfo](
	[UniqId] [nvarchar](50) NOT NULL,
	[Query_Date] [nvarchar](50) NULL,
	[Apply_user_name] [nvarchar](50) NULL,
	[Apply_user_idno] [nvarchar](50) NULL,
	[Apply_user_age] [nvarchar](50) NULL,
	[Apply_user_job_tenure] [nvarchar](50) NULL,
	[Apply_user_company_address] [nvarchar](300) NULL,
	[Apply_user_house_address] [nvarchar](300) NULL,
	[Promo_Depart] [nvarchar](50) NULL,
	[Cross_zone_case] [nvarchar](50) NULL,
	[Finacial_type] [nvarchar](50) NULL,
	[Apply_user_annual_income] [nvarchar](50) NULL,
	[Apply_user_avg_month_income] [nvarchar](50) NULL,
	[AML_Query_Date] [nvarchar](50) NULL,
	[Auth_Type] [nvarchar](50) NULL,
	[ip_address] [nvarchar](50) NULL,
	[Apply_date] [nvarchar](50) NULL,
	[Apply_user_MORTAGAGE] [nvarchar](50) NULL,
	[Apply_user_CAR_LOAN] [nvarchar](50) NULL,
	[Apply_user_PLOAN] [nvarchar](50) NULL,
	[Apply_user_CREDIT_CARD] [nvarchar](50) NULL,
	[Apply_user_CASH_CARD] [nvarchar](50) NULL,
 CONSTRAINT [PK_CaseDataKycInfo] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[CaseDataKycDebtInfo]    Script Date: 2020/12/4 上午 09:59:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CaseDataKycDebtInfo](
	[UniqId] [nvarchar](50) NOT NULL,
	[MORTAGAGE_FLAG] [varchar](2) NULL, -- 房貸註記(1.本行2.他行3.皆有)
	[MORTAGAGE_AMT] [varchar](200) NULL, -- 房貸餘額
	[MORTAGAGE_PAY] [varchar](200) NULL, -- 房貸每月應繳金額
	[CAR_LOAN_FLAG] [varchar](2) NULL, -- 車貸註記(1.本行2.他行3.皆有)
	[CAR_LOAN_AMT] [varchar](200) NULL, -- 車貸餘額
	[CAR_LOAN_PAY] [varchar](200) NULL, -- 車貸每月應繳金額
	[PLOAN_FLAG] [varchar](2) NULL, -- 信貸註記(1.本行2.他行3.皆有)
	[PLOAN_AMT] [varchar](200) NULL, -- 信貸餘額
	[PLOAN_PAY] [varchar](200) NULL, -- 信貸每月應繳金額
	[CREDIT_CARD_FLAG] [varchar](2) NULL, -- 信用卡註記(1.本行2.他行3.皆有)
	[CREDIT_CARD_AMT] [varchar](200) NULL, -- 信用卡餘額
	[CREDIT_CARD_PAY_KIND] [varchar](200) NULL, -- 信用卡付款方式(1.全清2.循環)
	[CREDIT_CARD_CYCLE_AMT] [varchar](200) NULL, -- 信用卡循環金額
	[CASH_CARD_FLAG] [varchar](2) NULL, -- 現金卡註記(1.本行2.他行3.皆有)
	[CASH_CARD_AMT] [varchar](200) NULL, -- 現金卡動用餘額
 CONSTRAINT [PK_CaseDataKycDebtInfo] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


