-- ----------------------------------------------------------------------------------------------------------------
SELECT * FROM lbdate ORDER BY EACHDAY desc;

-- find today of lbdate
SELECT TO_NUMBER(TO_CHAR(SYSDATE, 'YYYYMMDD')) - 19110000 FROM dual;

SELECT * FROM lbdate WHERE EACHDAY IN ((TO_NUMBER(TO_CHAR(SYSDATE, 'YYYYMMDD')) - 19110000)) ORDER BY EACHDAY desc;

SELECT * FROM lbdate WHERE EACHDAY IN (1091125, 1091124, 1091123) ORDER BY EACHDAY desc;

-- OracleWdDao
-- EACHDAY: eeeMMdd 民國年月日
-- CODE1: [工作日 CODE1=0, 假日 CODE1=1]
-- WHATDAY: [星期一 WHATDAY=1 ... 星期日 WHATDAY=7]
SELECT * FROM lbdate WHERE 1091120 < EACHDAY AND EACHDAY < 1091201 ORDER BY EACHDAY desc;
-- ----------------------------------------------------------------------------------------------------------------
--預核名單
SELECT 
	  CUSTOMER_ID
	, nvl(CREDIT_LINE_FINAL,0) AS CREDIT_LINE
	, INTEREST_RATE
	, CREATE_TIME
	, PROJECT_CODE
	, CASE_NO
	, SEX AS GENDER
	, CREATE_TIME + INTERVAL '38' DAY AS LASTSCOREDAY 
from 
	PSC_GM_SCORE_LOG
WHERE 
	-- CUSTOMER_ID = 'P223134648'
	-- AND CREATE_TIME >= sysdate - INTERVAL '38' DAY
	
	AND ROWNUM <100
ORDER BY 
	CREATE_TIME DESC;

-- CREATE_TIME = TO_DATE('2021/01/04 12:00:00', 'yyyy/mm/dd hh24:mi:ss') 
-- CUSTOMER_ID=
-- L123728181
-- D121319366
-- M156294566
-- P223134648
-- X298521228
	
SELECT CUSTOMER_ID, p.* FROM PSC_GM_SCORE_LOG p WHERE CREATE_TIME = TO_DATE('2021/01/04 12:00:00', 'yyyy/mm/dd hh24:mi:ss');

-- 2021-02-19
UPDATE PSC_GM_SCORE_LOG SET CREATE_TIME = TO_DATE('2021/02/19 12:00:00', 'yyyy/mm/dd hh24:mi:ss') WHERE CREATE_TIME = TO_DATE('2021/01/04 12:00:00', 'yyyy/mm/dd hh24:mi:ss');
-- ----------------------------------------------------------------------------------------------------------------