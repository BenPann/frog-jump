-- CaseData
ALTER TABLE [dbo].[CaseData] ADD [AgentNo] [nvarchar](50) NULL; -- 業務員代號
ALTER TABLE [dbo].[CaseData] ADD [AgentDepart] [nvarchar](50) NULL; -- 業務員部門
ALTER TABLE [dbo].[CaseData] ADD [CreateTime] [datetime] NULL; -- CreateTime
ALTER TABLE [dbo].[CaseData] ADD [EndTime] [datetime] NULL; -- EndTime
ALTER TABLE [dbo].[CaseData] ADD [SendedTime] [datetime] NULL; -- SendedTime
ALTER TABLE [dbo].[CaseData] ADD [SubStatus] [nvarchar](50) NULL; -- 副狀態
ALTER TABLE [dbo].[CaseData] ADD [Process] [nvarchar](6) NULL; -- SubStatus
ALTER TABLE [dbo].[CaseData] ADD [IdCardDate] [datetime] NULL; -- 身份證換發日期
ALTER TABLE [dbo].[CaseData] ADD [IdCardLocation] [nvarchar](50) NULL; -- 身份證換發地點
ALTER TABLE [dbo].[CaseData] ADD [IdCardCRecord] [nvarchar](1) NULL; -- 身份證換發紀錄 0:初領 1:補領 2:換發

ALTER TABLE [dbo].[CaseData] ADD [AddNewCaseTime] [datetime] NULL; -- 1. 發動起案時間
ALTER TABLE [dbo].[CaseData] ADD [SendCustInfoTime] [datetime] NULL; -- 2. 送詳細資料時間
ALTER TABLE [dbo].[CaseData] ADD [HasOldFinaImg] [nvarchar](5) NULL; -- 3. 行內是否有舊財力 00:無, 01:有
ALTER TABLE [dbo].[CaseData] ADD [OldFinaImgNoList] [nvarchar](400) NULL; -- 4. 舊財力影編代號列表
ALTER TABLE [dbo].[CaseData] ADD [CanUseOldFinaImg] [nvarchar](5) NULL; -- 5. 舊財力可否使用 00:不可, 01:可
ALTER TABLE [dbo].[CaseData] ADD [HasOldIdImg] [nvarchar](5) NULL; -- 6. 行內是否有舊ID 00:無, 01:有
ALTER TABLE [dbo].[CaseData] ADD [OldIdImgNoList] [nvarchar](400) NULL; -- 7. 舊ID影編代號列表
ALTER TABLE [dbo].[CaseData] ADD [CanUseOldIdImg] [nvarchar](5) NULL; -- 8. 舊ID可否使用 00:不可, 01:可
ALTER TABLE [dbo].[CaseData] ADD [HasUploadId] [nvarchar](5) NULL; -- 9. 是否已透過UI上傳ID 00:無, 01:有, 02:無須上傳
ALTER TABLE [dbo].[CaseData] ADD [HasUploadFina] [nvarchar](5) NULL; -- 10. 是否已已透過UI上傳財力 00:無, 01:有, 02:無須上傳
ALTER TABLE [dbo].[CaseData] ADD [HasQryAml] [nvarchar](5) NULL; -- 11. 是否已查詢AML 00:否, 01:是
ALTER TABLE [dbo].[CaseData] ADD [HasQryIdInfo] [nvarchar](5) NULL; -- 12. 是否已查詢戶役政 00:否, 01:是
ALTER TABLE [dbo].[CaseData] ADD [HasQryZ07] [nvarchar](5) NULL; -- 13. 是否已查詢Z07 00:否, 01:是
ALTER TABLE [dbo].[CaseData] ADD [HasFinishKyc] [nvarchar](5) NULL; -- 14. 是否已產生KYC表 00:否, 01:是
ALTER TABLE [dbo].[CaseData] ADD [IsCaseIntegrated] [nvarchar](5) NULL; -- 15. 案件是否已完整 00:否, 01:是
ALTER TABLE [dbo].[CaseData] ADD [ApplyWithoutFina] [nvarchar](5) NULL; -- 16. 是否為免財力證明 00:否, 01:是
ALTER TABLE [dbo].[CaseData] ADD [DecisionCaseNo] [nvarchar](50) NULL; -- 17. 決策平台編號 透過 api/KGI/ADD_LOAN_CASE 取得的編號，下列決策平台 API都需要用這個編號做後續發查用
ALTER TABLE [dbo].[CaseData] ADD [CheckSalaryAccount] [nvarchar](5) NULL; -- 18. 是否為薪轉戶 判斷客戶是否為薪轉戶(徵審) Y 薪轉戶 N 非薪轉戶
ALTER TABLE [dbo].[CaseData] ADD [CheckCreditcardAccount] [nvarchar](5) NULL; -- 19. 是否為信用卡戶 Y/N
ALTER TABLE [dbo].[CaseData] ADD [CheckCashcardAccount] [nvarchar](5) NULL; -- 20. 是否為現金卡戶 Y/N
ALTER TABLE [dbo].[CaseData] ADD [CheckKgibankAccount] [nvarchar](5) NULL; -- 21. 是否為存款戶 Y/N
ALTER TABLE [dbo].[CaseData] ADD [CheckLoanAccount] [nvarchar](5) NULL; -- 22. 是否為貸款戶 Y/N
ALTER TABLE [dbo].[CaseData] ADD [Phone] [nvarchar](14) NULL; -- 客戶填入之行動電話
ALTER TABLE [dbo].[CaseData] ADD [OCR_FRONT_Idno] [nvarchar](10) NULL; -- OCR 身份證字號
ALTER TABLE [dbo].[CaseData] ADD [OCR_FRONT_BYPASSID] [nvarchar](10) NULL; -- 身份證字號
ALTER TABLE [dbo].[CaseData] ADD [OCR_FRONT_RspCode] [nvarchar](5) NULL; -- OCR
ALTER TABLE [dbo].[CaseData] ADD [OCR_FRONT_Name] [nvarchar](50) NULL; -- OCR 姓名
ALTER TABLE [dbo].[CaseData] ADD [OCR_FRONT_IDCARD_ISSUE_DT] [nvarchar](7) NULL; -- OCR 發證日期eeeMMdd
ALTER TABLE [dbo].[CaseData] ADD [OCR_FRONT_IDCARD_ISSUE_Type] [nvarchar](5) NULL; -- OCR 發證類型
ALTER TABLE [dbo].[CaseData] ADD [OCR_FRONT_IDCARD_ISSUE_City] [nvarchar](10) NULL; -- OCR 發證縣市
ALTER TABLE [dbo].[CaseData] ADD [OCR_BACK_HouseholdAddr1] [nvarchar](300) NULL; -- OCR 縣市區域
ALTER TABLE [dbo].[CaseData] ADD [OCR_BACK_HouseholdAddr2] [nvarchar](300) NULL; -- OCR 里鄰
ALTER TABLE [dbo].[CaseData] ADD [OCR_BACK_HouseholdAddr3] [nvarchar](300) NULL; -- OCR 
ALTER TABLE [dbo].[CaseData] ADD [OCR_BACK_HouseholdAddr4] [nvarchar](300) NULL; -- OCR 
ALTER TABLE [dbo].[CaseData] ADD [OCR_BACK_Marriage] [nvarchar](4) NULL; -- OCR 婚姻:未婚/已婚
ALTER TABLE [dbo].[CaseData] ADD [JcicOk] [nvarchar](1) NULL; -- 發查聯徵成功 Y/N
ALTER TABLE [dbo].[CaseData] ADD [JcicTime] [datetime] NULL; -- 發查聯徵的時間
ALTER TABLE [dbo].[CaseData] ADD [Z07Html] [nvarchar](max) NULL; -- 發查Z07 回傳聯徵資料 html
ALTER TABLE [dbo].[CaseData] ADD [Z07Result] [nvarchar](1) NULL; -- 發查Z07 null, Y：是(有通報案件記錄), N：否
ALTER TABLE [dbo].[CaseData] ADD [Z07Time] [datetime] NULL; -- 發查Z07的時間 yyyy-MM-dd HH:mm:ss.sss
ALTER TABLE [dbo].[CaseData] ADD [AmlResult] [nvarchar](2) NULL; -- 發查AML成功:N 身分為正常 Y 身分為黑名單 V 身分為疑似有異常 or 99
ALTER TABLE [dbo].[CaseData] ADD [AmlTime] [datetime] NULL; -- 發查AML的時間 yyyy-MM-dd HH:mm:ss.sss
ALTER TABLE [dbo].[CaseData] ADD [CddRateRisk] [nvarchar](5) NULL; -- 風險評級:高 中 低
ALTER TABLE [dbo].[CaseData] ADD [LaterAddonSendTime] [datetime] NULL; -- 稍後補件上傳 簡訊傳送時間 yyyy-MM-dd HH:mm:ss.sss

ALTER TABLE [dbo].[CaseData] ADD [JobStatus] [nvarchar](50) NULL; -- 排程狀態