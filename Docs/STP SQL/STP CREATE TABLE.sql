USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[ContractApiLog]    Script Date: 2020/7/2 下午 01:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ContractApiLog](
    [Serial] [int] IDENTITY(1,1) NOT NULL,
    [Type] [nvarchar](2) NULL,
    [KeyInfo] [nvarchar](200) NULL,
    [ApiName] [nvarchar](200) NULL,
    [Request] [nvarchar](max) NOT NULL,
    [Response] [nvarchar](max) NOT NULL,
    [StartTime] [datetime] NOT NULL,
    [EndTime] [datetime] NOT NULL,
    [CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractApiLog] PRIMARY KEY CLUSTERED
(
    [Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[ContractMain]    Script Date: 2020/7/2 下午 04:48:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractMain](
	[UniqId] [nvarchar](50) NOT NULL,
	[UserType] [nvarchar](2) NULL,
	[CaseNoWeb] [nvarchar](50) NULL,
	[GmCardNo] [nvarchar](50) NULL,
	[Status] [nvarchar](2) NOT NULL,
	[ProductId] [nvarchar](20) NULL,
	[Idno] [nvarchar](50) NOT NULL,
	[ChtName] [nvarchar](50) NULL,
	[Birthday] [varchar](8) NOT NULL,
	[Email] [nvarchar](120) NULL,
	[DealPayDay] [datetime] NULL,
	[FinalPayDay] [datetime] NULL,
	[PayBankOption] [nvarchar](2) NULL,
	[ContractDay] [datetime] NULL,
	[PaymentDay] [nvarchar](2) NULL,
	[DocAddr] [nvarchar](200) NULL,
	[ReadDate] [datetime] NULL,
	[Notify] [nvarchar](2) NULL,
	[Court] [nvarchar](20) NULL,
	[DataUse] [nvarchar](2) NULL,
	[ApsConstData] [nvarchar](max) NULL,
	[ResultHtml] [nvarchar](max) NULL,
	[SignTime] [datetime] NULL,
	[SendTime] [datetime] NULL,
	[IpAddress] [nvarchar](20) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractMain] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[ContractMainActInfo]    Script Date: 2020/7/2 下午 04:48:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractMainActInfo](
    [UniqId] [nvarchar](50) NOT NULL,
    [AuthPhone] [nvarchar](15) NULL,
    [AuthBankId] [nvarchar](10) NULL,
    [AuthAccountNo] [nvarchar](50) NULL,
    [AuthStatus] [nvarchar](2) NULL,
    [AuthErrCount] [int] NULL,
    [AuthTime] [datetime] NULL,
    [SignPhone] [nvarchar](15) NULL,
    [SignBankId] [nvarchar](10) NULL,
    [SignAccountNo] [nvarchar](50) NULL,
    [SignStatus] [nvarchar](2) NULL,
    [SignErrCount] [int] NULL,
    [SignTime] [datetime] NULL,
    [EddaBankId] [nvarchar](10) NULL,
    [EddaAccountNo] [nvarchar](50) NULL,
    [EddaStatus] [nvarchar](2) NULL,
    [EddaErrCount] [int] NULL,
    [EddaTime] [datetime] NULL,
    [Creatime] [datetime] NOT NULL,
    [UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractMainActInfo] PRIMARY KEY CLUSTERED
(
    [UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[ContractMainPdf]    Script Date: 2020/7/2 下午 04:48:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractMainPdf](
    [UniqId] [nvarchar](50) NOT NULL,
    [Version] [nvarchar](50) NOT NULL,
    [PdfContent] [varbinary](max) NULL,
    [UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractMainPdf] PRIMARY KEY CLUSTERED
(
    [UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[ContractMainTerms]    Script Date: 2020/7/7 上午 10:10:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractMainTerms](
    [UniqId] [nvarchar](50) NOT NULL,
    [TermSerial] [int] NOT NULL,
    [IsAgree] [varchar](1) NOT NULL,
    [CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractMainTerms] PRIMARY KEY CLUSTERED 
(
    [UniqId] ASC,
    [TermSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[ContractVerifyLog]    Script Date: 2020/7/2 下午 01:37:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractVerifyLog](
    [UniqId] [nvarchar](50) NOT NULL,
    [UniqType] [char](2) NOT NULL DEFAULT '03',
    [VerifyType] [nvarchar](20) NOT NULL,
    [Count] [int] NOT NULL,
    [Request] [nvarchar](max) NOT NULL,
    [Response] [nvarchar](max) NOT NULL,
    [CheckCode] [nvarchar](20) NOT NULL,
    [AuthCode] [nvarchar](20) NULL,
    [Other] [nvarchar](1000) NULL,
    [Status] [nvarchar](2) NULL,
    [CreateTime] [datetime] NULL,
 CONSTRAINT [PK_ContractVerifyLog] PRIMARY KEY CLUSTERED
(
    [UniqId] ASC,
    [VerifyType] ASC,
    [Count] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[EntryDataDetail]    Script Date: 2020/7/3 上午 11:30:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EntryDataDetail](
    [UniqId] [nvarchar](50) NOT NULL,
    [UserAgent] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_EntryDataDetail] PRIMARY KEY CLUSTERED
(
    [UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[WhiteListData]    Script Date: 2020/7/3 上午 11:44:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WhiteListData](
    [ID] [nvarchar](50) NOT NULL,
    [PType] [int] NOT NULL,
    [ApiName] [nvarchar](200) NOT NULL,
    [Response] [nvarchar](max) NULL,
 CONSTRAINT [PK_WhiteListData] PRIMARY KEY CLUSTERED
(
    [ID] ASC,
    [PType] ASC,
    [ApiName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO