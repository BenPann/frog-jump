--立約正式機上板 DB設定事項

--(一) 新增資料
--TABLE [KGITerms]
INSERT [dbo].[KGITerms] ([Name], [Page], [Type], [Sort]) VALUES (N'EOPPersonalDataUsing', N'cont-initPlContract', N'1', 1)
GO
INSERT [dbo].[KGITerms] ([Name], [Page], [Type], [Sort]) VALUES (N'EOPPersonalDataUsing', N'con-initPlContract', N'1', 1)
GO
INSERT [dbo].[KGITerms] ([Name], [Page], [Type], [Sort]) VALUES (N'EOPPersonalDataUsing', N'cont-agreePlContract', N'1', 1)
GO
INSERT [dbo].[KGITerms] ([Name], [Page], [Type], [Sort]) VALUES (N'EOPPersonalDataUsing', N'cont-agreeRplContract', N'1', 1)
GO
INSERT [dbo].[KGITerms] ([Name], [Page], [Type], [Sort]) VALUES (N'EOPPersonalDataUsing', N'cont-agreeCashcardContract', N'1', 1)
GO
INSERT [dbo].[KGITerms] ([Name], [Page], [Type], [Sort]) VALUES (N'ebankingDealItems', N'cont-initPlContract', N'2', 1)
GO

--TABLE [DropdownData]
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'004', N'004 台灣銀行', 1, 4)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'005', N'005 土地銀行', 1, 5)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'006', N'006 合庫商銀', 1, 6)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'007', N'007 第一銀行', 1, 7)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'008', N'008 華南銀行', 1, 8)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'009', N'009 彰化銀行', 1, 9)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'011', N'011 上海銀行', 1, 11)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'012', N'012 台北富邦', 1, 12)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'013', N'013 國泰世華', 1, 13)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'016', N'016 高雄銀行', 1, 16)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'017', N'017 兆豐商銀', 1, 17)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'018', N'018 農業金庫', 1, 18)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'021', N'021 花旗銀行', 1, 21)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'022', N'022 美國銀行', 1, 22)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'025', N'025 首都銀行', 1, 25)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'039', N'039 澳盛銀行', 1, 39)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'040', N'040 中華開發', 1, 40)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'048', N'048 王道銀行', 1, 48)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'050', N'050 台灣企銀', 1, 50)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'052', N'052 渣打商銀', 1, 52)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'053', N'053 台中商銀', 1, 53)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'054', N'054 京城商銀', 1, 54)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'072', N'072 德意志銀行', 1, 72)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'075', N'075 東亞銀行', 1, 75)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'081', N'081 匯豐銀行', 1, 81)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'082', N'082 巴黎銀行', 1, 82)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'101', N'101 瑞興銀行', 1, 101)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'102', N'102 華泰銀行', 1, 102)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'103', N'103 臺灣新光銀行', 1, 103)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'104', N'104 台北五信', 1, 104)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'106', N'106 台北九信', 1, 106)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'108', N'108 陽信銀行', 1, 108)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'114', N'114 基隆一信', 1, 114)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'115', N'115 基隆二信', 1, 115)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'118', N'118 板信銀行', 1, 118)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'119', N'119 淡水一信', 1, 119)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'120', N'120 淡水信合社', 1, 120)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'124', N'124 宜蘭信合社', 1, 124)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'127', N'127 桃園信合社', 1, 127)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'130', N'130 新竹一信', 1, 130)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'132', N'132 新竹三信', 1, 132)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'146', N'146 台中二信', 1, 146)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'147', N'147 三信銀行', 1, 147)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'158', N'158 彰化一信', 1, 158)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'161', N'161 彰化五信', 1, 161)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'162', N'162 彰化六信', 1, 162)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'163', N'163 彰化十信', 1, 163)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'165', N'165 鹿港信合社', 1, 165)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'178', N'178 嘉義三信', 1, 178)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'179', N'179 嘉義四信', 1, 179)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'188', N'188 台南三信', 1, 188)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'204', N'204 高雄三信', 1, 204)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'215', N'215 花蓮一信', 1, 215)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'216', N'216 花蓮二信', 1, 216)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'222', N'222 澎湖一信', 1, 222)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'223', N'223 澎湖二信', 1, 223)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'224', N'224 金門信合社', 1, 224)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'508', N'508 通苑區漁會', 1, 508)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'510', N'510 南龍區漁會', 1, 510)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'511', N'511 彰化區漁會', 1, 511)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'512', N'512 雲林區漁會', 1, 512)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'515', N'515 嘉義區漁會', 1, 515)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'517', N'517 南市區漁會', 1, 517)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'518', N'518 南縣區漁會', 1, 518)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'520', N'520 小港漁會', 1, 520)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'521', N'521 彌陀永安興達林園漁會', 1, 521)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'523', N'523 東港林邊琉球區漁會', 1, 523)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'524', N'524 新港漁會', 1, 524)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'525', N'525 澎湖區漁會', 1, 525)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'542', N'542 麻豆農會', 1, 542)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'549', N'549 下營農會', 1, 549)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'551', N'551 官田農會', 1, 551)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'552', N'552 大內農會', 1, 552)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'557', N'557 新市農會', 1, 557)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'558', N'558 安定農會', 1, 558)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'562', N'562 仁德農會', 1, 562)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'567', N'567 南化農會', 1, 567)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'568', N'568 七股區農會', 1, 568)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'600', N'600 農金資中心', 1, 600)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'605', N'605 高雄市農會', 1, 605)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'606', N'606 新北市地區農會', 1, 606)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'607', N'607 宜蘭農會', 1, 607)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'608', N'608 桃園地區農會', 1, 608)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'612', N'612 神岡鄉農會', 1, 612)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'613', N'613 名間鄉農會', 1, 613)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'614', N'614 彰化地區農會', 1, 614)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'616', N'616 雲林地區農會', 1, 616)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'617', N'617 嘉義地區農會', 1, 617)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'618', N'618 台南地區農會', 1, 618)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'619', N'619 高雄地區農會', 1, 619)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'620', N'620 屏東地區農會', 1, 620)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'621', N'621 花蓮地區農會', 1, 621)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'622', N'622 台東地區農會', 1, 622)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'624', N'624 澎湖區農會', 1, 624)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'625', N'625 台中市農會', 1, 625)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'627', N'627 連江縣農會', 1, 627)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'633', N'633 北斗農會', 1, 633)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'635', N'635 線西農會', 1, 635)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'636', N'636 伸港鄉農會', 1, 636)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'650', N'650 福興農會', 1, 650)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'651', N'651 彰化市農會', 1, 651)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'700', N'700 中華郵政', 1, 700)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'803', N'803 聯邦銀行', 1, 803)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'805', N'805 遠東銀行', 1, 805)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'806', N'806 元大銀行', 1, 806)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'807', N'807 永豐銀行', 1, 807)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'808', N'808 玉山銀行', 1, 808)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'809', N'809 凱基銀行', 1, 809)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'810', N'810 星展銀行', 1, 810)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'812', N'812 台新銀行', 1, 812)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'814', N'814 大眾銀行', 1, 814)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'815', N'815 日盛銀行', 1, 815)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'816', N'816 安泰銀行', 1, 816)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'822', N'822 中國信託', 1, 822)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'870', N'870 梧棲區農會', 1, 870)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'882', N'882 大肚區農會', 1, 882)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'901', N'901 大里市農會', 1, 901)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'903', N'903 汐止市農會', 1, 903)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'904', N'904 新莊市農會', 1, 904)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'910', N'910 桃園新竹區農會', 1, 910)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'912', N'912 冬山鄉農會', 1, 912)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'915', N'915 西湖鄉農會', 1, 915)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'916', N'916 草屯鎮農會', 1, 916)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'919', N'919 三義農會', 1, 919)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'921', N'921 南庄鄉農會', 1, 921)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'922', N'922 台南市農會', 1, 922)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'928', N'928 板橋市農會', 1, 928)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'951', N'951 新北市農會北區共用中心', 1, 951)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'952', N'財團法人農漁會南區資訊中心', 1, 952)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'953', N'953 田尾鄉農會', 1, 953)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'954', N'954 農漁會中區共用中心', 1, 954)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'995', N'995 關貿網路', 1, 995)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'996', N'996 台北區支付處', 1, 996)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'997', N'997 信合社南區資訊中心', 1, 997)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'998', N'998 金融聯合資訊中心', 1, 998)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'EDDABank', N'999', N'999 票據交換所', 1, 999)
GO


INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'RelationCode', N'08', N'配偶', 1, 1) 
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'RelationCode', N'01', N'父母', 1, 1) 
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'RelationCode', N'02', N'子女', 1, 1) 
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'RelationCode', N'03', N'兄弟姊妹', 1, 1) 
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'RelationCode', N'04', N'祖父母', 1, 1) 
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'RelationCode', N'05', N'外祖父母', 1, 1) 
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'RelationCode', N'06', N'孫子女', 1, 1) 
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'RelationCode', N'07', N'外孫子女', 1, 1) 
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'RelationCode', N'09', N'配偶之父母', 1, 1) 
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'RelationCode', N'10', N'配偶之兄弟姊妹', 1, 1) 
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'RelationCode', N'11', N'其他親屬', 1, 1) 
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'RelationCode', N'12', N'其他非親屬自然人', 1, 1) 
GO



--TABLE [config]

INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.AdditionalPhotoDelete', N'0', N'#補件上傳完成之後 立約上傳完成之後 是否刪除圖檔 (1:刪除,0:不刪除) 預設為不刪除', CAST(N'2020-08-13 09:54:02.940' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ALN.Channel', N'ESBMQSVR.MB', N'#ALNWEB設定', CAST(N'2020-08-13 09:54:02.933' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ALN.ClientId', N'ALNWEB', N'#ALNWEB設定', CAST(N'2020-08-13 09:54:02.930' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ALN.ClientPAZZD', N'j2YqRDr7idRKMQCbUapcyA==', N'#ALNWEB設定 #ALNClientPAZZD=AirLoan8&Test', CAST(N'2020-08-13 09:54:02.930' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ALN.HostName', N'172.31.7.13', N'#ALNWEB設定', CAST(N'2020-08-13 09:54:02.930' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ALN.Port', N'1421', N'#ALNWEB設定', CAST(N'2020-08-13 09:54:02.933' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ALN.QueueManagerName', N' ', N'#ALNWEB設定', CAST(N'2020-08-13 09:54:02.933' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ALN.QueueReceiveName', N'EA.ALNWEB', N'#ALNWEB設定', CAST(N'2020-08-13 09:54:02.937' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ALN.QueueSendName', N'MQ.EA', N'#ALNWEB設定', CAST(N'2020-08-13 09:54:02.937' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.APIMApiKey', N'06f8f520-bc99-4bb0-b920-5c4f63baa36d', N'#APIM相關', CAST(N'2020-08-13 09:54:02.960' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.APIMChinalifeCaseSts', N'/v1/CrossCL/caseStsRq', N'#', CAST(N'2020-08-13 09:54:02.967' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.APIMChinalifeCif', N'/v1/CrossCL/cifRq', N'#', CAST(N'2020-08-13 09:54:02.963' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.APIMSecret', N'bfb865c5-8d46-4d3e-bf1c-6899d3b2ef85', N'#APIM相關', CAST(N'2020-08-13 09:54:02.960' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.APIMServerHost', N'http://apisit.kgibank.com:8065', N'#APIM相關', CAST(N'2020-08-13 09:54:02.960' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.APS.ServerHost', N'http://172.18.12.35:6872', N'# 驗身API用(GET_CIF_INFO,GET_OTP_TEL)', CAST(N'2020-08-13 09:54:02.850' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.AUM.Cmd_AddCLAUMInfo', N'KGI/ADD_CL_AUMINFO', N'#AUM 徵審系統', CAST(N'2020-08-13 09:54:02.970' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.AUM.Host', N'http://172.18.12.18:6877/', N'尚未使用', CAST(N'2020-08-13 09:54:02.967' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.AutInfo.Url', N'http://oad6.cdibank.com/Common/WS.nsf/AuthorizationKGIB?WSDL', N'#共銷同意註記 WS', CAST(N'2020-08-13 09:54:02.900' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.AutInfo.Url.ip', N'http://172.31.2.4/Common/WS.nsf/AuthorizationKGIB?WSDL', N'#共銷同意註記 WS', CAST(N'2020-08-13 09:54:02.900' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.BookingDay', N'30', N'#可以往後預約幾個月(不含當月)', CAST(N'2020-08-13 09:54:02.957' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.BookingMonth', N'0', N'#可以往後預約幾個月(不含當月)', CAST(N'2020-08-13 09:54:02.957' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.BreakPointRemainDays', N'7,14,21', N'#斷點通知信天數(用半形逗號分隔)', CAST(N'2020-08-13 09:54:02.913' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ChinalifeCaseSts', N'https://api-uat.chinalife.com.tw/sit/v1/KGService/caseStsRq', N'#', CAST(N'2020-08-13 09:54:02.967' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ChinalifeCif', N'https://api-uat.chinalife.com.tw/sit/v1/KGService/cifRq', N'#', CAST(N'2020-08-13 09:54:02.963' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.chkQuotalDays', N'7', N'#額度利率體驗天數', CAST(N'2020-08-13 09:54:02.940' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.CMBSendMsg.SOAP.URL', N'http://imageap-uat2.kgibank.com/imageapi/KGI_sendmessage.asmx', N'#', CAST(N'2020-08-13 09:54:02.980' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ContactMe.Mail.From', N'ebanking@kgi.com', N'#MAIL', CAST(N'2020-08-13 09:54:02.900' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ContactMe.Mail.To', N'Ben.Pann@kgi.com', N'#MAIL', CAST(N'2020-08-13 09:54:02.900' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.DGT.ServerHost', N'http://172.18.12.18:6877', N'# dgt server 立約用 GET_DGT_CONST, GET_DGT_UPDATE', CAST(N'2020-08-13 09:54:02.857' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.EopenImageServerHost', N'http://172.18.12.177:8090/nibox/import_eop.jsp', N'#', CAST(N'2020-08-13 09:54:02.950' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ESB.Channel', N'ESBMQSVR.EOPSYS', N'#ESB設定', CAST(N'2020-08-13 09:54:02.923' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ESB.ClientId', N'EOPSYS', N'#ESB設定', CAST(N'2020-08-13 09:54:02.920' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ESB.ClientPAZZD', N'3VUsRqd8Hv4FMHULPdWKabcA3aAe4zxi6hYTZ4HBm28=', N'#ESB設定 #ESBClientPAZZD=eOpenSystem@2019', CAST(N'2020-08-13 09:54:02.920' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ESB.HostName', N'172.31.7.13', N'#ESB設定', CAST(N'2020-08-13 09:54:02.923' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ESB.Port', N'1421', N'#ESB設定', CAST(N'2020-08-13 09:54:02.923' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ESB.QueueManagerName', N' ', N'#ESB設定', CAST(N'2020-08-13 09:54:02.927' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ESB.QueueReceiveName', N'EA.EOPSYS', N'#ESB設定', CAST(N'2020-08-13 09:54:02.930' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ESB.QueueSendName', N'MQ.EA', N'#ESB設定', CAST(N'2020-08-13 09:54:02.927' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.FAKE_EMAIL', N'vicky.tsai@kgi.com', N'#測試用收件信箱', CAST(N'2020-08-13 09:54:02.980' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.Img.FTPServer.Address', N'172.18.12.20', N'#FTP', CAST(N'2020-08-13 09:54:02.903' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.Img.FTPServer.Path', N'PoolingDir', N'#FTP', CAST(N'2020-08-14 12:00:00.000' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.Img.FTPServer.PWD', N'1@34qwer', N'#FTP', CAST(N'2020-08-13 09:54:02.907' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.Img.FTPServer.Userid', N'phoneapp', N'#FTP', CAST(N'2020-08-13 09:54:02.907' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.IMG.WATER.FONT', N'限凱基商業銀行申辦使用', N'#圖片浮水印字', CAST(N'2020-08-13 09:54:02.913' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.Mail.Host', N'mail.kgibank.com', N'#MAIL', CAST(N'2020-08-13 09:54:02.900' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.MailHunter.FTPServer.Address', N'172.31.1.182', N'#MailHunter設定', CAST(N'2020-08-13 09:54:02.910' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.MailHunter.FTPServer.PAZZD', N'xiYxCpNQxoowNJvrlwvGoQ==', N'#MailHunter設定 #MailHunterFTPServerPAZZD=1qaz@WSX', CAST(N'2020-08-13 09:54:02.910' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.MailHunter.FTPServer.Userid', N'mhu_ftp', N'#MailHunter設定', CAST(N'2020-08-13 09:54:02.910' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.MailHunter.Url', N'http://172.31.1.182/mailhunter_api/SendNow.asmx?wsdl', N'#MailHunter設定', CAST(N'2020-08-13 09:54:02.910' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.NCCC.MerchantID', N'1300816679', N'#NCCC設定', CAST(N'2020-08-13 09:54:02.913' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.NCCC.TID', N'A2100352', N'#NCCC設定', CAST(N'2020-08-13 09:54:02.917' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.NCCC.TransAmount', N'0', N'#NCCC設定', CAST(N'2020-08-13 09:54:02.920' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.NCCC.TransMode', N'0', N'#NCCC設定', CAST(N'2020-08-13 09:54:02.920' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.NCCC.UserID', N'W122217457', N'#NCCC設定', CAST(N'2020-08-13 09:54:02.917' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.NcccTotalCount', N'6', N'#Nccc驗證錯誤上限', CAST(N'2020-08-13 09:54:02.957' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.OCR.ServerHost', N'http://172.18.12.35:8085', N'#OCR', CAST(N'2020-08-13 09:54:02.940' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.OfflineAddPhotoDelete', N'0', N'#線下件補件流程 是否刪除圖檔 (1:刪除,0:不刪除) 預設為刪除', CAST(N'2020-08-13 09:54:02.943' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.OTP.BillDep', N'6660', N'#for OTP', CAST(N'2020-08-13 09:54:02.953' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.OTP.OpID', N'AP08', N'#for OTP', CAST(N'2020-08-13 09:54:02.950' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PayerAccountType', N'1', N'#代收付平台', CAST(N'2020-08-13 09:54:02.980' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PayerActionType', N'A', N'#代收付平台', CAST(N'2020-08-13 09:54:02.973' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PayerApKey', N'f403034e08cc4e80b8cbf5418336e8b8', N'#代收付平台', CAST(N'2020-08-13 09:54:02.970' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PayerApplyCode', N'ACH02', N'#代收付平台', CAST(N'2020-08-13 09:54:02.973' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PayerLaunchUniformId', N'03434016', N'#代收付平台', CAST(N'2020-08-13 09:54:02.973' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PayerSysId', N'PAY', N'#代收付平台', CAST(N'2020-08-13 09:54:02.970' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PayerTransBankCode', N'8090407', N'#代收付平台', CAST(N'2020-08-13 09:54:02.977' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PayerTransType', N'704', N'#代收付平台', CAST(N'2020-08-13 09:54:02.973' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PCode2566.KGIErrorMsg', N'資料有誤，請聯繫客服', N'#', CAST(N'2020-08-13 09:54:02.947' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PCODE2566.TMNL_ID', N'EOPS', N'#', CAST(N'2020-08-13 09:54:02.947' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PCODE2566.TMNL_TYPE', N'6614', N'#', CAST(N'2020-08-13 09:54:02.943' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.Pcode2566TotalCount', N'6', N'#Pcode2566驗證錯誤上限', CAST(N'2020-08-13 09:54:02.957' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.Cashcard.Footer', N'', N'# 現金卡立約書樣板版本Footer', CAST(N'2020-08-13 09:54:02.987' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.Cashcard.TemplateFilePath', N'/html/contract_CashCard.html', N'# 現金卡立約書樣板路徑', CAST(N'2020-08-13 09:54:02.987' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.Cashcard.VerDate', N'20190501', N'# 現金卡立約書樣板版本VerDate', CAST(N'2020-08-13 09:54:02.987' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.Cashcard.VerNo', N'2019/05', N'# 現金卡立約書樣板版本VerNo', CAST(N'2020-08-13 09:54:02.987' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.PL.Footer', N'PL002-1 一般個人信貸(Web 立約版)(2019/09)', N'# 一般貸款立約書樣板版本Footer', CAST(N'2020-08-13 09:54:03.007' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.PL.MBC.Footer', N'', N'# 一般貸款立約書MBC驗身樣板版本號Footer', CAST(N'2020-08-13 09:54:03.003' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.PL.MBC.TemplateFilePath', N'/html/contract_onlineLoan_MBC.html', N'# 一般貸款立約書MBC驗身樣板路徑', CAST(N'2020-08-13 09:54:03.000' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.PL.MBC.VerDate', N'20190901', N'# 一般貸款立約書MBC驗身樣板版本號VerDate', CAST(N'2020-08-13 09:54:03.000' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.PL.MBC.VerNo', N'2019/09', N'# 一般貸款立約書MBC驗身樣板版本號VerNo', CAST(N'2020-08-13 09:54:03.000' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.PL.TemplateFilePath', N'/html/contract_onlineLoan.html', N'# 一般貸款立約書樣板路徑', CAST(N'2020-08-13 09:54:03.003' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.PL.VerDate', N'20190901', N'# 一般貸款立約書樣板版本VerDate', CAST(N'2020-08-13 09:54:03.007' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.PL.VerNo', N'2019/09', N'# 一般貸款立約書樣板版本VerNo', CAST(N'2020-08-13 09:54:03.007' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.RPL.Footer', N'RPL002-1 (Web立約適用)(2019/09)', N'# 小額信貸立約書樣板版本Footer', CAST(N'2020-08-13 09:54:02.997' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.RPL.MBC.Footer', N'', N'# 小額信貸立約書MBC驗身樣板版本號Footer', CAST(N'2020-08-13 09:54:02.993' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.RPL.MBC.TemplateFilePath', N'/html/contract_E_Loan_MBC.html', N'# 小額信貸立約書MBC驗身樣板路徑', CAST(N'2020-08-13 09:54:02.990' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.RPL.MBC.VerDate', N'20190901', N'# 小額信貸立約書MBC驗身樣板版本號VerDate', CAST(N'2020-08-13 09:54:02.990' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.RPL.MBC.VerNo', N'2019/09', N'# 小額信貸立約書MBC驗身樣板版本號VerNo', CAST(N'2020-08-13 09:54:02.990' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.RPL.TemplateFilePath', N'/html/contract_E_Loan.html', N'# 小額信貸立約書樣板路徑', CAST(N'2020-08-13 09:54:02.993' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.RPL.VerDate', N'20190901', N'# 小額信貸立約書樣板版本VerDate', CAST(N'2020-08-13 09:54:02.997' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Contract.RPL.VerNo', N'2019/09', N'# 小額信貸立約書樣板版本VerNo', CAST(N'2020-08-13 09:54:02.993' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Credit.Footer', N'', N'# 信用卡樣板版本號Footer', CAST(N'2020-08-13 09:54:03.017' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Credit.MBC.Footer', N'', N'# 信用卡MBC驗身樣板版本號Footer', CAST(N'2020-08-13 09:54:03.010' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Credit.MBC.TemplateFilePath', N'/html/creditcard/apply_CreditCard_MBC.html', N'# 信用卡MBC驗身樣板路徑', CAST(N'2020-08-13 09:54:03.010' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Credit.MBC.VerDate', N'20190901', N'# 信用卡MBC驗身樣板版本號VerDate', CAST(N'2020-08-13 09:54:03.010' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Credit.MBC.VerNo', N'2020/01', N'# 信用卡MBC驗身樣板版本號VerNo', CAST(N'2020-08-13 09:54:03.010' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Credit.TemplateFilePath', N'/html/creditcard/apply_CreditCard.html', N'# 信用卡樣板路徑', CAST(N'2020-08-13 09:54:03.013' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Credit.VerDate', N'20190901', N'# 信用卡樣板版本號VerDate', CAST(N'2020-08-13 09:54:03.017' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Credit.VerNo', N'2019/04', N'# 信用卡樣板版本號VerNo', CAST(N'2020-08-13 09:54:03.013' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.FONT_WATER_TTF', N'C:/Windows/Fonts/mingliu.ttc,0', N'# 浮水印字型路徑', CAST(N'2020-08-13 09:54:02.983' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PdfServerUrl', N'http://172.31.1.116:7071/', N'# 立約 html2pdf', CAST(N'2020-08-13 09:54:02.983' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.Cht.Footer', N'', N'# 中華電信貸款樣板版本號Footer', CAST(N'2020-08-13 09:54:03.020' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.Cht.TemplateFilePath', N'/html/loan_application_cht.html', N'# 中華電信貸款樣板路徑', CAST(N'2020-08-13 09:54:03.017' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.Cht.VerDate', N'20181201', N'# 中華電信貸款樣板版本號VerDate', CAST(N'2020-08-13 09:54:03.020' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.Cht.VerNo', N'2018/12', N'# 中華電信貸款樣板版本號VerNo', CAST(N'2020-08-13 09:54:03.020' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.Footer', N'', N'# 貸款樣板版本號VerFooter', CAST(N'2020-08-13 09:54:03.033' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.MBC.Footer', N'', N'# 貸款MBC驗身樣板版本號Footer', CAST(N'2020-08-13 09:54:03.023' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.MBC.TemplateFilePath', N'/html/loan_application_mbc.html', N'# 貸款MBC驗身樣板路徑', CAST(N'2020-08-13 09:54:03.020' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.MBC.VerDate', N'20200201', N'# 貸款MBC驗身樣板版本號VerDate', CAST(N'2020-08-13 09:54:03.023' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.MBC.VerNo', N'2020/02', N'# 貸款MBC驗身樣板版本號VerNo', CAST(N'2020-08-13 09:54:03.023' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.Reject.Footer', N'', N'# 貸款拒件樣板版本號VerFooter', CAST(N'2020-08-13 09:54:03.030' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.Reject.TemplateFilePath', N'/html/loan_application_reject.html', N'# 貸款拒件樣板路徑', CAST(N'2020-08-13 09:54:03.027' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.Reject.VerDate', N'20200201', N'# 貸款拒件樣板版本號VerDate', CAST(N'2020-08-13 09:54:03.027' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.Reject.VerNo', N'2020/02', N'# 貸款拒件樣板版本號VerNo', CAST(N'2020-08-13 09:54:03.027' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.TemplateFilePath', N'/html/loan_application.html', N'# 貸款樣板路徑', CAST(N'2020-08-13 09:54:03.030' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.VerDate', N'20200201', N'# 貸款樣板版本號VerDate', CAST(N'2020-08-13 09:54:03.030' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PL.VerNo', N'2020/02', N'# 貸款樣板版本號VerNo', CAST(N'2020-08-13 09:54:03.030' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.QRCode.TemplateFilePath', N'/html/eopen_qrcode.html', N'# qrcode版本', CAST(N'2020-08-13 09:54:03.033' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PortalUrl', N'http://portal-test.kgibank.com/Index.aspx', N'#', CAST(N'2020-08-13 09:54:02.950' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.SecuritiesHost', N'https://172.31.2.51/WebService.asmx', N'#', CAST(N'2020-08-13 09:54:02.947' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.SendContractEmailFlag', N'0', N'#契約書是否直接寄送EMAIL開關(1:開啟直接寄送至客戶email 0:關閉)', CAST(N'2020-08-13 09:54:02.940' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.UpdateApsContractStatus', N'0', N'#立約完成之後 是否更新APS的立約狀態 (1:更新,0:不更新) 預設為更新', CAST(N'2020-08-13 09:54:02.943' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.upload.image.alpha', N'0.4f', N'#', CAST(N'2020-08-13 09:54:02.837' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.ValidationServerUrl', N'http://localhost:8090/Validation/validate/', N'#Validation Server', CAST(N'2020-08-13 09:54:02.950' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'CONT_Err_GET_DGT_CONST', N'您無任何可供線上簽約之案件或申請案件尚未達立約階段', N'沒有任何可以簽屬的立約案件', CAST(N'2020-07-23 17:03:35.580' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'CONT_Err_GETAPI', N'系統問題，如有任何需要協助，請洽客服人員', N'連接電文回傳 CHECK_CODE=99 系統錯誤', CAST(N'2020-07-23 17:22:38.293' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'CONT_Err_Unexpected', N'系統問題，如有任何需要協助，請洽客服人員', N'非預期的錯誤', CAST(N'2020-07-23 17:53:31.033' AS DateTime))
GO

INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.PLoan.TemplateFilePath', N'https://www.kgibank.com/cosmos/about_us/L04/docs/PL002-1xWebx201806.pdf', N'PL立約書樣板下載路徑', CAST(N'2020-09-15 17:30:45.467' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.RPLoan.TemplateFilePath', N'https://www.kgibank.com/cosmos/about_us/L04/docs/exRPL002-1xWebx-x107.06x.pdf', N'RPL立約書樣板下載路徑', CAST(N'2020-09-15 17:30:45.473' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.GMCashLoan.TemplateFilePath', N'https://www.kgibank.com/about_us/L04/docs/contract04_2.pdf', N'現金立約書樣板下載路徑', CAST(N'2020-09-16 09:53:32.163' AS DateTime))
GO

INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.PDF.Export', N'Y', N'# 是否匯出檔案至 localhost', CAST(N'2020-10-08 13:49:02.897' AS DateTime))
GO

INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.APIMChinalifeCifMoi', N'/v1/moi/rwv2c2', N'#', CAST(N'2020-10-13 14:56:33.817' AS DateTime))
GO


INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.payDateTime.pl.exp_bank_809', N'205959', N'調整立約時間,是PL(1)exp_bank本行 時間小於21點前 格式hhmmss: 205959', CAST(N'2020-08-13 09:54:02.940' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.payDateTime.pl.exp_bank_other', N'145959', N'調整立約時間,是PL(1)exp_bank它行 時間小於15點前 格式hhmmss: 205959', CAST(N'2020-08-13 09:54:02.940' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.payDateTime.pl.pay_bank', N'145959', N'調整立約時間,是PL(1)pay_bank有代償 時間小於15點前 格式hhmmss: 145959', CAST(N'2020-08-13 09:54:02.940' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.payDateTime.rpl', N'235959', N'調整立約時間,是RPL(3) 時間小於24點前 格式hhmmss: 235959', CAST(N'2020-08-13 09:54:02.940' AS DateTime))
GO
INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'airloanEX.payDateTime.cash', N'235959', N'調整立約時間,是現金卡(2) 時間小於24點前 格式hhmmss: 235959', CAST(N'2020-08-13 09:54:02.940' AS DateTime))
GO

INSERT [dbo].[Config] ([KeyName], [KeyValue], [Description], [UpdateTime]) VALUES (N'CONT_Err_PayDateExpired', N'代償立約的指定撥款日已逾期，請重新立約', N'有代償銀行且撥款日期已過', CAST(N'2020-07-23 17:03:35.580' AS DateTime))
GO

--(二)新增TABLE

/****** Object:  Table [dbo].[ContractMainTerms]    Script Date: 2020/7/7 上午 10:10:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractMainTerms](
	[UniqId] [nvarchar](50) NOT NULL,
	[TermSerial] [int] NOT NULL,
	[IsAgree] [varchar](1) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractMainTerms] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[TermSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ContractMainPdf]    Script Date: 2020/7/2 下午 04:48:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractMainPdf](
    [UniqId] [nvarchar](50) NOT NULL,
    [Version] [nvarchar](50) NOT NULL,
    [PdfContent] [varbinary](max) NULL,
    [UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractMainPdf] PRIMARY KEY CLUSTERED
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ContractMain]    Script Date: 2020/7/2 下午 04:48:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractMain](
	[UniqId] [nvarchar](50) NOT NULL,
	[UserType] [nvarchar](2) NULL,
	[CaseNoWeb] [nvarchar](50) NULL,
	[GmCardNo] [nvarchar](50) NULL,
	[Status] [nvarchar](2) NOT NULL,
	[ProductId] [nvarchar](20) NULL,
	[Idno] [nvarchar](50) NOT NULL,
	[ChtName] [nvarchar](50) NULL,
	[Birthday] [varchar](8) NOT NULL,
	[Email] [nvarchar](120) NULL,
	[DealPayDay] [datetime] NULL,
	[FinalPayDay] [datetime] NULL,
    [PayBankOption] [nvarchar](2) NULL,
    [SordBankOption] [nvarchar](2) NULL,
	[ContractDay] [datetime] NULL,
	[PaymentDay] [nvarchar](2) NULL,
	[DocAddr] [nvarchar](200) NULL,
	[ReadDate] [datetime] NULL,
	[Notify] [nvarchar](2) NULL,
	[Court] [nvarchar](20) NULL,
	[DataUse] [nvarchar](2) NULL,
	[ApsConstData] [nvarchar](max) NULL,
	[ResultHtml] [nvarchar](max) NULL,
	[SignTime] [datetime] NULL,
	[SendTime] [datetime] NULL,
	[IpAddress] [nvarchar](20) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractMain] PRIMARY KEY CLUSTERED
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ContractMainActInfo]    Script Date: 2020/7/2 下午 04:48:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractMainActInfo](
	[UniqId] [nvarchar](50) NOT NULL,
	[AuthPhone] [nvarchar](15) NULL,
	[AuthBankId] [nvarchar](10) NULL,
	[AuthBankName] [nvarchar](50) NULL,
	[AuthAccountNo] [nvarchar](50) NULL,
	[AuthStatus] [nvarchar](2) NULL,
	[AuthErrCount] [int] NULL,
	[AuthTime] [datetime] NULL,
	[SignPhone] [nvarchar](15) NULL,
	[SignBankId] [nvarchar](10) NULL,
	[SignBankName] [nvarchar](50) NULL,
	[SignBranchId] [nvarchar](5) NULL,
	[SignAccountNo] [nvarchar](50) NULL,
	[SignStatus] [nvarchar](2) NULL,
	[SignErrCount] [int] NULL,
	[SignTime] [datetime] NULL,
	[EddaBankId] [nvarchar](10) NULL,
	[EddaBankName] [nvarchar](50) NULL,
	[EddaAccountNo] [nvarchar](50) NULL,
	[EddaStatus] [nvarchar](2) NULL,
	[EddaErrCount] [int] NULL,
	[EddaTime] [datetime] NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ContractMainActInfo] PRIMARY KEY CLUSTERED
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ContractApiLog]    Script Date: 2020/9/3 下午 05:38:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ContractApiLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](2) NULL,
	[KeyInfo] [nvarchar](200) NULL,
	[ApiName] [nvarchar](200) NULL,
	[Request] [nvarchar](max) NOT NULL,
	[Response] [nvarchar](max) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Used] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_ContractApiLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[ContractVerifyLog]    Script Date: 2020/7/2 下午 01:37:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContractVerifyLog](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL DEFAULT '03',
	[VerifyType] [nvarchar](20) NOT NULL,
	[Count] [int] NOT NULL,
	[Request] [nvarchar](max) NOT NULL,
	[Response] [nvarchar](max) NOT NULL,
	[CheckCode] [nvarchar](20) NOT NULL,
	[AuthCode] [nvarchar](20) NULL,
	[Other] [nvarchar](1000) NULL,
	[Status] [nvarchar](2) NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PK_ContractVerifyLog] PRIMARY KEY CLUSTERED
(
	[UniqId] ASC,
	[VerifyType] ASC,
	[Count] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


--(三)修改資料
SELECT * FROM DropdownData WHERE Name= 'pCode2566Bank'

--DataName 必須是 銀行代號 + 銀行名稱, 如果沒有 先刪除 
delete from DropdownData WHERE Name= 'pCode2566Bank' --也要確認正式機是否其他會被影響

--再新增以下資料
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'004', N'004 台灣銀行', 1, 4)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'005', N'005 土地銀行', 0, 5)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'006', N'006 合庫商銀', 0, 6)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'007', N'007 第一銀行', 1, 7)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'008', N'008 華南銀行', 0, 8)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'009', N'009 彰化銀行', 1, 9)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'011', N'011 上海銀行', 1, 11)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'012', N'012 台北富邦', 1, 12)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'013', N'013 國泰世華', 1, 13)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'016', N'016 高雄銀行', 1, 16)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'017', N'017 兆豐商銀', 1, 17)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'018', N'018 農業金庫', 0, 18)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'021', N'021 花旗銀行', 1, 21)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'022', N'022 美國銀行', 0, 22)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'025', N'025 首都銀行', 0, 25)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'039', N'039 澳盛銀行', 0, 39)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'040', N'040 中華開發', 0, 40)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'048', N'048 王道銀行', 1, 48)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'050', N'050 台灣企銀', 0, 50)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'052', N'052 渣打商銀', 1, 52)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'053', N'053 台中商銀', 0, 53)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'054', N'054 京城商銀', 1, 54)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'072', N'072 德意志銀行', 0, 72)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'075', N'075 東亞銀行', 0, 75)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'081', N'081 匯豐銀行', 1, 81)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'082', N'082 巴黎銀行', 0, 82)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'101', N'101 瑞興銀行', 1, 101)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'102', N'102 華泰銀行', 1, 102)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'103', N'103 臺灣新光銀行', 1, 103)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'104', N'104 台北五信', 0, 104)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'106', N'106 台北九信', 0, 106)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'108', N'108 陽信銀行', 1, 108)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'114', N'114 基隆一信', 0, 114)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'115', N'115 基隆二信', 0, 115)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'118', N'118 板信銀行', 1, 118)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'119', N'119 淡水一信', 0, 119)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'120', N'120 淡水信合社', 0, 120)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'124', N'124 宜蘭信合社', 0, 124)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'127', N'127 桃園信合社', 0, 127)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'130', N'130 新竹一信', 0, 130)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'132', N'132 新竹三信', 0, 132)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'146', N'146 台中二信', 1, 146)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'147', N'147 三信銀行', 0, 147)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'158', N'158 彰化一信', 0, 158)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'161', N'161 彰化五信', 0, 161)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'162', N'162 彰化六信', 1, 162)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'163', N'163 彰化十信', 0, 163)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'165', N'165 鹿港信合社', 0, 165)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'178', N'178 嘉義三信', 0, 178)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'179', N'179 嘉義四信', 0, 179)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'188', N'188 台南三信', 0, 188)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'204', N'204 高雄三信', 0, 204)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'215', N'215 花蓮一信', 0, 215)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'216', N'216 花蓮二信', 1, 216)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'222', N'222 澎湖一信', 0, 222)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'223', N'223 澎湖二信', 0, 223)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'224', N'224 金門信合社', 0, 224)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'508', N'508 通苑區漁會', 0, 508)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'510', N'510 南龍區漁會', 0, 510)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'511', N'511 彰化區漁會', 0, 511)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'512', N'512 雲林區漁會', 0, 512)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'515', N'515 嘉義區漁會', 0, 515)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'517', N'517 南市區漁會', 0, 517)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'518', N'518 南縣區漁會', 0, 518)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'520', N'520 小港漁會', 0, 520)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'521', N'521 彌陀永安興達林園漁會', 0, 521)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'523', N'523 東港林邊琉球區漁會', 0, 523)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'524', N'524 新港漁會', 0, 524)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'525', N'525 澎湖區漁會', 0, 525)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'542', N'542 麻豆農會', 0, 542)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'549', N'549 下營農會', 0, 549)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'551', N'551 官田農會', 0, 551)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'552', N'552 大內農會', 0, 552)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'557', N'557 新市農會', 0, 557)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'558', N'558 安定農會', 0, 558)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'562', N'562 仁德農會', 0, 562)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'567', N'567 南化農會', 0, 567)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'568', N'568 七股區農會', 0, 568)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'600', N'600 農金資中心', 1, 600)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'605', N'605 高雄市農會', 0, 605)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'606', N'606 新北市地區農會', 0, 606)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'607', N'607 宜蘭農會', 0, 607)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'608', N'608 桃園地區農會', 0, 608)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'612', N'612 神岡鄉農會', 0, 612)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'613', N'613 名間鄉農會', 0, 613)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'614', N'614 彰化地區農會', 0, 614)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'616', N'616 雲林地區農會', 0, 616)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'617', N'617 嘉義地區農會', 0, 617)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'618', N'618 台南地區農會', 0, 618)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'619', N'619 高雄地區農會', 0, 619)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'620', N'620 屏東地區農會', 0, 620)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'621', N'621 花蓮地區農會', 0, 621)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'622', N'622 台東地區農會', 0, 622)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'624', N'624 澎湖區農會', 0, 624)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'625', N'625 台中市農會', 0, 625)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'627', N'627 連江縣農會', 0, 627)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'633', N'633 北斗農會', 0, 633)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'635', N'635 線西農會', 0, 635)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'636', N'636 伸港鄉農會', 0, 636)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'650', N'650 福興農會', 0, 650)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'651', N'651 彰化市農會', 0, 651)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'700', N'700 中華郵政', 0, 700)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'803', N'803 聯邦銀行', 1, 803)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'805', N'805 遠東銀行', 1, 805)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'806', N'806 元大銀行', 1, 806)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'807', N'807 永豐銀行', 1, 807)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'808', N'808 玉山銀行', 1, 808)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'809', N'809 凱基銀行', 1, 809)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'810', N'810 星展銀行', 1, 810)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'812', N'812 台新銀行', 1, 812)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'814', N'814 大眾銀行', 0, 814)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'815', N'815 日盛銀行', 1, 815)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'816', N'816 安泰銀行', 1, 816)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'822', N'822 中國信託', 1, 822)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'870', N'870 梧棲區農會', 0, 870)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'882', N'882 大肚區農會', 0, 882)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'901', N'901 大里市農會', 0, 901)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'903', N'903 汐止市農會', 0, 903)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'904', N'904 新莊市農會', 0, 904)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'910', N'910 桃園新竹區農會', 0, 910)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'912', N'912 冬山鄉農會', 0, 912)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'915', N'915 西湖鄉農會', 0, 915)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'916', N'916 草屯鎮農會', 0, 916)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'919', N'919 三義農會', 0, 919)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'921', N'921 南庄鄉農會', 0, 921)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'922', N'922 台南市農會', 0, 922)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'928', N'928 板橋市農會', 0, 928)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'951', N'951 新北市農會北區共用中心', 0, 951)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'952', N'財團法人農漁會南區資訊中心', 1, 952)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'953', N'953 田尾鄉農會', 0, 953)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'954', N'954 農漁會中區共用中心', 0, 954)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'995', N'995 關貿網路', 0, 995)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'996', N'996 台北區支付處', 0, 996)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'997', N'997 信合社南區資訊中心', 0, 997)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'998', N'998 金融聯合資訊中心', 0, 998)
GO
INSERT [dbo].[DropdownData] ([Name], [DataKey], [DataName], [Enable], [Sort]) VALUES (N'pCode2566Bank', N'999', N'999 票據交換所', 0, 999)
GO
