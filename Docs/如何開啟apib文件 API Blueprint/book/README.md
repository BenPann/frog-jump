<!-- # 凱基線上開戶

## 安裝前說明
- 目前將所有指令都放在根目錄package.json

## 專案架構
- 前端網頁(oaweb-f2e)
- 前端站台(oaweb)
- 後端站台(oaap)
- 共用專案(oacommon)
- 文件資料(oabook)
- 發行資料夾(deploy)

## 系統需求(必須)
- NodeJS
- OpenJDK
- Angular CLI
- Apache Maven 3.2+
  
## 系統需求(選擇性)
- Visual Studio Code (IDE)
- cmder (Command Line工具)

## 安裝過程
- 於根目錄輸入指令
  ``` npm run install-all ```
- 此指令會安裝三個站台所需的所有外部元件

## 開發方法
- 前端網頁
    - 於根目錄輸入指令
    ``` npm run dev-web-f2e ```
- WebServer
  - 於根目錄輸入指令
    ``` npm run dev-web ```
- APServer
    - 於根目錄輸入指令
 ``` npm run dev-ap ```

## 發行方法
- 於根目錄輸入指令
   ``` npm run deploy-all ```
- 至deploy資料夾取得War檔



## 開發想法說明
- Angular要做的事情
    - 使用者直接使用前端(Angular)的Router
    - 進來頁面會先經由Angular的Guard來檢查是否有登入
    - 前端的檢核跟送資料給Web
- Web站台 只做兩件事情
    - 客戶登入之後 會使用jwt做驗證
    - 檢查資料完整性
    - 直接後送到AP
    - 中間建議不含任何商業邏輯
- AP站台 
    - 跟DB串接
    - 跟客戶的API串接
    - 商業邏輯部分
    - Job
- 共用專案
    - 放兩個站台都會用到的東西
    - dto
    - 系統參數
    - 共用函式
- 文件資料
    - 預計用gitbook放文件
    - 內容都為markdown撰寫 -->