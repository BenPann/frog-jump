### 中壽代收付設定狀態 Log 資料表(ED3_CLPayerLog)

| PK  | NULL | IDENTITY | 欄位         | 型態           | 說明                    | 代碼說明 |
| --- | ---- | -------- | ------------ | -------------- | ----------------------- | -------- |
| v   |      |          | Serial       | int            | 序號                    |          |
|     |      |          | PolicySerial | int            | ED3_CLPolicyInfo.Serial |          |
|     | v    |          | PayerStatus  | nvarchar(10)   | 代收付處理狀態          |          |
|     | v    |          | PayerFailMsg | nvarchar(1000) | 代收付錯誤訊息          |          |
|     | v    |          | CreateTime   | datetime       | 新增時間                |          |

```sql
<!--
CREATE TABLE [dbo].[ED3_CLPayerLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[PolicySerial] [int] NOT NULL,
	[PayerStatus] [nvarchar](10) NULL,
	[PayerFailMsg] [nvarchar](1000) NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ED3_CLPayerLog] PRIMARY KEY CLUSTERED
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


-->


```
