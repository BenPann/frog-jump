### 路線 資料表(CG_RouteProduct)

| PK  | NULL | IDENTITY | 欄位            | 型態          | 說明                | 代碼說明               |
| --- | ---- | -------- | --------------- | ------------- | ------------------- | ---------------------- |
| v    |     |          | ChooseProductId | nvarchar(MAX) | 產品組合            |                        |
| v    |     |          | ChannelId       | varchar(20)   | Channel Id          |                        |
|     | v    |          | RouteID         | varchar(20)   | 產品組合編號        | ED3_CaseData.ProductId |
|     | v    |          | RouteType       | char(1)       | 決定是否在 EOpen 內 | 0-內, 1-外             |
|     | v    |          | RouteURL        | nvarchar(MAX) | 外部網址            |                        |
|     | v    |          | ChooseCount     | varchar(20)   | 產品組合的數量      |                        |

<!---


->
