### 產品設定 資料表(CG_SelectProduct_Pproduct)

| PK  | NULL | IDENTITY | 欄位          | 型態        | 說明                     | 代碼說明                         |
| --- | ---- | -------- | ------------- | ----------- | ------------------------ | -------------------------------- |
| v   |      |          | CGProductID   | varchar(20) | 大首頁後產品選擇產品編號 |  |
| v   |      |          | PProductType  | varchar(20) | 主產品種類           | QR_PProductTypeList.PProductType |
| v   |      |          | ChannelId     | varchar(20) | Channel Id                 | QR_ChannelList.ChannelId         |
|     |      |          | SelectDefault | int         | 預選                     |                                  |
|     |      |          | Required      | int         | 不可刪除                 |                                  |
|     |      |          | SORT          | varchar(20) | 排序                     |                                  |


<!--
CREATE TABLE [dbo].[CG_SelectProduct_Pproduct](
	[CGProductID] [varchar](20) NOT NULL,
	[PProductType] [varchar](20) NOT NULL,
	[ChannelId] [varchar](20) NOT NULL,
	[Select_Default] [int] NOT NULL,
	[Required] [int] NOT NULL,
	[SORT] [varchar](20) NOT NULL,
 CONSTRAINT [PK_CG_SelectProduct_Pproduct] PRIMARY KEY CLUSTERED 
(
	[CGProductID] ASC,
	[PProductType] ASC,
	[ChannelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


-->