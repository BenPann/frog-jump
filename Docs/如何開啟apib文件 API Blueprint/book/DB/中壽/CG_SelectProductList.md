### 讀取基本資料 資料表(CG_SelectProductList)

| PK  | NULL | IDENTITY | 欄位              | 型態         | 說明                       | 代碼說明 |
| --- | ---- | -------- | ----------------- | ------------ | -------------------------- | -------- |
| v   |      |          | CGProductID       | varchar(20)  | 大首頁後產品選擇產品編號   |          |
|     |      |          | CGProductName     | varchar(50)  | 大首頁後產品選擇產品名稱   |          |
|     |      |          | CGProductTitle    | varchar(max) | 大首頁後產品選擇產品標題   |          |
|     |      |          | CGProductSubTitle | varchar(max) | 大首頁後產品選擇產品子標題 |          |
|     |      |          | CGProductDesc     | varchar(max) | 大首頁後產品選擇產品敘述   |          |


<!--
CREATE TABLE [dbo].[CG_SelectProductList](
	[CGProductID] [nvarchar](20) NOT NULL,
	[CGProductName] [nvarchar](50) NOT NULL,
	[CGProductTitle] [nvarchar](max) NOT NULL,
	[CGProductSubTitle] [nvarchar](max) NOT NULL,
	[CGProductDesc] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_CG_SelectProductList] PRIMARY KEY CLUSTERED 
(
	[CGProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


-->