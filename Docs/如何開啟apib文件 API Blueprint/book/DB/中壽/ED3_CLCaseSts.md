### 中壽傳送狀態 資料表(ED3_CLCaseSts)

| PK  | NULL | IDENTITY | 欄位       | 型態         | 說明               | 代碼說明 |
| --- | ---- | -------- | ---------- | ------------ | ------------------ | -------- |
| v   |      |          | Serial     | int          | 序號               |          |
|     |      |          | UniqId     | nvarchar(50) | ED3_CLCif.UniqId   |          |
|     |      |          | PdctSts    | nvarchar(2)  | 要回傳給中壽的狀態 |          |
|     | v    |          | CLStatus   | nchar(1)     | 中壽傳送狀態       |          |
|     | v    |          | CLTime     | datetime     | 傳送中壽時間       |          |
|     |      |          | CreateTime | datetime     | 新增時間           |          |
|     | v    |          | UpdateTime | datetime     | 更新時間           |          |

#### PdctSts - 要回傳給中壽的狀態(僅供參考，請以中壽API規格文件為主)

| 代碼 | 中文                   |
| ---- | ---------------------- |
| 00   | 成功                   |
| 10   | 保單不承保             |
| 11   | 行動投保同意書逾時     |
| 20   | QRCode 逾 15 天        |
| 21   | KGI 網頁申辦逾 30 天   |
| 22   | KGI 申辦失敗           |
| 23   | 續扣申辦逾 120 天      |
| 24   | 續扣約定檔設定失敗逾時 |

#### CLStatus - 中壽傳送狀態

| 代碼 | 中文     |
| ---- | -------- |
| null | 未處理   |
| 0    | 已處理   |
| 1    | 處理中   |
| 3    | 傳送失敗 |
| 5    | 重傳失敗 |

```sql
<!--
CREATE TABLE [dbo].[ED3_CLCaseSts](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[PdctSts] [nvarchar](2) NOT NULL,
	[CLStatus] [nchar](1) NULL,
	[CLTime] [datetime] NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NULL,
 CONSTRAINT [PK_ED3_CLCaseSts] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-->


```
