### 中壽更新投保同意書狀態/財力圖檔/保單承保狀態 資料表(ED3_CLPolicyInfo)

| PK  | NULL | IDENTITY | 欄位             | 型態           | 說明                               | 代碼說明                                           |
| --- | ---- | -------- | ---------------- | -------------- | ---------------------------------- | -------------------------------------------------- |
| v   |      |          | Serial           | int            | 序號                               |                                                    |
|     | v    |          | MsgId            | nvarchar(20)   | 交易序號                           |                                                    |
|     | v    |          | UniqId           | nvarchar(50)   | Api 傳入之 CaseId                  |                                                    |
|     | v    |          | ActionType       | nchar(1)       | 作業類型                           | 1-更新同意書與提供財力圖檔，2-更新保單狀態         |
|     | v    |          | ApplNo           | nvarchar(50)   | 行動投保確認同意書序號             |                                                    |
|     | v    |          | EffDt            | nvarchar(10)   | 行動投保上傳日(效期起日), 即 T1    | 格式：YYYY-MM-DD                                   |
|     | v    |          | CashValue        | int            | 保單現金價值(台幣)，不可有千分號   |                                                    |
|     | v    |          | LoanAmt          | int            | 保單可借款金額(台幣)，不可有千分號 |                                                    |
|     | v    |          | RePremiu         | int            | 續期保費                           |                                                    |
|     | v    |          | RePmtMtd         | nvarchar(10)   | 續期保費繳款期別                   |                                                    |
|     | v    |          | OriPmtMtd        | nvarchar(10)   | 原續期扣繳產品                     | 1-信用卡，2-存款，3-現金                           |
|     | v    |          | PolicyImage      | varbinary(MAX) | 財力證明圖檔(PDF)                  | 當 actionType=1 且案件為加辦二合一類型時，則需提供 |
|     | v    |          | CalDate          | nvarchar(10)   | 保單資料日(財力證明圖檔上標註欄位) | 格式：YYYY-MM-DD                                   |
|     | v    |          | QryDate          | nvarchar(10)   | 電文詢問日(財力證明圖檔上標註欄位) | 格式：YYYY-MM-DD                                   |
|     | v    |          | ApplSts          | nchar(1)       | 行動投保同意書狀態                 | Y-同意書已簽回，N-未簽回                           |
|     | v    |          | ApplStsUpdDate   | nvarchar(10)   | 行動投保同意書狀態更新日期         | 格式：YYYY-MM-DD                                   |
|     | v    |          | MsgDate          | nvarchar(10)   | 電文傳送日期                       | 格式：YYYY-MM-DD                                   |
|     | v    |          | PolicySts        | nchar(1)       | 保單承保狀態                       | 1-結案(承保)，2-未承保                             |
|     | v    |          | PolicyStsUpdDate | nvarchar(10)   | 保單承保狀態更新日期               | 格式：YYYY-MM-DD                                   |
|     | v    |          | AumStatus        | nchar(1)       | Aum 處理狀態                       |                                                    |
|     | v    |          | AumTime          | datetime       | Aum 處理時間                       |                                                    |
|     | v    |          | PayerStatus      | nchar(1)       | 待收付處理狀態                     |                                                    |
|     | v    |          | PayerTime        | datetime       | 待收付第一次處理時間/完成時間      |                                                    |
|     | v    |          | CreateTime       | datetime       | 新增時間                           |                                                    |
|     | v    |          | UpdateTime       | datetime       | 更新時間                           |                                                    |

#### PayerStatus - 待收付處理狀態

| 代碼 | 中文           |
| ---- | -------------- |
| null | 未處理         |
| 0    | 已處理         |
| 1    | 處理中         |
| 3    | 代收付設定失敗 |
| 5    | 代收付重試失敗 |

#### AumStatus - Aum 處理狀態

| 代碼 | 中文           |
| ---- | -------------- |
| null | 未處理         |
| 0    | 已處理         |
| 1    | 處理中         |
| 3    | 代收付設定失敗 |
| 5    | 代收付重試失敗 |

```sql
<!--
CREATE TABLE [dbo].[ED3_CLPolicyInfo](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[MsgId] [nvarchar](20) NULL,
	[UniqId] [nvarchar](50) NULL,
	[ActionType] [nchar](1) NULL,
	[ApplNo] [nvarchar](50) NULL,
	[EffDt] [nvarchar](10) NULL,
	[CashValue] [int] NULL,
	[LoanAmt] [int] NULL,
	[RePremiu] [int] NULL,
	[RePmtMtd] [nvarchar](10) NULL,
	[OriPmtMtd] [nvarchar](10) NULL,
	[PolicyImage] [varbinary](max) NULL,
	[CalDate] [nvarchar](10) NULL,
	[QryDate] [nvarchar](10) NULL,
	[ApplSts] [nchar](1) NULL,
	[ApplStsUpdDate] [nvarchar](10) NULL,
	[MsgDate] [nvarchar](10) NULL,
	[PolicySts] [nchar](1) NULL,
	[PolicyStsUpdDate] [nvarchar](10) NULL,
	[AumStatus] [nchar](1) NULL,
	[AumTime] [datetime] NULL,
	[PayerStatus] [nchar](1) NULL,
	[PayerTime] [datetime] NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NULL,
 CONSTRAINT [PK_ED3_CLPolicyInfo] PRIMARY KEY CLUSTERED
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO



-->


```
