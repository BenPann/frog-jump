### 中壽傳送狀態 Log 資料表(ED3_CLCaseStsLog)

| PK  | NULL | IDENTITY | 欄位          | 型態           | 說明                 | 代碼說明 |
| --- | ---- | -------- | ------------- | -------------- | -------------------- | -------- |
| v   |      |          | Serial        | int            | 序號                 |          |
|     |      |          | CaseStsSerial | int            | ED3_CLCaseSts.Serial |          |
|     | v    |          | MsgId         | nvarchar(20)   | 交易序號             |          |
|     | v    |          | StsCode       | nvarchar(10)   | 中壽回覆狀態         |          |
|     | v    |          | StsDesc       | nvarchar(1000) | 中壽回覆訊息         |          |
|     |      |          | CreateTime    | datetime       | 新增時間             |          |

```sql
<!--
CREATE TABLE [dbo].[ED3_CLCaseStsLog](
	[Serial] [int] NOT NULL,
	[CaseStsSerial] [int] NOT NULL,
	[MsgId] [nvarchar](20) NULL,
	[StsCode] [nvarchar](10) NULL,
	[StsDesc] [nvarchar](1000) NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ED3_CLCaseStsLog] PRIMARY KEY CLUSTERED
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


-->


```
