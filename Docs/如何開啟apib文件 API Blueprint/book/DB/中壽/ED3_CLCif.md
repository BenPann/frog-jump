### 中壽回傳 CIF 資料表(ED3_CLCif)

| PK  | NULL | IDENTITY | 欄位          | 型態          | 說明                               | 代碼說明                   |
| --- | ---- | -------- | ------------- | ------------- | ---------------------------------- | -------------------------- |
| v   |      |          | Serial        | int           | 序號                               |                            |
|     |      |          | MsgId         | nvarchar(20)  | 交易序號                           |                            |
|     |      |          | UniqId        | nvarchar(50)  | ED3_CaseData.UniqId                |                            |
|     | v    |          | StsCode       | nvarchar(5)   | 回覆代碼                           |                            |
|     | v    |          | StsDesc       | nvarchar(50)  | 回覆代碼說明                       |                            |
|     | v    |          | ShortUrl      | nvarchar(50)  | 短網址                             |                            |
|     | v    |          | Entry         | nvarchar(50)  | 入口                               |                            |
|     | v    |          | CaseId        | nvarchar(50)  | 商銀案件編號                       | 中壽回傳，應與 UniqId 相同 |
|     | v    |          | ApplNo        | nvarchar(50)  | 行動投保確認同意書序號             |                            |
|     | v    |          | ApplyPdc      | nchar(1)      | 行動投保申辦商銀商品               | 1.數三 2.信用卡            |
|     | v    |          | ApplyPdcRePmt | nchar(1)      | 行動投保新申辦續期扣繳產品         | 1.數三 2.信用卡            |
|     | v    |          | ApplyPmtEndDt | nvarchar(23)  | 續扣效期迄日                       |                            |
|     | v    |          | MsgSendDt     | nvarchar(23)  | 電文傳送日期                       |                            |
|     | v    |          | Token         | nvarchar(50)  | Token                              |                            |
|     | v    |          | TokenStrDt    | nvarchar(10)  | Token 效期起日                     | YYYYMMDD                   |
|     | v    |          | TokenEndDt    | nvarchar(10)  | Token 效期迄日                     | YYYYMMDD                   |
|     | v    |          | IdNo          | nvarchar(10)  | 身份證字號(此欄位中壽實際上未回傳) |                            |
|     | v    |          | ChtName       | nvarchar(20)  | 中文姓名                           |                            |
|     | v    |          | EngName       | nvarchar(20)  | 英文姓名                           |                            |
|     | v    |          | PyName        | nvarchar(20)  | 羅馬拼音姓名                       |                            |
|     | v    |          | BirthDt       | nvarchar(10)  | 出生年月                           | YYYY-MM-DD                 |
|     | v    |          | Email         | nvarchar(50)  | 電子信箱                           |                            |
|     | v    |          | Marriage      | nchar(1)      | 婚姻狀況                           | 1：未婚 2：已婚            |
|     | v    |          | MobNo         | nvarchar(20)  | 行動電話                           |                            |
|     | v    |          | HomeTel       | nvarchar(20)  | 居住電話                           |                            |
|     | v    |          | ResAddr       | nvarchar(100) | 戶籍地址                           |                            |
|     | v    |          | ComAddr       | nvarchar(100) | 通訊地址                           |                            |
|     | v    |          | HomeAddr      | nvarchar(100) | 居住地址                           |                            |
|     | v    |          | ApplType      | nchar(1)      | 申請人身分                         | 1. 要保人 2. 被保險人      |
|     | v    |          | ChanId        | nvarchar(10)  | 通路來源                           | 介接業者代號               |
|     | v    |          | PromoDepart   | nvarchar(10)  | 推廣單位                           | 推廣單位代號               |
|     | v    |          | PromoMmbId    | nvarchar(20)  | 推廣人員                           | 推廣人員編或身分證字號     |
|     | v    |          | PromoMmbName  | nvarchar(20)  | 推廣人員姓名                       |                            |
|     | v    |          | CreateTime    | datetime      | 新增時間                           |                            |

```sql
<!--
CREATE TABLE [ED3_CLCif](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[MsgId] [nvarchar](20) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[StsCode] [nvarchar](5) NULL,
	[StsDesc] [nvarchar](50) NULL,
	[ShortUrl] [nvarchar](50) NULL,
	[Entry] [nvarchar](50) NULL,
	[CaseId] [nvarchar](50) NULL,
	[ApplNo] [nvarchar](50) NULL,
	[ApplyPdc] [nchar](1) NULL,
	[ApplyPdcRePmt] [nchar](1) NULL,
	[ApplyPmtEndDt] [nvarchar](23) NULL,
	[MsgSendDt] [nvarchar](23) NULL,
	[Token] [nvarchar](50) NULL,
	[TokenStrDt] [nvarchar](10) NULL,
	[TokenEndDt] [nvarchar](10) NULL,
	[IdNo] [nvarchar](10) NULL,
	[ChtName] [nvarchar](20) NULL,
	[EngName] [nvarchar](20) NULL,
	[PyName] [nvarchar](20) NULL,
	[BirthDt] [nvarchar](10) NULL,
	[Email] [nvarchar](50) NULL,
	[Marriage] [nchar](1) NULL,
	[MobNo] [nvarchar](20) NULL,
	[HomeTel] [nvarchar](20) NULL,
	[ResAddr] [nvarchar](100) NULL,
	[ComAddr] [nvarchar](100) NULL,
	[HomeAddr] [nvarchar](100) NULL,
	[ApplType] [nchar](1) NULL,
	[ChanId] [nvarchar](10) NULL,
	[PromoDepart] [nvarchar](10) NULL,
	[PromoMmbId] [nvarchar](20) NULL,
	[PromoMmbName] [nvarchar](20) NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ED3_CLCif] PRIMARY KEY CLUSTERED
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
-->


```
