### 紀錄客戶勾選條款資料表(EOP_CaseDataTerms)

| PK  | 欄位       | 型態         | 說明            | 代碼說明               |
| --- | ---------- | ------------ | --------------- | ---------------------- |
| v   | UniqId     | nvarchar(50) | 序號            |                        |
| v   | TermSerial | int          | 條款序號        | 跟 Terms Table 做 對應 |
|     | IsAgree    | varchar(1)   | Y:同意 N:不同意 |                        |
|     | CreateTime | datetime     | 新增時間        |                        |

```sql
create table dbo.EOP_CaseDataTerms
(
    UniqId     nvarchar(50) not null,
    TermSerial int          not null,
    IsAgree    varchar(1)   not null,
    CreateTime datetime     not null,
    constraint PK_EOP_CaseDataTerms
        primary key (UniqId, TermSerial)
)
go


```