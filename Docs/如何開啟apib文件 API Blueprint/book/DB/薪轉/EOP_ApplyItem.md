### 紀錄客戶申請項目資料表(EOP_ApplyItem)

| PK  | 欄位       | 型態          | 說明                                     | 代碼說明 |
| --- | ---------- | ------------- | ---------------------------------------- | -------- |
| v   | UniqId     | nvarchar(50)  | 序號                                     |          |
| v   | ItemId     | nvarchar(2)   | 附加服務代碼                             |          |
|     | Comment    | nvarchar(200) | 備註 (Y:申辦/N:不申辦) 或者 共銷的版本號 |          |
|     | CreateTime | datetime      | 新增時間                                 |          |

#### ItemId - 附加服務代碼

| 代碼 | 中文                       |
| ---- | -------------------------- |
| 1    | 中華電信                   |
| 2    | 信用卡                     |
| 3    | 約定帳戶                   |
| 4    | 電費                       |
| 5    | 瓦斯費                     |
| 6    | 台北水費                   |
| 7    | 台灣水費                   |
| 8    | 房貸                       |
| 9    | 薪轉帳戶優惠信貸           |
| 10   | 信貸立即申請               |
| 11   | 舊戶已申辦過網銀           |
| 12   | 舊戶純註記是否申辦網銀     |
| 13   | 新戶辦網路銀行             |
| 14   | 跳過信用卡驗身             |
| 15   | 共銷                       |
| 16   | 舊戶本身已有辦過電話銀行   |
| 17   | 舊戶純註記是否申辦電話銀行 |
| 18   | 新戶辦電話銀行             |
| 19   | 下次再驗                   |

```sql
create table dbo.EOP_ApplyItem
(
    UniqId     nvarchar(50)  not null,
    ItemId     nvarchar(20)  not null,
    Comment    nvarchar(200) not null,
    CreateTime datetime      not null,
    constraint PK_EOP_ApplyItem
        primary key (UniqId, ItemId)
)
go

```
