### 開戶案件資料 (EOP_CaseData)

| PK  | NULL | 欄位                  | 型態          | 說明                         | 代碼說明                                                |
| --- | ---- | --------------------- | ------------- | ---------------------------- | ------------------------------------------------------- |
| V   |      | UniqId                | nvarchar(50)  | 序號                         | 使用 ULID                                               |
|     | V    | Status                | varchar(2)    | 主狀態                       |                                                         |
|     | V    | Type                  | varchar(1)    | 客戶類型                     | 1: 新戶 2: 既有戶                                       |
|     | V    | Process               | varchar(20)   | 客戶流程                     | 1: 新戶流程 2: 既有戶流程                               |
|     | V    | SalaryAccountTw       | varchar(16)   | 台幣薪資帳戶                 | 新戶的話 此欄位由 KGI 傳回 既有戶為自己選擇             |
|     | V    | SalaryAccountFore     | varchar(16)   | 外幣薪資帳戶                 | 新戶才有值 由 KGI 傳回                                  |
|     | V    | IsTwTaxResident       | varchar(1)    | 是否台灣稅務居民             | Y: 是 N: 否                                             |
|     | V    | Phone                 | nvarchar(50)  | 手機號碼                     | 不管新舊戶都存                                          |
|     | V    | SetSecuritiesDelivery | varchar(1)    | 設定證劵交割戶               | Y: 是 N: 否                                             |
|     | V    | SecuritiesAddr        | nvarchar(50)  | 證卷交割分行縣市             |                                                         |
|     | V    | SecuritiesAddrZipCode | varchar(5)    | 證卷交割分行區碼             |                                                         |
|     | V    | SecuritiesCode        | varchar(5)    | 證卷交割分行代碼             |                                                         |
|     | V    | SecuritiesName        | nvarchar(50)  | 證卷交割分行名稱             |                                                         |
|     | V    | ProductId             | varchar(2)    | 帳戶或帳加卡或舊戶           | 1: 新戶-純帳戶 2: 新戶-帳戶加卡 3: 舊戶                 |
|     | V    | ChtName               | nvarchar(50)  | 中文姓名                     |                                                         |
|     | V    | EngName               | nvarchar(100) | 英文姓名                     |                                                         |
|     | V    | Idno                  | nvarchar(20)  | 身分證號                     |                                                         |
|     | V    | Birthday              | varchar(8)    | 生日                         |                                                         |
|     | V    | Gender                | varchar(1)    | 性別                         | 1:男 2:女                                               |
|     | V    | IdCardDate            | datetime      | 身份證換發日期               |                                                         |
|     | V    | IdCardLocation        | nvarchar(50)  | 身份證換發地點               |                                                         |
|     | V    | IdCardRecord          | varchar(2)    | 身份證換發紀錄               | 1：領(初領) 2：補 3：換                                 |
|     | V    | ResAddrZipCode        | nvarchar(6)   | 戶籍地址區碼                 |                                                         |
|     | V    | ResAddr               | nvarchar(200) | 戶籍地址                     |                                                         |
|     | V    | CommAddrZipCode       | nvarchar(6)   | 通訊地址區碼                 |                                                         |
|     | V    | CommAddr              | nvarchar(200) | 通訊地址                     |                                                         |
|     | V    | Education             | varchar(1)    | 教育程度                     | 1: 博士 2: 碩士 3: 大學 4:大專 5:高中(職) 6:國中 7:其它 |
|     | V    | Marriage              | varchar(1)    | 婚姻狀態                     | M : 已婚 S : 未婚                                       |
|     | V    | EmailAddress          | varchar(120)  | EMAIL 地址                   |                                                         |
|     | V    | ResTelArea            | nvarchar(4)   | 戶籍電話區碼                 |                                                         |
|     | V    | ResTel                | nvarchar(30)  | 戶籍電話                     |                                                         |
|     | V    | HomeTelArea           | nvarchar(4)   | 居住電話區碼                 |                                                         |
|     | V    | HomeTel               | nvarchar(30)  | 居住電話                     |                                                         |
|     | V    | EstateType            | varchar(1)    | 不動產類型                   | 1.本人所有 2.配偶所有 3.家族所有 4.無                   |
|     | V    | PassBookTw            | varchar(1)    | 台幣帳戶是否要存摺           | Y: 是 N: 否                                             |
|     | V    | PassBookFore          | varchar(1)    | 外幣帳戶是否要存摺           | Y: 是 N: 否                                             |
|     | V    | CorpName              | nvarchar(100) | 公司名稱                     |                                                         |
|     | V    | CorpTelArea           | nvarchar(4)   | 公司電話區碼                 |                                                         |
|     | V    | CorpTel               | nvarchar(30)  | 公司電話                     |                                                         |
|     | V    | CorpAddrZipCode       | nvarchar(6)   | 公司地址區碼                 |                                                         |
|     | V    | CorpAddr              | nvarchar(200) | 公司地址                     |                                                         |
|     | V    | Occupation            | char(6)       | 職業類別                     |                                                         |
|     | V    | JobTitle              | nvarchar(100) | 職稱                         |
|     | V    | YearlyIncome          | int           | 年收入                       | (單位：萬元)                                            |
|     | V    | OnBoardDate           | char(10)      | 到職日                       |                                                         |
|     | V    | IpAddress             | varchar(20)   | IP                           |                                                         |
|     | V    | SendType              | varchar(1)    | 對帳單寄送方式               | 2:電子帳單 3:郵寄帳單                                   |
|     | V    | BreakPointPage        | varchar(200)  | 斷點頁面                     |                                                         |
|     | V    | D3AccountType         | varchar(4)    | 數三要改開戶影像用的帳戶類別 |                                                         |
|     | V    | D3SubCategory         | varchar(4)    | 數三要改開戶影像用的子類別   |                                                         |
|     | V    | AMLResult             | varchar(1)    | 是否為高風險職業             | Y:高風險 N:非高風險                                     |
|     | V    | DataUse               | char(1)       | 是否有共銷                   | Y:共銷 N:非共銷                                         |
|     | V    | EOPStatus             | varchar(2)    | 開戶狀態                     | 0:成功 1:退件(失敗) 2:補件 3:舊客戶使用舊戶帳戶         |
|     | V    | TrustStatus           | varchar(2)    | 信託帳戶是否開戶成功         | Y / N                                                   |
|     |      | CreateTime            | datetime      | 新增時間                     |                                                         |
|     |      | UpdateTime            | datetime      | 更新時間                     |                                                         |
|     | V    | SendedTime            | datetime      | 發送時間                     |                                                         |

#### Status - 案件主狀態

| 代碼 | 中文             |
| ---- | ---------------- |
| 01   | 編輯中           |
| 02   | 編輯完成         |
| 11   | 資料處理中       |
| 12   | 資料送出成功     |
| 18   | 補件資料送出失敗 |
| 19   | 資料送出失敗     |

#### JobTitle - 職稱

| 代碼 | 中文             |
| ---- | ---------------- |
| 1    | 董監事/股東      |
| 2    | 負責人           |
| 3    | 高階管理職務人員 |
| 4    | 一般主管         |
| 5    | 工程師           |
| 6    | 業務人員         |
| 7    | 一般職員         |
| 8    | 約聘人員         |
| 9    | 老師             |
| 10   | 其他             |

#### EOPStatus - 案件開戶狀態

| 代碼 | 中文               |
| ---- | ------------------ |
| 0    | 成功               |
| 1    | 退件(失敗)         |
| 2    | 補件               |
| 3    | 舊客戶使用舊戶帳戶 |

#### 前端-案件進度

編輯中
編輯完成
開戶中
開戶完成
退件中
補件中

```sql
create table dbo.EOP_CaseData
(
    UniqId                nvarchar(50) not null
        constraint PK_EOP_CaseData
            primary key,
    Status                varchar(2),
    Type                  varchar(1),
    Process               varchar(20),
    SalaryAccountTw       varchar(16),
    SalaryAccountFore     varchar(16),
    IsTwTaxResident       varchar(1),
    Phone                 nvarchar(50),
    SetSecuritiesDelivery varchar(1),
    SecuritiesAddrZipCode varchar(5),
    SecuritiesAddr        nvarchar(50),
    SecuritiesCode        varchar(5),
    SecuritiesName        nvarchar(50),
    ProductId             varchar(2),
    ChtName               nvarchar(50),
    EngName               nvarchar(100),
    Idno                  nvarchar(20),
    Birthday              varchar(8),
    Gender                varchar(1),
    IdCardDate            datetime,
    IdCardLocation        nvarchar(50),
    IdCardRecord          varchar(2),
    ResAddrZipCode        nvarchar(6),
    ResAddr               nvarchar(200),
    CommAddrZipCode       nvarchar(6),
    CommAddr              nvarchar(200),
    Education             varchar(1),
    Marriage              varchar(1),
    EmailAddress          varchar(120),
    ResTelArea            nvarchar(4),
    ResTel                nvarchar(30),
    HomeTelArea           nvarchar(4),
    HomeTel               nvarchar(30),
    EstateType            varchar(1),
    PassBookTw            varchar(1),
    PassBookFore          varchar(1),
    CorpName              nvarchar(100),
    CorpTelArea           nvarchar(4),
    CorpTel               nvarchar(30),
    CorpAddrZipCode       nvarchar(6),
    CorpAddr              nvarchar(200),
    Occupation            varchar(10),
    JobTitle              nvarchar(100),
    YearlyIncome          int,
    OnBoardDate           char(10),
    IpAddress             varchar(20),
    SendType              varchar(1),
    BreakPointPage        varchar(200),
    D3AccountType         varchar(4),
    D3SubCategory         varchar(4),
    AMLResult             varchar(1),
    DataUse               varchar(1),
    EOPStatus             varchar(2),
    TrustStatus           varchar(1),
    CreateTime            datetime,
    UpdateTime            datetime,
    SendedTime            datetime,
    EndTime               datetime,
    ReceiveResultTime     datetime
)
go


```
