### EOP_ShortUrlChangeLog 短網址狀態異動紀錄 (EOP_ShortUrlChangeLog)

- 此 Table 是紀錄 短網址的狀態異動紀錄

| PK  | NULL | 欄位         | 型態          | 說明           | 代碼說明                                    |
| --- | ---- | ------------ | ------------- | -------------- | ------------------------------------------- |
| V   |      | Serial       | int           | 序號           |                                             |
|     |      | ShortUrl     | nvarchar(50)  | 連結網址       |                                             |
|     |      | ColumnName   | nvarchar(100) | 欄位名稱       | 只先記錄 EOP_ShortUrl 的 Status、Batchowner |
|     |      | ColumnBefore | nvarchar(50)  | 修改前欄位資料 |  同  EOP_ShortUrl 的狀態                     |
|     |      | ColumnAfter  | nvarchar(50)  | 修改後欄位資料 |  同  EOP_ShortUrl 的狀態                    |
|     |      | CreateUser   | nvarchar(50)  | 異動使用者     |                                             |
|     |      | CreateTime   | datetime      | 異動時間       |                                             |

```sql
create table dbo.EOP_ShortUrlChangeLog
(
	Serial int identity
		primary key,
	ShortUrl nvarchar(50) not null,
	ColumnName nvarchar(100) not null,
	ColumnBefore nvarchar(50) not null,
	ColumnAfter nvarchar(50) not null,
	CreateUser nvarchar(50) not null,
	CreateTime datetime not null
)
go


```