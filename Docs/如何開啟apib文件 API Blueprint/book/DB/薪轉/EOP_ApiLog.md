### 呼叫 API 的 LOG 資料表(EOP_ApiLog)

| PK  | 欄位       | 型態          | 說明                    | 代碼說明                 |
| --- | ---------- | ------------- | ----------------------- | ------------------------ |
| v   | Serial     | int           | 序號, 由資料庫自動產生  |                          |
|     | Type       | nvarchar(2)   | 1: 我方呼叫 2: 對方呼叫 |                          |
|     | KeyInfo    | nvarchar(200) | 識別此交易是由誰發出去  | 放 ULID 或其他查詢用資料 |
|     | ApiName    | nvarchar(200) | 呼叫的 API 名稱         |                          |
|     | Request    | nvarchar(max) | 呼叫 API 的需求電文     |                          |
|     | Response   | nvarchar(max) | API 的回應電文          |                          |
|     | StartTime  | datetime      | API 開始時間            |                          |
|     | EndTime    | datetime      | API 結束時間            |                          |
|     | CreateTime | datetime      | 新增時間                |                          |

### 此 Log 為 我方系統呼叫外部系統 或外部系統呼叫我方系統的時候 要記錄的

```sql
create table dbo.EOP_ApiLog
(
    Serial     int identity
        constraint PK_EOP_ApiLog
            primary key,
    Type       nvarchar(2)   not null,
    KeyInfo    nvarchar(200),
    ApiName    nvarchar(200) not null,
    Request    nvarchar(max) not null,
    Response   nvarchar(max) not null,
    StartTime  datetime      not null,
    EndTime    datetime      not null,
    CreateTime datetime      not null
)
go

create index EOP_ApiLog_ApiName_index_2
    on dbo.EOP_ApiLog (ApiName) include (Serial, Type, KeyInfo, Request, Response, StartTime, EndTime, CreateTime)
go

```