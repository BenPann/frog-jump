### EOP_ShortUrlChangeOwner 短網址案件移交審核表 (EOP_ShortUrlChangeOwner)

- 此 Table 是紀錄 短網址案件移交審核表

| PK  | NULL | 欄位        | 型態          | 說明             | 代碼說明                      |
| --- | ---- | ----------- | ------------- | ---------------- | ----------------------------- |
| V   |      | Serial      | int           | 序號             |                               |
|     |      | ShortUrl    | nvarchar(50)  | 連結網址         |                               |
|     |      | LastOwner   | nvarchar(100) | 案件上一個擁有者 | userid                        |
|     |      | NewOwner    | nvarchar(100) | 案件新的擁有者   | userid                        |
|     |      | Status      | nvarchar(2)   | 移交狀態         | 01 編輯中 02 送審中 03 已審核 |
|     |      | CreateUser  | nvarchar(50)  | 建立使用者       |                               |
|     |      | CreateTime  | datetime      | 建立時間         |                               |
|     |      | UpdateUser  | nvarchar(50)  | 最後更新使用者   |                               |
|     |      | UpdateTime  | datetime      | 最後更新時間     |                               |
|     | V    | ApproveUser | nvarchar(50)  | 放行使用者       |                               |
|     | V    | ApproveTime | datetime      | 放行時間         |                               |

```sql
create table dbo.EOP_ShortUrlChangeOwner
(
	Serial int identity
		constraint PK_EOP_ShortUrlChangeOwner
			primary key,
	ShortUrl nvarchar(50) not null,
	LastOwner nvarchar(100) not null,
	NewOwner nvarchar(100) not null,
	Status nvarchar(2) not null,
	CreateUser nvarchar(50) not null,
	CreateTime datetime not null,
	UpdateUser nvarchar(50) not null,
	UpdateTime datetime not null,
	ApproveUser nvarchar(50) not null,
	ApproveTime datetime not null
)
go

```