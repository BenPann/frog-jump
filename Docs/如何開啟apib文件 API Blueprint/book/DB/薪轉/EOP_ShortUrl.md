### EOP_ShortUrl 短網址建立表 (EOP_ShortUrl)

| PK  | NULL | 欄位              | 型態          | 說明                   | 代碼說明                                                      |
| --- | ---- | ----------------- | ------------- | ---------------------- | ------------------------------------------------------------- |
| V   |      | ShortUrl          | nvarchar(50)  | 連結網址               | 8 碼                                                          |
| V   |      | BatchId           | nvarchar(20)  | 批號                   | EBATYYYYMMDD+序號 5 碼                                        |
|     |      | Status            | varchar(2)    | 狀態                   |                                                               |
|     |      | DepartArea        | nvarchar(50)  | 開戶行區域             | 凱基分行 DB 內的縣市區域中文                                  |
|     |      | DepartID          | nvarchar(50)  | 開戶行單位代碼         | 凱基分行 DB 內的分行代碼                                      |
|     |      | DepartName        | nvarchar(50)  | 開戶行單位名稱         | 凱基分行 DB 內的分行中文                                      |
|     |      | CheckList         | char(1)       | 是否有檢核名單         |                                                               |
|     | V    | CheckListName     | nvarchar(50)  | 檢核名單的檔名         |                                                               |
|     |      | BusinessDepart    | nvarchar(50)  | 業務人員單位代號       |                                                               |
|     |      | BusinessMember    | nvarchar(50)  | 業務人員員工代號       |                                                               |
|     |      | CorpID            | nvarchar(50)  | 公司統一編號           |                                                               |
|     |      | CorpName          | nvarchar(50)  | 公司名稱               |                                                               |
|     |      | CorpTelArea       | nvarchar(50)  | 公司電話區碼           |                                                               |
|     |      | CorpTel           | nvarchar(50)  | 公司電話               |                                                               |
|     |      | CorpAddrZipCode   | nvarchar(50)  | 公司地址區碼           |                                                               |
|     |      | CorpAddr          | nvarchar(50)  | 公司地址               |                                                               |
|     |      | Occupation        | char(6)       | 職業類別               |                                                               |
|     |      | TaiwanPassbook    | nvarchar(50)  | 台幣帳戶是否要存摺     | 不指定 : 空值、是:Y、否: N                                    |
|     |      | ShortUrlStartTime | datetime      | 網址開始日期           |                                                               |
|     |      | ShortUrlEndTime   | datetime      | 網址結束日期           |                                                               |
|     |      | PayDate1          | datetime      | 預計首次撥薪日期       |                                                               |
|     |      | PayDate2          | datetime      | 預計首次撥薪日期 2     |                                                               |
|     |      | TwdRate           | nvarchar(20)  | 台幣帳戶薪轉利率       |                                                               |
|     |      | TwdCashWdraw      | nvarchar(4)   | 台幣帳戶每月跨提       |                                                               |
|     |      | TwdTranSfer       | nvarchar(4)   | 台幣帳戶每月跨轉       |                                                               |
|     |      | TwdWtdtr          | nvarchar(4)   | 台幣帳戶每月跨提跨轉   |                                                               |
|     |      | ChkTwdOffer       | varchar(8)    | 台幣帳戶被選中的選項   | 1: TwdRate 2:TwdCashWdraw 3:TwdTranSfer 4:TwdWtdtr 用逗號區隔 |
|     |      | ForeignOffer      | nvarchar(50)  | 外幣帳戶優惠           | 系統參數設定的代碼，從 1 開始編                               |
|     |      | TrustOffer        | nvarchar(50)  | 信託帳戶優惠           | 系統參數設定的代碼，從 1 開始編                               |
|     |      | CreditProductId   | nvarchar(200) | 信用卡卡別             | 逗號區隔卡別號碼；不指定放空值                                |
|     |      | HouseLoanRate     | nvarchar(20)  | 房貸利率               |                                                               |
|     |      | LoanRate          | nvarchar(20)  | 信貸利率               |                                                               |
|     |      | CreateUser        | nvarchar(50)  | 新增使用者             |                                                               |
|     |      | CreateTime        | datetime      | 新增時間               |                                                               |
|     |      | UpdateUser        | nvarchar(50)  | 最後更新使用者         |                                                               |
|     |      | UpdateTime        | datetime      | 最後更新時間           |                                                               |
|     | V    | CheckUser         | nvarchar(50)  | 初審人員               |                                                               |
|     | V    | CheckTime         | datetime      | 初審時間               |                                                               |
|     | V    | ApproveUser       | nvarchar(50)  | 放行使用者             |                                                               |
|     | V    | ApproveTime       | datetime      | 放行時間               |                                                               |
|     |      | BatchOwner        | nvarchar(50)  | 擁有者(案件移交人員)   |                                                               |
|     |      | BatchOwnerChgTime | datetime      | 擁有時間(案件移交時間) |                                                               |

#### Status - 此短網址狀態

| 代碼 | 中文                   |
| ---- | ---------------------- |
| 01   | 使用者編輯中           |
| 02   | 送審待匯入             |
| 03   | 送審待放行             |
| 04   | 放行                   |
| 05   | 停用待審核             |
| 06   | 停用待放行             |
| 07   | 停用                   |
| 08   | 刪除                   |
| 21   | 此短網址已經送影像     |
| 22   | 此短網址送影像失敗     |
| 23   | 短網址已過期，但無案件 |

開戶中 : 04 且 網址開始日期小於等於今天且網址結束日期大於等於今天
已到期 : 04 且 網址結束日期小於今天

#### CreditProductId - 信用卡卡號

使用者選不指定時放空值，其餘放卡片號碼，以","區隔

```sql
create table dbo.EOP_ShortUrl
(
	ShortUrl nvarchar(50) not null,
	BatchId nvarchar(20) not null,
	Status varchar(2) not null,
	DepartArea nvarchar(50) not null,
	DepartId nvarchar(50) not null,
	DepartName nvarchar(50) not null,
	CheckList char not null,
	CheckListName nvarchar(50),
	BusinessDepart nvarchar(50) not null,
	BusinessMember nvarchar(50) not null,
	CorpID nvarchar(50) not null,
	CorpName nvarchar(50) not null,
	CorpTelArea nvarchar(50) not null,
	CorpTel nvarchar(50) not null,
	CorpAddrZipCode nvarchar(50) not null,
	CorpAddr nvarchar(50) not null,
	Occupation varchar(10) not null,
	TaiwanPassbook nvarchar(50) not null,
	ShortUrlStartTime datetime not null,
	ShortUrlEndTime datetime not null,
	PayDate1 nvarchar(20) not null,
	PayDate2 nvarchar(20) not null,
	TwdRate nvarchar(20) not null,
	TwdCashWdraw nvarchar(4) not null,
	TwdTranSfer nvarchar(4) not null,
	TwdWtdtr nvarchar(4) not null,
	ChkTwdOffer nvarchar(200),
	ForeignOffer nvarchar(50) not null,
	TrustOffer nvarchar(50) not null,
	CreditProductId nvarchar(200) not null,
	HouseLoanRate nvarchar(20) not null,
	LoanRate nvarchar(20) not null,
	RejectReason nvarchar(200),
	CreateUser nvarchar(50) not null,
	CreateTime datetime not null,
	UpdateUser nvarchar(50) not null,
	UpdateTime datetime not null,
	CheckUser nvarchar(50),
	CheckTime datetime,
	ApproveUser nvarchar(50),
	ApproveTime datetime,
	BatchOwner nvarchar(50) not null,
	BatchOwnerChgTime datetime not null,
	constraint PK_EOP_ShortUrl
		primary key (ShortUrl, BatchId)
)
go

```