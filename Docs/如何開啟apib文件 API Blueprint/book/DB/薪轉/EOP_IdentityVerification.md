### 信用卡或存款帳號驗證狀態 (EOP_IdentityVerification)

| PK  | NULL | 欄位                | 型態          | 說明           | 代碼說明                          |
| --- | ---- | ------------------- | ------------- | -------------- | --------------------------------- |
| v   |      | UniqId              | nvarchar(50)  | 序號           | ULID                              |
|     |      | VerifyStatus        | char(1)       | 驗證狀態       | 0:驗證失敗 1:驗證成功             |
|     |      | VerifyType          | char(1)       | 驗證類型       | 1:他行信用卡 2:他行存款帳戶       |
|     |      | CardNo              | varchar(20)   | 信用卡卡號     | 卡號範例 1111**\*\*\*\***2222     |
|     |      | CardTime            | varchar(8)    | 信用卡到期年月 | 到期範例 : YYMM 月份 西元年後兩碼 |
|     |      | BankId              | varchar(10)   | 存款銀行       |                                   |
|     |      | AccountNo           | varchar(20)   | 存款帳號       |                                   |
|     | v    | ErrorCount          | int           | 錯誤次數       | 只能錯六次 所以最多到 6           |
|     | v    | NCCCErrorCount      | int           | 錯誤次數       | 紀錄 nccc 錯幾次                  |
|     | v    | Pcode2566ErrorCount | int           | 錯誤次數       | 紀錄 pcode2566 錯幾次             |
|     | v    | ErrorMsg            | nvarchar(max) | 錯誤原因       |                                   |
|     |      | CreateTime          | datetime      | 新增時間       |                                   |
|     |      | UpdateTime          | datetime      | 更新時間       |                                   |


```sql
create table dbo.EOP_IdentityVerification
(
    UniqId              nvarchar(50) not null
        constraint PK_EOP_IdentityVerificatio
            primary key,
    VerifyStatus        char         not null,
    VerifyType          char         not null,
    CardNo              varchar(20)  not null,
    CardTime            varchar(8)   not null,
    BankId              varchar(10)  not null,
    AccountNo           varchar(20)  not null,
    ErrorCount          int,
    NcccErrorCount      int,
    Pcode2566ErrorCount int,
    ErrorMsg            nvarchar(max),
    CreateTime          datetime     not null,
    UpdateTime          datetime     not null
)
go

```