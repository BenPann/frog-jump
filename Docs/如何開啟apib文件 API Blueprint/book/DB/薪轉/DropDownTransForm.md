### 凱基各系統選單轉換 (DropDownTransForm)

- 此 Table 對應 DropDownData 用作各系統間的選單值轉換

| PK  | NULL | 欄位        | 型態         | 說明                   | 代碼說明 |
| --- | ---- | ----------- | ------------ | ---------------------- | -------- |
| v   |      | Serial      | int          | 序號, 由資料庫自動產生 |          |
|     |      | DataKey     | nvarchar(50) | 選項的 key 值          |          |
|     |      | Credit      | nvarchar(50) | 信用卡                 |          |
|     |      | Loan        | nvarchar(50) | 信貸                   |          |
|     |      | Eop         | nvarchar(50) | 開戶                   |          |
|     |      | Description | nvarchar(50) | 選項的中文名           |          |

#### DataKey - 選項的 key 值

| 代碼         | 中文           |
| ------------ | -------------- |
| occupation   | 職業           |
| marriage     | 婚姻           |
| education    | 教育           |
| billType     | 帳單寄送方式   |
| idcardRecord | 身分證換發紀錄 |

```sql
create table dbo.DropDownTransForm
(
    Serial          int identity
        primary key,
    DropDownDataKey nvarchar(50) not null,
    Credit          nvarchar(50) not null,
    Loan            nvarchar(50) not null,
    Eop             nvarchar(50) not null,
    Description     nvarchar(50) not null
)
go

```