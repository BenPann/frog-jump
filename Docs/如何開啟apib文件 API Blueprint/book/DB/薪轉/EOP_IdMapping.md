### ID 對應表 (EOP_IdMapping)

- 此 Table 新增之後不准修改
- 客戶第二次進來 會使用資料來此 Table 查詢 UniqId 並產生相應的 Token

| PK  | NULL | 欄位       | 型態          | 說明       | 代碼說明                 |
| --- | ---- | ---------- | ------------- | ---------- | ------------------------ |
| V   |      | UniqId     | nvarchar(50)  | 序號       | 使用 ULID                |
|     |      | Type       | varchar(2)    | 類別       | 1:整批開戶 2:數三 3:中壽 |
|     |      | BatchId    | nvarchar(20)  | 批號       | 後端產出的批次號         |
|     |      | Idno       | varchar(20)   | 身分證字號 |                          |
|     |      | Birthday   | nvarchar(20)  | 出生年月日 |                          |
|     |      | Other      | nvarchar(max) | 備用欄位   | 就是備用                 |
|     |      | CreateTime | datetime      | 新增時間   | 就是新增時間             |

```sql
create table dbo.EOP_IdMapping
(
    UniqId     nvarchar(50)  not null
        constraint PK_EOP_IdMapping
            primary key,
    Type       varchar(2)    not null,
    BatchId    nvarchar(20)  not null,
    Idno       varchar(20)   not null,
    Birthday   nvarchar(20)  not null,
    Other      nvarchar(max) not null,
    CreateTime datetime      not null
)
go

```