### 呼叫 BNS08508100 的 帳戶留存資料表(EOP_AccountList)

供舊戶查詢薪轉帳號 BNS00040007 時 使用

| PK  | 欄位        | 型態         | 說明       | 代碼說明  |
| --- | ----------- | ------------ | ---------- | --------- |
| v   | UniqId      | nvarchar(50) | 序號       | 使用 ULID |
| v   | Account     | nvarchar(16) | 台幣帳戶   |           |
|     | AccountType | nvarchar(20) | 帳號產品別 |           |
|     | CreateTime  | dateTime     | 建立時間   |           |

```sql
create table dbo.EOP_AccountList
(
    UniqId      nvarchar(50) not null,
    Account     nvarchar(20) not null,
    AccountType nvarchar(20) not null,
    CreateTime  datetime     not null,
    constraint PK_EOP_AccountList
        primary key (UniqId, Account)
)
go
```