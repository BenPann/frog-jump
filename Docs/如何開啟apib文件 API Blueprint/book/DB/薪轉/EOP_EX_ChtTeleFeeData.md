### 開戶-附加服務-中華電信資料 (EOP_EX_ChtTeleFeeData)

| PK  | NULL | 欄位       | 型態         | 說明       | 代碼說明       |
| --- | ---- | ---------- | ------------ | ---------- | -------------- |
| V   |      | UniqId     | nvarchar(50) | 序號       | 使用 ULID      |
| V   |      | SubSerial  | int          | 子序號     | 使用流水號     |
|     |      | Status     | varchar(2)   | 狀態       | 紀錄是否已送出 |
|     |      | CampCode   | nvarchar(50)   | 營用處代號 |                |
|     |      | UserNo     | nvarchar(50)   | 用戶號     |                |
|     |      | CreateTime | datetime     | 新增時間   |                |
|     | V    | UpdateTime | datetime     | 修改時間   |                |
|     | V    | SendedTime | datetime     | 送出時間   |                |

#### Status - 案件主狀態

| 代碼 | 中文     |
| ---- | -------- |
| 01   | 編輯中   |
| 02   | 編輯完成 |
| 11   | 送件中   |
| 12   | 送件完成 |


```sql
create table dbo.EOP_EX_ChtTeleFeeData
(
    UniqId     nvarchar(50) not null,
    SubSerial  int          not null,
    Status     varchar(2)   not null,
    CampCode   nvarchar(50)   not null,
    UserNo     nvarchar(50)  not null,
    CreateTime datetime     not null,
    UpdateTime datetime,
    SendedTime datetime,
    constraint PK_EOP_EX_ChtTeleFeeData
        primary key (UniqId, SubSerial)
)
go

```
