### DB設定檔  (Config)
- 此Table在系統內為唯讀

| PK  | NULL | 欄位        | 型態           | 說明       | 代碼說明 |
| --- | ---- | ----------- | -------------- | ---------- | -------- |
| v   |      | KeyName     | nvarchar(50)   | 設定檔名稱 |          |
|     |      | KeyValue    | nvarchar(1000) | 設定檔內容 |          |
|     | V    | Description | nvarchar(50)   | 設定檔描述 |          |
