### 案件資料 (CaseData)

| PK  | 欄位            | 型態          | 說明                  | 代碼說明                                |
| --- | --------------- | ------------- | --------------------- | --------------------------------------- |
| v   | CaseNo          | nvarchar(50)  | 案件編號(數位信貸)    | 範例: CASEYYYYMMDD000001                |
|     | CaseNoWeb       | nvarchar(50)  | 案件編號(APS)         | APS 的案件編號 (ADD_NEW_CASE 產的)      |
|     | CaseNoAps       | nvarchar(50)  | APS 真正的案件編號    | APS 起案之後會來更新                    |
|     | GmCardNo        | nvarchar(50)  | 現金卡系統案件編號    |                                         |
|     | Idno            | nvarchar(50)  | 身分證字號或護照號碼  |                                         |
|     | Status          | char(2)       | 案件狀態              |                                         |
|     | ApsStatus       | varchar(2)    | APS 傳來的案件狀態    |                                         |
|     | IsPreAudit      | char(1)       | 是否預核              | 1:預核 2:非預核                         |
|     | ProductId       | nvarchar(50)  | 產品名稱              |                                         |
|     | Occupation      | char(2)       | 職業類別              |                                         |
|     | customer_name   | nvarchar(20)  | 姓名                  | 以下欄位皆為對應 WEB API 欄位           |
|     | birthday        | nchar(8)      | 生日                  |                                         |
|     | gender          | char(1)       | 性別                  | 1:男 2:女                               |
|     | house_city_code | nvarchar(6)   | 居住地行政區代碼      |                                         |
|     | house_address   | nvarchar(150) | 居住地址              |                                         |
|     | house_tel_area  | nvarchar(4)   | 住宅電話區碼          |                                         |
|     | house_tel       | nvarchar(30)  | 住宅電話              |                                         |
|     | education       | char(1)       | 教育程度              |                                         |
|     | marriage        | char(1)       | 婚姻狀況              |                                         |
|     | corp_name       | nvarchar(100) | 公司名稱              |                                         |
|     | corp_city_code  | nvarchar(6)   | 公司地址行政區代碼    |                                         |
|     | corp_address    | nvarchar(150) | 公司地址              |                                         |
|     | corp_tel_area   | nvarchar(4)   | 公司電話區碼          |                                         |
|     | corp_tel        | nvarchar(30)  | 公司電話              |                                         |
|     | corp_tel_exten  | nvarchar(6)   | 公司分機              | 畫面加上此欄位                          |
|     | corp_class      | char(1)       | 公司類別              |                                         |
|     | corp_type       | char(4)       | 行業別                | 帶空字串                                |
|     | title           | nvarchar(60)  | 職務名稱              |                                         |
|     | on_board_date   | char(6)       | 到職日                |                                         |
|     | yearly_income   | int           | 年收入                | (單位：萬元)                            |
|     | estate_type     | char(1)       | 不動產狀況            | (1.本人所有 2.配偶所有 3.家族所有 4.無) |
|     | p_apy_amount    | int           | 貸款申請金額          | (單位：萬元)                            |
|     | mobile_tel      | nvarchar(14)  | 行動電話              |                                         |
|     | p_period        | int           | 貸款期間              |                                         |
|     | p_purpose       | int           | 資金用途              |                                         |
|     | p_purpose_name  | nvarchar(50)  | 資金用途其他          |                                         |
|     | email_address   | varchar(120)  | EMAIL 地址            |                                         |
|     | ip_address      | varchar(20)   | 使用者 IP             |                                         |
|     | prj_code        | varchar(4)    | 專案代號              |                                         |
|     | case_no_est     | varchar(20)   | 預審案件編號          |                                         |
|     | ContactMe       | char(1)       | 客戶勾選與我聯絡      | 1:不勾選 2:勾選                         |
|     | ContactMeFlag   | char(1)       | 是否送給 TM           | 0:還沒 1:已送出                         |
|     | AddPhotoFlag    | char(1)       | 圖檔是否上傳影像      | 0:還沒 1:已送出                         |
|     | OccupatioFlag   | nvarchar(50)  | 職業類別 Aps 檢核結果 |                                         |
|     | ImportantCust   | char(1)       | 是否為重要客戶        | Y:重要客戶 N:非重要客戶                 |
|     | AMLFlag         | char(1)       | AML 回傳結果          |                                         |
|     | UTime           | datetime      | 更新時間              |                                         |
|     | UserType        | char(1)       | 使用者類別           | 0: 新戶 1:信用卡戶 2:存款戶 3:純貸款戶     |
|     | ResAddrZipCode | nvarchar(6) | 戶藉資料 | |
|     | ResAddr | nvarchar(200) | 戶藉資料 | |
|     | ResTelArea | nvarchar(4) | 戶藉資料 | |
|     | ResTel | nvarchar(30) | 戶藉資料 | |
|     | AgentNo | nvarchar(50) | 業務員代號 | |
|     | AgentDepart | nvarchar(50) | 業務員部門 | |
|     | CreateTime | datetime | CreateTime | |
|     | EndTime | datetime | EndTime | |
|     | SendedTime | datetime | SendedTime | |
|     | SubStatus | nvarchar(50) | 副狀態 | |
|     | Process | nvarchar(6) | SubStatus | |
|     | IdCardDate | datetime | 身份證換發日期 | |
|     | IdCardLocation | nvarchar(50) | 身份證換發地點 | |
|     | IdCardCRecord | nvarchar(1) | 身份證換發紀錄 | 0:初領 1:補領 2:換發 |
|     | AddNewCaseTime | datetime | 1. 發動起案時間 | |
|     | SendCustInfoTime | datetime | 2. 送詳細資料時間 | |
|     | HasOldFinaImg | nvarchar(5) | 3. 行內是否有舊財力 | 00:無, 01:有 |
|     | OldFinaImgNoList | nvarchar(400) | 4. 舊財力影編代號列表 | |
|     | CanUseOldFinaImg | nvarchar(5) | 5. 舊財力可否使用 | 00:不可, 01:可 |
|     | HasOldIdImg | nvarchar(5) | 6. 行內是否有舊ID | 00:無, 01:有 |
|     | OldIdImgNoList | nvarchar(400) | 7. 舊ID影編代號列表 | |
|     | CanUseOldIdImg | nvarchar(5) | 8. 舊ID可否使用 | 00:不可, 01:可 |
|     | HasUploadId | nvarchar(5) | 9. 是否已透過UI上傳ID | 00:無, 01:有, 02:無須上傳 |
|     | HasUploadFina | nvarchar(5) | 10. 是否已已透過UI上傳財力 | 00:無, 01:有, 02:無須上傳 |
|     | HasQryAml | nvarchar(5) | 11. 是否已查詢AML | 00:否, 01:是 |
|     | HasQryIdInfo | nvarchar(5) | 12. 是否已查詢戶役政 | 00:否, 01:是 |
|     | HasQryZ07 | nvarchar(5) | 13. 是否已查詢Z07 | 00:否, 01:是 |
|     | HasFinishKyc | nvarchar(5) | 14. 是否已產生KYC表 | 00:否, 01:是 |
|     | IsCaseIntegrated | nvarchar(5) | 15. 案件是否已完整 | 00:否, 01:是 |

### 備註

* 案件狀態 (01:填寫中 02:填寫完成 11:送件中 12: 送件完成 21:APS 作業中 22:APS 結案中 91:待立約 92:立約送出 93 立約完成)

- 結案狀態(APS 更新)
  * 拒貸
  * 系統自動結案

* 公司類別 (1.政府機關 2.教育機關 3.上市/上櫃/知名大企業 4.一般企業 5.小公司/獨資行號)
