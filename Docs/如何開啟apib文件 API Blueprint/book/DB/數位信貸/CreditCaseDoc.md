### 信用卡案件要上傳到影像的PDF (CreditCaseDoc)

| PK  | 欄位       | 型態         | 說明                 | 代碼說明                                         |
| --- | ---------- | ------------ | -------------------- | ------------------------------------------------ |
| v   | UniqId     | nvarchar(50) | 序號                 | CaseNo 或 PLNo 或 ConNo 或CreditNo               |
|     | UniqType   | char(2)      | 唯一代號類型         | 01 : 體驗 02: 申請 03:立約 04:專人聯絡 05:信用卡 |
|     | idno       | nvarchar(50) | 身分證字號或護照號碼 |                                                  |
|     | PdfContent | varchar(max) | PDF 內容             |                                                  |
