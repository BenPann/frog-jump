### 跨售AUM資料 (CS_AumData)
#### 資料只存原始資料 不存計算過後的資料

| PK  | NULL | 欄位               | 型態         | 說明         | 代碼說明                                         |
| --- | ---- | ------------------ | ------------ | ------------ | ------------------------------------------------ |
| v   |      | UniqId             | nvarchar(50) | 序號         | CaseNo 或 PLNo 或 ConNo 或CreditNo               |
|     |      | UniqType           | char(2)      | 唯一代號類型 | 01 : 體驗 02: 申請 03:立約 04:專人聯絡 05:信用卡 |
| v   |      | Currency           | varchar(20)  | 幣別         | TWD : 台幣, USD : 美金, RMB:人民幣               |
|     | V    | CurrentStock       | nvarchar(50) | 現股庫存     |                                                  |
|     | V    | FinancingBalance   | nvarchar(50) | 融資餘額     |                                                  |
|     | V    | BorrowStockBalance | nvarchar(50) | 融券餘額     |                                                  |
|     | V    | TotalFinancial     | nvarchar(50) | 合計財力價值 |                                                  |
|     | V    | ExchangeRate       | varchar(20)  | 匯率         | 計算當時的匯率                                   |
|     |      | UpdateTime         | datetime     | 更新時間     |
|     |      | CreateTime         | datetime     | 新增時間     |                                                  |