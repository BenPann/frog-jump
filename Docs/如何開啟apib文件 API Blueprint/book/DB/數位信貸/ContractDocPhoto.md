### 案件契約文件轉圖片的資料表(ContractDocPhoto)

| PK  | 欄位  | 型態          | 說明                | 代碼說明                 |
| --- | ----- | ------------- | ------------------- | ------------------------ |
| v   | ConNo | nvarchar(50)  | 立約編號            | 範例: CON-YYYYMMDD-00001 |
|     | Page  | int           | 頁碼                |                          |
|     | Image | nvarchar(max) | 契約書 PDF 內容圖片 |                          |
