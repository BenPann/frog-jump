### 驗證紀錄 (VerifyLog)
- 此Table為紀錄各個系統驗證結果以及驗證次數

| PK  | NULL | 欄位       | 型態           | 說明           | 代碼說明                                         |
| --- | ---- | ---------- | -------------- | -------------- | ------------------------------------------------ |
| v   |      | UniqId     | nvarchar(50)   | 序號           | CaseNo 或 PLNo 或 ConNo 或CreditNo               |
|     |      | UniqType   | char(2)        | 唯一代號類型   | 01 : 體驗 02: 申請 03:立約 04:專人聯絡 05:信用卡 |
| v   |      | VerifyType | nvarchar(20)   | 驗證類型       | Cht:中華電信 ,NCCC,PCode2566                     |
| v   |      | Count      | int            | 驗證次數       | 流水號數字 1,2,3 (錯誤次數就從Count去判斷)       |
|     |      | Request    | nvarchar(max)  | 驗證傳遞參數   | 需求RawData                                      |
|     |      | Response   | nvarchar(max)  | 驗證回應參數   | 回應RawData                                      |
|     |      | CheckCode  | nvarchar(20)   | 回覆代碼       | 拆解Response的CheckCode                          |
|     |      | AuthCode   | nvarchar(20)   | 授權碼或驗證碼 | 目前是NCCC才有回應的授權碼                       |
|     |      | Other      | nvarchar(1000) | 其他           | 如有其他欄位需求 先以Json方式塞進此欄位          |
|     |      | CreateTime | datetime       | 新增時間       | 就是驗證時間                                     |