###連接資訊 Table (EntryData)

| PK  |NULL | 欄位     | 型態           | 說明             | 代碼說明                               |
| --- |---- | -------- | -------------- | ---------------- | -------------------------------------- |
|     |     | UniqId   | nvarchar(50)   | 連線資訊唯一代號 | 可能為 CaseNo,ExpNo,ConNo              |
|     |v    | UniqType | char(2)        | 唯一代號類型     | 01 : 體驗 02: 申請 03:立約             |
|     |     | Entry    | nvarchar(100)  | 連結入口         | 客戶由哪個入口進入(ex:Pchome)          |
|     |     | Member   | nvarchar(100)   | 推薦人           | 推薦人                                 |
|     |v    | Process  | nvarchar(50)   | 推薦進入流程     | 推薦使用哪一條流程 (ex:申請,試算,立約) |
|     |v    | Browser  | nvarchar(50)   | 客戶使用瀏覽器   |                                        |
|     |     | Platform | nvarchar(50)   | 客戶使用平台     |                                        |
|     |v    | OS       | nvarchar(50)   | 客戶作業系統     |                                        |
|     |v    | Other    | nvarchar(1024) | 其餘參數         | 除了確定要使用的參數以外 先記錄在此    |
|     |     | UTime    | datetime       | 更新時間         |                                        |
