### 信用卡產品列表 (CreditCardProduct)

| PK  | NULL | 欄位        | 型態          | 說明         | 代碼說明 |
| --- | ---- | ----------- | ------------- | ------------ | -------- |
| v   |      | CardSerial  | int           | 卡片唯一序號 |          |
|     |      | ProductId   | nvarchar(50)  | 產品 ID      |          |
|     |      | CardType    | nvarchar(50)  | 卡片類型     |          |
|     |      | CardTitle   | nvarchar(50)  | 卡片標題     |          |
|     |      | CardContent | nvarchar(max) | 卡片內容     |          |
|     |      | CardImage   | nvarchar(max) | 卡片圖片     |          |
|     |      | StartDate   | datetime      | 上架開始時間 |          |
|     |      | EndDate     | datetime      | 上架結束時間 |          |
|     |      | CardStatus  | char(2)       | 狀態         |
|     |      | CreateUser  | nvarchar(50)  | 建立使用者   |          |
|     |      | CreateTime  | datetime      | 建立時間     |          |
|     |      | UpdateUser  | nvarchar(50)  | 更新使用者   |          |
|     |      | UpdateTime  | datetime      | 更新時間     |          |
|     |      | ApproveUser | nvarchar(50)  | 放行使用者   |          |
|     |      | ApproveTime | datetime      | 放行時間     |          |

#### CardStatus - 狀態

| 代碼 | 中文       |
| ---- | ---------- |
| 01   | 編輯中     |
| 02   | 待送審     |
| 03   | 送審中     |
| 04   | 上架       |
| 05   | 下架送審中 |
| 06   | 下架       |
| 07   | 結案       |
| 08   | 退件       |
