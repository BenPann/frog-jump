###資料填寫品質 Table (DataQual2)

| PK  | 欄位     | 型態         | 說明             | 代碼說明                      |
| --- | -------- | ------------ | ---------------- | ----------------------------- |
| v   | UniqId   | nvarchar(50) | 連線資訊唯一代號 | 可能為 CaseNo,ExpNo,ConNo     |
|     | UniqType | char(2)      | 唯一代號類型     | 01 : 體驗 02: 申請 03:立約    |
| v   | DataName | nvarchar(50) | 資料名稱         | 貸款: loan.*  信用卡 credit.* |
|     | Qual     | int          | CIF是否有帶資料  |                               |

- 此 Table 所有欄位 如果填寫資料的時候 有帶該欄位 資料就為 1 沒帶就為 0