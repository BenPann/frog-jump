### 案件結案 (CaseEnd)
#### 目前只有在APS自動結案使用
| PK  | 欄位        | 型態          | 說明               | 代碼說明                             |
| --- | ----------- | ------------- | ------------------ | ------------------------------------ |
| v   | CaseNo      | nvarchar(50)  | 案件編號(數位信貸) | 範例: CASE-YYYYMMDD-00001            |
|     | EndReasonId | nvarchar(50)  | 結案代號           | 01:拒貸 ,02:30天自動結案,03:正常結案 |
|     | EndReason   | nvarchar(200) | 結案原因           | 30 天自動結案                        |
|     | EndSystem   | nvarchar(50)  | 結案系統           | APS 結案                             |
|     | UTime       | datetime      | 更新時間           |                                      |
