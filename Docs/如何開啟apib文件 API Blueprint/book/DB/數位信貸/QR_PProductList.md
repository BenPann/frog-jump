### 推廣產品名稱列表(QR_PProductList)
| PK  | NULL | 欄位         | 型態          | 說明           | 代碼說明           |
| --- | ---- | ------------ | ------------- | -------------- | ------------------ |
| V   |      | PProductId   | nvarchar(20)   | 推廣產品主Key  | 0:個人信貸 1:E貸寶 |  |
|     |      | PProductName | nvarchar(200) | 推廣產品名稱   | 個人信貸 E貸寶     |
|     |      | Enable       | char(1)   | 此產品啟用狀態 | 1:啟用、0:停用     |

### 推廣產品明細內容
### 就算此Table為空 畫面上還是會顯示部指定的欄位