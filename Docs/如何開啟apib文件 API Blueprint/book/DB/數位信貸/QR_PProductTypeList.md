### 可推廣產品列表(QR_PProductTypeList)

| PK  | NULL | 欄位             | 型態          | 說明                   | 代碼說明                                             |
| --- | ---- | ---------------- | ------------- | ---------------------- | ---------------------------------------------------- |
| V   |      | PProductType     | varchar(20)   | 可推廣產品類型         | 0:貸款; 1:信用卡;                                    |
|     |      | PProductTypeName | nvarchar(200) | 推廣產品類型名稱       | 貸款  信用卡                                         |
|     | V    | Url              | nvarchar(200) | 此產品的相對應網址     | ex:/apply/init,/applycc/init ,外部的產品類型可以為空 |
|     |      | Enable           | char(1)       | 可推廣產品類型啟用狀態 | 1:啟用、0:停用                                       |
|     |      | Sort             | nvarchar(50)  | 排序                   |

### 貸款產品的ID跟Name的對應表