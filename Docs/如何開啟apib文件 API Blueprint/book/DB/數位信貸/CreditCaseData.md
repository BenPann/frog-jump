### 信用卡案件資料 (CreditCaseData)

| PK  | NULL | 欄位                | 型態          | 說明                 | 代碼說明                                                                  |
| --- | ---- | ------------------- | ------------- | -------------------- | ------------------------------------------------------------------------- |
| v   |      | UniqId              | nvarchar(50)  | 序號                 | CRED 開頭或 CASE 開頭                                                     |
|     |      | UniqType            | char(2)       | 唯一代號類型         | 01 : 體驗 02: 申請 03:立約 04:專人聯絡 05:信用卡 12:薪轉 13:數三 14: 中壽 |
|     | V    | CreditNoAps         | nvarchar(50)  | APS 的信用卡編號     |                                                                           |
|     |      | Status              | char(2)       | 案件狀態             |                                                                           |
|     |      | ApsStatus           | char(2)       | APS 的案件狀態       | 1:不核卡 2:核卡                                                           |
|     |      | Idno                | nvarchar(50)  | 身分證字號或護照號碼 |                                                                           |
|     |      | UserType            | char(1)       | 使用者類別           | 0: 新戶 1:信用卡戶 2:存款戶 3:純貸款戶                                    |
|     |      | Phone               | nvarchar(50)  | 手機號碼             |                                                                           |
|     |      | ProductId           | nvarchar(50)  | 產品名稱             | 信用卡代號                                                                |
|     |      | ChtName             | nvarchar(50)  | 中文姓名             |                                                                           |
|     | V    | EngName             | nvarchar(100) | 英文姓名             |                                                                           |
|     |      | Birthday            | nchar(8)      | 生日                 |                                                                           |
|     |      | Gender              | char(1)       | 性別                 | 1:男 2:女                                                                 |
|     |      | IdCardDate          | datetime      | 身份證換發日期       |                                                                           |
|     |      | IdCardLocation      | nvarchar(50)  | 身份證換發地點       |                                                                           |
|     |      | IdCardCRecord       | char(2)       | 身份證換發紀錄       | 0:初領 1:補領 2:換發                                                      |
|     |      | ResAddrZipCode      | nvarchar(6)   | 戶籍地址區碼         |                                                                           |
|     |      | ResAddr             | nvarchar(200) | 戶籍地址             |                                                                           |
|     |      | CommAddrZipCode     | nvarchar(6)   | 通訊地址區碼         |                                                                           |
|     |      | CommAddr            | nvarchar(200) | 通訊地址             |                                                                           |
|     |      | HomeAddrZipCode     | nvarchar(6)   | 居住地址區碼         |                                                                           |
|     |      | HomeAddr            | nvarchar(200) | 居住地址             |                                                                           |
|     |      | BillAddrZipCode     | nvarchar(6)   | 帳單地址區碼         |                                                                           |
|     |      | BillAddr            | nvarchar(200) | 帳單地址             |                                                                           |
|     |      | SendCardAddrZipCode | nvarchar(6)   | 寄卡地址區碼         |                                                                           |
|     |      | SendCardAddr        | nvarchar(200) | 寄卡地址             |                                                                           |
|     |      | Education           | char(1)       | 教育程度             |                                                                           |
|     |      | Marriage            | char(1)       | 婚姻狀態             |                                                                           |
|     |      | EmailAddress        | varchar(120)  | EMAIL 地址           |                                                                           |
|     |      | ResTelArea          | nvarchar(4)   | 戶籍電話區碼         |                                                                           |
|     |      | ResTel              | nvarchar(30)  | 戶籍電話             |                                                                           |
|     |      | HomeTelArea         | nvarchar(4)   | 居住電話區碼         |                                                                           |
|     |      | HomeTel             | nvarchar(30)  | 居住電話             |                                                                           |
|     |      | HomeType            | char(1)       | 居住房屋類型         | 0:自有宅 1:租房                                                           |
|     |      | HomeOwner           | char(1)       | 不動產所有           | 0:自有 1:國宅 2:租賃                                                      |
|     |      | CorpName            | nvarchar(100) | 公司名稱             |                                                                           |
|     |      | CorpTelArea         | nvarchar(4)   | 公司電話區碼         |                                                                           |
|     |      | CorpTel             | nvarchar(30)  | 公司電話             |                                                                           |
|     |      | CorpTelExten        | nvarchar(6)   | 公司電話分機         |                                                                           |
|     |      | CorpAddrZipCode     | nvarchar(6)   | 公司地址區碼         |                                                                           |
|     |      | CorpAddr            | nvarchar(200) | 公司地址             |                                                                           |
|     |      | Occupation          | char(2)       | 職業類別             |                                                                           |
|     |      | CorpDepart          | nvarchar(50)  | 公司部門             |                                                                           |
|     |      | JobTitle            | nvarchar(100) | 職稱                 |                                                                           |
|     |      | YearlyIncome        | int           | 年收入               | (單位：萬元)                                                              |
|     |      | OnBoardDate         | char(6)       | 到職日               |                                                                           |
|     |      | SendType            | char(1)       | 信用卡帳單型態       | 1:實體帳單 2:電子帳單 4:行動帳單                                          |
|     |      | FirstPresent        | varchar(50)   | 首刷禮               | 跟另一個 Table 對應                                                       |
|     |      | IpAddress           | varchar(20)   | IP                   |                                                                           |
|     |      | DataUse             | char(2)       | 同意資料使用         | 01:同意 02:不同意                                                         |
|     |      | CreateTime          | datetime      | 新增時間             |                                                                           |
|     |      | UpdateTime          | datetime      | 更新時間             |                                                                           |

#### Status - 案件狀態

| 代碼 | 中文       |
| ---- | ---------- |
| 01   | 填寫中     |
| 02   | 填寫完成   |
| 11   | 送件中     |
| 12   | 送件完成   |
| 21   | APS 作業中 |
| 22   | APS 結案中 |
| 91   | 待立約     |
| 92   | 立約送出   |
| 93   | 立約完成   |
