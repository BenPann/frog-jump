### 案件推廣單位跟人員(使用者輸入) (QR_PromoCaseUserInput)

| PK  | NULL | 欄位        | 型態         | 說明         | 代碼說明                                      |
| --- | ---- | ----------- | ------------ | ------------ | --------------------------------------------- |
| V   |      | UniqId      | nvarchar(50) | 唯一序號     | CaseNo 或 ExpNo 或 ConNo 或CreditNo           |
|     |      | UniqType    | char(2)      | 唯一代號類型 | 01:體驗 02:申請 03:立約 04:專人聯絡 05:信用卡 |
|     |      | PromoDepart | nvarchar(50) | 推廣單位     |                                               |
|     |      | PromoMember | nvarchar(50) | 推廣人員     |                                               |
|     |      | ChannelId   | nvarchar(50) | 通路來源    |                                               |