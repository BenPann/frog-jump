###資料填寫品質 Table (DataQual)

| PK  | 欄位            | 型態         | 說明               | 代碼說明                   |
| --- | --------------- | ------------ | ------------------ | -------------------------- |
| v   | UniqId          | nvarchar(50) | 連線資訊唯一代號   | 可能為 CaseNo,ExpNo,ConNo  |
|     | UniqType        | char(2)      | 唯一代號類型       | 01 : 體驗 02: 申請 03:立約 |
|     | EMail           | int          | 電子郵件地址       |                            |
|     | Addr            | int          | 現居地址           |                            |
|     | Phone           | int          | 居住電話           |                            |
|     | Education       | int          | 教育程度           |                            |
|     | Marriage        | int          | 婚姻狀況           |                            |
|     | Estate          | int          | 不動產             |                            |
|     | CompanyName     | int          | 公司名稱           |                            |
|     | CompanyAddr     | int          | 公司地址           |                            |
|     | CompanyPhone    | int          | 公司電話           |                            |
|     | Title           | int          | 職稱               |                            |
|     | OnBoardDate     | int          | 到職日             |                            |
|     | YearInCome      | int          | 年收入             |                            |
|     | OCRCompanyName  | int          | OCR 的公司名稱     |                            |
|     | OCRCompanyAddr  | int          | OCR 的公司地址     |                            |
|     | OCRCompanyZip   | int          | OCR 的公司郵遞區號 |                            |
|     | OCRCompanyPhone | int          | OCR 的公司電話     |                            |
|     | OCRTitle        | int          | OCR 的職稱         |                            |
|     | UTime           | datetime     | 更新時間           |                            |

此 Table 所有欄位 如果填寫資料的時候 有帶該欄位 資料就為 1 沒帶就為 0

###資料填寫品質追蹤專用報表
