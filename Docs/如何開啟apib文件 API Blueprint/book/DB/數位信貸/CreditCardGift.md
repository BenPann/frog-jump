### 信用卡首刷禮列表 (CreditCardGift)

| PK  | NULL | 欄位        | 型態         | 說明           | 代碼說明 |
| --- | ---- | ----------- | ------------ | -------------- | -------- |
| v   |      | GiftSerial  | int          | 首刷禮唯一序號 |          |
|     |      | GiftTitle   | nvarchar(50) | 首刷禮標題     |          |
|     |      | GiftNote    | nvarchar(50) | 首刷禮說明     |          |
|     |      | GiftCode    | nvarchar(50) | 首刷禮代號     |          |
|     |      | GiftImage   | nvarchar(50) | 首刷禮圖片     |          |
|     |      | GiftStatus  | nvarchar(50) | 狀態           |          |
|     |      | StartDate   | datetime     | 上架開始時間   |          |
|     |      | EndDate     | datetime     | 上架結束時間   |          |
|     |      | CreateUser  | nvarchar(50) | 建立使用者     |          |
|     |      | CreateTime  | datetime     | 建立時間       |          |
|     |      | UpdateUser  | nvarchar(50) | 更新使用者     |          |
|     |      | UpdateTime  | datetime     | 更新時間       |          |
|     |      | ApproveUser | nvarchar(50) | 放行使用者     |          |
|     |      | ApproveTime | datetime     | 放行時間       |          |
|     |      | SuspendUser | nvarchar(50) | 暫停使用者     |          |
|     |      | SuspendTime | datetime     | 暫停時間       |          |

#### GiftStatus - 首刷禮狀態

| 代碼 | 中文       |
| ---- | ---------- |
| 01   | 編輯中     |
| 02   | 送審待放行 |
| 03   | 送審待放行 |
| 04   | 放行完成   |
| 05   | 送審待下架 |
| 06   | 下架停用   |
| 08   | 退件       |
