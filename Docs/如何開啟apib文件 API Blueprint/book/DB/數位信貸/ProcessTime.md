###案件的操作時間(ProcessTime)
|PK|欄位|型態|說明|代碼說明|
|--|--|--|--|--|
| v | Serial | int | 序號 | 由資料庫自動產生 | |
| | UniqId | nvarchar(50) | 流程唯一代號 |可能為 CaseNo,ExpNo,ConNo |
| | UniqType | char(2) | 唯一代號類型| 01 : 體驗 02: 申請 03:立約 |
| | StartTime | datetime | 流程開始時間 | |
| | EndTime | datetime | 流程結束時間 | |
| | UpdateTime | datetime | 最後操作時間 | |
