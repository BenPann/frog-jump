###貸款產品主檔(LoanProduct)
|PK|欄位|型態|說明|代碼說明|
|--|--|--|--|--|
| v | Serial | int | 序號, 由資料庫自動產生 | |
| | ProductName | nvarchar(50) | 產品名稱 | |
| | Version | nvarchar(20) | 版本號 | |
| | ListViewImage | nvarchar(max) | 首頁列表視覺圖 |
| | MainViewImage | nvarchar(max)| 主視覺圖 | |
| | MainViewMobileImage | nvarchar(max)| 主視覺圖手機版 | |
| | MainContent | nvarchar(max) | 主要文字內容| |
| | ApplyUrl | nvarchar(300) | 申請此產品的網址| |
| | Status | nvarchar(20) | 條款狀態 |(01:編輯中,02:送出待放行,03:放行完成,07:重新上架待放行,08:停用,09:退件) |
| | AddedTime| datetime| 上版時間 | |
| | CreateUser | nvarchar(50) | 新增的使用者 | |
| | CreateTime | datetime | 新增時間 | |
| | UpdateUser | nvarchar(50) | 更新的使用者 | |
| | UpdateTime | datetime | 更新時間 | |
| | ApproveUser | nvarchar(50) | 放行的使用者 | |
| | ApproveTime | datetime | 放行時間 | |

##只有編輯中可以刪除 
