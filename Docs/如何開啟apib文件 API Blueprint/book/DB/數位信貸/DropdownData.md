###下拉選單資料表 (DropdownData)

| PK  | 欄位     | 型態         | 說明          | 代碼說明                                  |
| --- | -------- | ------------ | ------------- | ----------------------------------------- |
| v   | Name     | nvarchar(50) |               | 下拉選單群組名稱(如 Marriage)             |
| v   | DataKey  | nvarchar(50) | 選項的 key 值 | 要設定與要後送的 XML 中相同的欄位代碼一樣 |
|     | DataName | nvarchar(50) | 選項的中文名  |                                           |
|     | Enable   | int          | 是否顯示      | 0:不顯示 1:顯示                           |
|     | Sort     | int          | 排序用的值    |                                           |
