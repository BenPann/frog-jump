### 短網址資料產生器 (QR_ShortUrl)

| PK  | NULL | 欄位         | 型態         | 說明               | 代碼說明                          |
| --- | ---- | ------------ | ------------ | ------------------ | --------------------------------- |
| v   |      | ShortUrl     | nvarchar(50) | 短網址             | 範例: 1ad3f43f                    |
|     |      | ChannelId    | nvarchar(50) | 公司別             | 對應 QR_ChannelList               |
|     |      | DepartId     | nvarchar(50) | 推廣單位或活動代號 | 對應 QR_ChannelDepartList         |
|     |      | Member       | nvarchar(50) | 推薦人             | 員工編號或身分證字號              |
|     |      | PProductType | nvarchar(20) | 推薦產品類別       | 對應 QR_PProductTypeList          |
|     |      | PProductId   | nvarchar(1000) | 推薦產品ID         | 對應 QR_PProductList 空白為不指定 |
|     |      | Count        | int          | 使用次數           |                                   |
|     |      | CreateTime   | datetime     | 新增時間           |                                   |
|     |      | UpdateTime   | datetime     | 更新時間           |                                   |

### 使用單位加上推薦人當作判斷條件 一人最多產生三次
### 推薦產品類別決定使用者要導去哪個流程