### 入口Channel列表 (QR_ChannelList)


| PK  | NULL | 欄位            | 型態           | 說明                        | 代碼說明                                   |
| --- | ---- | --------------- | -------------- | --------------------------- | ------------------------------------------ |
| V   |      | ChannelId       | nvarchar(50)   | 異業合作時須約定的Channelid | 此欄位跟之前開的Entry同樣意思              |
|     |      | ChannelName     | nvarchar(200)  | 入口中文名                  |
|     |      | ChannelType     | char(1)        | Channel類型                 | 0:商銀; 1:集團非商銀; 2:異業廠商; 3:個人戶 |  |
|     |      | Enable          | char(1)        | 此Channel啟用狀態           | 1:啟用、0:停用                             |
|     | V    | ThemePath       | nvarchar(200)  | 此Channel使用的佈景Css路徑  | 前端依據此欄位取得css 空:預設值            |
|     | V    | DomainUrl       | nvarchar(200)  | 使用自訂網域                | 異業廠商會使用自訂網域                     |
|     |      | MBCFlag         | char(1)        | 是否包含MBC流程             | 0:否 1:可                                  |
|     |      | IdLogic         | char(1)        | ID欄位驗證方式              | 0:ID身分驗證 1:驗證數字 2:驗證英數字       |
|     |      | IdLength        | varchar(3)     | ID欄位長度限制              | 0~99 長度                                  |
|     |      | Sort            | nvarchar(50)   | 排序                        | 排序                                       |
|     |      | CanGen          | char(1)        | 是否可以產生QRCode          |                                            |
|     |      | GetCif          | char(1)        | 是否要反查CIF               |                                            |
|     |      | UrlType         | char(1)        | 網址類型(內網或外網)        |                                            |
|     |      | CifUrl          | nvarchar(2000) | 反查CIF的URL                |                                            |
|     |      | MemoUrl         | nvarchar(2000) | 反查MEMO的URL               |                                            |
|     |      | GetCifStartTime | datetime       | 取得CIF的開始時間           |                                            |
|     |      | GetCifEndTime   | datetime       | 取得CIF的結束時間           |                                            |

