###體驗紀錄資料表 (PLExp)

| PK  | 欄位        | 型態         | 說明                   | 代碼說明                 |
| --- | ----------- | ------------ | ---------------------- | ------------------------ |
| V   | ExpNo       | nvarchar(50) | 體驗編號               | 範例: EXP-YYYYMMDD-00001 |
|     | Phone       | nvarchar(50) | 手機號碼               |                          |
|     | Date        | char(8)      | 西元年月日             |                          |  |
|     | Name        | nvarchar(50) | 姓名                   |                          |  |
|     | CaseNoWeb   | nvarchar(50) | 案件編號(AIRLOAN 產的) |                          |
|     | CaseNo      | nvarchar(50) | 案件編號(KGI 產的)     |                          |
|     | QVer        | nvarchar(50) | 問題版本               |                          |
|     | AnsList     | nvarchar(50) | 問卷答案列表           |                          |
|     | ProjectCode | nvarchar(4)  | 專案代號               |                          |
|     | CreditLine  | int          | 建議額度               |                          |
|     | IntrestRate | decimal(6,3) | 建議利率               |                          |
|     | Rank        | int          | RANK                   |                          |
|     | Score       | decimal(8,3) | 分數                   |                          |
|     | UTime       | datetime     | 更新時間               |                          |

### 7 天內不能再使用試算功能 判斷用今天日期 往前推 7 天是否有紀錄
