### 案件文件(CaseDocument)

| PK  | 欄位         | 型態           | 說明           | 代碼說明                                                   |
| --- | ------------ | -------------- | -------------- | ---------------------------------------------------------- |
| v   | UniqId       | nvarchar(50)   | 序號           | CaseNo 或 PLNo 或 ConNo 或CreditNo                         |
|     | UniqType     | char(2)        | 唯一代號類型   | 01 : 體驗 02: 申請 03:立約 04:專人聯絡 05:信用卡           |
| V   | DocumentType | nvarchar(50)   | 文件類型       | 貸款PDF:Case.pdf 信用卡PDF:Credit.pdf 立約PDF:contract.pdf |
|     | Content      | varbinary(max) | 文件內容       |                                                            |
|     | Version      | nvarchar(50)   | 版本號         |                                                            |
|     | Other        | nvarchar(max)  | 其他欄位(備用) | 立約拿來放預覽Html用                                       |
|     | UpdateTime   | datetime       | 更新時間       |                                                            |
|     | CreateTime   | datetime       | 新增時間       |                                                            |