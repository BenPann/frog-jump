###呼叫 API 的 LOG 資料表(ApiLog)

| PK  | 欄位      | 型態          | 說明                   | 代碼說明 |
| --- | --------- | ------------- | ---------------------- | -------- |
| v   | Serial    | int           | 序號, 由資料庫自動產生 |          |
|     | ApiName   | nvarchar(200) | 呼叫的 API 名稱        |          |
|     | Request   | nvarchar(max) | 呼叫 API 的需求電文    |          |
|     | Response  | nvarchar(max) | API 的回應電文         |          |
|     | StartTime | datetime      | API 開始時間           |          |
|     | EndTime   | datetime      | API 結束時間           |          |
|     | UTime     | datetime      | 更新時間               |          |

###此 Log 為我們 Call 客戶的 Api 的 Log
