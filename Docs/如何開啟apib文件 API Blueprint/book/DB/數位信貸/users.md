###使用者(users)
| PK  | NULL | 欄位     | 型態         | 說明 | 代碼說明 |
| --- | ---- | -------- | ------------ | ---- | -------- |
|     |      | userId   | nvarchar(50) |      |          |
|     |      | userName | nvarchar(50) |      |          |
|     |      | userPwd  | nvarchar(50) |      |          |
|     | V    | FailCnt  | int          |      |          |