### 跨售取得API資料的設定 (CS_ApiSetting)

#### 暫時先開這樣 有需要再擴充

| PK  | NULL | 欄位      | 型態           | 說明                        | 代碼說明         |
| --- | ---- | --------- | -------------- | --------------------------- | ---------------- |
| V   |      | ChannelId | nvarchar(50)   | 異業合作時須約定的Channelid | 同QR_ChannelList |
|     |      | DomailUrl | nvarchar(100)  | Api的Domain網址             | www.google.com   |
|     |      | ApiUrl    | nvarchar(1000) | Domain後面的方法網址        | /test/api        |
