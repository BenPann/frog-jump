### 操作紀錄資料表(OPLog)

| PK  | 欄位                 | 型態          | 說明                         | 代碼說明    |
| --- | -------------------- | ------------- | ---------------------------- | ----------- |
| V   | opName               | nvarchar(50)  | 系統名稱                     | 例: Web,CMS |
| V   | opStatus             | nvarchar(50)  | 動作名稱                     |             |
| V   | opAccount            | nvarchar(20)  | 使用者帳號                   |
|     | http_client_ip       | nvarchar(50)  | 使用者 IP                    |             |
|     | http_x_forwarded_for | nvarchar(50)  | RequestHeader 中的 forwarded |             |
|     | remote_addr          | nvarchar(50)  | 使用者 IP                    |             |
|     | http_header          | nvarchar(max) | 完成 RequestHeader           |             |
|     | opDescribe           | nvarchar(max) | 詳細描述                     |             |
| V   | opClientDatetime     | datetime      | 點擊時間                     |             |
|     | modifyDatetime       | datetime      | 寫入時間                     |             |
