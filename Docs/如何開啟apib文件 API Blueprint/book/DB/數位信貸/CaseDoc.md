### 案件要上傳到影像的資料 (CaseDoc)

| PK  | 欄位       | 型態         | 說明                 | 代碼說明                  |
| --- | ---------- | ------------ | -------------------- | ------------------------- |
| v   | CaseNo     | nvarchar(50) | 案件編號             | 範例: CASE-YYYYMMDD-00001 |
|     | idno       | nvarchar(50) | 身分證字號或護照號碼 |                           |
|     | PdfContent | varchar(max) | PDF 內容             |                           |
|     | Image      | varchar(max) | 圖片內容             |                           |
