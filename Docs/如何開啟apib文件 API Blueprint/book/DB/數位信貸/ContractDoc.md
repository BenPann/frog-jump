### 案件契約文件資料表(ContractDoc)

| PK  | 欄位           | 型態          | 說明            | 代碼說明                 |
| --- | -------------- | ------------- | --------------- | ------------------------ |
| v   | ConNo          | nvarchar(50)  | 立約編號        | 範例: CON-YYYYMMDD-00001 |
|     | Version        | nvarchar(50)  | 契約書版本      |                          |
|     | UserSign       | nvarchar(max) | 使用者簽名      |                          |
|     | PreviewContent | nvarchar(max) | 契約書預覽內容  | 再確認                   |
|     | PdfContent     | nvarchar(max) | 契約書 PDF 內容 |                          |
