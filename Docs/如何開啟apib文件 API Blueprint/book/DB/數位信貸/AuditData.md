###審核資料(AuditData)

| PK  | 欄位        | 型態         | 說明                  | 代碼說明                   |
| --- | ----------- | ------------ | --------------------- | -------------------------- |
| v   | UniqId      | nvarchar(50) | 序號                  | CaseNo 或 PLNo 或 ConNo    |  |
|     | UniqType    | char(2)      | 唯一代號類型          | 01 : 體驗 02: 申請 03:立約 |
|     | ProjectCode | nvarchar(4)  | 專案代號              |                            |
|     | CreditLine  | nvarchar(10) | 建議額度              |                            |
|     | IntrestRate | decimal(6,3) | 建議利率              |                            |
|     | Rank        | varchar(3)   | RANK                  |                            |
|     | Score       | decimal(8,3) | 分數                  |                            |
|     | ShowScore   | char(1)      | 是否顯示額度利率(Y/N) |                            |
|     | UTime       | datetime     | 更新時間              |                            |
