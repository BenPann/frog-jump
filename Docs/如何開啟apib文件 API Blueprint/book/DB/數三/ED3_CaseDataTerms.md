### 紀錄客戶勾選條款資料表(ED3_CaseDataTerms)

| PK  | NULL | IDENTITY | 欄位       | 型態         | 說明            | 代碼說明               |
| --- | ---- | -------- | ---------- | ------------ | --------------- | ---------------------- |
| v   |      |          | UniqId     | nvarchar(50) | 序號            |                        |
| v   |      |          | TermSerial | int          | 條款序號        | 跟 Terms Table 做 對應 |
|     |      |          | IsAgree    | varchar(1)   | Y:同意 N:不同意 |                        |
|     |      |          | CreateTime | datetime     | 新增時間        |                        |

```sql
<!--
CREATE TABLE [dbo].[ED3_CaseDataTerms](
	[UniqId] [nvarchar](50) NOT NULL,
	[TermSerial] [int] NOT NULL,
	[IsAgree] [varchar](1) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ED3_CaseDataTerms] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[TermSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


-->
```
