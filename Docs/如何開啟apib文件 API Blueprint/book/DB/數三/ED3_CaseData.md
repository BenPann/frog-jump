### 開戶案件資料 (ED3_CaseData)

| PK  | NULL | IDENTITY | 欄位                  | 型態          | 說明                                   | 代碼說明                                                  |
| --- | ---- | -------- | --------------------- | ------------- | -------------------------------------- | --------------------------------------------------------- |
| V   |      |          | UniqId                | nvarchar(50)  | 序號                                   |                                                           |
|     | V    |          | UniqType              | varchar(1)    | 申請類別                               | 13: 數三 14: 中壽                                         |
|     | V    |          | UserType              | varchar(1)    | 客戶身分別                             | 0: 新戶 1:信用卡戶 2:存款戶 3:純貸款戶 4:其他 5:存戶+卡戶 |
|     | V    |          | Status                | varchar(2)    | 主狀態                                 | eOpenD3 系統使用的                                        |
|     | V    |          | Process               | varchar(1)    | 客戶流程                               | 1: 預約分行流程 2: 數位帳戶流程                           |
|     |      |          | Idno                  | nvarchar(20)  | 身分證號                               |                                                           |
|     |      |          | Birthday              | varchar(8)    | 生日                                   |                                                           |
|     | V    |          | Phone                 | nvarchar(50)  | 手機號碼                               | 不管新舊戶都存                                            |
|     | V    |          | IsTwTaxResident       | varchar(1)    | 是否台灣稅務居民                       | Y: 是 N: 否                                               |
|     | V    |          | PromoChannelID        | varchar(2)    | 推薦人身分                             | 跟 QR_ChannelList Table 做 對應                           |
|     | V    |          | PromoDepart           | nvarchar(50)  | 單位代號                               | 跟 QR_ChannelDepartList Table 做 對應                     |
|     | V    |          | PromoMember           | nvarchar(50)  | 員工代號                               |                                                           |
|     | V    |          | SetSecuritiesDelivery | varchar(1)    | 設定證劵交割戶                         | Y: 是 N: 否                                               |
|     | V    |          | SecuritiesAddr        | nvarchar(50)  | 證卷交割分行縣市                       |                                                           |
|     | V    |          | SecuritiesAddrZipCode | varchar(5)    | 證卷交割分行區碼                       |                                                           |
|     | V    |          | SecuritiesCode        | varchar(5)    | 證卷交割分行代碼                       |                                                           |
|     | V    |          | SecuritiesName        | nvarchar(50)  | 證卷交割分行名稱                       |                                                           |
|     | V    |          | ProductId             | varchar(2)    | 帳戶或帳加卡                           | 1: 純帳戶 2:帳戶加卡                                      |
|     | V    |          | ChtName               | nvarchar(50)  | 中文姓名                               |                                                           |
|     | V    |          | EngName               | nvarchar(100) | 英文姓名                               |                                                           |
|     |      |          | Gender                | varchar(1)    | 性別                                   | 1:男 2:女                                                 |
|     | V    |          | IdCardDate            | datetime      | 身份證換發日期                         |                                                           |
|     | V    |          | IdCardLocation        | nvarchar(50)  | 身份證換發地點                         |                                                           |
|     | V    |          | IdCardRecord          | varchar(2)    | 身份證換發紀錄                         | 1：領(初領) 2：補 3：換                                   |
|     | V    |          | ResAddrZipCode        | nvarchar(6)   | 戶籍地址區碼                           |                                                           |
|     | V    |          | ResAddrArea           | nvarchar(200) | 戶籍地址縣市鄉鎮區                     |                                                           |
|     | V    |          | ResAddr               | nvarchar(200) | 戶籍地址                               |                                                           |
|     | V    |          | CommAddrZipCode       | nvarchar(6)   | 通訊地址區碼                           |                                                           |
|     | V    |          | CommAddrArea          | nvarchar(200) | 通訊地址縣市鄉鎮區                     |                                                           |
|     | V    |          | CommAddr              | nvarchar(200) | 通訊地址                               |                                                           |
|     | V    |          | BirthPlace            | nvarchar(200) | 出生地                                 |                                                           |
|     | V    |          | Education             | varchar(1)    | 教育程度                               |                                                           |
|     | V    |          | Marriage              | varchar(1)    | 婚姻狀態                               | M : 已婚 S : 未婚                                         |
|     | V    |          | EmailAddress          | varchar(120)  | EMAIL 地址                             |                                                           |
|     | V    |          | SendType              | varchar(1)    | 對帳單寄送方式                         | 2:電子帳單 3:郵寄帳單                                     |
|     | V    |          | ResTelArea            | nvarchar(4)   | 戶籍電話區碼                           |                                                           |
|     | V    |          | ResTel                | nvarchar(30)  | 戶籍電話                               |                                                           |
|     | V    |          | HomeTelArea           | nvarchar(4)   | 居住電話區碼                           |                                                           |
|     | V    |          | HomeTel               | nvarchar(30)  | 居住電話                               |                                                           |
|     | V    |          | EstateType            | varchar(1)    | 不動產類型                             | 1.本人所有 2.配偶所有 3.家族所有 4.無                     |
|     | V    |          | Purpose               | varchar(2)    | 開戶目的                               |                                                           |
|     | V    |          | BranchID              | varchar(3)    | 帳務服務分行                           | 跟 ED3_BranchList Table 做 對應                           |
|     | V    |          | AuthorizeToallCorp    | char(1)       | 是否有共銷                             | Y:共銷 N:非共銷                                           |
|     | V    |          | AutVer                | char(20)      | 共銷版本號                             | 共銷版本號                                                |
|     | V    |          | Occupation            | nvarchar(10)  | 職業類別                               |                                                           |
|     | V    |          | CorpName              | nvarchar(100) | 公司名稱                               |                                                           |
|     | V    |          | CorpTelArea           | nvarchar(4)   | 公司電話區碼                           |                                                           |
|     | V    |          | CorpTel               | nvarchar(30)  | 公司電話                               |                                                           |
|     | V    |          | CorpAddrZipCode       | nvarchar(6)   | 公司地址區碼                           |                                                           |
|     | V    |          | CorpAddrArea          | nvarchar(200) | 公司地址縣市鄉鎮區                     |                                                           |
|     | V    |          | CorpAddr              | nvarchar(200) | 公司地址                               |                                                           |
|     | V    |          | JobTitle              | nvarchar(100) | 職稱                                   |
|     | V    |          | YearlyIncome          | int           | 年收入                                 | (單位：萬元)                                              |
|     | V    |          | OnBoardDate           | char(10)      | 到職日                                 |                                                           |
|     | V    |          | Injury                | varchar(1)    | 是否有重大傷病證明                     | Y: 是 N: 否                                               |
|     | V    |          | BreakPointPage        | varchar(200)  | 斷點頁面                               |                                                           |
|     | V    |          | AccountType           | varchar(4)    | 數三要改開戶影像用的帳戶類別(AcctType) |                                                           |
|     | V    |          | SubCategory           | varchar(4)    | 數三要改開戶影像用的子類別(IntCat)     |                                                           |
|     | V    |          | AMLResult             | varchar(1)    | 是否為高風險職業                       | Y:高風險 N:非高風險                                       |
|     |      |          | IpAddress             | varchar(20)   | IP                                     |                                                           |
|     |      |          | CreateTime            | datetime      | 新增時間                               |                                                           |
|     |      |          | UpdateTime            | datetime      | 更新時間                               |                                                           |
|     | V    |          | SendedTime            | datetime      | 發送時間                               |                                                           |
|     | V    |          | EndTime               | datetime      | 完成填寫時間                           |                                                           |
|     | V    |          | RouteID               | varchar(20)   | 路線編號                               | 本案件對應的 Route.RouteID                                |
|     | V    |          | ChannelStartPoint     | datetime      | 產品計算起始時間                       |                                                           |
|     | V    |          | CLToken               | nvarchar(200) | 中壽 Token                             |                                                           |
|     |      |          | IsPayer               | char(1)       | 是否設定續扣, null-否, 0-否, 1-是      | 預設值 0                                                  |

#### Status - 案件主狀態

| 代碼 | 中文               |
| ---- | ------------------ |
| 00   | 編輯中(斷點前)     |
| 01   | 編輯中(有斷點之後) |
| 02   | 編輯完成           |
| 06   | 30 天自動結案      |
| 07   | 舊案件的斷點取消   |
| 08   | 新案件取消         |
| 11   | 資料處理中         |
| 12   | 資料送出成功       |
| 18   | 補件資料送出失敗   |
| 19   | 資料送出失敗       |

#### Purpose - 開戶目的

| 代碼 | 中文     |
| ---- | -------- |
| 1    | 存款儲蓄 |
| 2    | 薪轉     |
| 3    | 資金調撥 |
| 4    | 投資理財 |
| 5    | 貸款繳息 |
| 6    | 證券戶   |
| 7    | 電子支付 |
| 8    | 其他     |

#### Education - 教育程度

| 代碼 | 中文     |
| ---- | -------- |
| 1    | 博士     |
| 2    | 碩士     |
| 3    | 大學     |
| 4    | 大專     |
| 5    | 高中(職) |
| 6    | 國中     |
| 7    | 其它     |

#### JobTitle - 職稱

| 代碼 | 中文             |
| ---- | ---------------- |
| 1    | 董監事/股東      |
| 2    | 負責人           |
| 3    | 高階管理職務人員 |
| 4    | 一般主管         |
| 5    | 工程師           |
| 6    | 業務人員         |
| 7    | 一般職員         |
| 8    | 約聘人員         |
| 9    | 老師             |
| 10   | 其他             |

```sql
<!--
CREATE TABLE [dbo].[ED3_CaseData](
	[UniqId] [nvarchar](50)
        constraint PK_ED3_CaseData
            primary key,
	[UniqType] [varchar](2) NULL,
	[UserType] [varchar](1) NULL,
	[Status] [varchar](2) NULL,
	[Process] [varchar](1) NULL,
	[Idno] [nvarchar](20) NULL,
	[Birthday] [varchar](8) NULL,
	[Phone] [nvarchar](50) NULL,
	[IsTwTaxResident] [varchar](1) NULL,
	[PromoChannelID] [varchar](2) NULL,
	[PromoDepart] [nvarchar](50) NULL,
	[PromoMember] [nvarchar](50) NULL,
	[SetSecuritiesDelivery] [varchar](1) NULL,
	[SecuritiesAddr] [nvarchar](50) NULL,
	[SecuritiesAddrZipCode] [varchar](5) NULL,
	[SecuritiesCode] [varchar](5) NULL,
	[SecuritiesName] [nvarchar](50) NULL,
	[ProductId] [varchar](2) NULL,
	[ChtName] [nvarchar](50) NULL,
	[EngName] [nvarchar](100) NULL,
	[Gender] [varchar](1) NULL,
	[IdCardDate] [datetime] NULL,
	[IdCardLocation] [nvarchar](50) NULL,
	[IdCardRecord] [varchar](2) NULL,
	[ResAddrZipCode] [nvarchar](6) NULL,
	[ResAddrArea] [nvarchar](200) NULL,
	[ResAddr] [nvarchar](200) NULL,
	[CommAddrZipCode] [nvarchar](6) NULL,
	[CommAddrArea] [nvarchar](200) NULL,
	[CommAddr] [nvarchar](200) NULL,
	[BirthPlace] [nvarchar](200) NULL,
	[Education] [varchar](1) NULL,
	[Marriage] [varchar](1) NULL,
	[EmailAddress] [varchar](120) NULL,
	[SendType] [varchar](1) NULL,
	[ResTelArea] [nvarchar](4) NULL,
	[ResTel] [nvarchar](30) NULL,
	[HomeTelArea] [nvarchar](4) NULL,
	[HomeTel] [nvarchar](30) NULL,
	[EstateType] [varchar](1) NULL,
	[Purpose] [varchar](1) NULL,
	[BranchID] [varchar](3) NULL,
	[AuthorizeToallCorp] [char](1) NULL,
	[AutVer] [char](20) NULL,
	[Occupation] [nvarchar](50) NULL,
	[CorpName] [nvarchar](100) NULL,
	[CorpTelArea] [nvarchar](4) NULL,
	[CorpTel] [nvarchar](30) NULL,
	[CorpAddrZipCode] [nvarchar](6) NULL,
	[CorpAddrArea] [nvarchar](200) NULL,
	[CorpAddr] [nvarchar](200) NULL,
	[JobTitle] [nvarchar](100) NULL,
	[YearlyIncome] [int] NULL,
	[OnBoardDate] [char](10) NULL,
	[Injury] [varchar](1) NULL,
	[BreakPointPage] [varchar](200) NULL,
	[AccountType] [varchar](4) NULL,
	[SubCategory] [varchar](4) NULL,
	[AMLResult] [varchar](1) NULL,
	[IpAddress] [varchar](20) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateTime] [datetime] NULL,
	[SendedTime] [datetime] NULL,
	[EndTime] [datetime] NULL
	[CLToken] [nvarchar](200) NULL,
) ON [PRIMARY]
GO


-->

```
