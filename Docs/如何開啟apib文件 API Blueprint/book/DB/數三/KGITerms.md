### 客製化條款 (KGITerms)

| PK  | NULL | IDENTITY | 欄位 | 型態         | 說明         | 代碼說明                      |
| --- | ---- | -------- | ---- | ------------ | ------------ | ----------------------------- |
|     |      |          | Name | nvarchar(50) | 條款英文名稱 | 與 Terms Table 的 Name 做對應 |
|     |      |          | Page | nvarchar(50) | 呈現頁面     | 產品簡碼-頁面                 |
|     |      |          | Type | char(1)      | 呈現方式     | 1: 連結方式 2:div 灰底框      |
|     |      |          | Sort | nvarchar(50) | 排序         |                               |

```sql
<!--

CREATE TABLE [dbo].[KGITerms](
	[Name] [nvarchar](50) NOT NULL,
	[Page] [nvarchar](50) NOT NULL,
	[Type] [varchar](1) NOT NULL,
	[Sort] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO



-->

```
