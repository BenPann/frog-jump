### 開戶影像回傳結果資料表(ED3_AOResult)

| PK  | NULL | IDENTITY | 欄位              | 型態           | 說明                     | 代碼說明                                              |
| --- | ---- | -------- | ----------------- | -------------- | ------------------------ | ----------------------------------------------------- |
| v   |      |          | Serial            | int            | 由資料庫產生的序號       |                                                       |
|     |      |          | UniqId            | nvarchar(50)   | 案件編號                 |                                                       |
|     | v    |          | Status            | varchar(1)     | 案件開戶狀態             | 0:成功 1:退件(失敗) 2:補件 3:舊客戶使用舊戶帳戶       |
|     | v    |          | OpenfailMsg       | nvarchar(max)  | 開戶退件失敗原因         |                                                       |
|     | v    |          | StatusTw          | varchar(1)     | 台幣帳戶結果狀態碼       | 0:成功 1:失敗                                         |
|     | v    |          | SalaryAccountTw   | nvarchar(16)   | 台幣帳戶                 | 16 碼阿拉伯數字                                       |
|     | v    |          | PassBookTw        | varchar(1)     | 台幣帳戶有無存摺         | Y: 有存摺 N:無存摺(如果台幣帳戶結果為失敗，也請給 N)  |
|     | v    |          | StatusFore        | varchar(1)     | 外幣帳戶結果狀態碼       | 0:成功 1:失敗                                         |
|     | v    |          | SalaryAccountFore | nvarchar(16)   | 外幣帳戶                 | 16 碼阿拉伯數字                                       |
|     | v    |          | PassBookFore      | varchar(1)     | 外幣帳戶有無存摺         | Y: 有存摺 N:無存摺(如果外幣帳戶結果為失敗，也請給 N)  |
|     | v    |          | TrustStatus       | varchar(1)     | 信託帳戶結果狀態碼       | 0:成功 1:失敗                                         |
|     | v    |          | D3Message         | nvarchar(max)  | 數三開戶結果通知信件內容 |                                                       |
|     |      |          | D3Base64PDF       | varbinary(MAX) | 數三開戶影像結果         | data:image/png;base64,+base64 字串                    |
|     | v    |          | FailedImage       | nvarchar(max)  | 需要補件的圖檔           | [{"pType":"1","sType":"1"},{"pType":"1","sType":"2"}] |
|     |      |          | CreateTime        | dateTime       | 建立時間                 |                                                       |
|     | v    |          | UpdateTime        | dateTime       | 更新時間                 |                                                       |
|     | v    |          | RegNo             | nvarchar(100)  | 掛號編號                 |                                                       |
|     | v    |          | RegDate           | varchar(20)    | 掛號日期                 |                                                       |

```sql

CREATE TABLE [dbo].[ED3_AOResult](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[Status] [varchar](1) NULL,
	[OpenfailMsg] [nvarchar](max) NULL,
	[StatusTw] [varchar](1) NULL,
	[SalaryAccountTw] [nvarchar](16) NULL,
	[PassBookTw] [varchar](1) NULL,
	[StatusFore] [varchar](1) NULL,
	[SalaryAccountFore] [nvarchar](16) NULL,
	[PassBookFore] [varchar](1) NULL,
	[TrustStatus] [varchar](1) NULL,
	[D3Message] [nvarchar](max) NULL,
	[D3Base64PDF] [varbinary](max) NOT NULL,
	[FailedImage] [nvarchar](max) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[RegNo] [nvarchar](100) NULL,
	[RegDate] [varchar](10) NULL,
 CONSTRAINT [PK_ED3_AOResult] PRIMARY KEY CLUSTERED
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX [DX_ED3_AOResult_UniqId] ON [dbo].[ED3_AOResult]
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO





```
