### 信用卡或存款帳號驗證狀態 (ED3_IdentityVerification)

| PK  | NULL | IDENTITY | 欄位                | 型態          | 說明                      | 代碼說明                          |
| --- | ---- | -------- | ------------------- | ------------- | ------------------------- | --------------------------------- |
| v   |      |          | UniqId              | nvarchar(50)  | 序號                      |                                   |
|     |      |          | VerifyStatus        | char(1)       | 驗證狀態                  | 0:驗證失敗 1:驗證成功             |
|     |      |          | VerifyType          | char(1)       | 驗證類型                  | 1:信用卡 2:存款帳戶 3:預約分行    |
|     | v    |          | CardNo              | varchar(20)   | 信用卡卡號                | 卡號範例 1111**\*\*\*\***2222     |
|     | v    |          | CardTime            | varchar(8)    | 信用卡到期年月            | 到期範例 : YYMM 月份 西元年後兩碼 |
|     | v    |          | BankId              | varchar(10)   | 存款銀行                  |                                   |
|     | v    |          | AccountNo           | varchar(20)   | 存款帳號                  |                                   |
|     | v    |          | BookingBranchId     | varchar(3)    | 預約分行代碼              |                                   |
|     | v    |          | BookingDate         | varchar(8)    | 預約申辦日期              |                                   |
|     | v    |          | BookingTime         | varchar(2)    | 預約申辦時間              |                                   |
|     | v    |          | ErrorCount          | int           | 錯誤次數                  | 只能錯六次 所以最多到 6           |
|     | v    |          | NCCCErrorCount      | int           | 錯誤次數                  | 紀錄 nccc 錯幾次                  |
|     | v    |          | Pcode2566ErrorCount | int           | 錯誤次數                  | 紀錄 pcode2566 錯幾次             |
|     | v    |          | ErrorMsg            | nvarchar(max) | 錯誤原因                  |                                   |
|     |      |          | CreateTime          | datetime      | 新增時間                  |                                   |
|     |      |          | UpdateTime          | datetime      | 更新時間                  |                                   |
|     | v    |          | MyCard              | char(1)       | 是否為本行卡, Y-是, N－否 |                                   |

```sql
<!--
CREATE TABLE [dbo].[ED3_IdentityVerification](
	[UniqId] [nvarchar](50)
        constraint PK_ED3_IdentityVerification
            primary key,
	[VerifyStatus] [char](1) NULL,
	[VerifyType] [char](1) NULL,
	[CardNo] [varchar](20) NULL,
	[CardTime] [varchar](8) NULL,
	[BankId] [varchar](10) NULL,
	[AccountNo] [varchar](20) NULL,
	[BookingBranchId] [varchar](3) NULL,
	[BookingDate] [varchar](8) NULL,
	[BookingTime] [nvarchar](2) NULL,
	[ErrorCount] [int] NULL,
	[NCCCErrorCount] [int] NULL,
	[Pcode2566ErrorCount] [int] NULL,
	[ErrorMsg] [nvarchar](max) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[MyCard] [char](1) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


-->

```
