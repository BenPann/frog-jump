### 開戶-另加開信用卡案件資料 (ED3_EX_CreditCaseData)

| PK  | NULL | IDENTITY | 欄位            | 型態         | 說明                 | 代碼說明                                         |
| --- | ---- | -------- | --------------- | ------------ | -------------------- | ------------------------------------------------ |
| v   |      |          | UniqId          | nvarchar(50) | 序號                 | ULID                                             |
|     | v    |          | AirloanUniqId   | nvarchar(50) | 數位信貸的信用卡編號 | CreditCaseData.UniqId                            |
|     | v    |          | UserType        | char(1)      | 使用者類別           | 0: 新戶 1:信用卡戶 2:存款戶                      |
|     | v    |          | CreditProductId | nvarchar(50) | 產品名稱             | 信用卡代號                                       |
|     | v    |          | IdCardCRecord   | char(1)      | 身份證換發紀錄       | 0:初領 1:補領 2:換發                             |
|     | v    |          | Occupation      | char(3)      | 職業類別             |                                                  |
|     | v    |          | Education       | char(1)      | 教育程度             | 1: 博士 2: 碩士 3: 大學 4:大專 5:高中(職) 6:其它 |
|     | v    |          | Marriage        | char(1)      | 婚姻狀態             | 1:已婚 2:未婚                                    |
|     | v    |          | SendType        | char(1)      | 信用卡帳單型態       | 1:實體帳單 2:電子帳單                            |
|     | v    |          | FirstPresent    | varchar(50)  | 首刷禮               | 跟 CreditCardGift Table 以及 GiftList Table 對應 |
|     | v    |          | DataUse         | char(2)      | 是否有共銷           | 01:共銷 02:非共銷                                |
|     |      |          | CreateTime      | datetime     | 新增時間             |                                                  |
|     |      |          | UpdateTime      | datetime     | 更新時間             |                                                  |
|     | v    |          | SendedTime      | datetime     | 發送時間             |                                                  |

#### Occupation - 職業類別

| 代碼 | 中文                   |
| ---- | ---------------------- |
| 001  | 軍公教                 |
| 002  | 金融保險業             |
| 003  | 營造/土木工程          |
| 004  | 醫療院所               |
| 005  | 製造業                 |
| 006  | 運輸業                 |
| 007  | 通訊/科技業            |
| 008  | 服務業                 |
| 009  | 其他(家管)             |
| 101  | 不動產經紀業           |
| 102  | 藝術品或骨董等專業拍賣 |
| 103  | 法律服務業             |
| 104  | 軍火業                 |
| 105  | 博弈業                 |
| 106  | 虛擬貨幣               |
| 107  | 典當業                 |
| 108  | 珠寶、銀樓業           |
| 109  | 特殊行業               |
| 103  | 會計服務業             |

```sql
<!--
CREATE TABLE [dbo].[ED3_EX_CreditCaseData](
	[UniqId] [nvarchar](50) NOT NULL,
	[AirloanUniqId] [nvarchar](50) NULL,
	[Status] [varchar](2) NULL,
	[UserType] [char](1) NULL,
	[CreditProductId] [nvarchar](50) NULL,
	[IdCardCRecord] [char](1) NULL,
	[Occupation] [char](3) NULL,
	[Education] [char](1) NULL,
	[Marriage] [char](1) NULL,
	[SendType] [char](1) NULL,
	[JobTitle] [nvarchar](50) NULL,
	[FirstPresent] [varchar](50) NULL,
	[DataUse] [char](2) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[SendedTime] [datetime] NULL,
 CONSTRAINT [PK_ED3_EX_CreditCaseData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-->
```
