### 預約分行(帳戶服務分行)清單 (ED3_BranchList)

| PK  | NULL | IDENTITY | 欄位              | 型態          | 說明                   | 代碼說明 |
| --- | ---- | -------- | ----------------- | ------------- | ---------------------- | -------- |
| v   |      | v        | Serial            | int           | 序號, 由資料庫自動產生 |          |
|     |      |          | BranchID          | nvarchar(16)  | 分行代碼               |          |
|     |      |          | BranchName        | nvarchar(50)  | 分行名稱               |          |
|     |      |          | BranchAddrZipCode | nvarchar(10) | 分行地址區碼           |          |
|     |      |          | BranchAddrCity    | nvarchar(50) | 分行地址縣市           |          |
|     |      |          | BranchAddrDist    | nvarchar(50) | 分行地址鄉鎮市區       |          |
|     |      |          | BranchAddr        | nvarchar(200) | 分行地址               |          |
|     |      |          | Enable            | varchar(1)    | 是否啟用               |          |
|     |      |          | Sort              | int           | 分行排序               |          |
|     |      |          | CreateTime        | datetime      | 新增時間               |          |
|     |      |          | UpdateTime        | datetime      | 更新時間               |          |

### 此 Table 為每日同步凱基內部資料表(BNS_BRHM)產生

```sql
<!--
CREATE TABLE [dbo].[ED3_BranchList](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[BranchID] [nvarchar](16) NOT NULL,
	[BranchName] [nvarchar](50) NOT NULL,
	[BranchAddrZipCode] [nvarchar](10) NOT NULL,
	[BranchAddrCity] [nvarchar](50) NOT NULL,
	[BranchAddrDist] [nvarchar](50) NOT NULL,
	[BranchAddr] [nvarchar](200) NOT NULL,
	[Enable] [varchar](1) NOT NULL,
	[Sort] [int] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ED3_BranchList] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000001', '10355', '建成分行', '承德路1段70號1、2樓及地下1樓', '台北市', '大同區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000067', '10462', '大直分行', '樂群三路７２號、７６號及７８號', '台北市', '大安區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000020', '10485', '松江分行', '松江路１３７號', '新北市', '三重區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000007', '10488', '城東分行', '南京東路三段224號', '台中市', '西區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000008', '10488', '信託部', '南京東路三段２２４號８樓', '台南市', '中西區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000072', '105', '民生分行', '民生東路五段2號', '高雄市', '苓雅區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000009', '10504', '營業部', '南京東路五段125號', '台北市', '中山區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000014', '10504', '國外部', '東光里南京東路５段１２５號', '台北市', '中山區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000018', '10504', '國金分行', '東光里南京東路５段１２５號', '台北市', '松山區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000102', '10504', '會計部', '東光里南京東路５段１２５號', '新北市', '板橋區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000040', '10650', '大安分行', '新生南路二段８號', '桃園市', '桃園區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000019', '10684', '敦南分行', '敦化南路一段364號', '桃園市', '中壢區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000002', '10694', '忠孝分行', '忠孝東路四段270號', '高雄市', '三民區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000024', '11090', '松山分行', '松山路１３２號', '臺北市', '松山區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000038', '11161', '天母分行', '中山北路6段248號', '新北市', '新莊區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000071', '11492', '瑞光分行', '瑞光路618號', '高雄市', '左營區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000043', '11494', '內湖分行', '行愛路７８巷２５號', '彰化縣', '員林市', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000028', '20442', '基隆分行', '麥金路１９３號', '臺北市', '松山區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000010', '22047', '板橋分行', '民生路三段１５號', '台北市', '大安區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000039', '23143', '新店分行', '北新路三段２０２號', '台北市', '中山區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000026', '23581', '中和分行', '景平路２００號', '新北市', '蘆洲區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000025', '23645', '土城分行', '金城路三段１２３號', '高雄市', '鳳山區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000003', '24162', '三重分行', '重陽路三段１９２號', '新竹市', '北區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000015', '24250', '新莊分行', '思源路３３１號', '台北市', '信義區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000021', '24747', '蘆洲分行', '集賢路２１７–２號', '新北市', '土城區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000066', '26550', '羅東分行', '公正路５０號', '新北市', '中和區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000023', '30049', '新竹分行', '西大路６４５號', '台南市', '中西區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000057', '30051', '風城分行', '中正路５９號', '基隆市', '安樂區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000058', '30061', '南大分行', '南大路３３９號', '台南市', '中西區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000059', '30074', '竹科分行', '光復路１段２３８號', '台南市', '北區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000012', '32041', '中壢分行', '中央東路１３號１樓', '屏東縣', '屏東市', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000011', '33042', '桃園分行', '南華街８０號', '台南市', '東區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000070', '33045', '藝文分行', '同德六街８９號', '台南市', '歸仁區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000042', '36043', '苗栗分行', '中正路８１號', '台南市', '安南區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000046', '40046', '繼光分行', '臺灣大道一段９９號', '台南市', '永康區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000004', '40354', '台中分行', '臺灣大道二段２２０號', '台北市', '士林區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000069', '40757', '市政分行', '市政路４００號', '新北市', '新店區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000051', '41254', '大里分行', '中興路二段３３１號', '台北市', '大安區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000048', '42041', '豐原分行', '中山路３２９號', '苗栗縣', '苗栗市', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000047', '50063', '彰化分行', '曉陽路１９９之３號', '台北市', '內湖區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000017', '51052', '員林分行', '莒光路２６６號', '花蓮縣', '花蓮市', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000052', '60050', '嘉義分行', '新榮路１９３號一樓', '台中市', '中區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000030', '70046', '東門分行', '府前路１段２６號', '彰化縣', '彰化市', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000027', '70050', '赤崁分行', '忠義路２段１６７號', '臺中市', '豐原區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000005', '70053', '台南分行', '西門路二段３５１號', '臺中市', '大里區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000033', '70165', '雙和分行', '林森路２段１８４號', '嘉義市', '西區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000031', '70442', '北門分行', '開元路１３３號', '新竹市', '東區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000035', '70949', '海東分行', '海佃路１段１２９號', '新竹市', '東區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000037', '71087', '永康分行', '永大路二段２１號', '新竹市', '東區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000034', '71143', '歸仁分行', '中正南路一段２３號', '台東縣', '台東市', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000006', '80203', '高雄分行', '四維三路８０號', '宜蘭縣', '羅東鎮', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000068', '80457', '高美館分行', '明誠四路１５６號', '臺北市', '中山區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000013', '80793', '北高雄分行', '民族一路８７８號', '高雄市', '鼓山區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000016', '81358', '左營分行', '博愛二路368號', '臺中市', '西屯區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000022', '83051', '鳳山分行', '博愛路１６５之３號', '桃園市', '桃園區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000032', '90053', '屏東分行', '廣東路４５１號', '臺北市', '內湖區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000062', '95046', '台東分行', '中華路一段３４１號', '台北市', '松山區', '1', '1', getdate(), getdate()) ;
insert into ED3_BranchList (BranchID, BranchAddrZipCode, BranchName, BranchAddr, BranchAddrCity, BranchAddrDist, Enable, Sort, CreateTime, UpdateTime) values ('0000000000000045', '97061', '花蓮分行', '中正路５６０號', '臺北市', '松山區', '1', '1', getdate(), getdate()) ;

-->
```
