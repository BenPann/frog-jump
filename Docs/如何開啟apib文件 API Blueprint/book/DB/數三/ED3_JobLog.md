### 排程的紀錄資料表 (ED3_JobLog)

| PK  | NULL | IDENTITY | 欄位    | 型態          | 說明        | 代碼說明                                |
| --- | ---- | -------- | ------- | ------------- | ----------- | --------------------------------------- |
| v   |      | v        | Serial  | int           | 序號        | 由資料庫自動產生                        |
|     |      |          | KeyInfo | nvarchar(200) | 主要 KEY 值 |                                         |
|     |      |          | JobName | nvarchar(50)  | Job 名稱    |                                         |
|     |      |          | Level   | int           | 訊息等級    | 1:Info 2:資料異常 3:連線中斷 4:系統錯誤 |
|     |      |          | Message | nvarchar(max) | 訊息內容    |                                         |
|     |      |          | UTime   | datetime      | 更新時間    |                                         |

```sql
<!--
CREATE TABLE [dbo].[ED3_JobLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[KeyInfo] [nvarchar](200) NULL,
	[JobName] [nvarchar](50) NULL,
	[Level] [int] NULL,
	[Message] [nvarchar](max) NULL,
	[UTime] [datetime] NULL,
 CONSTRAINT [PK_ED3_JobLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

-->
```
