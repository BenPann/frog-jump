### 拒絕申辦名單資料表 (ED3_RejectLog)

| PK  | NULL | IDENTITY | 欄位         | 型態         | 說明                                | 代碼說明         |
| --- | ---- | -------- | ------------ | ------------ | ----------------------------------- | ---------------- |
| v   |      | v        | Serial       | int          | 序號                                | 由資料庫自動產生 |
|     | v    |          | UniqId       | nvarchar(50) | 主要 KEY 值                         |                  |
|     |      |          | Idno         | nvarchar(20) | 身分證號                            |                  |
|     |      |          | IpAddress    | varchar(20)  | IP                                  |                  |
|     | v    |          | Phone        | nvarchar(50) | 手機號碼                            |                  |
|     | v    |          | EmailAddress | varchar(120) | EMAIL 地址                          |                  |
|     |      |          | RejectCode   | varchar(2)   | 拒絕原因代碼                        |                  |
|     |      |          | CreateTime   | datetime     | 更新時間                            |                  |
|     | V    |          | AccountType  | varchar(4)   | ESB 85081 回傳的 AcctType(產品大類) |                  |
|     | V    |          | SubCategory  | varchar(4)   | ESB 85081 回傳的 IntCat(產品子類)   |                  |

#### RejectCode - 拒絕原因代碼

| 代碼 | 中文             |
| ---- | ---------------- |
| 0    | 舊戶             |
| 1    | 兩次未接續斷點   |
| 2    | 手機申辦成功     |
| 3    | 電子信箱申辦成功 |
| 4    | 事故戶           |

```sql
<!--
CREATE TABLE [dbo].[ED3_RejectLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NULL,
	[Idno] [nvarchar](20) NOT NULL,
	[IpAddress] [varchar](20) NOT NULL,
	[Phone] [nvarchar](50) NULL,
	[EmailAddress] [varchar](120) NULL,
	[RejectCode] [varchar](2) NULL,
	[CreateTime] [datetime] NOT NULL,
	[AccountType] [varchar](4) NULL,
	[SubCategory] [varchar](4) NULL,
 CONSTRAINT [PK_ED3_RejectLog] PRIMARY KEY CLUSTERED
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


-->
```
