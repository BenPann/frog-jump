## Required of sheet

Google question sheet
User environment at mobile

### STYLE CHECK

 	- 單選(radix)
 	- 複選(checkbox)
 	- 下拉式(dropdown)
 	- 喜好(Linear scale)

### PAGE SWITCH

​	Change page at every question

### Response

	- 問卷完成率的比例結果
	- 統計結果的呈現樣式

### OTHER REQUIRED

	- Need statistic
	- check can put LOGO at title