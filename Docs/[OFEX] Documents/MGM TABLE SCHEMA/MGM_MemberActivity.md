### MGM活動資料表(MGM_MemberActivity)

| PK   | NULL | 欄位        | 型態          | 說明     | 代碼說明         |
| ---- | ---- | ----------- | ------------- | -------- | :--------------- |
| v    |      | CUST_ID     | nvarchar(50)  | 客戶編號 | 關聯於客戶資料表 |
|      |      | ACT_UNIQ_ID | nvarchar(250) | 活動編號 | 關聯於活動資料表 |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[MGM_MemberActivity]    Script Date: 9/2/2020 9:56:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MGM_MemberActivity](
	[CUST_ID] [nvarchar](50) NOT NULL,
	[ACT_UNIQ_ID] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MGM_MemberActivity] PRIMARY KEY CLUSTERED 
(
	[CUST_ID] ASC,
	[ACT_UNIQ_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

```
