### MGM活動資料表(MGM_Activity)

| PK   | NULL | 欄位            | 型態          | 說明                 | 代碼說明                |
| ---- | ---- | --------------- | ------------- | -------------------- | :---------------------- |
| v    |      | ACT_UNIQ_ID     | nvarchar(50)  | 活動編號             |                         |
|      |      | ACT_NAME        | nvarchar(250) | 活動名稱             |                         |
|      | v    | ACT_DESC        | nvarchar(max) | 活動內容描述         |                         |
|      |      | ACT_TYPE        | nvarchar(50)  | 活動類型             | 1: 單一產品 2: 多項產品 |
|      |      | PROD_TYPE_LIST  | nvarchar(500) | 活動可執行的產品種類 |                         |
|      | v    | ACT_PRJ_CODE    | nvarchar(50)  |                      |                         |
|      | v    | ACT_CREATE_TIME | datetime      | 建立時間             |                         |
|      | v    | ACT_START_TIME  | datetime      |                      |                         |
|      | v    | ACT_END_TIME    | datetime      |                      |                         |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[MGM_Activity]    Script Date: 9/2/2020 9:27:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MGM_Activity](
	[ACT_UNIQ_ID] [nvarchar](50) NOT NULL,
	[ACT_NAME] [nvarchar](250) NOT NULL,
	[ACT_DESC] [nvarchar](max) NULL,
	[ACT_TYPE] [nvarchar](50) NOT NULL,
	[PROD_TYPE_LIST] [nvarchar](500) NOT NULL,
	[ACT_PRJ_CODE] [nvarchar](50) NULL,
	[ACT_CREATE_TIME] [datetime] NULL,
	[ACT_START_TIME] [datetime] NULL,
	[ACT_END_TIME] [datetime] NULL,
 CONSTRAINT [PK_MGM_Activity] PRIMARY KEY CLUSTERED 
(
	[ACT_UNIQ_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

```
