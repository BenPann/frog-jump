### MGM活動短網址資料表(MGM_ActivityShortUrl)

| PK   | NULL | 欄位        | 型態         | 說明       | 代碼說明 |
| ---- | ---- | ----------- | ------------ | ---------- | :------- |
|      | v    | ACT_UNIQ_ID | nvarchar(50) | 活動編號   |          |
|      | v    | PROD_TYPE   | nvarchar(50) | 活動名稱   |          |
|      | v    | SHORT_URL   | nvarchar(50) | 活動短網址 |          |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[MGM_ActivityShortUrl]    Script Date: 9/2/2020 9:40:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MGM_ActivityShortUrl](
	[ACT_UNIQ_ID] [nvarchar](50) NULL,
	[PROD_TYPE] [nvarchar](50) NULL,
	[SHORT_URL] [nvarchar](50) NULL
) ON [PRIMARY]
GO


```
