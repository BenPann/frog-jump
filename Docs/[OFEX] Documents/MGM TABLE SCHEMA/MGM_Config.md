### MGM活動設定資料表(MGM_Config)

| PK   | NULL | 欄位        | 型態          | 說明     | 代碼說明 |
| ---- | ---- | ----------- | ------------- | -------- | :------- |
| v    |      | KeyName     | nvarchar(50)  | 設定名稱 |          |
|      |      | KeyValue    | nvarchar(max) | 設定值   |          |
|      | v    | Description | nvarchar(50)  | 設定描述 |          |
|      |      | UpdateTime  | datetime      |          |          |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[MGM_Config]    Script Date: 9/2/2020 9:43:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MGM_Config](
	[KeyName] [nvarchar](50) NOT NULL,
	[KeyValue] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_MGM_Config] PRIMARY KEY CLUSTERED 
(
	[KeyName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

```
