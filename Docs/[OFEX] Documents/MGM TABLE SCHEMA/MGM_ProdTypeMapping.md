### MGM活動資料表(MGM_MemberActivity)

| PK   | NULL | 欄位                     | 型態          | 說明         | 代碼說明 |
| ---- | ---- | ------------------------ | ------------- | ------------ | :------- |
| v    |      | PROD_TYPE                | nvarchar(50)  | 產品種類代碼 |          |
|      |      | QR_SHORTURL_PProductType | nvarchar(50)  |              |          |
|      |      | QR_SHORT_URL_PProductId  | nvarchar(250) |              |          |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[MGM_ProdTypeMapping]    Script Date: 9/2/2020 9:59:09 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MGM_ProdTypeMapping](
	[PROD_TYPE] [nvarchar](50) NOT NULL,
	[QR_SHORTURL_PProductType] [nvarchar](50) NOT NULL,
	[QR_SHORT_URL_PProductId] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_MGM_ProdTypeMapping] PRIMARY KEY CLUSTERED 
(
	[PROD_TYPE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

```
