### MGM客戶資料表(MGM_Activity)

| PK   | NULL | 欄位              | 型態          | 說明             | 代碼說明 |
| ---- | ---- | ----------------- | ------------- | ---------------- | :------- |
|      |      | CHANNEL_ID        | nvarchar(20)  |                  |          |
|      |      | CUST_ID           | nvarchar(50)  |                  |          |
|      |      | CUST_NAME         | nvarchar(250) | 客戶名稱         |          |
|      |      | CUST_PHONE        | nvarchar(20)  | 客戶電話號碼     |          |
|      |      | CUST_EMAIL        | nvarchar(100) | 客戶EMAIL        |          |
|      |      | CUST_BIRTH_4      | nvarchar(50)  | 客戶生日月日     |          |
|      |      | CUST_RIGSTER_FLAG | nvarchar(50)  | 客戶是否登入     |          |
|      |      | CUST_HAS_SET_PW   | nvarchar(2)   | 客戶是否已設密碼 |          |
|      |      | CUST_UNIT_ID      | nvarchar(20)  |                  |          |
|      |      | CUST_EMP_ID       | nvarchar(20)  | 客戶員工代號     |          |
|      |      | CUST_PW           | nvarchar(100) | 客戶密碼         |          |
|      |      | IMPORT_TIME       | datetime      |                  |          |
|      |      | REGISTER_TIME     | datetime      |                  |          |
|      |      | PW_SETTING_TIME   | datetime      |                  |          |
|      |      | STATUS            | nvarchar(20)  |                  |          |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[MGM_Member]    Script Date: 9/2/2020 9:45:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MGM_Member](
	[CHANNEL_ID] [nvarchar](20) NOT NULL,
	[CUST_ID] [nvarchar](50) NOT NULL,
	[CUST_NAME] [nvarchar](250) NULL,
	[CUST_PHONE] [nvarchar](20) NULL,
	[CUST_EMAIL] [nvarchar](100) NULL,
	[CUST_BIRTH_4] [nvarchar](50) NOT NULL,
	[CUST_RIGSTER_FLAG] [nvarchar](50) NULL,
	[CUST_HAS_SET_PW] [nvarchar](2) NULL,
	[CUST_UNIT_ID] [nvarchar](20) NULL,
	[CUST_EMP_ID] [nvarchar](20) NULL,
	[CUST_PW] [nvarchar](100) NULL,
	[IMPORT_TIME] [datetime] NULL,
	[REGISTER_TIME] [datetime] NULL,
	[PW_SETTING_TIME] [datetime] NULL,
	[STATUS] [nvarchar](20) NULL
) ON [PRIMARY]
GO

```
