# INFO PART 2.

## DB Connect

 	1. IP - telnet 172.31.2.36 2012
 	2. Username : apuser
 	3. Password : apuser

## IP Setting

​	IP address: 10.86.19.18
​	Subnet mask: 255.255.255.0
​	Default gateway: 10.86.19.254
​	DNS server 1: 172.18.254.225
​	DNS server 2: 172.18.254.224 

## Static Url

1. MGM Sector

   http://localhost:8080/onlineformEX/common/mgmUserLogin.html

2. MGM Single
   		http://localhost:8080/onlineformEX/common/mgmSingle.html

3. RPL
   		http://localhost:8080/onlineformEX/front/rpl/pi

4. MGM
   	http://172.31.1.117:8090/onlineformEX/common/mgmSingle.html
   
5. RPL SIT
    http://172.31.1.117:8090/onlineformEX/front/rpl/pi

## remote folder

​	URL:	172.31.1.117
​	USER:	.\localfordeploy
​	PASS:	!qaz2wsx@07
​	ADDRE:	toBen post validator
​	

#### PUT WEB.WAR in this location

- Port: no use
  \172.31.1.117\mapower\jboss-6.4_AirLoan_WEB\SERVER\WEB_MBC_V2_SIT\deployments
- Port: 8090
  \172.31.1.117\mapower\jboss-7.0_For_STP\standalone\deployments

#### PUT AP.WAR in this location

- Port: 8087

  \172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_MBC_V2\deployments

- Port: 8088

  \172.31.1.116\mapower\jboss-7.0_AirLoan_AP\SERVER\AP_MBC_V1\deployments

## A-System id

​	P111555555 -> X-Error
​	E120195890 -> 4

	Ctelnet://esb
	172.31.7.13

## FM_ApiLog

​	"", "-", "RPL MOCK TEST", req, res, startTime, endTime

## PROXY setting

​	angular.json
​	=> projects
​		=> architect
​			=> serve
​				=> options
​					=> "proxyConfig": "src/proxy.conf.json"

## MGM DB DATA

KeyName	KeyValue	Description	UpdateTime
SMS.Api.Url	http://172.31.1.116:3000/sendSMS	簡訊發送API	2020-08-20 00:00:00.000
SMS.BillDepart	9600	簡訊發送歸帳單位	2020-08-10 00:00:00.000

## MGM WORK ITEM

1. QRCODE CREATE
2. shortUrl CREATE
   ​2.1. model already have
   ​			2.1.1 QR_ShortUrl
   ​			2.1.2 QR_ShortUrlDao
   ​			2.1.3 QR_ShortUrlCaseMatchDao

## CL - userId 7-8 number

​	Move init show product one, button selected

## PRODUCT NAME

- DGT3數位存款帳戶
- HL房貸
- RPL循環信貸
- PL個人信貸
- CREDITCARD信用卡
- (NONE) 財富管理

## NEW PASSWORD OF SHA-1

​	id + password

# How to create PDF

​	=> OrbitSerivce
​		=> ContractPDF
​			=> ContractController
​			=> PDFHelper
​	html	=> preview-pl-contract
​				=> saft-pipe

## STAGE 1. Create n' View HTML

​	Access to controller : after data-confirm.component, beform all-finish

​	EX : ContractController.previewContract()

  		1. preview HTML,
 		2. insert parameter

## STAGE 2. 產生契約書PDF寄送EMAIL給客戶並儲存到DB

​	contractDocService.generateContractPDF(uniqId);

	3. HTML to PDF

​	method how it flow

*Access from JOB*

​	**createContractPDF()** with UniqId that find the **STATUS** is "02"

​		Choose PDF type by constructure of PdfHelper()

​		Going to PdfHelper.PdfProcess()

​		**pdfProcess()** main stage to create PDF

​			beforePdfProcess() get PDF prototype that HTML of contract

​			get PATH at local storage

​			**generatePDF()** create it create it

​		finish step of above change **STATUS** to "03"

## STAGE 3.

​	orbitService.uploadContractDataToOrbit();

​	ContractPDF.java replaceField()	ContractPDFDomain.java

		4. PDF to image
		5. to XML -> to claim attribute detail about image and pdf
		6. upload pdf, image, xml



# INFO PART 4.

** SOAP **
​	idAddress: "FF-FF-FF-F1  "
​	
	凱翔: 2989

apply - old 預核
			非預核
applyloannew - new account