USE [AIRLOANDB]
GO


/****** Object:  Table [dbo].[FM_ApiLog]    Script Date: 2020/4/30 上午 11:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FM_ApiLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](2) NOT NULL,
	[KeyInfo] [nvarchar](200) NULL,
	[ApiName] [nvarchar](200) NOT NULL,
	[Request] [nvarchar](max) NOT NULL,
	[Response] [nvarchar](max) NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_FM_ApiLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO



/****** Object:  Table [dbo].[FM_CaseData]    Script Date: 2020/4/30 上午 11:51:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FM_CaseData](
	[UniqId] [nvarchar](50) NOT NULL,
	[Status] [nvarchar](2) NOT NULL,
	[Idno] [nvarchar](20) NOT NULL,
	[Birthday] [nvarchar](8) NULL,
	[ASystemContent] [nvarchar](max) NULL,
	[ASystemCaseNo] [nvarchar](50) NULL,
    [UserType] [nvarchar](1) NULL,
    [CaseType] [nvarchar](1) NULL,
	[UserName] [nvarchar](50) NULL,
    [EmailAddress] [nvarchar](120) NULL,
	[Phone] [nvarchar](14) NULL,
    [CreditCur] [nvarchar](7) NULL,
    [CreditContract] [nvarchar](7) NULL,
    [CreditModify] [nvarchar](18) NULL,
	[EmailSendTime] [datetime] NULL,
	[ImageSendTime] [datetime] NULL,
	[AuthStatus] [nvarchar](2) NULL,
	[AuthErrCount] [nvarchar](1) NULL,
	[AuthTime] [datetime] NULL,
    [ResultHtml] [nvarchar](max) NULL,
	[IpAddress] [nvarchar](20) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_FM_CaseData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



/****** Object:  Table [dbo].[EntryData]    Script Date: 2020/4/30 上午 11:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EntryData](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[Entry] [nvarchar](100) NOT NULL,
	[Member] [nvarchar](100) NULL,
	[Process] [nvarchar](50) NULL,
	[Browser] [nvarchar](50) NULL,
	[Platform] [nvarchar](50) NULL,
	[OS] [nvarchar](50) NULL,
	[Other] [nvarchar](1024) NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EntryData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[EntryDataDetail]    Script Date: 2020/7/3 上午 11:30:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EntryDataDetail](
	[UniqId] [nvarchar](50) NOT NULL,
	[UserAgent] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_EntryDataDetail] PRIMARY KEY CLUSTERED
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[FM_CaseDoc]    Script Date: 2020/4/30 上午 11:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FM_CaseDoc](
	[UniqId] [nvarchar](50) NOT NULL,
	[CaseNo] [nvarchar](50) NOT NULL,
	[Idno] [nvarchar](50) NOT NULL,
	[PdfContent] [nvarchar](max) NOT NULL,
	[ImageContent] [nvarchar](max) NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_FM_CaseDoc] PRIMARY KEY CLUSTERED 
(
	[CaseNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[FM_MailHistory]    Script Date: 2020/4/30 上午 11:52:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FM_MailHistory](
	[UniqId] [nvarchar](50) NOT NULL,
	[MailType] [nvarchar](20) NOT NULL,
	[Status] [nvarchar](20) NOT NULL,
	[ErrorMessage] [nvarchar](1000) NOT NULL,
	[PDFname] [nvarchar](100) NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[TemplateId] [nvarchar](20) NOT NULL,
	[EMailAddress] [nvarchar](max) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_FM_MailHistory] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


