### 契變信件 (FM_MailHistory)

| PK   | NULL | IDENTITY | 欄位         | 型態          | 說明     | 代碼說明 |
| ---- | ---- | -------- | ------------ | ------------- | -------- | -------- |
| v    |      | v        | UniqId       | nvarchar(50)  |          |          |
|      |      |          | MailType     | nvarchar(50)  |          |          |
|      |      |          | Status       | nvarchar(50)  |          |          |
|      |      |          | ErrorMessage | nvarchar(max) |          |          |
|      |      |          | PDFname      | nvarchar(max) |          |          |
|      |      |          | Title        | nvarchar(200) |          |          |
|      |      |          | TemplateId   | nvarchar(20)  |          |          |
|      |      |          | EMailAddress | nvarchar(max) |          |          |
|      |      |          | CreateTime   | datetime      | 新增時間 |          |
|      |      |          | UpdateTime   | datetime      | 更新時間 |          |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[FM_MailHistory]    Script Date: 2020/4/30 上午 11:52:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FM_MailHistory](
	[UniqId] [nvarchar](50) NOT NULL,
	[MailType] [nvarchar](20) NOT NULL,
	[Status] [nvarchar](2) NOT NULL,
	[ErrorMessage] [nvarchar](1000) NOT NULL,
	[PDFname] [nvarchar](100) NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[TemplateId] [nvarchar](20) NOT NULL,
	[EMailAddress] [nvarchar](max) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_FM_MailHistory] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

```
