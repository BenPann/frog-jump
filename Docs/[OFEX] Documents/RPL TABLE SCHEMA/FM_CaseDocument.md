### 契變 案例文件(FM_CaseDocument)

| PK   | NULL | IDENTITY | 欄位         | 型態          | 說明     | 代碼說明               |
| ---- | ---- | -------- | ------------ | ------------- | -------- | ---------------------- |
| v    |      | v        | UniqId       | nvarchar(50)  | 契變編號 | 範例: RPLYYYYMMDD00001 |
|      |      |          | CaseNo       | nvarchar(50)  |          |                        |
|      |      |          | Idno         | nvarchar(50)  |          |                        |
|      |      |          | PdfContent   | nvarchar(max) |          |                        |
|      |      |          | ImageContent | nvarchar(max) |          |                        |
|      |      |          | StartTime    | datetime      |          |                        |
|      |      |          | CreateTime   | datetime      |          |                        |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[FM_CaseDocument]    Script Date: 10/6/2020 3:29:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FM_CaseDocument](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[DocumentType] [nvarchar](50) NOT NULL,
	[Content] [varbinary](max) NOT NULL,
    [XmlContent] [nvarchar](max) NULL,
	[Version] [nvarchar](50) NOT NULL,
	[Other] [nvarchar](max) NULL,
	[UpdateTime] [datetime] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_FM_CaseDocument] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[DocumentType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
```
