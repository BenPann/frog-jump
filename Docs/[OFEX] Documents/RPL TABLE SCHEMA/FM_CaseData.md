### 案件契約資料表(FM_CaseData)

| PK   | NULL | 欄位           | 型態          | 說明                          | 代碼說明                            |
| ---- | ---- | -------------- | ------------- | ----------------------------- | :---------------------------------- |
| v    |      | UniqId         | nvarchar(50)  | 契變編號                      | 範例: RPLYYYYMMDD00001              |
|      |      | Status         | nvarchar(2)   | 案件狀態                      | [詳見列表](#Status案件主狀態)       |
|      | v    | Idno           | nvarchar(50)  | 身分證字號                    | 登入取得                            |
|      | v    | Birthday       | nvarchar(8)   | 生日                          | YYYYMMDD                            |
|      | v    | ASystemContent | nvarchar(max) | A案回傳                       |                                     |
|      | v    | ASystemCaseNo  | nvarchar(10)  | A案回傳編號                   | 專案批號                            |
|      | v    | UserType       | nvarchar(1)   | 戶況(使用者使用者類別)        | 1：專案調額客戶 2：自行申請調額客戶 |
|      | v    | CaseType       | nvarchar(1)   | 簽署文件種類                  | A案取得                             |
|      | v    | UserName       | nvarchar(50)  | 客戶姓名                      | A案取得                             |
|      | v    | EmailAddress   | nvarchar(100) | 電子信箱                      | A案取得                             |
|      | v    | Phone          | nvarchar(14)  | 手機號碼                      | A案取得                             |
|      | v    | CreditCur      | nvarchar(7)   | 目前額度                      | A案取得                             |
|      | v    | CreditContract | nvarchar(7)   | 契約額                        | A案取得                             |
|      | v    | CreditModify   | nvarchar(18)  | 調升後額度/希望調整之額度     | 調額                                |
|      | v    | EmailSendTime  | datetime      | OTP寄送時間                   | 簡訊發送時間                        |
|      | v    | ImageSendTime  | datetime      | 影像上傳時間                  |                                     |
|      | v    | AuthStatus     | nvarchar(2)   | 登入驗身時的 OTP 驗證狀態     | 0:驗證失敗 1:驗證成功               |
|      | v    | AuthErrCount   | nvarchar(1)   | 登入驗身時的 OTP 驗證錯誤次數 |                                     |
|      | v    | AuthTime       | datetime      | 登入驗身時的 OTP 驗證時間     |                                     |
|      | v    | ResultHtml     | nvarchar(max) | nvarchar(max)                 | 預覽用                              |
|      |      | IpAddress      | nvarchar(20)  | IP                            |                                     |
|      |      | Creatime       | datetime      | 建立時間                      |                                     |
|      |      | UpateTime      | datetime      | 更新時間                      |                                     |

#### Status - 案件主狀態

| 代碼 | 中文                                |
| ---- | ----------------------------------- |
| 00   | 剛進入(沒立約資料)                  |
| 01   | 填寫中(OTP 驗身成功後)              |
| 02   | 填寫完(最後預覽 OTP 成功後)         |
| 03   | PDF 產生成功                        |
| 04   | PDF 產生失敗                        |
| 11   | 資料處理中(送影像 從 03 變成)       |
| 12   | 資料送出成功(送影像成功 從 11 變成) |
| 19   | 資料送出失敗(送影像失敗)            |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[FM_CaseData]    Script Date: 11/11/2020 10:49:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FM_CaseData](
	[UniqId] [nvarchar](50) NOT NULL,
	[Status] [nvarchar](2) NOT NULL,
	[Idno] [nvarchar](20) NOT NULL,
	[Birthday] [nvarchar](8) NULL,
	[ASystemContent] [nvarchar](max) NULL,
	[ASystemCaseNo] [nvarchar](50) NULL,
	[UserType] [nvarchar](1) NULL,
	[CaseType] [nvarchar](1) NULL,
	[UserName] [nvarchar](50) NULL,
	[EmailAddress] [nvarchar](120) NULL,
	[Phone] [nvarchar](14) NULL,
	[ManageStore] [nvarchar](4) NULL,
	[CreditCur] [nvarchar](7) NULL,
	[CreditContract] [nvarchar](7) NULL,
	[CreditModify] [nvarchar](18) NULL,
	[EmailSendTime] [datetime] NULL,
	[ImageSendTime] [datetime] NULL,
	[AuthStatus] [nvarchar](2) NULL,
	[AuthErrCount] [nvarchar](1) NULL,
	[AuthTime] [datetime] NULL,
	[ResultHtml] [nvarchar](max) NULL,
	[IpAddress] [nvarchar](20) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_FM_CaseData] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

```
