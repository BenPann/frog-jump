### 連接資訊 Table (EntryData)

| PK   | NULL | 欄位     | 型態           | 說明           | 代碼說明                               |
| ---- | ---- | -------- | -------------- | -------------- | -------------------------------------- |
| v    |      | UniqId   | nvarchar(50)   | 案件編號       |                                        |
|      |      | UniqType | char(2)        | 唯一代號類型   |                                        |
|      |      | Entry    | nvarchar(100)  | 連結入口       | 客戶由哪個入口進入(ex:Pchome)          |
|      | v    | Member   | nvarchar(100)  | 推薦人         | 推薦人                                 |
|      | v    | Process  | nvarchar(50)   | 推薦進入流程   | 推薦使用哪一條流程 (ex:申請,試算,立約) |
|      | v    | Browser  | nvarchar(50)   | 客戶使用瀏覽器 |                                        |
|      | v    | Platform | nvarchar(50)   | 客戶使用平台   |                                        |
|      | v    | OS       | nvarchar(50)   | 客戶作業系統   |                                        |
|      | v    | Other    | nvarchar(1024) | 其餘參數       | 除了確定要使用的參數以外 先記錄在此    |
|      |      | UTime    | datetime       | 更新時間       |                                        |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[EntryData]    Script Date: 2020/7/3 上午 11:30:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EntryData](
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[Entry] [nvarchar](100) NOT NULL,
	[Member] [nvarchar](100) NULL,
	[Process] [nvarchar](50) NULL,
	[Browser] [nvarchar](50) NULL,
	[Platform] [nvarchar](50) NULL,
	[OS] [nvarchar](50) NULL,
	[Other] [nvarchar](1024) NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_EntryData] PRIMARY KEY CLUSTERED
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
```
