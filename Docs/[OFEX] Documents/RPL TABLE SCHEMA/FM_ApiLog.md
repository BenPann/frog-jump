### 契變 APILog 資料表(FM_ApiLog)

| PK   | NULL | IDENTITY | 欄位      | 型態          | 說明                   | 代碼說明 |
| ---- | ---- | -------- | --------- | ------------- | ---------------------- | -------- |
| v    |      | v        | Serial    | int           | 序號, 由資料庫自動產生 |          |
|      |      |          | UniqId    | nvarchar(2)   |                        |          |
|      |      |          | FlowPath  | nvarchar(200) |                        |          |
|      |      |          | ApiName   | nvarchar(200) |                        |          |
|      |      |          | Request   | nvarchar(max) | 呼叫 API 的需求電文    |          |
|      |      |          | Response  | nvarchar(max) | API 的回應電文         |          |
|      |      |          | StartTime | datetime      | API 開始時間           |          |
|      |      |          | EndTime   | datetime      | API 結束時間           |          |
|      |      |          | UTime     | datetime      | 新增時間               |          |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[FM_ApiLog]    Script Date: 8/31/2020 9:47:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FM_ApiLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NULL,
	[FlowPath] [nvarchar](200) NULL,
	[ApiName] [nvarchar](200) NULL,
	[Request] [nvarchar](max) NOT NULL,
	[Response] [nvarchar](max) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[UTime] [datetime] NOT NULL,
 CONSTRAINT [PK_KGI_FM_ApiLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

```
