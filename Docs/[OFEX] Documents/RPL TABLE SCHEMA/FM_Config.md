### 契變 案例文件(FM_Config)

| PK   | NULL | IDENTITY | 欄位       | 型態           | 說明 | 代碼說明 |
| ---- | ---- | -------- | ---------- | -------------- | ---- | -------- |
| v    |      | v        | KeyGroup   | nvarchar(200)  |      |          |
|      |      |          | KeyName    | nvarchar(200)  |      |          |
|      | v    |          | KeyValue   | nvarchar(1200) |      |          |
|      | v    |          | KeyDesc    | nvarchar(1200) |      |          |
|      | v    |          | CreateTime | datetime       |      |          |
|      | v    |          | UpdateTime | datetime       |      |          |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[FM_Config]    Script Date: 8/25/2020 5:43:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FM_Config](
	[KeyGroup] [nvarchar](200) NOT NULL,
	[KeyName] [nvarchar](200) NOT NULL,
	[KeyValue] [nvarchar](1200) NULL,
	[KeyDesc] [nvarchar](1200) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateTime] [datetime] NULL
) ON [PRIMARY]
GO



```
