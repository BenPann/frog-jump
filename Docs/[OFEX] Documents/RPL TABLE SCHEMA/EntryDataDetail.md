### 連接資訊 Table (EntryDataDetail)

| PK  | NULL | 欄位      | 型態          | 說明             | 代碼說明 |
| --- | ---- | --------- | ------------- | ---------------- | -------- |
| v   |      | UniqId    | nvarchar(50)  | 連線資訊唯一代號 |          |
|     |      | UserAgent | nvarchar(max) | 連線資訊         |          |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[EntryDataDetail]    Script Date: 2020/7/3 上午 11:30:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EntryDataDetail](
	[UniqId] [nvarchar](50) NOT NULL,
	[UserAgent] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_EntryDataDetail] PRIMARY KEY CLUSTERED
(
	[UniqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
```
