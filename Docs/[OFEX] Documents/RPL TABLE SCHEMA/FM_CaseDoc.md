### 契變 案例文件(FM_CaseDoc)

| PK   | NULL | IDENTITY | 欄位         | 型態          | 說明     | 代碼說明               |
| ---- | ---- | -------- | ------------ | ------------- | -------- | ---------------------- |
| v    |      | v        | UniqId       | nvarchar(50)  | 契變編號 | 範例: RPLYYYYMMDD00001 |
|      |      |          | CaseNo       | nvarchar(50)  |          |                        |
|      |      |          | Idno         | nvarchar(50)  |          |                        |
|      |      |          | PdfContent   | nvarchar(max) |          |                        |
|      |      |          | ImageContent | nvarchar(max) |          |                        |
|      |      |          | StartTime    | datetime      |          |                        |
|      |      |          | CreateTime   | datetime      |          |                        |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[FM_CaseDoc]    Script Date: 2020/4/30 上午 11:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FM_CaseDoc](
	[UniqId] [nvarchar](50) NOT NULL,
	[CaseNo] [nvarchar](50) NOT NULL,
	[Idno] [nvarchar](50) NOT NULL,
	[PdfContent] [nvarchar](max) NOT NULL,
	[ImageContent] [nvarchar](max) NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_FM_CaseDoc] PRIMARY KEY CLUSTERED 
(
	[CaseNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


```
