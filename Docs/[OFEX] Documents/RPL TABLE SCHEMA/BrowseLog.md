### 客戶瀏覽紀錄的 LOG 資料表(BrowseLog)

| PK  | NULL | IDENTITY | 欄位     | 型態          | 說明         | 代碼說明 |
| --- | ---- | -------- | -------- | ------------- | ------------ | -------- |
| v   |      | v        | Serial   | int           | 流水號       |          |
|     |      |          | UniqId   | nvarchar(50)  | 序號         |          |
|     |      |          | UniqType | char(2)       | 唯一代號類型 | 03:立約  |
|     |      |          | Page     | nvarchar(200) | 瀏覽頁面     |          |
|     |      |          | Action   | nvarchar(200) | 動作         | GET      |
|     |      |          | UTime    | datetime      | 瀏覽時間     |          |

### Create SQL

```mssql
USE [AIRLOANDB]
GO

/****** Object:  Table [dbo].[BrowseLog]    Script Date: 2020/7/2 下午 02:01:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BrowseLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[UniqType] [char](2) NOT NULL,
	[Page] [nvarchar](200) NOT NULL,
	[Action] [nvarchar](200) NOT NULL,
	[UTime] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
```
