
報表平台API:

http://172.31.1.116:3000/imgAPI/GET_CASE_COMPLETE

method: POST
format: JSON

req sample:
{"ID":"D121319366","UUID":"2021030000699"}

response sample:
{"CHECK_CODE":"01","RTN_CODE":"N","RTN_MSG":"此ID：D121319366 UUID：2021030000699 ，找不到影像案件。"}
OR
{"CHECK_CODE":"","RTN_CODE":"N","RTN_MSG":"整件未完整或找不到案件"}
OR
{"CHECK_CODE":"","RTN_CODE":"Y","RTN_MSG":"已人工整件完成，無須再上傳影像"}

----->故直接取 RTN_CODE 來判斷即可


