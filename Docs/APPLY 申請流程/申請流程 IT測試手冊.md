# 測試規劃

## 第一輪

| uniqId(CaseNo)    | idno 簡述         | 出生年月日 | 備註                                                         |
| ----------------- | ----------------- | ---------- | ------------------------------------------------------------ |
|                   | V111111111 白名單 |            | Charles說：「白名單，可重覆測試表單填寫、狀態變更、PDF產出。」 |
|                   | A123123007 新戶   |            | 尚未結案(已新增貸款申請案件)                                 |
|                   | B103578945 新戶   |            | 尚未結案(已新增貸款申請案件)                                 |
|                   | F224316254 既有戶 |            | 已結案(可新增貸款申請案件)                                   |
|                   |                   |            |                                                              |
| LOAN2020111700001 | A259917933        | 19900909   | 此次壞在Z07及KYC未完成實作，及CHECK_Z07_RESULT異常。         |
| LOAN2020120100001 | B297686812        | 19900909   | 有BUG死在 apply/verify2                                      |
| LOAN2020120100002 | C248832814        | 19900909   | 待Charles處理未打 GET_AML_DATA 待查，待Ben處理CHECK_Z07_RESULT |
| LOAN2020120100003 | D231168104        | 19900909   |                                                              |
| LOAN2020120100004 | E250739419        | 19900909   | DEBUG GET_AML_DATA 新增 FastPass 時未寫入 EndTime 欄位值     |
| LOAN2020120100005 | F241527626        | 19900909   | 已使用                                                       |
|                   | G200850330        |            | 無法使用，該帳號為既有戶                                     |
| LOAN2020120300003 | H247685987        | 19900909   | 已使用                                                       |
| LOAN2020120300004 | J282175443        | 19900909   | BUG fixed                                                    |
| LOAN2020120300005 | K272578782        | 19900909   | GET_KYC_DEBT_INFO 逾時未回傳                                 |
| LOAN2020120400001 | L206824711        | 19900909   |                                                              |
| LOAN2020120700001 | M223578666        | 19900909   | 已使用                                                       |
| LOAN2020120700002 | N248575091        | 19900909   | 已使用                                                       |
| LOAN2020120700003 | P255981046        | 19900909   | 成功產製**【KYC 與 AML 檢核表】**                            |
|                   | Q223600513        |            |                                                              |
|                   | R209711092        | 19900909   | 已使用                                                       |
|                   | S220349720        | 19900909   | 已使用                                                       |
|                   | T279497394        | 19900909   | 已使用                                                       |
|                   | U229417614        | 19900909   | 已使用                                                       |
|                   | V233822740        | 既有戶     |                                                              |
|                   | X274541442        | 19900909   | 已使用                                                       |
|                   | Y286744445        | 19900909   | 已使用                                                       |
|                   | Z286817678        | 19900909   | 已使用                                                       |
|                   | O232192373        | 19900909   | 已使用                                                       |
|                   | I207130220        | 19900909   | 已使用                                                       |

| uniqId(CaseNo)    | idno 簡述  | 出生年月日 | 備註                                                         |
| ----------------- | ---------- | ---------- | ------------------------------------------------------------ |
|                   | A212506369 | 19900909   |                                                              |
|                   | B224412104 | 19900909   |                                                              |
| LOAN2021021700001 | C297263589 | 19900909   | 完成身份證及財力證明上傳                                     |
|                   | D271702037 | 19900909   |                                                              |
|                   | E289660978 | 19900909   | AgentNo=117033 AgentDepart=9735                              |
| LOAN2021031200001 | F266935133 | 19900909   | http://localhost:4200/apply/start?PT=2&entry=iwantbanana&loan=10000&junk=yard&qr=05rp0cfk |
| LOAN2021031200002 | G289415528 | 19900909   | http://localhost:4200/apply/start?PT=2&entry=KBOTE0009&loan=10000&junk=yard&qr=te7s0lsw |
| LOAN2021031500003 | H250212952 | 19900909   | http://localhost:4200/apply/start?EXP=CC202103002649         |
|                   | J281220654 | 19900909   |                                                              |
|                   | K269497054 |            | 被我刪掉了，哭。                                             |
|                   | L204493343 | 19900909   | GET_CASE_COMPLETE                                            |
|                   | M256890088 | 19900909   |                                                              |
|                   | N223791357 | 19900909   |                                                              |
|                   | P262351914 | 19900909   |                                                              |
|                   | Q212470036 | 19900909   | 測中斷點在pcode2566。                                        |
|                   | R243536131 | 19900909   | 用掉了。                                                     |
|                   | S233938339 |            |                                                              |
|                   | T211977217 | 19900909   |                                                              |
|                   | U272606878 | 19900909   | /UPL/step0 頁                                                |
|                   |            |            |                                                              |

### 以下情境時 UPDATE_DGTCASE_INFO 中 COMP_CASE=Y

| COMP_CASE=Y | 申請金額      | ID圖檔有上傳或有影編 | 財力圖檔有上傳或有影編 | KYC是否完整 | 是否有業務員編資訊 | 有填同一關係人表 |
| ----------- | ------------- | -------------------- | ---------------------- | ----------- | ------------------ | ---------------- |
| 情境一      | 5萬以下(含)   | Y                    | Y                      | Y           | -                  | -                |
| 情境二      | 5萬~100萬(含) | Y                    | Y                      | Y           | Y                  | -                |
| 情境三      | 大於100萬     | Y                    | Y                      | Y           | Y                  | Y                |



## 第二輪

# 既有戶內行留有財力證明

| uniqId(CaseNo) | idno 簡述  | 出生年月日 | 備註                   |
| -------------- | ---------- | ---------- | ---------------------- |
|                | H223618644 | 19861002   | Ben 提供的既有財力證明 |

# 既有戶 F224316254, F223730438 表單輸入

| 欄位名稱                  | 值                               | 說明 |
| ------------------------- | -------------------------------- | ---- |
| 身分證字號                | F224316254                       |      |
| 生日 YYYY/MM/DD           | 19781231                         |      |
| 手機號碼                  | 0918300000                       |      |
| 業務員代號                | 117033                           |      |
| 貸款金額                  | 101                              |      |
| 貸款期間                  | 5                                |      |
| 貸款用途                  | 償還貸款                         |      |
| 姓名                      | 查爾斯                           |      |
| 電子信箱                  | charles.yen@frog-jump.com        |      |
| 居住地址                  | 宜蘭縣宜蘭市宜興路一段102號      |      |
| 住家市話                  | 0222519000                       |      |
| 戶籍市話                  | 0222519000                       |      |
| 公司名稱 (可輸入統編搜尋) | 53102940                         |      |
| 行業類別                  | 金融服務業                       |      |
| 職稱                      | 課長                             |      |
| 到職年月                  | 2014                             |      |
| 到職年月                  | 01                               |      |
| 公司地址                  | 台北市大同區承德路一段70-1號15樓 |      |
| 公司電話                  | 0225508556                       |      |
| 分機                      | 104                              |      |
| 學歷                      | 大學                             |      |
| 年收入                    | 150                              |      |
| 婚姻狀況                  | 已婚                             |      |
| 不動產狀態                | 本人所有                         |      |
|                           |                                  |      |
|                           |                                  |      |
|                           |                                  |      |
|                           |                                  |      |

## 查詢尚未結案

LOAN2020110300011 A123123007
LOAN2020103000005 V111111111
LOAN2020103000003 F224316254
LOAN2020102900007 A123123105

```sql
select * from contractapilog where keyinfo like 'LOAN%' AND ApiName in ('/KGI/CREATE_NEW_CASE') 
AND Response like '{
  "CHECK_CODE": "",
  "ERR_MSG": "",
%'
order by CreateTime desc;
```



# TODO FIX

## apply/start

- [ ] 待Ben提供 GET_CIF_INFO 的欄位規格 例如：ADR_1 請更新文件 **[功能規格]WebAPI介接說明_數位信貸ALN.docx** 最新的規格書"。
- [x] 開發接斷點，重新申請，則取消斷點(若選擇重新申請，則取消斷點，將先前 00, 01 的狀態更新為 07)，重新寫一筆新的主檔CaseData。
- [x] airloanEX.ALN.ServerHost 由 http://172.18.12.35:6872 改為 http://172.18.12.35:7999
- [ ] 新戶(isNewOne)/既有戶(isOldOne)
- [x] 修正斷點取消的狀態更新
- [ ] 待Ben提供條款「調閱本人與貴行往來資料」。
- [ ] 待Ben提供條款「未透過他人代辦或行員勸誘取得資金」。

## apply/loan

- [ ] 移除 PL RPL 既有戶。
- [ ] 貸款金額101萬時，客戶須填寫同一關係人表，反之則不需要。

## apply/work

- [x] 到職年月 - 未正常寫入CaseData的on_board_date格式為yyyyMMdd，dd的部份預設為28，demand by Ben。
- [x] 到職年月 - 邏輯為，最大年份為今年，最小年份為生日年份+15，demand by Ben。
- [x] 職稱 - 棄用下拉式選單，採用input text，demand by Ben。
- [x] 打 CHECK_NO_FINPF 異常，暫時測試正常。
- [x] 打 CHECK_HAVE_FINPF 異常，暫時測試正常。

## apply/confirm

- [x] apply/work 移除打AML, CHECK_NO_FINPF, CHECK_HAVE_FINPF的部份，demand by Ben。
- [x] apply/confirm 先需先打AML, CHECK_NO_FINPF, CHECK_HAVE_FINPF，再打CREATE_NEW_CASE，demand by Ben。
- [ ] 打戶役政，已發問，待Ben回覆。
- [ ] 啟用CREATE_NEW_CASE, 
- [ ] 移除 非既有戶(PL/RPL) 既有戶。

## apply/identify

- [ ] 實作 試試看其他方式，包含用信用卡驗證、紙本申請。
- [ ] 移除 此銀行不支援帳戶身分驗證。

## apply/otherway

- [ ] 用信用卡驗證 CAD/gift
- [ ] 紙本驗證 apply/paper

## UPL/step0

- [ ] 實作 手邊沒有文件嗎？　您可以[稍後補件上傳](http://localhost:4200/UPL/later)

## UPL/step1

- [x] 身份證反面未正確處理**縣市**及**鄉鎮市區**。
- [x] DEBUG 上傳身份證正反面 OCR 後回填 CaseData
- [x] 身份證正面 OCR 後回填 CaseData 身份證號碼
- [x] 上傳時身份證正反面時 OCR 寫檔CaseData.OCR_*欄位，及確認上傳後更新CaseData相關欄位

## UPL/later

- [ ] 稍候補件頁邏輯實作須問Ben

## CAL/fill

- [x] 開發 訪客登入機制 calToken, calMain

## CAL/result

- [ ] 待 Ben 提供輸入手機後傳送方案的規格
- [ ] 開發 個人資料運用告知聲明使用者條款及約定

- [x] 開發 額度利率體驗接續申請流程，即新增 CaseData 的 CaseNo 時回寫入 PLExp 的 CaseNoWeb

## 排程 SendPDFJob

- [x] orbitService.uploadCaseDataToOrbit();      //貸款
- [x] photoService.uploadCreditCaseDataToOrbit(); //信用卡
- [x] photoService.uploadAddPhotoOrbitData(); // online補件資料上傳ftp

## 問題

- [ ] 同一關係人表儲存後，須呼叫APS API傳給APS儲存(API稍後提供)。
- [ ] 上傳財力頁增加呼叫影像系統API，檢核案件是否已在影像系統標註為完整，如完整，則跳過上傳財力頁。
- [x] 案件主狀態與副狀態之關聯建立。
- [ ] 影像XML調整。
- [x] 開發，申請流程白名單機制 V111111111
  - bypass CHECK_REP_CASE
  - bypass GET_AML_DATA
  - bypass CHECK_NO_FINPF
  - bypass CHECK_HAVE_FINPF2
  - bypass CHECK_HAVE_FINPF3
  - bypass CREATE_NEW_CASE
  - bypass APIM 戶役政驗證
- [x] 呼叫AML 查完後須寫 FastPass
- [ ] 呼叫Z07查詢

  - SEND_JCIC(PRODUCT=6)

  - CHECK_Z07_RESULT
- [ ] 呼叫外部負債資料查詢 GET_KYC_DEBT_INFO
- [x] Ben 提供 loan_kyc.html **【KYC 與 AML 檢核表】** 作為產製PDF的樣板檔
- [ ] 開發 CaseKycPDF.java 產製  **【KYC 與 AML 檢核表】** 
- [ ] Ben 提供 KYC 資料 的 TABLE SCHEMA
- [ ] 立約流程 BrowseLog 須記錄 IPAddress 欄位值
- [x] 申請流程 BrowseLog 須記錄 IPAddress 欄位值
- [x] 申請流程 BrowseLog 須記錄 頁面 進入(GET) 及 離開(POST)
- [ ] 測試流程apply/start?PT=(預設)2 PL, 3 RPL, 4 GM

# 白名單V111111111帳號

說明：V111111111可略過有些具有交易性的電文，例如：新增案件(CREATE_NEW_CASE)。	

- bypass CHECK_REP_CASE
- bypass GET_AML_DATA
- bypass CHECK_NO_FINPF
- bypass CHECK_HAVE_FINPF2
- bypass CHECK_HAVE_FINPF3
- bypass CREATE_NEW_CASE
- bypass APIM 戶役政驗證

# 查詢資料庫驗證寫檔 SQL 指令

```sql
-- FIND ALL 
select * from QR_PromoCaseUserInput where uniqid in (select CaseNo from casedata where idno = 'F224316254');
select * from CreditCaseData where uniqid in (select CaseNo from casedata where idno = 'F224316254');
select * from CreditVerify where uniqid in (select CaseNo from casedata where idno = 'F224316254');
select * from CaseDoc where CaseNo = 'F224316254';
select * from CaseDocument where uniqid in (select CaseNo from casedata where idno = 'F224316254'); -- CasePDF, CreditCardPDF
select * from browselog where uniqid in (select CaseNo from casedata where idno = 'F224316254');
select * from casedata where idno = 'F224316254';
select * from userphoto where uniqid in (select CaseNo from casedata where idno = 'F224316254');
```

# PDF產製

舊資料(舊的信貸申請系統)成功產製PDF如下，

```sql
-- CasePDF 信貸申請書 CasePDF.previewHtml.html
select * from CaseDocument where UniqId = 'CASE2020021700055';
-- CreditCardPDF 信用卡申請書 CreditCardPDF.previewHtml.html
select * from CaseDocument where UniqId = 'CASE2020031300011';
```

新資料(新的信貸申請系統)成功產製PDF如下，

```sql
-- CasePDF 信貸申請書 CasePDF.previewHtml.html
select * from CaseDocument where UniqId = 'LOAN2020110400001';
-- CreditCardPDF 信用卡申請書 CreditCardPDF.previewHtml.html
select * from CaseDocument where UniqId = 'LOAN2020110400001';
```

新資料的JobLog

```sql
-- CasePDF 信貸申請書
select top 500 * from joblog where JobName = 'SendData' and Serial in ('9849968', '9849967', '9849966', '9849965', '9849964', '9849962', '9849961', '9849960', '9849959', '9849958') order by Serial desc;
-- CreditCardPDF 信用卡申請書
select top 500 * from joblog where JobName = 'SendData' and Serial in ('9850274', '9850273', '9850271') order by Serial desc;
```

比對 舊資料件(陳筱玲) vs. 新資料件(查爾斯)

```sql
-- CasePDF 信貸申請書 舊資料件(陳筱玲) vs. 新資料件(查爾斯)
select * from CaseData where CaseNo in ('CASE2020021700055', 'LOAN2020110400001');
-- CreditCardPDF 信用卡申請書 舊資料件(陳筱玲) vs. 新資料件(查爾斯)
select * from CreditCaseData where UniqId in ('CASE2020031300011', 'LOAN2020110400001');
```

# 申請流程凱基既有戶帳號

F224316254 19781231 蔡ＸＸ 0918300000

```sql
select * from casedata where idno='F224316254' and caseno like 'LOAN%'; -- LOAN2020102700008
```

# (太舊現在都改LOAN不用CASE)申請流程帳號

A123123123 常用
B103578945 19900909 CASE2020092400005 測試 上傳身份證正反
C125804338 19900909 CASE2020092400033 測試 上傳財力證明
D138190764 19900909 CASE2020093000002 猴塞雷
E121428389 19900909 CASE2020100600075
F164295269 19900909
G155836717 19900909 

# 業務員代號

- 100660
- 101989
- 110914
- 112401
- **117033**

#  &#9825; 申請信貸(LOAN)企業邏輯說明

## 登入

- 判斷產品別：1 PA(預核名單), 2 PL(個人信貸), 3 RPL(循環個人信貸), 4 GM(現金卡)
- 是否為既有戶(若否則為新戶)(GET_OTP_TEL_STP)
- 取得既有戶資料(GET_CIF_INFO)
- 是否為預核名單(查詢Oracle資料表PSC_GM_SCORE_LOG)
- 檢核申請人在APS系統是否有案件尚未結案(CHECK_REP_CASE)
- 是否為薪轉戶(CHECK_SALARY_ACCOUNT)
- 是否為信用卡戶
- 是否為現金卡戶
- 是否為存款戶
- 是否為貸款戶
- 查詢行內人員對應單位代號(QRY_EMP_DEP)
- 取數位編號(ADD_LOAN_CASE) - 取 DecisionCaseNo

## OTP 2 確認頁

- 檢核申請人在APS系統是否有案件尚未結案(CHECK_REP_CASE)
- 驗證洗錢防治(GET_AML_DATA)
- 判斷是否為免財力證明客戶(CHECK_NO_FINPF)
- 判斷是否有 2.身份證(CHECK_HAVE_FINPF2)
- 判斷是否有 3.財力證明(CHECK_HAVE_FINPF3)
- 新增案件(CREATE_NEW_CASE)
- 查詢行內戶役政檢核結果資料(QRY_IDCARD_INFO)
  - 新增戶役政檢核結果資料(ADD_IDCARD_INFO)
  - 透過call戶役政驗證身分證(APIM_info)
  - AML風險評級介接查詢(存取預儲程序usp_Get_CDD_Rate) - 風險評級:高 中 低
  - Z07 異常身證通報紀錄(簡單說就是身份證有無被偽冒的紀錄)(SEND_JCIC, CHECK_Z07_RESULT)
  - 呼叫外部負債資料查詢(GET_KYC_DEBT_INFO)

## 身份證及財力證明上傳頁

- 新增戶役政檢核結果資料(ADD_IDCARD_INFO)
- 透過call戶役政驗證身分證(APIM_info)
- AML風險評級介接查詢(存取預儲程序usp_Get_CDD_Rate) - 風險評級:高 中 低
- Z07 異常身證通報紀錄(簡單說就是身份證有無被偽冒的紀錄)(SEND_JCIC, CHECK_Z07_RESULT)
- 呼叫外部負債資料查詢(GET_KYC_DEBT_INFO)

## 同一關係人資料頁

- 新增同一個關係人資料(ADD_RELSHIP_DATA)

## RPL 利率頁

- 取得指數利率(i)

## 額度利率體驗頁

QRY_QA

SEND_ASW

# 電文一覽表

## 排程

### UPDATE_CASE_INFO  發查聯徵

## apply/start 登入頁

### GET_OTP_TEL_STP

POST http://172.31.1.51:3060/api/KGI/GET_OTP_TEL_STP

request

```json
{"CUSTOMER_ID":"F224316254","BIRTHDATE":"19781231"}
```

response

```json
{
    "OTP_DATA": [
    {
        "DATA_TYPE": "3",
        "OTP_TEL": "0918300000000000",
        "FORMAT": "N"
    }],
    "BANK_BIRTHDATE": "19781231",
    "BIRTHDAY_SOURCE": "3",
    "ERR_MSG": "",
    "CHECK_CODE": ""
}
```

### GET_CIF_INFO

PATCH http://172.18.12.35:6872/KGI/GET_CIF_INFO

request

```json
{"CUSTOMER_ID":"F224316254","BIRTHDATE":"19781231"}
```

response

```json
{
  "CHECK_CODE": "",
  "ERR_MSG": "",
  "CUST_ID": "F224316254",
  "NAME": "蔡ＸＸ",
  "EML_ADDR": "",
  "MOBILE_TEL": "",
  "ADR_ZIP": "116",
  "ADR": "台北市文山區ＸＸＸＸＸＸＸＸＸＸ",
  "ADR_ZIP_1": "116",
  "ADR_1": "台北市文山區ＸＸＸＸＸＸＸＸＸＸ",
  "ADR_ZIP_2": "",
  "ADR_2": "",
  "TEL_NO_AREACODE": "",
  "TEL_NO": "",
  "TEL_NO_EXT": "",
  "TEL_NO_AREACODE_1": "",
  "TEL_NO_1": "",
  "TEL_NO_EXT_1": "",
  "EDU": "",
  "MARRIAGE": "",
  "COMPANY_NAME": "",
  "COMPANY_ADDR_ZIP": "",
  "COMPANY_ADDR": "",
  "COMPANY_TEL_AREACODE": "",
  "COMPANY_TEL": "",
  "COMPANY_TEL_EXT": "",
  "OCCUPATION": "",
  "ON_BOARD_DATE": "",
  "INCOME": "",
  "HOUSE_OWNER": ""
}
```

### QRY_EMP_DEP

POST http://172.18.12.18:6877/KGI/QRY_EMP_DEP

request

```json
{"EMP_ID":"117033"}
```

response

```json
{
  "CHECK_CODE": "",
  "ERR_MSG": "",
  "DEP_CODE_ACT": "9735"
}
```

### ADD_LOAN_CASE

POST http://172.31.1.51:3061/api/KGI/ADD_LOAN_CASE

request

```json
{"CUSTOMER_ID":"F224316254","DATA_TYPE":"CC","CASE_NO_REF":""}
```

response

```json
{"CASE_NO":"CC202010000326","ERR_MSG":"","CHECK_CODE":""}
```

### CHECK_SALARY_ACCOUNT

POST http://172.31.1.51:3061/api/KGI/CHECK_SALARY_ACCOUNT

request

```json
{"CASE_NO":"CC202010000326"}
```

response

```json
{"RESULT":"N","ID_TYPE":"11","ERR_MSG":"","CHECK_CODE":""}
```

## apply/verify 輸入簡訊驗證碼(OTP)

### /sendOTP

request

```json
{"phone":"0918300000","idno":"F224316254","opId":"AP08","billDep":"6660","txnId":"CONT202010270026"}
```

response

```json
{
    "code": "0000",
    "message": "OTP取得成功",
    "sk": "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff",
    "txnID": "CONT202010270026",
    "txnDate": "20201027102143",
    "req": "THIS IS PSEUDO MODE",
    "res": "THIS IS PSEUDO MODE"
}
```

### /checkOTP

request

```json
{"idno":"F224316254","opId":"AP08","billDep":"6660","otp":"111111","sk":"ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff","txnId":"CONT202010270026","txnDate":"20201027102143","phone":"0918300000"}
```

response

```json
{"code":"0000","message":"OTP取得成功","req":"THIS IS PSEUDO MODE","res":"THIS IS PSEUDO MODE"}
```

## apply/work

### /api/company/info

POST http://172.18.12.35:8081/api/company/info

request

```json
{"CompanyId":"53102940"}
```

response

```json
{
  "Code": "200",
  "Message": null,
  "Data": {
    "CompanyNameSource": "全國營業稅籍登記",
    "CompanyName": "德義資訊股份有限公司",
    "UnifiedBusinessNumber": "53102940",
    "HeadOfficeId": null,
    "RegisterAddressSource": "全國營業稅籍登記",
    "RegisterAddressNormalize": [
      {
        "Address": "臺北市大同區建泰里承德路一段70―1號15樓",
        "City": "臺北市",
        "District": "大同區",
        "Road": "承德路一段",
        "Number": "70號",
        "Other": null
      },
      {
        "Address": "臺北市大同區建泰里承德路一段70―1號15樓",
        "City": "臺北市",
        "District": "大同區",
        "Road": "承德路一段",
        "Number": "1號",
        "Other": null
      }
    ],
    "KgiIndustryType": "1006",
    "KgiCompanyType": "4",
    "Government": null,
    "Medical": null,
    "Nursing": null,
    "Tax": {
      "Capital": 58320250,
      "RegisterDate": "2010-10-26T00:00:00",
      "CompanyType": "股份有限公司",
      "IndustryTypeCode": "464111",
      "IndustryType": "電腦及電腦週邊設備批發"
    },
    "School": null,
    "Stock": null,
    "YellowPage": null
  }
}
```

## apply/confirm



## apply/identify  用他行帳號驗證

### /pCode2566/other

request

```json
{
    "phone": "0918300000",
    "bank": "007",
    "bankName": "",
    "account": "7777777777777777",
    "branchId": "",
    "idno": "F224316254",
    "pcodeUid": "3864",
    "birthday": "19781231",
    "tmnlId": "EOPS",
    "tmnlType": "6614",
    "ESBClientId": "EOPSYS",
    "ESBClientPAZZD": "3664e9541cef7291a3874eba3dad0a88",
    "ESBTimestamp": "2020-10-27T11:22:29.583+0800",
    "ESBChannel": "ESBMQSVR.EOPSYS",
    "ESBQueueReceiveName": "EA.EOPSYS"
}
```

response

```json
{
    "result": 0,
    "statusCode": "0",
    "message": "",
    "verifyCodeByVerifyType": "00",
    "verifyCodeByAccountState": "00",
    "verifyCodeByOpenAccountState": "01",
    "req": "THIS IS PSEUDO MODE",
    "res": "THIS IS PSEUDO MODE"
}
```

## apply/verify2

### /sendOTP

request

```json
{"phone":"0918300000","idno":"F224316254","opId":"AP08","billDep":"6660","txnId":"CONT202010270035"}
```

response

```json
{
    "code": "0000",
    "message": "OTP取得成功",
    "sk": "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff",
    "txnID": "CONT202010270035",
    "txnDate": "20201027112233",
    "req": "THIS IS PSEUDO MODE",
    "res": "THIS IS PSEUDO MODE"
}
```

### /checkOTP

request

```json
{"idno":"F224316254","opId":"AP08","billDep":"6660","otp":"111111","sk":"ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff","txnId":"CONT202010270035","txnDate":"20201027112233","phone":"0918300000"}
```

response

```json
{"code":"0000","message":"OTP取得成功","req":"THIS IS PSEUDO MODE","res":"THIS IS PSEUDO MODE"}
```

### CHECK_REP_CASE

### GET_AML_DATA 

POST http://172.31.10.6:80/CheckAML/services/CheckAMLPort?wsdl

request

```json
{"ID_NO":"F224316254","NAME":"","CHECK_DEPT":"9735"}
```

response

```json
{
    "CHECK_CODE": "",
    "ERR_MSG": "",
    "AML_RESULT": "N",
    "AML_XML_DATA": "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><ns2:checkAMLResponse xmlns:ns2=\"http://kgiaml/\"><return><aml_result>N</aml_result><category_desc></category_desc><dob></dob><error_code>0</error_code><error_message></error_message><id></id><invest_comment></invest_comment><invest_result></invest_result><list_type></list_type><name></name><nationality></nationality><original_source></original_source><passport></passport><seq>1</seq></return></ns2:checkAMLResponse></soap:Body></soap:Envelope>",
    "TIME": "2020-10-27 02:59:23.515"
}
```

#### xml 內容

request

```xml
<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <ns2:checkAML xmlns:ns2="http://kgiaml/">
            <arg0>
                <checkEntities>
                    <ch_name></ch_name>
                    <country>TW</country>
                    <en_name></en_name>
                    <entity_type>3</entity_type>
                    <id>F224316254</id>
                    <seq>1</seq>
                </checkEntities>
                <check_dept>9735</check_dept>
                <check_type>1</check_type>
                <system_type>13</system_type>
                <unique_key>F224316254</unique_key>
            </arg0>
        </ns2:checkAML>
    </soap:Body>
</soap:Envelope>
```

response

```xml
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <ns2:checkAMLResponse xmlns:ns2="http://kgiaml/">
            <return>
                <aml_result>N</aml_result>
                <category_desc></category_desc>
                <dob></dob>
                <error_code>0</error_code>
                <error_message></error_message>
                <id></id>
                <invest_comment></invest_comment>
                <invest_result></invest_result>
                <list_type></list_type>
                <name></name>
                <nationality></nationality>
                <original_source></original_source>
                <passport></passport>
                <seq>1</seq>
            </return>
        </ns2:checkAMLResponse>
    </soap:Body>
</soap:Envelope>
```



### CHECK_NO_FINPF 判斷是否為免財力證明客戶

POST http://172.31.1.51:3061/api/KGI/CHECK_NO_FINPF

request

```json
{
    "CASE_NO": "CC202010000326",
    "APY_TYPE": "2",
    "IS_DGT": "Y",
    "IS_IMM": "N",
    "COMPANY_NAME": "",
    "TITLE": "",
    "CORP_TITLE": "",
    "CUST_KIND": "1",
    "APPLY_AMT": "101"
}
```

response

```json
{
    "RESULT": "",
    "RESULT_CODE": "",
    "YEARLY_INCOME": "",
    "MEMO": "",
    "IS_PL": "",
    "IS_RPL": "",
    "IS_GM": "",
    "IS_CC": "",
    "IS_CUST": "",
    "ERR_MSG": "",
    "CHECK_CODE": "05;06;07;08;10;11;12"
}
```

### CHECK_HAVE_FINPF 判斷是否有 2.身份證 (IMG_TYPE=2)

POST http://172.31.1.51:3061/api/KGI/CHECK_HAVE_FINPF

request

```json
{"CASE_NO":"CC202010000326","CUST_TYPE":"01","IMG_TYPE":"2"}
```

response

```json
{"ERR_MSG":"","CHECK_CODE":"","HAVE_IMG":"N","IMG_CASE_NO":""}
```

### CHECK_HAVE_FINPF 判斷是否有 3.財力證明 (IMG_TYPE=3)

POST http://172.31.1.51:3061/api/KGI/CHECK_HAVE_FINPF

request

```json
{"CASE_NO":"CC202010000326","CUST_TYPE":"01","IMG_TYPE":"3"}
```

response

```json
{"ERR_MSG":"","CHECK_CODE":"","HAVE_IMG":"N","IMG_CASE_NO":""}
```

###  CREATE_NEW_CASE

### 產製 貸款申請書 pdf

CasePDF.previewHtml.html

```html
<!doctype html>
<html lang="en">
<head>
</head>
<style>
body {  height:100%; margin:0px; padding:20px; font-family:'新細明體',arial; font-size: 62.5%;}
.SpecialFont{
    font-family: "微軟正黑體", "Microsoft JhengHei", "微软雅黑", "Microsoft YaHei", "メイリオ", Arial;
}
table{ border-collapse:collapse; border:0px solid #444444; text-align:justify;}
table td{ padding:3px; font-size:16px;}
p{margin-bottom:5px;}
.text-center{text-align:center;}
.fontbold{ font-weight:900;}

.TDcaption{	font-size:14px; text-align:center; border:1px solid #999999; background-color:#dfdfdf;}
.TDvalue{border:1px solid #999999; font-size:14px;}
.tc{text-align:center;}
.fs12{font-size:8px;}
.red{color:red;}

</style>
	
<body STYLE="font-family: mingliu;">
	<div style="width:100%;">
	
		<table style="width:100%;">
			<tr>
				<td>
					<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALYAAAAsCAIAAABHb9V2AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAC65JREFUeNrsnFtsFNcZx88se3ULtaXS2vAQU6m4QFvf0gesJtggtWoqfIEoVHbVmFAriloJTOgTUcAtfUoEttQqjQixU8VWIyVgO4IH1GRNVNmqGmwTKXbgoTYPYJI+2ASCLzg7/Z/5do/PzuzMzs7sBfAejcbj2ZlzZs/3O9/tnFmFlR9lLso+/8TQj9aXNYy6qaSz5m+V365m+fJAFo9LPrq+cfHzTx67OlCV78pHtXhd8sF8EcYUUIIzLnXJai7jn83M3ZmvKCspXBdKe+VD//kv9oVrQxU/KMkeIoIPxasyRWEqyy0l7a+cH79669ndla0NVRltoqKs+NQffmn8tO63Z7B39gAQYV3bmziofXxT+I0DuhbN7jrYvL1x51b7lR97vi57iMTx4Y9wRFCyTsnx1z7oeD1M3YquHPp4akd1KUkLx+iR4y/swr9KxUu6GwvXBs+dbK79yfdSG+haE6aS0D6iB0gZvlcviEqAhUAwaYvq+ImklaOL6CsfaqmR+83Og4VPP4de8rrkQ/GpzOthy5GcUOKszN1ZQP/aQaRnYHT65iwd0wH26GKz6y9dnhafgl07TeB6UhW4Hk/V2TtSvrlEVkU4ryPv+sxcz+CYrTjg7WGCDCrHsQnzuuUj8DXz+Ri7/4BTYqFULMpb74/pxvH0zTmLIYiL5euTIgIEqTZ6vP0vvwfZ7z92FmcEJeCDnly2HXYQQeWkn2Ac5Rpa66uAnZ2vD98oNURW+AAZvigfSjCiIcJyRQm8PHQZ9jS8xHFaCjpXMjTwKBegsanjEhqa0g2FpRuK6Iw4sBBhlIb6ShJh9x/38vODY0DTpVMlrIlmVVumb8ySFsTDl24swpZ+LWLGh+JnzCcqyQEl0NLkjlHn2tTANovsmZKLAwgw4s0QgbuqG/EWIQzxARE21G6hoINqAIjYZE0G+6UbFTZdEDwt+AAQ9PBoa+r8ixkxNAY+IiwQIT6UAItpkWxTAv/ryrVbcm/KJekgdk+kS+UnHKOmw306J1Fn2rA5aKKxbkt3xx64IODPjUeSHJE/BS+1hcYS8BFgiRDJHiXQ/OWbi80+xQDKRKPWhiZVEe6o3jQwNAlchMdqTGDADEG16DAVQZB1MMIR/HJeqCsMKlg3qKUU9OiRp5Ig0hW6uC80wbymfCRCBGWZLX/NEAtHMkgJpGIdvKUa1tr04OTUhSgpecH0bPR4h35dI1IXCWt+rKTQwRcRt0BFkRI69vxOqBB4JCnRDHy9LvngiAT8ie8HJZ4oJYu3C7Y+829v8H4apWXtltt02h1wmSoNOSwIkQgI9AZYJPtr1jN0pU5NQqV5XfKhBM0RkSiZu77+k7//9Me/+Vd6KTE3B6FMqJD0FiNq4oycZoWmtJnpMvIhnHfhYiNQMouVqHWjmvSmzIeEiCfElAJLRCRK7n5emF5KkhoaGjfpLbocuWNDk+lCIUxaqkqASFfBxX1Bu3wooWSIZJKShCEA2XWX2RHj/Ai8Y9rTdIxZqk0XoFrM2oj0ufBFEibUSQFgMFSUFY+983v744eshlnE5xyRlPjge45IgHumSs4oyYRNMZsfoeS92V3GANVi1qbz7WFw3FpfpcucatmXIl0OHojIyOIynEmoz0TkBf8U9TszUqaIOODDs6JFFFu6RE0/JRbD2nnS7MhTsh7q6hvpD09SpHqwebs0TEOUgYUk6IwuRrVIz1y5NgNf4fbdhYbaLbqG0LoujKKD/g8nIHvRlkUZ+8fvSjcWWUwnOUFEz4c/wmdhkvHhgS/itzY0Shwl2n9poUR0XLqMblzlUmaiZ2CU+IB4KBklzJmIFMBK5a/+yqUYnoTI7UzT4zJKBwtE5r6cFyDGqYR1IZrk23/srDAcYNGi8pRS7LYQScJHDBEjH9zQmCGimOiSNFECMcAL0fLKITGZQriAHgJo+sasiHGc5EC1vBOJjVIL5JHIaQORO4fMIG/KlsKBONRSY53KJIxky0UNgQajgKG6cBnxgYbwJGmEIDkiRj6UgGZibPDhCdpwV3W4GChxkcZeIFs7+9FRnhe6MdvxOh/cU+dfpB4UU/nyhJydgqp6Bkeh9kkq5PcZc5q6fDwuE08F1/Vgcw3EaQEK6Qbh4dLT6nKpgie6mJyMrPGB4nHJB8+L+PycEr+20QEc2KA46ePpV5935cC7hiOj8I0oWV7wOVYkJBUaiDQKQYPoQTFAU52ygfKAmMWotTn7BdUFJ4ByU/BbgVRn77DF9dAN5042i6QFbsTxpct8YZHxYm7jNFKbDvdCvWUNEe8P1/yPeVRljcrWRPfMqypeFrdBgn7OCu355uPnmfZpePd6FlGZGlnZRyJM1fbiOPppbMMxHFce53Fd7ftqnRuPRBuIfHi99f4oDeKiJ05QCmhgaJLRHH3qww4VwicQQ1YXkVIiJGGaBGdgfYSHa90E9BAeXg7XE6aGUSG+ESiBFdNU15nujr3OFhqmjMieu0+fZe9uY18oXG4eJSq6SPSvVCKM4taY/omdrP2+342lQDxy6sgvUr2RDMGVa7dIOXf2jmATkSdhgWsoUDSqbsQRSSMa1OB4pRZNwWCsW6QlEmbboALBBxxY7d55yrWQMYXjBa0JSqDhiBLgm670oIVa8t5WAy4pccmHzgG0i8jNWV3EjxGJzi3fXCxWBDa197L4ZZsrOYneEfJzmcnkGQ3QpPOiFpk0QIZK7EBGWOyo3oQ9XQ+dUfTkn2Xihcfd2lAFdQJbQ4asq2946sIR5xlqLb8iO21kuPXuqhNKYobCo+aAD9ndQ/+Wby4xRgEiUoWoZDmR00eeJk/St2y3BtE6orbIpCVN76JpGYvEseuGQlIqchQN8uDxwMuBLXO2lERnpnXpYEOkEXsb71vK4tlvvrst8AXPmPlUvo/GvWqc6xqM91tDzP9z1SUfYnFDOtOj2pspCXOvGDrgg95boYV6FuZMDC8Hve/YTuHh+8MTjXVbrb0NyqNYLGEUi4la66vMvqZYwg2PnneIoUVFfmHTLiUBjZKCKCXBRtWl/sgEIvmStqA3zonTLM6ni99R73vYfYXvlzzqgkddVNRFFt2WtP09FrnH1HkWmWcu+ciXhwmRJJQssBVQJEryfKwuRKwoWYpRshRHSZ6PVYeILV0iUZLnYzUiYkrJYgJK8nysUkRsUWIDkTwfjzIiBko8ekqWkiCSOT7GP+NpTcT0FNmL4+OvfVC57y9KxUtFT5xoau/leXrtU1yvu5e2/S+/JxYMMG11Es7Il/V/OCE+bX/lPF+waGg6LSt3HlZE4ilROB+gZFHyS5Zyoz/4Qo2Pp6ZvztKbj/i3sW4LmhNvUdPasNKNRZQhlXOddK+WLCrsGRxrOtwrkkg4jzMEDV3GJ0RieI1fvUU5N9F0+6sX8K8xwb+6EImjZFmjZDnOe82hfbk+M1fXdqZwbTB8+kB/eBKSozXAx1/Yhe3cqRaLe3dUl9Jr1mKmratvmGYoegZH475I2xnjLNftuwvEVnfH3kz8tNBDhog1JTn0P2iV17mTzfSeGeMrMOwO6EuXp2GSNJWzk8wKNASf0OGLClam7qCciBLd7Z29fH6ku2NPdmbkHwJELCjJoX9Kg76rb8TaWUlYgBTNnUJ5QElgz2LvokH2YrkhDFZrfSV/jTZ+mQ+9M5yw6dWLiBklcYLRJsezFr9AfrWPb4KJgcNI68qEAhAeg9m9z+6uDL9xgMSPK2lauOlwH7kpsiLh0/plxVAb8vfC7VAw9NNTeUSSUZIjPqjQcj14qRjWECRkRmEIrTez9mPERKj2ev5C+PRz6viJ2Y+OChMW1VXrQudOthhfW6KmgY6FrlqNiBAlu+60vHNvG1Ei8+H+3a9UC2IWiIoUADxHTSvM4BiSk38zKKEfU9f2Jh4Y4Q/9tgzNNgMIekdBhgytwCPWmzmOTjPTfs9OjpwfsaK4+fVmvjS64NPv/vMrl3zkFwM8yMXr5uaD936G/ZM50h/5kp3yfwEGAEviyhiCSxHKAAAAAElFTkSuQmCC" style="width:160px;" alt="" />
				</td>
				<td style="text-align:center;margin:0;font-size:24px; font-weight:900;">凱基商業銀行個人貸款申請書</td>
				<td style="text-align:right; font-size:14px;">( Web版-10902 )</td>
			</tr>
		</table>
		
		<table  cellspacing="0"   style="border-collapse:collapse; width:100%;border:1px solid #777777;">
			<tr>
				<td style="width:10%;font-size:12px; border:0px solid #444444; border-left:0px; border-right:0px;text-align:left;">會員編號：</td>
				<td style="width:15%;font-size:12px; border:0px solid #444444; border-left:0px; border-right:0px;text-align:left;"></td>
				<td style="width:15%;font-size:12px; border:0px solid #444444; border-left:0px; border-right:0px;text-align:left;">卡號／帳號：</td>
				<td style="width:15%;font-size:12px; border:0px solid #444444; border-left:0px; border-right:0px;text-align:left;"></td>
				<td style="width:25%;font-size:12px; border:0px solid #444444; border-left:0px; border-right:0px;text-align:left;"> 放款帳號：</td>
				<td style="width:20%;font-size:12px; border:0px solid #444444; border-left:0px; border-right:0px;text-align:left;"></td>
			</tr>
		</table>
		
		<table cellspacing="0"  style="border-collapse:collapse; width:100%;border:1px solid #999999;">
			<tr>
				<td width="20%" class="TDcaption">申  請  項  目：</td>
				<td colspan="3" class="TDvalue">
					<div style="padding:5px; float:left;"><img alt="" style="width:20px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJzSURBVGiB7Zo9SBxBGIafCImKKNyhgpIqpEshasDqRBsLG5GEYIqEVCEEQkQUKyOIRYoUdjaWgoXiIdgIFrFLJATEQoRgYwQlBk08JUT8KfY7VmW9m5n9G2VfGGa5/X7e9/ZmZ76ZgwQJEvhBLTAMLAM7wFnEbUdyDwE1piKeA7kYyF/XDoBnuiKeAqcSYBrIACndIAEgBbQCM8LlFOhWda4G/ojj+zDYGaIPh9MekFZxGMF9ErYhi8NtWMV4RYwzYTIyRBsOt+8qxgdiXBEiIVNU4nD7q2Kcf0vYCk9+JTEQCQWJENtgo5DSoALFOdhfAD+BxwVslPnFJaQFOJLcbwvYWS3kIbAreT8VsbVWSApYk5xZio9bK4XcAz5LvmWgXMHHSiETkmsTqFf0sU5Ir+TJAU0aflYJ6QCOcQqlHk3fQIWUAgvAa00SAA24K+xBA/9AhTzBLT3faZCoATbEdxK4o+Grw0/L8A1wIrYfFUhVAN/EfhG4q0LGBz+tMdID/Bf7ca6fA0pwNxDWUay5/fLTHeydwKH4TOH9TY/K/V/AA43YvviZvLUywL74zXN5Ynspn/8jmH2A0F+/jbi7kUtAFdCO+9N7ZRDTFz8/88gjYEv8vwDbcj1mGM8LkU2IdcDqhTizBFvARTqzp3GeyFfUFoI6iHyJkgLuBxDnKqxaa/lBsq91I3CrheSkt3UTG5wy4BK8hGxIr1O1RYVm6X9cveElZE76/tDomGNA+rmCVoKLR299YTEywACaR2/gfRhaFga7IijHx2FoHt3AbwofGUfZdoEuXRF5pIEPOJtn+eIpynaI+4eBOI7HEyS4NTgH3tZKOTc9lX8AAAAASUVORK5CYII="/></div>
					<div style="padding:8px 20px 8px 0px; float:left;font-size:18px;">個人信用貸款</div>
					<div style="padding:5px; float:left;"><img alt="" style="width:20px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFFSURBVGiB7ZqxSgNBFEUPVopYJKgfonXEP5CACH6FEEypAX9HIeQ3LAR7sQ8ooknaaDFvicqosxPHfcg9MLxi3+zew8IW8xaEEMuwDQyAa2AMvP7xGtuzz4CtXIljYNpA+K/WBDiqK3EIzO0Gl0AHaNW9yS/QAvaAK8syB7qpmzeBZ9t4UiJdJj1CpiegnbLhgsWb8MaQkG2Q0nxrzZ2SiTLZJ2S7SWmeWPN6wUC5bBCyvaQ0V18Jr0TzrTQQpAgS8YZEvCERb0jEGxLxhkS8IRFvSMQbEvGGRLwhEW9IxBsS8UZMZGrV6yE2hIP2D8RE7q3uFIuTz67Vu88XYiIjq6fF4uTTtzr6tst4P3rrlUqUQZ+aozeID0NXS6T7gTWWGIZWdIFHmh9LV+sBOKgrUdEGzglD+1kD4WcsfhhoYjwuxL/hDTwNt7HmQsC4AAAAAElFTkSuQmCC"/></div>
					<div style="padding:8px 20px 8px 0px; float:left;font-size:18px;">小額循環信用貸款</div>
					<p style="color:blue;font-size:9px">【注意事項】小額循環信用貸款申請人若約定本行存款帳戶為證券款項劃撥約定帳戶(以下簡稱證券交割帳戶)，則本行將於申請人之證券交割帳戶餘額不足抵扣應繳付證券公司之款項時，逕自申請人小額循環信用貸款放款連結帳戶轉撥不足金額至證券交割帳戶，且無須申請人之存摺、取款條、留存印鑑或申請人簽發之支票、本票等支付憑證。
					</p>
				</td>				
			</tr>
			<tr>
				<td width="20%" class="TDcaption">申  請  金  額：</td>
				<td colspan="3" class="TDvalue">
					<div style="padding:8px 20px 0px 8px; float:left; font-size:16px;">個人信用貸款</div>
					<div style="padding:5px 5px 0px 5px; float:left; font-size:16px;"><img alt="" style="width:20px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJzSURBVGiB7Zo9SBxBGIafCImKKNyhgpIqpEshasDqRBsLG5GEYIqEVCEEQkQUKyOIRYoUdjaWgoXiIdgIFrFLJATEQoRgYwQlBk08JUT8KfY7VmW9m5n9G2VfGGa5/X7e9/ZmZ76ZgwQJEvhBLTAMLAM7wFnEbUdyDwE1piKeA7kYyF/XDoBnuiKeAqcSYBrIACndIAEgBbQCM8LlFOhWda4G/ojj+zDYGaIPh9MekFZxGMF9ErYhi8NtWMV4RYwzYTIyRBsOt+8qxgdiXBEiIVNU4nD7q2Kcf0vYCk9+JTEQCQWJENtgo5DSoALFOdhfAD+BxwVslPnFJaQFOJLcbwvYWS3kIbAreT8VsbVWSApYk5xZio9bK4XcAz5LvmWgXMHHSiETkmsTqFf0sU5Ir+TJAU0aflYJ6QCOcQqlHk3fQIWUAgvAa00SAA24K+xBA/9AhTzBLT3faZCoATbEdxK4o+Grw0/L8A1wIrYfFUhVAN/EfhG4q0LGBz+tMdID/Bf7ca6fA0pwNxDWUay5/fLTHeydwKH4TOH9TY/K/V/AA43YvviZvLUywL74zXN5Ynspn/8jmH2A0F+/jbi7kUtAFdCO+9N7ZRDTFz8/88gjYEv8vwDbcj1mGM8LkU2IdcDqhTizBFvARTqzp3GeyFfUFoI6iHyJkgLuBxDnKqxaa/lBsq91I3CrheSkt3UTG5wy4BK8hGxIr1O1RYVm6X9cveElZE76/tDomGNA+rmCVoKLR299YTEywACaR2/gfRhaFga7IijHx2FoHt3AbwofGUfZdoEuXRF5pIEPOJtn+eIpynaI+4eBOI7HEyS4NTgH3tZKOTc9lX8AAAAASUVORK5CYII="/></div>
					<div style="padding:8px 20px 0px 0px; float:left; font-size:16px;">新臺幣101萬元</div>
					<div style="padding:8px 20px 0px 8px;"></div>
					<div style="padding:8px 20px 8px 8px; float:left; font-size:16px;">小額循環信用貸款</div>
					<div style="padding:5px 5px 0px 5px; float:left; font-size:16px;"><img alt="" style="width:20px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFFSURBVGiB7ZqxSgNBFEUPVopYJKgfonXEP5CACH6FEEypAX9HIeQ3LAR7sQ8ooknaaDFvicqosxPHfcg9MLxi3+zew8IW8xaEEMuwDQyAa2AMvP7xGtuzz4CtXIljYNpA+K/WBDiqK3EIzO0Gl0AHaNW9yS/QAvaAK8syB7qpmzeBZ9t4UiJdJj1CpiegnbLhgsWb8MaQkG2Q0nxrzZ2SiTLZJ2S7SWmeWPN6wUC5bBCyvaQ0V18Jr0TzrTQQpAgS8YZEvCERb0jEGxLxhkS8IRFvSMQbEvGGRLwhEW9IxBsS8UZMZGrV6yE2hIP2D8RE7q3uFIuTz67Vu88XYiIjq6fF4uTTtzr6tst4P3rrlUqUQZ+aozeID0NXS6T7gTWWGIZWdIFHmh9LV+sBOKgrUdEGzglD+1kD4WcsfhhoYjwuxL/hDTwNt7HmQsC4AAAAAElFTkSuQmCC"/></div>
					<div style="padding:8px 20px 8px 0px; float:left; font-size:16px;">新臺幣____________萬元</div>
				</td>				
			</tr>
			<tr>
				<td width="20%" class="TDcaption">貸  款  期  間：<br/><span style="font-size:10px;">(個人信用貸款)</span></td>
				<td width="30%" class="TDvalue tc" style="">5年</td>
				<td width="20%" class="TDcaption">資  金  用  途：<br/><span style="font-size:10px;">(個人信用貸款)</span></td>
				<td width="30%" class="TDvalue tc" style="">償還貸款</td>
			</tr>
		</table>
		
		<table  cellspacing="0"   style="border-collapse:collapse; width:100%;border:1px solid #999999;">
			<tr>
				<td colspan="8" style="text-align:center;background-color:#0088EE;border:1px solid #0088EE; font-size:16px;"><b><font color="white">基　本　資　料</font></b></td>
			</tr>
			<tr>
				<td width="15%" class="TDcaption">申請人姓名</td>
				<td width="20%" class="TDvalue ">蔡ＸＸ</td>
				<td width="8%" class="TDcaption">性別</td>
				<td width="7%" class="TDvalue">女</td>
				<td width="10%" class="TDcaption">公司名稱</td>
				<td colspan="3" class="TDvalue">德義資訊股份有限公司</td>
			</tr>
			<tr>
				<td width="15%" class="TDcaption">出生日期</td>
				<td width="20%" class="TDvalue">民國67年12月31日</td>
				<td width="8%" class="TDcaption">婚姻</td>
				<td width="7%" class="TDvalue">已婚</td>
				<td width="10%" class="TDcaption">職稱</td>
				<td colspan="3" class="TDvalue">課長&nbsp;</td>
			</tr>
			<tr>
				<td width="15%" class="TDcaption">身分證<br/>統一編號</td>
				<td colspan="3" class="TDvalue">F224316254&nbsp;</td>
				<td width="10%" class="TDcaption">年收入</td>
				<td width="15%" class="TDvalue">150萬元&nbsp;</td>
				<td width="10%" class="TDcaption">到職年月</td>
				<td width="15%" class="TDvalue" style="">民國____年____月</td>
			</tr>
			<tr>
				<td width="15%" class="TDcaption">教育程度</td>
				<td colspan="3" class="TDvalue">大學&nbsp;</td>
				<td width="10%" class="TDcaption">公司電話</td>
				<td colspan="3" class="TDvalue">(02)25508556 分機:104&nbsp;</td>
			</tr>
			<tr>
				<td width="15%" class="TDcaption">不動產狀況</td>
				<td colspan="3" class="TDvalue">本人所有&nbsp;</td>
				<td width="10%" class="TDcaption">電子信箱</td>
				<td colspan="3" class="TDvalue">charles.yen@frog-jump.com&nbsp;</td>
			</tr>
			<tr>
				<td width="15%"  class="TDcaption">戶籍地址<br/><span class="fs12 red">(既有戶免填)</span></td>
				<td colspan="3"  class="TDvalue">&nbsp;
				
				</td>
				<td width="10%"  rowspan="2"  class="TDcaption">戶籍電話<br/><span class="fs12 red">(既有戶免填)</span></td>
				<td colspan="3"  rowspan="2" class="TDvalue">(02)22519000&nbsp;
				
				</td>
			</tr>
			<tr>
				<td width="15%" rowspan="2" class="TDcaption">現居地址</td>
				<td colspan="3" rowspan="2" class="TDvalue">260宜蘭縣宜蘭市宜興路一段102號&nbsp;</td>
			</tr>
			<tr>
				<td width="10%" class="TDcaption">公司地址</td>
				<td colspan="3" class="TDvalue">103台北市大同區承德路一段70-1號15樓&nbsp;</td>
			</tr>			
			<tr>
				<td width="15%" class="TDcaption">居住電話</td>
				<td colspan="3" class="TDvalue">(02)22519000&nbsp;</td>
				<td width="10%" class="TDcaption">行動電話</td>
				<td colspan="3" class="TDvalue">0918300000&nbsp;</td>
			</tr>			
			<tr>
				<td width="15%" class="TDcaption">推廣單位</td>
				<td colspan="3" class="TDvalue">&nbsp;</td>
				<td width="10%" class="TDcaption">推廣員工</td>
				<td colspan="3" class="TDvalue">117033&nbsp;</td>
			</tr>
			<tr>
				<td width="15%" class="TDcaption">轉介單位</td>
				<td colspan="3" class="TDvalue">9735&nbsp;</td>
				<td width="10%" class="TDcaption">轉介員工</td>
				<td colspan="3" class="TDvalue">117033&nbsp;</td>
			</tr>
			<tr>
				<td width="15%" class="TDcaption">專案代碼</td>
				<td colspan="3" class="TDvalue">&nbsp;</td>
				<td width="10%" class="TDcaption">通路來源</td>
				<td colspan="3" class="TDvalue">凱基商銀&nbsp;</td>
			</tr>	
			<tr>
				<td width="15%" class="TDcaption">來源IP</td>
				<td colspan="3" class="TDvalue">&nbsp;</td>
				<td width="10%" class="TDcaption">身分認證</td>
				<td colspan="3" class="TDvalue">OTP&nbsp;</td>
			</tr>				
			
		</table>
		

		
		<table  cellspacing="0"   style="border-collapse:collapse; width:100%;height:100%;border:2px solid #ff0000;">
			<tr>
				<td style="width:100%;">
					<p style="color:red;font-size:9px">
						聲明暨同意事項：<br/>
						<span style="color:black;text-indent:40px;">1.凱基商業銀行(以下稱貴行)所提供之線上個人貸款申請書(以下稱本申請書)為貸款契約書之一部分，與書面契約具有相同之效力。</span>
						<br/>
						<span style="color:black;text-indent:40px;">2.申請人及貴行同意以電子文件作為表示方法，依本申請書交換之電子文件，其效力與書面文件相同。</span>
						<br/>
						<span style="color:black;text-indent:40px;">3.申請人及貴行同意得以符合「金融機構辦理電子銀行業務安全控管作業基凖」所訂之安全規範或以電子簽章法之簽章或中華電信行動身分認證，做為申請人之身分識別與同意本申請書之依據，無需另行簽名或蓋章。</span>
						<br/>						
						<span style="color:black;text-indent:40px;">4.申請人瞭解貴行保有核貸及決定借款金額、借款期間、借款利率、本金及利息攤還方式、費用之權利；撥款時，經再次查詢財團法人金融聯合徵信中心(下稱聯徵中心)，倘發現有其他新增核准授信額度應計入金融機構無擔保債務且歸戶後之總餘額除以最近一年平均月收入超過主管機關所規範之倍數，貴行保留最終核貸與否之權利。</span>
						<br/><span style="color:red;">5.申請人聲明已經合理期間詳細審閱後同意「申請人可能負擔之利率及各項費用」、「蒐集、處理及利用個人資料告知書」及「電話/網路/行動銀行約定條款」之內容，同意  貴行、其委託之第三人及前述告知書所列資料利用對象得依告知書及其他法令許可範圍內對本人之個人資料為蒐集、處理、利用及國際傳輸，且貴行於辦理授信業務之目的範圍內，得向聯徵中心蒐集、處理及利用本人之租賃、分期交易或貸款等相關資訊；本人並同意貴行得提供各項業務、金融商品或服務的相關訊息，及寄送各項業務之消費、行銷或優惠活動訊息；如申請人未同意，貴行即無法提供前述訊息。申請人瞭解貴行得依金融控股公司法第43條第2項規定，將本人「姓名」及「地址」(含e-mail電郵地址)提供予貴行所從屬中華開發金融控股股份有限公司內各子公司作為共同行銷建檔、揭露、轉介或交互運用。申請人得隨時以書面、電話或電子郵件等方式通知貴行停止對申請人上述資料之交互運用。貴行於接獲通知後，應立即停止並通知上述公司停止使用申請人資料。（上述公司名單將揭露於貴行網站，貴行得隨時新增或變更。申請人如欲了解，得隨時至貴行網站查詢）。</span>
						<br/><span style="color:black; text-indent:240px;">6.申請人聲明本申請書所載內容及檢附之資料均為真實，並同意 貴行得向有關單位查證，無論貴行核貸與否，申請書及所附文件毋須退還申請人。</span>
						<br/><span style="color:black;">7.申請人聲明本申請書是由本人所填寫或授權貴行人員代為填寫，且經本人審核正確無誤，並聲明本次申請未有透過代辦公司辦理之情事，亦瞭解除貴行貸款契約書所載費用外，並無貴行員工或其他人員向本人收取額外費用或報酬。</span>
						<br/><span style="color:red;">
						8.申請人同意貴行及受 貴行委任代為處理事務之人皆得就與本申請書及各項業務往來有關事項之雙方口頭及電話談話予以錄音，並得自行決定保存電話錄音之期間。在任何爭訟程序中，並得以該錄音作為本人意思表示之證據。</span>
						<br/><span class="fontbold" style="color:red;">
						9.本次貸款未有貴行行員勸誘本人以貸款方式取得資金於貴行進行投資。</span>
						<br/><span style="color:black;">10.申請人同意貴行得調閱本人與貴行其他業務往來之基本資料或財力資料(包括存款帳戶之往來明細)，並以之作為本次申請貸款審核之用，並得將本次貸款有關作業(如資料登錄、處理、輸出及其他經主管機關核定得委外辦理之作業項目)，委託適當之第三人(機構)處理。</span>
						<br/>						


						<br/><span style="color:red;">提醒您：</span>
						<span style="color:black;">●本行不受理代辦貸款之案件，如您於申辦貸款業務過程，遇有代辦業者要求匯款或收取任何現金，或要求您於同一時間共同辦理收件或對保之情事，提醒您請勿受騙，以維護自身權益</span>
						<span style="color:red;">●貸款或核卡後如未按時依約繳款，本行將依主管機關規定報送登錄金融聯合徵信中心信用不良紀錄，而可能影響您現有卡片之使用及未來申辦其他貸款(含現金卡)或信用卡之權益。上述信用不良紀錄之揭露期間請上聯徵中心網站(www.jcic.org.tw)「社會大眾專區」之「資料揭露期限」查詢。<br/>客戶意見專線：(02)2232-1296 </span>
					</p>
				</td>
			</tr>
		</table>

		
		<table  cellspacing="0"   style="border-collapse:collapse; width:100%;height:100%;border:2px solid #ff0000;">
			<tr>
				<td style="width:100%;">
					<p style="color:black;font-size:16px; ">
						申請日期：中華民國109年10月27日<br/>
						<span style="color:red;font-size:9px">**貸款或核卡後如未按時依約繳款，本行將依主管機關規定報送登錄金融聯合徵 信中心信用不良紀錄，而可能影響您現有卡片之使用及未來申辦其他貸款(含現金卡)或信用卡之權益。<br/>上述信用不良紀錄之揭露期間請上聯徵中心網站(www.jcic.org.tw)「社會大眾專區」之「資料揭露期限」查詢。</span>
					</p>					
				</td>
			</tr>
		</table>
		
		<table  cellspacing="0"   style="border-collapse:collapse; width:100%;height:100%;border:0px;">
			<tr>
				<td style="width:100%;text-align:right; font-size:12px; letter-spacing:3px;">
				數位申辦平台案件編號：LOAN2020102700008					
				</td>
			</tr>
		</table>
		
	</div>
</body>
</html>
```

## 

## UPL/step0

## UPL/step1 上傳身分證

### /OCR/getIDOCR 身份證正面

request

```json
{"BYPASSID":"F224316254","IDFront_CONTENT":"/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAEwAh4DASIAAhEBAxEB/8QAHQAAAQQDAQEAAAAAAAAAAAAAAAQGBwgCAwUJAf/EAGUQAAIBAgUBBgMDCAUECg4FDQECAwQRAAUGEiExBwgTIkFRFGFxIzKBCRUYQpGWodMWUlhisSQzcsEXJTQ1OEOCtdHwJlR1hIWSlaKksrTU4fFFhpSlxCcoREZVZGVmc3Sjs8L/xAAcAQACAwEBAQEAAAAAAAAAAAAAAgEDBAUGBwj/xAA7EQACAgEDAgQDBgMIAQUAAAAAAQIRAwQSITFBBRMiUWFxgRQVMpGhwQYjQgcXJTM1Q7HhU3Ki0fDx/9oADAMBAAIRAxEAPwDyqwYMGAAwYMGAAwYMdLTmmdSaxzmn05pHT+ZZ5m1Xv+HoMtpJKmom2IXbZHGCzWVWY2HAUnoMAHNwYkk92fvHgXPd+7SR/wDVSv8A5WD9GbvIdf0fu0n91K/+VgAjbBiSf0Z+8he36P3aT+6lf/Kx9/Rk7yQ693ztK/dOv/lYAI1wYkr9GXvI/wBnztK/dOv/AJWD9GTvJf2e+0v906/+VgAjXBiSv0Y+8n/Z77S/3Tr/AOVg/Rl7yI693ztK/dOv/lYAI1wYkod2TvJHp3fO0o//AFTr/wCVj7+jF3lP7PXaZ+6Vf/KwARpgxJn6MPeV/s89pn7pV/8AKwfow95X+zz2mfulX/ysAEZ4MSX+jD3lP7PPaZ+6Vf8AysH6MPeV/s89pn7pV/8AKwARpgxJn6MPeV/s89pn7pV/8rB+jD3lf7PPaZ+6Vf8AysAEZ4MSZ+jB3lf7PHaZ+6Vf/Kwfowd5b+zx2m/ulmH8rABGeDEmfowd5b+zx2mfulmH8rB+jB3lun6PHab+6WYfysAEZ4MSb+i93mP7O/ab+6OYfysH6L3eX/s79pv7pZh/KwARlgxJv6L3eY/s79pv7o5h/Kwfovd5j+zt2m/ujmH8rABGWDEm/ovd5j+zt2nfujmH8rB+i73mP7O3ad+6OYfysAEZYMScO673mT07uvad+6OYfysH6LneZ/s69p37o5h/KwARjgxJ36LveZ/s69p37o5h/Kwfou95n+zt2nfujmH8rABGODEnfou95n+zt2nfujmH8rAO653mT07uvad+6OYfysAEY4MScO653mT07uvad+6OYfysH6LneZHXu69p37o5h/KwARjgxJ36LveZ/s69p37o5h/Kwfou95n+zr2nfujmH8rABGODEm/ou95j+zt2nfujmH8rH39F3vM/2de0790cw/lYAIxwYk79FzvM/wBnXtO/dHMP5WPn6LveY/s7dp37o5h/KwARlgxJv6L3eY/s7dp37o5h/Kwfov8AeX/s79pv7pZh/KwARlgxJv6L3eY/s79pv7o5h/Kwfovd5j+zt2m/ujmH8rABGWDEm/ovd5f+zv2m/ujmH8rB+i93mP7O/ab+6OYfysAEZYMSb+i/3lx17u/ab+6WYfysfP0YO8t/Z47TP3SzD+VgAjPBiTP0Ye8r/Z57TP3Sr/5WPn6MPeV/s89pn7pV/wDKwARpgxJZ7sPeUHJ7vPaZ+6Vf/Kx8/Rj7yf8AZ77S/wB06/8AlYAI1wYkv9GLvKf2eu0v906/+Vg/Ri7yh6d3rtL/AHTr/wCVgAjTBiSz3Y+8mOvd67S/3Tr/AOVhs6z7Me0ns5+D/wBkLs91Lpf84+J8H+ecpqKL4jw9u/w/GRd+3el7XtuW/UYAG1gwYMABgwYMABgwYMABgwYMABgwYMABiyP5Og275PZ90/8Apbr/ANyqvFbsSv3XNVVeiO2ag1Xl8pjq8tyXUE1M4/Um/M9YEb6BiD+GJXUGevOedsGv+0TVObaM7EEy/L8tyKRqTNNWZlA1TF8Up2vTUUClRNKrEKzswRTxZjwdCaA7W3B8TvKaulqGZkJhyrLFjWS44C/Ck25PqeCvOO92Y6QpNDdnmmdJw7EioaKCOUwmxlnZA8zbv1nZjct7v9cO6LxGgDyCONHKg2FvIWBZVPpwVF/7uNscUYq2UykyOJez/taRW+H7yur2O0ssbZPlW/oCN16YWsLk/hjYNA9skanZ3nNSMWViv+0eVNyvX/8AR+Ru4v8ATEgpGauWyI13EZVS5ADMzEqPoB1N+E+Yxk8rRBSZUKsV3MQxBAII553KGuTbggWJ4w3lxfFC72R6dD9uQt4feWz1txYIH05lfm5AB/zIsDcdfY/LGc2i+3lCGpe8pmc0bMdt9LZbdlBtuA2jg2PX19hiRqIEGAeI0sjnw7223sGLMwPRVsPTkgmx4OM1iFQ4+JQyXYMqMtztXz8jqF81rc8E35sMCxxTqiXJsjek0t28zIHHeVqgLtuvpTL2UAKOAdouSxsB0sDySMKY9G94CYrHF3kGYhiGJ0hQECzbbXDcn1v0AIviRamghVPGBMRiO7eX5IQXZyf1RuIAHUbrXF8Y000Ib4edFiRHsCgIO0liwQG993AF+doHW2J8qD5RFsjuo0h3iKeJWp+8dCWO4/aaPo/LckIOGFyQrG3pxjdTaY7wlRD4q95KmUlbhTo6kuD6k+cWQDkk9OmJFjWSVywkVpFkALgXCSBegJ+8S1zf0KjnkDG2op4bmY2Qq4VQo3AR+UBLdXJ8zWN+huLnC+VFcDW+xHC6b7xpKhe8NQsrAE/9hVOx6E8ETc2sBjXPpnvJwRCQ94PKj5QWH9DacbW5uAfHsbXW5+eJNpqxg6x1bfDTICrcWVjwBZum0WJt7j0ON8KKwUIilbEghf1Wvwv9UWBH7fXFbgkFu+SMU0x3kGRXTvB5Q25QQx0VCVu33V/3QCeLE2BtjcumO8qZERe3vIWDAWB0VGXNybcCqt6X9rdSPWQpIjS/amVRC6lWSMjyE9dt+CQpAP8AD1utVo3p2kXcysoG4E2Ba/APBY2NyeQORxfFMkkWIiuHIu83J4iS9u2mEaMcKNFAn1+8Pi+AALk3tjJNO95t32/7O+mSxN9i6JLMF+f+VDzHjg24uTa3MlzwSSMBDBEkpIPhi20j/R/0ebn6/TdC0W0ptUsgsUDX22HmO3ooFwL+vvzilyH2kXQ6f70Ds0Y7a9MNIDtC/wBCSbm5vyK2w4F+bfLGf5n7zQJUduWjeOpOi2tb3Fq0kjr+rz8gDiSpKdJGVY6eNZ4kIEShQSOoUrtPB6m/1NuMb6Ob4qKORp/v3Mik9bdSwtwAeBc+2K3NoZRIppqHvOVFgnbVoxnLABV0W7+W19x21psCLe/PHGFD5P3p443mh7XdEypGpZw2ip1Yj3H+W2I+pF/T5ydVbI2erRVdJCIpBIC4ZfQsSRu6/dHFv4bfGVow0cIlC7WZzsAPzNmAHpZSf42BXfaJSS6kYfmbvV3unbBoTaLEudF1IHzPNYOP+g/XBHlveleSVE7W9EN4Xm3HRNQFKW4NhW7uTxwOgxIsBkV3pNjwlAZEfkNs6Ddz046D3HubhuStfv4hAhZniJYqxFiWb1va3mFhfCrIwUVdkbzUvejiEL/7LWhHSRjGSNGVCkNYkWBrhu544sOeeeMbJcu71ccTuO1HQRZY2aw0dVdQL2/3bx872/wBkqqpJJ6dvBgDSkBYTEoJDA3UXvbqBzcn298ZRLS1dLHK0Ebb1E5PggKL8jgkGwva7WuRf6s8lhtRF6U3emtHv7V+z1RKeL6Um4FrgH/L+vy+nTHyng71c4Ji7U+zkhCVa2kaom/ytW259B1P8cSHTVlNRQxR1lR4LqxjiU/e2/3UUFv6puACdx9BcEeZ0tO8rgSwROw8IzxtFGbLtY2KhBxa9zcXHucIsjpEOHuR89L3rxU/Dp2p9mwuu7c2lqlbc9CDW2B5HFz8r4zqKPvUQ0zT/wCy1oFtpCkDRlUbEkC1/jfn6gYkWWaSaspXSqjEfhuFkScJfp06G3lt0N/TBXQU60/jSBSxKuhRQeNwH6oIP/KIPsL4fzGS40R62X96dKjwR2saAYki19G1XS1z5RXbv4W+eMY6DvYSAsnaf2fAB3RWOkanaSpt6VpPvxYfXEofDxGNZGsKZWYKhiYFSfkdzL9evyGOelVTUlBDUV1QqmQbk37FLc3+rdelxbk2AAtDyc8k7L4RHIg71DzMg7XezsqoU3XSNTe5B4sazk8XsLm18bZaXvSpJDFH2v8AZ+7ytby6MqiOhN7CtJ9OnHyPs/qbNaNpZpqueeAtsjvIXCsqi4uxXpc+pvx7c4VJPT1dXGUmSSFUMitHI7XYm3zAayn1sb8++E8xhsI4ko+9HBEZpe2DQqqoLOU0TUsAPQ/7uHB97W97WxnHlHepZI3PbHoUs9rqujJmA9xda0knr6Dp+OJEzCJ0aGnjkVPFNmtdb2szBWPHPQgn1Fh642z1cSq5knd1bgKCEbcbGwKqCCDb0t/jh/MoNhGEdD3pnZ9/bLoQLG+wuujpSG46rau59Ljgg39sE+X96GJVWTtn0bukYBCmiZQpubcFq25I62t6YkyAGKkQeFNMkljIXlOwyE3Js/Cng9L+17841QnxKqaZZlb4clI9sYVr+pZfMvqVvcdDiVMhpEdHJ+9CF3Sdteko2W+9Tok+X16fGkkWHJAxrXLO801YaZe3bSPhxrd5Doq3mtewtWn0PBuOnpiTZBSU15liQvISkZa4Dkngi4KkH1F+BjOGh8BHhik2vw5kYOHLevDAAEc2I9D62w6dhtIzORd5uTYI+3nTLM1zsTQ4LEe4/wAtI/Dn64xTT3ealldF7ftOBUawP9Co1LWHO29UeeeAfY4kepWoqHSjpZW8SQESA+U3+akL5unrc3vcY2RE0SCnSYLHGzeUqRusfUEnzC546kA9cWJoTb8SM3073mBHu/SEyJRusWOhk22PQgioII5FwOR+BxrXTneRmZiveGykpEpaYx6JgJVQQCVAqCSByT8rdMShUz00sckKuWeRbySRBmCAjzW233LZgevB+ePpQwssfjb1BBDMQV3j1LDkblPU+x62uJSQVXNkXSaT7yKll/SRorrcBk0XSBGbni5lNri1r+txjXNpLvErDHMO8zGSQWYLoui8ykXDL5jx736HjnEqVFcI1WXeskskZCALz8mDHi3JDCwHXj0KEJGhllq4IWcx7JH52gG6srC102kDm/uSTuvh0o2Q1Ssjk6M7xQUmfvLS3AWzRaPoNpJYgi5HB49vT5jH19C9vjfd7z1cGCbm/wCxTLRs9NxGy9g1geenOJKkqEqG8RZbXS7hzclWADEjq1iFBt6cg+mEkLyVxXxT4dLEpLOBYlW27yoNrDoTYEexF7YtjGDE2tckZHR3b86uY+9BmZMKbpiulsrdV4BJBEd+LjqAOep6Y2T6D7d4nZZe9BnRZELOI9N5UCbeq/YnixU268/LEnqfhd0aO77Nu5evBLKxW58wNybH3HtjCaWFE8ecmHYitYG+690YrzyQ1rj2463OLdkfYV0yNDoDtrZFk/Se1IVVN8pXT2U2CgqGK/Yc2uTb2/ZhGmhe2p1Mad6DUbzDcLjJcp2Ei/r8Nxa3It6YlKE1lVH4RMioiBhti8x4FyFPO7afS9736nj54TQTsYJldlDuNpuSQwIZW/WG08g/3h7YlRguxHyIzfs47aFliVu87qyTxACfDybKQQChYOP8m5HHQcixB5tj4vZr2vSxmVe8/rUkfdRctyglrre4tS+//qn5Yk94y6p4IYMAzra/JVrsU/vcbrEdTb0BwmmliijBj8IbpQ0bo+7btk3Bl9V9bixFiBfi2J8uEuwOyNx2cdqG4Gp7z2u1Fk3eHQZVYgg+ZD8JzyAbdQN3qMaW7Ou12IK9B3mtXmpRgL1OXZZUQlrruvGKZSwAJP3h9RiTamnqJ0eWVSUKyFo1IYBgN9h6Ejc3l4uL+tsb12PaK732m4lblVJBB/uupB+VlvY9MGzGudpHPuR/ovtg1xpnV2X9nfbdTZY8meSPBp7U2Wo0dHmcyXJppY2LGCoIG5Rco4+6b8GoH5ZYD/8AJAQ17/0g9PT/AGv5/wAcWw7xOQUmf9jOqRNN8FV5VSy51SVEaBXgrKZfGjkUj7rrKgHzU35AGKYflVc+qNVaD7vupqtAk2bZTmldIo6K8sOWuw/axxmz41CpR7lsW+h56YMGDFAwYMGDAAYMGDAAYMGDAAYMGDAAYfnYihk16VXqMiz5h8rZTVm/y6dcMPEh9gkZl7RGQC5OndRH9mTVhxK6ge3On1ZckoJoGd4RSROXszko0YuA1uOh4W/U9LAhTmeeZfl0M9VmFTHToCzNJPIIY7EAIg38KAGJN+psPXGvS9U50vk8ziPbNQwTEE8sPDXZc/1BcG3r+OIZ75NKX7Cs+KQlRFU0rLfzMR4iC7eijnkW6kD9U47GkxLUZI4pcW0ZM0vLi5EvprzRTSFqjV+TMr71ASuiIsLA283AHKj+t154x9j1npSdkEmr8oYRsQo+PjBYBCoBIa6qPl/WY+gx5AqGkfi1+lyfpbn2xOXZd3ZtU61qKP8AOdLmFAYMzp4syoZaKUVIy5wGNXEAPOo84NuAStid3HqMv8N4NNBzyZqXyOfj10snCR6KvqfSwjIptTZVsVih/wArQeKLDxD4e4AXY2HoeODjKk11pszMp1BlLVEhLK5r47SMNoZ7k88Kzccc+hx5na87v2uNBRRJU0NbVVj/ABVRVUlPRSM2X0aPtjmnIFkLgOyqbeUDkXOIs3EMG6k8Ajp1/iP4DD4f4ZwaiG7HmtfL/sHrpxdOJ7Iy6q01UNJtz2gnGxQpNUg2hyRuPPWwW3FwD62Jwpkz7IJoxFUZzRkOVuizpxuYszEhuTYFQORcW5AOPIbIWY0R3Ho569cdPcSBu629Tfrj534lrZeHazJpVG9rqz694N/AMPFtDi1nnVvV1XQ9XKTUGWRgNJmkK0rRxks8qF0W63Q29wn1tbjC2PPssnZWpK6ld+GVxUIA5uXKIfa4tcjoox5MA26Gw9r2GMt7G4DXuLEXPI9sYX41K16Dp/3YRvnP+n/Z64iqy2tJip6qCpKBSgRwSAqm3APzuSfnc43RTy0m6mrZLWLBJW/WPQFz7rY9LAm3re1J+4iZJte6hiDA+JlAupaxf7aO/wD8bc2xdk7JIY41SOXxY16+UWsxFvYX5J9vf06WmzvUYlkao+feP+D/AHJrHpN26kndV1N8JDsrKpBJO7cLGxJ4/u/dI+u63phO4aBhPFOSjg+IsPBAJBOwWuABa/v+2+MDPRxb5m8SKQGZOD5OACebllW4+tzxhRTnxAgRPJcqdzXZvMQAeeBYEDm979AcEzko3fYvA0lgUkXzObgG4uV9Wawte4/xONEsU6VUk6RxqF2NKpNwbXJsi/dIuvr7Dqcaqi1MN8Mv+dYGUQnaetyUsLhbAW9TYn0wtE1OYiVtIt1A9FYDzEWJuV5A5+Q9DjPdFhmJYfDdY9zI/lTmwcG632r0BNwPW3tY301Es8U8pjDjcRu3EbLgDkIDcBRb3BvzyMYMsscrS+B9mCDIoZhte3UoAABc882J9yGwpDxpGBGshWSxCbtviD9W+3g35NrXsecJLklHxokXc8jQFY/OeAp5HJJO7k3NtwAxqhlgjkanqXpyWN0d5F49NlwPMwBAtf39r4wineEmku/hSMSm26ggfesFuST+rctxf2NoM11Ry5h2i1tBSoFmqqiKGNSTe7KgFyefa+M+XJ5aVHT8N0C183CTpJWTrUz0aQJLHUhDSHykhQAOhHIN2I9D8sbnnoquF4fs5UX7rbkdhf5Hjf8AQYr+NB1irIsmocihaMlJEet2sCByDx1B9Ofr6Y+yaDqVdh/SfT4Cfd21xAY3PAOw88fhivzZdkdL7k0z/wB79CeKSqWWPbWCLxogY2uYzdhcEnjgDgjgXB98IzUFJXyqnlO9nEvigKCI2tY8MQXJuB0AsCemIRqNC1lK4Wp1BkQazcNWWI4uOCpI4tY+txjVTaRqKhEdNQZKN1zZqvZYgXNxbjr/ABxHmzbqiPubSpX536FiMrjyqlWVI4/BmSNnlWVw5lX++Ty3rfnr0uMLHg+JnSmidl8SMvI6khg/9TcPQ8Dr9PS1YtR6dr9MVMFLV1FNP48Inieml3oyelmsP4YsVoSSGq07R1LjiSmhjfkXUqihX+V+l/l8uLsWTe9rRj8R8MjoscMsZ7rM5KGlgq/haaORax7zwPEALkXB3qfKxB4uebeo64Su+btSmKNVqUclS4fwnUhudysQtxbm24et8Lp5mkds5v8AaK9kIBHTyyAD2ttb6qffG6olp6WtnP8AxLhZYz0KhR9ra/qVt684scFJHIicxFzd5WWqlkgWFBJOtI9nUH0Z2tYHqAoa4v5sK6LKqNKUV2Xq9OWXxYvDYhvCHQEnli3sTwfpjZHvGXJG77ZKudg5UniFuT1/VVSP2YVZQ4gknoHBtE/jKg6lT9xLfTzH5sPniVFIsV9jOkmEkcEytHF8QjCYLwkRBJuB0HRhfqevItjkVIJmFZl8TU8r/ekJKRyyK20rJtt+sVAb7wJHzA3xbjNV5SjjZTzLUzSAgcccA+wAsfbCzOfhKWgmhYsiqsiRovBIsG49uRx7EYlxvglRt0c7LaoVlVPUJC8XgKIihBEqsT5w9t1+QBcEEEfd99jzeNXRoXtFEoqZFZZHI9AGF7EXuQw6EDA80TV/x0Uc0KTrd1CEbJFUXYXIuCm026eQ/XGikpx4TVnlVpXMxLLubaR5SBdr8AXvfm4+WKunBDVG6vqQkISJRPNMBGrFkc7rc3BuxFlBuOeBjTEDFDFHvbdCPIkQkLMf6w3EG/y9z+OPkVM1dUSVe6R4QSkJLFo9w+8y229DYDynoffFVc37dtcwa1zPTeWZlRmOOteBVqKVVUqsxSxPpxfk2ONeDBLOm49jJmzxw1u7lq6Lw66eScHcjMyx8/e92cFiwvawIPz9cbZ5pKaNmCxNKrBQtkUyAehG7r7NY+55xrpllho1iaEAJEGO2RtqHaLjgW2cW6C3uOMfEklkqGkkg3bwogSUq1h6BgbMwPJBazccX6YXldS6+ODbTLJTpIkpDzygF97EyH1KhSt7ji17G9uuM5Z2CgU0Sb5I7hVhZuOu/wAtw6G5uD68dbDA7mMRKImDkmPYSVfcTwVXaVLC3Knpc9AcJaenWCQSLUwq8gJv4YIRmI2gFbMu0qR9T7EjDJ0K+ophX4dxItV5tq+diGG7kq24WK9SOvQW5B4wqK8RbJFVHkZDsjVgCDwdwcixFwVIIHqT1F+NrPX2ndB6fn1PqbM6akoaRWZ5J3C2Frgq39bcFULbng83xSDtL739dr7PjlmiXzChyNw0bmywzVlyxJlZblAL2CKT0BPXGjHieR9BXKK4Z3+8H3v9Y6O1vVaT7OVyqGmoT8JXZhUoJnkmUkMix38oUgrcAm4vdb2MGZr35u1uerIOspRGPK7xxoruCLEDi1uPW7cXwzdU5jVV2bVdTTZesMeWqaghEVndjdr26ADk3IHTjm2Ij+NylKySWtp5JnkO55XPiFjfkncQDybfh+GOjOEIQ4jbMCyzcuJUkWRoO+b2iV00dO+oZDBu5WqnMrG7XY8bSvJJ+7ycTlobvS6knZIP6V5POYwC/jzgOhsPQcDgAdD0GKe6Lq9MyVBWTTJmiYLuanpjNKnlNwNg2i/rf584lXLNWdlVWVoI8xOXzhSFpqyOSG56XZUMinnizKoOMawxb9UWjdDPJ8RaZc6j7xOWNSO+f5Sm5UZTPR1QcG9gDyqngLY7b3BtzxjuZN256BzaZvzhm7Rq8qFlcEeRmIJuQLdf1lHr74p5BTZjQwmbIcyirMvYA3RwqdOVtFaNB/pRk/PDQ1VqA5QoaZJKCVgdk0IsjWPvtCsD/WFsXfY5zX8ifPsOpwi6ywaPTKjr6WqgE9BWxvCwV1lSTyOtirMrWsvFz06MTyBfC+ZUK+LLG0Z8rEKLKSLhv9FluLBva3QY8yezPvPap7O8wvBUJX5XKU+KoJOYqhQ17rz5Wv7db88Yv12V9p+ku1jTn9I8hzeGRth8SCZbSUjBeA6nnpuXdf8A1jFS3RVTVMrnGK5h0HfTGapSOj3FEcBbta5ZdqXA55523NuCp+v0xBSjkbd0gLxTC4X9Vo29iCw8w4IJPUDCieJPiHjeEQMY3UuxuyKxvZr9VB3i7DgD5C+uMiohiWoYbHbeSp8QhHBIJ/rKGuG/0R7XxbfsVOzEyqfGlq3KmJVL72sPI3G482P3RuHUNe4ItjAq1JKElEgaJXUtv67NpG+3Nyp4YX4ZSLAEYxmlqY1lkgblyVRHUsyAodykgHeDbqbHhT1BtjPQLV7SytGXZo4UIIIAt5ASSNxW1l4v0OGVIldBk9s9WJuxzXQgUOgyDNSl4jbYIZGALC6kqd1jwLFgL84od+UzUx9kXdpj/qadrh0//dsrxe3trNPSdjfaDPNGCp01mUYKk8B6Rytrm68g2UnpcH7vNGPyn9x2X93FP6mQ5gv/APgyzGfVfhjXxGg23yef+DBgxkLAwYMGAAwYMGAAwYMGAAwYMGAAxJvdxj8XtQ8O176b1N/zHXYjLEqd2RDJ2spGt7tpvU4Fv+4VdiV1A9m+z+eBdC6YdFMtsrodzyt5U/ydXLMf9XsL+oOI873sTHsC1MjIXMQo3Z25ZQ1QrDd7Eg/sHTknEg9mlJ8R2f6YqnILtk1AI1JLHxDTJz167SrXHoAvpiPO9uKh+wHUhkC7C9M5sALuZUs1weSR6fQ36X7vhzX2rE/iv+THqlcGq6nm1QvTRVkM1VBLNArq0kcUnhuy8XAYg7T87G2PUnuqZzpqh0mmmog2W5isEM0WXVudx5jVwUxUhEdht2HeJW8LaCA9rAk28rmLKORztB5HHph7aE7WtS6DpoMroYIJcr/O9LmuY00sW4V5htsilJveMAtx7ub9Bb3/AI1oV4ng2RlT+Zy9K54JcxZf7vh5jprOdN1OTCKozd6GikmzDLsszuKjnZA6mNmDK3jBCJGaPaSLhja4OPNCV42lZ4VdEJBjRjdlT0v0v9bD6Yc+re0jU+tcuosrz94pKfK6ipmotsdmgWdw7RAiw2BuQPQkgWHGGn5gxI55v0/jh/BtH93YPLyTT+v/AGLqU80tzj8uHY48h/3I1wfvnHTAubDHJyByKQk9d56+uOspsPTnHxL+Jl/iuZ13P1T/AAh/ommtf0ozF2A9vnjJLXvb9gvjC/0/Zj6COADe3pjg7bZ6biizfcXphVdoueQbD/vMxuv3riaMKB6ckjr6Xxaztf1/WdlHZ1nmvGyRs3GQ07T/AA8U4iEyhhuG8gldqqTYDnYRfm+Kndxaqhp+0bO0efwTLkhRRcIWYzRi1/le/wCHvh19tXb7rTXOg+17RmRdjGYtTaZp8xyjNs4mzqnihhi8PekioR4jbo7OEC9Cw3G5I9N4a600UfBP4/X+My+SLV6P1Eur9K5Hqmjoqiiizqigr46eqQJKiSAOqyAeVTwdwBN7ewuVaCSCnhenUzRoWYxeGLA7OXC/IEce/TFQF7VO8p2Vah7IsjzbKNIZ1kusBHlsGSZO05qlRIYj48k0oAGxZCxAAXaHHAO4Wx1NSVuoNM1uV5LqWryKoqF2LmNMsLzU/mXzESqyH9a4IPHqMaJc8njooY2p+8HpPLOzWt7S9Ng51RZfncGTVNMztTS/EPVx07o3iqCGQFiRaxt95Qb4f1DnmU5rRpm2n84pq+hqGMjTUMqPE20lmKyISthYgi/VSfrQjP8ATmi8k1xm3Z1Vae1j2mwUeo5tX6hlzsvQU6Ua0vhT1EvieHE4E+6XckZDiNVuQMXb7PtL9m+X6ITI9B5HldBpuuVplpaNfsHWoAsf728cXHFuehxRKKrkbqhH2odu/Zd2PaW/pTq/UVNHBKviUlLCRJPXMOQsMIO5xe/JsADcsByOl2Y9oWku03SOX660XW1FTkOaK7QytE6PE19rLJGBxZgy8G3pcjFC9JJp/SvZCNKaD7EKmLU2qp67R1VrTOYfCoRUTzVMMaxzMHkbyDZtjVUuLE34F/ez/T9DpXs8yDSEFBSLHl2XU9GIaZG+GkMcYRiinkru3Eb7MepuTiuSRK45HJUqkoFLZ0ZgXKWFwxHlY7QWuov7c298Vs7S+0XQuiO0OTMtY6myzLKamqqd5jNUDcUCoWbZyxNrGwv/AIXT9t+lez6btyyvNO0vMc3XT8Wi64y09DVVwaZoa6BUDpR2aQH4lxbm/UjgYrLo/KKGp7RM20bozsxoHydNR+DNV6gyxaSpmyqoCmOFVlCzCRInZwWBJRQeScU5YJ0dfwfL5WSdd0y7FVq7Kql6rVNDqOLMcnq5leGajzABEhZVIa6psC8/eDAEWsMczPNX6FyfJ/zlnHaNlkUk1S3gB8xiKRxtE3BefYCGAexsbnaBa+I17vGiM1zHunad00sNWuUV9HmRqo4F3GQMrEBui23gixsbc4gP8x5Zofsx0PrXQ3YdmlXnmUJk2ZV2ejKFfLqhWiKVEctRKGXzeKAbA7WANwBwmxNs6DyqEIpdS2fZ/wB4nsd1pqGp7NNKakgz7UFfMJaVEXw454UVALTsdrbQpuqk2AJ6Yl2grqrIqRnz/I6SSgmmrKl2o3VjGpZi4ZGCkhb+l/fFSeybK9Q5j20dnWc9oEGXCXLsy1QlHJSRh/hZEo4EallZFXc0MhmS9h90G1zh7d6/TuiKztJ7LM21dorNdU0SVmb5bUZPlKSTTVpNGskKbEZdy+LGWJJC2DbuAcPjxqzlanU7JUlxZws27wnZVqvXWX9m2mdSCtzGnoWVTDGxgDqXLRLJ91nCgk249ASQcW3yKlqDpTK6l0jpIVoIRIVG3euwCzEtbn6YoZoCmqtVdok+um0FlWlcqyDLDpOhylfLWUDwzvJIJI1QLGWEoJ5Jsy2NicXwyGseuyDJzEpdKWigCKjBXMmwDeAeoDcD2PPtiMcUsjRs1+SU9Bicvd18hjdr/aTqTRo0tlulchbOc31DnMGX0uSxyFJpabYxqJmk/VCRcs3QC1yCeJHgy/LAiBYqYx7P86Y0cqOgRrgklvf/AOBaqnajk2otXd4vONOZrT9pGb5I2SZXFl1PpuWlpI6OjrJJFqjU1Eu2SONmphfawdwtidoAw6+6FqjTujtMaN7B7ZhPmlbkddqgLJIZl+GOYSRxgk8qdhUjizW4IIsdO1JHBT44Jq1fHmmT6fzGs0fpuLM86p6ZpYKBKwUXigHzwl9jLv2ji6EEkdBcjndneuaPtT01kmr8mqJIos0jYwzMuyRJgxSWnlW/lkRldCLnlTY8Wwye3WvzTUXav2VdkeiK85bUnO11fmckQKvS5bRxujblH6k7SeEAeCQ2GPmuuKDuw9uuYaM1rVpR6D7Sp5M5yirJ2QZbm1wKqCQ87ElIhlD3ARnvwCWESh3Q0Zroyyeocxj0hk1TqHMUjpqLLKOaermWN5LwIpdyVXc7EBSRtuTY+XHIpdc6Pr8rybOF1bltT/SiLflQE6r8WrU7MDCjWZrBb2tcA+nU8LXfbf8AmCpp9AUHZrqbXeeZxRmSCLK6VDQzwE7PEmrHYQxL6Nckgm4FiMVg1R2SZLnmodDdnXahpbSendOabyCszQZlT53JNLltHvkhhpZKt1SMjxqpJAwABaHbyFuZSdExm9xbDTWs6DtI0y+o8vpailpmneOJZ4VDslPO0DSp5iGjdbuj9CrKfo5a2eeBY44dqPLIIQ8hddpI5byjgj62PscQN3ZoaY02qcyznWcObaiWph01WpT7I6CkpqWJo6GSmiQsPCkhkExkvYhxa1sTfDUzCtlqqyhrInMfhBBSyuw6lyQBtILG1wSLW6dcVTrcPNPqLWQ5dRs92lWGO5MhIFgLAMAp5uLbvQ7TcY87tRU01d2r5yTKt2zSZ2nYhzGvjE8sGLMORbrz6Y9AY6hc1ntuMgo3Je8ciuzXAFt3VR1s3rbrbFGNaZHVUfbDmUab3pY8yldphGWVizdeRYi7Wt746nhsklP5HI8Qg24fMvTQsKymp6YqsyUojWSXg+a1kXduHkPUjjqFFzjoyPHFA8bRzLGhPkdySq/rKobyki/C+w69caKOCkp6QUhYlYEISJ2UMp6sAzKLE8k/P2tjUjirZKwn7BjaINu5tddxXdcHzAi3A6gi4GMLas6S6IKanVJTVFaeMylhsiivsFwCbqQQQwU8X5K24FsZZhmcVHRvU1MkZ2byxaSwUAHdIX6LawIJ4AKjm18faqtXwGaULUNcsUB+/ZbMxYWtYEEEjobdelN/yhvbzVdnWkIuyfT+YAZ/q6J2zaoi4aGguytGCOLTMDzb7qt6nFsFuYk5UivHeu7ydT21a2k05pyrDaXyqodKXYdgr5dxvUOOLpckRjiykngsQIy0zn9FkEry1DJmM6bRYqXjVQfS/lPPAHS5tz6xnl9XKpZYATI/BfqTiQ9EdlmqtWReDldKWMhVmZpCqm4Py5xvhnjp6lLoZFiyZ3tx8slJs6Opciijmhp6SaocFQIykZhQBmJVfLsUX5C+oxFo09SVOeiLMYYI0kZvCLS+EXs1mVGbch9fKQelrXJtNunewHtIyfL28GnoWmkjKv5mVitrAcjkdOOnGGhrzsv1xDklTTvlSx1dKFM9PIQ5qVUWWRTtBLAcX4a36x6Y0/bdNqZeliz8O1WCNtcHDzTs8zWmpZUyNqDM44kH2GY0gjmhHXb4h3qv+isit8lvbEb1mp9VZdXfAVoqKMwGy0lQ5mSP12qkxYgG/ofnjfkmuNeaeqEy+GplZaa4igqL7oAW8wie+5CSLkKQD+sDjpfnuk1vKKbOKJEqbtvUpaQc3JsLBh6krY+6t1xDbmuBIxj0Z09B9ptZkVWhQSURU7WNKp8M3PO6In+I/wDhiaK3UOldbZQwzKNGaRQvxNMeVe3HlsGH0sD8jivtTobM6FUqcuYVdOeY1vYnr91v9XB+WFNPndXSofDMsFRGux1+64X/AP6H1w8YWueGXY9RPC9suY+zEutMpzLS+YvLBVU81PvsksZChxzxaw5sPS4/jh39g/b1nvZPquizrKp2amEqiqpi1hJESNyn9gt7HDOzHUP503xVscTrN9/g7WP9YezYaNRHJllR5F3R9VPq49sZZxbfJOScG92J/Q9xtEa4yLXWmMu1NpufxqavhidFut1ZSb2J9QAysD9BxhaKOURR79scA2KgWLeAjITci5F/Tgfq+/WiX5O7tuNJmtV2WZxWoUqo2q8rJ9J0F2Vb24ZBe1+iEe2L9jwpYYfh5GQW+yVdxI22cKl7EgbL7evJ6beUjKuAXPLMo5B4BTw28MBblUF4luVYAjrYAlW6cWta4wmZyoRzJJKhVRLcgkMlwSSfUBzY89LG/p9M8CF9xEUVvK6W2HeFKlSeLXvxbkMbWsbpp6r7ZJjCkTRqJOpULvVgwF1sRc/dsfvkepAdIbhDM7eQ0fY12gkqEP8ARrNEeNuisKZ2Fz1F+SD6hbHqMUW/KjADs27uoHpkmZDrf/iMsxdnt9WZuw3XctRK8Ij0zXgWINj8LIArFhu9Lc2+vvSv8qgoXs/7vSgnjJ8zFvQfY5Z0xVqVUY/X9hoO7PPfBgwYyDhgwYMABgwYMABgwYMABgwYMABiXO6qgk7ZqaMi+7Tuphb/AMBV2IjxM3dATxO3nLE/rZHqMev/AOxK72xK6kPoewHZO5fsp0WyeZ5Mhol3pxuBp4wQvt95R9R88M/vUjx+w/U1rSBvhnLL6gTwgdeiXJsPp6A47fYrUSzdkGh0qPPHJprLCttoFkiTy8G4+8fSxJv0GOR3n54Zuw/Upk++IoeFtzL8TFuJ59AQBYn1v7406i44G17HQ8FSfiWFSVrcv+TzuEEYN/CQW+Xyx8FPCouYU5/ujG0izcG+AWX3x5R6rOuk3+bP0dDR6ev8tfkjWtNCTbwI+P7oxl4MG47YUH/Jxlbjdzc4FNyAD/DErV51/W/zY60Wm/8AHH8kAjSM7UUKPkMZDrdR6YBZrg9b4y2ggf4fLGecnOTlJ22aIQjjW2KpAHuLgEfjjMPYcj/54wUBbDGwbb2625/HCW0P2ZY7uQRq/aJngaFZAmRSvdudp8WLm3qeSAPci/GHN245f2rZHU94GHTGkcuqdNZ3k9LmNfW11fLTyIBRPDJJCixkSPdGO0sOQt73vht9xyRI+0/NN0TSK2TvuKmxT7eG5A9/n1xaDvByRT9g2u6c1ETOuna0Ns5CnwZCBbrxYC54ucem8O50qPg/8f8A+sv/ANKI97LezDtgzbtB0t2n9qepNI1eXabyOaky7LcqpJ42pvHihBmLSnltihfx4FsSf2yVPaGMjOW9nei6LP6rNPEpqyetzoUC0iOdviMfDZnUXIGyzDqLX4culqSB9K5Qjwxx7aSBDsTlfslawH6xNx1uOMcntH0ZknaJpPMNLayyMZzQThZVi+Kkp1eZQXXdKnmUklgXFxZr29Dpm7PFroUSm1ZX1eSCHtP7X8pyjPX7OpNPpV0sqPLJF+cvh2hqPEMjTu4R3dlCkrdgBa5tB3UqPszqclzuq7Iu2HVesaOkkgy8jPKnemXbA23wo3iRlRoyABbaQtgQQRiKMl7Htez9sNPkWQaZ7POyjfo2adRk2XjNpo4fiUW5eVY0+IJZQHO6wP6xNxajLoqjs40S9ZqfNPzomU5Z42ZZolItNK4gDFpGjTjcxseL7bfLFEug8V2Kk1h7aNSd3oQacyrTmT6b0vV1Wpoc6eseqzF5IcxkqB8NTbFRZF2Nw7MCFPF8Wp7L9GZdoXTkU2n9Tag1JHmzfnCrzHOcyeqmqjJGH8QE2C3HIjjCjqD5jfEG9zij7Q+03Q+Xak1nKuTaKpFrqfI8tiUo+ZtNM/i1VQb+ZQGMaJfbe7EE2OOfW9qM/d90dm/YFrHOK2mzOkqKZNF5pc/7Y5TUVcUaRxyqLCenRnRhe+2MWPXFMk2qRYlTHh3kq3M6DXuT5pprMM0pquk0zXTNJlaRPWPTDNcsWo8FXDASmESBeAdyqeDitukNM6wo+8PofK9e5pmclVUOmoq45zOktYlRTtLHCjsGIYiKaG4vyE8wFrCe+8VPk0/bfpjLa7tQqezygOl8ynrM2o8xiopaiNqukMkSySG3iOkZ+6b3A5PTFbNbTdidPq6TNNJ9puW0mb0ubUUuX1VEtVNS0lAjAtDJMqEtvVnklkb70hJIAGK5S4o3+Gx9cn7IsZ2B5RTT9jWn6vMK2qSKnkPxMfiMB8G87gsgHNtwYm1xx1xX/R+i6zUVHkmV62znPqLTEmR/EU1ZLnpp6KKObL5kjhjgMg8VhUqkpAuQBY/OeO7zm9BWd3vJ5zV5ZEtOtYYzLms8Tyj4iUhlQXAUg8W/gb4grQ0HYtP2Z6efONeSzZtW5ZBBX0qzZjmM1FThbT0lNHFCyU5nIG/zFtpZRYHhYcNmjUSeXanxx8B7dh7ZPNH2RZ/K1JU5xqXUec5lmi0lUzK3xlJJIqk7hYjbsJ6kxEXNjiTu2jUGfZDq3s4TRGTZdW5lTZjnb5fSVWYCCMxCnmjPiSuDb/OE8i1+OLYgXQebaDy7vFaci0RT5jQ0uY5w+ZrlGb5fLl5pk/NkqSSozgjwXlkkYDixvcAEXlTt91drjL+1Hs10r2dZblOd6wzOHN60JIjVNLSU9QfC+IYX2kLtkaxUXZbm+6xbncqKtrkvV0T4+dDR/oRrKq7ac7PaZqOmoq2oFHquPJ9LVzplszTIImdpD9pMQ1OAwvs3G/61sXTn1Vluhezoazzx6hcuybLI6ionpaV5ZYlCxgt4aAsyKSrE2PlXd6Yqj205DnnZ1lGgdd0bV+b1+k8raDPA/M9dRSyE1DWHqsgEiL0Ci3A4xMfYH2w5D2iQ6y1Plur0j0tkKZZTmsllTwokWiSR2+0OxArMwYEEgpyTwMRjVzZfr3FaLHB3dsjDW3avkWtda6j7bsl7LtY6i0tkWm8uSnr6yvnyDKGmpZ6qXfKruhqjeWLw02OS112jgtI/cs7OMxg7L6ftgzikWv1Hq2iR6cCreM0uXxlvhaKPaoUDq7E2LO5J6caaSFe3bVNJr/W+Z1uS9lem6xanI0zSZaY6gzMMPCrZo5QAIV6wxkXcndYAAF/d13PKyn7BtKVHkrqSWKq8FkAVx/lU23p5WQi1yCCPY8409uTgxvuQ32sJ3h+wl847W8qodJ5nqDVslHl9RWVEtRVTUpapWKjyyghsPuCTeZJGG997FRZRjXkPZGO1btY7Tuyftq1bmmvdKZTBlGbwNmiiCpo80qEJPw7w7fBURo6eGvB4uBYkvLW9Tqztc1PPrepyerrdCdm3i5rlWXxRCBs/zqBG88bMGLQxDcsblbPK11G0Xxzu6NqOr7VK/tO7bcvlr6LLNXZ3DR5elYUM0tLR0qRqZFN7Es7crJ1B5Nr4JS44GjHnkdGrdEaZ0H2Q1ehdFadjloKGn8uUVWqKnL4CrWaWF6zxDIF8u4oDY2K2AY4ijWGk9NZTSaTpf6P9k9dlmf5D8fTZjmFfLLkWULQPDanpV27qhP8AKJJLu6szSTXtwBJXeU0PnOv6DTtTRafynMq3K9TUlRCtfTNNQlGV4ZDNGLnYPFDnqLoOcQznVfoLsz0bprs7y/O9K6yrtD5bmtJmWWU+RCqlWvlp5WSZQkckkLpVKFbzA7ZWYgfdERnfCLpR7j07GOzap1/QaF7f3z+t0rqnO8sgpswo9NvTx5bX0KSP4ZkhdGFzEABYgpwBt2cWbhYRySx08TySSTy+Gg/WIASx9vMep62/rXtBnd67S+zmqyeHQugq/UGZTaKyeCnn/OeVz0njhI1j8aJpgCVBBurWKh72ZQRicaKRKKhIgqUZ0VTPWMLxx2W9k5uSCxNrfqX5uMK37mmt0VRtr4g6VVRNVpLNRor3VQzBNxDIOLkFRuAN+bHFNtaDMR2stBTxVwNVnJlR4XCpsUxMbXF1U7yDbgbb++LeiN43alZVQkrHKGF5fFkK8Ej+rGDx9b9DiuOvdCVcmrpq2qyOpmHxUto/ADAp4i28y/dBX3sPfG3RTjHdfscrxLHJyioK6LJxSDMIbndJRoqqd9yHKkEh1JBstz8jZuf1iqqKuWBXiA+Im37grFj4gX7xYjzDyWO4gnhhe/GNcGY00MENLSyQVDoieHHHJvNk5sbG6qBxc3I29bnGKQLRM88tQoeTYJJmYbNtytw1vL5SDY3uAb3sCcjVM0R6UEojoFnqqioiabYTNK23zJdg9jxdRyxBHTk89fFHvI9ptb2u9septazy+JTT1r09CoPEdJEdkKgexUBvqSTj1P72PaLUaP7AdXakgmNNI9J8HTkAo8k03hoPKegAYkH1Ceg4x41wq89akZKl2YC979ca8a2xv3M+RtypE2933siXWeZwVeZIXpo3BKLezc9CffHofo3s+ybI8vgp6fL4IUQcIi9LdMRL3X9FQ5VpDL5pKdd0sayE2HN/XjFk6eJQLLbj8PTGC/tGRyl0O7jj9kxKMOrNFPlVIlh4C2HrtwnznRmQ57TGGsoI3b0cCzKfcEY7cSeUcjd8+mN63vdQPqBjVDFGPRFDyNvllVe07ug5PqNampy6KOKcgvHJGACr2/63/wCnnFcdRd3zP8knbKsyppvioJA8Uo8r9Tax6N6WPp649N9ovtCAHrf54b+pNF5RqOAR1dMgkUHY4XkHG/T6jynTVmTUaaGdqfRo8yhUVuk5Pzdn8Rs1/DqI1sk1uPOD91geCevr9eTqhMurj4sdQoLLdHUc82te/pz1+WLv9oXd5y/OqWaGamja/wB1wlww+fsb+vXFRu1DsH1VoySaso4ZKijRidu3zJ7/AFGNn2mM1SMGXSvsRE8U0TvFIAVH3x6/UfLBV0qyUxilsWAJS/UH0t+y+FCVRa8Uq2kW4F/p0xrWSOSFoWvviJKk+q8c/hitxRg2tM36F1Dm2h9ZZTqTJ6loKrL6uKphkFztcMCOPW9rfjj2Y0Rq1NY6VyTUNCfEhzGlpqrcZFFpGQqebjdtJAvYE8A3sTjxflplliBjdQyKHW5/q8n+F/2Y9He4T2kHVHZnW6ZrJY3qMhmVgrsDeJvNa172Vlf6eIORjNOOxpmjE+aZaEZfGZk8ZgSqpIFIHnBLC+292Ck835HFrYxg2U8UcQdI3XzpIj7lVlYLc9Ny24b+rtwqQtKs1OFEm8sRvADhrX2Nfob7739eevJQzSCyl5dyyMsj72tw6EGxIsGBBFjxdR6E4dc8mnbQwO8YFh7EO0FNvh/9jOYIDsDhQsDnaeORyF+RUDiwvSz8qsrLofu/qb+XKc0Xk+0OWYuV3i695uwTtAamFi+na02VSQP8ncOw5O1SoPltYG3PrinH5V0FdG9ga+2W5t//AKstxRqekfr+w8Vy2eeODBgxlGDBgwYADBgwYADBgwYADBgwYADE3dy9Q/eIyNCu4NlGoRb3/wBpK3EI4nXuQp4neU04n9bLM/H/ANy1uBES6M9XO7/FLH2F6GnpN0d9NUBEm4eV2gTzj5gKbfVsJ+8FkGZ6k7LNQZRp/LKmtrauFEjgjDMzFZ4bKijqQFv8rn0GN/d/kg/2BtAK1UvhjTdAGuv6xiQFmt0CqenzOH9E5ZzUA+EYCyiEvdh9oAAbfrMQ34Ae+NrismNwl3LdPqJaPVrPFfhkn+R5tHsH7XgPFHZ1nmxdxJ+FbgDgke/J/bjJuwrthRWLdnGerZS1zRt0tf8AwIOPSWq+IvPEyqFaJlS1goN2BP8Aojaxv6sPphXMVKHdHubzJs/XUs4D8e9rD6XHpjnfdGJqmz3sf7SdbX+XH9TzKHYl2rlwD2f59e9rfBvfra1rfI/xxtTsI7YGk2Ds31BuJUf7hk6kEi3HSwJv9Mel3wcksjWVZJlZtthaNWLiw4PKra5PUjaPQ41SxkSRxU80xSJW5C3F0JFj683Pl62K82xH3RhfFj/3la1f7Uf1PNduwrtgNmPZvqEqRcWoHuTbp0+R/wAPTHxuw/tdUEf7G+ohsA5/N0p6mwvYe+PS5pKinZ0lpxKscgttPLFT0PABUbibKTz16nBS1tP8SIKiUREmN5FA8yldxbd6X4Nh05vyLDC/c2PsyV/aXrO+KP6nmkOw3teLMF7NdScAE2y6UgXJA5284xbsT7Wk+/2dahXmwvl8hsR7nbz/ANffHp00pbwY1bw42KhYgfMGKkgevNtnXgX98fYQqwN8SiLHYeXbYlDYtt624IG4+g9gLp9zw92T/eXq/wDxR/Up93POz/XWlO0bMM31DpLNsrp2yedIp6uleJPEEkTWBYWPAPH+OLZao0jlGsNOZxpXOVnWlzKhloJzTkKyo8ZRwpvY8MOTcXF+eMK2pnkkKQziAugB3NuBBJLHaf1bAEX9gLG+NkdTKkSpLDaKRSWZV5APAJA5AC9BYcqL43YsK08NiPGeNeLZPG9V9qyRp1XBrET5fRiiVRKPB2B1bk7Y1TpYXABt5QASPbC5Jo5o53EgkDxG9iVKiwFja9hwBbr0+mNNNNHIWmYjaGJYBgWN3IA4vyRcWB/gcfZVkDSNuKShr+LusRd1tYAX4KkAW+fOEkc2I3X0UG7Rl13S1cMDDKJMqSGxAAaoDswN/JxYcc3UdbjHfnq4swjWizOnYCpVo2gaMbHie42Jx/VW3O3r09cbNs9MBuUuEYbpFUlkUlh5lPU2IFr3+XHOO6Gvifxj4sZdT6ix2Em/IuQb9Olr2HXFEmWJCXLchp8jy6mpdOUFJl8FIiww0kAEcBiCqQiKBZT5QLj0v1AJDc1p2b9nPa3l1LkXaFp2kzIZe3iwLIGilppl/XjkUhxcC5YGx5uOb479RVLQSSQQSpW7uVpxJumACheCt+CebbeOu7G+Wojq2ZRRV5ZrbSqrCyrc3A3sCBewt7X98UuTTsbgauY9mWm8/wBb0+sc0pRX1mR0EuX0RqU8SNFldXsUbhivgCx6ncxN7Yj/ALSezzM9S5znOXwaeqZKatpvhZQIeqGMI1rWuOT068Yl+jfMaYsat2klnCPaCBWtFZubdDdmJ8t+CBe/BWU9YHkY5bW0ksijYUX7Jj/daNuLewsOfnwUa3dS/Sa96LI5xinfHJXbs90Zr7s70FkmhqKGuany6iNEJfhEjaVP1w1lubljwSbk/PHR0/Tav0NlOXaYyakqKChy6GOkpoPhlsiRiwQll5tze5JN7++J5iqoqqWaJaCVYVBMyFURImsA3mY2O4ECwsfW3OOfneTU1bSK8rXQqdivJdgL3FiRbb6WNxzxiqWJ1aZul49SX8mLIIzjR2tc41xp/tJrKOtFdpylrKSmPwKWaOoCqwI2/qlWt82Jw43oO0mTOY9RyZNP+dYqNqFKtqCPxkpXcOY9224Xcqm3uBiX9J5RMtBTzZlMamUDdHGZPMiW4uPU297/AOOO/G0nxVQRJt3bbb0PB225Pp/hh44W1bZZDxlNW8MfyK+5tp3tB1HKlRmeT1tS8YstqYA2JJIAUD3OH/2X9inZbobJ8wpsk0PR0b54KauzikqYnkp5qlFsJFhe8aEMW+6o5xItUj1atBFmDxG9wY0NwLDgi9x69ORa9xj4ojirAkjWdYPK873Nwx9wbH/HFkIeW+GZtb4i9bCOPaopexx9faJyXtI0Rm+hc9pVfL86o5KSbYL+CCDZ1Pup8w9ioPphN2a6Gynsz7P8g7P8rm8WmyOghoUmK3eUotmdgAACxBP42w698sg3E7mY3Y3Krx/jyMc+srfDU7a2IWHIuSOnyt7D9mLZScUcyueAqq2nj8QSVsoL9R4Yu3+lxb9mG5kGVZRpjJ6bTuksgpKDLaZj4FNlsAWOEsxJKqtrXYkn5kk3x28ogbMWepqqjxILcbFIV7enPX8LY6Kw0WWs5jjEO4BvDKfeF/YcftxW90u5LpHOrcod8vkSRZag7DcTAt5bc2t6/M45FNJ4VElFNlghqWDieeZ/Cd18wSwC2JswIY26tb3w5nqYowhaqMB6orbnB+XsBf0wnzGspqejaOoaON5rADcpDm9+QPTrgaUObHWSlQkizUPVpHUyeBJLIwVJv82wKmyjYSpuWbksWHPAwgzTOKTJ6SNJ5o1FHTxgkkksy/dFvQXXcSBc8Dqwx2KdI6+mFPUzQTK1hYKSevUNbgjrf0IB9MMzNtJ1c2oUpK+pkq6Pwy9MQwUtYi6tuILW8t/1iNvPGGk20btJ5eSXrdJG7LMzlzOmiqKVJwlmZ/ECxhWY8Auetxcm3muzdQd2O1DR2kMGXRI8tQlw6AxRotrNJtB3OtrWLsLk3wZfTQ0dCphjkn+HXasaGMWIPO0b7oSfKeCTbk4zpq2aVHWjp2MpI+LqJl8IRsOqqpuRtvYnbxceptiYR28kvbKTeNcCuaBamaOKaoknWmRhJMEUMr242sq8NwWazEcc40pFVVM4fMqkSmCpZCiR8Myked1Y+YcK20X2g2+iWSrU76KiqYp5IxYxRRtOEN7kyNchf4k89bbRnPUVE7SGnNTJQSuZlanQ75LNzx95NpB6Br+pGG6mbNBr1Mpz+U11r4XZnpjRsFUrLXZmamfbyrLDEFQAnrzI5/5Iv6Y87NJUL5lqSho0XmWZVH1Jti1P5SXWEGcdq+RaToWdKfT+TR+NEybNlRMxZhbi52LFzbpbEHd23IVz/tVymnkiDJE/jsD/AHTcf4Y2TlthfwMGNXlr4npZ2S5M+SaXoIZQoZKeNLWt6YkKN47gbj+GOdkuX+LQQIg2Iqc8WwPWZdR1HgyZhGjE2sWxjipQhuSO2/5jocSSIRYjg/LCqKINxY2xx6eqRgsaMHLdCDfj3x3KQIIyCxvb1OLsWVvhlGTHtRrkiCt5TcAYyjiJNiRbr0xu3IfXkYxllTqOB9cXsz89jRUU6SqEdFsQRbDY1Fo3J81pZIa2mjcSC3I6YcFTmMVPud2G0DrfDS1D2jacyxxHWZjGh9PMMI8tcIuhhnLtZU/t27p4dZ890VCvjgl5KcfrD3Htio2YZVXZfmElDWUrU9RC5DqwtyOCPx6/hj1WfUOXZ1TGWgmEsbj0PXFN+9boBMlzCHV+XQJ8PUSbJ7LwrehP+H44vw6jzPRIxa3Q7I74IrcJoqeqhZEBSwJ554PI/ZfFgO43ribSHb1SZRJPtps2pamnCm3ncIXAIPHmKAfjit9TvD3B2keYewb1GHP2bauqdF9pWkdXxMl8qzOnma4vuVHG5T7ggEdfXGmXMaOOnUrPZjwKyYDZtAQRq5ILMu0EqpIPPCjj2XpjbTUsclNHJVVLm+0yN4jXVSpS4Vj6MGB6GwP4/aadJolk3G94o13Egkh7KCfe6kBup/Bhj5DMI6qJqhthCbpT5vOoIQkkj0XeCOvlNvTCqR0EuLIy7xxcdgPaCY/DLppyrjBA3mP7F1I63IIDAH5ID0xT78rIu3S3YSgtYUGb8e32WW/sxcLvMusPYL2gu7xiUZBXQuGX7M2Y3Hsb7gQf62KgflaiP6N9hYF+KHOeD6eTLsZ9TdR+v7DRfU868GDBjMMGDBgwAGDBgwAGDBgwAGDBgwAGJ57jQ3d5vTAPQ5fn1/8AyNW4gbE99xX/AIUOlQBe9Dngt9cnrMCIfQ9TO7winsC7P1UF5Bp/L2aMWjD3S3T/AEefxxJEdCYzsiqQ77TyAOgIQbje5J4N+tyMRh3bip7AdEx+ZBJp+BbqbEBY1Nz8zZtv/wAMStPJaJ4UcxeFI6lwb8qN/X0AB6+ht7Y6Cb2pIqly7E0fjHz3RlG4l7XHBUlLni1wg9rt9b7GndhtZWRlPiF9u8hiwYjcOAdwAHyU43CMEFY5rRyktTxDgdQyrccc33cDoV+ePsrpJVCRow3maPaAbW3XdrH3uyj5MMCdsEmz6KulWM08MixABmKgXN9xVQQeigE3vweT64N0UjtUMpWGMv5QLFrOtm46XstvbafW2Majw/DkDwQySxl3UW8gAAIH+isdwR67gMEVPSRjdSzE3VnLhh5rdLcdWJuPYAYhpVYPgzmitFUIqBGZXVY1I8z8X/Ddcn5qQehvsqFieyVAj2uslvKbi7AAkD0A6D22/PCRfjI2aZZIpEiMm8yA2sCGJ4/U5Un1O5sbnqqqPeZKbbJcyB285aQ7W6WG63At0ANz7Yjp0BNvqfEoCGZaV5IHTeAkQ3H02ob+tm69ALe+FDtWUtUyrJE5clmdRZm2Arut7L6c2uCcAqqZQAJFj3qykyEqSTwqE+hNySfr7DH2CQbTtd9qu0m61rjep6G9lsVAX3J9OMDbZNmS1UEMwaZ3jkBLSCQW3BQegtySTYDkAY2RKo3eE/ibTuL8bRZLkg/rc+vHpbg2wOiGN4pCyqC9rjzF7gnr1N/U9ALe+PrUFIrWjYwb1k5DWDC4Fuf1bH1+oxW6omPBm8dOKi5VbnlHU7Nqqtib/ipJPpwMYQR16MHVhUoFX7NvKy2Fyb9APMBe1ybji98Y+HVIzSzN4u1mXa42mxKgAgXub28vQW+pOypzARB4phJDYGyhdwsAeSV6tyxJP9X3tjPNFqNU2bJSCJXikaeMhRCFs7Nt4C2Pr5Rwb9fbCagy6SqlFVVU6SuqlFiWdYoxYm4YjzSH34C/XjCikZKnN6me0O2m2JCHPl3kKXv/AFiQAB7+bHThWelYvRiplprhpIgoi8O3oAQDb9uKGk+SZPsc6KNomfL58uhiD8KqVEqJY8WsibcfSoVCRl1THMhudlRIw9PRhbnC6snoqmHxopPtSD4aiZ5HJ9BtH+vH2mgqM2Il8qxlVEkoHLn02n0B6g9T16WvW4iRjfc485SQI3hVcKseHqEhARrdVYMp6W4PucJVpfznOtFUvF4jSqrSow+0SxLEqLi4AIvf2w63p8iygh5grTspt1kkYe9hcsPS/A9CccevziCeciCDwVRWhVANpFz5rkAqG4At7E4Rw7i7FF3ZmpURhFDRxMgWONY04sL9C3LdeSPXm9jjOty4GkDzBZN+wHwWBshcB1bgcWPFh7+2E9DVlHNQ7sPDCuQBvO/oo2C1yTf36/jjoQ089arNXzBd3m+HSQoqewFrbj9eL9LW5lDJJsXkgose24BtdgBb9nH7MImeq+JlNOxQ+TeGAYOLHgYWptHlEisbcqwII/H3wmh2x1c/hKyhAhtwQOD6nDPoWpV0PlJRmkYOJCZvMkjoOCS1z1J636/IX6Y2xuyVwO3b9j0HJazdeP2W+fHGNok3DaTP5Bbopb16j8Rf8MJCwhrL71U+EDcKXJ54v7/W+IrakHLFE1R5GkWPxFt95gyheeliL44lRJU5hIYqZomUemwuOfmt7fjxj5mlSKqQJaSWRjYbbIQf+j3+WF1JSJQUREaku53O8bG306nqPX59L4V3OW3sOkkjcJhldOkLJvcC5V5fMD72/wCjj545VTXtVByJxIhFgjeUjn1a9r+1/phLmVVLCAjVFSACRsCjy35BJIN/2DCKKeU2nEtPKxuCDHZz9bG3+GKZ5OaQRjfUdmX2Rt1Q8qNtFg0isAOvsQPwY45+a+I2apN4xMboNhVQQTf9owmoSHdGfxUDnzBgJCP4cYU1dQZK2OKBZCtONu9UUXP0PXFl3EhKmdag2+HueTcOAQYuuNGdxyVC0kniMZEqVEe6INs3DYzW5vZSTb1tz74VUkxQqWklup3cooH8MZT00dfGit8RujcvGyuUKta24FbEnrwfni1LgmL2ysQ1VM9OJY4V8aJmVGZyUVVI+7yCPQk2boTfrxzM6p4ayB6iRFaSiCETeT7WNL7w3LG4W5Bt5enG7no1VRJCTltdUmQTbmp5fG8MeKbsd1rebi6sLA89DzjnGucSiOoijDKu2UPJsAjYC6qTyOCASeALni5vJphJrkQESqJKeSWClhhuqxRAyGboLu119fnjqtGiGNFas3SC+4NTxxxr7KNxPzt7nHYos1oPBjps2EIeNQonkUbWUD7xJF1JHUEAXvYnrjHOsppabLajM8uk+GemjkmDKfJbaWPXjkD58ftxZjVsXPqHtdo8f+1aky7tc74Ob5VmE8kOWV2etRM6ybmSniBUAMb8lYrYfvZZ2SUfZv3oRpzJKhqjLp8pbMqaR2DnwS+0C462YcN6jEFVefVtF2wnVVMxepgzw1iHdyzCXda/zBI+hxeLK9Jw5Z3i8vziDbsj0u9GyEHyMKhDx+DH9mLtYnGTXYjw6EM2GUmvUn+hO81OxodnxEqKRwFawwxM60BkebbzLntdRzk8SLOBb06N88P/ADSGtbKppaCDxZSjBFv6+/OKn617LO1TU1DqXMNQZlV0VetLIcjpKSa8bSA3Bke17n2Fhe3pivnbGLOphSUZZETLR6H17kaxvketzWRpbiZLllt09f4WxIWls31EqiHPaVBJbh0Bsf24pT2E0fb1DqGipqnMK7IqCky93zGTNY5dlRUBm8NEVzySCoJUACxNuObkaPzubPMpNbPFslicwyA8gOOu0+o/6/LFM24S6jP+biuvzH0tUredR6Y4+dZpVRr4dFGHl29D0BxvjqVFGzbrMFtyMcpJppmfwlDODwb4nJmaj6TNhhFy6DDz7Rmp9VzGXPtX1OX0gBtFSvsAHzIscNas7MOy1UWhqM0fM6lbHdPmRLk2vbhvx/EYa/eNy7tU1np6r/oDm1VSz5bWiOXL4oGjkqoOfEZZDbodtlFiRfm+Iu7MOwLWGfxauzfWlBm2U1FVKjZHBHJMjU7g3ZyGZvJYqvnLE249CYUVt3Xyb45JQyKKTr9C0GSaayrIaWODKojHHa4G9m/iScNjtmyCHP8As+zehmhRyaSR0FuQyrcH+GOn2WaU15pzTCUGtKumq6qPhZ4mJDr6GxHBwu1rCz6azKKJd0xpZVUe5KmwxOJ1NFOpjGW6+Tz57Fezgdquoa/KazMDQ0uW0prKuoAG5QOAFB4+8f44Zesskl0pqOvyFpjN8BPeKQ8F14INunIN8SJ2IaX1JmWd58ctmMf+QNJMu4gMUkDKD78r9OvtiPe0jPP6QazzWvjAEZfwo7H7yxrt3D67b/jjfGc3N30PO5cOGGljJL1WevnYRrGXW3ZBpXPyTOtXlMD1DK+4vKqgyW5vw6vY9Q272w+p6eotNJUSSbUZhUKL3uo3N1/utfjkgNiv3cOzr87d2bIhNJ4smWT5hSxnaSFAkLkfQqSAPmcWHdWljImgKTDe7k8GyBo95sDyNyD0Nt3yxohLi0UxXBFHeahil7vWu3qSsgbI6gNwSyDwQ6k+n37j6g35xUr8rcCMg7Dxz/uPOev+hl2Le95J5G7Au0kgod2RViOm1uJBGhbk8dSxB9bfPFRfyufGR9iC24FJnQB9/Jl2KdTfp+v7DwjVnnPgwYMZRgwYMGAAwYMGAAwYMGAAwYMGAAxYDuFru71WkF96TOx/90VmK/4sF3Bhu712jF96fOf+aazAQ+h6Z925Jn7CtDuFk5yKnkVQAbhBIOv9Y28o6WJviU50qYiLGLYsm2S4+zIDLvv8rEA363Hsbxn3ZmWo7ANGqSpDZTE/sNqGRHU/Xnp74laujjeaHoFQqs1iOSGXxCb/ADMYPvY46ClwhGuTRHUVkfgiSMhvNtYW3bjHbpfk7PoBwT643QVTibxCk96glkDEkIRvQbVseB1t1JUfhroZGkpIZKhgpCdOn3kAYkD12e3oMKUd0liDErJOomZR/wAW7bgR+HQfNScRZBrarpoXjEjuqjasjN5Dtsqm3uSbki3AHyxv8VDGsZqFRLAk+oYoxY29AL29729saK9VnO/YB4P+cHH2ibkG039PLuH1Hvj7XUVNHSConiYJsbcA1mXYCGJt1LNst+zpfENruApglR9zyIrJMUjMFjcG5AB9uEufTkY0vLJJt3p4jEHcy3DFWXzG/wDeLg36j/DWKWJ2JSrlWYy7ZSjGytc7mufbgg/PGMVDWqY0pqtlAZShKhg1ivlA9Qpbp9cDSBdToySUtQJIJGDSyMZL7br05NvkFuPr7jCXwKQ001RTqyX3FnQ2BJRStvqxBwgK1yRoAtOyBEABJU2Bk2g9Tyd1z8+ThZHJVKFWWiZ0Uqu1GDEAPy3JHDWtYD6C2Da10ZHcVpHXBvHFSrooY7pUvvjve/HS/rxc7jjbNNXoTJJTq7Alrqd3Nla1jboot1/acJaavhhPiTSKsihVuyFE8vlvc+ykdPXG5KiCaT4dJkcyBTvFitt22wuepBBP4+2ElY5muZ0tOCGV4ncNdpV2ncfuj2BINyb8WHtbG+BhKpl8RXVCSXIJugcEkfK1uPUk/PHx3V4wWkJiLFZWKizAXRm+hvb9vsMazQQEbmhCSqwYBPKzcWHKm/Xn9uM0i1IxhkahrqqGCJPFnkSRN9lCkgqzD2ttAHyI98dMZc1SR+cKyWdk+8qeRh9VPBHzGOLLSvDNDWGSapSNSWRyDeJgS1/Xdcbh+GN8UMlYREK41JjbijINoxf72/0FrWPJxQ3XBE7TN9SaOlDrkwTx3R4mMXQ3BADX5Bufx6Y2rnBekSHK9qRltpqLCxubeS/HFrbibC1hf0S1IlR/zb8N8LJPdXKWd/Dt5j4nBta4Fxe7DnGcccawyCyhtgR3JsALWHhkglr+lhzYfO6JNsVOlwZU1Gal3RIkk3XM/i3YMCPvNJfzcj8P2Y3Jk2WQxzVNZ4DRkhGkZ2AJ9AALXIvY/hhb/kscKVxpmlEQADyrd5GPAABJPJI/HH2mp5K6pNRUyn7MldyHgH1RPb5t1JvzYYihowrliEmOurqShjp59lIvxG3cV9LLxuuANxtex+vXHRaRWbY4u97kMdv/AF9MaammSLNEWCGMCWm2DaBcWa5tfqbN/DGQEm3cvHPR4ySAfnf5YKomJsICsQxZb8Wc7gP9ZxqgJSqn2A/dXzRnyjg9BjNw23axKgH9Zb/jcYQO8ySTlYpWEqKEeEbgCAQeBb1tgdlnAuNZIHk8ZJlINkJc7iSP6v4evtjm1VdFHUSKtRFHujBFibm56n2+uMI6mWmJmiirg5IfxPhtzA2sVYm5PT097HHKMj1dalDBStGNoTbLECdt7nyjn/r7YrnN1RMVydHJYpK2qasmnE0cfCXfcpb5ccn9uOtXPLtBj+INjYkAjk8EcDn6/XHyCAU9MlPEuzYB96MsQByTYHjpjTXDem4RSSIy3YK9gp/E8XGGpxiRLk40sVQPI0dfGov5VbYA3Tj3xzPEjml8KqmWV4jYJUdV+tzc/hjqVYj8MOtGbFgVUPcEj2sef2Y1UEUtZJsVpvMxBEkTNb5WLcj8MZnG5Uh06QqpJVymkat3EM5siPPbafdh1HyvjflcQkk8REhZ5PMxjmN+evr/AKsfc8QKaSlR9p5a6QEWFv6t7euFOXRgMp3RMwFgCgBOLYq5Cv3OpEzlAoEwIFut+L/TGcoZE3Sx2VWv9o5sD78i30+eMBEiXIiYA+zEcnr64+FTCPMrR2B2luVH0W+NKEaE+YU01bSPCk0wkfzRlR5Ve91Yj5MAbi3TDbHaBoWWmZmz6GKoZkSVXLtIsoI8u4GxYE8Dn+GHW7FYjIyu4HU3II4639B8jintBmD5l2i0tDUFKSnFfK7iylZWLlhcdN1jYeo98atPgWa77FOfUTwpJdy21RQ7Fh+EaQkAsqhGA3WHnLD0A45YdeMNbXlZUZF2faor6CcrBSZRWNKFAdD9kxeyqfLbjkEAk8gnnDzoJWDxUs5YBSYkLWLRNbiO5+8pF7G1+l+esV96zNpMi7Adc5+23nL3pUHQs0rCC7A3uw3H/qMV4luyJIvnkex7vY8faivFPmL5jJ5jAzTEG5BI4A+t7ftx6TUWe5Pn+p8q1Zl0DiHN6KKsppr8PTyoGU/+r+OPMfNvEi3Ty3KNwzX+d/8AXi6XdS1Eme9ieUfGVDtVadzGbLVLG94SwdFuegHi8f6NsXa1O7Zo8DyrdLG/6kXLopb0qKLEMPXGmTLHf7RWYE+Y3OEWS1HiUUXNrcG+HBAy7ebEfTGd1NUzZPG4NuJzRQSlNsp8h42hbDGFRHtiNvf5Y67lNvlC2+mG/nNakS7EK3xVkjGKGx7p9TVLM/wzC/BwjyWoKVDRsfvG98b2jkOVmYqQTcjnr+GOBlmYIK0AkA3tyfXFGSVNF2GFp0PWXL4qjawNivT5Y0vkoIBkldgxuR7/ADPz+eF1NKzKtwMZyycE9PbF6gmrKlkl3ONXhUTw1ThR1PXDA1hPDBlVZJIwAELk/sw9M5qWS9z1B6YjfVkQzejmyk1KU4q/sPFdrBd3FyfQYaP4vkMuIWypOgdSHSD6uinRaaClp6iSaSTh5GCMFjQeouxPX1JxWisneas3bVYHym4/C+LYd5KhyLs80D+asrqlnra2VoGqAwLVDv5nY+4AB6epGKnlVDFi3lufN1twPf6Y24u8vc43iD2KOL2PRb8m3qZcy7HM90pUQiRcrzsudxItHKqPa/ofsnN/QXOLhIKqcoFkjO47VYLwxcG+1ulmC3uQQQPQ8YoD+TIz2emfXOnXZAtVTwVqIxO7cniRED2uJbA/THoGKiGnaWTw/sFl8OnCKFK+fbHa3rcOB/ycXRuKoyQ9kQ93oUnh7vuvaqplbzZHILNwxEh4P1ux3exsPXFWPyux/wBp+xIEWPwudH/zcuxbHvPRTnsB19G/hyypktW5jPQRnYhA9LArx/o4qb+V1/3n7FP/AO2zo/8Am5dirUu1H6/sWp9TznwYMGMxIYMGDAAYMGDAAYMGDAAYMGDAAYsJ3Av+Fnon/wDo5x/zTV4r3iw35P8AAPe20QCAR4WccHp/vTV4CH0PSvu1w1cfYPotjN4aHLI1Cnkm7SBVNvu3s9+vUYlKWOtlTw5X8QsSWjZWJZSyrdre7KvQcc4jTuwzeN2BaKKyMsEdEyOL3KlZHVrf+Mvz5OJWp5BLGrSrd15DEelldufXqW+WN0JPahJdRJDNWrsLqgSMkJJeyrdfDYHg34KgXtyD743R1dYZXappAPGPnIe7DhlO3p0BI/0l+eMoli8UxEMg8MpKFPmJtdibc3Phq30GFMx8WRZjbzLtjdfvkNYcfMqwP1vid3aiBNUVzho1ko5LJYlUCksBtupJP929z7jH2SqiqYCk0FRZ1ANoj6IVP7SVYc8cetwFBmWanEhBJn+1lVQer2vx7EKSP9DGaX85abcEZvCuB9oSpj2/Rto569fYYN4CaGro1ZEkns/ibpSVZVYb28QjcBbgi3sAcKBXUF0SWrRIo72csOHZl38elrk+3p7Y+xK8Uln2faMVY2+8QNt7egO8/jb1xk0QEiuXVli867xfxAq9D+zke4Prgbi+oGL1UTssjJGXKkmOE3JLblsOnA2g3/vHCxHjWkWmVQ0qvuUh911DBRfjpvs1vnhHLFTCJDVxwcqkWxl8xKq17X9/T52x9hp8taRKYCPx0EchFx90kkGw6Ag2J4AIGIdMi6FNOV2yAMfB2gjavC223P8A4wwoeCFkY1MMXjF/uMnUE3JH0a4+gGOI8mSU1bBQvUKk8ijZTfFWlcAgttW4JsAOl+fT1wsny8XQrUTCVxtV/FZuCzEWH0PT324RpUMjbUZZRMQkUaKzPxtZgFuw3Dj0Frf8q/XGxaJtniUtXUr9m/6wa5LAqLkHqSb/AF9r41GkadZPCzKqRXJ3AlL3Yr0uL9LfQjGv4atJAizIW8OTwwacMGBsF6W5sG+QPTGaZdF0bWgzCJW8OsiZJN7gyRWLji3IIt0YdOBjGhEC5ZSfGEA+GRGzEkWP3bBfMfLboQLEYS17Vxq48smrIFp52fxm8IqY4QfNbzH/AEenUkYWwTvRoq0+XtA9jaqZTuK3sBtW5sLAAHjpwMZ2GSRjCFapiEYmMAifw2mAEXLR3sD5vuj198daicRVJqikjtYiNJOZju9vQJxjl19yhzSau8cU95HZyqbY7eby9Bx/WPp69MKMvzKmnjgzCCqiliZFmFRC91kDDyhL9Qb8BSeoGI5XBXFnWm3MaV5FB31e5kt90lCAp/5VvxwqysL+bacMxL+Gu4nm7W5J/EG/zwhgq6fMRWQvJGWUiOVIZAXhIAI3DqrG4IB5HHXHyCqkpJvCqF3CUE+Xyh7gHcl//OXqCRgXDL+qNufvT/DJuAacvan46Nbkm/oB1H+u2NLOYwFbYu8cgEBSfW3Xjj8MYT1PxGbJLDIHEMFwBdSrMxv9D5frz0GNoeSJtiOyRkWtx9ep564hu2JH3Pqyog2pZLixKm6/x4P8MJ0qxVVjQqsbqi3LIfMGv1A9ret+uM2LkMCGBIO5ke/H16/xwjhyiliqDUKzSBQBCrPwq25t+IvziJNqkh7syr2RE3FJASDzcAD+PF8Yafp0SM1ahlaU2srHyAeoseMJs7mVEcRxbLG6nevHHPUf68KtPtJ+bo1lViQSAtxcLfm/oT8vpbCJXMm6R1XlSBC25kJ6X5ZvmbdcN+orI6ipWJUZ7X8wYe/zP+F8LEznKa6kabKczpqin3su+ndJU3jqLgkXBB4HS2OBWieabeHqJSF+75SP/l/hiMzcVSCPJlXWlfbLT7gWKhjLZTx6EcE/hjr6bG9F8WOfyAcSMoN/ltJ/jbDehp2nez0oOxhtVqgAX+RA5/jh1ZOqUtIyvEqswZ2VZSzAi/yscU4uZ2PN8UJMxkkmzZzsmdYdqAxuPxuPrjr0rbo/DZ+b3KutuPrjiZdBHUSSyiFC92Y7ZCCCeeb9b/wx3ad5EQpuZTxs3C63HUXHOL4dxGbwu3crKLA8MpH/AF/63wI6x38scZtfcouSPXpyf28dcCgSbVkRB62LW/h9cBZUJJKowNiL9Pbg8DnFxF30EOabptkNQXWimfbVOrWug6EgG20mwLC5tfpiq9XTZXlfbMYZdkQjzEuACCi/aC4W3IYdR9R88WxkihnhkjqYgY2UglxcE+o5sRxe9sU9phKO1Zpq+oWnmXM2iLtGHaoIkATd6AkC/oeffG7SP0yMWtp7fmXGzIA+I0Zu5pgxK2++rjw7ce5IGK6flC82OWd3jM0jcxLWZrR0ZRTxKxYy8/IeHce5viw1LJNNF+cJyPBjtKXkuqyuo8oH9xff1PI6G9Vfylxi/wBgigk8aSSeTP6a5QWjAEU5A/jxe5+nTFWm4ypmnKnsPMjNalJqOeKwPk3D6AXP+GHp2B9ref8AZtVS0VIsdTluYSq1XSyoSCVF1kQ/qkfxxGNRUyNuDE227b4W6VO2s4vZSWJHXpbGqbWZpSMmGc8HqgewGjq+OqoYpIn8SJ0V4291IuD/AIYdqSgCwsLjFbO6/wBokequz+ggkqkarypfgKlL8gJxGT9UA/ENiwKVCSQiRXtxjkyk4TcWemjl87GprudHxQVtcYa2Z+PT1A8eimmDPx4drW/bjo/nyioWPxsvhRggFyLAc+/pjY+f5HUW/wBsqdgwNrSD/wCOCSc1wW4t/NKxFmmrsvoMqSI05dl4ChLsfw64aE8EtWRNS0lTHPNIHVGhdCPwYD+OHj/tEH+O+IpOLjf4gvhdR1+Sr9pDUU+48kh1ufxwk8cp0XY92OLcYv8AIW0UbJTrHIQXUC9vp/04+TvZeOo/xwmqs9yqCXY1VHvbkL6/sBwj+NWcM6yEp6ceuHctioypO+Tk6glVGcKN20XIxV/vM9qsmgMtoJ8tjimqqirCrDISFcBST05te2LFamr44IZmacKqqSx9bY82e8nrxtWdoUlPT1QkpcsBij2njcTyf4W/DFmml5knfQXWTeDDx1Yyu0LtK1X2iZvHmepXiUU6lIKeBCkMK3vYA8k9OTc45CkiOPaikMt2N+gIH/ThHVlT4Qbau5b9OhxulbdtgjAO4BfkeMb4VE85mySzeqXUt1+TznqaPtRqoaOQK9dk9TCSQLCzRsGN+oG0mw+ePR2GmrY6dKWDwgl/s4l55B8VQT1Fg24EjgH1IGPNruO/D5d25ZHT1MqxRvBIzM3IYeE11597Wx6VxRhKcB1IkiDtFcWsEG1246WTYp+f44uUqdBiVIiLvQtVP2A6+RpSSuVSBto81nsy8jixs5tzYm1z1xVz8rx/vV2Kj2p87H/m5di1Pej+KfsD1vTyF0eDKJlZrCwZZLlSB0slvp4g9sVU/K7knLexc8f7nzv/ANXL8U6rnb9f2Lo9zznwYMGMowYMGDAAYMGDAAYMGDAAYMGDAAYsP+T8/wCFzoX/AEM2/wCaqvFeMWH/ACfX/C60J/oZt/zVV4EB6T916KV+wnSQhARHp5o02Dl28Ri261+do9rn05tiU4IszZmjinjsOVXY1ldhwB8tqix6c2PtiMO63VCbsK0o8yXWCKoCttBIIqnG763JH0K4d+rq3XGWKjaW0/ldX4cUjyPWZhLFIuxCNqwxwv4oBQAAOvW3rfG5OolUuon7OdbvrvTx1NRZdPl4NbUUIiqVG+Vopng3q17FCYns3Ui98b+zrWNb2i6UptS0mV1OU3mkhFNWovixPFNJBcFWO4b0cA354uOlq/6QyPVWe0jous4abINPTZXBmNUIpkgTMaZpKypRYkl8R5TNVQq7XUWjkBW4AMkd2rOarNdLSSS6hzCveWtzCbwXyh6Sljd6uWa8cjxgyE3NhvNgRwCMEW2K6XUmCn/OL04ZKiC6bUUleqhtqEj5Hn6Nj6zVtJF4M1TBDFAC8jMCUjSN/vFiQBYgtz7N9Mb6dAZzZRvVFR+AAy+GxS1/7uy9/W2GV2v5loHJNETZn2iTsMijkSSSjUtIatjdvC8IENN1cGPnd6i17zL5B14QzIe8vl0dfTjMckzvZnuePlOmpaTKJp/zvEm1/iE4A8IuxsQWOwBuhsJlZK6am8L4mnPMhO2JtrFTvPQ3uevT35HTFbOzrPtL6H0unbZrRc+no6fL4hksa0U1RFp3JnYOiB3VRK4BjLsm47QqruChjYzK8+yyuy2Cvpp0FPmUUVRFEV2MY33HcAebhGB6D24tbEfQaqI77wdNXV2lcjgmcOp1Vk9M4hDI8ivWxhhuVujXIHI6HnnDQyHTNHpHNdXdtdBqGgyGjzWrTKaaerpJqyIUEZWFX2CYN9rUMCGHIUKehOHt27ZpTRZXpyOSVvstW5EVZDyCa+I2sB0AW/yDD2wwe1GszLJKzUPZvkdJmUtDW/mvUmWJQUhlnomGawJUokW0gxm0cqqQQrBx04EN12I28WItS6nra3t20hXVnaHpuqno8jzeelqn0xOFgZjSqdsfxG4Ow3AMGG3z9b8SX2c6szfVvaHnkVP2wUWcplsMEOYZJT6ekpIaR5FvHOsrOxZieo3EW2mw5vCuoe13IKbt6yLKDq7tFkzHLslzKmmgfI6b4mKSolpnjjEZpgCHVTuutwdgBG6xlnsIqs8bWXaVVZrRaiV6nMqbwKvPKQUs0kaUUS3KoiqbbLEgc8et8LXFhDqTHEK0Fd1XEI1ZdpKE3Ph2YHzc8WP15642LHXU0w2Swjds48A+VQHuAd3vz9SMa5psydWVIKaJoi6rE0xf9Xy2IHA8x5+XOAyZjNu8Oamj2l932THw/KrW6j1v6e/pjPLh8l6ZrpYjLVSzCpVp4BDGjbRZ9q7rAE9W3g/Vb47QrZKdvDqqGaJjbcsYvub++9+v4/txyqH4ikr5IJmQtVhRE3hGwZQAwtzZttiPxwuheavLQZbuiph5XkhkDb/UgBjz+zp74zvqE0R72t0GYa/y2bsy0XNNQzZvTMuZ5nToY46KhYEOqEi0k8tjGoF9oJc2sAzE0lozVldlBodAa4zmmTRdYhpsuy5cvhyzM1G5QoqUisV4cSxkB1KsSL7WPf7y3aLnPZ/2e55R9neSy1OoKeieeSqaLZHl0JG3xmlXhpCbhI+STybKOW9plc07FtX0umMhpa0UOW6HgrvzBRuHWuzaaqaIySu0avI5kUAykLxcsAouJXxESvqcrTdHW6RzzOzknaNmuY6hrs4RdbVOT5VSyT0FRLtWIeE8Jl+GRSFFnbw1DHafMVnDQuR5lpChqMrzfWGeaqkrKszls3aBvh95BMKGONNqg8heQCQBYWAheGN+zrsE1DrKDtNzn+kSSVtfW0q0lCC+fyyFWhG6BpOagqi+cnaFsRxiT9Cdn+eaQydX1FrLN9Q5lmKLWVDVskWymqCqieOIRooRNx3KlzYKenN0k2la6E8xdj7c01PmkVRCTLFs8BkdfFVWPKjf6AkW5J5Ix0kkUbksQDyOrKPx6A9cN+jUSBoZ5G8CQm4MRIS/QqoPHIFj7/jbppUvSRNFWwSlY7N4yoSpU/dLAdCfX0+YPGEix4sWEKRvVQL+qA2GMXKupt4ZJBFz5T+FsfY28xKuG3C94m8pB9bdP2XxjMoK7VeNrepXn/pw75HIF17l+s8s7TdJadoe1TU0VBqd80eoiEdD9gsUSuixk0xIUFwLsWNh1vzh0ZvNmXZj2e0ehNM6jzjUups3MtFks+b1CzzF25aeWRVUGKFSXLdbBV5JUFj9rnZtk2ZdrWhcsqKvPpKbOEz4VEKZ7UoNr06bkQ7vs1NyCFsLcdOMOfJtCQ5TSjINP9nuWaloUp4qJ5c61DK06xxveOmHiRTHYGFwA4Ba5235wsZKMiErVnIyhtNd1yDL9GV+YpDpGrhjZcz4Y0VcQEkeo9VinfzrJyquzqSBsAlCorY6qFaqJmqIpFDo3iHaVt1FjYj/AB9MQ72kRtoj83R0nd00ZmWoNSzHIcvp4MyBlnRwWlVz8KD4KoGZySAByeSLyVlWRZXprJKDJMl05TZNS0saxrRUTDwKcn7ypttcX6mwvcXF+qahKrGg7Z2KOjSQROGgVZCDZn3Jb5Atx+Bx3MwMUWWiODw9zkKPDi6L6+45wlyGmWKAsZoltcs9PyR/pEjGM1VJmVVuMckkUVkjYVAF1HrYWub++Eiko89SZdRZlkcccS7djBeLbQu36WJv+OOqNyDjcvFuV3A3Pt9PbCeKyqDJLaw6Tev0Jtb8Pljf4o2qXV12jre6k9B5voPUDFsVSIfJmslxZvCO0G/UX+vOMvGV0QAqgU8FOvsbe3/W2NalJW33iv6kJe1ufl+3Guerpacq0tShMhsqRKS7EDoLcseDwMNYtNvgwr6iOjppKl0jDRoXVXY7nN+B9SeLH3xUytp5qLP3gTOaEz01UZmns3iKxkKqhWxJPlv6kX624xaiYVVVurKqlaGlgbfHHIyktKeFZ1INhyABzyQevGI4bsq0rWahfUj1dYKiXdI+2oSwQFmJI5PAJ5v6cW6nVp8qxWn3Fz6V5dtdmSRHn0NRCJYY1M0g3RSsu5StuVUsRY246Drir35Q9PjO77UNDYLRZ1RSmwYlCVdSrE3sfODtuOvQ9RaSiyunFOkubVzRrKA/gicKSCALO97sbcELYX4IOK3flFs0y9u7RX5flQskWbUO4Rx7UQBm4t6C9vTEYH/MRbmcY43E8k6hnsVa/Xk46OQTiGuLhudh46X9LY5kzBmN+DgpZzBOst/unri9PoYUlt4Jn7Ge1qq7KtdRZgZJJsqrSYK2BT9+Mk2cDpuU8j8ffHpHovV2V55l9JVUdSs9NVxiWGZD5HU+v8R+39nkHU1gMsTixAbda/yti2HdG7Uszp66fQ9ZKstHFTtV0m9iXjYOAyj+75if2+5xk1uLdHzY9UdDw3P5cvIl0fQvhmOWQZhQT0koBjmUqb8jnEP5xoFKSQ05r81oV3WWSmqCYx89puB+AxKen87hzKmBVgSRYjdfnC2qoaeoKl73HHOMuLNwmj0+i1s9DNxfQhKLs/rWTjtNrFg2lvNRRs3QG263zwkpNAyVM5EWq8/rjfr4yxRj6bADibDpfK3feyRk+n2YJONyZTTQbQicKOB6YvnmlXCOpPx2Di1GPI29HaNp8khZ2jV5JFszO5dv/Ga5P7cdTOMxgoIGVG2jba49D74V19aKWJ1LC6i4ucRD2ga5GXRuqHxZnB2Rj0PzxjySk+O5xfVmm5zI87yfbPS6M05PSU1UpzGtQxwR381jxu+nrjz+qZZquoeqmctLK25ifUnk4fnbZnOb53ryrkzactInCqT5UXrYYZz09qKKTaFJJPA9MdTT4PKgeb8Q1Ly5K9hOftZwvFkAXCzKolnziOJ9pUvtP4i2OcrlQ0gW+9uBjqadp/Fr6VXNt0hH42Nv440R5pHOlSXBZnupMx7cNFVEkscSSR1EMgYA8bJQeD69LfMjHpxLT1sUMoarJ8MsrlrD9Xc/N7EEE3462v0x5OdmmfDS2cad1SjBfzZXJM0ijkIJFLCx+Qbrj1hTMaSqpqXOkkBpHVKhdxJ3qxNvrusCP9A40NrdwWQTRE3ec+NXu+69lkmDBsodFcCwl3KH3EfeBZRu5J6Dpewq5+VzJOW9jJJv9hnfPv5cuxa7vO0wh7vGu4JYFkkOTTvYdE2lVYra3SwU+9hiqP5XM3yzsY6/5jO+v+jl2M2pd7fr+xbDuedODBgxmGDBgwYADBgwYADBgwYADBgwYADFiPyfQv3u9CD3XNr/APkqrxXfFiPyfAv3vdBD5Zrb/wAl1eAD0f7rssrdiGl1RQEMdYm0G29fiZd7H16IBcfI9ecSTneVZ1mOWVNLQ57Llc7AxmrSmWZ4OhYor3UvsCkbgeL8HoY47sAB7D9PAW4StdSyjgmql559CIyv4nEuP4kiFQpWQ3LbuQV3GPcfpwD8r43x5iimS5I70r2MUek2Emjs+rMnNaqrmLqyzyZjNtaQVEzSqbysZJFaTbdrBSbKBjq6e7MWyUUMFNrvU0cGXMD4Us8Aimud7b/shcElb2sVLcWw74aks09QqB40Z2YMfOHG3cvPzJt+PsLZ1EsYLxIwY2lYsLEEhLEE3uA10t9bHpieV0F5D83wKkUbNNckPIm87lHVgB0NgpFva2G8vZvpqpzGXP8AODVZvXPGYVqqqXf8KjAiRIEHljurA8De1vMxvhx1M8KSPKalXYMQrWADi17n5G7L/wDLGDVkKqIftZipZWEalw3kUC20GxD9ORwBg57gRrRdiFRWaCpuzvUeva+ooqWCijZqajpolkSDa6KxdHZlIjUHkXsQeuJXjghgkRjEVjG1VR7Wa7ctYC17hQT7AnCWOZ97hacsWBCobJZbN+Nrkn5EW4vgdszHhq0UapBc7rlpCUCrythfqpI/h7xTb6jLpQxe3xQmlclNYN9MmqsjNQmwsVVswiLA29rnn2Bxh/Ql9I55mlZ2d5FUzaj1BGkEmc5pVzTUeX0xdyqr4jG6X3MIY+rE7mUFTh/tTVzQBpK20iqfNGBbci+Tre9wTY/t5xmuXRF9pllLuhjiZWspG7arEdAd1z/ysK17sbtRBT9kumsr7UNOaZzBKurOfZBndVnObSErLV1slRQSfEFwLJICgKgH7PYoAsuH7obUXaVQZxNozX2RDOaSKM/CalpHjEcsNzZKiEkOkwsAdilTcEFRxh/GgpFkkSGjUFQzICAQxKqoHPryTfClx4kZVI0Ik8xWwAIYhQR6WIH7QvthHQR4ZjPXK6oY6OplAA6R+GeCGa263pyOvpjGNsyk3t8NErEednPG4EnbtAsb9OvW3vjL4mk2M7vGIGYCPd5dgJ28D0AUD5kWwJmCSVKpDTTELYcAqQWZeLtb6i3W4xnmXJI0VeX181hJXQxKV8TakRDLyGuC1/MCbfRfnjZTTVNKpgrCPCcE+MtOJI2BtcsltwPPvb2AwWrKgpcxQF1BNmLOUG/9XgE2vexPpjOOJ3VkmqmjAYSBR5V2XBHPUcKf2D3xmbHasi/vEI0/YhrepepppT+bXVdlIzKRdebqSF4uLH5n1Jxhm+cDTfbCms6zNXqqLNNLx5VFFR0Mk9VNUJVNJaOnVS7qFk5IBAutyBiTZsuyvdIaqEOCp8Qkjxb2v948k8H8bjGHLAxUktY5RSQVYsoPQAGXg+4IucJvcVQnk13K3ao0f2mZvrWm7U4NEyVGnKOshzOPRVVXRR1mZVMcZjSt8oaNJgAhEN2B8NSW3jEtaD7UKTWVG+aQ5BneWPQ1HhVFBmmXTUskBIs6F3tE/lJ+4TyBh1tDmCM0MzxePEyRM9PIIvIQ3lBIAU3BuVt6Xt1CiKkkgl8GgoadKhjvDMC77eu4u1utj78/K5xXuciqUXFtCyJoo9skTQu0pvFIEAXg3CrwACQOQCeFI+u2uzA/ASI4Z2SRZ5pWZheNHBZrXNx6DnCGlWSnrTNHmMrFR9vIHUIzki5VStlUL62PoL9cIs6zrL8sjkd2lYOhCKYkBN+llK2IPUngD9mJlJRXIKSjyx3DbJaexu58rCxv9Pl9LYHtyu/eehVhc/t64bWlc7mqKGGlzSPwJAo8F2O3cpJ2ru9SB726evXDjLW8jS23Ak709Prh4yTRoi1JEO1PYrnmpc/p9S9pXaFWZpPlk9Z+a6PKwuWQw08rbTukiAmdzGqBiHVbhgBzhzQ6Vo4cvXINO5x+YFVD8HLQxg+FJcnzo4KyqbncHve5NwbMHtNNCy7mmXzgcFRYn+9x19scysojUR7mjnksw2MkgjII+X/RhZdUyUiPeyCvzHU+rdT1naJlnha401KMol+HiK0UVC4DxyUu65Am2h3JO667DYIow+5oknrzGaaFOfKZCHuflbn8L43QZs9JIKKuj5ayRs8t2H4gf4nGTqaWrqahdkZCE/1vpf1X9uFySUvoSk0Y1ebXU5UkyRIoszeYt05Gxen43xvy2CgUMIZYSeOANpJ+XPH7Mc3LVcgNFPIGa7HwIjfn5nqMd+FgLo1RLduCrxlVb8SP9eIj6vUM0kKYwsa7AHiDcWfzbvpbG5LADysig8kenp/1vjSqLEQsAjW9r7FDA/XjGzZ5rBArH7xiPX6r1/ji8RKjKR4ZCWLxG428gdMIs1rYad6V2qWAaZugUsQI2LWF+hFwPqMKHnSmiM0tTtiVQWLKBb3uT0/jiPMw1g39I0bMI2hpVRWozJ+uu7hztINm6dRawvYE2hy2mrTaaeWTaHtUSmrmjieEI4HleJVuT1VuNx/HixOObmdakESxymbx64lPDfcfsw3ncxrY2tdbj1Y2Fr41ipFbEUo2hik8EbC1KNyq36w3M4Zb9Dax974wioquOnaWJxVhW3VcNXGHljZRa4K+Yjrbym36t+QZXJfjgk+eDWs3laWWBWSc3jmhkjdHPHFpVO03J6sfTrziD+/lRtXd23PN8tSDSVNFL4MtPYL9ui3DKdgHn9ubYm6alCrJNQUk8Rm3FvhpllilX5q97A88heDbra2K0/lBtawZJ2P0mg4qlmlzjM7fbENI1PTMd7MwIH+cMYACgWJtexI0adNzKdZLFDG66s8vKhGSYqTY9cagGa+0Y61fQuyNLGvMfX5jCCFlvdf4++NDjTORbrk0AsWAe44tY+mJV7u2ayU3apkQjlsXd4WF+oZWBGGhl8GXV4WGenj9PMG8w/6cODTWQtkmdUOoshr7VmX1Ec6wScb7G/B+fthMqbg17mjBG8kZezPSTJaypoVSpoioawLoRbdbDzodTUlYAssnhy2uY3PN/qeMRzovPcv1RkdLmlJKFaaNXdQb7WsLj8DjvT0cU4G5AbdDbpjz2OU8T2roeqlGOTmfA+fzlE67hMFVRyOBfCCp1DQUiPI9XHttcXccfLDOOVRySC8stvQ7yMfGyCiXkqzHqbm/P44eWok+KBYcS7ifP9TVubO9NlKeW3MrHg/TDXzDTUPwclVVEyPtJZm+8Th5/BU9PBtWJb9ePTDT1xmX5sySpl3AqkbMSTipScnZaox7Hn52wuK7tJzERxhF8QR39AMNVGaqYxIbRRqQOf8Ar7/wx0NX5n8dqLMKoNdZJWsx/WN/+jCHLY0ZWJbg3339vYY9RHhUeJztSm2JWgUzgAXCC/J46Y6OSNHTVEE8lvs3LD6g3GEsyqsMksZBuL8D5kY+s0W1LEArIbc+luMOlTKOqHtQZmPzTNRu5WWaMNGt+DY8i/0x6Vd0ntDHaJ2P5TTT5gxzTI4moKwA8qsYHhtx7x7be5ZzjytBrGpllh5lpnLAA82ucWj7hOrcxl7XKDJMuqGRcwgqUqqXd9mSkLuj8dAGVT+GBy2svjTLl95+KqTsC7QFWcsRlFRG25SPulWYA38y3a9wBza/U4qp+VvcSZZ2MsOngZ1/6uXYtL3qJWfsE1y1IfAikyeVlcsLMjk+/Pm8Mcf3RirP5W0N+auxgkWvT516/wB3L8Vajna/n+w8erPO3BgwYzjBgwYMABgwYMABgwYMABgwYMABiw/5Ps273ehCP6ubel//AKLq8V4xYb8n6QO91oO5HTNev/cuqwAej/dpnqv9g3TkbwGRENcoG37yislNrjkC/F/mfniVoTXGMt40d0YXceYHexFri3BcHm37L4iLu00+ZT9iuQy0ObfDCKepjki8CNvuV8pBv6Dk3+X4Ykyly7NI0UH
```



response

```json
{
    "BYPASSID": "F224316254",
    "IDCARD_ISSUE_City": "北市",
    "Sex": "2",
    "Birthday": "0570605",
    "RspCode": "0",
    "IDCARD_ISSUE_Type": "3",
    "IDCARD_ISSUE_DT": "0940701",
    "Idno": "A234567890",
    "Name": "陳筱玲"
}
```

### /OCR/getIDOCR 身份證反面 

request

```json
{"BYPASSID":"F224316254","IDFront_CONTENT":"","IDBack_CONTENT":"/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAEdAfwDASIAAhEBAxEB/8QAHgAAAQMFAQEAAAAAAAAAAAAAAAQGBwECAwUICQr/xABmEAACAQIFAgQCBgMLBgYOBA8BAgMEEQAFBhIhBzEIEyJBUWEUFTJxgZEWI0IJFzNSYoKhorHS8BgkJcHR0zRDZXLC4RkmJ0RTY2SSlJWyw9TxKFeTozU3OEVGR1RWdHWDhaS0xP/EABoBAAIDAQEAAAAAAAAAAAAAAAADAQIEBQb/xABGEQACAQIEAwIIDAQCCwAAAAAAAQIDEQQSITEFQVETMgYUIjNSYXHBQmKBkZOUobHR0tPwFiNyknOyJTQ1Q2SCs7TC4fH/2gAMAwEAAhEDEQA/APKrBgwYADBgwYADBgwWPwwAGDBtb+KfyxXY5Fwjdr9vbABTBiu1r22n8sU2kckHAAYMV2te203HyxSx+BwAGDBY/A4NrDuDgAMGCxwbSPY4ADBioRz2Rj+GDY/8U/lgApgwWPwxXY/8U/lgApgwWI4IOCx+GAAwYLH4YLE+2AAwYLE9hgAJ4AJwAGDBY/DFdrfxT2v29sAFMGCxwWPwwAGDBgwAGDBtPaxxXY9r7DYC97e2ACmDBY9rYMABgwYMABgxUqw5KkXF+3timAAwYMGAAwYLHFdrHsp/LABTBgsfhiuxrX2m33YAKYMZfodX9F+m/RZvo+/yvO2HZvtfbu7Xtzb4YxWPwxCaexaUJQtmVr6/IGDBgxJUMGCx+GDAAYMABJsAcFj8MABgwbSRcA2GKhHb7KMffgYAKYMGDAAYMGDAAYMGDAAYMGDAAYMGDAAYMGDAAYljw6+HbXviM1mNMaNpfLpqVVmzTM5QRT0NOTYszAH1G9lUAknngAkRPj3A8AvSnJum/hm0hLRQRjMNT0keoMynuL1LVUavGCPdUgZI7Hi6sf2icZMbiVhaebmXpwc3ZDV6bfuZvhv0bTwPqLJMx1jmEdmefNKpo4t3uBBGAm29yA4YgHk4l4eEvw2NdpOiOkC5p1ppGOWpzGLWAA4BFu474lsJNtALsSw4LG5v8z7n44qSbFF5awv+GPNxx1erdydjYsPG2pDVX4NvDLmLTLUdEtKKrnZIYqRozYuWYgh+/ONbH4HvC3DlS5cOi2QNAZ2lJbzTPcnt5wYNt9R9Nyvbg4ndYwzGyG7G55xdvfhNoAHa/vhsMTVe8g7CKII/yIPCxHI8zdENPl3J3L+s2j7rMP7MWDwMeFExEv0SyAMx8ywaotu9h/Cjj8MT4pVnIcXNrnFzRq9j7gcYfGvV6lJUUQFJ4GPCk0QT943T4Nwbhqnt7/8AHYT1PgR8JxPo6KZIpP8A42pH/vsdDbiVsQN3thJOVEgJgLMPccj8bYbCvUbs2V7FHPcngT8KWwOvRfJbjiwlqf8AfYxS+AvwqW56N5UD7haiq/3uOiZWjVXcLtZELsu4EKPiSCQB8u+EKzRSQpUwTLLHIodWRgQQfhh8ak3zDsUQEngO8J4ZXTo3lnHqsauqHqHAH8Lij+AzwpC9ujOWWvxepqv95joBSxH2mAPyGMsE8MvmmJ/N8i24AHg41wnLmQ6SRzi/gH8KBBf95+hB+Arqz/fYwf5BPhRZt69JqNQO5FZVen85cdG1U8dy0S7LGwI5OEsgqGLrEVVzZQ57g++NFN3Ydmjnw+ArwozRf/igo0u3/wC3Vtz/ACv4btjC/gG8KIlEf701KzFwh2V1b3Ivx+v5Py7n8MdGHeoIZg5JHrIsbYwltoaXaDZgefha35X9hY37EDjGiMbg6aRzyvgB8Jsl93SSEBrkFM2q+3y/W4RVX7n74V6eqtF0siMZAIvmdafv/wCOx0lcNZmSx9yT9q3uLe3GEckiTTLFtuGuwYGwH34tlSKuCOef8gDwr7ZLdL4xY8f6VrPf/wDq4oPAH4Vixv0tUEbSP9K1vcLY/wDG/HnHRRItviIu/JF/h3wohM1TEkoglVDcb9lxcd+3PtjRGCa2I7M5rP7n34VY2VD03tfsBmtZfi/HMh597WN++xz68J5PAH4XKeBZY+mAazCW7ZrVOCt+RzIeP5x+/HSkkayIGPCJc2IIufu72+XB+BA4xiJjliakkY72jNwW9yeO2LRpx6B2Zz6/7nt4UZrsemhUuxcH61qkUD4D9cMIpv3PPwpysXXp9VAWsuzO6sj8/Mx0bl9TIIikz7XpiyFQdn2RcEmx7/djLLU0tPd2mVbW5LBvwvwT+Qxbs49CHSOZp/3PLwopAS2gp0DEBXOeVdw3uD+txjl/c8fCySy/oFVoWPA+u6yw7fy8dLLOlTIF8qplaQblG0BSB/FJxYXn/ZQMHBNz3GDs49CFCxzrF+5/+Fiji/WdOJXJG3cc3rTa33Si/wCYPwZR6MJP+x2+FtQ0p0VWsvdQM7qfjfm79vna3HKk846T3egKH3KeTc4sMQZ9yxWDDaSbW2Hkm/sL+/bB2cehPZnOsn7nx4UZZJZj08r90jliPr6sAW/t/Ce1/wCg4tpf3Orwq1xlaPQ1SHQhVX67rCA4/ZP6zuRzjpAbgNojYX4IIsfuA/HCOrMlBN9Np4XKmEmVYjdnAPDL7bx8Dbi/yxWUIrkT2ZAUv7nJ4WnVJYtB1/luGYEZ7W2HHY+vg/234xhH7nd4UnhVhoOqJ7HdnVbe/wAP4XHQx1Bl8e9zNDs3bGURk7Ga5AILC7WuVDWNrlQRhUx8uMJFUNL5m1o5CrL5iMTtYA2JBHwBH34XKmnsHZM5uP7nZ4U1POhq4Edyc8rAB8/4Xtiv/Y7/AAosxVdBZiCTZV+vawG/sPt98dJQbmc73FwebG3P5gjv8QfmBxjMqjzPJ2Mey9v6tvj77QD8dp5OFOCRMYI5of8Ac7PCjY7+n2ZAWFiM+qz/ANPB/wBjq8KLRkJobMrkcXz2rH/Tx0w7SAEXDW7XPce3v/rwRlmIIQ3NxwcLkiciOaR+5y+FEFFbQ+Z7m+Ge1hH/ALeMY/c4/Co0hj/QjNO55XO6n+9jp+9mUSCxUnm98XoYitviLX+eEzdiVTTOYl/c5PCgsQf9CM2IvtP+nar+/i5P3N/wl7X36QzS/cAZ7Vdr+924Hz/246bRQ6G5sU7D2OL+Zdp+ywIIC37+xFiOfu9XwNrjCJVGtiyoI40Pge8Og6lnpkdL5qcgXI1zwU/1xMJPppqDDuLfatsH2fxw4Z/3OHwpIqv+iGchWBN/r2f/AAcS64X/ACjGCcr+hKH4D/h7fzbf0fK+JAq4Wk8ser0E7wRwT8/f8+f6Mc2hWks9vSZ6Pj9BXwv+DT+5nLqfucvhQYKTpTN7N9m+ez+r7sVP7nL4Ud4QaSzkgi9xns9sdKmnRRsdTGxNlZeQo+WMktPMqu+z+E27Nvt8cP7ep1OB2C6HNK/ubnhVbbfSmcgsQADns/N7f3sYR+5x+FWYFoNN52VBszfXc/cDn8sdPpFceWyjeJQ9ze4Hp4H4KcJ5aeZolMe1HWJxe1rMWuD91uCfuHvhdTE1I8wWHTOY4/3OjwpvB9JGns5WGylpPrye1i1gefa/F8WTfucvhbjCs+R50qtYKTnM3e32R88dRml3TF2CFLpeHkXYMTYgcggHi3PwIHfBNRNJtQzmAd9ygHzUv73sALkc+3390SxdXky0cNE5Tzf9zd8NVZRNFR0eoMoleFnSohzZpNpHFysqsD/Z8ccn+JLwC6v6M5fVaz0jmcmpdK0RBr5TBtrMuQkHdLEvDKFYXZePkMerjQNUVUULwnYHO4jt6RwvxJvz8bdiBfCeICSkljrIYJ1mifzIZIgVMbWDKWPDI4uDcWWw574vSx84SvPYpVw6Ubo+f6siWGVkSZZUB9LqCAy34Nj2wnxL3it6a5P0k666o0Rp95DldPNHU0ayLYpFPGkoT7l3EfhiIcd1SzrMjCGDBgwAGDBgwAGDBgwAGDBgwAGDBgwAGPf7wupGnhy6WssYZm0Tkf2vb/MIb48AcfQD4Xhfw4dLGDA20VkQP40EOOPxnzUfb7jVhe8yTWkdVCrxY8k/DGq1HqnJtORQVGeZtS5elZWQ0FMZpAplqpmKxQr/ABnex2+xII7420i2Rwe44ueP8fdiJep+QZxTZ3Ra/paT9JZshLzQZVVVP0WOhhCXmqKVLCOWsABZZJWBC3VGU8ng0leVjbJjnrerOjMl1pRdPs6zOXLs4zbd9WrUwMIsxZV3MIJQCkjL2ZAdykcgYescccsbGNm9QJDM27+jtiBOheqqTq7Hn/UhMyOY6VzPPYo9L0uYU0Q+r4aWBYfOCsB5DvJ5rhCFZQVKgE7sTtHM0kayHdyLncdzMb2+/v8AHnD5wyOzKrUyqPLUtuDFV5sLdvlhVC8e4lh24tjAyMVaP0hrMp2/ffFLuGtMwKA/ccOg7IrJMyysiqR2PthNHHIBvuefljJOS9nieyj2I5xgJUlZ2hYKGI3Xth0NSlmWTypsG+xIJurNcfnt/tB+/EF9QPFF086ZZ5nOQ53T6mkqsk+hCoNBkklVDvrLCnQSINgZ2uoDW5Bte64myuKuN2y42nvxx7jHEvU6T6r646w1ZncBk09p3OdMV2bIxfzLNRTR01SoAAk8iaVJNhBFl3clADuoxuLm7E/ap8RnT3TCZJLPnNPWpn2aQZZDHRVUDtE0qM3mygyDy4kCnc5N1Pth3aa6l6G1PWVuU6b1Tk+aV9DHE1TBQ5nDLJCJATGXWNjYNt4J4PHxx5x6l1DqzU2ZaZqJohFT0+vq6jpHgyTI2hHlPVxI673SQhI1QMJ1VGZWJa9genfCXn+qs9r9eVuogyDKM8gymkibL6CjqI4Vp4p5EaakBjKsZS/2nsO20Gw2KFhfaEzdS+r2UdN48o+scuzHMcw1FmEeV5ZltBEklTUSEEu6o7KNiKrM7E+kC59sMvXPiw6MaHp46qu1xlNe8lfFQGnoa6CSoSSQ2YtGXBVU/aJF+PTu4xFPXHNspXrRp7R2ic+zem1rqqdKGu1HCGY6eymnjM8sNG7gxxyyiLcyqHcK733cLiCM5qNf54j0NTSdQ53y986rCkVTQ7UzSnqn+q5lV23eTEoPmBlCjcvBFsPjFIh1DvrSnWnpzrbOajTek9cZLnGZUdN9Lnp6GtSby4dxAfcpKAXHNzcE2IB4xfrLrH070Rm0WQaz1hQZRW1ULz08FXvjM6KyKzqbWsDIL/0Y568LmbZ9n+tdX1FdXalaLLMqyWilps3lppmp62RZZKnYIF2gFlVkB/ZN9ptcbjqvPqSl8ROm5dIZRQ5tWxaKzVjDU5maONB9PpbOJEif1elLDuee2NUWluQptkjJ4mujM1Bl1fkusJc6hzLMKjKqeTLctqqxJKqFA0iDyo2PCHf2+zz8cXaf6+9OtW6zXQ2Q5tXSZm1M9WEmyqqpY2gRlVm3TxoANxt94OOYNMSa1l1BoqLL2yug1G2v9Zuy1cb1nlsFnEsQ2hCwIYqCCOVB2W5xJ2mW6nt17P6f57lGbNJoWdaaTL6WeMRxNWpt3kzOhYP7729NuTza25KbbOpGayKhiYi3shI5+BUEEW+GLMoqI4A8UyqWjlYMGYgRBr7WNmJHta4F78Xwloaj6VT08Jmh3zRiSNvsq4+AJaw/PkWtiuaU9RRVSZg6unlSKk6FhYKB3HKkXHvuF/i/c6IDrWNlIgVtrSAso5uCL/fcA4s8gqCZEG+/cewwgiq2jooakxiRS++Lenlgxs1gDcD37Y2l4Y1VldXVrhSZEtce26/J+WGIiTtozWTMKfM0bcDFWqI2S3N1PJ/LGaKCTdPWrWVLqIgYlQblg9mtYHk/O3bFa6jSuUyIXSRrOpA5Uqfsj7z3xrcxlr6/LHkyxadsxKHaKlGaNHB+yTwy+9tuBkJpjT1J1Pg0xqN9JZ/k9VAayD6XlxoVmrfORZRH+tkCCOBrtGAN37ag84WaQ6i5TrHMc1yqmoK+jzDIqv6LW0lZGokifbuuSjMgFrH7W7ntjn/qHrmPMdX59TamzTSebfUkdFlFZSUSKXDSziepZfpMga8KpAzDgkKQATxjB0nefWOuNYw6Q8QaUGYNqR6tsvy6gyxnzFI6OmjEpilUygKt47hwAQ27aeMQmmU5nVsPlSMBHwx5Un9r4W+WFJgtx5rBw1j24a1/uv8AmPkTzhPSwiNBucFgAFKIQO3AC2uD7gd7c9sZnlCqGZla5Dd+CBx37W+f2fbvgLRNJqLUuR6XoEzDOcyho4Zpo6aKSQkB5JDtRBweWPbDKXxBdN4quvp6zM64VGU10mWVBgy+plQ1a+yNHG1wARuPHcWvzjQeJzLs0zXR+UmiyXMMzjy/U+UZrU01BTmWcU9POrtKicMQAGB/avbi2IRyrXeaaX1DqDLaLO85oW1ZnWYZ9TZbU9Pa6on8t1iWTjzkP6thFvIG0eYLXOKTKylrodRaE6waH129e+lZPpMmSVzUtSHoJIZBOsalgiyopceoXuCSebopAw41r6Wghkq485vlu1dzSMyiFw32HDLZCbrYFgDfi4xzx4cs+oq/UGt8qzGqzCfN58yp8zzFK/IHylIJKiFI40VZJHZh+oLIWNhckFGN8SV1lQxabqXTSlTnLzA00sVLTUczxRkbvMkWqIj2ANcK57qps49eK3VhilpqbPN+suhtLxZNDmecUzvnmZJl9IKN45h5zE3YkMR5a9mcX29jje0/UbR75pSacOpcpGYZkrmlpDVxGSoI5cIgbcQFsSR3B9Ldxjg/LPrPWvUKl1DR5LmWYZFpM1GUZRWU2SZETLX+ZacKqExTRKpHu63vyuJH6bapjoOqL/U2m9Q5xnuTCCCvyOfLcpooqSmqpA5r1swlbYrqfMDAOSeALYS3di09Tsxq9XS/mhllUkM1wfSbG4NiPvsOcNyTqNkY1U2i5ppkzZKVa+KKWBwlRASA7xSbdjCNiA5v6Tx34wg1zmGmzpKvk1ZTyVeSyosVZFHSVVXdCQFBSIec21yOB7A3OOGet9Do+j0JmeoMth0hRzK0cVPU0mkM7pKiEGeFXAnkl8tUYbbk29W9bbiMJfMJSyndOQdW9N5/qzNtIZT9JnmyIKldVNTlaOKoIv8ARvOb0GYLyY77hyCAQRh+0rwyJuNgDyAPhzjz9yDJuleTZnR5/qbJNK1uSRyGoloso0Bn0dZOxsYmikkY2aKWzvJY3vbn277yqoNXTrUiZisoDglCpIPNyG5/PnGaZNOTbNhsiItGRz8cWiOSQ7VkO5eAu0G4/wBnHvx8iecVDqreXtBJ9wP7cVkRI3Vlcjb6r/P/AGYyVG0m0a47EYKGPiOkLOL/AKFq3zv9Ob7zf8b/AIcYkcuFF1K7Lkdxwt+/3fdx/TiPp2STxHeYFA36HTcPYEV7Xv7e3vcfK/OH7I3lSgcBWF+T2P39/wAb7vnbHLwkrxlf0mej4/vhf8Gn9zFGzehYMATyAVxheInYTIRbvb4YEjMguXsbXFu39HGMhDJtV+bjGhs4KLZFcAMGFwePTfj54xqSbsLA3ufb8eOfy5+YFxi9xHexc3+WLWCbwiHv3wmq9AZXgAmRUAA9VyO3w+Fu3tt/GxxYSoe6hh6gSvJJa3x7g2+Hqt724xnlBUAHZx2JJ/1f6rH5jkYTO/7IRWA44A7fiLfhYj5E84QyARFIZkKqlrqoAtYnt8Nt/wCbf2vzjHMCNylttzcd73+B5vf5/a/DF1njtJt2X9R78H5c3/p3fO3GME8hlViVACAi3A4Hc/C3P/N55F+cLk0RNeQzyd/dMMkGW+ICnzFKdkXM8kppTIdtpXR3jYix9toHt27XvjknHYP7p1nc2YddctylliEWWZDCse2+8GSaVzvvzfkEX+OOPseqp+bj7DkWsGDBgxcAwYMGAAwYMGAAwYMGAAwYMGAAx9AHhiC/5OHStAbBtE5ET/6BDj5/8e/nhik3eHPpYRwE0XkK3se/0CH4Y43GnalH2mjDu0mStPyjBL2PAPy+PxxHOtujGT9R6wR6yz7OsyyHzFd9OipSDLJdoFhURom6qXeA2yVmS4+ziQZCxjAVST6mHpPt2H4/6sVUp6SHU/aNhbkAXufzx51VMjTRveVrcg3V/h2q6nV4170w6hZn06z6eOODMzldJHU0GaxIAFWejciMuAthICpIspAFiJh0zR5xl2U0tBnubR5hXRxbJ6xKVafzz7nywSEB5NgTb4nvjYuArMpJXsSNpsb/ADwKeSPNK2H2fcDGh1e01ZWLiuZmiWMElF2i/a/GLC21wLFS17c98WbU2O4JspFjtPJIPA4t8PzGMSygSrStuaUBj5aq17A/G1vccYfdWRLkjNsZgXDDd7XbjFqqN+9+TYgX7L8xjEroxjuRtUKAVTgi3f8A68Z0kdlCmPgXF7EY002hcmhDUtI423LbQbHsfvGOO+pHS+t6ga36vZUmpM3yTJpJMg+tafKsuiq5szpPoTLLDELFo5CwA8xTxzusMdlykrvUROSAfTtHOEirDNCZWSVhIQG9JVyArcbiDa24cdrDG6lKwmdpI86OqvhY/TfVGlNS6r0KmkMv1FqA5Nl+SZTSKKrLqOSmqpmqqtgrbql6hUaxNkFkBa+/HR/h4yrNdPz5zobU/SdNLZ1RyCeqzzKqGNMkzxpP1ZqYHTjz3RF3ROQy/Z5IxPclCA7TwiNQUYyqkQAkBYcWsRY9j7G/IPBxkSoraGYD6qQlDsdfNf1qCTsFjusosBcbntcrYnGvPoUVNS2ObPEpHnOU9QOleb5TpnMs7roMzzqoGW5YNlRX1X1VMIkDHjcRsFyd20n4Y5/1VkGlkzzVmUdZJaDI89NLk+c5HU/om1bSQ5gaeN6yVTDC3nq04ZJY3ezrYi9segVTmcMiMtdkx8ryzHzChADElgCLiLkWHft6GAsMUOY5ZUnypZU3zWJZ/SxYL9ohr2ZR6bA8/mMPhK4SpPkjmnw75zU5vqPMM80f05yfKtBZp9KjSuhyoZXUJPSuIoyY2jSSop6mOUyAj+DcSoWUEAv/AF/0erdY62y/W9NrnP8AIpqDJ6nKGiy1YTJMZZldS8kySX+x9gIDwLXHOJhEeW1LFjSlTIw2lWDqjFQLEkEMgA7HsWNmjAti6SloVMhfzIAV9asCxQsAQCLE8m4VbXbaSVP2saoq4tJx3ONdOdBMtzXX02SZv5erNP6LGbVlTWZzSq0VXmuaVAmeF0QopEMKpukQqoeW3pvsC3pYdB0XiJzWXprkdJloyrShpc2pxQ1dAGqTXKY4m3qpN1N1KooIFzttbHVuYZLKd6U8EVRLEys0Z2shDgEsAO+4BD3b7IuCecN2TI2qp5Myp7GR5UEu5wZ5kDAklmBVmALekXbt6j3xpjFFlJCugo45JZqbMJY2g8tE8uoYuU3MwUl/syWC3vewBBF8bGpNbksCRTRmtpIiJEp23SFYiLFonT1kfK7qPdU7YQRUE9FT/SGV98wRXVnJMltxB/aZblhe4HAwsSOXLVMdFLM8Uoc/QaiTchcklipHKN73B54uDcYYo2HdpFLTVifyFp8sljoPLIR3kofJaE+Yt90Y3qihvXuBG4ngcHG0pamWoVKmMTGO1/Sp3m/Nrbd3FxcMbDnjFivRwVTyfQ3pI5LmVlkIjKv9lnVAVPHqDbL8m5GLaiGpoW30bQSU7NZIHs0ctxf07SL8du3f2xdEZu11Ytd6Wnjjq8yhlWFibMSBdj8AAPfgWHcHGmSSGevlVaWNIqgmSMMn/Giy2PquO4vxfvhNldPLSSNUV2ZV09PK7tFD9HhWSmVidieaqbyitfbuvc3DOygYyVbU5WGqUJTwLMIwR6VTduCkuTz+xzck25572tcvGnuajMNE09JpzMMo0XNHpmpq/OqVqqOljLLVysXadlO4O5J53A3BIsOLs/o30Yg0bkFHJq8ZZqHVRrq3N6/NvoUabq6scNKYLoPJS21QACoCgbDzaWkJIBedn/WMG4sfkL4uhBBYb440upJLjgBbcn7iD92FqFncXKNjHAkwVY1YOApTlAqkA/AWsPkCOfccriu0sTJTlmk80MOx3SbeD2+BPt2/YPcKoaNXXy9oAS/mFjtt+f3qf5w7XvhNI1HSoCQ0gsY9wvYte9hwb3Hyse9j3xNmKbfIinq3m+UZLBT1+bdS59KQHzI4oKdad5qqU9mSNklkdweNiAj1KfliD9N6D6haw6g1HVan1DqXTmUU2XVOS6afM0poJ6l5WBmqZojEAtOWjhXy3BkYLuKbrjHVGZ1U2YSrNFD5UULG8tzEEDiz2vfj0qLjkm9yMCxQ01UKWOYOsi3qqTz03ywqLCVFcEOUNl2hhe1wzd8Q4PmW7LNscoaO07Ux9aZMr11qLWum9d15paqZKWrpXyzPIqNGWJY5lpVPlje5aCVYiOPVuXHQetdJ0PUPLIshrdR19DlckhWrpqCqFMuZRkfwTyxLvVbi5RDdxe72xtostC1cVEHVmB/0caj9W6HjeNzMdp/nru44xt5cpolpZaKrj8wRhiUmO29/4xbdtv8AyjY/yj6sVcBvZZVqcO5H050VHqbXWX5dlPS1FodUT0VLBn2TT1NRCPKiVb/Rm2LDuPpjXnj1EsMbzw+9A9J6tzrVOY5jSZXTZplerIRl+ZaVElLHTtHTw74wTIJooWYkyKeGa5Uc46ypckoGhQwUccJu8rlVYE3A3yE2/WEbVG5yRxZjay42uWZVldJ9PppcvhImqPpNVGYB65BYCT1i4sqryCCD+1fkolCwKCuaLWWTUn6JTrqLUlVp+lnZS+bUGY/VzQ7XEgEVQxO4EDkfaN+RfHNOe9M6zrxneXZDkOtOodfoTL6ta/Pc6zbMXFPXiIgxU9Gjxr5gEoDGYqY1tYGThj1tNHLSQvAKT6zpHcSSU9XEspkC3AMm5T5pHFi5Yj2PvhRA7SKZmgkppZE9Xr38/LcWsbcdrD4HvjO9C1Si272OWM4yTOND53Fpvq/rXqdS5BnNWKWk1NlGo55KL+EusNYghElLLJwrOt0f5XsOscmVaKkipJJJGEaLHukcO9wO7MOCfn74x/RKCqDK0SGKaxZHj3dh6dwNwwB7A8qOLnvhZCzRL6kCg7g1hezAXP4XOM0tSIU3HkL45I0UWlv7cjGORnlvt/P/AB+WE0dQgdWYEkElLA8/l8L4bGe5rWajzB9F5BUtDAirJnFbE9pKaNrEQxn2mkBvf9hLt3KXyVXl1+b1nQwOEliaji3lileTe0Vzb+xJbttJasb1ZpeXXnUCXVWR5/nGS01Bl5yefMKBor1pWXeYoxNG67I2JBe1i1wPsnG2m6aZ8u5f33taN73D0H9H+a4eOX0+X5ZQw5TlcUUNPTxinhihACIv7Nh3tbGSWpYPaJw20i/B/txz3g4rWW73s2jtVvCDEvLToW7OCUY5oQk7Lq3FvXpey2WiQzoOm2eSC79XtbrtHHroB/8A8mKt03zpX8tusGuCR2IfL/8A4XD2lqY2U7HUsbBgpuV/1/ljG8rLJeoJYKLDapsAP43HH44pLDQ9fzv8RS47jvi/R0/yDHfpxnHmWPVzXHPxfL//AIXFP3uc8VuOrmtgv/PoP/hMPQVAQlEDbyxFjbk4sNQ7J5jjaCdttp74VPDRe1/nf4g+O45ej9HT/INF+nmbhSD1f1uSe3roLD//ABMY/wB7rOGHPV3Wot/KoP8A4TDzeRrKWAA5Fyrcff74qHMgs+4AsFBsbEm/b3+H5jCnh4x3v87Bcex3xfo6f5BkP04zgDjq5rU/fJQf/C4wydOs2ELOOres7r29VDYN7H/gvfuPx/N9SNtXs9tm++08YSyyl422s9r/APgzhEqC9fzv8QlxzHSi7Zfo6f5DyN8fdHWZD1wzTKsxhkzOSpoKOeHNq+xq3iCqODFsj2hg6fwY7H35xy9jpD90AqnqfEvqG9f9IjjpaJI03E+QohXdHz/4ws1vnjm/HrsPTVOmlH77/eeWxmMq42p2la10raRjH7IpL5dwwYMGHGUMGDBgAMGDBgAMGDBgAMGDBgAMe7vhr0ZR1vh96ZVBzXO4vP0bkcrCHMpY0U/QYbhVHAx4RY+gPwumJ/Dl0qidCR+hWRD02vc0EJxxuMq9ON+pow6vJjoTQmXJHFGud6jLIxkVjm8pO9eUB57cnGE6Fy54HRM/1LtkLxSkZ3UjZYWHG75YdM93DMqtHbsfb7+MRx1W6xZJ0pjgqs607qPMIqnzhA+WUImid44ZZ3RpC4VWWKCVyCRwhHcjHn408ztE6GVWHC+hcueMRvn+oXVSF3HNprkD53xWbQFBJIwOfapRU7Fc8qO3H8vDTr+uGR6S04uea8oMwyKWOmkrPoEFNJmE0lIoJMtqcOnCgkhWNrEe2Hvo/WGWa403lup9PTGfLc2pYaukleNomeJ0DKSr2IurD+jGhUnFeUhagriGo6f5X5haTPNSx3tYLnU1gAe979yVXFp0NQRGm2Z5qS9K5liU5vLZ35FyL3bh2PPwXG+zGuSKnlqvNSGHYTM0h2psS+5ix42rtO61zweDbHO83iyocj0jFqzVmi84paeshqc0y/6BHG5qMqFX5FNMTNJGUeRXhkIAICyqe4sNEad1oTkRMyaIy55CpzvUapKziTyszlvZzclVB+OKx6DoUdmOotSSkAxlZM1mXj7r9+2G30r6tjqPV6hoo9L1OXDIKqKkSoeoFVHVyPTpPaIod59Mq349J4OJND+exWMo1hdAT7e3PN/vw9RykOCew2F6fZdEq7841ED7XzibkfPnCcaHpUnqzHn+o1FaBZWzaUpGFP7Ivxe2HdKYmsy70YCzKw9/ljTZ3nFFQo81bUrTQ0kb1VVNKbRwQg8s/v25sASfYHGqnqUlC25p20TQBin1zn7SEOxVc5mBNyTfg2AFve3HxxRND0RMrNn+o2VwY0vm8tlQ8mw72v7Xv8CBiFsk8TOcZlqCq1ZnVFBl/SHMJly/Is3aIxTecCqLV1e6zLSTsZRHL9gEDftDAmeqDMo60KUlUiWxQK19ykXBHxFu2NaTKJLkzVtoHLjeRs81INwsQ2cykAnj0+w9va3wAwkm0JlxiFPPm+opIrnaXzOUklhbgctb3ufj7jjDnd5FXckUs3Hp8tGb0/O3bEZJr7PJuvv73sf0VsmptLR5xWRpAGqJZpKySFLurXCqkfa2NEUyb5Vub2bp1lxVjS55qSnclChizaVdpAsT3HPf4HngjkYTvoeoEDwfXmeTKpKq/wBayCSRSm2zHtdQXINrX/ZvyHmahEIgnUJJ5ayMJDtbabevaedp9m7H2wxeueu6zpt0u1HrOgpqVqjJsuepWCU2Rgp5v3Ykj2AP4Y1wESnrqKP0Xy+SnehTU2paeomcF/8AS0o2ydxsbkBQOLC57YvzTS1JWVKVsue5/BVfZSsWulQqL7RYkhGAN1IFvVcXvfHPmY+JjOhEozPP+mhl8sctBnoCMS1tv+aAEjaeR8e3th0dCesGreo+a6rNZSZDVZZkldT0lPPlIqPMfdAkj3+kqrCwZRukVSwAszKAuNcGLlJPRExtoN4aSUVucZ3NGzAtIMxl9RBH7VwQf5Fzb44INCUFWqw0erdRxBm2IiZlIjo+x12gNyp2uxK3JJVNoIW5cWU1weNnQPuQ7XQBonHHuPtg+xBuvwFuMGaU2W35pWqhKrIhjF5InB4uBZXTj3Kn3Fu2GpomI3KTS8MNRFl76lzyohiQtRxpnLNEQeSNgBv+Kg/E4Ty6TgiLRSai1GpRPNgYZq62QDmM7rcWsQRfjDilSCtjtFUTybCQtLJt3RMAG4lQEEW7hiALfHvjrPpuYjy8jejneJTKXWp2PG7KNjhSpvEfc2BFxYHE3Q6K5jfyzQbgmpoM91DNMBLG0BzKVlkAuCCwuxAHqX07ee+EdRpLz41MmZ6jhgjWNJEObyHem4txY+xPA3EW9sODLq2qWngzCupYqZKqNZquFQWWmnA9RBFgyX3AnYpvwSDhZPJBPRkSxrHvNiWbbt4HNr3A5Hc+4HcjEZrbD1NRQ34tJ0biaJNRajElROrlI82kusiBbKlrAXDrfsPa9yAc02j8qyulYz6iz/aAS0f1tJa1zbdYcW7gL94secRlL1iqIc+1TS6dqstlyTT2Syzxzzt5bV2Yx7vOaMWO6lQMsTNYXkJAFhICs6e6+1NqHWlZkuscr09l9LRUGW18CwZm8k61FQHvC4kVSxEcZHub2vY4M3UU53d7D4/RpMwhSOlz/UrJHbafreQHgcWFgLc37jtfgkthOnTygd/Or891A8jbtv8ApaVVA9xbt37i23sbE8lzTTiOKaTdGpjA3lmIDAm3BHF8MnXXUDMdKLktHQ5HHUxZ7Wmh+kHMY6JIZhHJIpZ5DwpEZG63cjA5WLRrNK8UbVtI7qdEotRagXyUtFImZOCwtuKNbaQTtNmc8jsThDUaNrapn8/Oc5UCS6hM1mUBlQHcXcXbaDa27kep7XxoenXVebVi1cOo6PL8uklz2ryLJp6LM3r4sweCP9Y6PsXbIpMoEbX3BSV45wq6Oax1nmuhsrzTWuWwSZnUiZWly+B0gMazuiN5ZJ8tiq3NieTcEG7Yhyb2LLEVOVjax9PqWpMFNW55n0xG9zG2auytY2X09gR7EHi/vjHnWkM0oIKakp861PXU5njjKNmzs8J/VndvPO30twQR2sR3w8aeGSOoNfOjAswAUqqlEHBG3ixv7fDtYcBRW0xjmSmzCCQwVClWkLEMsgPpIJ72A+ybA9hfFJSZZYirzsNaDRkbRSSfXmejcZIz/pKVAOwFgCfYcE8AWBF+cX1eg6aRbpn+erK5ViGzOY+gc24P4d782DW4w7qujSniEfmyfZvGzROjG3BNiBxfuR6Qb3ONPLIZSI6eseB3UFDEPMRyoBN3W69iPfC3K+hZVJz6CGTQtDIm0Z5n6pZjYZrMvBN7ADgf4+eLjounWXzkz3USWj8uy5vIAARa/wB+N2lQ0kCPEB6fQLNYX+YOFUaK8b7ywkPBtyL4y1C6xFSPQaUuhKaE2fPdQ7nUbWfNZD2HN+eMXfoVSJC0q5/qciQsQn1zJ3dQOBb2OHQIYd4CliGFnUG97f4ONLqTNzklJFHS0rVeY1biDLaNZAGnmPsT+yoHLMfsgE/I4pzVNOT2NGGlXxVRUaaTb/e+yXNt6Jasaupsg+iyx6c0/nmeyZ1malgz5nNJFTQLe9RKLj0hhZR+2xt2BI2+S9OstyigNFRZ/qJhK7VMkrZq4eSY92cgck9yfjx7Y2ultPSZLBPU19QKrNMwb6RX1ZJs7/sog/ZjT7Kr8iTcsTjbrKqU4OyyMjMS5+Bsfw/lH0/E9sZI5nLtJrXl6v3zN2MxvZxWEwzTinq/Tl1/pXwU/W2k20m+dEUaSqItQajBUKLjN5OQFtccYwyaIoZo1imzrURIVV4zaa7WHfvbDpK/rdim5+yo7X+OLJlkiJiZhYi9rcfn3wupJmRVqkdHYa8ehKWCUJFn2p4rofWudSFo7jmxt3/sxX9CKbyIqb9IdSbIovo9/rWViy3uSxv6iT74c8YBiGxbfHm//XjJYFQoAFucJlJk+MVF0Gr+gNFJd31FqXeWJumcSjbf4C3Hti09O6J0LPm+pSAtr/XU1z88PVPJtwFAt6tw7nFJQotaMbGvz2xTM0VeJm+gzY9BULxpGc+1EAjAkjN5mY27A84ufQlHJUNVvnOobtJ5jL9bSgWtc2F/iq/04dICt/BrZTybfDGIpue5c7PbCXNp7ErET9Q05tC5cwuM/wBRMwVlKjNpbcm+E1RoSgRpGOc6lCbtxC5zMODh5TKqG0YvfCWqCgDcO993GFyqZilWtOUGeMfjdpY8v8S2sqCPzCkD0io0jl2I+ixElmPJJJOIIx0P4+kgXxR6v8k3P+Z7+COfokPxxzxj1FPuL2HAerDBgwYuQGDBgwAGDBgwAGDBgwAGDBgwAGPoC8LjqfDp0quCbaMyE27dsvhx8/uPoC8LcgHhy6WL/F0TkZ7f+Qw44/GfNR9vuNOGV5Mk5oSUvHxY3AHJHy5xyz4h9GjWuu6jK6x80kgpNOKwp6annqF8yU18UcphRlW26Qlyx5EQFiCcdUsTJ6IksG7/ADOGjnXSzQ+f51U6lzfTVBmNdUUUOXvJWwCcGGJ5WQBG9A5mfnaT8zjhUZqErs3vaxy50n0lHn9RWdOK/NK7J8mrZdTzZpkuS1k0FLUzR19MqIpcNII44pW4RgCCbjHTmjdaac1DVZ1kOQ1q1E+la9MnrFSVJPLqDBHIEDKbD0yAcdmVhxYYbR8NfRmTIIdK1Og8snyylzCqzGCN1cPBNUyeZP5bqwZQzGxF7WjSww7dE9OtF9O8sGS6J0vR5NQSzNJNHRR2aRnI3tI5N3ayrYnkAfPGupUVTYhIQdTNOUur9MVeXZvmVTS5T5TvmcMDBXrKJYz5lN5n/FxuQAxUXeMtHdblscn6jqKrUGg8p07UZDHvi0pk7aZlopqyeqq4quNWZfLgjsBupBYOwv5VxfgntjNcvpcwpKmgzGCOSCceVPGpIDIRZvuNuP8AZ3xqck0Xp3SdDFBklBDSCnyynyobSf8AgsG4RIxv6lQSP3sLsbgm12UZqCVwaIL6V5zqbT+u8n0dR5NS5dprO6bPc2raStpp46yKalqqdPOWV5pPOWc1EbkyABFAACkbR0dTyhlJmkBZTa4uC3z5wzMr6QaLybqTnHVbL6GoGos3pY6OoeWqlkjCLYs0SMW8p3KxgsL7tgJBJvh7+WNxZ1DM1yCOx57/AC5vx/t4dKopy0QLQwysvPIueVZvbHN3ieyOPPIocm1tBqvOdIZxJDRjLMjqaSigFa7hENXLLMjTMb7kThAdoYEkX6TlU77CNTcW9iP8f7cRT1i0drbXmT5voyLKNK1OQZll5pjJXZhVwVSTE8Sp5cThGQhGRwb7lUkDbzppOwuburI5Ozeo1PqbWuedLs+yPrTVaefStPJLlq5zl/0ndJUujTPJu2spWNUCqd4EZXag9WJK8G2SZdHBm2b6Jm19QaHpGGW5ZlOdZ5T12WTMHKStEiszRMrryhbYDxZiL4WZp0A6y5nnuaZxUZppiNM207l2maqohzKrWrelp5neWQTfR/S8okKM4HAB9ziV+lGl+oekayTJq3I9GZXpDL8vhpcpy/JJKuSop5lNtjtMqo8ZUDgruHwIF8bE00Z4xad2bfVvTDRGqMwi1FqDIlq8yMT04kM1SDKFFtqrHIqlgBz3KgXNhY45GyzS+kD1dzTqv9E3dOY82punyn6wqGgV4wdteskc92jWuCw33Ff1hNhjqzqdp/qHrGY6L09WLprTlVEq5pqGJvPzCWE96agThY2HvJIfTc7LHnC2DpXodOn6dMoNMUq6X+rXylcq37k8hls1mAB3dzv+0Xs/B4xoi0EjV6a6LdOdN6oh1bkOlo6TP6JZoIa36ZUyyoHN5FBlkKtdiRtIJB4suGf4ms509NojMOn2Z5xQUrZ5BsqBU59DlclJTswcTo0oLWAjblVY7Tbvwdl0t0b1n6f6kl0bnmbZZqfp7RRqcnzerqJY86pVTmKlqBs2VSRj0h9ysbXO7Dw6m0Wp63JEoun+S0FVnNazQLmFY6LDliMeauUHmQqfUqFSrMBuK3IxrhqKnFs4s6U6tl1d9N1brPxOnI8sYy0WX6dpNZqamlkhmdA0lVIF8xZShkUIm0o62IPGJA8KGttI1OWUVVm/UPL6vWOvpUzZaapztGnmmjhMHlgizB0jRQTILixVDYC8+aL6NaZ0ToHL9BZJRQV8OW0Zo42qadfNrJWVpHlZiLB5HYsQd1jwoA5xrugXTmHQXTjS2SZrktLHmmV0CUkrtSxJIkiAgvvVS3JPseOeThsdBLhLoSj9BjKIi2gZTaN4RtYrfgDv6Rb7P9JxfS1FTBMaGZCtVtbbt2qJ0+IF7H8x+OFdPNAqeYG3uDuXcSeT3se+Eta9M5KO7RbW3UzNAxTd7gMAWJP5YbEbFNbjer6vKqrMKKUZnW3hrpKWmjUTRRvV+XZo+AeLAmwIQ/G+NRT1lJV00E2W5tWySU61qUQqhJMh3x3kWQSJuUqd2xmlUWHpB4w8Z3eriphupWpmlMOYRyH1BT2WNRcCTda5+1YHjGop5NQjctWcshq1ebz/AChKDc2WAqm25XbbcbG1uO5xaSNCNLS1un5srlq6LU2bxU75Oane1TNHMkIcA1JZoyyyqSE38yAcbfcKNQaoosky2u+jGepq6eOJWoo1HmSGRbQxgW2gNcjhri5uBdlZTtz+kpaqoSOgkPlLKscu9Ylr7jdGx2FvJtuIHIv3txikp1DVV3nSpRzQS1ETpE9RLGxUXaV2t9lluPLHZux2jvEQICyvRmdZhmXULNc8+o63UcdPQ0iRswhpMspDC0oy9ZXJBhDO194CTNu3hVIAydDI8q6p6rznXWf5vRamkyuWkoMiOa5Pl6ZjlpWIGeqXal44zIX8sn0FYfSSDfExw9MdPT12eVuZabyyeTO80SsqI3DyxzvHAIo/MieyBtiglQpNhu3E8Y1OrehvS3WeeZdrXU2l0qM5yZUFHmMU8tNVJEvMQMsLqzi5YWa4s3AA4xLVykrt2Q+qkJmS/RGdpI1/WMnBN/huHc/d8b4i7xBLT6i03H00ocwp4Mx1KVjjlkRCcupI2LVGYgkHyzFECqE7SzSAKQLgS5k8DQ0kZLMJDZvU7yEACygs5LMQO5JJJ7k98JqnTWTzS1UzZdTSSVsYhqpZUDvPBwdjE/sKQSoFgCxufbEyTaJl0Rz50zynVGuM6paw6iyvNcu0rretoaaRKeCCSopo6J4NsfknyJWE80pLcbViJNzYmQujFVDL06yCpKiMSmUC29NoSecG5JuoLKRY8k42o6NaSptIx6IyDTGW0WR01IMtfLwrJDEjt5quHU3SQMSyudxDMbBDclVonp9T6H0dlei42EGT5PBHT3qZRJI4R2djIbkyTbma4JsD+03fEQiwgOFUaWlaJZxUTuAZnjViFuSe9vz5xSrq3qJyjIJVaURhrMW3nuVYMNgH8m1+xvi2mqMuWnaalWqeCBkWq86QN6SLeakin0/j2wqCSRpDU5MaarhjMllml9TqW4YNYCRtth6bkH4Yqxq1CKszfLofKmpFmpm5Y7rqeB+tIBDAi/G0hhfi64TfS8k8mSKsy+ajqGJJpgtvPBUDcQlkJ+LLYfJe2FS/WdS1kpKemiYFQXXsnfeb8XFwLH0n35wSUsghaKmnffHaRDISwcnkjnlQb+17+5GE23LxcYlY1TMKlIKWGOOnjbbGig3cgdwezD5i+F6qnlPsYML9x2J7cYTUuc0tSDT1A2tG4kaGSMAWUdufSy/86/HYDBWZ7lVJC1RUeWsMSNO06y7VhuLsSWNgo+JOM9XRXZCi5u0Vdsx5vm1Dp7Kp82r5Gjp4V5VE3ySP7IijlnYkAKOSTjRadyXMmrH1VqSNPreqRoo4VF1oKVrHyVPuzEBpG924HpUYTZGlVqnNqbVebQyx5bSEtk1FILE9x9LlXtvYfYB+ypv9pjZ3GVQgFjZB3tbtjmeeed93l+P4fOder/o6m8LDzku++i9Bf+b5vyVonmsLEssTElW7+x3Ad/6D+eNdm+YSZeIaeiieWvq2ZaZAbsxA9UhY8eWimzE3ALBbMxBK8yxswLkBVuT/ALMI6bJqb6xnzSV3eWYRxDzG3LFGo+wq/wATcSxX9ok3t6SId1qY6bS1YkybJ3pA1TXVUlTU1Cg1ExYrGUX7Kov7KL9nd34ANjxjcm7szOpJbgA8f0d8Zbwsp2xLGT6nbcSxPxue5+HFrdwTzhPL5YKrGosF7sP8f04zTZaU+0dy8R7lAdVDWt6WPH54yRJHtBs1wdp4xRXSnUoEBP3++LkAMV1AFrMTf44U9SuxddWBAK/a2i2KlC3O7lSFA9vnjItlj27iATclQO2LRUojFALgc3Pe2Fz0F31LGIWRnA+0LWHsMCAMfLVRwLknBI3mApEPa5xjASwtEytbk3wqWxNzFP6iR2PJFvhhDPa20m7EcYVzSRRwPK7rtS5b3sB72HJxppK1qqKKanYCGZS2973t8bfP2xnkmS35LR4+ePBpG8UWs/MkZtslIoBJIFqSHtjn7HS37oRliUHiQzedYGQ1tDR1DM0m4sxTbf5cKOPljmnHq6Xm17DivcMGDBhhAYMGDAAYMGDAAYMGDAAYMGDAAY93/DPqqvpPD90ygXRGoahYtF5IBLCsBSQChj9S3lBsdo7gfaHxx4QY+gbwuRhfDn0r4DBtEZEbnuCaCn4/oxx+M+aj7TRh1eTHIdYZxGokTp7qNyfYCm3fEH+Gxjn1nmyw1EzdPNRDylZ2Xy6e7WQMbfrrEc2v2uCMPE0iqx9SlwthfksP/lxhBW1FHQ00tXVzRwQRAyyySNtVEUX33B4A9XPFtpsRa2POrc2ZH1G0dXZieF0NqC9xdPKh3KT7H9ba4+WBtY5rTKWPTvUjqe9ooDb5/wALhzRTUc8ENVTVS1NPKu6OSB1dZEIBUoVJDKQbjbwfYnjFlXmNDlsYmzLMIKWJmVGlqJvLQOSABckfEc9hfnnGiMktGGR9RuTasziJoh+97qNr+ssI6bnlB387/wAZ/VPwsbY9X18ywfS+nmpUMrn1eXAbH2+zNe/w7N8LDDqjMMgWOOxBcAXa7N2PFjz7/kMJMzzegyKhrM1zXMKfLqKigaqqJ55NiQxKNzOxP2UsL3Pp4uQTh8U9wyPqab9Ja6EqI9E6hsVLCyQC9uf/AAnvhNDrXM5n2t0/1NGGG4GSKntz7fw3fDmSrpMyRqqCqjqI51R1kEty8ZF0YHsdw7XNyMZQpJNrEt3UvwB8rcYdDcMtkNM6wzQEyydPNSSHcyX2U9zYXvfze3OD9LsylqJaV9CakXy/sk00NifV2tJzbbzh3JCERLWshVSDfkBjc/O4wnlZQJLurxsAXIXcU9YF7bux9fONkItiJOw2KjVVfDG7jQmoTZmC2jhuSCQCP1l7kC47G3aw4whqtZZv5U4g6f6hKoxUEx021wD8fNtt/AL8RfvuJNZaejz/APRaprqX60NGa5qEyXkFGG8tpSp/4vzBYsfTuHPqwuLq48tpLyRt64yT6e9r87rkjbfg3sN3tjUotbkKWYj+fqjPSyFavQepaWSobymQQQlpH9l4lZh+I5HvjeUGrswkMWzQOo1T1CxpY7rzYdpLWPsb43k8EKS/S4qdZHN181jZgl/s8Aj8LW+IvzjTjW2iIaWuzGv1LkkVBl1WaGuq5a6EJT1AIXypST6XBKjabnkd+w0w1JaMc+q68otQNA6jZ7BvL+jQM57biP1oJC8hvZSCDY8G2TWOYSMKj977U05MgKWp4dxKx3BBEl782X37gEDu4Y84qqMyfqZXhHraohXeSBcL2PJIZjwALez/AGsKFzGnrYxWK9ERcIz7m2OAxY2JO21+LHjjseDjbTWhFuY0Tq3MI4Z1qNB6n2IFUq1PCxZWbbuFpNu24542/InGGt1bUiSMrpDUDGRQ29YYvQSOx3S3+AxtqzUunfrtcjGd0iZrLTNXxUjzkTSU/CmYIW3FVY2JF2UMp4U4unlF0IlBTgIEW+7jgAj0nj3B5FsNQJpGsGs6+BlifQWqHPAVlpYCD2/8d88Wz63q/MZzoHUzSpGrF0p4SQCjHaB51hyoHfu6j3w56SLdCE2R+oem7Mbmx5uG/wCbjKiNs9KNGbBWs57j4f0flhsC2ZdCP6zU7fXtE1Ro3VZ855TDLFStIKdlj58zyprhSt7bm2+nsLC7dXNcvimoly3Qus4ots7rGIJPKO+SIylgZiFa7kqbKCAdrHjEtVcUEklEFZGeKoSRhu3hC3puwvYcOeVKn5jDah/SHzY6WTMKR5DTTCSMKwd5S1opFbzWJCre49ZPHa1wwmM10GLLqDLqpURdF6yMceVKhIin/WUwAIjF59zSk2JUEy9+Lc4UvqqB62skbp/rf/vWAmKlmJPq2hoikxtwAXMZ5/avh3vQ6jRKmAZplrMtIp3vTHaakqAZyGfYYWINo1NuQdzG5Kusoc5p4KmWllpbTywNGr0bNa/8KGIa7ErbbYi3xOAtnXQalBqjL6iWaN9M6xozHmxV5DSyBJal0uUUmTy/L225+wG7i+Fsetp86AmpdHamjp0eRp2aiETXQkMu2R7g7gQFYgt3BYc4cYbM6OnjnWeiWKWrJBSOSNjSrHtMHmBz+svzyvH8Q98KcuyvOKaSm+l1FE5heU1LJCd7KT+rMfqNio9J4uTyCn2cSmkSqi6Gjk1vUxU7zPozVMYQAlfoUe4X9gBIcYKXWdTNI7S6O1PGkfqDPTRgcWAH8JcH1cWsfmOQXfKshkZZbkxi73lcWt7cdz/twjnh+kUEtZFHvEreZ5YYksAy3IuRe1u5t7c4sRGcb6jZ/SCuqEhr6LTeronkvK6xUQ2xt252sAhHvtIvbnthJFqGoqxPJJpnVVR9HaPmOjhEaC9g1lYqrcci9h7H3w5KmdKipWogm2RGIxseX8odzdQL9vk1+3JGLoIqU1iw0sjTXSNmdUuCoNw12jvzccLyOQQMSnYaqkI8hr12qZ1q4q2bSOohBYw1W2mp2tEeQxJlutyBaxGLBq/N8sZKik0TqauiV9ghky+N5EUlBcEyEMPWDZr2twR3w+qmBYV9IaTcSOC/qvwTcc/DttJ4v7YxUmXrDAQsZu0gjZzYsgFiOLAknaOLXPe3F8IkXVeHQ0qayrKiQunT7U0EwJK2pt4Kqm7cLybrkHgXLd7EDFZ9aVBtEmhtTOg/g2ShjC7L8NcP29rjjDi+iRFAr06LEu662VgFPB5sRs4F7+kEc84ujgjc3uDIbA8Wu1vha4a3863Y24wqTF9tG/dGdNq6ZmaOp0Fqh9rgBfosRKsbWH8Jc/d73+F8NqXUR1zmcNLFpPO30zT7pXdaBTHX1Mb/AGSQ9vJRgQR6lZuCQFILrz9m1PWSaPyKpmioaaL/AE1XxEKYoje1NGb28xwPVt+ypvbcyHDly7Lqekp6bLKWmihp4FEUMaLtVNotdeCeLWuLkdiQLDHPqvxh5V3Vv6309nXqd2jWhwukqtv50lePxE9pf1NPyei8rdxs2ItZVVzIukNTRptFy1GguxtdQFkvcX5Htiwa1qpFdRojVTLHezChT1H/AM/DxSljTzFYqqcAqnAYjte3pNvmCB+yFsLWmJC29lcsTYmwP535/pwqRy3WpvvRGaNaVM9qYaM1OB9p2aki4N1B/wCM/lH8j8MLqfV7ywySnRupRsYAp9BG5gRfcPXa3zw5jQRxwFnblhY7SeeGHx/lX/DFRTUsRVbMAGtcEXKW7dvj8fbC20tyY1qa7sRoLrGt8vzG0TqwRvfahoEuAD35ktb2/HGOfWtasIqP0F1be1hH9AQPzz28z2w9DFGQboDIdrAm1gfvA/pFj8CBxjBLGissgYXiJUDYtub/ACxlnNdBsK8eg1DrStVZpBoLVLFXsB9BiJY7lF/4T+X/AEYsGuJ5Ygf0R1MGl2kxilQbDbkN67A/dh1vSQIHRj6mtdgi9+Dft8hi+WnUgI0gK7BYXZQePe2FXT2JqYiHQah1xWJuJ0PqohTb00SHj79+BtdVnmC2iNUt6C3/AAOK/wAbcyfLDmip0ZxGoJCrc2UEfmecXRRpcASuqgkj1N7/ABxScktysK8HyG0dcVKU8byaI1ShkYf94obX+ID4uq9X1FKCV0bqeZVZhdKNbDaoY93+eHIKeE7mDMd/pF1U2I798YzENu4qoLmxvGOQfft8MJnOPQs6tJ7xGnUa3ry4hj0Jqi7naJVoIx2+P6zthBV60rHWWMaI1YLEICtDH2HAt+s7YfIpoLEsE/aI9C+/4Y1tVAIIgQvtzdF55wp1I9Crq0opuMTxp8amaz5x4kdY1VRFVxFZaaNYqoASRD6NEShAJtYluL4g3E5eNin8jxN65O7cJauGS/308RxBuPTwd4K3Q869W2GDBgxYgMGDBgAMGDBgAMGDBgAMGDBgAMfQL4XiT4cOlhH7OiMi/wD9CHHz9Y+gfwsDd4cOlxawA0TkVvnaghxyOMeaj7R9BXkSmwYD1WO4WPxGIB619c9J6a1XVdKM1rIak5npOvrKympaOeuq1kJ8qCEJAjApIhqWN7AeXycdATAsl2I2k8W7/wCOMQJruo6wV/VuuybR3T6rrMjfTJoZszrc0hpqA1NRKQrD0tOTGN3CqQd7/LHFw6TlqbnK0TT9GNZ5/FT9MNDZPpjMcmyM6JZ6o5hln0fd5MVIIRAzPuAvK5uwUEbSC3bDU8RustTarpOomjRpXNc60bHldHlrmipInh+uFqkmmLzvIpVYo3plIsw3MbFSDiSdNaY6tZBnOgqTM6qgGQ5TQVeXZpS5HHUmnkRaSBYmnSUqrt5kXG1Ab41PWDKKbqDouuynJtE6/nkZlCxUtLLltJO7zrJLJJeSIS39ZJLeokG/AxrThGT0uVjN2JO07qHUDZFV5rrLTh069I8myj+sPrArSrdfMfy1JQiwBALFSLXJBOOcKjqvkWW9K9YZBUvnmY0We5nqyipKjNoZyDG0VW9PHEjL5skatH5IFgYwy2NgTiVNO5zqzKidGaN6Lal06nlSvlOb5/UR1uXQ1YNl+ktFVSVJQqPtKNxPuAcaup6FZ/kunabMKvbq/UzVVbV5okgjoBJFXR1C1dPREqTCoacModzuKjeb+sug4rVlZyY6Oiep8yzjKa/TtfpIZBT6TlpMjgRaxZjMBRwzbZFCmMEiWP0qzLcEG5FzKgQlg8hPPJ/wecQFoGg6h0utshy/S/RXNdD6RXNMwzrUNXneaU9RJWyTwsgihSGabkuY2uxG3aAL826BjhlWG0obcODu7j5HFrK+iJUtNSjI4jYlwN/H3C/+OMcq+N46YyXQVVq+Gvyyi1XQxRrQGdonlrqVJkE9MiNwwHnbiVVmBsLY6rdH8trkDYLm/wDZ/j4Y5E8ZPTXOq7LqjW+lZK7MNRZk1LklL9ImqWgySnkaTzJ4YIIpJSZCYw5+yQi420I6q4io3bQ5U+sshpdYHV6amknq41ipWZ2jWNoo2Zwpj+hd95IuLEfEjnHTvgxpNJNpmk1FnGY5U+vs+SsWaE1cX06KlWcgUkaJY+QipHIXCnb5gG4m9uXKLRmvptQjppWz16ZquRjOptQtQZvO8DCTySRRCkVuWBAFtu2x3Xx1b4Oul2lskyin1RmunWg1rQUE+TCorKeanqxl8VTIVmiSZEliWXeHLWLcBdwUc7ZRTZnpuXQm7XertSZDYZL0z1HqWKeASPLlUlIiRvut5bCadGL8XsAbjnHJGZVK1Phs15WVWmZFfMOqM81Tls8cTSSyDP6b9XILlJGABS++1gLX5x2rqzUWldM0bVuq8/yjLKaNhZq6eGmS6nlULkAg7gbWK3PIJ5xxQc8yDP8Aw8axrKHMRW0I6pNUR1R3rTvDNn0EkcyTOoR0ZLsHViPuwRikx8k5I6n0p1C1BnGoJMmrulWo8jyI0hlhzfMZqTyTJvXdA0Uc7MrAm+6xX0k7TYYbXiBzmPSOnafX76m13k3kzRUTUmkKqNaiqFRMI0ErSrsYBnHbvcc84cWbaoyDJtWaf0itRA0+p5a1kcVKFkSGmaQFRf1AlPuO7DO8TWXZlV9NIPMoqqqnfP8AImkFNEXdFTM6ZpH2qeE2JywBAub2vjZEso2W5ztrrMNSSdWOnsj1/Xg5k8+Zus1QtE0ylaZVElLBxyWBaTmxAXn4SZ0+qn1J1ao8j1FqvrhQZlkMEWfQnNaiipqcpM7Iq1CRrecNta7brm18OXqVTfWHXnpdSoJUMa6hlUxG5LGiQDYrPYObC4FiT3ti/p1VV2q+vGu6qq0tnmS0lJkuUZZU/WeWNTLJVJU1Lll3lg67ZlsfWDbu32i1JlVozouiaJIo9yBSo27bG6H4c4z7GIBVyATa98IMoqBLQR+dMvmRrtk/VbXDDizAKoBPwAH3YWTIJVG2RgFF+eDiyRbkYsyRZ6dYH5TzoztuCqjeLghyR6tp72+/GmGU5ZRLHRx6eljWKnqIozSRgrFGSD5S3ICvJci1ip2m9uDjYtN500SR3G1wVNhewa5tyLfzSt/e+NNnbpE8w/SCOjjMNQTD5UN1X9XumueSyWuAf/CH8XXBGrFJQV/mPDo7MGiky+Bp444SC6EkpTkq4tIgCXG4j1oL3IstzWHKZK2qE2mZ6hjUUs1Q8KCQSgjakigFiFQA7yeQR2POMroVFQjasijmioaeQ1LxQMYG3OTUXYEBJLgD1AbV4BBIKyIPJXVlRNncCQmrhVY9kO6P+PC3udy7SOSfUd3OKydiRIlHS0+YyVS5JVRVb5mWWTytipUeT/wp18wN5IF1Kk8k/YGMtFHlVPLRfRMgrqPy1qUjklgskA3ks0g37UEh5jvYMCLY2PmVMsyNHqGIiOrY7QiFGum0wBt1nK8kqvqHfGNcur5TSxLqOGZUScyRikhvKCw2KnJYbNwuFuxANwMUvfco5IzU9aZsviqlinoTWKJfLmTbLGCbWdb3Ug9x7YugzjLqSJ6aWLzp42+1ZlUsAQOT7hb2Kkk+xAwloaTMVSGfNs/oauCCjBkESIqzFTcyB1O0Rm/cHYO24E43kOXlLQUccMgMY2iPglSLlVDXFwP2CLe+1u+HKSIujUO4jXdSRikjDqsEtrSOxYjcCBe5IJ9NwGO4E3xkpKuWSolo81pYXdT5gmanZVlP7UikdnB+0BySe2NjUZe0kSlVMTel1kjXcri5I3Ak9x7gj8OMa/aHooKlvKgq6Z/Kf0SXVgOUABLMrdyQzAe4wXT2G3utCyoLLWfRVjViw3LIsLx7vkbkG/JuCLfgSDcIKl6fdU5vUiPYsfBBVYwQfkSL/MHjuPs4yJm2ayRLT1FBGxHCyAhzFb4dgb8/7O+M8MiwQirnSzNdiUU7dwHwuCPbsSeeCBxhc9gszDHXVy1DS5pUedDNIpM3ELpIwuC/G35bjYHttPfDf1XntZT1cel8gqaRM5rodzrtFqCmuN80o5F73CJezOOQQrMLNUawfLqaM00Yqp6mXyMvy+NL/SSR6ibgeWq8s7WICqbckKcml9HzZDRTRZjVJWVWbSCrnrFh2yzVW0cKASdi2skd7gfHknDVcqj7KHyvp6vb92/Q6uDpQw1PxzEJO/ci/hNc38WL3v3n5KulKynJ4Mm05l0eSUdDMFXe5le7TVDl9xlL3vuLEs7LZmJ9fwwulzpqphFTQpDExKO7xqbKxuSV5XkjgNcD9mwAGFUU6VlErzSwSK6kkF1szKbXL35N/Y7rewGKGncKrsyh0+yVB9I+RvcX/k2+ZOKuKirRWhiqVZVZupUd5PVvq2BjBQQzki4Jvc3Y+x5JOMkUCckx3utr37YtWkVGDBNx7i5tb8uMZU8wJ5bLtJa5Jta2M0kyt0DBokuQStuPgP5R+Xzxg8uNkub7hxYDn7vv97fDC1r3sjfZ4v8AL/B9ufnbjGDyYo4mO0e4AYDkf2Wv8tvyvzhMky0WJuVBLc8biw7W+P3fPFpimdn9VgQOLe+FDRuzbmV1I73B+1/bf+t87YCzLtVV49rYyTi7l1dFiwm+6U2HbkYpLHIpUCQbSeeO4xUOVciS547Yq5Z0ZDxtW+FbB3txO8qQOVh53Hvi0SJI4VZNvc7ivAt3P3YN4RF2i5cX/LFqRsBEsZAZiT7/AD/xwb/MC4xSeuwJJMvBQMojUsy9xbm59rfH5YozOimUFTcG1+20G2LVUQApEBa21gbWBuLj4W/q/K+CRVqYyjg7gpB5t74zVNC6RY4XcQAbX5whrfLSNg/qtyLmwvhfKbAhLAD4411XFvX1G6nkfG+EtorV7jPG/wAdBoP8pzWIoI5Vs9KJd77ryClhDW+VwfzxAWJx8bBRvE9rvYSbVsIN/Y/R4sQdj1lPzcfYcUMGDBi4BgwYMABgwYMABgwYMABgwYMABj6BfC5Ih8OXSqIt6RonIzZTzf6BDj5+se8fhq0Vllf4fOmE09dmiGfReRuRT5tURWJoYeyrIAPuAGONxq/ZRt1H0L3dieQ7O6rY2uB7cXwSARAFYQbgsqenaLDcDY8+/wARhuQdOcjSAK1fnw8t1cn65qy3bgH9ZjHN08yhvMJr89KsrLu+uqu63UL/AOEx5xRnL1GpZpchx+SizKzAlkkj9flsAQpJ3AXsp7jdhI9MqWK2Dg8hl5J/lEdz88aR9CZOoSnTNM6kVY/L9Wa1H2fc33d74R1GgMnk3WzLUCs5JAXOqqwvz/4TGyEmtC0Yu45Vp1V2kjk2gjapY/aJHY2PPt+eMyI1/o6ICDYFFttY2vwCDzb7sNSPp7kmxFfMM+QkWv8AXNWexHP8J/JXGdOn+SRLFIMzz69PJuVvracl7jncGJ/1j4gnnDk5dCZJjjpkgWHfBTMxZCgIS6kX9gFwoJDttSOT7YAAXhrnix7d8NU6Dyj7Jr89kFiVUZvUC5Pf9r+nGCXpplFTLTJJnGqVSleJ41j1BVqDsJYBgH55Jxpg5dBTVh3OwkjLIGvySCh7Wue3Hax+7DD1v0Z6aa7zGPN9Z6PoM0rEj8pZ6hXDiJe26xtwb24xs36eZL5YiarztUBdifrqoN7qFI5PyGCbQWWbmqJM2z7/ADmMxLEM4mICHm45742U3Io1pcjs+FfoItSapOlWQxTBVDi0iuyqxIRmD7tt+SL9x2GHBpHoR0s0BnE+pdIaNy3Ls1mpfob16K5lkgY32MSzXANux5GHBPonLJnMseb6hXm5b67qBz3tYNwOMWQaAypC8MOZ6iYzHfznNWSp72/hPy+WNcZOwuCd7iDUfTTp7rLOqPUuq9IZHnOaZRC9NR1GZUwqPJjLozookJCncgIbkixGF2daG0/qrT8mn8+yKjzbKKuM0zUVRTLLE2w+kGMnYFU7boFA4BBOKjRWXK0jjN9QEMm4g53VbQLW3cvaw3tf52xr59F0dAfNXNtQiF5QkijM5rlmvZuCSLdz9+GwbbGDVyDw7dFNDZzBqLSfTDTuU5pSpJFBXxU5M1MjKVZV9Q23uRyL+rD3M1qhHqQwCDfIu5gwJIswIBZQLA9uRf1DsUh0flVdTFjn+eKJlURzfXVUhcgDngkgnavsPfGA9O8upZXp6nMM8YcuA2azyBmYbSSb34ViQdxF7cY2xDY1WcabpNR53kmpc9oESvyAVEmVz2CtTiZAhB2WLhrEABmX2AvcBysqU1SrVcU0YEYZJIoC4ik2gABtput73KEfj2xpavSLvF6s6zyRmd2Ros1kWR3LXYE2O2wFjb7Q7BTzi2v07QSVifWOc5s0OzbUO2YMR8B7Dk2/ijn4dsaoJPcrJ3N/HK9LmEkVKu1JY1kRQzopZOAwBZSbjnhbfdjPVTSBAjsOe9m9v4xub+2G7Dp+ntFSRZvm7QrFtpPMzGQFQXCi688cjs2MmY6PpZoWWLPM/jY3iH0fNJPYOp4PbiRvxUYYoq5a6NiahUcTEjYsbSqXjsGXdbi/B+OMeYxwLQPWSZfT1lQIZlp4ZAt6gEn0F2WxjdkQEcjjsTydPDoqjadYvrrOjGvoZhmcgW3e24tYn5Yur9L09VTzo+oNSQkoI0ePNHSSBuLG5Nh9hSLgXu2JaSJi0ZqgmKmmWHSVHKEpokhCiA+aWbb9GAIJCrYd9wswscK/1VNTSGTIoXg82EeaTEVqY+3nEXIYxcmx3ACMbcMvPdIZdDJVRz681HTqtJH5wlzp1jpVJCrUNYXjdmB5YgHaLXxnGjctmqKj6V1C1UXStpkFPJnjJ9HkJVlpyrAFfM3KLMFY3PGK2vuS0nu7Dt+u5JK+Fss0tCCa95jWF4y6ErsFVGu434Gz0gsSSX2e2voJa+Skoan9HqKistSiFRA3kO7qTGpUkkyjc10IU8mVmbk6dNN5UXSioOpupJxPmDAx/XnJdI+ab0qSoRbNcEc3xrk0xk0zwq3UfUDBY6i7vmxLVLKzAblAu6x+hQqjjcS2DImWg4PccaQ5lDSBo9N5apGWG0DGmCILbfoJNwvl+kbnBChiF3rcpjbxVubQVUkUuTRrTxBEjd2AR7qzeWsajhlK2+yASVNu7YZceUZMlEWHUPWJEFF58M8uchfKXjZUHjkiPdYgEEBSOCSN1QaZpK4SU1JnGoAI4laSR86ZisbBWZ9ygqSSlwoGwjcT2JBkRbLTew6l1GxEkUy1VOrsWYyncTtJ+0yk7fmeRfsThUsmUZvOa6sCLM0i+ZKAd20XA81SwsAeAeDz74aUOmYWWClOq9RrBCERnGaSLJLYAAMDa7Edud4vxxxjNLoLJqjbPBqXVNNKtkSaDOHby4yxY2Njdvmw9+yjgDSiRlhtccVfFJBAjU6yTNLYRGNWYOv7VuL3HNxYWxpM/wA/y/LsulzTMqqVaKHaIUibc08h4RFH7TMeAPc2+8Isw0fLl1JUVFTqvN56enhLzyPm0kPnqq+oyXuNygE+q/xFrY1Gmun1Zqk02sc6zHU9HQXjbJaaerkaSIFBaeRWuyzOCbR/aWM9rsbZcRUatCHef2etnQweFotPEYl/y4723k+UVpu+b1yq7s9E91p3Ja6grDrPUFMJszrImp5qaKUSJl9LyUp09iQOHkFtzFv2VXG9p0qqSkf6uq0+izxjbHLH5iKo9muQXAPYXQj+N7HAdJKXeVtQagZmUs0iZo4Cj37XuB2v2wlOikJIbUWplRSyBBmsly977e1+wPGIpwVKOWP/AN9YjFYvxuo6k2uiS2SWyXqS/wDd2baOomp021G9JEUqXLMxkJPe6lSfe/ZfgtuMLwFfgi7KQQApsRbuDaxw26jR8E00e3UWoxa8isM2e4BPft29vhi79ARHOKtdS6mBWIRor5jdDccsVtw3fm+KacxCjCXMdUQiZw0jXbuOD29sY6hm5YAkL34w1F0cNqwNqXU1kULcZvJc7RYG9u+Lf0J8x2nbVOqmd1tf64kGz5gW74zztyLRpQ9IdAnkAB2AE9lPB9v7w/P54FmEhPrjJVgobeAQ1uLAG97drc/Di+G1+gkJVXbVOqWZAFuc1e5ttPw7/qxx374ouiUiiZE1bqkxuQ5Z82LFV4vYkcLf7x8QTzjO+ZdU6a+EOBJrptDGUWI427fut2/D7PyvzixqhUJR2UE9vWOcNr9AgAEGq9VA9izZtIWY/wC37+cW/oHEsflHVWq2Uchvrdr3/wDNxmlZ7jslNrSQ61dth3ML/Ei9vuxgml5BDA3W5YsB74bp0G0iASat1bYj2zd8Ul0ZPsWN9WapKJGUAXNn7fPjCGo3JjTh1N7IEjs7DbsuLH3+62MYlHlpO5uEBtx29vbnGhqtDQS7tmq9UqLk7VzTdf8AEpfCFtHBaqCkGptSx3R1I+t3IJ3Dnt88RZFHRhe9xy+cEbyU5BPmbhe1/hfv/YfmORjKkiiIyi6k3ZgRa3PI+X3Ya8ujAyI41RqhXiUFguavybcjCg6DM6ktqXUq8EMPrWQXub3wiUIvVjY06aXeN9JKOCZfe1rd8Iqx5FjUqNxNyAO5BB/6vzGNNPoONZlli1ZqjhmO361ksoP4YRVmhlqBK36Vap9ZB3Lmz3QjkgfL0rhGSBFSNNQd5HkR416Wem8TWuGmQqJquCVCbcq1NEQRb2tiDsTl416RaHxLazpVqJpxHNTAPK5drfRorAk9/hiDcemhpCPsOA9wwYMGLEBgwYMABgwYMABgwYMABgwYMABj6AfCnRwp4dumDohLS6JyMksLkH6DD2x8/wBj6BfC00q+HPpUETcP0JyPke3+YQ45PF/Nx9vuNOG7zJbo/pJnkSSGIQxgeUyyHc7e+4WAFvbFtTYyGJiRbjgjtdRz8PtYI25I28jk+q2Gb1Z1rT6F0Tm+pfL8yoip5IqCniUGSor5QIqWBQxHmSPM6KsZI3G5HCsRxKaUmbkOTfZi8TqykbG2ENZgAdvHY25+43xVDEwvvVr9mB4P/ViC67rRn+ks30VR6s170+kaeF6TVtMKtmrFrkpw4lo4on3sol3xtT7B6dpDCxUTLpzUGV6ryyPOskqWqKWa4WaSGVHLDuCsiq9/mVHf78N7O1miboWSssCGNV5bld/BJ+A+N8ZUCsSIm8wEblAsTz2t8eMaLXefZhpDReeamyrL2rKrLMvrK2GkMpgEzxRGRUErKbE7WHHvhr5n1Czk9HJ+peSadWLMZtOHUFFlVbIx5NMsnltMq3Yi9iVU/aW9u4dCLsRJoklANoMjq+1SR7WANiT8geL4vjPllpNliTe3ww1enmqarWmjMg1VVZQuWTZ3l9JXtRxy+a1M80KyCMMDuYqCVuPVZfhxhyxQAsLyAAC5P+AOcaYipO4TgSWZnCg349+cRJ1T6/aM6U5nDkupqPNkmqoPPpzAkISosSCI3aUFtptuFrgHj3xLlRI0asQwYlbc/ZC/7f8AVfHn947azW36WZDlmdZM2Y6fkq5KnJ4KCuWmmgRKeNauqnkdeWcTECFCRaMG4ONtNXE1H5OhN/TnxiaF6hV2QZTl+QajjzjUED1UNEaWP/N40cCWSZvNtGq9zusbex7YlfXGv4NDaY/SKry6qrYHr6PL1giAF3qZ0iQqXsGG+RWIPFrmxa2PPDwuUNfmnWWlqelecUWXpmeTo31jWQ/SpazL0qYnqoREW/VE2RFaQAkbjc8Y6p8Ren9J0WQ0GtMymkpswXUORulTPmlRDTtavpyFEPmrC5KXINmva9salFCoSaRL+k9fU+sanP1p6CogXT2dT5NJJLIP180MaMXQAkkWkABvuve1lthHq/qnpnT1HmMjVU8k+VK6GmWgnAaVozIiKwj2G4VrG4X4nEL9NNN6Hkz/AFz1Dz4V30ik6h1cVJUjM6kQw1B+jQ05MMTrE53PtKspuDfnG51rSPnvWnPcnq6zNoqKi0B9Ihp6KumgjjlaoqVbdHAyI7bVX7QJA+/DkkiZSY5NPdeNCZrpfKdU5TPmL0OfJDNDSLltVNLF5wBUO0UbBVG63JCcd8b3VPXXp7ommq6XOdY5VFWUtL9J+rnzGCKokUglVVZXReSp28gcffjnvpS2VwVvQ3TuWalzhsyn0rNX12WfWlU0RiOWwiB2jc7ABIQVvcXbgc87jUGQ6w1b1E6h6S0giUH1zSZLR5tm4eJY6HLvInM6qq23TOrvsYC3rvfgHGiItzZKWR9e+n+rIsoqMk1ppw1OexRPS0S5pTip3vEZTCo3kGQKG3AEi4Nr4T666t6d0KucVWb5bmjrlVB9atJDSq0VQCdpSFnZVaXtdJNpswIDDkQxk+Wau0Z1D6adNc0yEfozp3PaqTT2erVxyIKQ5XWhKOcN6/NiUWD22ttDEqTYa7xHrpXMs1qtANmWXhNQ1tDR5pWZ3ManLcgmqY52R6dQ4MM8iQFmMbhAgRmU3DHQm7aFXOxMVR4iNI0upINOw5dmktRJmlJkxlpKdJqSOrqlSoiDSq5AQxk+sej02BPGFVf4htJZHWZpBX0OexRabkpafMKn6tIp4palUaMlnI4betvfnHGHTXJZtZ6y0t1PodI6byXJo9W5bCDlcEwZqlsvKSQot2RYYzFchizBi3C2w9urepp8l1Z1C08zQUzmei1FDLMzedNU5dRUUlLTALG6ssrOoN1ta3ODM1uGdnS+muv+nNYZxDp7TmTaiqmlkiizCqWhUQUSOCYzNMsnAYAgAM3v6cZNZ9fdIaG1Dlmlc0ynUD1dXWGiRKTJKiVFfaXDAovK7VuTEOLerEE9DNcLk3USopIHpaiq1PW1FDmIlq7VdJHl9MTGzLBDGLO0jAb37+2NZ1IzXXOadUv0ryqt1RHken6yp03NmGX0eVyJRVde1JHEYNzbmX9aiM7C68H7JJDM90TGbJ80513ynOtV5tkmUabzr6VluYw5fFUz5TWRQSVfliWRWkSAiGIRsu6RivL8HZydh1B616a6d1Gb0GfQPFPkkVNX+X5bCSSikkiheoBsI90UrhSg3WbY2wBkY8ldJZM4zzN+n9OmZ5xmOXvqeuEua/VtZSw0tWIq5kiFYsqJU7kWM7I4hGoVbs32Q9/EPS0+YDMtK6M6jZjJUVGbZLJmNAoE0eXLUToEhpXCFvMfak/lgFVEUpJKsm2rb5EubZPCdZMikzmLTktGUr6rVE2m2VSW2VaURqNynYbkxizuGdVvYsTjY5p1L0jpjMDk2e1FHS1WW0EuZZgIlkqPoVOZJFjlZkiO0P5UjEMV2CIXuRbHLuhE1RRdT8hbV2eZRmFHP1Dr6fMoUbZuzBMrlvNCSAWhdJCWUglSqiM7cOTqsuTvmms8+p86q5aZdHDLYimf5nRyiRVrZW3iONo5E/XRbQzfssDa9iJvmEZMnrKureVajr6fIckyClqa6tyR8wpoqqR0jnjRyrRh5IQjpeYEMCUICEqD238WdR5tqmqyHM89ppc0WmSqWmSrWaqMe5RukgjAYLcixIsQF9gQ3PHR6lpqbVOn8wzDUOYwUjaDamnb6yzKpkjUSU0nmpvhEdOwUPYiS7BBt4uMSjWZO6eI/LswjLTvVaFr45/1pJG2vpit1FiOH9129wRcXwZmXU2O7WHVXQGhGo8u1VnVJC9WYlgVYvMRo3nWDeAt12LI67jzwblSDuxo6Lrr0hr5aX6r1HMj5nNSU8H0aCoaN3qQxh3FUZbsEZjcggAk2CkDkLxO6vpn68vHqPTlTm+Xy02XyyQSbaVY6iOoTcS8hAmQt5MDyF1ZQwUbwt8I+iDx1jZZmWfxZLSwSV2SxVNPQ18G6dq4yQrJFFGiqamOOpZXIAYAT7mDthVWt2a6t7Lqx2DovFVGnLLGOspeiuvrfJLm2lod4Zelf1Arg0swqtL5XPviViFGa1CN3JW4aGJwCtrrI6hjcC5er6glp2anqKSph3RqplblWUWuDY2CX7m97/tD7OLsthpaHL4qGGkSGKFBFGirtWJB2VR7ADjGaNXkBjjfYS1l78EjgW9zY3HY27EAnFKdPIrt3b3f75GnFYvxpqEFlpx7q976ye8n8iskks/0yGptNJEFd1Hk1MVyzi1ttwALqD3NmABtb7OMtNSU89PamJZiLeWSCb9+OCCD3C2577b840axU/0s0kNS9FUujSRvDzBUgErZo+yt6mPpv87YuNTVZQyw19OzRSkBJ6dQ9/ckADkdrlQx93W/azWhidPNsxfTxNEzSTMPPdibcjjt94Pzvu+YHGM0kqIoOzcOwF+34YxwZrHVA1CFJkNrulmbZbvcnnjGRooA3mwMZYyLjavI+8e2M70GJ3VrGORWEm/y7KR2xjZI1jd1QC1u7YVGVJVMQKmwvxyB+PtjAId0TgMPb3uMIe5Ka2LBJtUhtgBB9+w+Jv7X+Nx8Re2LwrbxvB3bu/Nyf/avb+db5cYxMoa3PI9JAv39rfO3wsfgbXxapRbCNroF7WFgvv8AILf+b+OM8ixkYktsAXb2Frdvh8LfL7PyvzjHIgDEXPfGXlbtJe55He4/PCaYFyQTYHGSoMgmW+a4B2EkDi4+PwwFZJmCNuFxz8cVQmxBUCwtyPb4/fiiXAAZjccgn3GEscixhGPTH37D4njt9+MBpo3mWqkH6xfsN+ztuvc/Dgi+FDNT3L2IFvVfsRf58Wv8ePjzY4tb1zAq5JJ45N91vzvb+db3txikm1sBgFG4lBla6XItz37hPv8Al3xkEjKg2FrD24xkkVVTaAGsBYcWtf8AK39X5Xxa9nLDewYm5t8cLk21qRYwMGdg7KwA9j74TVMMSROyqoueRc2P342EUW/0iUsR3De2ENWUCSLySCeRyB88ISbegur3GeLfjbeKTxO68kgq0qENXCt1PCMsEQKfeCCDiCsTF4vdg8SPUFY49ijOpTYjknalz+OIdx6iHcXsOUGDBgxYAwYMGAAwYMGAAwYMGAAwYMGAAx9BPhZaZfDX0teYrGq6JyK1u7D6BDbHz7Y+gbwukt4b+loaygaIyEbvcf5hDbHI4x5qPt9xpw3eZLEQ3t5rHabccfh/rxA3iQ0nSVWVyat1RVx1dPQ1mXUuS0GzbDST1NbBCalyWHmys0pQXHpU8AsS+J6i7sIxc25LXF/9eNNqvTGSayy36oz7LY62nFRDVmnkN0SeBxJEwPfiREb8McOlPI7m5HM/V+rpH6kdPs1/fG0Fk8NDn9VWwVn0VJqikK5fOvmSb6hUa9wgDjlnuQCuJ06a630zrPKhFkPUPLNY1OWRfRszq6OeIsZL2LOsRKpc/srcYcVLp3JKAMKTKKOlVwbeRTrGx7kcjk3PB+V8LqWhpIZXaOngUzcSqqizHuLgAcj4kHnGztM8bC0rPUinxDZ3k2WaOqcirNQZ9RZjnNPVUeVwZJFMazM6kQMFiR4Inut5OQbbRe5GIezzIW0TpTJzVZVmWRTUmhtSx1FNmGaCSSoK0FPseVS7rcOpCi7G99oX365mG5FNlLub3KDvYixAtuFvnwb98NOq6Q9PaiuqMym0ZkktROWeSWpoY55Hkb7blpFYkFbi1zf35xopNKOpEtRh+H+n0ZleU0eU5T1JzHPs1qssoUko6jVC5g8SJCo2wUu5jTjjkW7AXFxfE0sSgAkAU35UX7X7c8/nhuZR0+0RkuYpnGU6K05QV8Y8tKmlymnhqAh/Z8xEBt8u9sOEwUpkDAfdY8YampbFSszxKGJ4FuPxxAPWKl6HZhqeTLtdafq86zhaE1MiUNHX1eylZjGS60xaNLkMvI3XWxtifKyNXibadt7c+wOObOpOQV2fdea5Po7Gly7RtHUSSfX1XlawE19WFc/R77ltvJ3WHA740wKTNRozSHhvi1Pk2ltN6Lr8vzSKNszy6lrKDNqWP/NjGFlXz2SIBDJEtgOQwJBvh0avoui2neoTZ9qbR1EueZnRTZo2cV+WtUIIKTyY5JQ7Btu0zxAOoBLHj7LWjvoFl+dSmg1DmM9BW6g0tU1mnM3zGv1hXVc7pM6kPDE4MZM4SjljckI6twSQRiIvEvoup1V1zzrVFNldcuUUema6KSClSWSuzSaFoVmnpSW2wtH9Jp1Vyuz/ADeQkG5bGtK6ESuuRMUeofCjqrqFDmVXS0i6krcyFalRVw1sUVXWU9UadKvbfY8omRQHYWcBGufVtlrU/UvpRk612Wag1pp6kkghlgq6STMoY51ARn8u+4M3Dsdo5F7EXvjzq8O8C5r1T0blWYZW7ZjLWZZmCVEFTJS04lc1Va0kqGCzL6jZS6qQpAYhr46H8VA1dkeoY8/o486/RqXI/oJbL53Y1VfLMxSBYYKmF6iQRkA8TEINzDvhyWhVSfMnHp/1S6H5jo7TmX6V1nksdBJRU+W5LTfWEf0yGIovlRbQ3mq7XtY8fqxfGca96X5JrWryKvzeKn1fqCrp8uroG82R6mqjj/zdb+XtUtGQ/faA3e2Oa/CTDrnPs20dmsUeffUOSabnpKwTSyCKlq/LgSnLRtUyMreWZGi9Cb1syrY8Xa/rNTzeInNqHKaHWFZlkVWuZ1tFkuZUaSx1MVDR04qQJE3OqxTspj3i+zd3w+LKbsn3Jsx6Fa917Raros+oKnVmT1tZl1M4zNknapjDRVGyIvZyoXYDsIIZz8MOnMdNaK03FmWd5hFQUIkqHzjN5pad5Enm8sRNUSWFwxVFQkgelWHAJxyR0PzOmy3qppmto+nVVV5TqClmj0hAFIl05QysstTJNNLEizkiRQZFcleVUFeTO/UukXNepWVaKoK3UVRU59SVdTmkcGoaqlhoMrVCnmRxISQ0juiKqgK/6y7Gxxqg0GRsdWW9HentVmWT5zSZLTtJp6qqczy6SCeTyzNKWMlQzL6JSfNkO435t2wzddaZ6FZjrs02oa9IdWZiYcsZMvrKuKtm8xVKedDSvZTbb+scBbJdiMYNM6P0ho7WFN05mqNU5bU5ElPX6bpJ9R1M1HV0sYKPthclSsIba8PYoY2BPIEba7z7OaPrnrWj03qV6erebI6eOiXVMFFHmE4ouEkppInepKl1/Vq8aMrBSeTts5ImcMqQ/OmeSdBs61PE+lM5rswzfL0GY09K2eZi6sj3XzAkzBXBb0japJ4F+9t7qro90a1Tm+YZZn2VZP8ApFnIOYVskte8NeZDfbOqFzOCoVW3qoUWVrq24iKukeoM2zDqNouh0pUV+W5VBpFaTNlq6uhrFioaaeQIajyyGhlaZpUKJcrtkuAIycPPUGQUlbqmXTWloFr1g1dSalzvVFVFAn1WyvCyZZT1LAyzTsBEgsbJFNJu3I4Rb5lbYqkPr97PplDqXTTSwvHXaWgkiyWkq81alhQCAx2NM8gDekMijaGU8ElbMdjU9OND5JnEWooVoBmdRm0uc+XVTSIlRUyxJDJMkRI3SiFIlivuEXmuoRDtB5x60QaiqNba2zmh01QTabyOXTNNqGqmhT6fp2SKVZv9Gq6ksVjYPKLkqXa+/sJG8Q+jqbOdOazzmhloM2z6n02I6mmq6uNzk1CJZpmzGJCu41AbzGi2yRq7QqN1k5i/qJsPbUGmtBabaTVGZjLoYMqzuv1LPUGrmlSOZqNo/OaM3J/VAl4r7DtZmN7Et3OMl0Fqet+ramecSaloJq2XLxqbMIYpoGKou1N42QFpU3dnDMAoI5EV+KSnhzvQMcGTwismq6NUihkytalYaaonEU2ZzVE7jyIi0ibVDWUldvG5xG+oNF0GVa40/BLlVVNRxZNmBr54dN0FfPlUMU9NsqvosNQ7N5bmMHjcVsy7yGtSTT2BSynXWTdHxkUlBV0mps8DZU8VRSwVeocwkokrFQRIjxeZseAbjZbclrPci5dNRofRcOoRrzM9IZbTZ1Egg+uTJGHih+yIi7DhSXBChRcqCIm+3iPuqOkMoqdG6FziqdavNcrz/TMdHmO+Wndo5aynR28pXAG8M3pYBlNwbAWNviAzXN6rVnT3S2R6ey7UCZpV5jO+X1kqxUzvFTqsUsqsrodn0gnYdikqpAJuwq6kaUXKQ+hSq4yqqNFav7PW3sklq29EtWOLUmmcgzzUWZ5LTZRTZzmuf0kS1GZThKtcvy/7Lx2e4Vdyh0jud0jBixEZxt9NdGek3TinkzKmoMqNFlcdHIKmup4QlAaeFo1qNtlMbt3d2Nrm49JBMK5FoPL9X9GtBjRWnaTM1nzGJ62k1JmPlCdIopoXjmVLtP5TxqEVNxIVOCLkPPQ+s8hyfpRV5Rk+ksl002n6uoyGbJ5cxjgo4JkcxyNBPLHJZWZw6OUBbcSd59AVGLk+0mrPp0XT8X7kjXjKtPs1hMK7wi7t+nLbNrrZaqKdrJt2TlIl+bqbobLQPrDW+n6NUBJM2aU6oAv2iH324PHe/Fsb/K9RZNnOUxZxktdSZhQ1EO+KelljkjlU3JVCCQ/A3EC68gkXPHBM/VHKVefqdTzfSMujyt9MNRrqugGaSzfWAiFVGyUojKPuU7jZiLnbieqDM4NZ9IU+tNSZNSZFp3OIoq96HMHziCvy6kkG+kmkjEZklluqttUguwADBhizaW5zo1LaE/Zg80sTWR1nN/IvGwEkgHpF2BBJHbduN+wGKUubUtTHE/nr5JcM7d7sLjaD6v2vjv8AltxEfSLSlbRZPqSpzXKZslyfUeYNV5VkYm2yZZlxp0i2kRW+imaWN5vK7J5gG24JDP6cZ5U6c8PlTWVGpqrK44MvzwJmdQxqXo5YqmpEUrmP9Y7xgI6gszN5fJ7tiMyaLqodCzrFTJNUROaWVV3kLyjADlh3uPuuca+h6gZSK2aGLN6Rp4f4Y01ZHKLAtfcVYlQAOQwA+F7G3K/QrONSQZ1qCkz3S0+j8xh0jlUxyKTMhMuZyfrjJmSM25AXZ1jdCS2628KSN3POWVupPqKmyjRGcPk1VUZNFQ5jHWVEGVz00dNQVz06VXlwXDEHeWcgMVG4gG4RURbtD1Ky7U2W52lO2XSUtQtWiywvTSgRzRtbayj9oNfgjv3HAOFyzXSQNHsYkegWNvy/t/6scKeB7TC5HW55nU8efQhKOj05PTVkTpQwzQlSUcyvtkdSyngxqEY/bJa3ctJIJIvIeN/M77jL9s9yNxsGPx5ve+ESY1R5lQHZnU2sbm57Efjxbt34+PNsXKCDfa+8NyLNfd8fje3863vbFrLeTdE1j7KCdwP9t/yb7hcYuKDy1bzgU2g2Di22/wALbbX/AJt/a/OM09Ny8dWWzS7k9a7FAvfjlfjx7Yxt2tYj4374z+tkcKhXa1yx5Ib+2/8AW+dsYZbKAAwPFweOf9WMc2aUnYxFUUMQ5JtaxxWOJ2VSDdu1j8MVjVD9sgX+IxUyNFGZYTu29wB7YUydi2RFlUi9mU2sPjz2tzf7ufhxcYsiOyARqwItc+kW2X/K1/5vyvzgCtPvkBsG9RvaxH+y/wAePjzY4zxmNLhmJdTewuW3fjzut8fX8DbCqizbEoTy1EfmrDK1pi7/AKsghnKqGJHP8U377rdmtcYscbnVpY7sLd+P7MUzCCjzKOBnvG1NNHUo6kA+gm4PtsZfQSOLOb9sVmcuARuuALkixPA/x+WEyi47shlhn9RG25B4UnCKqklEyyN+rjUElSO+FIKK12+0MYamWFo2d+fliE7SQurrBniT4uzu8R+v2BuPrmUX+YVL4h7EyeMFqU+JTX/0Mny/rZi1/Z9i7v6b4hvHpYd1HKQYMGDFwDBgwYADBgwYADBgwYADBgwYADHv/wCGEVJ8O/SlWiDxHRGRGRuQwX6BDtsBwfxx4AY94fDV090rW+H3plmM2XTST1Oi8jaUitqACxoYSTZZAB9wGORxhXpx9powztJk+idktF6i7SbPsHnjFkpfd9kOpB3qBbbx74bQ6WaNLADLalWSQPf6wq79vb9bijdMNJxqVkoK196BHtmVT2Ha36zHAS1NeZjl8sypG4jawUANbgcfn+WApLM3nRehI5FLOEFjxb7zhvJ0/wBNSLGppKxhAqhAcxqQAAO32+cJZ+m+mJmO6CtuzhgVzGqG09+2/Dk7EOTY6Xd49wRCQGJN+WsATfj42IxUWY8lpCSQCFPf8cNT97nS0kxqGo6suLAWzKpU3UhgTZ+fUFP3Aj3xlXprpUCG1PmCCA3UDNKk3+/141U2xbbWw6JfQFUxHc3y+74i3x73HB4Nr4qkLKSGje5ueATe3398NeTpvpp2MrUtcWl+0TmlSLXHt6/5RxYOnelqbynWLMXUCxBzSpve3/Pxoi7EJt7jkkB27ipH7Vyh7dz+FucRnrDop011trCDXOr8jTM62loFoY46qVvoTIspkRpabcFkKszWZwbXbscb5+mmlpDskpszsWdmIzWp/aUKVHr7WAxlbp3kZlqJFmzZPpCNC1s4q+I7khQPMt+0ecPpSdwY2896JaM1BrLJte1tDX02dZE8LpNRzywxVghLmKOpjViswQvdCSNhsBewxTVnTPJtQ5+ueVNTWioiyauyRdjbESmrBF5hI7hw0YIYm1yb2LGzhbpxppA58rM4y7mRiM1qTyT2Hrvi0dMdO1BlPm5qFkYFmGcVgsB27Sj8xz8CBxjbGTKNEM6T8GXTvSmc0Gf5TmurvpuXtRyLK9eiGU0oAiDlIQwVkLq4BBK3uWJxI9b0h0rXZlW53mGXz1mZZhTz0MdVVytK1HTSpteCmRgY4QftHagN/c4cL9NtNlisYzG3Iuczqb/Dj1/Bji8dP8i8kQIc1WNCzFfrWo5J787saoyuUcbjKpugugKOTTeY/VzTZzpSihoKPNYqlqapnihgEKx1DRn9fEQNxjkUrftbGoqfD103i1hnfUqTIJq7UOZMZjVVkjzx0tljQx08LfqkGyNCTtuCFxI0vTnI0jCRyZwgsANmb1QP3X8zCKXphpyuH0eSXNlKJIt/rWpJYuACSd/wGGxQRgRnpXoPkeia7IpotU6tzRtNUEtJltPmmaLLFFFJHHHt8qwO3ZH7cgkcYc+U9PstyPUmb6pinqJ8w1BURzVc9UxEo8ksII0AACRKpUrHYm+8/tHDlTpXp2KlemE2bjfJ5o8vN6pbE+32yT27D/5Jsz6f6faUGaozTyzGLt9Yz8i/flzx8b3F+9zydCeg7RI12b9Lcl1JmuntQZjSu9fpaqkq8rniY3jLxPGwawA2ESm6C63jQ/GzaoOhGndN59qrUOlalcgzXWM1PPNXU1FTNUUzxxlJGjZ1N2kW5YtwzsGYE84f0fTzI5GaR6jOWbbYj62qACLf87/H5YTVHTbT1WXgkrM8RZEVRtzqq9CqrqLWfufMJP8AzVxaJFoy3Ix094atN6Q1SNYaO1TqzKp6iRZc8ikrTWRZ7Jdn86p81Syy3dl3Q7LLusButGrbw4dPKzP59U1OVZo1XU10WZuiZvVxQyViPvEpp9xh3cIPsDspPB2iSF6eZMKqOrSuzmJ41Kov1rUFFv8AaP2uScYIemWQxOsMVbnbeo8nNqi45vx6u/pFh8CcPTViHGC5jLzvw/8AT/Ps1qM4zalrUauSFKuljzCaKkrzTNenM9OhEcoX7I3A2AA9r4Uao6H6d1bXVdfm9RnJkraNcvrVirJIVzKk3uVjqFX0SKu9lIYEBXYEtYLh4r08yBH86Svzp1aBo9pzqq2iNmuSLSAcH3/+WMD9PcljkSqFbnhaJg3ObVHe5N/tbh3HIN/hYYto0QlB8xpa16VU+s8gl0hmeevHR1FSPLNNAu+KOJt0CRKzOpkjMafaUoFjZSg3cNvLugOcTmgOZdWM6qxTVUFbI+WZNl1Eahop1mCPJHEWcXVbgsVtuW1m4d9D0/ymOsppH05n1MJMwqHaofMd30UgkeeLEkCYC/pBP6w3tbG5qdD6S07lBqaqqzLLKDLYmkkdc2mRYkA9RJDWsLe3e3zwqyjqy8KSqSUIat6JdWabq3pHSup8hy99W1mexUuV18FfQ0uU5i0Ms1YsgaGNVFt53qpAsPVfgAE41tb0eh1g9Nm+rs6zel1PFMKmkrcpzBoqnLFMckRgjkN1ZNksgYFbs5LcWUBdpzp0mdTpqbPavPqSGNgMoo3zaczU0ZAHmOd38K45t3VLDvfDmXp7lMMjyjMM7BkYyF2ziYkKT8SeR7Xvbt73GE0oqtJVZbLZe9+7p8p1MR2WApPB02nN99r/ACJ9FvJrRvRaK8m5J0U0cmm8g0vlUVZlsOlSJcnqMvqTDUUsojkjL+c6vfcJpN9wVYuWI3AHGWg6ZUendIvpLStW1K4eSYZjWsJ5/pU0m+ark7eZUFjv3XUE2DBlAAckmjctq2SI5ln9lO0IuazL2+YIP43B+FhwMJ0LlRACZvqFCvEf+magbfvAa35W+4Y1WijlZYrmR7mnROmpOnVNoDKJ5JKCjmpKoVckHmT+ZHVpUSSMAoDmRg7MCdu5gdhPOHhqvQGQ9RN
```

response

```json
{
    "BYPASSID": "",
    "Father": "陳德明",
    "Mother": "吳眷美",
    "Military": "",
    "Partner": "金大昇",
    "Marriage": "已婚",
    "Birthplace": "臺北市",
    "HouseholdAddr1": "臺北市內湖區",
    "HouseholdAddr2": "葫洲里",
    "HouseholdAddr3": "1民權東路六段",
    "HouseholdAddr4": "283巷15弄218號"
}
```

### APIM 發查戶役政

### SEND_JCIC 呼叫Z07查詢

### CHECK_Z07_RESULT 呼叫Z07查詢

### GET_KYC_DEBT_INFO 呼叫外部負債資料查詢

### 產生KYC表