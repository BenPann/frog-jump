﻿/***************
共用頁首載入
***************/
var headerXhr=new XMLHttpRequest();

headerXhr.open('GET', '../header.html', true);
headerXhr.send();

headerXhr.onreadystatechange=function(){
	try{

		if(headerXhr.readyState==4 && headerXhr.status==200 && !header.classList.contains('header-progress')){
			header.innerHTML=headerXhr.responseText;
		}
	}catch(err) {
		
	}
};


/***************
共用頁尾載入
***************/
var footerXhr=new XMLHttpRequest();

footerXhr.open('GET', '../footer.html', true);
footerXhr.send();

footerXhr.onreadystatechange=function(){
	if(footerXhr.readyState==4 && footerXhr.status==200 && window['footer']){
		footer.innerHTML=footerXhr.responseText;
	}
};


/***************
cleave.js格式檢核

1. 限輸數字（檔英文/注音/貼上）
2. 給數字套上格式
***************/

/*日期檢核 .cleave-date*/
var cleaveDates=document.querySelectorAll('.cleave-date');
for(i=0;i<cleaveDates.length;i++){
	new Cleave(cleaveDates[i], {
		date: true,
		datePattern: ['Y', 'm', 'd'],
	});
}

/*小寫英文自動轉大寫 .cleave-upper*/
var cleaveUppers=document.querySelectorAll('.cleave-upper');
for(i=0;i<cleaveUppers.length;i++){
	new Cleave(cleaveUppers[i], {
		uppercase: true,
		blocks: [9999],
	});
}

/*僅輸入數字 .cleave-number*/
var cleaveNumbers=document.querySelectorAll('.cleave-number');
for(i=0;i<cleaveNumbers.length;i++){
	new Cleave(cleaveNumbers[i], {
		numericOnly: true,
		blocks: [9999],
	});
}

/*僅輸入金額（會抓首碼是不是填0） .cleave-money*/
var cleaveMoneys=document.querySelectorAll('.cleave-money');
for(i=0;i<cleaveMoneys.length;i++){
	new Cleave(cleaveMoneys[i], {
		numeral: true,
		numeralThousandsGroupStyle: 'thousand'
	});
}

/***************
電話號碼檢核

1. 必填未填訊息改成「如無市話，請填寫手機號碼」
2. 抓出區碼自動帶「-」

※ telValidate的function一定要放在 blur簡單格式檢核 的上面（必須先觸發）
不然「如無市話請填寫手機號碼」會帶不進去
***************/
function telValidate(th){
	var value=th.value;

	if(th.validity.valueMissing==true){
		th.setCustomValidity('如無市話，請填寫手機號碼');
	}else{
		th.setCustomValidity('');
	}

	if(value.search('-')==-1){
		var newValue;

		if(value.search('0826')==0){
			newValue=value.replace('0826', '0826-');
		}else if(value.search('0836')==0){
			newValue=value.replace('0836', '0836-');
		}else if(value.search('082')==0){
			newValue=value.replace('082', '082-');
		}else if(value.search('089')==0){
			newValue=value.replace('089', '089-');
		}else if(value.search('049')==0){
			newValue=value.replace('049', '049-');
		}else if(value.search('037')==0){
			newValue=value.replace('037', '037-');
		}else {
			newValue=value
			.replace(/^08/, '08-')
			.replace(/^07/, '07-')
			.replace(/^06/, '06-')
			.replace(/^05/, '05-')
			.replace(/^04/, '04-')
			.replace(/^03/, '03-')
			.replace(/^02/, '02-');
		}

		th.value=newValue;
	}
}

/***************
身分證末碼檢核

身分證欄位掛上.id-validate
***************/
function idValidate(th){
	if(checkID(th.value)==false && th.checkValidity()==true){
		th.setCustomValidity('此身分證字號有誤');
	}else{
		th.setCustomValidity('');
	}
}

/***************
blur簡單格式檢核

1. blur後觸發
2. 檢核必填/簡單regex格式
3. 檢核input type
4. 錯誤訊息拿原生噴的

memo: checkbox/radio不用也不能做blur檢核
因為checkbox/radio沒勾的話「下一步」按鈕連案也不能案，所以不需要
***************/
var textInputs=document.querySelectorAll('.form-item [required]:not([type="checkbox"]):not([type="radio"])');
for(i=0;i<textInputs.length;i++){
	textInputs[i].addEventListener('blur', function(){
		this.classList.add('need-valid');
		this.nextElementSibling.textContent=this.validationMessage;
	})
}


/***************
button由灰轉藍的效果
***************/
var allInputs=document.querySelectorAll('.form-item [required]');
for(i=0;i<allInputs.length;i++){
	function checkValid(th){
		var form=th.form;
		var button=form.querySelector('button.btn-submit');
		if(form.checkValidity()==true){
			button.disabled=false;
		}else{
			button.disabled=true;
		}
	}

	allInputs[i].addEventListener('change', function(){
		var that=this;
		setTimeout(function(){
			checkValid(that); //IE下拉選單會有延遲問題
		}, 300);
	});

	allInputs[i].addEventListener('input', function(){
		checkValid(this);
	});

	allInputs[i].addEventListener('blur', function(){
		checkValid(this);
	});

}

/***************
簡訊驗證碼 輸入完畢後自動focus到下一個
***************/
var rapidInputs=document.querySelectorAll('.rapid-form input');
var rapidInputsArray=Array.prototype.slice.call(rapidInputs);

rapidInputsArray.forEach(function(item, idx){
	item.addEventListener('input', function(){
		var nextInput=rapidInputs[idx+1];
		if(this.value.length==this.maxLength){
			if(nextInput){
				nextInput.focus();
			}else{
				document.forms[0].submit();
			}
		}
	})
})


/***************
Dialog Popup
***************/
var bodyWrapper=document.body.children[1];
var bodyScrollBarWidth=innerWidth-bodyWrapper.clientWidth; //body scroll bar
// var contactScrollBarWidth=window.innerWidth-contact.clientWidth-side.clientWidth;
var nowOpenedDialog;
/*關閉Dialog：要關閉的組件套上.dialog-close*/
function closeDialog(){
	if(nowOpenedDialog){
		nowOpenedDialog.classList.remove('dialog-show');
		var otherDialog=document.querySelector('.dialog-show');
		if(otherDialog){
			nowOpenedDialog=otherDialog;
			return;
		}else {
			document.body.classList.remove('has-dialog');
			bodyWrapper.style.paddingRight='';
		}
	}	
}

function closeAllDialog(){
	document.body.classList.remove('has-dialog');
	bodyWrapper.style.paddingRight='';
	var nowDialogs=document.querySelectorAll('.dialog-show');
	for(i=0;i<nowDialogs.length;i++){
		nowDialogs[i].classList.remove('dialog-show');
	}
}

var CloseButtons=document.querySelectorAll('.dialog-close');
for(i=0;i<CloseButtons.length;i++){
	CloseButtons[i].addEventListener('click', closeDialog);
}

// 關閉全部
var allCloseButtons=document.querySelectorAll('.dialog-close-all');
for(i=0;i<allCloseButtons.length;i++){
	allCloseButtons[i].addEventListener('click', closeAllDialog);
}

/*呼叫Dialog：套上onclick="showDialog('#dialog1')"*/
function showDialog(target){
	nowOpenedDialog=document.querySelector(target);
	bodyWrapper.style.paddingRight=bodyScrollBarWidth+'px';
	document.body.classList.add('has-dialog');
	nowOpenedDialog.classList.add('dialog-show');

	window.addEventListener('keydown', function(e){
		if(e.keyCode==27){
			closeDialog();
		}
	})
}

function showCommonMsg(inMessageText,inBtnText,inBtnHrefLink){

	nowOpenedDialog=document.querySelector("#dialogCommon");
	
	var msgtext = nowOpenedDialog.querySelector(".dialogCommon-msg");
	msgtext.innerHTML=inMessageText;
	
	var btntext = nowOpenedDialog.querySelector(".btn-submit");
	if (inBtnText!="" && inBtnText!=undefined ){btntext.textContent=inBtnText;}
	
	
	bodyWrapper.style.paddingRight=bodyScrollBarWidth+'px';
	document.body.classList.add('has-dialog');
	nowOpenedDialog.classList.add('dialog-show');

	window.addEventListener('keydown', function(e){
		if(e.keyCode==27){
			closeDialog();
		}
	})


	
	
}

/***************
行政區下拉選單
***************/
var cityjson={
	"基隆市": [
		{"code": 200, "text": "仁愛區"}, 
		{"code": 201, "text": "信義區"}, 
		{"code": 202, "text": "中正區"}, 
		{"code": 203, "text": "中山區"}, 
		{"code": 204, "text": "安樂區"}, 
		{"code": 205, "text": "暖暖區"}, 
		{"code": 206, "text": "七堵區"}, 
	],

	"台北市": [
		{"code": 100, "text": "中正區",}, 
		{"code": 103, "text": "大同區",}, 
		{"code": 104, "text": "中山區",}, 
		{"code": 105, "text": "松山區",}, 
		{"code": 106, "text": "大安區",}, 
		{"code": 108, "text": "萬華區",}, 
		{"code": 110, "text": "信義區",}, 
		{"code": 111, "text": "士林區",}, 
		{"code": 112, "text": "北投區",}, 
		{"code": 114, "text": "內湖區",}, 
		{"code": 115, "text": "南港區",}, 
		{"code": 116, "text": "文山區",},
	],


	"新北市": [
		{"code": 207, "text": "萬里區",}, 
		{"code": 208, "text": "金山區",}, 
		{"code": 220, "text": "板橋區",}, 
		{"code": 221, "text": "汐止區",}, 
		{"code": 222, "text": "深坑區",}, 
		{"code": 223, "text": "石碇區",}, 
		{"code": 224, "text": "瑞芳區",}, 
		{"code": 226, "text": "平溪區",}, 
		{"code": 227, "text": "雙溪區",}, 
		{"code": 228, "text": "貢寮區",}, 
		{"code": 231, "text": "新店區",}, 
		{"code": 232, "text": "坪林區",}, 
		{"code": 233, "text": "烏來區",}, 
		{"code": 234, "text": "永和區",}, 
		{"code": 235, "text": "中和區",}, 
		{"code": 236, "text": "土城區",}, 
		{"code": 237, "text": "三峽區",}, 
		{"code": 238, "text": "樹林區",}, 
		{"code": 239, "text": "鶯歌區",}, 
		{"code": 241, "text": "三重區",}, 
		{"code": 242, "text": "新莊區",}, 
		{"code": 243, "text": "泰山區",}, 
		{"code": 244, "text": "林口區",}, 
		{"code": 247, "text": "蘆洲區",}, 
		{"code": 248, "text": "五股區",}, 
		{"code": 249, "text": "八里區",}, 
		{"code": 251, "text": "淡水區",}, 
		{"code": 252, "text": "三芝區",}, 
		{"code": 253, "text": "石門區",},
	],

	"桃園市": [
		{"code":  320, "text": "中壢區",}, 
		{"code":  324, "text": "平鎮區",}, 
		{"code":  325, "text": "龍潭區",}, 
		{"code":  326, "text": "楊梅區",}, 
		{"code":  327, "text": "新屋區",}, 
		{"code":  328, "text": "觀音區",}, 
		{"code":  330, "text": "桃園區",}, 
		{"code":  333, "text": "龜山區",}, 
		{"code":  334, "text": "八德區",}, 
		{"code":  335, "text": "大溪區",}, 
		{"code":  336, "text": "復興區",}, 
		{"code":  337, "text": "大園區",}, 
		{"code":  338, "text": "蘆竹區",},
	],

	"宜蘭縣": [
		{"code": 260, "text": "宜蘭市",}, 
		{"code": 261, "text": "頭城鎮",}, 
		{"code": 262, "text": "礁溪鄉",}, 
		{"code": 263, "text": "壯圍鄉",}, 
		{"code": 264, "text": "員山鄉",}, 
		{"code": 265, "text": "羅東鎮",}, 
		{"code": 266, "text": "三星鄉",}, 
		{"code": 267, "text": "大同鄉",}, 
		{"code": 268, "text": "五結鄉",}, 
		{"code": 269, "text": "冬山鄉",}, 
		{"code": 270, "text": "蘇澳鎮",}, 
		{"code": 272, "text": "南澳鄉",}, 
		{"code": 290, "text": "釣魚臺",},
	],


	"新竹縣": [
		{"code": 302, "text": "竹北市",}, 
		{"code": 303, "text": "湖口鄉",}, 
		{"code": 304, "text": "新豐鄉",}, 
		{"code": 305, "text": "新埔鎮",}, 
		{"code": 306, "text": "關西鎮",}, 
		{"code": 307, "text": "芎林鄉",}, 
		{"code": 308, "text": "寶山鄉",}, 
		{"code": 310, "text": "竹東鎮",}, 
		{"code": 311, "text": "五峰鄉",}, 
		{"code": 312, "text": "橫山鄉",}, 
		{"code": 313, "text": "尖石鄉",}, 
		{"code": 314, "text": "北埔鄉",}, 
		{"code": 315, "text": "峨眉鄉",},
	],

	"新竹市": [
		{"code": 300, "text": "東區",},
		{"code": 300, "text": "北區",},
		{"code": 300, "text": "香山區",},
	],

	"苗栗縣": [
		{"code": 350, "text": "竹南鎮",}, 
		{"code": 351, "text": "頭份市",}, 
		{"code": 352, "text": "三灣鄉",}, 
		{"code": 353, "text": "南庄鄉",}, 
		{"code": 354, "text": "獅潭鄉",}, 
		{"code": 356, "text": "後龍鎮",}, 
		{"code": 357, "text": "通霄鎮",}, 
		{"code": 358, "text": "苑裡鎮",}, 
		{"code": 360, "text": "苗栗市",}, 
		{"code": 361, "text": "造橋鄉",}, 
		{"code": 362, "text": "頭屋鄉",}, 
		{"code": 363, "text": "公館鄉",}, 
		{"code": 364, "text": "大湖鄉",}, 
		{"code": 365, "text": "泰安鄉",}, 
		{"code": 366, "text": "銅鑼鄉",}, 
		{"code": 367, "text": "三義鄉",}, 
		{"code": 368, "text": "西湖鄉",}, 
		{"code": 369, "text": "卓蘭鎮",},
	],

	"台中市": [
		{"code": 400, "text": "中區",}, 
		{"code": 401, "text": "東區",}, 
		{"code": 402, "text": "南區",}, 
		{"code": 403, "text": "西區",}, 
		{"code": 404, "text": "北區",}, 
		{"code": 406, "text": "北屯區",}, 
		{"code": 407, "text": "西屯區",}, 
		{"code": 408, "text": "南屯區",}, 
		{"code": 411, "text": "太平區",}, 
		{"code": 412, "text": "大里區",}, 
		{"code": 413, "text": "霧峰區",}, 
		{"code": 414, "text": "烏日區",}, 
		{"code": 420, "text": "豐原區",}, 
		{"code": 421, "text": "后里區",}, 
		{"code": 422, "text": "石岡區",}, 
		{"code": 423, "text": "東勢區",}, 
		{"code": 424, "text": "和平區",}, 
		{"code": 426, "text": "新社區",}, 
		{"code": 427, "text": "潭子區",}, 
		{"code": 428, "text": "大雅區",}, 
		{"code": 429, "text": "神岡區",}, 
		{"code": 432, "text": "大肚區",}, 
		{"code": 433, "text": "沙鹿區",}, 
		{"code": 434, "text": "龍井區",}, 
		{"code": 435, "text": "梧棲區",}, 
		{"code": 436, "text": "清水區",}, 
		{"code": 437, "text": "大甲區",}, 
		{"code": 438, "text": "外埔區",}, 
		{"code": 439, "text": "大安區",},
	],

	"南投縣": [
		{"code": 540, "text": "南投市",}, 
		{"code": 541, "text": "中寮鄉",}, 
		{"code": 542, "text": "草屯鎮",}, 
		{"code": 544, "text": "國姓鄉",}, 
		{"code": 545, "text": "埔里鎮",}, 
		{"code": 546, "text": "仁愛鄉",}, 
		{"code": 551, "text": "名間鄉",}, 
		{"code": 552, "text": "集集鎮",}, 
		{"code": 553, "text": "水里鄉",}, 
		{"code": 555, "text": "魚池鄉",}, 
		{"code": 556, "text": "信義鄉",}, 
		{"code": 557, "text": "竹山鎮",}, 
		{"code": 558, "text": "鹿谷鄉",},
	],

	"彰化縣": [
		{"code": 500, "text": "彰化市",}, 
		{"code": 502, "text": "芬園鄉",}, 
		{"code": 503, "text": "花壇鄉",}, 
		{"code": 504, "text": "秀水鄉",}, 
		{"code": 505, "text": "鹿港鎮",}, 
		{"code": 506, "text": "福興鄉",}, 
		{"code": 507, "text": "線西鄉",}, 
		{"code": 508, "text": "和美鎮",}, 
		{"code": 509, "text": "伸港鄉",}, 
		{"code": 510, "text": "員林市",}, 
		{"code": 511, "text": "社頭鄉",}, 
		{"code": 512, "text": "永靖鄉",}, 
		{"code": 513, "text": "埔心鄉",}, 
		{"code": 514, "text": "溪湖鎮",}, 
		{"code": 515, "text": "大村鄉",}, 
		{"code": 516, "text": "埔鹽鄉",}, 
		{"code": 520, "text": "田中鎮",}, 
		{"code": 521, "text": "北斗鎮",}, 
		{"code": 522, "text": "田尾鄉",}, 
		{"code": 523, "text": "埤頭鄉",}, 
		{"code": 524, "text": "溪州鄉",}, 
		{"code": 525, "text": "竹塘鄉",}, 
		{"code": 526, "text": "二林鎮",}, 
		{"code": 527, "text": "大城鄉",}, 
		{"code": 528, "text": "芳苑鄉",}, 
		{"code": 530, "text": "二水鄉",},
	],

	"雲林縣": [
		{"code": 630, "text": "斗南鎮",}, 
		{"code": 631, "text": "大埤鄉",}, 
		{"code": 632, "text": "虎尾鎮",}, 
		{"code": 633, "text": "土庫鎮",}, 
		{"code": 634, "text": "褒忠鄉",}, 
		{"code": 635, "text": "東勢鄉",}, 
		{"code": 636, "text": "臺西鄉",}, 
		{"code": 637, "text": "崙背鄉",}, 
		{"code": 638, "text": "麥寮鄉",}, 
		{"code": 640, "text": "斗六市",}, 
		{"code": 643, "text": "林內鄉",}, 
		{"code": 646, "text": "古坑鄉",}, 
		{"code": 647, "text": "莿桐鄉",}, 
		{"code": 648, "text": "西螺鎮",}, 
		{"code": 649, "text": "二崙鄉",}, 
		{"code": 651, "text": "北港鎮",}, 
		{"code": 652, "text": "水林鄉",}, 
		{"code": 653, "text": "口湖鄉",}, 
		{"code": 654, "text": "四湖鄉",}, 
		{"code": 655, "text": "元長鄉",},
	],

	"嘉義縣": [
		{"code": 602, "text": "番路鄉",}, 
		{"code": 603, "text": "梅山鄉",}, 
		{"code": 604, "text": "竹崎鄉",}, 
		{"code": 605, "text": "阿里山鄉",}, 
		{"code": 606, "text": "中埔鄉",}, 
		{"code": 607, "text": "大埔鄉",}, 
		{"code": 608, "text": "水上鄉",}, 
		{"code": 611, "text": "鹿草鄉",}, 
		{"code": 612, "text": "太保市",}, 
		{"code": 613, "text": "朴子市",}, 
		{"code": 614, "text": "東石鄉",}, 
		{"code": 615, "text": "六腳鄉",}, 
		{"code": 616, "text": "新港鄉",}, 
		{"code": 621, "text": "民雄鄉",}, 
		{"code": 622, "text": "大林鎮",}, 
		{"code": 623, "text": "溪口鄉",}, 
		{"code": 624, "text": "義竹鄉",}, 
		{"code": 625, "text": "布袋鎮",},
	],

	"嘉義市": [
		{"code": 600, "text": "東區",},
		{"code": 600, "text": "西區",},
	],

	"台南市": [
		{"code": 700, "text": "中西區",}, 
		{"code": 701, "text": "東區",}, 
		{"code": 702, "text": "南區",}, 
		{"code": 704, "text": "北區",}, 
		{"code": 708, "text": "安平區",}, 
		{"code": 709, "text": "安南區",}, 
		{"code": 710, "text": "永康區",}, 
		{"code": 711, "text": "歸仁區",}, 
		{"code": 712, "text": "新化區",}, 
		{"code": 713, "text": "左鎮區",}, 
		{"code": 714, "text": "玉井區",}, 
		{"code": 715, "text": "楠西區",}, 
		{"code": 716, "text": "南化區",}, 
		{"code": 717, "text": "仁德區",}, 
		{"code": 718, "text": "關廟區",}, 
		{"code": 719, "text": "龍崎區",}, 
		{"code": 720, "text": "官田區",}, 
		{"code": 721, "text": "麻豆區",}, 
		{"code": 722, "text": "佳里區",}, 
		{"code": 723, "text": "西港區",}, 
		{"code": 724, "text": "七股區",}, 
		{"code": 725, "text": "將軍區",}, 
		{"code": 726, "text": "學甲區",}, 
		{"code": 727, "text": "北門區",}, 
		{"code": 730, "text": "新營區",}, 
		{"code": 731, "text": "後壁區",}, 
		{"code": 732, "text": "白河區",}, 
		{"code": 733, "text": "東山區",}, 
		{"code": 734, "text": "六甲區",}, 
		{"code": 735, "text": "下營區",}, 
		{"code": 736, "text": "柳營區",}, 
		{"code": 737, "text": "鹽水區",}, 
		{"code": 741, "text": "善化區",}, 
		{"code": 742, "text": "大內區",}, 
		{"code": 743, "text": "山上區",}, 
		{"code": 744, "text": "新市區",}, 
		{"code": 745, "text": "安定區",},
	],

	"高雄市": [
		{"code": 800, "text": "新興區",}, 
		{"code": 801, "text": "前金區",}, 
		{"code": 802, "text": "苓雅區",}, 
		{"code": 803, "text": "鹽埕區",}, 
		{"code": 804, "text": "鼓山區",}, 
		{"code": 805, "text": "旗津區",}, 
		{"code": 806, "text": "前鎮區",}, 
		{"code": 807, "text": "三民區",}, 
		{"code": 811, "text": "楠梓區",}, 
		{"code": 812, "text": "小港區",}, 
		{"code": 813, "text": "左營區",}, 
		{"code": 814, "text": "仁武區",}, 
		{"code": 815, "text": "大社區",}, 
		{"code": 817, "text": "東沙群島",}, 
		{"code": 819, "text": "南沙群島",}, 
		{"code": 820, "text": "岡山區",}, 
		{"code": 821, "text": "路竹區",}, 
		{"code": 822, "text": "阿蓮區",}, 
		{"code": 823, "text": "田寮區",}, 
		{"code": 824, "text": "燕巢區",}, 
		{"code": 825, "text": "橋頭區",}, 
		{"code": 826, "text": "梓官區",}, 
		{"code": 827, "text": "彌陀區",}, 
		{"code": 828, "text": "永安區",}, 
		{"code": 829, "text": "湖內區",}, 
		{"code": 830, "text": "鳳山區",}, 
		{"code": 831, "text": "大寮區",}, 
		{"code": 832, "text": "林園區",}, 
		{"code": 833, "text": "鳥松區",}, 
		{"code": 840, "text": "大樹區",}, 
		{"code": 842, "text": "旗山區",}, 
		{"code": 843, "text": "美濃區",}, 
		{"code": 844, "text": "六龜區",}, 
		{"code": 845, "text": "內門區",}, 
		{"code": 846, "text": "杉林區",}, 
		{"code": 847, "text": "甲仙區",}, 
		{"code": 848, "text": "桃源區",}, 
		{"code": 849, "text": "那瑪夏區",}, 
		{"code": 851, "text": "茂林區",}, 
		{"code": 852, "text": "茄萣區",},
	],

	"屏東縣": [
		{"code": 900, "text": "屏東市",}, 
		{"code": 901, "text": "三地門鄉",}, 
		{"code": 902, "text": "霧臺鄉",}, 
		{"code": 903, "text": "瑪家鄉",}, 
		{"code": 904, "text": "九如鄉",}, 
		{"code": 905, "text": "里港鄉",}, 
		{"code": 906, "text": "高樹鄉",}, 
		{"code": 907, "text": "鹽埔鄉",}, 
		{"code": 908, "text": "長治鄉",}, 
		{"code": 909, "text": "麟洛鄉",}, 
		{"code": 911, "text": "竹田鄉",}, 
		{"code": 912, "text": "內埔鄉",}, 
		{"code": 913, "text": "萬丹鄉",}, 
		{"code": 920, "text": "潮州鎮",}, 
		{"code": 921, "text": "泰武鄉",}, 
		{"code": 922, "text": "來義鄉",}, 
		{"code": 923, "text": "萬巒鄉",}, 
		{"code": 924, "text": "崁頂鄉",}, 
		{"code": 925, "text": "新埤鄉",}, 
		{"code": 926, "text": "南州鄉",}, 
		{"code": 927, "text": "林邊鄉",}, 
		{"code": 928, "text": "東港鎮",}, 
		{"code": 929, "text": "琉球鄉",}, 
		{"code": 931, "text": "佳冬鄉",}, 
		{"code": 932, "text": "新園鄉",}, 
		{"code": 940, "text": "枋寮鄉",}, 
		{"code": 941, "text": "枋山鄉",}, 
		{"code": 942, "text": "春日鄉",}, 
		{"code": 943, "text": "獅子鄉",}, 
		{"code": 944, "text": "車城鄉",}, 
		{"code": 945, "text": "牡丹鄉",}, 
		{"code": 946, "text": "恆春鎮",}, 
		{"code": 947, "text": "滿州鄉",},
	],

	"花蓮縣": [
		{"code": 970, "text": "花蓮市",}, 
		{"code": 971, "text": "新城鄉",}, 
		{"code": 972, "text": "秀林鄉",}, 
		{"code": 973, "text": "吉安鄉",}, 
		{"code": 974, "text": "壽豐鄉",}, 
		{"code": 975, "text": "鳳林鎮",}, 
		{"code": 976, "text": "光復鄉",}, 
		{"code": 977, "text": "豐濱鄉",}, 
		{"code": 978, "text": "瑞穗鄉",}, 
		{"code": 979, "text": "萬榮鄉",}, 
		{"code": 981, "text": "玉里鎮",}, 
		{"code": 982, "text": "卓溪鄉",}, 
		{"code": 983, "text": "富里鄉",},
	],

	"台東縣": [
		{"code": 950, "text": "臺東市",}, 
		{"code": 951, "text": "綠島鄉",}, 
		{"code": 952, "text": "蘭嶼鄉",}, 
		{"code": 953, "text": "延平鄉",}, 
		{"code": 954, "text": "卑南鄉",}, 
		{"code": 955, "text": "鹿野鄉",}, 
		{"code": 956, "text": "關山鎮",}, 
		{"code": 957, "text": "海端鄉",}, 
		{"code": 958, "text": "池上鄉",}, 
		{"code": 959, "text": "東河鄉",}, 
		{"code": 961, "text": "成功鎮",}, 
		{"code": 962, "text": "長濱鄉",},
		{"code": 963, "text": "太麻里鄉",}, 
		{"code": 964, "text": "金峰鄉",}, 
		{"code": 965, "text": "大武鄉",}, 
		{"code": 966, "text": "達仁鄉",},
	],

	"澎湖縣": [
		{"code": 880, "text": "馬公市",}, 
		{"code": 881, "text": "西嶼鄉",}, 
		{"code": 882, "text": "望安鄉",}, 
		{"code": 883, "text": "七美鄉",}, 
		{"code": 884, "text": "白沙鄉",}, 
		{"code": 885, "text": "湖西鄉",},
	],

	"金門縣": [
		{"code": 890, "text": "金沙鎮",}, 
		{"code": 891, "text": "金湖鎮",}, 
		{"code": 892, "text": "金寧鄉",}, 
		{"code": 893, "text": "金城鎮",}, 
		{"code": 894, "text": "烈嶼鄉",}, 
		{"code": 896, "text": "烏坵鄉",},
	],

	"連江縣": [
		{"code": 209, "text": "南竿鄉",}, 
		{"code": 210, "text": "北竿鄉",}, 
		{"code": 211, "text": "莒光鄉",}, 
		{"code": 212, "text": "東引鄉",},
	],
}

function makeDistrict(th, districtSelect){
	var districtEl=document.querySelector(districtSelect);
	var defaultOption=districtEl.children[0].cloneNode('deep');

	districtEl.innerHTML='';
	districtEl.appendChild(defaultOption);
	districtEl.classList.remove('need-valid');

	for(i=0;i<cityjson[th.value].length;i++){
		var node=document.createElement('option');
		node.value=cityjson[th.value][i].code;
		node.textContent=cityjson[th.value][i].text;
		districtEl.appendChild(node);
	}
}

var phonejson={
	基隆市: '02',
	台北市: '02',
	新北市: '02',
	桃園市: '03',
	宜蘭縣: '03',
	新竹縣: '03',
	新竹市: '03',
	苗栗縣: '037',
	台中市: '04',
	南投縣: '049',
	彰化縣: '04',
	雲林縣: '05',
	嘉義縣: '05',
	嘉義市: '05',
	台南市: '06',
	高雄市: '07',
	屏東市: '08',
	花蓮縣: '03',
	台東縣: '089',
	澎湖縣: '06',
	金門縣: '0826',
	連江縣: '0836',
}

/***************
選好縣市後自動帶入公司電話
***************/
function prefixFill(th, target){
	var targetEl=document.querySelector(target);
	if(targetEl.value!=='') return;
	targetEl.value=phonejson[th.value]+'-';
}

/***************
輸入框候選清單 操作動態
***************/

//共用函式 關閉候選清單
function closeCandidate(target){
	var myinput=target.querySelector('input');
	target.classList.remove('has-candidate');
	myinput.focus();
	myinput.blur(); //跑檢核
}

//共用函式 帶入選項
function importValue(th){
	var mywrapper=th.parentNode.parentNode;
	var myinput=mywrapper.querySelector('input');

	myinput.value=th.textContent;
	closeCandidate(mywrapper);
}


var candidateItems=document.querySelectorAll('.candidate .candidate-item');
for(i=0;i<candidateItems.length;i++){

	//上下鍵選取、Enter鍵帶入、Esc鍵關閉候選清單
	candidateItems[i].addEventListener('keydown', function(e){
		var nextEl=this.nextElementSibling;
		var prevEl=this.previousElementSibling;
		var mywrapper=this.parentNode.parentNode;

		if(e.keyCode==27){
			closeCandidate(mywrapper);
		}

		if(e.keyCode==13){
			importValue(this);
		}

		if(e.keyCode==40){
			if(nextEl){
				nextEl.focus();
			}
		}

		if(e.keyCode==38){
			if(prevEl){
				prevEl.focus();
			}
		}
	})

	//滑鼠點選後帶入值
	candidateItems[i].addEventListener('click', function(e){
		importValue(this);
	})
}

//開啟候選清單後任意按畫面一個地方可以關掉它
window.addEventListener('click', function(e){
	var mywrapper=document.querySelector('.has-candidate');
	if(mywrapper && e.target!==mywrapper.querySelector('input')){
		closeCandidate(mywrapper);
	}
})

//email候選清單
function dedicateMail(myvalue, th){
	var mylist=th.parentNode.querySelector('.candidate');
	var mywrapper=mylist.parentNode;

	if(/@$/.test(myvalue)){
		mywrapper.classList.add('has-candidate');
		// mylist.children[0].focus();
		mylist.children[0].textContent=myvalue+'gmail.com';
		mylist.children[1].textContent=myvalue+'yahoo.com.tw';
		mylist.children[2].textContent=myvalue+'hotmail.com';
		mylist.children[3].textContent=myvalue+'hinet.net';
		//清空檢核紅字
		th.nextElementSibling.textContent='';

		th.onkeydown=function(e){
			if(e.keyCode==40){
				mylist.children[0].focus();
			}

			if(e.keyCode==27){
				closeCandidate(mywrapper);
			}
		}
	}else{
		mywrapper.classList.remove('has-candidate');
		th.onkeydown='';
	}
}

//公司名稱候選清單
function dedicateCompany(myvalue, th){
	var mylist=th.parentNode.querySelector('.candidate');
	var mywrapper=mylist.parentNode;

	if(/./.test(myvalue)){
		mywrapper.classList.add('has-candidate');
		// mylist.children[0].focus();
		th.onkeydown=function(e){
			if(e.keyCode==40){
				mylist.children[0].focus();
			}

			if(e.keyCode==27){
				closeCandidate(mywrapper);
			}
		}
	}else{
		mywrapper.classList.remove('has-candidate');
		th.onkeydown='';
	}
}

//LINE Browser 輸入框收回後頁面回不來的bug
if(/Android.*Line/.test(navigator.userAgent)==true){
	setInterval(function(){
		document.querySelector('.body-wrapper').scrollBy(0,0);
	},1000);
}

//身分證檢查(長度、尾碼計算)
function checkID(id){

	tab = "ABCDEFGHJKLMNPQRSTUVXYWZIO"                    

		A1 = new Array (1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3 );
		A2 = new Array (0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5 );
		Mx = new Array (9,8,7,6,5,4,3,2,1,1);

		if ( id.length != 10 ) return false;
		i = tab.indexOf( id.charAt(0) );
		if ( i == -1 ) return false;
		sum = A1[i] + A2[i]*9;

		for ( i=1; i<10; i++ ) {
			v = parseInt( id.charAt(i) );
			if ( isNaN(v) ) return false;
			sum = sum + v * Mx[i];
		}

		if ( sum % 10 != 0 ) return false;
		return true;
}

var G_CHATBOT_STATUS = "close";

function showChatBot(){
	
	if (G_CHATBOT_STATUS=="close"){
		//open it
		document.getElementById("SpiritAI").style.display = "block";
		//document.getElementById("talk-triangle-top").style.display = "block";
		//document.getElementById("talk-triangle-base").style.display = "block";
		G_CHATBOT_STATUS = "open";
	}else{
		
		if (G_CHATBOT_STATUS=="open"){
			//open it
			document.getElementById("SpiritAI").style.display = "none";
			//document.getElementById("talk-triangle-top").style.display = "none";
			//document.getElementById("talk-triangle-base").style.display = "none";
			G_CHATBOT_STATUS = "close";
		}		
		
	}
	
	

	
}


function closeChatBot(){
	
			document.getElementById("SpiritAI").style.display = "none";
			//document.getElementById("talk-triangle-top").style.display = "none";
			//document.getElementById("talk-triangle-base").style.display = "none";
			G_CHATBOT_STATUS = "close";

}


function showContactUsDialog(){
			
			closeChatBot();
			showDialog('#dialogContactMethod');
	
}




//禁止回上頁
window.history.forward(1);

/***************
手機因為CSS 100vh不會減去上下方瀏覽器導覽頁的高度導致東西有一點被切掉，改用innerHeight處理
***************/
var onePageItems=document.querySelectorAll('.fixed-panel, .slide-block');
function calculatePhoneSize(){
	if(onePageItems.length>0 && innerWidth<1001){
		for(i=0;i<onePageItems.length;i++){
			var phoneHeight;
			phoneHeight=innerHeight-header.clientHeight;

			onePageItems[i].style.minHeight=phoneHeight+'px';
		}
	}	
}
calculatePhoneSize()

window.addEventListener('orientationchange', calculatePhoneSize);


