### 異業 API 資料表(QR_ChannelDataUrl)

| PK  | NULL | 欄位         | 型態           | 說明                         | 代碼說明                           |
| --- | ---- | ------------ | -------------- | ---------------------------- | ---------------------------------- |
| v   |      | ChannelId    | nvarchar(2)    | 異業合作時須約定的 Channelid |                                    |
| v   |      | UrlNameType  | nvarchar(50)   | Url 名稱代碼                 | 1:getcif 2:getmemo 3:getcreditAuto |
|     |      | Enable       | char(1)        | 是否開啟反查異業資料         | 0:不開放 1:開放                    |
|     |      | UrlType      | char(1)        | 網址類型(內網或外網)         | 0:內網 1 :外網                     |
|     |      | Url          | nvarchar(2000) | 反查的 URL                   |                                    |
|     |      | UrlStartTime | datetime       | 取得資料的開始時間           |                                    |
|     |      | UrlEndTime   | datetime       | 取得資料的結束時間           |                                    |
