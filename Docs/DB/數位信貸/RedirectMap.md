### 轉址對應表(RedirectMap)
- 此Table為CMS使用外部

| PK  | 欄位        | 型態          | 說明                                                    | 代碼說明 |
| --- | ----------- | ------------- | ------------------------------------------------------- | -------- |
| v   | Name        | nvarchar(50)  | 轉址的key 放在Redirect後面                              |          |
|     | Url         | nvarchar(500) | 要轉址的網頁 放{depart} 可取得部門 {member}可取得使用者 |          |
|     | Description | nvarchar(200) | 中文描述                                                |          |

