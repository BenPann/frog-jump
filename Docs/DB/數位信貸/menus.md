###表單(menus)
| PK  | NULL | 欄位     | 型態           | 說明 | 代碼說明 |
| --- | ---- | -------- | -------------- | ---- | -------- |
| V   |      | menuId   | int            |      |          |
|     |      | menuName | nvarchar(50)   |      |          |
|     | V    | URL      | nvarchar(500), |      |          |
|     | V    | parentId | int,           |      |          |
|     | V    | itype    | int            |      |          |