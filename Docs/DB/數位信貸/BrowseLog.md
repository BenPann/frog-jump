###客戶瀏覽紀錄的 LOG 資料表(BrowseLog)

| PK  | 欄位     | 型態          | 說明         | 代碼說明                   |
| --- | -------- | ------------- | ------------ | -------------------------- |
| v   | Serial   | int           | 流水號       |                            |
|     | UniqId   | nvarchar(50)  | 序號         | CaseNo 或 PLNo 或 ConNo    |  |
|     | UniqType | char(2)       | 唯一代號類型 | 01 : 體驗 02: 申請 03:立約 |
|     | Page     | nvarchar(200) | 瀏覽頁面     |                            |
|     | Action   | nvarchar(200) | 動作         |                            |
|     | UTime    | datetime      | 瀏覽時間     |                            |
