### 信用卡或存款帳號驗證狀態 (CreditVerify)

| PK  | NULL | 欄位         | 型態         | 說明           | 代碼說明                                      |
| --- | ---- | ------------ | ------------ | -------------- | --------------------------------------------- |
| v   |      | UniqId       | nvarchar(50) | 序號           | CaseNo 或 ExpNo 或 ConNo 或CreditNo           |
|     |      | UniqType     | char(2)      | 唯一代號類型   | 01:體驗 02:申請 03:立約 04:專人聯絡 05:信用卡 |
|     |      | VerifyStatus | char(1)      | 驗證狀態       | 0:驗證失敗 1:驗證成功                         |
|     |      | VerifyType   | char(1)      | 驗證類型       | 0:信用卡 1:存款帳戶 2:行動門號 4:線下驗身      |
|     |      | CardNo       | varchar(20)  | 信用卡卡號     | 卡號範例 1111********2222                     |
|     |      | CardTime     | varchar(8)   | 信用卡到期年月 | 到期範例 : MMYY 月份 西元年後兩碼             |
|     |      | BankId       | varchar(10)  | 存款銀行       |                                               |
|     |      | AccountNo    | varchar(20)  | 存款帳號       |                                               |
|     |      | ErrorCount   | int          | 錯誤次數       | 只能錯三次 所以最多到3                        |
|     |      | CreateTime   | datetime     | 新增時間       |                                               |
|     |      | UpdateTime   | datetime     | 更新時間       |                                               |



