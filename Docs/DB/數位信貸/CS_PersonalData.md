### 跨售個人資料 (CS_PersonalData)

| PK  | NULL | 欄位               | 型態          | 說明                | 代碼說明                                                |
| --- | ---- | ------------------ | ------------- | ------------------- | ------------------------------------------------------- |
| v   |      | UniqId             | nvarchar(50)  | 序號                | CaseNo 或 PLNo 或 ConNo 或CreditNo                      |
|     |      | UniqType           | char(2)       | 唯一代號類型        | 01 : 體驗 02: 申請 03:立約 04:專人聯絡 05:信用卡        |
|     |      | HasAum             | char(1)       | 是否有AUM資料       | 0 : 沒有 1: 有                                          |
|     |      | UpdateAum          | char(1)       | 更新AUM資料成功失敗 | 空白:未更新  0 : 更新失敗 1: 更新成功                   |
|     |      | UUID               | nvarchar(50)  | UniqId              | AUM資料給KGI的時候 產生的特殊序號KSYYYYMMDDHHmmss000001 |
|     |      | Idno               | nvarchar(50)  | 身分證字號          |                                                         |
|     | V    | Name               | nvarchar(50)  | 姓名                |                                                         |
|     |      | Birthday           | nvarchar(50)  | 生日                |                                                         |
|     | V    | Mobile             | nvarchar(50)  | 手機號碼            |                                                         |
|     | V    | EMail              | nvarchar(200) | 電子信箱            |                                                         |
|     | V    | CommAddressZipCode | nvarchar(6)   | 通訊地址            |                                                         |
|     | V    | CommAddress        | nvarchar(200) | 通訊地址            |                                                         |
|     | V    | ResTelArea         | nvarchar(4)   | 戶籍電話            |                                                         |
|     | V    | ResPhone           | nvarchar(50)  | 戶籍電話            |                                                         |
|     |      | CreateTime         | datetime      | 新增時間            |                                                         |