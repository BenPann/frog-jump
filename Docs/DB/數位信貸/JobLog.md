###排程的紀錄資料表 (JobLog)

|PK|欄位|型態|說明|代碼說明|
|--|--|--|--|--|
| v | Serial  | int           | 序號    |  由資料庫自動產生                   |  | 
|   | JobName | nvarchar(50)  | Job名稱 |                             |  | 
|   | Level   | int           | 訊息等級  | 1:Info 2:資料異常 3:連線中斷 4:系統錯誤 |  | 
|   | Message | nvarchar(max) | 訊息內容  |                             |  | 
|   | UTime   | datetime      | 更新時間  |                             |  | 

