### 案件推廣人 (QR_PromoCase)

| PK  | NULL | 欄位         | 型態         | 說明          | 代碼說明                                      |
| --- | ---- | ------------ | ------------ | ------------- | --------------------------------------------- |
| V   |      | UniqId       | nvarchar(50) | 唯一序號      | CaseNo 或 ExpNo 或 ConNo 或CreditNo           |
|     |      | UniqType     | char(2)      | 唯一代號類型  | 01:體驗 02:申請 03:立約 04:專人聯絡 05:信用卡 |
|     |      | TransDepart  | nvarchar(50) | 轉介單位  | 貸款用                                        |
|     |      | TransMember  | nvarchar(50) | 轉介人員  | 貸款用                                        |
|     |      | PromoDepart  | nvarchar(50) | 推廣單位  | 貸款用                                        |
|     |      | PromoMember  | nvarchar(50) | 推廣人員  | 貸款用                                        |
|     |      | PromoDepart1 | nvarchar(50) | 推廣單位  | 信用卡用                                      |
|     |      | PromoMember1 | nvarchar(50) | 推廣人員  | 信用卡用                                      |
|     |      | PromoDepart2 | nvarchar(50) | 推廣單位2 | 信用卡用                                      |
|     |      | PromoMember2 | nvarchar(50) | 推廣人員2 | 信用卡用                                      |

### 每個案件會有推薦或轉介