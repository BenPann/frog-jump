### 信用卡案件 Log 資料 (CreditCaseApprovalLog)

| PK  | NULL | 欄位         | 型態          | 說明                 | 代碼說明              |
| --- | ---- | ------------ | ------------- | -------------------- | --------------------- |
| v   |      | UniqId       | nvarchar(50)  | 序號                 | CRED 開頭或 CASE 開頭 |
|     |      | Idno         | nvarchar(50)  | 身分證字號或護照號碼 |                       |
|     |      | ProductId    | nvarchar(50)  | 產品代號             | 信用卡代號            |
|     |      | CreditApsNo  | nvarchar(50)  | APS 的信用卡編號     |                       |
|     |      | ProductName  | nvarchar(50)  | 產品名稱             |                       |
|     |      | SupNo        | nvarchar(50)  |                      |                       |
|     |      | SupChtName   | nvarchar(50)  |                      |                       |
|     |      | Status       | varchar(5)    | 案件狀態             |                       |
|     |      | StatusDesc   | nvarchar(50)  |                      |                       |
|     |      | ProdFlg      | nvarchar(100) |                      |                       |
|     |      | SalesMarket  | nvarchar(100) |                      |                       |
|     |      | ChtName      | nvarchar(50)  | 中文姓名             |                       |
|     | v    | AP00Time     | datetime      |                      |                       |
|     | v    | ApprovedTime | datetime      |                      |                       |
|     |      | CrediteLimit | nvarchar(50)  |                      |                       |
|     |      | CreateTime   | datetime      | 新增時間             |                       |
|     |      | UpdateTime   | datetime      | 更新時間             |                       |
