###斷點接續 Table (BreakPoint)

| PK  | 欄位     | 型態          | 說明                 | 代碼說明                               |
| --- | -------- | ------------- | -------------------- | -------------------------------------- |
| v   | UniqId   | nvarchar(50)  | 連線資訊唯一代號     | 可能為 CaseNo,ExpNo,ConNo              |
|     | UniqType | char(2)       | 唯一代號類型         | 01 : 體驗 02: 申請 03:立約             |
|     | MainPage | nvarchar(100) | 最後使用頁面         | 客戶最後使用的頁面(表示該頁面已經完成) |
|     | SubPage  | nvarchar(100) | 最後使用頁面的子頁面 | 有些頁面可能會有多個步驟 在此紀錄      |
|     | IsEnd    | char(1)       | 是否結束             | 本次流程是否結束 0:尚未結束 1:結束     |
|     | TempData | nvarchar(max) | 暫存資料             | 如果斷點有必須儲存的資料時 儲存的地方  |
|     | UTime    | datetime      | 更新時間             |                                        |
