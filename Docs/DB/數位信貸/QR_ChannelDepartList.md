### 公司推廣單位列表 (QR_ChannelDepartList)

| PK  | NULL | 欄位              | 型態           | 說明                   | 代碼說明           |
| --- | ---- | ----------------- | -------------- | ---------------------- | ------------------ |
| V   |      | ChannelId         | nvarchar(50)   | 所屬ChannelId          |                    |  |
| V   |      | DepartId          | nvarchar(50)   | 推廣單位ID或活動代號   |                    |
|     |      | DepartName        | nvarchar(200)  | 推廣單位名稱           |                    |
|     |      | Enable            | char(1)        | 此Depart啟用狀態       | 1:啟用、0:停用     |
|     |      | EMail             | nvarchar(1000) | 申辦未完成 通知的EMAIL | 多個EMAIL使用;分隔 |
|     |      | TransDepart       | nvarchar(50)   | 預設轉介單位           | 貸款用             |
|     |      | TransMember       | nvarchar(50)   | 預設轉介人員           | 貸款用             |
|     |      | PromoDepart       | nvarchar(50)   | 預設推廣單位           | 貸款用             |
|     |      | PromoMember       | nvarchar(50)   | 預設推廣人員           | 貸款用             |
|     |      | AssignDepart      | nvarchar(50)   | 貸款分派單位           | 貸款用             |
|     |      | PromoDepart1      | nvarchar(50)   | 預設推廣單位           | 信用卡用           |
|     |      | PromoMember1      | nvarchar(50)   | 預設推廣人員           | 信用卡用           |
|     |      | PromoDepart2      | nvarchar(50)   | 預設推廣單位2          | 信用卡用           |
|     |      | PromoMember2      | nvarchar(50)   | 預設推廣人員2          | 信用卡用           |
|     |      | AssignDepartExist | nvarchar(50)   | 信用卡既有戶分派單位   | 信用卡用           |
|     |      | AssignDepartNew   | nvarchar(50)   | 信用卡新戶分派單位     | 信用卡用           |


### 如果客戶沒有輸入推廣單位  就在此Table 搜尋DepartId= '-' 可以查到預設的分派邏輯
