### 中華電信取得個人資料 (CHT_PreFill)

| PK  | NULL | 欄位        | 型態          | 說明         | 代碼說明                                         |
| --- | ---- | ----------- | ------------- | ------------ | ------------------------------------------------ |
| v   |      | UniqId      | nvarchar(50)  | 序號         | CaseNo 或 PLNo 或 ConNo 或CreditNo               |
|     |      | UniqType    | char(2)       | 唯一代號類型 | 01 : 體驗 02: 申請 03:立約 04:專人聯絡 05:信用卡 |
|     | V    | Name        | nvarchar(50)  | 姓名         |                                                  |
|     | V    | Sex         | nvarchar(50)  | 性別         |                                                  |
|     | V    | Birthday    | nvarchar(50)  | 生日         |                                                  |
|     | V    | HomeAddress | nvarchar(200) | 居住地址     |                                                  |
|     | V    | Mobile      | nvarchar(50)  | 手機號碼     |                                                  |
|     | V    | EMail       | nvarchar(200) | 電子信箱     |                                                  |
|     | V    | LocalPhone  | nvarchar(50)  | 居住電話     |                                                  |
|     | V    | BillAddress | nvarchar(200) | 帳單地址     |                                                  |
|     |      | CreateTime  | datetime      | 新增時間     |                                                  |