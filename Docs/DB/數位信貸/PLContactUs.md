### 專人與我聯絡資料表 (PLContactUs)

| PK  | NULL | 欄位        | 型態           | 說明                     | 代碼說明                   |
| --- | ---- | ----------- | -------------- | ------------------------ | -------------------------- |
| v   |      | Serial      | int            | 序號                     | 由資料庫自動產生           |
|     |      | UniqId      | nvarchar(50)   | 序號                     | CaseNo 或 PLNo 或 ConNo    |
|     |      | UniqType    | char(2)        | 唯一代號類型             | 01 : 體驗 02: 申請 03:立約 |
|     |      | Name        | nvarchar(50)   | 姓名                     |                            |
|     |      | Phone       | nvarchar(50)   | 手機號碼                 |                            |
|     |      | EMail       | nvarchar(50)   | 電子郵件                 |                            |
|     |      | ContactTime | nvarchar(50)   | 方便聯絡時間             |                            |
|     |      | Note        | nvarchar(1000) | 意見或問題               |                            |
|     |      | PrevPage    | varchar(2)     | 來源頁面(額度體驗試算/…) |                            |
|     |      | ContactFlag | char(1)        | 是否已經處理             | 0:還沒 1:已送出            |
|     | V    | ProductId   | nvarchar(50)   | 申請商品                 |                            |
|     | V    | DepartId    | nvarchar(50)   | 聯絡單位                 |                            |
|     | V    | Member      | nvarchar(50)   | 聯絡人員                 |                            |
|     |      | UTime       | datetime       | 更新時間                 |                            |

### 來源頁面 1:申請(非預核) 2:體驗 3:申請(預核) 4:立約 5:新戶 6:首頁 7:有ChannelData