### 使用者案件驗證方式 (CaseAuth)

| PK  | NULL | 欄位         | 型態          | 說明                   | 代碼說明                                      |
| --- | ---- | ------------ | ------------- | ---------------------- | --------------------------------------------- |
| v   |      | UniqId       | nvarchar(50)  | 序號                   | CaseNo 或 ExpNo 或 ConNo 或CreditNo           |
|     |      | UniqType     | char(2)       | 唯一代號類型           | 01:體驗 02:申請 03:立約 04:專人聯絡 05:信用卡 |
|     |      | AuthType     | varchar(20)   | 驗證類型               | 1:OTP 2:MBC                                   |
|     |      | EntType      | nvarchar(50)  | 異業合作ID             |                                               |
|     |      | MBCValid     | varchar(20)   | MBC是否通過            | 0:未通過 1:通過                               |
|     |      | MBCCheckCode | varchar(20)   | MBC驗證的回應代碼      |
|     |      | MBCMessage   | nvarchar(500) | MBC驗證的回傳訊息      |
|     |      | ChtAgree     | varchar(20)   | 是否同意取中華電信資料 | 0:不同意 1:同意                               |
|     | V    | ChtAgreeTime | datetime      |                        | 同意中華電信時間                              |  |
|     |      | CreateTime   | datetime      | 新增時間               |                                               |



