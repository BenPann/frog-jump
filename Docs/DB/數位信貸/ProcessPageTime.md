###案件每頁面的操作時間(ProcessPageTime)
|PK|欄位|型態|說明|代碼說明|
|--|--|--|--|--|
| v | Serial | int | 序號 | 由資料庫自動產生 | |
| | UniqId | nvarchar(50) | 流程唯一代號 |可能為 CaseNo,ExpNo,ConNo |
| | UniqType | char(2) | 唯一代號類型| 01 : 體驗 02: 申請 03:立約 |
| | Page | nvarchar(50) | 頁面 | |
| | StartTime | datetime | 頁面開始時間 | |
| | EndTime | datetime | 頁面結束時間 | |
| | UpdateTime | datetime | 最後操作時間 | |
