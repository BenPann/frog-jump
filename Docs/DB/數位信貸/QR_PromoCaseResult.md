### 案件推廣單位跟人員 派件結果 QR_PromoCaseResult

| PK  | NULL | 欄位         | 型態         | 說明         | 代碼說明                                      |
| --- | ---- | ------------ | ------------ | ------------ | --------------------------------------------- |
| V   |      | UniqId       | nvarchar(50) | 唯一序號     | CaseNo 或 ExpNo 或 ConNo 或CreditNo           |
|     |      | UniqType     | char(2)      | 唯一代號類型 | 01:體驗 02:申請 03:立約 04:專人聯絡 05:信用卡 |
|     |      | PromoDepart1 | nvarchar(50) | 推廣單位一   | 貸款時 此欄位為轉介單位                       |
|     |      | PromoMember1 | nvarchar(50) | 推廣人員一   |                                               |
|     |      | PromoDepart2 | nvarchar(50) | 推廣單位二   | 貸款時 此欄位為推廣單位                       |
|     |      | PromoMember2 | nvarchar(50) | 推廣人員二   |                                               |
|     |      | AssignDepart | nvarchar(50) | 派件單位     |                                               |
|     |      | CreateTime   | datetime     | 建立時間     |