###權限對應表單(menu_permission)
| PK  | NULL | 欄位         | 型態 | 說明 | 代碼說明 |
| --- | ---- | ------------ | ---- | ---- | -------- |
| V   |      | menuId       | int  |      |          |
| V   |      | permissionId | int  |      |          |
