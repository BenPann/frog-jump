###流水號紀錄檔 (SN)
|PK|欄位|型態|說明|代碼說明|
|--|--|--|--|--|
| v | Group | nvarchar(50) | 群組(通常是使用的地方 如:OTP)      |  | 
| v | Key   | nvarchar(50) | 此群組的辨識值(通常是日期 20171026) |  | 
|   | Sn    | int          | 流水號(由SP自動加總並傳出)         |  | 

* 使用範例:
``` Sql
DECLARE        @return_value int

EXEC        @return_value = [dbo].[sp_gen_serialnumber]
                @Group = N'Test',
                @Key = N'20171026'

SELECT        'Return Value' = @return_value"
```