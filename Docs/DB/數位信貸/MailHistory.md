### 發送郵件歷史 (MailHistory)
- 此Table為紀錄發送郵件使用

| PK  | NULL | 欄位         | 型態            | 說明          | 代碼說明                                         |
| --- | ---- | ------------ | --------------- | ------------- | ------------------------------------------------ |
| v   |      | Serial       | int             | 流水號        |                                                  |
|     |      | UniqId       | nvarchar(50)    | 序號          | CaseNo 或 PLNo 或 ConNo 或CreditNo               |
|     |      | MailType     | nvarchar(20)    | 郵件類型      | 1:MailHunter寄送行外 ,2: SMTP 寄送行內,3: TM通知,4: 簡訊通知 |
|     |      | Status       | nvarchar(20)    | 是否寄送      | 0:尚未寄送 1: 寄送成功 9: 寄送失敗               |
|     |      | ErrorMessage | nvarchar(1000)  | 錯誤原因      |                                                  |
|     |      | Title        | nvarchar(200)   | 標題          |                                                  |
|     |      | Content      | nvarchar(max)   | 內文          |                                                  |
|     |      | TemplateId   | nvarchar(20)    | 樣板ID        | 給MailHunter用                                   |
|     |      | Attatchment  | nvarbinary(max) | 附件          | 給MailHunter用                                   |
|     |      | EMailAddress | nvarchar(max)   | 發送目標EMAIL |                                                  |
|     |      | CreateTime   | datetime        | 新增時間      | 就是新增時間                                     |
|     |      | UpdateTime   | datetime        | 更新時間      | 就是發送時間                                     |
