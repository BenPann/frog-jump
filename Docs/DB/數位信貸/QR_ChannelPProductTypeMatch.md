### Channel可推廣產品對應表 (QR_ChannelPProductTypeMatch)

| PK  | NULL | 欄位        | 型態         | 說明                        | 代碼說明                      |
| --- | ---- | ----------- | ------------ | --------------------------- | ----------------------------- |
| V   |      | ChannelId   | nvarchar(50) | 異業合作時須約定的Channelid | 此欄位跟之前開的Entry同樣意思 |
| V   |      | PProductType | varchar(20)  | 可推廣產品類型              | 0:貸款; 1:信用卡;             |

### 該Channel可推廣哪些類型的產品