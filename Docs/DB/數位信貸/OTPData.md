###跟 APS 取得的 OTP 資料 (OTPData)

| PK  | 欄位      | 型態         | 說明               | 代碼說明                   |
| --- | --------- | ------------ | ------------------ | -------------------------- |
| v   | UniqId    | nvarchar(50) | 序號               | CaseNo 或 PLNo 或 ConNo    |
|     | UniqType  | char(2)      | 唯一代號類型       | 01 : 體驗 02: 申請 03:立約 |
|     | PhoneNum  | nvarchar(20) | 行動電話           |                            |
|     | OTPSource | char(1)      | OTP 的電話號碼來源 |                            |
|     | UTime     | datetime     | 更新時間           |                            |
