###案件契約資料表(ContractData)

| PK  | 欄位         | 型態          | 說明               | 代碼說明                           |
| --- | ------------ | ------------- | ------------------ | ---------------------------------- |
| v   | ConNo        | nvarchar(50)  | 立約編號           | 範例: CONYYYYMMDD00001             |
|     | CaseNoWeb    | nvarchar(50)  | 案件編號(APS)      | APS 的案件編號 (ADD_NEW_CASE 產的) |
|     | CaseNoAps    | nvarchar(50)  | APS 真正的案件編號 | APS 起案之後會來更新               |
|     | Status       | char(2)       | 案件狀態           | (01:立約中 02:立約填寫完成)        |
|     | ProductId    | nvarchar(50)  | 產品代號           |                                    |
|     | Idno         | nvarchar(50)  | 身分證字號         |                                    |
|     | CustomerName | nvarchar(50)  | 客戶姓名           |                                    |
|     | DealPayDay   | datetime      | APS 撥款日期       | APS 傳來的 PayDay                  |
|     | FinalPayDay  | datetime      | 最終撥款日期       | 計算過後的 PayDay                  |
|     | ContractDay  | datetime      | 立約日期           | 立約當日                           |
|     | DocAddr      | nvarchar(200) | 文件寄送地址       |                                    |
|     | ReadDate     | datetime      | 審閱日期           |                                    |
|     | Notify       | char(2)       | 告知方式           | 01:簡訊 02:書面                    |
|     | Court        | nvarchar(20)  | 法院名稱簡寫       |                                    |
|     | EMail        | nvarchar(200) | 寄送契約書的 EMAIL |                                    |
|     | DataUse      | char(2)       | 同意資料使用       | 01:同意 02:不同意                  |
|     | ApsConstData | nvarchar(max) | APS 的立約資料     |                                    |
|     | IpAddress    | varchar(20)   | IP                 |                                    |
|     | UTime        | datetime      | 更新時間           |                                    |
