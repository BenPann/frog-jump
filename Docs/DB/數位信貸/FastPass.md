### 快速通關 (FastPass)

| PK  | 欄位        | 型態          | 說明               | 代碼說明                                                     |
| --- | ----------- | ------------- | ------------------ | ------------------------------------------------------------ |
| v   | CaseNo      | nvarchar(50)  | 案件編號(數位信貸) | 範例: CASEYYYYMMDD00001                                      |
|     | AddPhoto    | char(1)       | 是否上傳扣憑所清   | 0:尚未上傳 1:已上傳                                          |
|     | AML         | char(1)       | AML 查詢結果       | 0:尚未查詢AML, Y,N,V: 查詢AML成功的狀態, 9:AML失敗           |
|     | AMLRequest  | nvarchar(max) | AMLRequset         | AML需求電文                                                  |
|     | AMLResponse | nvarchar(max) | AMLResponse        | AML回應電文                                                  |
|     | EndTime     | datetime      | 快速通關期限       | 過了此時間就不算快速通關件 此時間通常會是 產生完PDF之後N分鐘 |
|     | CreateTime  | datetime      | 新增時間           |                                                              |
|     | UpdateTime  | datetime      | 更新時間           |                                                              |

### N目前可能會是30
### 傳送PDF至影像系統 要先查看此Table 如果30分鐘之內有把所有東西補齊 走快速 過了30分鐘 走一般
### 只有貸款會使用
### 傳送影像的邏輯  看AML是否已查詢  已查詢才能繼續
### AML查詢為黑名單 直接送件
### 看時間是否已超過EndTime 超過就直接傳送PDF給影像
### 查詢是否有上傳影像 
### 沒上傳 跳過
### 有上傳 看是否扣繳憑單或所得清單 不是 跳過
### 是 且職業類別為非洗錢職業類別  送快速
### 是 且職業類別為洗錢職業類別 送一般