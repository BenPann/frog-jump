###首頁點擊紀錄 Table (IndexClick)
|PK|欄位|型態|說明|代碼說明|
|--|--|--|--|--|
| v | Serial |int | 代號 | 自動增加 |
| | Entry | nvarchar(100)| 連結入口 | 客戶由哪個入口進入(ex:Pchome) |
| | Browser | nvarchar(50) | 客戶使用瀏覽器 | |
| | Platform | nvarchar(50) | 客戶使用平台 | |
| | OS | nvarchar(50) | 客戶作業系統 | |
| | Action | nvarchar(50) | 點選按鈕 | |
| | UTime | datetime | 更新時間 | |
