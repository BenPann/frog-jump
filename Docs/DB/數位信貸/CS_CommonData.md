### 跨售跟異業的一些基本資料 (CS_CommonData)
#### 目前預計只會在入口新增

| PK  | NULL | 欄位       | 型態          | 說明         | 代碼說明                                         |
| --- | ---- | ---------- | ------------- | ------------ | ------------------------------------------------ |
| v   |      | UniqId     | nvarchar(50)  | 序號         | CaseNo 或 PLNo 或 ConNo 或CreditNo               |
|     |      | UniqType   | char(2)       | 唯一代號類型 | 01 : 體驗 02: 申請 03:立約 04:專人聯絡 05:信用卡 |
|     |      | CSType     | nvarchar(2)   | 跨售類型     | 1: 跨售(CS) 2: 異業(CSCommon)                    |
|     |      | ChannelId  | nvarchar(50)  | 通路來源     | ChannelId                                        |
|     |      | ResultData | nvarchar(max) | 結果資料     | 解析完成之後 最後在前端可使用的資料              |
|     |      | UpdateTime | datetime      | 更新時間     |
|     |      | CreateTime | datetime      | 新增時間     |                                                  |