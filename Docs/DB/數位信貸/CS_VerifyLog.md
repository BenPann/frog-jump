### 跨售核身LOG 備查用 (CS_VerifyLog)

| PK  | NULL | 欄位            | 型態         | 說明         | 代碼說明                                         |
| --- | ---- | --------------- | ------------ | ------------ | ------------------------------------------------ |
| v   |      | UniqId          | nvarchar(50) | 序號         | CaseNo 或 PLNo 或 ConNo 或CreditNo               |
|     |      | UniqType        | char(2)      | 唯一代號類型 | 01 : 體驗 02: 申請 03:立約 04:專人聯絡 05:信用卡 |
|     |      | Idno            | nvarchar(50) | 身分證字號   |                                                  |
|     |      | IpAddress       | varchar(20)  | IP           |                                                  |
|     |      | Time            | nvarchar(50) | 時間         |                                                  |
|     |      | MessageSerial   | nvarchar(50) | 電文序號     |                                                  |
|     |      | ContractVersion | nvarchar(50) | 契約版號     |                                                  |
|     |      | CreateTime      | datetime     | 新增時間     |                                                  |