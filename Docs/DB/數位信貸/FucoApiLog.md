###使用者呼叫信貸 API 的 LOG 資料表 (FucoApiLog)
|PK|欄位|型態|說明|代碼說明|
|--|--|--|--|--|
| v | Serial | int | 序號 | 由資料庫自動產生 | |
| | ApiName | nvarchar(50) | 呼叫的 API 名稱 | | |
| | Request | nvarchar(max) | 呼叫 API 的需求電文 | | |
| | Response | nvarchar(max) | API 的回應電文 | | |
| | UTime | datetime | 更新時間 | | |

###此 Log 為客戶 Call 我們的 Api 時的 Log
