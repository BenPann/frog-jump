### 備註資料  (MemoData)
- 此Table為保存額外的資料用

| PK  | NULL | 欄位       | 型態          | 說明         | 代碼說明                                         |
| --- | ---- | ---------- | ------------- | ------------ | ------------------------------------------------ |
| v   |      | UniqId     | nvarchar(50)  | 序號         | CaseNo 或 PLNo 或 ConNo 或CreditNo               |
|     |      | UniqType   | char(2)       | 唯一代號類型 | 01 : 體驗 02: 申請 03:立約 04:專人聯絡 05:信用卡 |
| V   |      | MemoName   | nvarchar(50)  | 備註名稱     |                                                  |
|     |      | Memo       | nvarchar(max) | 備註內容     |
|     |      | UpdateTime | datetime      | 更新時間     |
|     |      | CreateTime | datetime      | 新增時間     |
