### 中華電信取得信評資料 (CHT_CreditRating)
| PK  | NULL | 欄位                    | 型態         | 說明                                 | 代碼說明                                         |
| --- | ---- | ----------------------- | ------------ | ------------------------------------ | ------------------------------------------------ |
| v   |      | UniqId                  | nvarchar(50) | 序號                                 | CaseNo 或 PLNo 或 ConNo 或CreditNo               |
|     |      | UniqType                | char(2)      | 唯一代號類型                         | 01 : 體驗 02: 申請 03:立約 04:專人聯絡 05:信用卡 |
|     | V    | CustomerPhone           | nvarchar(20) | 行動電話號碼                         |                                                  |
|     | V    | CustomerIdcard          | nvarchar(20) | 身份證字號                           |                                                  |
|     | V    | Processyyymm            | nvarchar(20) | 評等處理年月                         |                                                  |
|     | V    | IsOldCustomer           | nvarchar(20) | 是否與中華電信往來超過三個月以上註記 |                                                  |
|     | V    | CreditValue             | nvarchar(20) | 電信評等分數                         |                                                  |
|     | V    | PaymentBehaviorBeyond14 | nvarchar(20) | 繳費行為                             | 觀察期六次帳單遲繳超過14天之次數                 |
|     |
|     | V    | PaymentBehaviorBeyond30 | nvarchar(20) | 繳費行為                             | 觀察期六次帳單遲繳超過30天之次數                 |
|     | V    | PaymentMethod           | nvarchar(20) | 繳費方式                             | 1,2,3 信用卡/存簿/其他                           |
|     |
|     | V    | IsSuspended             | nvarchar(20) | 欠費停話                             |                                                  |
|     | V    | IsRevoked               | nvarchar(20) | 欠費拆機者                           |                                                  |
|     | V    | IsRevoked6mon           | nvarchar(20) | 前6個月曾有欠費拆機者                |                                                  |
|     | V    | DemandLetter            | nvarchar(20) | 依法追繳函                           |                                                  |
|     | V    | DebtRank                | nvarchar(20) | 呆帳                                 |                                                  |
|     | V    | HasDebt                 | nvarchar(20) | 是否呆帳                             |                                                  |
|     | V    | IsRiskCustomer          | nvarchar(20) | 風險客戶                             |                                                  |
|     | V    | CustomerType            | nvarchar(20) | 客戶屬性                             |                                                  |
|     | V    | HasDesignatedManager    | nvarchar(20) | 專戶識別                             |                                                  |
|     | V    | MaxTenureMonth          | nvarchar(20) |                                      |                                                  |
|     | V    | DataUsage               | nvarchar(20) | 行動數據用量(MB)                     |                                                  |
|     | V    | VoiceCallCnt            | nvarchar(20) | 行動門號語音通話數(by月)             |                                                  |
|     | V    | VoiceCallMin            | nvarchar(20) | 行動門號語音通話分鐘(by月)           |                                                  |
|     | V    | VoSendTotNum            | nvarchar(20) | 行動用戶通話對象數                   |                                                  |
|     | V    | WorkCity                | nvarchar(20) | 行動用戶工作地縣市                   |                                                  |
|     | V    | HomeCity                | nvarchar(20) | 行動用戶居住地縣市                   |                                                  |
|     | V    | HasBadRecord            | nvarchar(20) | 是否有不良紀錄                       |                                                  |  |
|     | V    | HasData                 | nvarchar(20) | 是否有資料                           |                                                  |  |
|     |      | CreateTime              | datetime     | 新增時間                             |                                                  |



