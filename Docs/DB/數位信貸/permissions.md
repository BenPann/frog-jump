###權限(permissions)
| PK  | NULL | 欄位           | 型態         | 說明 | 代碼說明 |
| --- | ---- | -------------- | ------------ | ---- | -------- |
|     |      | permissionId   | int          |      |          |
|     |      | permissionName | nvarchar(50) |      |          |
|     |      | permissionKey  | nvarchar(50) |      |          |
