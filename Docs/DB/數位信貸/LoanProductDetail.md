###貸款產品明細檔(LoanProductDetail)
|PK|欄位|型態|說明|代碼說明|
|--|--|--|--|--|
| V | MainSerial | int | 主檔序號 | LoanProduct.Serial |
| V | DetailSerial | int | 明細序號 | 明細的流水號 |
| | Type | nvarchar(50) | 類型 | 產品圖片說明或產品文字說明 |
| | ViewImage | nvarchar(max)| 視覺圖 | |
| | Title | nvarchar(200) | 文字標題內容| |
| | Content | nvarchar(300) | 文字內容| |
| | CreateUser | nvarchar(50) | 新增的使用者 | |
| | CreateTime | datetime | 新增時間 | |
