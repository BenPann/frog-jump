### EOP_ShortUrlCheckList 短網址檢核名單表 (EOP_ShortUrlCheckList)

- 此 Table 是儲存 短網址檢核名單表

| PK  | NULL | 欄位         | 型態          | 說明       | 代碼說明 |
| --- | ---- | ------------ | ------------- | ---------- | -------- |
| V   |      | ShortUrl     | nvarchar(50)  | 連結網址   |          |
| V   |      | Idno         | nvarchar(20)  | 身分證號   |          |
|     |      | ChtName      | nvarchar(50)  | 姓名       |          |
|     |      | Phone        | nvarchar(50)  | 手機號碼   |          |
|     |      | EmailAddress | nvarchar(120) | EMAIL 地址 |          |
|     |      | CreateUser   | nvarchar(50)  | 異動使用者 |          |
|     |      | CreateTime   | datetime      | 異動時間   |          |

```sql
create table dbo.EOP_ShortUrlCheckList
(
	ShortUrl nvarchar(50) not null,
	Idno nvarchar(100) not null,
	ChtName nvarchar(100) not null,
	Phone nvarchar(10) not null,
	EmailAddress nvarchar(50) not null,
	CreateUser nvarchar(50) not null,
	CreateTime datetime not null,
	constraint PK_EOP_ShortUrlCheckList
		primary key (ShortUrl, Idno)
)
go

```