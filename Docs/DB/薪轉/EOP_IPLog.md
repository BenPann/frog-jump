serial uniqid ip createtime

### 紀錄客戶每次登入時的 IP 資料表(EOP_IPLog)

| PK  | 欄位       | 型態          | 說明                   | 代碼說明 |
| --- | ---------- | ------------- | ---------------------- | -------- |
| v   | Serial     | int           | 序號, 由資料庫自動產生 |          |
|     | UniqId     | nvarchar(50)  | 案件序號               |          |
|     | IPAddress  | nvarchar(200) | 登入的 IP              |          |
|     | CreateTime | datetime      | 新增時間               |          |

```sql
create table dbo.EOP_IPLog
(
    Serial     int identity
        constraint PK_EOP_IPLog
            primary key,
    UniqId     nvarchar(50)  not null,
    IPAddress  nvarchar(200) not null,
    CreateTime datetime      not null
)
go

```