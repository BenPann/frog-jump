### 驗證紀錄 (EOP_VerifyLog)

- 此 Table 為紀錄各個系統驗證結果以及驗證次數

| PK  | NULL | 欄位       | 型態           | 說明           | 代碼說明                                     |
| --- | ---- | ---------- | -------------- | -------------- | -------------------------------------------- |
| v   |      | UniqId     | nvarchar(50)   | 序號           | ULID                                         |
| v   |      | VerifyType | nvarchar(20)   | 驗證類型       | NCCC 或 PCode2566                            |
| v   |      | Count      | int            | 驗證次數       | 流水號數字 1,2,3 (錯誤次數就從 Count 去判斷) |
|     |      | Request    | nvarchar(max)  | 驗證傳遞參數   | 需求 RawData                                 |
|     |      | Response   | nvarchar(max)  | 驗證回應參數   | 回應 RawData                                 |
|     |      | CheckCode  | nvarchar(20)   | 回覆代碼       | 拆解 Response 的 CheckCode                   |
|     |      | AuthCode   | nvarchar(20)   | 授權碼或驗證碼 | 目前是 NCCC 才有回應的授權碼                 |
|     |      | Other      | nvarchar(1000) | 其他           | 如有其他欄位需求 先以 Json 方式塞進此欄位    |
|     |      | CreateTime | datetime       | 新增時間       | 就是驗證時間                                 |

```sql
create table dbo.EOP_VerifyLog
(
	UniqId nvarchar(50) not null,
	UniqType char(2),
	VerifyType nvarchar(20) not null,
	Count int not null,
	Request nvarchar(max) not null,
	Response nvarchar(max) not null,
	CheckCode nvarchar(20) not null,
	AuthCode nvarchar(20) not null,
	Other nvarchar(1000) not null,
	CreateTime datetime not null,
	constraint PK_EOP_VerifyLog
		primary key (UniqId, VerifyType, Count)
)
go

```