### 從屬表 (EOP_Organization)

| PK  | NULL | 欄位     | 型態         | 說明       | 代碼說明 |
| --- | ---- | -------- | ------------ | ---------- | -------- |
|    |      | UserId   | nvarchar(50) | 員工編號   | USERID   |
|     |      | Minister | nvarchar(50) | 主管       | USERID   |
|     |      | Manager  | nvarchar(50) | 主管的主管 | USERID   |

```sql
create table dbo.EOP_Organization
(
	UserId nvarchar(50) not null,
	Minister nvarchar(50),
	Manager nvarchar(50)
)
go
```
