### 開戶影像回傳結果資料表(EOP_AOResult)

| PK  | 欄位              | 型態          | 說明                     | 代碼說明                                              |
| --- | ----------------- | ------------- | ------------------------ | ----------------------------------------------------- |
| v   | Serial            | int           | 由資料庫產生的序號       |                                                       |
|     | UniqId            | nvarchar(50)  | 案件編號                 |                                                       |
|     | Status            | varchar(1)    | 案件開戶狀態             | 0:成功 1:退件(失敗) 2:補件 3:舊客戶使用舊戶帳戶       |
|     | OpenfailMsg       | nvarchar(max) | 開戶退件失敗原因         |                                                       |
|     | StatusTw          | varchar(1)    | 台幣帳戶結果狀態碼       | 0:成功 1:失敗                                         |
|     | SalaryAccountTw   | nvarchar(16)  | 台幣帳戶                 | 16 碼阿拉伯數字                                       |
|     | PassBookTw        | varchar(1)    | 台幣帳戶有無存摺         | Y: 有存摺 N:無存摺(如果台幣帳戶結果為失敗，也請給 N)  |
|     | StatusFore        | varchar(1)    | 外幣帳戶結果狀態碼       | 0:成功 1:失敗                                         |
|     | SalaryAccountFore | nvarchar(16)  | 外幣帳戶                 | 16 碼阿拉伯數字                                       |
|     | PassBookFore      | varchar(1)    | 外幣帳戶有無存摺         | Y: 有存摺 N:無存摺(如果外幣帳戶結果為失敗，也請給 N)  |
|     | TrustStatus       | varchar(1)    | 信託帳戶結果狀態碼       | 0:成功 1:失敗                                         |
|     | D3Message         | nvarchar(max) | 數三開戶結果通知信件內容 |                                                       |
|     | D3Base64PDF       | nvarchar(max) | 數三開戶影像結果         | data:image/png;base64,+base64 字串                    |
|     | FailedImage       | nvarchar(max) | 需要補件的圖檔           | [{"pType":"1","sType":"1"},{"pType":"1","sType":"2"}] |
|     | CreateTime        | dateTime      | 建立時間                 |                                                       |

```sql

create table EOP_AOResult
(
  Serial            int identity
    constraint PK_EOP_AOResult
      primary key,
  UniqId            nvarchar(50) not null,
  Status            varchar(1),
  OpenfailMsg       nvarchar(max),
  StatusTw          varchar(1),
  SalaryAccountTw   nvarchar(16),
  PassBookTw        varchar(1),
  StatusFore        varchar(1),
  SalaryAccountFore nvarchar(16),
  PassBookFore      varchar(1),
  TrustStatus       varchar(1),
  D3Message         nvarchar(max),
  D3Base64PDF       nvarchar(max) not null,
  FailedImage       nvarchar(max),
  CreateTime        datetime not null
)
go

```