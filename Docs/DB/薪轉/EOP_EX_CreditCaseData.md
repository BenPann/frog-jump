### 開戶-附加服務-信用卡案件資料 (EOP_EX_CreditCaseData)

| PK  | NULL | 欄位            | 型態          | 說明                     | 代碼說明                                         |
| --- | ---- | --------------- | ------------- | ------------------------ | ------------------------------------------------ |
| v   |      | UniqId          | nvarchar(50)  | 序號                     | ULID                                             |
|     |      | AirloanUniqId   | nvarchar(50)  | 數位信貸的信用卡編號     | CreditCaseData.UniqId                            |
|     |      | Status          | char(2)       | 案件狀態                 |                                                  |
|     |      | Idno            | nvarchar(50)  | 身分證字號或護照號碼     |                                                  |
|     |      | UserType        | char(1)       | 使用者類別               | 0: 新戶 1:信用卡戶 2:存款戶                      |
|     |      | Phone           | nvarchar(50)  | 手機號碼                 |                                                  |
|     |      | ProductId       | nvarchar(50)  | 產品名稱                 | 信用卡代號                                       |
|     |      | ChtName         | nvarchar(50)  | 中文姓名                 |                                                  |
|     | V    | EngName         | nvarchar(100) | 英文姓名                 |                                                  |
|     |      | Birthday        | nchar(8)      | 生日                     |                                                  |
|     |      | Gender          | char(1)       | 性別                     | 1:男 2:女                                        |
|     |      | IdCardDate      | datetime      | 身份證換發日期           |                                                  |
|     |      | IdCardLocation  | nvarchar(50)  | 身份證換發地點           |                                                  |
|     |      | IdCardCRecord   | char(2)       | 身份證換發紀錄           | 0:初領 1:補領 2:換發                             |
|     |      | ResAddrZipCode  | nvarchar(6)   | 戶籍地址區碼             |                                                  |
|     |      | ResAddr         | nvarchar(200) | 戶籍地址                 |                                                  |
|     |      | CommAddrZipCode | nvarchar(6)   | 通訊地址區碼             |                                                  |
|     |      | CommAddr        | nvarchar(200) | 通訊地址                 |                                                  |
|     |      | HomeAddrZipCode | nvarchar(6)   | 居住地址區碼             |                                                  |
|     |      | HomeAddr        | nvarchar(200) | 居住地址                 |                                                  |
|     |      | Education       | char(1)       | 教育程度                 |                                                  |
|     |      | Marriage        | char(1)       | 婚姻狀態                 |                                                  |
|     |      | EmailAddress    | varchar(120)  | EMAIL 地址               |                                                  |
|     |      | ResTelArea      | nvarchar(4)   | 戶籍電話區碼             |                                                  |
|     |      | ResTel          | nvarchar(30)  | 戶籍電話                 |                                                  |
|     |      | HomeTelArea     | nvarchar(4)   | 居住電話區碼             |                                                  |
|     |      | HomeTel         | nvarchar(30)  | 居住電話                 |                                                  |
|     |      | HomeType        | char(1)       | 居住房屋類型             | 0:自有宅 1:租房                                  |
|     |      | HomeOwner       | char(1)       | 居住房屋所有權人         | 1.本人所有 2.配偶所有 3.家族所有 4.無            |
|     |      | CorpName        | nvarchar(100) | 公司名稱                 |                                                  |
|     |      | CorpTelArea     | nvarchar(4)   | 公司電話區碼             |                                                  |
|     |      | CorpTel         | nvarchar(30)  | 公司電話                 |                                                  |
|     |      | CorpTelExten    | nvarchar(6)   | 公司電話分機             |                                                  |
|     |      | CorpAddrZipCode | nvarchar(6)   | 公司地址區碼             |                                                  |
|     |      | CorpAddr        | nvarchar(200) | 公司地址                 |                                                  |
|     |      | Occupation      | char(3)      | 職業類別                 |                                                  |
|     |      | CorpDepart      | nvarchar(50)  | 公司部門                 |                                                  |
|     |      | JobTitle        | nvarchar(100) | 職稱                     |                                                  |
|     |      | YearlyIncome    | int           | 年收入                   | (單位：萬元)                                     |
|     |      | OnBoardDate     | char(6)       | 到職日                   |                                                  |
|     |      | SendType        | char(1)       | 信用卡帳單型態           | 1:實體帳單 2:電子帳單 4:行動帳單                 |
|     |      | FirstPresent    | varchar(50)   | 首刷禮                   | 跟 CreditCardGift Table 以及 GiftList Table 對應 |
|     |      | IpAddress       | varchar(20)   | IP                       |                                                  |
|     |      | QuickHandle     | varchar(1)    | 加速辦理或提高信用卡額度 | Y:同意 N:不同意                                  |
|     | V    | DataUse         | char(2)       | 是否有共銷               | 01:共銷 02:非共銷                                |
|     |      | CreateTime      | datetime      | 新增時間                 |                                                  |
|     | V    | UpdateTime      | datetime      | 更新時間                 |                                                  |
|     | V    | SendedTime      | datetime      | 發送時間                 |                                                  |

#### Status - 案件主狀態

| 代碼 | 中文     |
| ---- | -------- |
| 01   | 編輯中   |
| 02   | 編輯完成 |
| 11   | 送件中   |
| 12   | 送件完成 |

```sql
create table dbo.EOP_EX_CreditCaseData
(
    UniqId          nvarchar(50) not null
        constraint PK_EOP_EX_CreditCaseData
            primary key,
    AirloanUniqId   nvarchar(50),
    Status          char(2),
    Idno            nvarchar(50),
    UserType        char,
    Phone           nvarchar(50),
    ProductId       nvarchar(50),
    ChtName         nvarchar(50),
    EngName         nvarchar(100),
    Birthday        nvarchar(50),
    Gender          nvarchar(50),
    IdCardDate      datetime,
    IdCardLocation  nvarchar(50),
    IdCardCRecord   char,
    ResAddrZipCode  nvarchar(6),
    ResAddr         nvarchar(200),
    CommAddrZipCode nvarchar(6),
    CommAddr        nvarchar(200),
    HomeAddrZipCode nvarchar(6),
    HomeAddr        nvarchar(200),
    Education       char,
    Marriage        char,
    EmailAddress    varchar(120),
    ResTelArea      nvarchar(4),
    ResTel          nvarchar(30),
    HomeTelArea     nvarchar(4),
    HomeTel         nvarchar(30),
    HomeType        char,
    HomeOwner       char,
    CorpName        nvarchar(100),
    CorpTelArea     nvarchar(4),
    CorpTel         nvarchar(30),
    CorpTelExten    nvarchar(6),
    CorpAddrZipCode nvarchar(6),
    CorpAddr        nvarchar(200),
    Occupation      char(3),
    CorpDepart      nvarchar(50),
    JobTitle        nvarchar(100),
    YearlyIncome    int,
    OnBoardDate     char(10),
    SendType        char,
    FirstPresent    nvarchar(50),
    IpAddress       varchar(20),
    QuickHandle     varchar(1),
    DataUse         char(2),
    CreateTime      datetime,
    UpdateTime      datetime,
    SendedTime      datetime
)
go

```