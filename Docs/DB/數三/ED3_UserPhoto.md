### 使用者上傳圖片紀錄 (ED3_UserPhoto)

| PK  | NULL | IDENTITY | 欄位       | 型態           | 說明                 | 代碼說明                       |
| --- | ---- | -------- | ---------- | -------------- | -------------------- | ------------------------------ |
| V   |      |          | UniqId     | nvarchar(50)   | 序號                 | 使用 ULID                      |
| V   |      |          | SubSerial  | int            | 流水號               | 每個 uniqId 從 1 開始 為流水號 |
|     |      |          | Status     | nvarchar(20)   | 圖片狀態             | 詳見[圖片狀態](#圖片狀態)      |
|     |      |          | PType      | int            | 主分類               | 詳見[主分類](#主分類)          |
|     |      |          | SType      | int            | 次分類               | 詳見[次分類](#次分類)          |
|     |      |          | ImageBig   | varbinary(max) | 使用者上傳的圖片(大) |                                |
|     |      |          | ImageSmall | varbinary(max) | 使用者上傳的圖片(小) |                                |
|     |      |          | CreateTime | datetime       | 新增時間             | 就是新增時間                   |
|     |      |          | UpdateTime | datetime       | 更新時間             | 就是發送時間                   |
|     | V    |          | SendedTime | datetime       | 傳送時間             | 就是傳送時間                   |

#### UserPhoto - 圖片狀態

| 代碼 | 中文                                     |
| ---- | ---------------------------------------- |
| 0    | 暫存用(使用完之後在轉處理中或轉別 TABLE) |
| 1    | 使用者處理中                             |
| 2    | 等待上傳中                               |
| 3    | 上傳成功待刪除                           |
| 9    | 上傳失敗                                 |

#### UserPhoto - 主分類

| 代碼 | 中文         |
| ---- | ------------ |
| 1    | 身分證明文件 |
| 2    | 財力證明文件 |

#### UserPhoto - 次分類

| 代碼 | 中文         |
| ---- | ------------ |
| 1    | 身分證正面   |
| 2    | 身分正反面   |
| 3    | 健保卡/駕照  |
| 20   | 薪資轉帳證明 |
| 21   | 土地謄本     |
| 22   | 存款證明     |
| 23   | 扣繳憑單     |
| 24   | 所得清單     |

```sql
<!--
CREATE TABLE [dbo].[ED3_UserPhoto](
	[UniqId] [nvarchar](50),
	[SubSerial] [int],
	[Status] [nvarchar](20) NULL,
	[PType] [int] NULL,
	[SType] [int] NULL,
	[ImageBig] [varbinary](max) NULL,
	[ImageSmall] [varbinary](max) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateTime] [datetime] NULL,
	[SendedTime] [datetime] NULL
	constraint PK_ED3_UserPhoto
		primary key (UniqId, SubSerial)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

-->

```
