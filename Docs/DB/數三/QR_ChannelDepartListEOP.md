### 公司推廣單位列表存款戶專用 (QR_ChannelDepartListEOP)

| PK  | NULL | 欄位         | 型態         | 說明                   | 代碼說明 |
| --- | ---- | ------------ | ------------ | ---------------------- | -------- |
| V   |      | ChannelId    | nvarchar(50) | 所屬 ChannelId         |          |
| V   |      | DepartId     | nvarchar(50) | 推廣單位 ID 或活動代號 |          |
|     |      | PromoDepart1 | nvarchar(50) | 預設推廣單位           | 存款用   |
|     |      | PromoMember1 | nvarchar(50) | 預設推廣人員           | 存款用   |
|     |      | PromoDepart2 | nvarchar(50) | 預設推廣單位 2         | 存款用   |
|     |      | PromoMember2 | nvarchar(50) | 預設推廣人員 2         | 存款用   |
|     |      | AssignDepart | nvarchar(50) | 存款戶分派單位         | 存款用   |

### 如果客戶沒有輸入推廣單位 就在此 Table 搜尋 DepartId= '-' 可以查到預設的分派邏輯

### 與 QR_ChannelDepartList 是要一組的


```sql
<!--
CREATE TABLE [dbo].[QR_ChannelDepartListEOP](
	[ChannelId] [nvarchar](50) NOT NULL,
	[DepartId] [nvarchar](50) NOT NULL,
	[PromoDepart1] [nvarchar](50) NOT NULL,
	[PromoMember1] [nvarchar](50) NOT NULL,
	[PromoDepart2] [nvarchar](50) NOT NULL,
	[PromoMember2] [nvarchar](50) NOT NULL,
	[AssignDepart] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_QR_ChannelDepartListEOP] PRIMARY KEY CLUSTERED 
(
	[ChannelId] ASC,
	[DepartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-->

```