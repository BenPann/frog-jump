### 驗證紀錄 (ED3_VerifyLog)

- 此 Table 為紀錄各個系統驗證結果以及驗證次數

| PK  | NULL | IDENTITY | 欄位       | 型態           | 說明           | 代碼說明                                     |
| --- | ---- | -------- | ---------- | -------------- | -------------- | -------------------------------------------- |
| v   |      |          | UniqId     | nvarchar(50)   | 序號           | ULID                                         |
| v   |      |          | VerifyType | nvarchar(20)   | 驗證類型       | NCCC 或 PCode2566                            |
| v   |      |          | Count      | int            | 驗證次數       | 流水號數字 1,2,3 (錯誤次數就從 Count 去判斷) |
|     |      |          | Request    | nvarchar(max)  | 驗證傳遞參數   | 需求 RawData                                 |
|     |      |          | Response   | nvarchar(max)  | 驗證回應參數   | 回應 RawData                                 |
|     |      |          | CheckCode  | nvarchar(20)   | 回覆代碼       | 拆解 Response 的 CheckCode                   |
|     |      |          | AuthCode   | nvarchar(20)   | 授權碼或驗證碼 | 目前是 NCCC 才有回應的授權碼                 |
|     |      |          | Other      | nvarchar(1000) | 其他           | 如有其他欄位需求 先以 Json 方式塞進此欄位    |
|     |      |          | CreateTime | datetime       | 新增時間       | 就是驗證時間                                 |

```sql
<!--
CREATE TABLE [dbo].[ED3_VerifyLog](
	[UniqId] [nvarchar](50) ,
	[VerifyType] [nvarchar](20) ,
	[Count] [int] NULL,
	[Request] [nvarchar](max) NULL,
	[Response] [nvarchar](max) NULL,
	[CheckCode] [nvarchar](20) NULL,
	[AuthCode] [nvarchar](20) NULL,
	[Other] [nvarchar](1000) NULL,
	[CreateTime] [datetime] NULL
CONSTRAINT [PK_ED3_VerifyLog] PRIMARY KEY CLUSTERED 
(
	[UniqId] ASC,
	[VerifyType] ASC,
	[Count] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



-->

```
