### 呼叫 API 的 LOG 資料表(ED3_ApiLog)

| PK  | NULL | IDENTITY | 欄位       | 型態          | 說明                    | 代碼說明                 |
| --- | ---- | -------- | ---------- | ------------- | ----------------------- | ------------------------ |
| v   |      | v        | Serial     | int           | 序號, 由資料庫自動產生  |                          |
|     |      |          | Type       | nvarchar(2)   | 1: 我方呼叫 2: 對方呼叫 |                          |
|     |      |          | KeyInfo    | nvarchar(200) | 識別此交易是由誰發出去  | 放 ULID 或其他查詢用資料 |
|     |      |          | ApiName    | nvarchar(200) | 呼叫的 API 名稱         |                          |
|     |      |          | Request    | nvarchar(max) | 呼叫 API 的需求電文     |                          |
|     |      |          | Response   | nvarchar(max) | API 的回應電文          |                          |
|     |      |          | StartTime  | datetime      | API 開始時間            |                          |
|     |      |          | EndTime    | datetime      | API 結束時間            |                          |
|     |      |          | CreateTime | datetime      | 新增時間                |                          |

### 此 Log 為 我方系統呼叫外部系統 或外部系統呼叫我方系統的時候 要記錄的

```sql
<!--
CREATE TABLE [dbo].[ED3_ApiLog](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](2) NOT NULL,
	[KeyInfo] [nvarchar](200) NULL,
	[ApiName] [nvarchar](200) NOT NULL,
	[Request] [nvarchar](max) NOT NULL,
	[Response] [nvarchar](max) NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ED3_ApiLog] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX [IX_ED3_ApiLog] ON [dbo].[ED3_ApiLog]
(
	[KeyInfo] ASC,
	[ApiName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

-->


```