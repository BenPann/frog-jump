### Mail Hunter 紀錄 (MailHunterHistory)

| PK  | NULL | IDENTITY | 欄位         | 型態           | 說明        | 代碼說明                   |
| --- | ---- | -------- | ------------ | -------------- | ----------- | -------------------------- |
| v   |      | v        | Serial       | int            | 序號        | 由資料庫自動產生           |
|     |      |          | UniqId       | nvarchar(50)   | 主要 KEY 值 |                            |
|     | v    |          | Type         | nvarchar(50)   | 寄件類型    |                            |
|     |      |          | TemplateId   | nvarchar(20)   | Template Id |                            |
|     | v    |          | ChtName      | nvarchar(50)   | 中文名稱    |                            |
|     | v    |          | Idno         | nvarchar(50)   | Idno        |                            |
|     | v    |          | EMailAddress | nvarchar(MAX)  | Email       |                            |
|     | v    |          | Status       | nvarchar(20)   | 狀態        | 0:準備寄出, 1:成功, 9:失敗 |
|     | v    |          | ErrorMessage | nvarchar(1000) | 失敗訊息    |                            |
|     | v    |          | Title        | nvarchar(200)  | 信件標題    |                            |
|     | v    |          | Content      | nvarchar(MAX)  | 信件內容    |                            |
|     | v    |          | ProjectCode  | nvarchar(20)   | 專案        |                            |
|     | v    |          | OwnerID      | nvarchar(20)   | 創立人員    |                            |
|     | v    |          | ProductCode  | nvarchar(20)   | 產品代碼    |                            |
|     | v    |          | Attatchment  | varbinary(MAX) | 附檔        |                            |
|     |      |          | CreateTime   | datetime       |             |                            |
|     |      |          | UpdateTime   | datetime       |             |                            |

```sql
<!--
CREATE TABLE [dbo].[MailHunterHistory](
	[Serial] [int] IDENTITY(1,1) NOT NULL,
	[UniqId] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NULL,
	[TemplateId] [nvarchar](20) NOT NULL,
	[ChtName] [nvarchar](50) NULL,
	[Idno] [nvarchar](50) NULL,
	[EMailAddress] [nvarchar](max) NULL,
	[Status] [nvarchar](20) NULL,
	[ErrorMessage] [nvarchar](1000) NULL,
	[Title] [nvarchar](200) NULL,
	[Content] [nvarchar](max) NULL,
	[ProjectCode] [nvarchar](20) NULL,
	[OwnerID] [nvarchar](20) NULL,
	[ProductCode] [nvarchar](20) NULL,
	[Attatchment] [varbinary](max) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_MailHunterHistory] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


-->

```
