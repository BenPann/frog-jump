### 信件範本 (MailTemplate)

| PK  | NULL | IDENTITY | 欄位       | 型態          | 說明                 | 代碼說明 |
| --- | ---- | -------- | ---------- | ------------- | -------------------- | -------- |
| v   |      |          | Type       | nvarchar(50)  | 寄件類型             |          |
| v   |      |          | TagName    | nvarchar(20)  | Mail Hunter Tag Name | 不能亂設 |
|     |      |          | Content    | nvarchar(MAX) | 信件內容             |          |
|     |      |          | CreateTime | datetime      |                      |          |

```sql
<!--

CREATE TABLE [dbo].[MailTemplate](
	[Type] [nvarchar](50) NOT NULL,
	[TagName] [nvarchar](50) NOT NULL,
	[Content] [nvarchar](max) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_MailTemplate] PRIMARY KEY CLUSTERED
(
	[Type] ASC,
	[TagName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO



-->

```
