### 選擇產品 資料表(ED3_Casedata_CGproduct)

| PK  | NULL | IDENTITY | 欄位      | 型態         | 說明                     | 代碼說明 |
| --- | ---- | -------- | --------- | ------------ | ------------------------ | -------- |
| v   |      |          | UniqId    | nvarchar(50) | 交易編號                 |          |
|    |      |          | MainProduct | varchar(20)  | 大首頁後產品選擇產品編號 |          |
|    | v     |          | SubProduct | nvarchar(MAX)  | 大首頁後產品選擇產品編號 |          |
|    |      |          | CreateTime | datetime  | 建立時間 |          |
|    |      |          | UpdateTime | datetime  | 更新時間 |          |
