### 讀取基本資料 資料表(CG_SelectProductList_UserType)

| PK  | NULL | IDENTITY | 欄位         | 型態        | 說明                     | 代碼說明 |
| --- | ---- | -------- | ------------ | ----------- | ------------------------ | -------- |
|     |      |          | UserTypeID   | varchar(20) | 客戶身份別               |          |
|     |      |          | CGProductID  | varchar(20) | 大首頁後產品選擇產品編號 |          |
|     |      |          | PProductType | varchar(20) | 主產品種類               |          |

<!--


-->
