### 客戶類型 資料表(CG_UserType)

| PK  | NULL | IDENTITY | 欄位         | 型態          | 說明         | 代碼說明 |
| --- | ---- | -------- | ------------ | ------------- | ------------ | -------- |
| v   |      |          | UserTypeID   | varchar(20)   | 客戶類型代號 |          |
|     |      |          | UserTypeName | varchar(20)   | 客戶類型名稱 |          |
|     | v    |          | UserTypeDef  | nvarchar(MAX) | 說明         |          |

<!--
CREATE TABLE [dbo].[CG_UserType](
	[UserTypeID] [varchar](20) NOT NULL,
	[UserTypeName] [nvarchar](20) NOT NULL,
	[UserTypeDef] [nvarchar](max) NULL,
 CONSTRAINT [PK_CG_UserType] PRIMARY KEY CLUSTERED
(
	[UserTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO



-->
