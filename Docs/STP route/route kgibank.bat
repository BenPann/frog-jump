@echo off
route Print interface
set /p var=請輸入介面清單的區域網路網卡代號:
If /I "%var%"=="" Echo "未輸入" && Goto End 
echo Route Delete 172.31.1.0 (1/7)
route delete 172.31.1.0 mask 255.255.255.0
echo Route Delete 172.31.2.0 (2/7)
route delete 172.31.2.0 mask 255.255.255.0
echo Route Delete 172.31.10.0 (3/7)
route delete 172.31.10.0 mask 255.255.255.0
echo Route Delete 172.18.12.0 (4/7)
route delete 172.18.12.0 mask 255.255.255.0
echo Route Delete 172.16.253.0 (5/7)
route delete 172.16.253.0 mask 255.255.255.0
echo Route Delete 172.16.3.0 (6/7)
route delete 172.16.3.0 mask 255.255.255.0
echo Route Delete 172.31.7.0 (7/7)
route delete 172.31.7.0 mask 255.255.255.0
echo Route Delete 10.86.18.0 (7/7)
route delete 10.86.18.0 mask 255.255.255.0
echo Route Delete 172.17.131.11 (7/7)
route delete 172.17.131.11 mask 255.255.255.0
echo Route Delete Complete

echo Route Add 172.31.1.0 (1/7)
route add -p 172.31.1.0 mask 255.255.255.0 10.86.19.254 IF %var%
echo Route Add 172.31.2.0 (2/7)
route add -p 172.31.2.0 mask 255.255.255.0 10.86.19.254 IF %var%
echo Route Add 172.31.10.0 (3/7)
route add -p 172.31.10.0 mask 255.255.255.0 10.86.19.254 IF %var%
echo Route Add 172.18.12.0 (4/7)
route add -p 172.18.12.0 mask 255.255.255.0 10.86.19.254 IF %var%
echo Route Add 172.16.253.0 (5/7)
route add -p 172.16.253.0 mask 255.255.255.0 10.86.19.254 IF %var%
echo Route Add 172.16.3.0 (6/7)
route add -p 172.16.3.0 mask 255.255.255.0 10.86.19.254 IF %var%
echo Route Add 172.31.7.0 (7/7)
route add -p 172.31.7.0 mask 255.255.255.0 10.86.19.254 IF %var%
echo Route Add 10.86.18.0 (7/7)
route add -p 10.86.18.0 mask 255.255.255.0 10.86.19.254 IF %var%
echo Route Add 172.17.131.11 (7/7)
route add -p 172.17.131.11 mask 255.255.255.0 10.86.19.254 IF %var%
echo Route Add Complete
pause
:End